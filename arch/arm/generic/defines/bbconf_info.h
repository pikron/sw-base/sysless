#ifndef _BBCONF_INFO_H_
#define _BBCONF_INFO_H_

#if 1 /* Provide FLASH start directly */

/*FIXME: should not be provided directly*/
#ifndef BBCONF_FLASH_START
#define BBCONF_FLASH_START 0x00000000
#endif

#else /* FLASH start taken from ldscript */

#ifndef __ASSEMBLY__
extern char __flash_base;
#define BBCONF_FLASH_START ((unsigned long)(&__flash_base))
#else /*__ASSEMBLY__*/
.global __flash_base
#define BBCONF_FLASH_START __flash_base
#endif /*__ASSEMBLY__*/

#endif /* decission about flash start */

#define BBCONF_MAGIC_VAL  0xd1ab46d6

#if defined(__thumb2__) || defined (__ARM_ARCH_6M__)
#define BBCONF_MAGIC_ADDR (BBCONF_FLASH_START+0x20)
#define BBCONF_PTPTR_ADDR (BBCONF_FLASH_START+0x24)

#else
#define BBCONF_MAGIC_ADDR (BBCONF_FLASH_START+0x40)
#define BBCONF_PTPTR_ADDR (BBCONF_FLASH_START+0x44)
#endif

#define BBCONF_PTTAG_END          0x00
#define BBCONF_PTTAG_BBVER        0x01
#define BBCONF_PTTAG_KVPB_START   0x12
#define BBCONF_PTTAG_KVPB_BYCFI   0x13
#define BBCONF_PTTAG_KVPB_SIZE    0x14
#define BBCONF_PTTAG_WITH_BATPACK 0x15

#define BBCONF_PT_MAX_CNT         0x80


#ifndef __ASSEMBLY__

int bbconf_get_param(unsigned long tag, unsigned long *pval);

#endif /*__ASSEMBLY__*/

#endif /*_BBCONF_INFO_H_*/
