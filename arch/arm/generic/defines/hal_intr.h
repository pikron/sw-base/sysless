#ifndef HAL_INTR_H
#define HAL_INTR_H

#include <hal_ints.h>

//--------------------------------------------------------------------------
// Static data used by HAL
// ISR tables
extern uint32_t hal_interrupt_handlers[HAL_ISR_COUNT];
extern uint32_t hal_interrupt_data[HAL_ISR_COUNT];

//--------------------------------------------------------------------------
// Default ISR
// The #define is used to test whether this routine exists, and to allow
// code outside the HAL to call it.
 
typedef void (*hal_isr)(int vector, uint32_t data); //function ptr
extern uint32_t hal_default_isr(int vector, uint32_t data);
#define HAL_DEFAULT_ISR hal_default_isr

//--------------------------------------------------------------------------
// Vector translation.

#ifndef HAL_TRANSLATE_VECTOR
#define HAL_TRANSLATE_VECTOR(_vector_,_index_) \
    (_index_) = (_vector_)
#endif

//--------------------------------------------------------------------------
// Interrupt and VSR attachment macros

#define HAL_INTERRUPT_IN_USE( _vector_, _state_)                          \
    {                                                                     \
    uint32_t _index_;                                                     \
    HAL_TRANSLATE_VECTOR ((_vector_), _index_);                           \
                                                                          \
    if( hal_interrupt_handlers[_index_] == (uint32_t)hal_default_isr ) \
        (_state_) = 0;                                                    \
    else                                                                  \
        (_state_) = 1;                                                    \
    }

#define HAL_INTERRUPT_ATTACH( _vector_, _isr_, _data_)                     \
    {                                                                      \
    if( hal_interrupt_handlers[_vector_] == (uint32_t)hal_default_isr ) \
    {                                                                      \
        hal_interrupt_handlers[_vector_] = (uint32_t)_isr_;             \
        hal_interrupt_data[_vector_] = (uint32_t) _data_;                  \
    }                                                                      \
    }

#define HAL_INTERRUPT_DETACH( _vector_, _isr_ )                            \
    {                                                                      \
    if( (hal_interrupt_handlers[_vector_] == (uint32_t)_isr_) || !(_isr_) )  \
    {                                                                      \
        hal_interrupt_handlers[_vector_] = (uint32_t)hal_default_isr;   \
        hal_interrupt_data[_vector_] = 0;                                  \
    }                                                                      \
    }


//--------------------------------------------------------------------------
// Interrupt controller access

extern void hal_interrupt_mask(int);
extern void hal_interrupt_unmask(int);
extern void hal_interrupt_acknowledge(int);
extern void hal_interrupt_configure(int, int, int);
extern void hal_interrupt_set_level(int, int);

#define HAL_INTERRUPT_MASK( _vector_ )                     \
    hal_interrupt_mask( _vector_ ) 
#define HAL_INTERRUPT_UNMASK( _vector_ )                   \
    hal_interrupt_unmask( _vector_ )
#define HAL_INTERRUPT_ACKNOWLEDGE( _vector_ )              \
    hal_interrupt_acknowledge( _vector_ )
#define HAL_INTERRUPT_CONFIGURE( _vector_, _level_, _up_ ) \
    hal_interrupt_configure( _vector_, _level_, _up_ )
#define HAL_INTERRUPT_SET_LEVEL( _vector_, _level_ )       \
    hal_interrupt_set_level( _vector_, _level_ )

#endif /* HAL_DEFAULT_ISR */
