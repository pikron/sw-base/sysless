#include <string.h>
#include <keyvalpb.h>
#include <lpciap.h>

unsigned long lpciap_buff[ISP_RAM2FLASH_BLOCK_SIZE/4];
char *lpciap_addr_base=NULL;

#define ISP_RAM2FLASH_BLOCK_SIZE_MASK (ISP_RAM2FLASH_BLOCK_SIZE-1)

int lpcisp_kvpb_erase(struct kvpb_block *store, void *base,int size)
{
  return lpcisp_erase(base, size);
}

int lpcisp_kvpb_flush(struct kvpb_block *store) 
{
  if (lpciap_addr_base==NULL) return -1;
  lpcisp_write(lpciap_addr_base,lpciap_buff,ISP_RAM2FLASH_BLOCK_SIZE);
  lpciap_addr_base=NULL;
  return 0;
}

int lpcisp_kvpb_copy(struct kvpb_block *store,void *des, const void *src, int len)
{
  char *addr_base,*addr_src=(char*)src;
  int cp_len;

  while(len) {
    addr_base=(char*)((unsigned long)des&~ISP_RAM2FLASH_BLOCK_SIZE_MASK);
    cp_len=ISP_RAM2FLASH_BLOCK_SIZE-((unsigned long)des&ISP_RAM2FLASH_BLOCK_SIZE_MASK);
    if (len<cp_len) cp_len=len;
    if (lpciap_addr_base) {
      if (lpciap_addr_base!=addr_base) {
        lpcisp_kvpb_flush(store);
        memcpy(lpciap_buff,addr_base,ISP_RAM2FLASH_BLOCK_SIZE);
        lpciap_addr_base=addr_base;
      }
    } else {
      memcpy(lpciap_buff,addr_base,ISP_RAM2FLASH_BLOCK_SIZE);
      lpciap_addr_base=addr_base;
    }
    memcpy((char*)lpciap_buff+((unsigned long)des&ISP_RAM2FLASH_BLOCK_SIZE_MASK),addr_src,cp_len);
    des=(char*)des+cp_len;
    addr_src+=cp_len;
    len-=cp_len;
    if (((unsigned long)des&ISP_RAM2FLASH_BLOCK_SIZE_MASK)==0x00) 
      lpcisp_kvpb_flush(store);
  }
  return 1;
}
