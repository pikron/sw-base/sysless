#ifndef _LPCIAP_KVPB_H
#define _LPCIAP_KVPB_H

#include <system_def.h>

extern unsigned long lpciap_buff[ISP_RAM2FLASH_BLOCK_SIZE/4];

int lpcisp_kvpb_erase(struct kvpb_block *store, void *base,int size);
int lpcisp_kvpb_flush(struct kvpb_block *store); 
int lpcisp_kvpb_copy(struct kvpb_block *store,void *des, const void *src, int len);

#endif  /* _LPCIAP_KVPB_ */
