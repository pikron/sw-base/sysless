/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  bbconf_info.c - boot block config parameters retrieval

  Copyright (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#include <string.h>
#include <bbconf_info.h>

int bbconf_get_param(unsigned long tag, unsigned long *pval)
{
  unsigned long *magic  = (unsigned long *)BBCONF_MAGIC_ADDR;
  unsigned long *pt_ptr = (unsigned long *)BBCONF_PTPTR_ADDR;
  unsigned long *pt;
  int cnt;

  if(*magic != BBCONF_MAGIC_VAL)
    return -2;

  if(!(*pt_ptr) || !(*pt_ptr+1))
    return -2;

  pt = (unsigned long *)*pt_ptr;

  for(cnt = 0; (cnt < BBCONF_PT_MAX_CNT) &&
      (pt[0] != BBCONF_PTTAG_END); cnt++, pt += 2) {
    if(pt[0] == tag) {
      *pval=pt[1];
      return 1;
    }
  }
  return -1;
}
