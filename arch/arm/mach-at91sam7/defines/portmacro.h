#ifndef PORTMACRO_H
#define PORTMACRO_H

#include <stddef.h>
#include <inttypes.h>

#define THUMB_INTERWORK
#define inline

#ifndef LITTLE_ENDIAN
#define LITTLE_ENDIAN 1234
#endif

#ifndef uint64_t
#define uint64_t unsigned long long /**< 64-bit */
#endif

#ifndef int64_t
#define int64_t signed long long    /**< 64-bit */
#endif

#define false (0)
#define true  (1)

#ifndef FALSE
#define FALSE        false
#endif
#ifndef TRUE
#define TRUE         true
#endif

#endif /* PORTMACRO_H */
