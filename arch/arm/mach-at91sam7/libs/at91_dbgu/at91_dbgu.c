/*
 * at91_dbgu.c
 *
 * A driver for the DBGU serial unit of the Atmel AT91 series.
 *
 * Copyright (c) 2009 Tran Duy Khanh. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <stdint.h>
#include <system_def.h>
#include <lib_AT91SAM7XC256.h>
#include <hwinit.h>
#include <system_stub.h>

#include "at91_dbgu.h"

#define configCPU_CLOCK_HZ		((unsigned long) 48000000)
#define DBGU_BAUD_RATE			115200

/* Initialization of the serial console. */
void at91_dbgu_init(void)
{
	AT91F_DBGU_CfgPIO();
	((AT91PS_USART)AT91C_BASE_DBGU)->US_CR = AT91C_US_RSTTX|AT91C_US_RSTRX;
	AT91F_US_Configure(
		(AT91PS_USART)AT91C_BASE_DBGU,
		configCPU_CLOCK_HZ,
		AT91C_US_ASYNC_MODE,
		DBGU_BAUD_RATE,
		0);
	/* Enable Transmitter & receiver */
	((AT91PS_USART)AT91C_BASE_DBGU)->US_CR = AT91C_US_RXEN | AT91C_US_TXEN;
}

/* Put a char to the serial console. */
int at91_dbgu_putchar(int ch)
{
	/* wait for TX buffer to empty */
	while (!(AT91C_BASE_DBGU->DBGU_CSR & AT91C_US_TXRDY))
		continue;

	/* put char to Transmit Holding Register */
	AT91C_BASE_DBGU->DBGU_THR = (uint8_t)ch;

	/* return a char - stdio.h compatible? */
	return (uint8_t)ch;
}

/* Write to the serial console. */
int at91_dbgu_write(int file, const char *ptr, int len)
{
	int cnt;
	unsigned char ch;

	for (cnt=0; cnt<len; cnt++,ptr++) {
		ch = *ptr;
		if(ch == 0xa)
			at91_dbgu_putchar(0xd);
		at91_dbgu_putchar(ch);
	}

	return cnt;
}

void at91_dbgu_init_printf(void)
{
	/* Debug Unit initialization */
	at91_dbgu_init();

	system_stub_ops.write = at91_dbgu_write;
}
