/*
 * at91_dbgu.h
 *
 * A driver for the DBGU serial unit of the Atmel AT91 series.
 *
 * Copyright (c) 2009 Tran Duy Khanh. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef __AT91_DBGU_H__
#define __AT91_DBGU_H__

void at91_dbgu_init(void);
int at91_dbgu_putchar(int ch);
int at91_dbgu_write(int file, const char *ptr, int len);
void at91_dbgu_init_printf(void);

#endif /* __AT91_DBGU_H__ */
