/*
 * at91_leds.c
 *
 * LED manipulating functions.
 *
 * Derived from the M2M implementation of Open Controller, by Ruud Vlaming
 * and Peter W. Zuidema.
 * Tran Duy Khanh <tran@pbmaster.org>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <system_def.h>
#include <lib_AT91SAM7XC256.h>
#include <at91_timer.h>
#include "at91_leds.h"

#if (AT91B_NB_LED == 8)
static const uint32_t ledMask[AT91B_NB_LED] = {
	AT91B_LED1, AT91B_LED2, AT91B_LED3, AT91B_LED4,
	AT91B_LED5, AT91B_LED6, AT91B_LED7, AT91B_LED8
};
#else
static const uint32_t ledMask[AT91B_NB_LED] = {
	AT91B_LED1, AT91B_LED2, AT91B_LED3, AT91B_LED4
};
#endif

#define ledMASK_MASK (AT91B_NB_LED-1)

void at91_set_led(unsigned int led, int val)
{
	if (!val)
		AT91F_PIO_SetOutput(AT91D_BASE_PIO_LED,
				ledMask[led & ledMASK_MASK]);
	if (val)
		AT91F_PIO_ClearOutput(AT91D_BASE_PIO_LED,
				ledMask[led & ledMASK_MASK]);
}

void at91_all_leds_on(void)
{
	int i;
	for (i=0; i<AT91B_NB_LED; i++)
		at91_set_led(i, 1);
}

void at91_all_leds_off(void)
{
	int i;
	for (i=0; i<AT91B_NB_LED; i++)
		at91_set_led(i,0);
}

void at91_catch(unsigned int i)
{
	while (1) {
		at91_all_leds_on();
		at91_wait_20_millisec(10);
		at91_all_leds_off();
	}
}

void at91_toggle_led(int num, int *toggle)
{
	if (*toggle)
		at91_set_led(num, 1);
	else
		at91_set_led(num, 0);
	*toggle = !(*toggle);
}
