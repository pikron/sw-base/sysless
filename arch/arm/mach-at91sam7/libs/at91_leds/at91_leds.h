/*
 * at91_leds.h
 *
 * LED manipulating functions.
 *
 * Derived from the M2M implementation of Open Controller, by Ruud Vlaming
 * and Peter W. Zuidema.
 * Tran Duy Khanh <tran@pbmaster.org>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef AT91_LEDS_H
#define AT91_LEDS_H

#include "portmacro.h"

void at91_set_led(unsigned int led, int val);
void at91_all_leds_on(void);
void at91_all_leds_off(void);
void at91_catch(unsigned int i);
void at91_toggle_led(int num, int *toggle);

#endif /* AT91_LEDS_H */
