/*
 * at91_timer.c
 *
 * Timer manipulating functions.
 *
 * Derived from the M2M implementation of Open Controller, by Ruud Vlaming
 * and Peter W. Zuidema.
 * Tran Duy Khanh <tran@pbmaster.org>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <system_def.h>
#include <lib_AT91SAM7XC256.h>
#include "at91_timer.h"

#if Timer_Counter_Number == 0
#define AT91C_ID_TCn AT91C_ID_TC0
#define AT91C_BASE_TCn AT91C_BASE_TC0
#elif Timer_Counter_Number == 1
#define AT91C_ID_TCn AT91C_ID_TC1
#define AT91C_BASE_TCn AT91C_BASE_TC1
#elif Timer_Counter_Number == 2
#define AT91C_ID_TCn AT91C_ID_TC2
#define AT91C_BASE_TCn AT91C_BASE_TC2
#endif

static void at91_start_timer(void);

#define DIVISOR_FOR_20MS	29952

static void at91_init_timer(void)
{
	/* Enable the clock of the TIMER */
	AT91F_PMC_EnablePeriphClock( AT91C_BASE_PMC, 1<<AT91C_ID_TCn );
	/* Disable the clock and the interupts */
	AT91C_BASE_TCn->TC_CCR = AT91C_TC_CLKDIS;
	AT91C_BASE_TCn->TC_IDR = AT91C_TC_COVFS | AT91C_TC_LOVRS |
				AT91C_TC_CPAS | AT91C_TC_CPBS |
				AT91C_TC_CPCS | AT91C_TC_LDRAS |
				AT91C_TC_LDRBS | AT91C_TC_ETRGS ;
	/* Set the Mode of the Timer Counter (fastest mode MCK/32)
	 * and RC compare */
	AT91C_BASE_TCn->TC_CMR =
		AT91C_TC_CLKS_TIMER_DIV3_CLOCK | AT91C_TC_CPCTRG;
	/* Write the compare register with the magical value */
	AT91C_BASE_TCn->TC_RC = DIVISOR_FOR_20MS;
	/* Enable the clock */
	AT91C_BASE_TCn->TC_CCR = AT91C_TC_CLKEN;
}

static void at91_start_timer(void)
{
	volatile int status;

	/* Clear status register */
	status = AT91C_BASE_TCn->TC_SR;
	/* Reset and start the timer */
	AT91C_BASE_TCn->TC_CCR = AT91C_TC_SWTRG;
}

static int at91_check_timer(void)
{
	/* Check if counter value passed the magical value
	 * by checking the status rc compare bit. */
	return ((AT91C_BASE_TCn->TC_SR & AT91C_TC_CPCS)>0);
}

void at91_wait_20_millisec(int count)
{
	at91_init_timer();
	while ((count--)>0) {
		at91_start_timer();
		while (!at91_check_timer());
	}
}
