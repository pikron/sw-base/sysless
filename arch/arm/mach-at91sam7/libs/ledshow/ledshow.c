/*
 * ledshow.c
 *
 * An open implementation of Profibus DP functionalities.
 * Led show routines.
 *
 * Copyright (c) 2009 Tran Duy Khanh. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * Project homepage:
 *
 *	http://www.pbmaster.org
 *	http://pbmaster.sourceforge.net
 *
 * Contact information:
 *
 *	Tran Duy Khanh	<tran@pbmaster.org>
 *			<trandk1@users.sourceforge.net>
 */

#include "ledshow.h"

void led_show1(struct ledshow *ls)
{
	unsigned char i;

	ls->pattern = 0x01;
	ls->set(ls->pattern);

	for (i=0; i<ls->led_cnt; i++) {
		ls->set(ls->pattern);
		ls->pattern = ls->pattern << 1;
		ls->wait(1);
	}

	for (i=0; i<ls->led_cnt; i++) {
		ls->set((0x01 << (ls->led_cnt-1)));
		ls->wait(1);
		ls->set(0x0);
		ls->wait(1);
	}

	ls->pattern = (0x01 << (ls->led_cnt-1));
	for (i=0; i<ls->led_cnt; i++) {
		ls->set(ls->pattern);
		ls->pattern = ls->pattern >> 1;
		ls->wait(1);
	}

	for (i=0; i<ls->led_cnt; i++) {
		ls->set(0x01);
		ls->wait(1);
		ls->set(0x0);
		ls->wait(1);
	}
}

void led_show2(struct ledshow *ls)
{
	unsigned char i;

	ls->pattern = 0x01;
	ls->set(ls->pattern);

	for (i=0; i<ls->led_cnt; i++) {
		ls->set(ls->pattern);
		ls->pattern = (ls->pattern << 1) | 0x01;
		ls->wait(2);
	}

	ls->pattern = (0x01 << (ls->led_cnt-1));
	for (i=0; i<ls->led_cnt; i++) {
		ls->set(ls->pattern);
		ls->pattern = (ls->pattern >> 1) | (0x01 << (ls->led_cnt-1));
		ls->wait(2);
	}
}

void led_show3(struct ledshow *ls)
{
	unsigned char i;

	for (i=0; i<ls->led_cnt; i++) {
		ls->set(0x99);
		ls->wait(1);
		ls->set(0x0);
		ls->wait(1);
	}

	for (i=0; i<ls->led_cnt; i++) {
		ls->set(0x66);
		ls->wait(1);
		ls->set(0x0);
		ls->wait(1);
	}
}

void led_show4(struct ledshow *ls)
{
	unsigned char i;

	ls->pattern = 0x01;
	ls->set(ls->pattern);
	for (i=0; i<ls->led_cnt; i++) {
		ls->set(ls->pattern);
		ls->pattern = ls->pattern << 1;
		ls->wait(1);
	}

	ls->pattern = (0x01 << (ls->led_cnt-1));
	for (i=0; i<ls->led_cnt; i++) {
		ls->set(ls->pattern);
		ls->pattern = ls->pattern >> 1;
		ls->wait(1);
	}
}

void led_show5(struct ledshow *ls)
{
	unsigned char i;

	ls->pattern=0x1;
	ls->set(ls->pattern);

	for (i=0; i<ls->led_cnt; i++) {
		ls->set(ls->pattern);
		ls->pattern = (ls->pattern << 1) | 0x01;
		ls->wait(2);
	}

	ls->pattern = 0xff;
	for (i=0; i<ls->led_cnt; i++) {
		ls->set(ls->pattern);
		ls->pattern = (ls->pattern << 1);
		ls->wait(1);
	}

	ls->pattern = 0x00;
	ls->set(ls->pattern);

	for (i=0; i<ls->led_cnt; i++) {
		ls->set(ls->pattern);
		ls->pattern = (ls->pattern >> 1) | (0x01 << (ls->led_cnt-1));
		ls->wait(2);
	}

	ls->pattern = 0xff;
	for (i=0; i<ls->led_cnt; i++) {
		ls->set(ls->pattern);
		ls->pattern = (ls->pattern >> 1);
		ls->wait(1);
	}
}

void led_show6(struct ledshow *ls)
{
	ls->set(0x55);
	ls->wait(3);
	ls->set(0xaa);
	ls->wait(3);
}

void led_show7(struct ledshow *ls)
{
	ls->set(0x99);
	ls->wait(2);
	ls->set(0x66);
	ls->wait(2);
}

void led_show8(struct ledshow *ls)
{
	unsigned char i;

	for (i=0; i<6; i++) {
		ls->set(0xcc);
		ls->wait(1);
		ls->set(0x00);
		ls->wait(1);
	}

	for (i=0; i<6; i++) {
		ls->set(0x33);
		ls->wait(1);
		ls->set(0x00);
		ls->wait(1);
	}
}
