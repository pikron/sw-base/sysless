/*
 * ledshow.h
 *
 * An open implementation of Profibus DP functionalities.
 * Led show routines.
 *
 * Copyright (c) 2009 Tran Duy Khanh. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * Project homepage:
 *
 *	http://www.pbmaster.org
 *	http://pbmaster.sourceforge.net
 *
 * Contact information:
 *
 *	Tran Duy Khanh	<tran@pbmaster.org>
 *			<trandk1@users.sourceforge.net>
 */

#ifndef __LEDSHOW_H__
#define __LEDSHOW_H__

struct ledshow {
	int led_cnt;
	void (*set)(unsigned char pattern);
	void (*wait) (unsigned int count);
	void (**shows)(struct ledshow *ls);
	unsigned char pattern;
};

void led_show1(struct ledshow *ls);
void led_show2(struct ledshow *ls);
void led_show3(struct ledshow *ls);
void led_show4(struct ledshow *ls);
void led_show5(struct ledshow *ls);
void led_show6(struct ledshow *ls);
void led_show7(struct ledshow *ls);
void led_show8(struct ledshow *ls);

#endif /* __LEDSHOW_H__ */
