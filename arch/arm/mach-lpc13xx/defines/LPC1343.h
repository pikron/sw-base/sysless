/**
 * LPC1343.h - header file for LPC1343
 */


#ifndef _LPC1343_HEADER_FILE_
  #define _LPC1343_HEADER_FILE_

#include "LPC13xx.h"
#include "lpcSYSCON.h"
// #include "lpcIOCON.h"
#include "lpcUART.h"
#include "lpcUSB.h"

#endif /* _LPC1343_HEADER_FILE_ */
