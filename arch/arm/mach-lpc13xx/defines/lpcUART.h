/******************************************************************************
 * Header file for NXP LPC134x UART enabled ARM Processors 
 * 2015 Roman Bartosinski <bartosr@centrum.cz>
 *
 * No guarantees, warrantees, or promises, implied or otherwise.
 * May be used for hobby or commercial purposes provided copyright
 * notice remains intact or GPL license is applied.
 *
 *****************************************************************************/

#ifndef _lpcUART_HEADER_FILE_
#define _lpcUART_HEADER_FILE_

/* Divisor Latch LSB (DLAB=1) */
#define UART_DLL_MASK           0xFF

/* Divisor Latch MSB (DLAB=1) */
#define UART_DLM_MASK           0xFF

/* Interrupt Enable Register */
#define UART_IER_RBRIE          (1 << 0)    // Enable Receive Data Available Interrupt
#define UART_IER_THREIE         (1 << 1)    // Enable Transmit Holding Register Empty Interrupt
#define UART_IER_RXLIE          (1 << 2)    // Enable Receive Line Status Interrupt
#define UART_IER_ABEOINTEN      (1 << 8)    // Enable the end of auto-baud interrupt
#define UART_IER_ABTOINTEN      (1 << 9)    // Enable the auto-baud time-out interrupt

/* Interrupt Identification Register */
#define UART_IIR_NO_INT         (1 << 0)    // NO INTERRUPTS PENDING
#define UART_IIR_MASK_ID        0x0E
#define UART_IIR_ID_MODEM       (0 << 1)    // MODEM Status
#define UART_IIR_ID_THRE        (1 << 1)    // Transmit Holding Register Empty
#define UART_IIR_ID_RDA         (2 << 1)    // Receive Data Available
#define UART_IIR_ID_RLS         (3 << 1)    // Receive Line Status
#define UART_IIR_ID_CTI         (6 << 1)    // Character Timeout Indicator
//#define UART_IIR_FIFOEN1        (1 << 6)
//#define UART_IIR_FIFOEN2        (1 << 7)
#define UART_IIR_ABEOINT        (1 << 8)    // End of auto-baud interrupt
#define UART_IIR_ABTOINT        (1 << 9)    // Auto-baud time-out interrupt

/* FIFO Control Register */
#define UART_FCR_FIFOEN         (1 << 0)    // Enable both FIFOs
#define UART_FCR_RXFIFOR        (1 << 1)    // Reset Receive FIFO
#define UART_FCR_TXFIFOR        (1 << 2)    // Reset Transmit FIFO
#define UART_FCR_RXTLVL_1CH     (0 << 6)    // Trigger @ 1 character in FIFO
#define UART_FCR_RXTLVL_4CH     (1 << 6)    // Trigger @ 4 characters in FIFO
#define UART_FCR_RXTLVL_8CH     (2 << 6)    // Trigger @ 8 characters in FIFO
#define UART_FCR_RXTLVL_14CH    (3 << 6)    // Trigger @ 14 characters in FIFO

/* Line Control Register */
#define UART_LCR_MASK_WLS       0x03
#define UART_LCR_WLS_5          (0 << 0)    // 5-bit character length
#define UART_LCR_WLS_6          (1 << 0)    // 6-bit character length
#define UART_LCR_WLS_7          (2 << 0)    // 7-bit character length
#define UART_LCR_WLS_8          (3 << 0)    // 8-bit character length
#define UART_LCR_MASK_STOP      0x04
#define UART_LCR_STOP_1         (0 << 2)    // 1 stop bit
#define UART_LCR_STOP_2         (1 << 2)    // 2 stop bits
#define UART_LCR_MASK_PAR       0x38
#define UART_LCR_PAR_NO         (0 << 3)    // No Parity
#define UART_LCR_PAR_ODD        (1 << 3)    // Odd Parity
#define UART_LCR_PAR_EVEN       (3 << 3)    // Even Parity
#define UART_LCR_PAR_MARK       (5 << 3)    // MARK "1" Parity
#define UART_LCR_PAR_SPACE      (7 << 3)    // SPACE "0" Paruty
#define UART_LCR_BREAK          (1 << 6)    // Output BREAK line condition
#define UART_LCR_DLAB           (1 << 7)    // Enable Divisor Latch Access

/* Modem Control Register */
#define UART_MCR_DTR            (1 << 0)    // Data Terminal Ready
#define UART_MCR_RTS            (1 << 1)    // Request To Send
#define UART_MCR_LMS            (1 << 4)    // Loopback
#define UART_MCR_RTSEN          (1 << 6)    // RTS enable
#define UART_MCR_CTSEN          (1 << 7)    // CTS enable

/* Line Status Register */
#define UART_LSR_RDR            (1 << 0)    // Receive Data Ready
#define UART_LSR_EOVERRUN       (1 << 1)    // Overrun Error
#define UART_LSR_EPARITY        (1 << 2)    // Parity Error
#define UART_LSR_EFRAME         (1 << 3)    // Framing Error
#define UART_LSR_BREAKINT       (1 << 4)    // Break Interrupt
#define UART_LSR_THRE           (1 << 5)    // Transmit Holding Register Empty
#define UART_LSR_TEMT           (1 << 6)    // Transmitter Empty
#define UART_LSR_RXFE           (1 << 7)    // Error in Receive FIFO
#define UART_LSR_MASK_ERR       0x8E

/* Modem Status Register */
#define UART_MSR_DCTS           (1 << 0)    // Delta Clear To Send
#define UART_MSR_DDSR           (1 << 1)    // Delta Data Set Ready
#define UART_MSR_TERI           (1 << 2)    // Trailing Edge Ring Indicator
#define UART_MSR_DDCD           (1 << 3)    // Delta Data Carrier Detect
#define UART_MSR_CTS            (1 << 4)    // Clear To Send
#define UART_MSR_DSR            (1 << 5)    // Data Set Ready
#define UART_MSR_RI             (1 << 6)    // Ring Indicator
#define UART_MSR_DCD            (1 << 7)    // Data Carrier Detect

/* Scratch Pad Register */
#define UART_SCR_MASK_PAD       0xFF

/* Auto-baud Control Register */
#define UART_ACR_START          (1 << 0)    // Auto-baud start
#define UART_ACR_MODE           (1 << 1)    // Mode select bit
#define UART_ACR_AUTORESTART    (1 << 2)    // Restart in case of time-out
#define UART_ACR_ABEOINTCLR     (1 << 8)    // End of auto-baud interrupt clear bit
#define UART_ACR_ABTOINTCLR     (1 << 9)    // Auto-baud time-out interrupt clear bit

/* Fractional Divider Register */
#define UART_FDR_MASK_DIVADDVAL  0x0f
#define UART_FDR_SHIFT_DIVADDVAL 0
#define UART_FDR_MASK_MULVAL     0xf0
#define UART_FDR_SHIFT_MULVAL    4

/* Transmit Enable Register */
#define UART_TER_TXEN           (1 << 7)    //

/* RS485 Control Register */
#define UART_RS485CTRL_NMMEN    (1 << 0)    // NMM enable
#define UART_RS485CTRL_RXDIS    (1 << 1)    // Receiver enable
#define UART_RS485CTRL_AADEN    (1 << 2)    // AAD enable
#define UART_RS485CTRL_SEL      (1 << 3)    // Direction control pins select
#define UART_RS485CTRL_DCTRL    (1 << 4)    // Direction control enable
#define UART_RS485CTRL_OINV     (1 << 5)    // Reverse the polarity of the direction control signal

/* RS485 Address Match Register */
#define UART_RS485ADR_MASK      0xFF

/* RS485 Delay Value Register */
#define UART_RS485DLY_MASK      0xFF


#endif /* _lpcUART_HEADER_FILE_ */
