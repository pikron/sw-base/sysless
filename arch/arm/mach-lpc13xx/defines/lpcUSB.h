/******************************************************************************
 *
 * $RCSfile$
 * $Revision$
 *
 * Header file for NXP LPC134x USB enabled ARM Processors 
 * 2015 Roman Bartosinski <bartosr@centrum.cz>
 * Based on lpcUSB.h for LPC214x by Pavel Pisa <pisa@cmp.felk.cvut.cz>
 *
 * No guarantees, warrantees, or promises, implied or otherwise.
 * May be used for hobby or commercial purposes provided copyright
 * notice remains intact or GPL license is applied.
 *
 *****************************************************************************/

#ifndef _lpcUSB_HEADER_FILE_
#define _lpcUSB_HEADER_FILE_

/* Device interrupt registers */
#define USBDevIntSt_o       0x0000  /* USB Device Interrupt Status (RO) */
#define USBDevIntEn_o       0x0004  /* USB Device Interrupt Enable (R/W) */
#define USBDevIntClr_o      0x0008  /* USB Device Interrupt Clear (WO) */
#define USBDevIntSet_o      0x000C  /* USB Device Interrupt Set (WO) */

#define   USBDevInt_FRAME     (1<<0) /* Frame interrupt @1kHz for ISO transfers*/
#define   USBDevInt_EP_0      (1<<1) /* USB core interrupt for physical endpoint 0 */
#define   USBDevInt_EP_1      (1<<2) /* USB core interrupt for physical endpoint 1 */
#define   USBDevInt_EP_2      (1<<3) /* USB core interrupt for physical endpoint 2 */
#define   USBDevInt_EP_3      (1<<4) /* USB core interrupt for physical endpoint 3 */
#define   USBDevInt_EP_4      (1<<5) /* USB core interrupt for physical endpoint 4 */
#define   USBDevInt_EP_5      (1<<6) /* USB core interrupt for physical endpoint 5 */
#define   USBDevInt_EP_6      (1<<7) /* USB core interrupt for physical endpoint 6 */
#define   USBDevInt_EP_7      (1<<8) /* USB core interrupt for physical endpoint 7 */
#define   USBDevInt_DEV_STAT  (1<<9) /* USB Bus reset, USB suspend change or Connect occured */
#define   USBDevInt_CCEMPTY   (1<<10) /* Command code register is empty/ready for CMD */
#define   USBDevInt_CDFULL    (1<<11) /* Command data register is full/data available */
#define   USBDevInt_RxENDPKT  (1<<12) /* Current packet in the FIFO is transferred to the CPU */
#define   USBDevInt_TxENDPKT  (1<<13) /* TxPacket bytes written to FIFO */

#define USBDevIntPri_o        0x002C  /* USB Device Interrupt Priority (WO) */
#define   USBDevIntPri_FRAME    (1<<0) /*0/1 FRAME int routed to the low/high priority interrupt line */
#define   USBDevIntPri_BULKOUT  (1<<1) /*0/1 int routed for bulk out endpoints (for logEP 3 = phyEP 6,7) */
#define   USBDevIntPri_BULKIN   (1<<2) /*0/1 int routed for bulk in endpoints (for logEP 3 = phyEP 6,7) */
//#define   USBDevIntPri_EP_FAST  (1<<1) /*0/1 EP_FAST int routed to the low/high priority line*/


/* USB transfer registers */
#define USBRxData_o           0x0018  /* USB Receive Data (RO) */
#define USBRxPLen_o           0x0020  /* USB Receive Packet Length (RO) */
#define   USBRxPLen_PKT_LNGTH   (0x03FF) /* Remaining amount of bytes to be read from RAM */
#define   USBRxPLen_DV          (1<<10)  /* Data valid. 0 only for error ISO packet */
//#define   USBRxPLen_PKT_RDY   (1<<11)  /* Packet length valid and packet is ready for reading */
#define USBTxData_o           0x001C  /* USB Transmit Data (WO) */
#define USBTxPLen_o           0x0024  /* USB Transmit Packet Length (WO) */
#define   USBTxPLen_PKT_LNGTH   (0x03FF) /* Remaining amount of bytes to be written to the EP_RAM */
#define USBCtrl_o             0x0028  /* USB Control (R/W) */
#define   USBCtrl_RD_EN         (1<<0) /*Read mode control*/
#define   USBCtrl_WR_EN         (1<<1) /*Write mode control*/
#define   USBCtrl_LOG_ENDPOINT  0x003C /*Logical Endpoint number*/

/* Command registers */
#define USBCmdCode_o            0x0010  /* USB Command Code (WO) */
#define   USBCmdCode_CMD_PHASE    0x0000FF00 /*The command phase*/
#define   USBCmdCode_CMD_CODE     0x00FF0000 /*The code for the command*/
#define USBCmdData_o            0x0014  /* USB Command Data (RO) */

/* Miscellaneous registers */
#define USBDevFIQSel_o          0x002c  /* USB Device FIQ Select register (RW) */
#define   USBDevFIQ_FRAME_EN      0x01
#define   USBDevFIQ_BULKOUT_EN    0x02
#define   USBDevFIQ_BULKIN_EN     0x04

/* -------------------------------------------------------------------------- */
/* Command Codes - the MSByte means number bytes for reading/writing and it is cleared when it is send to SIE */
#define USB_CMD_SET_ADDR        0x01D00500
#define USB_CMD_CFG_DEV         0x01D80500
#define USB_CMD_SET_MODE        0x01F30500
#define USB_CMD_RD_INT_STAT     0x02F40500
#define USB_DAT_RD_INT_STAT     0x00F40200
#define USB_CMD_RD_FRAME        0x02F50500
#define USB_DAT_RD_FRAME        0x00F50200
#define USB_CMD_RD_TEST         0x02FD0500
#define USB_DAT_RD_TEST         0x00FD0200
#define USB_CMD_SET_DEV_STAT    0x01FE0500
#define USB_CMD_GET_DEV_STAT    0x01FE0500
#define USB_DAT_GET_DEV_STAT    0x00FE0200
#define USB_CMD_GET_ERR_CODE    0x01FF0500
#define USB_DAT_GET_ERR_CODE    0x00FF0200
#define USB_DAT_WR_BYTE(x)     (0x00000100 | ((((uint32_t)x) & 0xff) << 16))
#define USB_CMD_SEL_EP(x)      (0x01000500 | ((((uint32_t)x) & 0xff) << 16))
#define USB_DAT_SEL_EP(x)      (0x00000200 | ((((uint32_t)x) & 0xff) << 16))
#define USB_CMD_SEL_EP_CLRI(x) (0x01400500 | ((((uint32_t)x) & 0xff) << 16))
#define USB_DAT_SEL_EP_CLRI(x) (0x00400200 | ((((uint32_t)x) & 0xff) << 16))
#define USB_CMD_SET_EP_STAT(x) (0x01400500 | ((((uint32_t)x) & 0xff) << 16))
#define USB_CMD_CLR_BUF         0x01F20500
#define USB_DAT_CLR_BUF         0x00F20200
#define USB_CMD_VALID_BUF       0x00FA0500

/* Device Address Register Definitions */
#define USBC_DEV_ADDR_MASK       0x7F
#define USBC_DEV_EN              0x80

/* Device Configure Register Definitions */
#define USBC_CONF_DEVICE         0x01

/* Device Mode Register Definitions */
#define USBC_AP_CLK              0x01
#define USBC_INAK_CI             0x02
#define USBC_INAK_CO             0x04
#define USBC_INAK_AI             0x08
#define USBC_INAK_AO             0x10
//#define USBC_INAK_BI             0x20
//#define USBC_INAK_BO             0x40

/* Device Read Interrupt Status Definitions */
/* byte 1 */
#define USBC_INT_EP0             0x0001
#define USBC_INT_EP1             0x0002
#define USBC_INT_EP2             0x0004
#define USBC_INT_EP3             0x0008
#define USBC_INT_EP4             0x0010
/* byte 2 */
#define USBC_D_ST                0x0400

/* Device Status Register Definitions */
#define USBC_DEV_CON             0x01
#define USBC_DEV_CON_CH          0x02
#define USBC_DEV_SUS             0x04
#define USBC_DEV_SUS_CH          0x08
#define USBC_DEV_RST             0x10

/* Error Code Register Definitions */
#define USBC_ERR_EC_MASK         0x0F
#define USBC_ERR_EA              0x10

/* Error Status Register Definitions */
#define USBC_ERR_NOERR           0x00
#define USBC_ERR_PID_ENCODING    0x01
#define USBC_ERR_PID_UNKNOWN     0x02
#define USBC_ERR_UNEXP_PKT       0x03
#define USBC_ERR_CRC_TOKEN       0x04
#define USBC_ERR_CRC_DATA        0x05
#define USBC_ERR_TIMEOUT         0x06
#define USBC_ERR_BABBLE          0x07
#define USBC_ERR_EOP             0x08
#define USBC_ERR_TXRXNAK         0x09
#define USBC_ERR_TXSTALL         0x0A
#define USBC_ERR_B_OVRN          0x0B
#define USBC_ERR_TXEMPTY         0x0C
#define USBC_ERR_BTSTF           0x0D
#define USBC_ERR_SYNC            0x0E
#define USBC_ERR_TGL             0x0F

/* Endpoint Select Register Definitions */
#define USBC_EP_SEL_F            0x01
#define USBC_EP_SEL_ST           0x02
#define USBC_EP_SEL_STP          0x04
#define USBC_EP_SEL_PO           0x08
#define USBC_EP_SEL_EPN          0x10
#define USBC_EP_SEL_B_1_FULL     0x20
#define USBC_EP_SEL_B_2_FULL     0x40

/* Endpoint Status Register Definitions */
#define USBC_EP_STAT_ST          0x01
#define USBC_EP_STAT_DA          0x20
#define USBC_EP_STAT_RF_MO       0x40
#define USBC_EP_STAT_CND_ST      0x80

/* Clear Buffer Register Definitions */
#define USBC_CLR_BUF_PO          0x01

/* -------------------------------------------------------------------------- */

#define USBDevIntSt     LPC_USB->DevIntSt
#define USBDevIntEn     LPC_USB->DevIntEn
#define USBDevIntClr    LPC_USB->DevIntClr
#define USBDevIntSet    LPC_USB->DevIntSet

#define USBCmdCode      LPC_USB->CmdCode
#define USBCmdData      LPC_USB->CmdData

#define USBRxData       LPC_USB->RxData
#define USBRxPLen       LPC_USB->RxPLen
#define USBTxData       LPC_USB->TxData
#define USBTxPLen       LPC_USB->TxPLen
#define USBCtrl         LPC_USB->Ctrl


#endif /* _lpcUSB_HEADER_FILE_ */
