#include <system_def.h>
#include <cpu_def.h>
#include <hal_machperiph.h>

//#include <stdint.h>
//#include <LPC13xx.h>


/*----------------------------------------------------------------------------
  Check the register settings
 *----------------------------------------------------------------------------*/
#define CHECK_RANGE(val, min, max)                ((val < min) || (val > max))
#define CHECK_RSVD(val, mask)                     (val & mask)

#if (CHECK_RANGE((SYSMEMREMAP_Val), SYSCON_MEMREMAP_MAP_BOOTLOADER, SYSCON_MEMREMAP_MAP_FLASH))
   #error "SYSMEMREMAP: Invalid values of reserved bits!"
#endif

#if (CHECK_RSVD((SYSPLLCTRL_Val),  ~(SYSCON_SYSPLLCTRL_MASK_MSEL | SYSCON_SYSPLLCTRL_MASK_PSEL)))
   #error "SYSPLLCTRL: Invalid values of reserved bits!"
#endif

#if (CHECK_RSVD((USBPLLCTRL_Val),  ~(SYSCON_USBPLLCTRL_MASK_MSEL | SYSCON_USBPLLCTRL_MASK_PSEL)))
   #error "USBPLLCTRL: Invalid values of reserved bits!"
#endif

#if (CHECK_RSVD((SYSOSCCTRL_Val),  ~0x00000003))
   #error "SYSOSCCTRL: Invalid values of reserved bits!"
#endif

#if (CHECK_RSVD((WDTOSCCTRL_Val),  ~(SYSCON_WDTOSCCTRL_MASK_DIVSEL | SYSCON_WDTOSCCTRL_MASK_FREQSEL)))
   #error "WDTOSCCTRL: Invalid values of reserved bits!"
#endif

#if (CHECK_RANGE((SYSPLLCLKSEL_Val), SYSCON_SYSPLLCLKSEL_SEL_IRC, SYSCON_SYSPLLCLKSEL_SEL_SYSOSC))
   #error "SYSPLLCLKSEL: Value out of range!"
#endif

#if (CHECK_RANGE((USBPLLCLKSEL_Val), SYSCON_USBPLLCLKSEL_SEL_IRC, SYSCON_USBPLLCLKSEL_SEL_SYSOSC))
   #error "USBPLLCLKSEL: Value out of range!"
#endif

#if (CHECK_RANGE((MAINCLKSEL_Val),  SYSCON_MAINCLKSEL_SEL_IRC, SYSCON_MAINCLKSEL_SEL_PLLOUT))
   #error "MAINCLKSEL: Invalid values of reserved bits!"
#endif

#if (CHECK_RSVD((SYSAHBCLKDIV_Val), ~SYSCON_SYSAHBCLKDIV_MASK_DIV))
   #error "SYSAHBCLKDIV: Value out of range!"
#endif

#if (CHECK_RSVD((SYSAHBCLKCTRL_Val),  ~0x0005FFFF))
   #error "AHBCLKCTRL: Invalid values of reserved bits!"
#endif

#if (CHECK_RSVD((SSP0CLKDIV_Val), ~SYSCON_SSP0CLKDIV_MASK_DIV))
   #error "SSP0CLKDIV: Value out of range!"
#endif

#if (CHECK_RSVD((UARTCLKDIV_Val), ~SYSCON_UARTCLKDIV_MASK_DIV))
   #error "UARTCLKDIV: Value out of range!"
#endif

#if (CHECK_RSVD((SSP1CLKDIV_Val), ~SYSCON_SSP1CLKDIV_MASK_DIV))
   #error "SSP1CLKDIV: Value out of range!"
#endif

#if (CHECK_RSVD((TRACECLKDIV_Val), ~SYSCON_TRACECLKDIV_MASK_DIV))
   #error "TRACECLKDIV: Value out of range!"
#endif

#if (CHECK_RSVD((SYSTICKCLKDIV_Val), ~SYSCON_SYSTICKCLKDIV_MASK_DIV))
   #error "SYSTICKCLKDIV: Value out of range!"
#endif

#if (CHECK_RANGE((USBCLKSEL_Val), SYSCON_USBCLKSEL_SEL_USBPLL, SYSCON_USBCLKSEL_SEL_MAINCLK))
   #error "USBCLKSEL: Invalid values of reserved bits!"
#endif

#if (CHECK_RSVD((USBCLKDIV_Val), ~SYSCON_USBCLKDIV_MASK_DIV))
   #error "USBCLKDIV: Value out of range!"
#endif

#if (CHECK_RANGE((WDTCLKSEL_Val), SYSCON_WDTCLKSEL_SEL_IRC, SYSCON_WDTCLKSEL_SEL_WDTOSC))
   #error "WDTCLKSEL: Invalid values of reserved bits!"
#endif

#if (CHECK_RSVD((WDTCLKDIV_Val), ~SYSCON_WDTCLKDIV_MASK_DIV))
   #error "WDTCLKDIV: Value out of range!"
#endif

#if (CHECK_RANGE((CLKOUTCLKSEL_Val), SYSCON_CLKOUTCLKSEL_SEL_IRC, SYSCON_CLKOUTCLKSEL_SEL_MAINCLK))
   #error "CLKOUTCLKSEL: Invalid values of reserved bits!"
#endif

#if (CHECK_RSVD((CLKOUTDIV_Val), ~SYSCON_CLKOUTDIV_MASK_DIV))
   #error "CLKOUTDIV: Value out of range!"
#endif



/*----------------------------------------------------------------------------
  Clock Variable definitions
 *----------------------------------------------------------------------------*/
unsigned int system_frequency = IRC_OSC; /*!< System Clock Frequency (Core Clock)  */

/*----------------------------------------------------------------------------
  Clock functions
 *----------------------------------------------------------------------------*/
void system_core_clock_update (void)            /* Get Core Clock Frequency      */
{
  uint32_t wdt_osc = 0;
  uint32_t pllin_clk, pllout_clk;

  /* Determine clock frequency according to clock register values             */
  switch ((LPC_SYSCON->WDTOSCCTRL >> 5) & 0x0F) {
    case 0:  wdt_osc =  400000; break;
    case 1:  wdt_osc =  500000; break;
    case 2:  wdt_osc =  800000; break;
    case 3:  wdt_osc = 1100000; break;
    case 4:  wdt_osc = 1400000; break;
    case 5:  wdt_osc = 1600000; break;
    case 6:  wdt_osc = 1800000; break;
    case 7:  wdt_osc = 2000000; break;
    case 8:  wdt_osc = 2200000; break;
    case 9:  wdt_osc = 2400000; break;
    case 10: wdt_osc = 2600000; break;
    case 11: wdt_osc = 2700000; break;
    case 12: wdt_osc = 2900000; break;
    case 13: wdt_osc = 3100000; break;
    case 14: wdt_osc = 3200000; break;
    case 15: wdt_osc = 3400000; break;
  }
  wdt_osc /= ((LPC_SYSCON->WDTOSCCTRL & 0x1F) << 1) + 2;

  switch (LPC_SYSCON->SYSPLLCLKSEL & SYSCON_SYSPLLCLKSEL_MASK_SEL) {
    case SYSCON_SYSPLLCLKSEL_SEL_IRC:
      pllin_clk = IRC_OSC;
      break;
    case SYSCON_SYSPLLCLKSEL_SEL_SYSOSC:
      pllin_clk = OSC_CLK;
      break;
  }
  pllout_clk = pllin_clk * ((LPC_SYSCON->SYSPLLCTRL & SYSCON_SYSPLLCTRL_MASK_MSEL)+1);

  switch (LPC_SYSCON->MAINCLKSEL & SYSCON_MAINCLKSEL_MASK_SEL) {
    case SYSCON_MAINCLKSEL_SEL_IRC:     /* Internal RC oscillator             */
      system_frequency = IRC_OSC;
      break;
    case SYSCON_MAINCLKSEL_SEL_PLLIN:   /* Input Clock to System PLL          */
      system_frequency = pllin_clk;
      break;
    case SYSCON_MAINCLKSEL_SEL_WDTOSC:  /*  */
      system_frequency = wdt_osc;
      break;
    case SYSCON_MAINCLKSEL_SEL_PLLOUT:                             /* System PLL Clock Out               */
      system_frequency = pllout_clk;
      break;
  }

  system_frequency /= (LPC_SYSCON->SYSAHBCLKDIV & SYSCON_SYSAHBCLKDIV_MASK_DIV);
}

void system_clock_init(void)
{
  #if (CLOCK_SETUP)                                 /* Clock Setup              */
  #if (SYSCLK_SETUP)                                /* System Clock Setup       */

  #if (SYSOSC_SETUP)                                /* System Oscillator Setup  */

  LPC_SYSCON->PDRUNCFG     &= ~SYSCON_PDRUNCFG_SYSOSC_PD;          /* Power-up System Osc      */
  LPC_SYSCON->SYSOSCCTRL    = SYSOSCCTRL_Val;
  #endif /* SYSOSC_SETUP */

  #if (SYSPLL_SETUP)                                /* System PLL Setup         */
  LPC_SYSCON->SYSPLLCLKSEL  = SYSPLLCLKSEL_Val;     /* Select PLL Input         */
  LPC_SYSCON->SYSPLLCLKUEN  = SYSCON_SYSPLLCLKUEN_ENA;                  /* Toggle Update Register      */
  LPC_SYSCON->SYSPLLCLKUEN  = 0x00;

  LPC_SYSCON->SYSPLLCTRL    = SYSPLLCTRL_Val;
  LPC_SYSCON->PDRUNCFG     &= ~SYSCON_PDRUNCFG_SYSPLL_PD;               /* Power-up SYSPLL      */
  while (!(LPC_SYSCON->SYSPLLSTAT & SYSCON_SYSPLLSTAT_MASK_LOCK)) ;     /* Wait Until PLL Locked    */
  #else
  LPC_SYSCON->PDRUNCFG     |=  SYSCON_PDRUNCFG_SYSPLL_PD;
  #endif /* SYSPLL_SETUP */

  LPC_SYSCON->MAINCLKSEL    = MAINCLKSEL_Val;     /* Select Clock */
  LPC_SYSCON->MAINCLKUEN    = SYSCON_MAINCLKUEN_ENA;              /* Update MCLK Clock Source */
  LPC_SYSCON->MAINCLKUEN    = 0x00;               /* Toggle Update Register   */

  #endif /* SYSCLK_SETUP */

  #if (USBCLK_SETUP)                                /* USB Clock Setup          */
  LPC_SYSCON->PDRUNCFG     &= ~SYSCON_PDRUNCFG_USBPAD_PD;         /* Power-up USB PHY         */

  #if (USBPLL_SETUP)                                /* USB PLL Setup            */
  LPC_SYSCON->PDRUNCFG     &= ~SYSCON_PDRUNCFG_USBPLL_PD;         /* Power-up USB PLL         */
  LPC_SYSCON->USBPLLCLKSEL  = USBPLLCLKSEL_Val;   /* Select PLL Input         */
  LPC_SYSCON->USBPLLCLKUEN  = SYSCON_USBPLLCLKUEN_ENA;            /* Update Clock Source      */
  LPC_SYSCON->USBPLLCLKUEN  = 0x00;               /* Toggle Update Register   */
  LPC_SYSCON->USBPLLCTRL    = USBPLLCTRL_Val;
  while (!(LPC_SYSCON->USBPLLSTAT & SYSCON_USBPLLSTAT_MASK_LOCK)) ;     /* Wait Until PLL Locked    */
  LPC_SYSCON->USBCLKSEL     = SYSCON_USBCLKSEL_SEL_USBPLL;               /* Select USB PLL           */
  #else /* USBPLL_SETUP */
  LPC_SYSCON->USBCLKSEL     = SYSCON_USBCLKSEL_SEL_MAINCLK;               /* Select Main Clock        */
  #endif /* USBPLL_SETUP */

  #else /* USBCLK_SETUP */
  LPC_SYSCON->PDRUNCFG     |=  SYSCON_PDRUNCFG_USBPAD_PD;         /* Power-down USB PHY       */
  LPC_SYSCON->PDRUNCFG     |=  SYSCON_PDRUNCFG_USBPLL_PD;         /* Power-down USB PLL       */
  #endif /* USBCLK_SETUP */

  LPC_SYSCON->SYSAHBCLKDIV  = SYSAHBCLKDIV_Val;
  #if (SSP0CLKDIV_Val)
    LPC_SYSCON->SSP0CLKDIV = SSP0CLKDIV_Val;
  #endif
  #if (SSP1CLKDIV_Val)
    LPC_SYSCON->SSP1CLKDIV = SSP1CLKDIV_Val;
  #endif
  #if (UARTCLKDIV_Val)
    LPC_SYSCON->UARTCLKDIV = UARTCLKDIV_Val;
  #endif
  #if (TRACECLKDIV_Val)
    LPC_SYSCON->TRACECLKDIV = TRACECLKDIV_Val;
  #endif
  #if (SYSTICKCLKDIV_Val)
    LPC_SYSCON->SYSTICKCLKDIV = SYSTICKCLKDIV_Val;
  #endif

#endif /* CLOCK_SETUP */

  LPC_SYSCON->SYSAHBCLKCTRL = SYSAHBCLKCTRL_Val;
  LPC_SYSCON->PDRUNCFG     |= SYSCON_PDRUNCFG_WDTOSC_PD;     /* Power down WDT, if it will be used, it should be enabled egain*/

  system_core_clock_update();
}


//TODO: check WDT settings
void lpc_watchdog_feed()
{
  unsigned long flags;

  save_and_cli(flags);
  LPC_WDT->FEED = 0xAA;
  LPC_WDT->FEED = 0x55;
  restore_flags(flags);
}

void lpc_watchdog_init(int on,int timeout_ms)
{
  if (!on) return;
  LPC_SYSCON->WDTOSCCTRL     = WDTOSCCTRL_Val;
  LPC_SYSCON->WDTCLKDIV      = WDTCLKDIV_Val;        //preddelic musi byt >0 (0 je disable, 1= deleni 1 atd.)
  LPC_SYSCON->PDRUNCFG      &= ~SYSCON_PDRUNCFG_WDTOSC_PD;        // Power-up WDT Clock (kdyby byl nahodou vyputy, vychozi je ale zapnuto)
  LPC_SYSCON->SYSAHBCLKCTRL |= SYSCON_SYSAHBCLKCTRL_WDT;   // je nutne povolit hodiny WDT v SYSAHBCLKCTRL

  LPC_SYSCON->WDTCLKSEL = SYSCON_WDTCLKSEL_SEL_WDTOSC; //zdroj hodin bude WDT oscilator
  LPC_SYSCON->WDTCLKUEN = SYSCON_WDTCLKUEN_ENA;
  LPC_SYSCON->WDTCLKUEN = 0;
  LPC_WDT->TC  = (((__WDT_OSC_CLK) / 1000) * (timeout_ms / 4));
  //LPC_WDT->TC = 1000;//(__WDT_OSC_CLK/4)*(1000/timeout_ms);
  LPC_WDT->MOD = 0x03;           /* Enable watchdog timer and reset */
}

void power_down_unused_periph(void)
{
  if ( (LPC_SYSCON->MAINCLKSEL > 0) &&
       (LPC_SYSCON->WDTCLKSEL > 0) &&
       ((LPC_SYSCON->SYSPLLCLKSEL > 0) ||
        (LPC_SYSCON->PDAWAKECFG & SYSCON_PDRUNCFG_SYSPLL_PD)) )
    LPC_SYSCON->PDRUNCFG |=  SYSCON_PDRUNCFG_IRCOUT_PD | SYSCON_PDRUNCFG_IRC_PD; /* if IRC is not used, power-down IRC */

  //vypnu nepotrebne periferie, pokud je budu potrebovat, zapnou se v initu dane periferie
  LPC_SYSCON->PDRUNCFG   |=  SYSCON_PDRUNCFG_ADC_PD;       /* power-down ADC */
}
