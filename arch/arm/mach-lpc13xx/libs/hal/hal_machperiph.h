#ifndef _HAL_MACHPERIPH_H
#define _HAL_MACHPERIPH_H

#include <system_def.h>

/*----------------------------------------------------------------------------
  Define clocks
 *----------------------------------------------------------------------------*/

#define WDTOSCCTRL_Val        0x000000A0 //frekvence WDT oscilatoru 1,6MHz, delicka 2, vysledna frekvence 0.8MHz

#define __FREQSEL   ((WDTOSCCTRL_Val >> 5) & 0x0F)
#define __DIVSEL   (((WDTOSCCTRL_Val & 0x1F) << 1) + 2)

#ifdef WATCHDOG_ENABLED                    /* Watchdog Oscillator Setup*/
        #if  (__FREQSEL ==  0)
          #define __WDT_OSC_CLK        ( 400000 / __DIVSEL)
        #elif (__FREQSEL ==  1)
          #define __WDT_OSC_CLK        ( 500000 / __DIVSEL)
        #elif (__FREQSEL ==  2)
          #define __WDT_OSC_CLK        ( 800000 / __DIVSEL)
        #elif (__FREQSEL ==  3)
          #define __WDT_OSC_CLK        (1100000 / __DIVSEL)
        #elif (__FREQSEL ==  4)
          #define __WDT_OSC_CLK        (1400000 / __DIVSEL)
        #elif (__FREQSEL ==  5)
          #define __WDT_OSC_CLK        (1600000 / __DIVSEL)
        #elif (__FREQSEL ==  6)
          #define __WDT_OSC_CLK        (1800000 / __DIVSEL)
        #elif (__FREQSEL ==  7)
          #define __WDT_OSC_CLK        (2000000 / __DIVSEL)
        #elif (__FREQSEL ==  8)
          #define __WDT_OSC_CLK        (2200000 / __DIVSEL)
        #elif (__FREQSEL ==  9)
          #define __WDT_OSC_CLK        (2400000 / __DIVSEL)
        #elif (__FREQSEL == 10)
          #define __WDT_OSC_CLK        (2600000 / __DIVSEL)
        #elif (__FREQSEL == 11)
          #define __WDT_OSC_CLK        (2700000 / __DIVSEL)
        #elif (__FREQSEL == 12)
          #define __WDT_OSC_CLK        (2900000 / __DIVSEL)
        #elif (__FREQSEL == 13)
          #define __WDT_OSC_CLK        (3100000 / __DIVSEL)
        #elif (__FREQSEL == 14)
          #define __WDT_OSC_CLK        (3200000 / __DIVSEL)
        #else
          #define __WDT_OSC_CLK        (3400000 / __DIVSEL)
        #endif
#else
          #define __WDT_OSC_CLK        (1600000 / 2)
#endif  // WATCHDOG_ENABLED


#if (CLOCK_SETUP)
  #if (SYSPLLCLKSEL_Val==SYSCON_SYSPLLCLKSEL_SEL_IRC)
    #define PLLIN_CLOCK     IRC_OSC
  #elif (SYSPLLCLKSEL_Val==SYSCON_SYSPLLCLKSEL_SEL_SYSOSC)
    #define PLLIN_CLOCK     OSC_CLK
  #else
    #define PLLIN_CLOCK     IRC_OSC
  #endif /* SYSPLLCLKSEL */

  #define PLLOUT_CLOCK      PLLIN_CLOCK*((SYSPLLCTRL_Val & SYSCON_SYSPLLCTRL_MASK_MSEL)+1)

  #if (MAINCLKSEL_Val==SYSCON_MAINCLKSEL_SEL_IRC)
    #define   MAIN_CLOCK    IRC_OSC
  #elif (MAINCLKSEL_Val==SYSCON_MAINCLKSEL_SEL_PLLIN)
    #define   MAIN_CLOCK    PLLIN_CLOCK
  #elif (MAINCLKSEL_Val==SYSCON_MAINCLKSEL_SEL_WDTOSC)
    #define   MAIN_CLOCK    WDT_OSC
  #elif (MAINCLKSEL_Val==SYSCON_MAINCLKSEL_SEL_PLLOUT)
    #define   MAIN_CLOCK    PLLOUT_CLOCK
  #else
    #define   MAIN_CLOCK    IRC_OSC
  #endif /* MAINCLKSEL */
#else /* CLOCK_SETUP */
  #define   MAIN_CLOCK    IRC_OSC
#endif /* CLOCK_SETUP */

#if (USBCLK_SETUP)

  #if (USBPLLCLKSEL_Val==SYSCON_USBPLLCLKSEL_SEL_IRC)
    #define USBPLLIN_CLOCK    IRC_OSC
  #elif (USBPLLCLKSEL_Val==SYSCON_USBPLLCLKSEL_SEL_SYSOSC)
    #define USBPLLIN_CLOCK    OSC_CLK
  #else
    #define USBPLLIN_CLOCK    IRC_OSC
  #endif

  #define USBPLLOUT_CLOCK      USBPLLIN_CLOCK*((USBPLLCTRL_Val & SYSCON_USBPLLCTRL_MASK_MSEL)+1)

  #if (USBCLKSEL_Val==SYSCON_USBCLKSEL_SEL_USBPLL)
    #define   USB_CLOCK   USBPLLOUT_CLOCK/USBCLKDIV_Val
  #elif (USBCLKSEL_Val==SYSCON_USBCLKSEL_SEL_MAINCLK)
    #define   USB_CLOCK   MAIN_CLOCK/USBCLKDIV_Val
  #else
    #define   USB_CLOCK   USBPLLOUT_CLOCK/USBCLKDIV_Val
  #endif

#else
    #define USB_CLOCK     IRC_OSC
#endif /* USBCLK_SETUP */

extern unsigned int system_frequency; // CPU and ROM clock
#define   PCLK          MAIN_CLOCK  /* kazda periferie ma jeste delicku z main_clock */
#define   PCLK_SSP0     PCLK/SSP0CLKDIV_Val     /* clock for SSP0 */
#define   PCLK_SSP1     PCLK/SSP1CLKDIV_Val     /* clock for SSP1 */
#define   PCLK_UART     PCLK/UARTCLKDIV_Val     /* clock for UART */
#define   PCLK_SYSTICK  PCLK/SYSTICKCLKDIV_Val  /* clock for SysTick */
#define   PCLK_TRACE    PCLK/TRACECLKDIV_Val    /* clock for ARM Trace */

void system_clock_init(void);
void system_core_clock_update (void);
void power_down_unused_periph(void);

void lpc_watchdog_init(int on,int timeout_ms);
void lpc_watchdog_feed();

#endif /* _HAL_MACHPERIPH_H */
