/****************************************************************************//**
 * @file :    startup_LPC17xx.c
 * @brief : CMSIS Cortex-M3 Core Device Startup File
 * @version : V1.01
 * @date :    4. Feb. 2009
 *
 *----------------------------------------------------------------------------
 *
 * Copyright (C) 2009 ARM Limited. All rights reserved.
 *
 * ARM Limited (ARM) is supplying this software for use with Cortex-Mx
 * processor based microcontrollers.  This file can be freely distributed
 * within development tools that are supporting such ARM based processors.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * ARM SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR
 * CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 ******************************************************************************/

// Mod by nio for the .fastcode part

#include "LPC13xx.h"

#define WEAK __attribute__ ((weak))
//*****************************************************************************
//
// Forward declaration of the default fault handlers.
//
//*****************************************************************************
/* System exception vector handler */
void WEAK     Reset_Handler(void);             /* Reset Handler */
void WEAK     NMI_Handler(void);               /* NMI Handler */
void WEAK     HardFault_Handler(void);         /* Hard Fault Handler */
void WEAK     MemManage_Handler(void);         /* MPU Fault Handler */
void WEAK     BusFault_Handler(void);          /* Bus Fault Handler */
void WEAK     UsageFault_Handler(void);        /* Usage Fault Handler */
void WEAK     SVC_Handler(void);               /* SVCall Handler */
void WEAK     DebugMon_Handler(void);          /* Debug Monitor Handler */
void WEAK     PendSV_Handler(void);            /* PendSV Handler */
void WEAK     SysTick_Handler(void);           /* SysTick Handler */


void WEAK       WAKEUP0_IRQHandler(void);
void WEAK       WAKEUP1_IRQHandler(void);
void WEAK       WAKEUP2_IRQHandler(void);
void WEAK       WAKEUP3_IRQHandler(void);
void WEAK       WAKEUP4_IRQHandler(void);
void WEAK       WAKEUP5_IRQHandler(void);
void WEAK       WAKEUP6_IRQHandler(void);
void WEAK       WAKEUP7_IRQHandler(void);
void WEAK       WAKEUP8_IRQHandler(void);
void WEAK       WAKEUP9_IRQHandler(void);
void WEAK       WAKEUP10_IRQHandler(void);
void WEAK       WAKEUP11_IRQHandler(void);
void WEAK       WAKEUP12_IRQHandler(void);
void WEAK       WAKEUP13_IRQHandler(void);
void WEAK       WAKEUP14_IRQHandler(void);
void WEAK       WAKEUP15_IRQHandler(void);
void WEAK       WAKEUP16_IRQHandler(void);
void WEAK       WAKEUP17_IRQHandler(void);
void WEAK       WAKEUP18_IRQHandler(void);
void WEAK       WAKEUP19_IRQHandler(void);
void WEAK       WAKEUP20_IRQHandler(void);
void WEAK       WAKEUP21_IRQHandler(void);
void WEAK       WAKEUP22_IRQHandler(void);
void WEAK       WAKEUP23_IRQHandler(void);
void WEAK       WAKEUP24_IRQHandler(void);
void WEAK       WAKEUP25_IRQHandler(void);
void WEAK       WAKEUP26_IRQHandler(void);
void WEAK       WAKEUP27_IRQHandler(void);
void WEAK       WAKEUP28_IRQHandler(void);
void WEAK       WAKEUP29_IRQHandler(void);
void WEAK       WAKEUP30_IRQHandler(void);
void WEAK       WAKEUP31_IRQHandler(void);
void WEAK       WAKEUP32_IRQHandler(void);
void WEAK       WAKEUP33_IRQHandler(void);
void WEAK       WAKEUP34_IRQHandler(void);
void WEAK       WAKEUP35_IRQHandler(void);
void WEAK       WAKEUP36_IRQHandler(void);
void WEAK       WAKEUP37_IRQHandler(void);
void WEAK       WAKEUP38_IRQHandler(void);
void WEAK       WAKEUP39_IRQHandler(void);
void WEAK       I2C_IRQHandler(void);
void WEAK       TIMER_16_0_IRQHandler(void);
void WEAK       TIMER_16_1_IRQHandler(void);
void WEAK       TIMER_32_0_IRQHandler(void);
void WEAK       TIMER_32_1_IRQHandler(void);
void WEAK       SSP_IRQHandler(void);
void WEAK       UART_IRQHandler(void);
void WEAK       USB_IRQHandler(void);
void WEAK       USB_FIQHandler(void);
void WEAK       ADC_IRQHandler(void);
void WEAK       WDT_IRQHandler(void);
void WEAK       BOD_IRQHandler(void);
void WEAK       EINT3_IRQHandler(void);
void WEAK       EINT2_IRQHandler(void);
void WEAK       EINT1_IRQHandler(void);
void WEAK       EINT0_IRQHandler(void);
void WEAK       SSP1_IRQHandler(void);


/* Exported types --------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
extern unsigned long _etext;
extern unsigned long _sidata;   /* start address for the initialization values of the .data section. defined in linker script */
extern unsigned long _sdata;    /* start address for the .data section. defined in linker script */
extern unsigned long _edata;    /* end address for the .data section. defined in linker script */

extern unsigned long _sifastcode;   /* start address for the initialization values of the .fastcode section. defined in linker script */
extern unsigned long _sfastcode;    /* start address for the .fastcode section. defined in linker script */
extern unsigned long _efastcode;    /* end address for the .fastcode section. defined in linker script */

extern unsigned long _sbss;     /* start address for the .bss section. defined in linker script */
extern unsigned long _ebss;     /* end address for the .bss section. defined in linker script */

extern void _estack;    /* init value for the stack pointer. defined in linker script */

extern void (_setup_board)(void);         /* setup_board adress function */
extern unsigned long _mem_app_start;


/* Private typedef -----------------------------------------------------------*/
/* function prototypes ------------------------------------------------------*/
void Reset_Handler(void) __attribute__((__interrupt__));
//void Reset_Handler(void);
extern int main(void);

typedef void (*FNC)(void);
FNC fnc_entry;

/******************************************************************************
*
* The minimal vector table for a Cortex M3.  Note that the proper constructs
* must be placed on this to ensure that it ends up at physical address
* 0x0000.0000.
*
******************************************************************************/

extern unsigned long _stack;

__attribute__ ((section(".isr_vector")))
void (* const g_pfnVectors[])(void) =
{
        (void (*)(void))&_stack,   /* The initial stack pointer */
        Reset_Handler,             /* Reset Handler */
        NMI_Handler,               /* NMI Handler */
        HardFault_Handler,         /* Hard Fault Handler */
        MemManage_Handler,         /* MPU Fault Handler */
        BusFault_Handler,          /* Bus Fault Handler */
        UsageFault_Handler,        /* Usage Fault Handler */
        0,                         /* Reserved */
        0,                         /* Reserved */
        0,                         /* Reserved */
        0,                         /* Reserved */
        SVC_Handler,               /* SVCall Handler */
        DebugMon_Handler,          /* Debug Monitor Handler */
        0,                         /* Reserved */
        PendSV_Handler,            /* PendSV Handler */
        SysTick_Handler,           /* SysTick Handler */

  // External Interrupts
        WAKEUP0_IRQHandler,
        WAKEUP1_IRQHandler,
        WAKEUP2_IRQHandler,
        WAKEUP3_IRQHandler,
        WAKEUP4_IRQHandler,
        WAKEUP5_IRQHandler,
        WAKEUP6_IRQHandler,
        WAKEUP7_IRQHandler,
        WAKEUP8_IRQHandler,
        WAKEUP9_IRQHandler,
        WAKEUP10_IRQHandler,
        WAKEUP11_IRQHandler,
        WAKEUP12_IRQHandler,
        WAKEUP13_IRQHandler,
        WAKEUP14_IRQHandler,
        WAKEUP15_IRQHandler,
        WAKEUP16_IRQHandler,
        WAKEUP17_IRQHandler,
        WAKEUP18_IRQHandler,
        WAKEUP19_IRQHandler,
        WAKEUP20_IRQHandler,
        WAKEUP21_IRQHandler,
        WAKEUP22_IRQHandler,
        WAKEUP23_IRQHandler,
        WAKEUP24_IRQHandler,
        WAKEUP25_IRQHandler,
        WAKEUP26_IRQHandler,
        WAKEUP27_IRQHandler,
        WAKEUP28_IRQHandler,
        WAKEUP29_IRQHandler,
        WAKEUP30_IRQHandler,
        WAKEUP31_IRQHandler,
        WAKEUP32_IRQHandler,
        WAKEUP33_IRQHandler,
        WAKEUP34_IRQHandler,
        WAKEUP35_IRQHandler,
        WAKEUP36_IRQHandler,
        WAKEUP37_IRQHandler,
        WAKEUP38_IRQHandler,
        WAKEUP39_IRQHandler,
        I2C_IRQHandler,
        TIMER_16_0_IRQHandler,
        TIMER_16_1_IRQHandler,
        TIMER_32_0_IRQHandler,
        TIMER_32_1_IRQHandler,
        SSP_IRQHandler,
        UART_IRQHandler,
        USB_IRQHandler,
        USB_FIQHandler,
        ADC_IRQHandler,
        WDT_IRQHandler,
        BOD_IRQHandler,
        EINT3_IRQHandler,
        EINT2_IRQHandler,
        EINT1_IRQHandler,
        EINT0_IRQHandler,
        SSP1_IRQHandler
};

/*******************************************************************************
* Function Name  : Reset_Handler
* Description    : This is the code that gets called when the processor first starts execution
*          following a reset event.  Only the absolutely necessary set is performed,
*          after which the application supplied main() routine is called.
* Input          :
* Output         :
* Return         :
*******************************************************************************/

void Reset_Handler(void)
{
  unsigned long *pulDest;
  unsigned long *pulSrc;

  //
  // Copy the data segment initializers from flash to SRAM in ROM mode
  //

  if (&_sidata != &_sdata) {  // only if needed
    pulSrc = &_sidata;
    for(pulDest = &_sdata; pulDest < &_edata; ) {
      *(pulDest++) = *(pulSrc++);
    }
  }

  // Copy the .fastcode code from ROM to SRAM

  if (&_sifastcode != &_sfastcode) {  // only if needed
    pulSrc = &_sifastcode;
    for(pulDest = &_sfastcode; pulDest < &_efastcode; ) {
      *(pulDest++) = *(pulSrc++);
    }
  }

  //
  // Zero fill the bss segment.
  //
  for(pulDest = &_sbss; pulDest < &_ebss; )
  {
    *(pulDest++) = 0;
  }

  //
  // remap IRQ vector or not, depending on their RAM/ROM location
  //
// compare isr_table[1] with address 0x00000004 to avoid gcc optimalization "the comparison will always evaluate as 'false' for the address of 'g_pfnVectors' will never be NULL [-Waddress]"
  if ((&g_pfnVectors[1])==(void *)4) { // IRQ table in FLASH
    LPC_SYSCON->SYSMEMREMAP = 2;
    SCB->VTOR=0;
  } else { // IRQ table in RAM
    LPC_SYSCON->SYSMEMREMAP = 1;
    SCB->VTOR=0x10000000;
  }

  // init IO/clk/...
  _setup_board();

  //
  // Call the application's entry point.
  //
  main();

  /* disable interrupts */
  NVIC->ICER[0] = 0xffffffff;
  NVIC->ICER[1] = 0x00000007;
  __set_MSP(*(unsigned long *)(&_mem_app_start));         /* Stack pointer */
  fnc_entry = (FNC)*(unsigned long *)(&_mem_app_start+1); /* Reset handler */
  fnc_entry();
}

//*****************************************************************************
//
// Provide weak aliases for each Exception handler to the Default_Handler.
// As they are weak aliases, any function with the same name will override
// this definition.
//
//*****************************************************************************
#pragma weak MemManage_Handler = Default_Handler          /* MPU Fault Handler */
#pragma weak BusFault_Handler = Default_Handler           /* Bus Fault Handler */
#pragma weak UsageFault_Handler = Default_Handler         /* Usage Fault Handler */
#pragma weak SVC_Handler = Default_Handler                /* SVCall Handler */
#pragma weak DebugMon_Handler = Default_Handler           /* Debug Monitor Handler */
#pragma weak PendSV_Handler = Default_Handler             /* PendSV Handler */
#pragma weak SysTick_Handler = Default_Handler            /* SysTick Handler */


/* External interrupt vector handler */
#pragma weak WAKEUP0_IRQHandler = Default_Handler
#pragma weak WAKEUP1_IRQHandler = Default_Handler
#pragma weak WAKEUP2_IRQHandler = Default_Handler
#pragma weak WAKEUP3_IRQHandler = Default_Handler
#pragma weak WAKEUP4_IRQHandler = Default_Handler
#pragma weak WAKEUP5_IRQHandler = Default_Handler
#pragma weak WAKEUP6_IRQHandler = Default_Handler
#pragma weak WAKEUP7_IRQHandler = Default_Handler
#pragma weak WAKEUP8_IRQHandler = Default_Handler
#pragma weak WAKEUP9_IRQHandler = Default_Handler
#pragma weak WAKEUP10_IRQHandler = Default_Handler
#pragma weak WAKEUP11_IRQHandler = Default_Handler
#pragma weak WAKEUP12_IRQHandler = Default_Handler
#pragma weak WAKEUP13_IRQHandler = Default_Handler
#pragma weak WAKEUP14_IRQHandler = Default_Handler
#pragma weak WAKEUP15_IRQHandler = Default_Handler
#pragma weak WAKEUP16_IRQHandler = Default_Handler
#pragma weak WAKEUP17_IRQHandler = Default_Handler
#pragma weak WAKEUP18_IRQHandler = Default_Handler
#pragma weak WAKEUP19_IRQHandler = Default_Handler
#pragma weak WAKEUP20_IRQHandler = Default_Handler
#pragma weak WAKEUP21_IRQHandler = Default_Handler
#pragma weak WAKEUP22_IRQHandler = Default_Handler
#pragma weak WAKEUP23_IRQHandler = Default_Handler
#pragma weak WAKEUP24_IRQHandler = Default_Handler
#pragma weak WAKEUP25_IRQHandler = Default_Handler
#pragma weak WAKEUP26_IRQHandler = Default_Handler
#pragma weak WAKEUP27_IRQHandler = Default_Handler
#pragma weak WAKEUP28_IRQHandler = Default_Handler
#pragma weak WAKEUP29_IRQHandler = Default_Handler
#pragma weak WAKEUP30_IRQHandler = Default_Handler
#pragma weak WAKEUP31_IRQHandler = Default_Handler
#pragma weak WAKEUP32_IRQHandler = Default_Handler
#pragma weak WAKEUP33_IRQHandler = Default_Handler
#pragma weak WAKEUP34_IRQHandler = Default_Handler
#pragma weak WAKEUP35_IRQHandler = Default_Handler
#pragma weak WAKEUP36_IRQHandler = Default_Handler
#pragma weak WAKEUP37_IRQHandler = Default_Handler
#pragma weak WAKEUP38_IRQHandler = Default_Handler
#pragma weak WAKEUP39_IRQHandler = Default_Handler
#pragma weak I2C_IRQHandler = Default_Handler
#pragma weak TIMER16_0_IRQHandler = Default_Handler
#pragma weak TIMER16_1_IRQHandler = Default_Handler
#pragma weak TIMER32_0_IRQHandler = Default_Handler
#pragma weak TIMER32_1_IRQHandler = Default_Handler
#pragma weak SSP_IRQHandler = Default_Handler
#pragma weak UART_IRQHandler = Default_Handler
#pragma weak USB_IRQHandler = Default_Handler
#pragma weak USB_FIQHandler = Default_Handler
#pragma weak ADC_IRQHandler = Default_Handler
#pragma weak WDT_IRQHandler = Default_Handler
#pragma weak BOD_IRQHandler = Default_Handler
#pragma weak FMC_IRQHandler = Default_Handler
#pragma weak PIOINT3_IRQHandler = Default_Handler
#pragma weak PIOINT2_IRQHandler = Default_Handler
#pragma weak PIOINT1_IRQHandler = Default_Handler
#pragma weak PIOINT0_IRQHandler = Default_Handler
#pragma weak SSP1_IRQHandler = Default_Handler


//*****************************************************************************
//
// This is the code that gets called when the processor receives an unexpected
// interrupt.  This simply enters an infinite loop, preserving the system state
// for examination by a debugger.
//
//*****************************************************************************
void Default_Handler(void) {
  // Go into an infinite loop.
  //
  while (1) {
  }
}
