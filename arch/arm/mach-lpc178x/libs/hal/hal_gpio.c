#include <cpu_def.h>
#include <LPC17xx.h>

#include <hal_gpio.h>

int hal_pin_conf_fnc(unsigned gpio, int fnc)
{
  __IO uint32_t *p = &(LPC_IOCON->P0_0);

  if(fnc & PORT_CONF_FNC_MASK)
    fnc = __mfld2val(PORT_CONF_FNC_MASK, fnc);

  p += hal_gpio_get_port_num(gpio)*32 + (gpio & 0x1f);

  *p = (*p & ~7) | fnc;

  return 0;
}

int hal_pin_conf_mode(unsigned gpio, int mode)
{
  __IO uint32_t *p = &(LPC_IOCON->P0_0);
  uint32_t iocon;

  if(mode & PORT_CONF_MODE_MASK)
    mode = __mfld2val(PORT_CONF_MODE_MASK, mode);

  p += hal_gpio_get_port_num(gpio)*32 + (gpio & 0x1f);

  iocon = *p & ~(3 << 3);
  if (mode == __mfld2val(PORT_CONF_MODE_MASK,PORT_CONF_MODE_ADC)) {
    iocon &= ~(1 << 7);
  } else {
    iocon |= (mode << 3) | (1 << 7);
  }
  *p = iocon ;

  return 0;
}

int hal_pin_conf_od(unsigned gpio, int od)
{
  __IO uint32_t *p = &(LPC_IOCON->P0_0);

  p += hal_gpio_get_port_num(gpio)*32 + (gpio & 0x1f);

  if(od)
    *p |= (1 << 10);
  else
    *p &= ~(1 << 10);

  return 0;
}

int hal_pin_conf_set(unsigned gpio, int conf)
{
  gpio &= ~PORT_CONF_MASK;
  hal_pin_conf_mode(gpio, conf & PORT_CONF_MODE_MASK);
  hal_pin_conf_od(gpio, conf & PORT_CONF_OD_MASK);
  if(conf & PORT_CONF_SET_DIR) {
    if((conf & PORT_CONF_DIR_MASK) == (PORT_CONF_DIR_IN & PORT_CONF_DIR_MASK))
      hal_gpio_direction_input(gpio);
    else
      hal_gpio_direction_output(gpio, conf & PORT_CONF_INIT_HIGH);
  }
  hal_pin_conf_fnc(gpio, conf & PORT_CONF_FNC_MASK);

  return 0;
}
