#ifndef _HAL_GPIO_H_
#define _HAL_GPIO_H_

#include <cpu_def.h>
#include <LPC17xx.h>
#include <hal_gpio_def.h>

#define HAL_GPIO_PORT_BITS 3

static inline
LPC_GPIO_TypeDef *hal_gpio_get_port_base(unsigned port)
{
  char *p = (char*)LPC_GPIO0_BASE;
  p += ((char*)LPC_GPIO1_BASE - (char*)LPC_GPIO0_BASE) * port;
  return (LPC_GPIO_TypeDef *)p;
}

static inline
unsigned hal_gpio_get_port_num(unsigned gpio)
{
  gpio >>= PORT_SHIFT;
  return gpio & ((1 << HAL_GPIO_PORT_BITS) - 1);
}

static inline
LPC_GPIO_TypeDef *hal_gpio_get_base(unsigned gpio)
{
  return hal_gpio_get_port_base(hal_gpio_get_port_num(gpio));
}

static inline
int hal_gpio_get_value(unsigned gpio)
{
  return ((hal_gpio_get_base(gpio)->PIN) >> (gpio & 0x1f)) & 1;
}

static inline
void hal_gpio_set_value(unsigned gpio, int value)
{
  if(value)
    hal_gpio_get_base(gpio)->SET = 1 << (gpio & 0x1f);
  else
    hal_gpio_get_base(gpio)->CLR = 1 << (gpio & 0x1f);
}

static inline
int hal_gpio_direction_input(unsigned gpio)
{
  hal_gpio_get_base(gpio)->DIR &= ~(1 << (gpio & 0x1f));
  return 0;
}

static inline
int hal_gpio_direction_output(unsigned gpio, int value)
{
  hal_gpio_set_value(gpio, value);
  hal_gpio_get_base(gpio)->DIR |= (1 << (gpio & 0x1f));
  return 0;
}

int hal_pin_conf_fnc(unsigned gpio, int fnc);

int hal_pin_conf_mode(unsigned gpio, int mode);

int hal_pin_conf_od(unsigned gpio, int od);

int hal_pin_conf_set(unsigned gpio, int conf);

static inline
int hal_pin_conf(unsigned gpio)
{
  return hal_pin_conf_set(gpio, gpio);
}

#endif /*_HAL_GPIO_H_*/
