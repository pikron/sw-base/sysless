#include <cpu_def.h>
#include <LPC17xx.h>
#include <hal_mpu.h>
#include <string.h>

void hal_mpu_clear(void)
{
  int i;
  int reg_cnt = hal_mpu_region_count();
  uint32_t rbar_dis = 0;
  uint32_t rasr_dis = 0;

  MPU->CTRL &= ~MPU_CTRL_ENABLE_Msk;

  for (i = 0; i < reg_cnt; i++) {
    MPU->RBAR = rbar_dis | MPU_RBAR_VALID_Msk | i;
    MPU->RASR = rasr_dis;
  }
}

int hal_mpu_find_empty_region(void)
{
  int i;
  int reg_cnt = hal_mpu_region_count();

  for (i = 0; i < reg_cnt; i++) {
    MPU->RNR = i;
    if (!(MPU->RASR & MPU_RASR_ENA_Msk))
      return i;
  }
  return -1;
}

void hal_mpu_set_region(int rnr, uint32_t addr, uint32_t size, uint32_t attrib)
{
  uint32_t szfld;

  if (size >= 0x80000000)
    szfld = 31;
  else
    szfld = 31 - __CLZ(size * 2 - 1);

  if (szfld < 5)
    szfld = 5;
  addr &= ~((1 << szfld) - 1);

  MPU->RBAR = addr | MPU_RBAR_VALID_Msk | rnr;
  MPU->RASR = __val2mfld(MPU_RASR_SIZE_Msk, szfld - 1) | attrib;
}

int hal_mpu_cover_region(uint32_t addr, uint32_t size, uint32_t attrib)
{
  int i;
  int reg_cnt = hal_mpu_region_count();
  int rnr = -1;
  uint32_t rsz, ra, rsznear = 0xffffffff;
  int update = 0;

  for (i = 0; i < reg_cnt; i++) {
    MPU->RNR = i;
    if (!(MPU->RASR & MPU_RASR_ENA_Msk)) {
      if (rnr < 0)
        rnr = i;
    } else {
      ra = MPU->RBAR & MPU_RBAR_ADDR_Msk;
      rsz = (1 << (__mfld2val(MPU_RASR_SIZE_Msk, MPU->RASR) + 1)) - 1;
      if ((ra + rsz >= addr) && (ra - addr < size)) {
        if (rsz <= rsznear) {
          rnr = i;
          rsznear = rsz;
        }
      }
    }
  }
  if (rnr >= 0) {
    if ((rsznear >= size) && update)
      size = rsznear + 1;
    hal_mpu_set_region(rnr, addr, size, attrib);
  }
  return rnr;
}
