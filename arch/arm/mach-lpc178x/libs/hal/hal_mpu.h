#ifndef _HAL_MPU_H_
#define _HAL_MPU_H_

#include <cpu_def.h>
#include <LPC17xx.h>
#include <stdint.h>

#define HAL_MPU_ATTR_COMBINE(XN, AP, TEX, S, C, B, ENA) ( \
	__val2mfld(MPU_RASR_XN_Msk, XN) | \
	__val2mfld(MPU_RASR_AP_Msk, AP) | \
	__val2mfld(MPU_RASR_TEX_Msk, TEX) | \
	__val2mfld(MPU_RASR_S_Msk, S) | \
	__val2mfld(MPU_RASR_C_Msk, C) | \
	__val2mfld(MPU_RASR_B_Msk, B) | \
	__val2mfld(MPU_RASR_ENA_Msk, ENA))

/*                                                 XN AP TEX  S  C  B ENA */
#define HAL_MPU_ATTR_STD_RX    HAL_MPU_ATTR_COMBINE(0, 6,  0, 1, 1, 0, 1)
#define HAL_MPU_ATTR_STD_FLASH HAL_MPU_ATTR_COMBINE(0, 3,  0, 0, 1, 0, 1)
#define HAL_MPU_ATTR_STD_RWX   HAL_MPU_ATTR_COMBINE(0, 3,  0, 1, 1, 1, 1)
#define HAL_MPU_ATTR_STD_IO    HAL_MPU_ATTR_COMBINE(1, 3,  0, 1, 0, 0, 1)

static inline int hal_mpu_region_count(void)
{
  return __mfld2val(MPU_TYPE_DREGION_Msk, MPU->TYPE);
}

void hal_mpu_clear(void);
void hal_mpu_set_region(int rnr, uint32_t addr, uint32_t size, uint32_t attrib);
int hal_mpu_cover_region(uint32_t addr, uint32_t size, uint32_t attrib);
int hal_mpu_find_empty_region(void);


#endif /*_HAL_MPU_H_*/