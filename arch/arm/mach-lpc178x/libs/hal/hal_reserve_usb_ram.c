#include <stdint.h>
#include <string.h>
#include <hal_machperiph.h>

extern char _eusbram;

void *lpc_reserve_usb_ram(unsigned long size)
{
  static char *usb_ram_avail=&_eusbram;
  char *prev_avail;

  /*printf("usb reserve %lu at 0x%lx\n",size,(unsigned long)usb_ram_avail);*/

  if(!usb_ram_avail)
    return NULL;

  prev_avail=(char *)(((uintptr_t)usb_ram_avail+3)&~3);
  usb_ram_avail=prev_avail+size;

  return prev_avail;
}