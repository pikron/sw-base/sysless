#!/bin/sh

#pdftotext -nopgbrk user.manual.lpc17xx.pdf lpc17xx-um.txt

# 10.1.1 USB Clock Control register (USBClkCtrl - 0x5000 CFF4)
# #define USBDevIntSt_o	0x0000	/* USB Device Interrupt Status (RO) */
# #define USBDevIntSt	(*(REG32*)(USB_REGS_BASE+USBDevIntSt_o))

cat lpc17xx-um.txt | \
  sed -n -e 's/^[0-9.]* \+\([^(]*\) \+(\([^ ]*\) - 0x5000 C\([^)]*\)).*$/\3@#define \2_o\t0x0\3\t\/* \1 *\/\nx\3@#define \2\t(*(REG32*)(USB_REGS_BASE+\2_o))/p' | \
  sort -u | sed -e 's/^.*@\(.*\)$/\1/'

