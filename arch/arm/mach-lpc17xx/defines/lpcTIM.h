/******************************************************************************
 *
 * Header file for Philips LPC178x TIM[0,1,2,3] enabled ARM Processors 
 * Copyright 2013 Pavel Pisa <pisa@cmp.felk.cvut.cz>
 *
 * No guarantees, warrantees, or promises, implied or otherwise.
 * May be used for hobby or commercial purposes provided copyright
 * notice remains intact or GPL license is applied.
 *
 *****************************************************************************/

#ifndef _lpcTIM_H
#define _lpcTIM_H

#define LPC_TIM_IR_o	0x000	/* Interrupt Register (R/W) */
#define   LPC_TIM_IR_MR0INT_m	(1<<0)	/* Interrupt flag for match channel 0. */
#define   LPC_TIM_IR_MR1INT_m	(1<<1)	/* Interrupt flag for match channel 1. */
#define   LPC_TIM_IR_MR2INT_m	(1<<2)	/* Interrupt flag for match channel 2. */
#define   LPC_TIM_IR_MR3INT_m	(1<<3)	/* Interrupt flag for match channel 3. */
#define   LPC_TIM_IR_CR0INT_m	(1<<4)	/* Interrupt flag for capture channel 0 event. */
#define   LPC_TIM_IR_CR1INT_m	(1<<5)	/* Interrupt flag for capture channel 1 event. */
#define   LPC_TIM_IR_ALL_m	0x3f
#define LPC_TIM_TCR_o	0x004	/* Timer Control Register (R/W) */
#define   LPC_TIM_TCR_CEN_m	(1<<0)	/* Enable timer */
#define   LPC_TIM_TCR_CRST_m	(1<<1)	/* Synchronous reset on next positive PCLK */
#define LPC_TIM_TC_o	0x008	/* Timer Counter Register (R/W) */
#define LPC_TIM_PR_o	0x00C	/* Prescale Register (R/W) */
#define LPC_TIM_PC_o	0x010	/* Prescale Counter Register (R/W) */
#define LPC_TIM_MCR_o	0x014	/* Match Control Register (R/W) */
#define   LPC_TIM_MCR_MR0I_m	(1<<0)	/* Interrupt on MR0 */
#define   LPC_TIM_MCR_MR0R_m	(1<<1)	/* Reset on MR0 */
#define   LPC_TIM_MCR_MR0S_m	(1<<2)	/* Stop on MR0 */
#define   LPC_TIM_MCR_MR1I_m	(1<<3)	/* Interrupt on MR1 */
#define   LPC_TIM_MCR_MR1R_m	(1<<4)	/* Reset on MR1 */
#define   LPC_TIM_MCR_MR1S_m	(1<<5)	/* Stop on MR1 */
#define   LPC_TIM_MCR_MR2I_m	(1<<6)	/* Interrupt on MR2 */
#define   LPC_TIM_MCR_MR2R_m	(1<<7)	/* Reset on MR2 */
#define   LPC_TIM_MCR_MR2S_m	(1<<8)	/* Stop on MR2. */
#define   LPC_TIM_MCR_MR3I_m	(1<<9)	/* Interrupt on MR3 */
#define   LPC_TIM_MCR_MR3R_m	(1<<10)	/* Reset on MR3 */
#define   LPC_TIM_MCR_MR3S_m	(1<<11)	/* Stop on MR3 */
#define LPC_TIM_MR0_o	0x018	/* Match Register 0 (R/W) */
#define LPC_TIM_MR1_o	0x01C	/* Match Register 1 (R/W) */
#define LPC_TIM_MR2_o	0x020	/* Match Register 2 (R/W) */
#define LPC_TIM_MR3_o	0x024	/* Match Register 3 (R/W) */
#define LPC_TIM_CCR_o	0x028	/* Capture Control Register (R/W) */
#define   LPC_TIM_CCR_CAP0RE_m	(1<<0)	/* Capture on CAPn.0 rising edge */
#define   LPC_TIM_CCR_CAP0FE_m	(1<<1)	/* Capture on CAPn.0 falling edge */
#define   LPC_TIM_CCR_CAP0I_m	(1<<2)	/* Interrupt on CAPn.0 event */
#define   LPC_TIM_CCR_CAP1RE_m	(1<<3)	/* Capture on CAPn.1 rising edge */
#define   LPC_TIM_CCR_CAP1FE_m	(1<<4)	/* Capture on CAPn.1 falling edge */
#define   LPC_TIM_CCR_CAP1I_m	(1<<5)	/* Interrupt on CAPn.1 event */
#define LPC_TIM_CR0_o	0x02C	/* Capture Register 0 (R/ ) */
#define LPC_TIM_CR1_o	0x030	/* Capture Register 1 (R/ ) */
#define LPC_TIM_EMR_o	0x03C	/* External Match Register (R/W) */
#define   LPC_TIM_EMR_EM0_m	(1<<0)	/* External Match 0. When a match occurs between the TC and */
#define   LPC_TIM_EMR_EM1_m	(1<<1)	/* External Match 1. When a match occurs between the TC and */
#define   LPC_TIM_EMR_EM2_m	(1<<2)	/* External Match 2. When a match occurs between the TC and */
#define   LPC_TIM_EMR_EM3_m	(1<<3)	/* External Match 3. When a match occurs between the TC and */
#define   LPC_TIM_EMR_EMC0_m	(3<<4)	/* External Match Control 0 functionality */
#define   LPC_TIM_EMR_EMC1_m	(3<<6)	/* External Match Control 1 functionality */
#define   LPC_TIM_EMR_EMC2_m	(3<<8)	/* External Match Control 2 functionality */
#define   LPC_TIM_EMR_EMC3_m	(3<<10)	/* External Match Control 3 functionality */
#define   LPC_TIM_EMR_NOP	0
#define   LPC_TIM_EMR_SET	1
#define   LPC_TIM_EMR_CLEAR	2
#define   LPC_TIM_EMR_TOGLE	3
#define LPC_TIM_CTCR_o	0x070	/* Count Control Register (R/W) */
#define   LPC_TIM_CTCR_CTMODE_m	(3<<0)	/* Counter (0)/Timer Mode (1 rising, 2 falling, 3 both edges */
#define   LPC_TIM_CTCR_CINSEL_m	(3<<2)	/* Count Input Select (0 CAPn0, 1 CAPn1) */

#endif /*_lpcTIM_H*/
