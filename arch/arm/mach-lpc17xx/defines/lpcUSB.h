/******************************************************************************
 *
 * $RCSfile$
 * $Revision$
 *
 * Header file for Philips LPC17xx USB enabled ARM Processors 
 * Copyright 2006-2009 Pavel Pisa <pisa@cmp.felk.cvut.cz>
 * Based on same author code for LPC214x
 *
 * No guarantees, warrantees, or promises, implied or otherwise.
 * May be used for hobby or commercial purposes provided copyright
 * notice remains intact or GPL license is applied.
 *
 *****************************************************************************/

#ifndef _lpcUSB_H
#define _lpcUSB_H

#if !defined(REG_8) || !defined(REG16) || !defined(REG32)
#define REG_8           volatile unsigned char
#define REG16           volatile unsigned short
#define REG32           volatile unsigned long
#endif

/* USBIntSt - USB Interrupt Status (R/W) */
#define   USB_INT_REQ_LP	(1<<0) /*Low priority interrupt line status (RO) */
#define   USB_INT_REQ_HP	(1<<1) /*High priority interrupt line status. (RO) */
#define   USB_INT_REQ_DMA	(1<<2) /*DMA interrupt line status. This bit is read only. (LPC2146/8 only) */
#define   USB_need_clock	(1<<8) /*USB need clock indicator*/
#define   USB_EN_USB_INTS	(1<<31) /*Enable all USB interrupts*/

/* USB Host Controller Registers */
#define HcControlHeadED_o	0x0020	/* PADDR of 1st EP desc. of control list (R/W) */
#define HcControlCurrentED_o	0x0024	/* PADDR of curr. EP desc. of control list (R/W) */
#define HcBulkHeadED_o		0x0028	/* PADDR of 1st EP desc. of bulk list (R/W) */
#define HcBulkCurrentED_o	0x002C	/* PADDR of curr. EP desc. of bulk list (R/W) */
#define HcDoneHead_o		0x0030	/* PADDR of the last desct added to Done q. (R) */
#define HcFmInterval_o		0x0034	/* full speed max frame time (R/W) */
#define HcFmRemaining_o		0x0038	/* 14-bit counter of remaining in curr. frame (R) */
#define HcFmNumber_o		0x003C	/* 16-bit HC and HDC timing counter (R) */
#define HcPeriodicStart_o	0x0040	/* 14-bit earliest time to start HC periodic list (R/W) */
#define HcLSThreshold_o		0x0044	/* 11-bit HC timer for 8-byte LS packet before EOF (R/W) */
#define HcRhDescriptorA_o	0x0048	/* Characteristics of the root hub - 1st part (R/W) */
#define HcRhDescriptorB_o	0x004C	/* Characteristics of the root hub - 2nd part (R/W) */
#define HcRhStatus_o		0x0050	/* D-word hub info (low - status, high change) (R/W) */
#define USB_MODULE_ID_o		0x00FC	/* USB Module ID 0x3505yyzz - yy .. ver, zz .. rev (R) */

/* USB On-The-Go Registers */
#define OTGIntSt_o	0x0100	/* OTG Interrupt Status (RO) */
#define OTGIntEn_o	0x0104	/* OTG Interrupt Enable (R/W) */
#define OTGIntSet_o	0x0108	/* OTG Interrupt Clear (WO) */
#define OTGIntClr_o	0x010C	/* OTG Interrupt Clear (WO) */
#define OTGStCtrl_o	0x0110	/* OTG Status and Control (R/W) */
#define OTGTmr_o	0x0114	/* OTG Timer (R/W) */

/* Device interrupt registers */
#define USBDevIntSt_o	0x0200	/* USB Device Interrupt Status(R0) */
#define USBDevIntEn_o	0x0204	/* USB Device Interrupt Enable (R/W) */
#define USBDevIntClr_o	0x0208	/* USB Device Interrupt Clear (WO) */
#define USBDevIntSet_o	0x020C	/* USB Device Interrupt Set (WO) */
#define   USBDevInt_FRAME	(1<<0) /*Frame interrupt @1kHz for ISO transfers*/
#define   USBDevInt_EP_FAST	(1<<1) /*Fast interrupt transfer for the endpoint*/
#define   USBDevInt_EP_SLOW	(1<<2) /*Slow interrupt transfer for the endpoint*/
#define   USBDevInt_DEV_STAT	(1<<3) /*USB Bus reset, USB suspend change or Connect occured*/
#define   USBDevInt_CCEMTY	(1<<4) /*Command code register is empty/ready for CMD*/
#define   USBDevInt_CDFULL	(1<<5) /*Command data register is full/data available*/
#define   USBDevInt_RxENDPKT	(1<<6) /*Current packet in the FIFO is transferred to the CPU*/
#define   USBDevInt_TxENDPKT	(1<<7) /*TxPacket bytes written to FIFO*/
#define   USBDevInt_EP_RLZED	(1<<8) /*Endpoints realized after Maxpacket size update*/
#define   USBDevInt_ERR_INT	(1<<9) /*Error Interrupt - Use Read Error Status Command 0xFB*/
#define USBDevIntPri_o	0x022C	/* USB Device Interrupt Priority (WO) */
#define   USBDevIntPri_FRAME	(1<<0) /*0/1 FRAME int routed to the low/high priority interrupt line*/
#define   USBDevIntPri_EP_FAST	(1<<1) /*0/1 EP_FAST int routed to the low/high priority line*/

/* Endpoint interrupt registers - bits corresponds to  EP0 to EP31 */
#define USBEpIntSt_o	0x0230	/* USB Endpoint Interrupt Status (R0) */
#define USBEpIntEn_o	0x0234	/* USB Endpoint Interrupt Enable (R/W) */
#define USBEpIntClr_o	0x0238	/* USB Endpoint Interrupt Clear (WO) */
#define USBEpIntSet_o	0x023C	/* USB Endpoint Interrupt Set (WO) */
#define USBEpIntPri_o	0x0240	/* USB Endpoint Interrupt Priority (WO) */
/* Endpoint realization registers */
#define USBReEp_o	0x0244	/* USB Realize Endpoint (R/W) */
#define USBEpInd_o	0x0248	/* USB Endpoint Index (RO) */
#define   USBEpInd_Ind		0x001F	/* Index for subsequent USBMaxPSize (WO) */
#define USBMaxPSize_o	0x024C	/* USB MaxPacketSize (R/W) */
#define   USBMaxPSize_Size	0x03FF	/* The maximum packet size value */
/* USB transfer registers */
#define USBRxData_o	0x0218	/* USB Receive Data (RO) */
#define USBRxPLen_o	0x0220	/* USB Receive Packet Length (RO) */
#define   USBRxPLen_PKT_LNGTH	(0x03FF) /*Remaining amount of bytes to be read from RAM*/
#define   USBRxPLen_DV		(1<<10) /*Data valid. 0 only for error ISO packet*/
#define   USBRxPLen_PKT_RDY	(1<<11) /*Packet length valid and packet is ready for reading*/
#define USBTxData_o	0x021C	/* USB Transmit Data (WO) */
#define USBTxPLen_o	0x0224	/* USB Transmit Packet Length (WO) */
#define   USBTxPLen_PKT_LNGTH	(0x03FF) /*Remaining amount of bytes to be written to the EP_RAM*/
#define USBCtrl_o	0x0228	/* USB Control (R/W) */
#define   USBCtrl_RD_EN		(1<<0) /*Read mode control*/
#define   USBCtrl_WR_EN		(1<<1) /*Write mode control*/
#define   USBCtrl_LOG_ENDPOINT	0x003C /*Logical Endpoint number*/
/* Command registers */
#define USBCmdCode_o	0x0210	/* USB Command Code (WO) */
#define   USBCmdCode_CMD_PHASE	0x0000FF00 /*The command phase*/
#define   USBCmdCode_CMD_CODE	0x00FF0000 /*The code for the command*/
#define USBCmdData_o	0x0214	/* USB Command Data (RO) */
/* DMA registers (LPC2146/8 and LPC17xx only) */
#define USBDMARSt_o	0x0250	/* USB DMA Request Status (RO) */
#define USBDMARClr_o	0x0254	/* USB DMA Request Clear (WO) */
#define USBDMARSet_o	0x0258	/* USB DMA Request Set (WO) */
#define USBUDCAH_o	0x0280	/* USB UDCA Head (R/W) has to be aligned to 128 bytes */
#define USBEpDMASt_o	0x0284	/* USB Endpoint DMA Status (RO) */
#define USBEpDMAEn_o	0x0288	/* USB Endpoint DMA Enable (WO) */
#define USBEpDMADis_o	0x028C	/* USB Endpoint DMA Disable (WO) */
#define USBDMAIntSt_o	0x0290	/* USB DMA Interrupt Status (RO) */
#define USBDMAIntEn_o	0x0294	/* USB DMA Interrupt Enable (R/W) */
#define   USBDMAInt_EoT		(1<<0) /*End of Transfer Interrupt bit, 1 if USBEoTIntSt != 0*/
#define   USBDMAInt_New_DD_Rq	(1<<1) /*  New DD Request Interrupt bit, 1 if USBNDDRIntSt != 0*/
#define   USBDMAInt_SysError	(1<<2) /*System Error Interrupt bit, 1 if USBSysErrIntSt != 0*/
#define USBEoTIntSt_o	0x02A0	/* USB End of Transfer Interrupt Status (RO) */
#define USBEoTIntClr_o	0x02A4	/* USB End of Transfer Interrupt Clear (WO) */
#define USBEoTIntSet_o	0x02A8	/* USB End of Transfer Interrupt Set (WO) */
#define USBNDDRIntSt_o	0x02AC	/* USB New DD Request Interrupt Status (RO) */
#define USBNDDRIntClr_o	0x02B0	/* USB New DD Request Interrupt Clear (WO) */
#define USBNDDRIntSet_o	0x02B4	/* USB New DD Request Interrupt Set (WO) */
#define USBSysErrIntSt_o	0x02B8	/* USB System Error Interrupt Status (RO) */
#define USBSysErrIntClr_o	0x02BC	/* USB System Error Interrupt Clear (WO) */
#define USBSysErrIntSet_o	0x02C0	/* USB System Error Interrupt Set (WO) */

/* USB I2C registers */
#define USB_I2C_RX_o	0x0300	/* I2C Receive (RO) */
#define USB_I2C_TX_o	0x0300	/* I2C Transmit (WO) */
#define USB_I2C_STS_o	0x0304	/* I2C Status (RO) */
#define USB_I2C_CTL_o	0x0308	/* I2C Control (R/W) */
#define USB_I2C_CLKHI_o	0x030C	/* I2C Clock High (R/W) */
#define USB_I2C_CLKLO_o	0x0310	/* I2C Clock Low (WO) */

/* Clock control registers */
#define OTGClkCtrl_o	0x0FF4	/* OTG Clock Control (R/W) */
#define USBClkCtrl_o	0x0FF4	/* USB Clock Control (R/W) */
#define OTGClkSt_o	0x0FF8	/* OTG Clock Status (RO) */
#define USBClkSt_o	0x0FF8	/* USB Clock Status (RO) */

/* Command Codes */
#define USB_CMD_SET_ADDR        0x00D00500
#define USB_CMD_CFG_DEV         0x00D80500
#define USB_CMD_SET_MODE        0x00F30500
#define USB_CMD_RD_FRAME        0x00F50500
#define USB_DAT_RD_FRAME        0x00F50200
#define USB_CMD_RD_TEST         0x00FD0500
#define USB_DAT_RD_TEST         0x00FD0200
#define USB_CMD_SET_DEV_STAT    0x00FE0500
#define USB_CMD_GET_DEV_STAT    0x00FE0500
#define USB_DAT_GET_DEV_STAT    0x00FE0200
#define USB_CMD_GET_ERR_CODE    0x00FF0500
#define USB_DAT_GET_ERR_CODE    0x00FF0200
#define USB_CMD_RD_ERR_STAT     0x00FB0500
#define USB_DAT_RD_ERR_STAT     0x00FB0200
#define USB_DAT_WR_BYTE(x)     (0x00000100 | ((x) << 16))
#define USB_CMD_SEL_EP(x)      (0x00000500 | ((x) << 16))
#define USB_DAT_SEL_EP(x)      (0x00000200 | ((x) << 16))
#define USB_CMD_SEL_EP_CLRI(x) (0x00400500 | ((x) << 16))
#define USB_DAT_SEL_EP_CLRI(x) (0x00400200 | ((x) << 16))
#define USB_CMD_SET_EP_STAT(x) (0x00400500 | ((x) << 16))
#define USB_CMD_CLR_BUF         0x00F20500
#define USB_DAT_CLR_BUF         0x00F20200
#define USB_CMD_VALID_BUF       0x00FA0500

/* Device Address Register Definitions */
#define USBC_DEV_ADDR_MASK       0x7F
#define USBC_DEV_EN              0x80

/* Device Configure Register Definitions */
#define USBC_CONF_DEVICE         0x01

/* Device Mode Register Definitions */
#define USBC_AP_CLK              0x01
#define USBC_INAK_CI             0x02
#define USBC_INAK_CO             0x04
#define USBC_INAK_II             0x08
#define USBC_INAK_IO             0x10
#define USBC_INAK_BI             0x20
#define USBC_INAK_BO             0x40

/* Device Status Register Definitions */
#define USBC_DEV_CON             0x01
#define USBC_DEV_CON_CH          0x02
#define USBC_DEV_SUS             0x04
#define USBC_DEV_SUS_CH          0x08
#define USBC_DEV_RST             0x10

/* Error Code Register Definitions */
#define USBC_ERR_EC_MASK         0x0F
#define USBC_ERR_EA              0x10

/* Error Status Register Definitions */
#define USBC_ERR_PID             0x01
#define USBC_ERR_UEPKT           0x02
#define USBC_ERR_DCRC            0x04
#define USBC_ERR_TIMOUT          0x08
#define USBC_ERR_EOP             0x10
#define USBC_ERR_B_OVRN          0x20
#define USBC_ERR_BTSTF           0x40
#define USBC_ERR_TGL             0x80

/* Endpoint Select Register Definitions */
#define USBC_EP_SEL_F            0x01
#define USBC_EP_SEL_ST           0x02
#define USBC_EP_SEL_STP          0x04
#define USBC_EP_SEL_PO           0x08
#define USBC_EP_SEL_EPN          0x10
#define USBC_EP_SEL_B_1_FULL     0x20
#define USBC_EP_SEL_B_2_FULL     0x40

/* Endpoint Status Register Definitions */
#define USBC_EP_STAT_ST          0x01
#define USBC_EP_STAT_DA          0x20
#define USBC_EP_STAT_RF_MO       0x40
#define USBC_EP_STAT_CND_ST      0x80

/* Clear Buffer Register Definitions */
#define USBC_CLR_BUF_PO          0x01

typedef struct
{
  REG32 _padA[0x200];
/* Device interrupt registers */
  REG32 DevIntSt;	/* USB Device Interrupt Status (RO) 0000 */
  REG32 DevIntEn;	/* USB Device Interrupt Enable (R/W) 0004 */
  REG32 DevIntClr;	/* USB Device Interrupt Clear (WO) 0008 */
  REG32 DevIntSet;	/* USB Device Interrupt Set (WO) 000C */
/* Command registers */
  REG32 CmdCode;	/* USB Command Code (WO) 0010 */
  REG32 CmdData;	/* USB Command Data (RO) 0014 */
/* USB transfer registers */
  REG32 RxData;		/* USB Receive Data (RO) 0018 */
  REG32 TxData;		/* USB Transmit Data (WO) 001C */
  REG32 RxPLen;		/* USB Receive Packet Length (RO) 0020 */
  REG32 TxPLen;		/* USB Transmit Packet Length (WO) 0024 */
  REG32 Ctrl;		/* USB Control (R/W) 0028 */
/* Device interrupt priority register */
  REG_8  USBDevIntPri;	/* USB Device Interrupt Priority (WO) 002C */
  REG_8  _pad0[3];
/* Endpoint interrupt registers */
  REG32 EpIntSt;	/* USB Endpoint Interrupt Status (RO) 0030 */
  REG32 EpIntEn;	/* USB Endpoint Interrupt Enable (R/W) 0034 */
  REG32 EpIntClr;	/* USB Endpoint Interrupt Clear (WO) 0038 */
  REG32 EpIntSet;	/* USB Endpoint Interrupt Set (WO) 003C */
  REG32 EpIntPri;	/* USB Endpoint Priority (WO) 0040 */
/* Endpoint realization registers */
  REG32 ReEp;		/* USB Realize Endpoint (R/W) 0044 */
  REG32 EpInd;		/* USB Endpoint Index (WO) 0048 */
  REG32 MaxPSize;	/* USB MaxPacketSize (R/W) 004C */
/* DMA registers (LPC2146/8 only) */
  REG32 DMARSt;		/* USB DMA Request Status (RO) 0050 */
  REG32 DMARClr;	/* USB DMA Request Clear (WO) 0054 */
  REG32 DMARSet;	/* USB DMA Request Set (WO) 0058 */
  REG32 _pad1[9];
  REG32 UDCAH;		/* USB UDCA Head (R/W) 0080 */
  REG32 EpDMASt;	/* USB Endpoint DMA Status (RO) 0084 */
  REG32 EpDMAEn;	/* USB Endpoint DMA Enable (WO) 0088 */
  REG32 EpDMADis;	/* USB Endpoint DMA Disable (WO) 008C */
  REG32 DMAIntSt;	/* USB DMA Interrupt Status (RO) 0090 */
  REG32 DMAIntEn;	/* USB DMA Interrupt Enable (R/W) 0094 */
  REG32 _pad2[2];
  REG32 EoTIntSt;	/* USB End of Transfer Interrupt Status (RO) 00A0 */
  REG32 EoTIntClr;	/* USB End of Transfer Interrupt Clear (WO) 00A4 */
  REG32 EoTIntSet;	/* USB End of Transfer Interrupt Set (WO) 00A8 */
  REG32 NDDRIntSt;	/* USB New DD Request Interrupt Status (RO) 00AC */
  REG32 NDDRIntClr;	/* USB New DD Request Interrupt Clear (WO) 00B0 */
  REG32 NDDRIntSet;	/* USB New DD Request Interrupt Set (WO) 00B4 */
  REG32 SysErrIntSt;	/* USB System Error Interrupt Status (RO) 00B8 */
  REG32 SysErrIntClr;	/* USB System Error Interrupt Clear (WO) 00BC */
  REG32 SysErrIntSet;	/* USB System Error Interrupt Set (WO) 00C0 */
} usbRegs_t;

#define USBIntSt (*(REG32*)0x400FC10) /* USB Interrupt Status (R/W) */

#define USB_REGS_BASE	0x5000C000UL

#define HcControlHeadED_o	0x0020	/* PADDR of 1st EP desc. of control list (R/W) */
#define HcControlCurrentED_o	0x0024	/* PADDR of curr. EP desc. of control list (R/W) */
#define HcBulkHeadED_o		0x0028	/* PADDR of 1st EP desc. of bulk list (R/W) */
#define HcBulkCurrentED_o	0x002C	/* PADDR of curr. EP desc. of bulk list (R/W) */
#define HcDoneHead_o		0x0030	/* PADDR of the last desct added to Done q. (R) */
#define HcFmInterval_o		0x0034	/* full speed max frame time (R/W) */
#define HcFmRemaining_o		0x0038	/* 14-bit counter of remaining in curr. frame (R) */
#define HcFmNumber_o		0x003C	/* 16-bit HC and HDC timing counter (R) */
#define HcPeriodicStart_o	0x0040	/* 14-bit earliest time to start HC periodic list (R/W) */
#define HcLSThreshold_o		0x0044	/* 11-bit HC timer for 8-byte LS packet before EOF (R/W) */
#define HcRhDescriptorA_o	0x0048	/* Characteristics of the root hub - 1st part (R/W) */
#define HcRhDescriptorB_o	0x004C	/* Characteristics of the root hub - 2nd part (R/W) */
#define HcRhStatus_o		0x0050	/* D-word hub info (low - status, high change) (R/W) */

#define HcControlHeadED	(*(REG32*)(USB_REGS_BASE+HcControlHeadED_o))
#define HcControlCurrentED	(*(REG32*)(USB_REGS_BASE+HcControlCurrentED_o))
#define HcBulkHeadED	(*(REG32*)(USB_REGS_BASE+HcBulkHeadED_o))
#define HcBulkCurrentED	(*(REG32*)(USB_REGS_BASE+HcBulkCurrentED_o))
#define HcDoneHead	(*(REG32*)(USB_REGS_BASE+HcDoneHead_o))
#define HcFmInterval	(*(REG32*)(USB_REGS_BASE+HcFmInterval_o))
#define HcFmRemaining	(*(REG32*)(USB_REGS_BASE+HcFmRemaining_o))
#define HcFmNumber	(*(REG32*)(USB_REGS_BASE+HcFmNumber_o))
#define HcPeriodicStart	(*(REG32*)(USB_REGS_BASE+HcPeriodicStart_o))
#define HcLSThreshold	(*(REG32*)(USB_REGS_BASE+HcLSThreshold_o))
#define HcRhDescriptorA	(*(REG32*)(USB_REGS_BASE+HcRhDescriptorA_o))
#define HcRhDescriptorB	(*(REG32*)(USB_REGS_BASE+HcRhDescriptorB_o))
#define HcRhStatus	(*(REG32*)(USB_REGS_BASE+HcRhStatus_o))

#define USB_MODULE_ID	(*(REG32*)(USB_REGS_BASE+USB_MODULE_ID_o))

#define OTGIntSt	(*(REG32*)(USB_REGS_BASE+OTGIntSt_o))
#define OTGIntEn	(*(REG32*)(USB_REGS_BASE+OTGIntEn_o))
#define OTGIntClr	(*(REG32*)(USB_REGS_BASE+OTGIntClr_o))
#define OTGIntSet	(*(REG32*)(USB_REGS_BASE+OTGIntSet_o))
#define OTGStCtrl	(*(REG32*)(USB_REGS_BASE+OTGStCtrl_o))
#define OTGTmr		(*(REG32*)(USB_REGS_BASE+OTGTmr_o))

#define USBDevIntSt	(*(REG32*)(USB_REGS_BASE+USBDevIntSt_o))
#define USBDevIntEn	(*(REG32*)(USB_REGS_BASE+USBDevIntEn_o))
#define USBDevIntClr	(*(REG32*)(USB_REGS_BASE+USBDevIntClr_o))
#define USBDevIntSet	(*(REG32*)(USB_REGS_BASE+USBDevIntSet_o))
#define USBDevIntPri	(*(REG32*)(USB_REGS_BASE+USBDevIntPri_o))
#define USBEpIntSt	(*(REG32*)(USB_REGS_BASE+USBEpIntSt_o))
#define USBEpIntEn	(*(REG32*)(USB_REGS_BASE+USBEpIntEn_o))
#define USBEpIntClr	(*(REG32*)(USB_REGS_BASE+USBEpIntClr_o))
#define USBEpIntSet	(*(REG32*)(USB_REGS_BASE+USBEpIntSet_o))
#define USBEpIntPri	(*(REG32*)(USB_REGS_BASE+USBEpIntPri_o))
#define USBReEp		(*(REG32*)(USB_REGS_BASE+USBReEp_o))
#define USBEpInd	(*(REG32*)(USB_REGS_BASE+USBEpInd_o))
#define USBMaxPSize	(*(REG32*)(USB_REGS_BASE+USBMaxPSize_o))
#define USBRxData	(*(REG32*)(USB_REGS_BASE+USBRxData_o))
#define USBRxPLen	(*(REG32*)(USB_REGS_BASE+USBRxPLen_o))
#define USBTxData	(*(REG32*)(USB_REGS_BASE+USBTxData_o))
#define USBTxPLen	(*(REG32*)(USB_REGS_BASE+USBTxPLen_o))
#define USBCtrl		(*(REG32*)(USB_REGS_BASE+USBCtrl_o))
#define USBCmdCode	(*(REG32*)(USB_REGS_BASE+USBCmdCode_o))
#define USBCmdData	(*(REG32*)(USB_REGS_BASE+USBCmdData_o))
#define USBDMARSt	(*(REG32*)(USB_REGS_BASE+USBDMARSt_o))
#define USBDMARClr	(*(REG32*)(USB_REGS_BASE+USBDMARClr_o))
#define USBDMARSet	(*(REG32*)(USB_REGS_BASE+USBDMARSet_o))
#define USBUDCAH	(*(REG32*)(USB_REGS_BASE+USBUDCAH_o))
#define USBEpDMASt	(*(REG32*)(USB_REGS_BASE+USBEpDMASt_o))
#define USBEpDMAEn	(*(REG32*)(USB_REGS_BASE+USBEpDMAEn_o))
#define USBEpDMADis	(*(REG32*)(USB_REGS_BASE+USBEpDMADis_o))
#define USBDMAIntSt	(*(REG32*)(USB_REGS_BASE+USBDMAIntSt_o))
#define USBDMAIntEn	(*(REG32*)(USB_REGS_BASE+USBDMAIntEn_o))
#define USBEoTIntSt	(*(REG32*)(USB_REGS_BASE+USBEoTIntSt_o))
#define USBEoTIntClr	(*(REG32*)(USB_REGS_BASE+USBEoTIntClr_o))
#define USBEoTIntSet	(*(REG32*)(USB_REGS_BASE+USBEoTIntSet_o))
#define USBNDDRIntSt	(*(REG32*)(USB_REGS_BASE+USBNDDRIntSt_o))
#define USBNDDRIntClr	(*(REG32*)(USB_REGS_BASE+USBNDDRIntClr_o))
#define USBNDDRIntSet	(*(REG32*)(USB_REGS_BASE+USBNDDRIntSet_o))
#define USBSysErrIntSt	(*(REG32*)(USB_REGS_BASE+USBSysErrIntSt_o))
#define USBSysErrIntClr	(*(REG32*)(USB_REGS_BASE+USBSysErrIntClr_o))
#define USBSysErrIntSet	(*(REG32*)(USB_REGS_BASE+USBSysErrIntSet_o))

#define USB_I2C_RX	(*(REG32*)(USB_REGS_BASE+I2C_RX_o))
#define USB_I2C_TX	(*(REG32*)(USB_REGS_BASE+I2C_TX_o))
#define USB_I2C_STS	(*(REG32*)(USB_REGS_BASE+I2C_STS_o))
#define USB_I2C_CTL	(*(REG32*)(USB_REGS_BASE+I2C_CTL_o))
#define USB_I2C_CLKHI	(*(REG32*)(USB_REGS_BASE+I2C_CLKHI_o))
#define USB_I2C_CLKLO	(*(REG32*)(USB_REGS_BASE+I2C_CLKLO_o))
#define OTGClkCtrl	(*(REG32*)(USB_REGS_BASE+OTGClkCtrl_o))
#define USBClkCtrl	(*(REG32*)(USB_REGS_BASE+USBClkCtrl_o))
#define OTGClkSt	(*(REG32*)(USB_REGS_BASE+OTGClkSt_o))
#define USBClkSt	(*(REG32*)(USB_REGS_BASE+USBClkSt_o))

#endif /*_lpcUSB_H*/
