#include <cpu_def.h>
#include <LPC17xx.h>

#include <hal_gpio.h>

int hal_pin_conf_fnc(unsigned gpio, int fnc)
{
  __IO uint32_t *p = &(PINCON->PINSEL0); 
  uint32_t mask;

  if(fnc & PORT_CONF_FNC_MASK)
    fnc = __mfld2val(PORT_CONF_FNC_MASK, fnc);

  p += hal_gpio_get_port_num(gpio)*2;
  if(gpio & 0x10)
    p++;
  gpio &= 0x0f;
  gpio *= 2;
  mask = 3 << gpio;

  *p = (*p & ~mask) | ((fnc << gpio) & mask);

  return 0;
}

int hal_pin_conf_mode(unsigned gpio, int mode)
{
  __IO uint32_t *p = &(PINCON->PINMODE0); 
  uint32_t mask;

  if(mode & PORT_CONF_MODE_MASK)
    mode = __mfld2val(PORT_CONF_MODE_MASK, mode);

  p += hal_gpio_get_port_num(gpio)*2;
  if(gpio & 0x10)
    p++;
  gpio &= 0x0f;
  gpio *= 2;
  mask = 3 << gpio;

  *p = (*p & ~mask) | ((mode << gpio) & mask);

  return 0;
}

int hal_pin_conf_od(unsigned gpio, int od)
{
  uint32_t mask = 1 << (gpio & 0x1f);

  if(od)
    (&(PINCON->PINMODE_OD0))[hal_gpio_get_port_num(gpio)] |= mask;
  else
    (&(PINCON->PINMODE_OD0))[hal_gpio_get_port_num(gpio)] &= ~mask;

  return 0;
}

int hal_pin_conf_set(unsigned gpio, int conf)
{
  gpio &= ~PORT_CONF_MASK;
  hal_pin_conf_mode(gpio, conf & PORT_CONF_MODE_MASK);
  hal_pin_conf_od(gpio, conf & PORT_CONF_OD_MASK);
  if(conf & PORT_CONF_SET_DIR) {
    if((conf & PORT_CONF_DIR_MASK) == (PORT_CONF_DIR_IN & PORT_CONF_DIR_MASK))
      hal_gpio_direction_input(gpio);
    else
      hal_gpio_direction_output(gpio, conf & PORT_CONF_INIT_HIGH);
  }
  hal_pin_conf_fnc(gpio, conf & PORT_CONF_FNC_MASK);

  return 0;
}
