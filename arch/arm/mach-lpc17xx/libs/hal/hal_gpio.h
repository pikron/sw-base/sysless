#ifndef _HAL_GPIO_H_
#define _HAL_GPIO_H_

#include <cpu_def.h>
#include <LPC17xx.h>
#include <hal_gpio_def.h>

#define HAL_GPIO_PORT_BITS 3

static inline
GPIO_TypeDef *hal_gpio_get_port_base(unsigned port)
{
  char *p = (char*)GPIO0_BASE;
  p += ((char*)GPIO1_BASE - (char*)GPIO0_BASE) * port;
  return (GPIO_TypeDef *)p;
}

static inline
unsigned hal_gpio_get_port_num(unsigned gpio)
{
  gpio >>= PORT_SHIFT;
  return gpio & ((1 << HAL_GPIO_PORT_BITS) - 1);
}

static inline
GPIO_TypeDef *hal_gpio_get_base(unsigned gpio)
{
  return hal_gpio_get_port_base(hal_gpio_get_port_num(gpio));
}

static inline
int hal_gpio_get_value(unsigned gpio)
{
  return ((hal_gpio_get_base(gpio)->FIOPIN) >> (gpio & 0x1f)) & 1;
}

static inline
void hal_gpio_set_value(unsigned gpio, int value)
{
  if(value)
    hal_gpio_get_base(gpio)->FIOSET = 1 << (gpio & 0x1f);
  else
    hal_gpio_get_base(gpio)->FIOCLR = 1 << (gpio & 0x1f);
}

static inline
int hal_gpio_direction_input(unsigned gpio)
{
  hal_gpio_get_base(gpio)->FIODIR &= ~(1 << (gpio & 0x1f));
  return 0;
}

static inline
int hal_gpio_direction_output(unsigned gpio, int value)
{
  hal_gpio_set_value(gpio, value);
  hal_gpio_get_base(gpio)->FIODIR |= (1 << (gpio & 0x1f));
  return 0;
}

int hal_pin_conf_fnc(unsigned gpio, int fnc);

int hal_pin_conf_mode(unsigned gpio, int mode);

int hal_pin_conf_od(unsigned gpio, int od);

int hal_pin_conf_set(unsigned gpio, int conf);

static inline
int hal_pin_conf(unsigned gpio)
{
  return hal_pin_conf_set(gpio, gpio);
}

#endif /*_HAL_GPIO_H_*/
