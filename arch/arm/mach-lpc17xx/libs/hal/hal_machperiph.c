
#include <system_def.h>
#include <cpu_def.h>
#include <hal_machperiph.h>


/*----------------------------------------------------------------------------
  Check the register settings
 *----------------------------------------------------------------------------*/
#define CHECK_RANGE(val, min, max)                ((val < min) || (val > max))
#define CHECK_RSVD(val, mask)                     (val & mask)

/* Clock Configuration -------------------------------------------------------*/
#if (CHECK_RSVD((SCS_Val),       ~0x00000030))
   #error "SCS: Invalid values of reserved bits!"
#endif

#if (CHECK_RANGE((CLKSRCSEL_Val), 0, 2))
   #error "CLKSRCSEL: Value out of range!"
#endif

#if (CHECK_RSVD((PLL0CFG_Val),   ~0x00FF7FFF))
   #error "PLL0CFG: Invalid values of reserved bits!"
#endif

#if (CHECK_RSVD((PLL1CFG_Val),   ~0x0000007F))
   #error "PLL1CFG: Invalid values of reserved bits!"
#endif

#if ((CCLKCFG_Val != 0) && (((CCLKCFG_Val - 1) % 2)))
   #error "CCLKCFG: CCLKSEL field does not contain only odd values or 0!"
#endif

#if (CHECK_RSVD((USBCLKCFG_Val), ~0x0000000F))
   #error "USBCLKCFG: Invalid values of reserved bits!"
#endif

#if (CHECK_RSVD((PCLKSEL0_Val),   0x000C0C00))
   #error "PCLKSEL0: Invalid values of reserved bits!"
#endif

#if (CHECK_RSVD((PCLKSEL1_Val),   0x03000300))
   #error "PCLKSEL1: Invalid values of reserved bits!"
#endif

#if (CHECK_RSVD((PCONP_Val),      0x10100821))
   #error "PCONP: Invalid values of reserved bits!"
#endif

/* Flash Accelerator Configuration -------------------------------------------*/
#if (CHECK_RSVD((FLASHCFG_Val), ~0x0000F07F))
   #error "FLASHCFG: Invalid values of reserved bits!"
#endif

/*----------------------------------------------------------------------------
  Clock Variable definitions
 *----------------------------------------------------------------------------*/
unsigned int system_frequency = IRC_OSC; /*!< System Clock Frequency (Core Clock)  */

void system_clock_init(void)
{
#if (CLOCK_SETUP)                       /* Clock Setup                        */
  SC->SCS       = SCS_Val;
  if (SCS_Val & (1 << 5)) {             /* If Main Oscillator is enabled      */
    while ((SC->SCS & (1 << 6)) == 0);  /* Wait for Oscillator to be ready    */
  }

#if (PLL0_SETUP)
  SC->CLKSRCSEL = CLKSRCSEL_Val;        /* Select Clock Source for PLL0       */
  SC->PLL0CFG   = PLL0CFG_Val;
  SC->PLL0CON   = 0x01;                 /* PLL0 Enable                        */
  SC->PLL0FEED  = 0xAA;
  SC->PLL0FEED  = 0x55;
  while (!(SC->PLL0STAT & (1 << 26)));  /* Wait for PLOCK0                    */
  SC->CCLKCFG   = CCLKCFG_Val;          /* Setup Clock Divider                */

  SC->PLL0CON   = 0x03;                 /* PLL0 Enable & Connect              */
  SC->PLL0FEED  = 0xAA;
  SC->PLL0FEED  = 0x55;
#endif

#if (PLL1_SETUP)
  SC->PLL1CFG   = PLL1CFG_Val;
  SC->PLL1CON   = 0x01;                 /* PLL1 Enable                        */
  SC->PLL1FEED  = 0xAA;
  SC->PLL1FEED  = 0x55;
  while (!(SC->PLL1STAT & (1 << 10)));  /* Wait for PLOCK1                    */

  SC->PLL1CON   = 0x03;                 /* PLL1 Enable & Connect              */
  SC->PLL1FEED  = 0xAA;
  SC->PLL1FEED  = 0x55;
#endif

#ifdef USBCLKCFG_Val
#if USBCLKCFG_Val != 0
  SC->USBCLKCFG = USBCLKCFG_Val;
#endif
#endif

#endif

  /* Determine clock frequency according to clock register values             */
  if (((SC->PLL0STAT >> 24) & 3) == 3) {/* If PLL0 enabled and connected      */
    switch (SC->CLKSRCSEL & 0x03) {
      case 0:                           /* Internal RC oscillator => PLL0     */
      case 3:                           /* Reserved, default to Internal RC   */
        system_frequency = (IRC_OSC *
                          (((2 * ((SC->PLL0STAT & 0x7FFF) + 1))) /
                          (((SC->PLL0STAT >> 16) & 0xFF) + 1))   /
                          ((SC->CCLKCFG & 0xFF)+ 1));
        break;
      case 1:                           /* Main oscillator => PLL0            */
        system_frequency = (OSC_CLK *
                          (((2 * ((SC->PLL0STAT & 0x7FFF) + 1))) /
                          (((SC->PLL0STAT >> 16) & 0xFF) + 1))   /
                          ((SC->CCLKCFG & 0xFF)+ 1));
        break;
      case 2:                           /* RTC oscillator => PLL0             */
        system_frequency = (RTC_CLK *
                          (((2 * ((SC->PLL0STAT & 0x7FFF) + 1))) /
                          (((SC->PLL0STAT >> 16) & 0xFF) + 1))   /
                          ((SC->CCLKCFG & 0xFF)+ 1));
        break;
    }
  } else {
    switch (SC->CLKSRCSEL & 0x03) {
      case 0:                           /* Internal RC oscillator => PLL0     */
      case 3:                           /* Reserved, default to Internal RC   */
        system_frequency = IRC_OSC / ((SC->CCLKCFG & 0xFF)+ 1);
        break;
      case 1:                           /* Main oscillator => PLL0            */
        system_frequency = OSC_CLK / ((SC->CCLKCFG & 0xFF)+ 1);
        break;
      case 2:                           /* RTC oscillator => PLL0             */
        system_frequency = RTC_CLK / ((SC->CCLKCFG & 0xFF)+ 1);
        break;
    }
  }

#if (FLASH_SETUP == 1)                  /* Flash Accelerator Setup            */
  SC->FLASHCFG  = FLASHCFG_Val;
#endif
}

void lpc_watchdog_feed()
{
  unsigned long flags;

  save_and_cli(flags);
  WDT->WDFEED = 0xAA;
  WDT->WDFEED = 0x55;
  restore_flags(flags);
}

void lpc_watchdog_init(int on,int timeout_ms)
{
  if (!on) return;
  WDT->WDCLKSEL = 1;
  WDT->WDTC = (PCLK/4)/(1000/timeout_ms);
  WDT->WDMOD = 0x03;				   /* Enable watchdog timer and reset */
}
