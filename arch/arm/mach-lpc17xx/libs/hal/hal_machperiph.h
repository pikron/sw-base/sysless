#ifndef _HAL_MACHPERIPH_H
#define _HAL_MACHPERIPH_H

extern unsigned int system_frequency;
#define PCLK ((system_frequency+2)/4)

void system_clock_init(void);

void lpc_watchdog_init(int on,int timeout_ms);
void lpc_watchdog_feed();
void *lpc_reserve_usb_ram(unsigned long size);

#endif /* _HAL_MACHPERIPH_H */

