#ifndef INC_LPC_ADC_214x_H
#define INC_LPC_ADC_214x_H

// A/D Converter Registers
typedef struct
{
  REG32 cr;                     // Control Register
  REG32 gdr;			// Global Data Register
  REG32 gsr;			// Global Start Register
  REG32 inten;			// Interrupt Enable Register
  REG32 dr0;			// Channel 0 Data Register
  REG32 dr1;			// Channel 1 Data Register
  REG32 dr2;			// Channel 2 Data Register
  REG32 dr3;			// Channel 3 Data Register
  REG32 dr4;			// Channel 4 Data Register
  REG32 dr5;			// Channel 5 Data Register
  REG32 dr6;			// Channel 6 Data Register
  REG32 dr7;			// Channel 7 Data Register
  REG32 stat;			// Status Register
} adc214xRegs_t;

#define	ADCR_SEL 		0x000000FF
#define	ADCR_CLKDIV 		0x0000FF00
#define	ADCR_BURST   		0x00010000
#define	ADCR_CLKS 		0x000E0000
#define	ADCR_PDN 		0x00200000
#define	ADCR_START 		0x07000000
#define	ADCR_EDGE 		0x08000000

#define	ADGDR_RESULT 		0x0000FFC0
#define	ADGDR_CHN 		0x07000000
#define	ADGDR_OVERRUN	 	0x40000000
#define	ADGDR_DONE 		0x80000000

#define	ADGSR_BURST 		0x00010000
#define	ADGSR_START 		0x07000000
#define	ADGSR_EDGE 		0x08000000

#define	ADSTAT_DONE 		0x000000FF
#define	ADSTAT_DONE0 		0x00000001
#define	ADSTAT_DONE1 		0x00000002
#define	ADSTAT_DONE2 		0x00000004
#define	ADSTAT_DONE3 		0x00000008
#define	ADSTAT_DONE4 		0x00000010
#define	ADSTAT_DONE5 		0x00000020
#define	ADSTAT_DONE6 		0x00000040
#define	ADSTAT_DONE7 		0x00000080
#define	ADSTAT_OVERRUN	 	0x0000FF00
#define	ADSTAT_OVERRUN0 	0x00000100
#define	ADSTAT_OVERRUN1 	0x00000200
#define	ADSTAT_OVERRUN2 	0x00000400
#define	ADSTAT_OVERRUN3 	0x00000800
#define	ADSTAT_OVERRUN4 	0x00001000
#define	ADSTAT_OVERRUN5 	0x00002000
#define	ADSTAT_OVERRUN6 	0x00004000
#define	ADSTAT_OVERRUN7 	0x00008000
#define	ADSTAT_ADINT 		0x00010000

#define	ADINTEN_INTEN	 	0x000000FF
#define	ADINTEN_INTEN0	 	0x00000001
#define	ADINTEN_INTEN1	 	0x00000002
#define	ADINTEN_INTEN2	 	0x00000004
#define	ADINTEN_INTEN3	 	0x00000008
#define	ADINTEN_INTEN4	 	0x00000010
#define	ADINTEN_INTEN5	 	0x00000020
#define	ADINTEN_INTEN6	 	0x00000040
#define	ADINTEN_INTEN7	 	0x00000080
#define	ADINTEN_GINTEN	 	0x00000100

#define	ADDR_RESULT 		0x0000FFC0
#define	ADDR_OVERRUN 		0x40000000
#define	ADDR_DONE 		0x80000000

#endif
