/******************************************************************************
 *
 * $RCSfile$
 * $Revision$
 *
 * Header file for Philips LPC ARM Processors.
 * Copyright 2004 R O SoftWare
 *
 * No guarantees, warrantees, or promises, implied or otherwise.
 * May be used for hobby or commercial purposes provided copyright
 * notice remains intact.
 *
 *****************************************************************************/
#ifndef INC_LPC_SPI_H
#define INC_LPC_SPI_H

// Serial Peripheral Interface Registers (SPI)
typedef struct
{
  REG_8 cr;                             // Control Register
  REG_8 _pad0[3];
  REG_8 sr;                             // Status Register
  REG_8 _pad1[3];
  REG_8 dr;                             // Data Register
  REG_8 _pad2[3];
  REG_8 ccr;                            // Clock Counter Register
  REG_8 _pad3[3];
  REG_8 tcr;                            // Test Control Register
  REG_8 _pad4[3];
  REG_8 tsr;                            // Test Status Register
  REG_8 _pad5[3];
  REG_8 tor;                            // Test Observe Register
  REG_8 _pad6[3];
  REG_8 flag;                           // Interrupt Flag Register
  REG_8 _pad7[3];
} spiRegs_t;


// SPI Control Register
#define SPCR_BE    (1 << 2)            // BitEnable : If set the SPI controller
                                       // sends and receives the number of bits
				       // selected by bits 11:8.
#define SPCR_CPHA  (1 << 3)            // Clock phase control
#define SPCR_CPOL  (1 << 4)            // Clock polarity control.
#define SPCR_MSTR  (1 << 5)            // Master mode select.
#define SPCR_LSBF  (1 << 6)            // LSB First controls
#define SPCR_SPIE  (1 << 7)            // Serial peripheral interrupt enable.
#define SPCR_BITS  (0xF << 8)          // When bit 2 of this register is 1,
                                       // this field controls the number of 
				       // bits per transfer
				       // 1000 :  8 bits per transfer
				       // 1001 :  9 bits per transfer
				       // 1010 : 10 bits per transfer
				       // 1011 : 11 bits per transfer
				       // 1100 : 12 bits per transfer
				       // 1101 : 13 bits per transfer
				       // 1110 : 14 bits per transfer
				       // 1111 : 15 bits per transfer
				       // 0000 : 16 bits per transfer

//SPI Status Register
#define SPSR_ABRT  (1 << 3)           // Slave abort.
#define SPSR_MODF  (1 << 4)           // Mode fault.
#define SPSR_ROVR  (1 << 5)           // Read overrun.
#define SPSR_WCOL  (1 << 6)           // Write collision.
#define SPSR_SPIF  (1 << 7)           // SPI transfer complete flag.

//SPI Interrupt register
#define SPINT_IF   (1 << 0)           // SPI interrupt flag.


#endif
