#include <system_def.h>
#include <cpu_def.h>
#include <hal_ints.h>
#include <hal_intr.h>
#include <stdint.h>
 
// -------------------------------------------------------------------------
// Hardware init

// Return value of VPBDIV register. According to errata doc
// we need to read twice consecutively to get correct value
uint32_t lpc_get_vpbdiv(void)
{   
    uint32_t vpbdiv_reg;

    vpbdiv_reg=VPBDIV;
    vpbdiv_reg=VPBDIV;

    return (vpbdiv_reg);
}


// -------------------------------------------------------------------------
// This routine is called to respond to a hardware interrupt (IRQ).  It
// should interrogate the hardware and return the IRQ vector number.
int hal_IRQ_handler(void)
{
    uint32_t irq_num, irq_stat;

    irq_stat=VICIRQStatus;
    for (irq_num = 0; irq_num < 32; irq_num++)
      if (irq_stat & (1 << irq_num))
        break;
    
    // If not a valid interrrupt source, treat as spurious interrupt    
    if (irq_num < HAL_ISR_MIN || irq_num > HAL_ISR_MAX)
      irq_num = HAL_INTERRUPT_NONE;
    
    return (irq_num);
}


// -------------------------------------------------------------------------
// Interrupt control
//

// Block the the interrupt associated with the vector
void hal_interrupt_mask(int vector)
{
    VICIntEnClear = 1 << vector;
}

// Unblock the the interrupt associated with the vector
void hal_interrupt_unmask(int vector)
{
    VICIntEnable = 1 << vector;
}

// Acknowledge the the interrupt associated with the vector. This
// clears the interrupt but may result in another interrupt being
// delivered
void hal_interrupt_acknowledge(int vector)
{

    // External interrupts have to be cleared from the EXTINT register
    if (vector >= HAL_INTERRUPT_EINT0 &&
        vector <= HAL_INTERRUPT_EINT3)
      {
        // Map int vector to corresponding bit (0..3)
        vector = 1 << (vector - HAL_INTERRUPT_EINT0);
        	
        // Clear the external interrupt
	EXTINT=vector;
      }
    
    // Acknowledge interrupt in the VIC
    VICVectAddr=0;
}

// This provides control over how an interrupt signal is detected.
// Options are between level or edge sensitive (level) and high/low
// level or rising/falling edge triggered (up).
//
// This should be simple, but unfortunately on some processor revisions,
// it trips up on two errata issues (for the LPC2294 Rev.A these are
// EXTINT.1 and VPBDIV.1) and so on these devices a somewhat convoluted
// sequence in order to work properly. There is nothing in the errata
// sequence that won't work on a processor without these issues.
void hal_interrupt_configure(int vector, int level, int up)
{
    uint32_t regval;
#ifdef HAL_ARM_LPC2XXX_EXTINT_ERRATA
    uint32_t saved_vpbdiv;
#endif

    // Map int vector to corresponding bit (0..3)
    vector = 1 << (vector - HAL_INTERRUPT_EINT0);
    
#ifdef HAL_ARM_LPC2XXX_EXTINT_ERRATA
    // From discussions with the Philips applications engineers on the
    // Yahoo LPC2000 forum, it appears that in order up change both
    // EXTMODE and EXTPOLAR, the operations have to be performed in
    // two passes as follows:
    // old=VPBDIV (x2),
    //     VPBDIV=0, EXTMODE=n, VPBDIV=n, VPBDIV=0, EXTPOLAR=y, VPBDIV=y
    // VPCDIV=old
    
    // Save current VPBDIV register settings
    saved_vpbdiv = lpc_get_vpbdiv();
    
    // Clear VPBDIV register
    VPBDIV=0;
    
    // Read current mode and update for level (0) or edge detection (1)
    regval=EXTMODE;
    if (level)
      regval &= ~vector;
    else
      regval |= vector;
    EXTMODE=regval;
    
    // Set VPBDIV register to same value as mode
    VPBDIV=regval;
    
    // Clear VPBDIV register
    VPBDIV=0;
    
    // Read current polarity and update for trigger level or edge
    // level: high (1), low (0) edge: rising (1), falling (0)
    regval=EXTPOLAR;
    if (up)
      regval |= vector;
    else
      regval &= ~vector;
    EXTPOLAR=regval;
      
    
    // Set VPBDIV register to same value as mode
    VPBDIV=regval;
    
    // Restore saved VPBDIV register
    VPBDIV=saved_vpbdiv;
#else
    // Read current mode and update for level (0) or edge detection (1)
    regval=EXTMODE;
    if (level)
      regval &= ~vector;
    else
      regval |= vector;
    EXTMODE=regval;
    
    // Read current polarity and update for trigger level or edge
    // level: high (1), low (0) edge: rising (1), falling (0)
    regval=EXTPOLAR;
    if (up)
      regval |= vector;
    else
      regval &= ~vector;
    EXTPOLAR=regval;
#endif
    // Clear any spurious interrupt that might have been generated
    EXTINT=vector;
}

// Change interrupt level. This is a non-operation on the LPC2XXX
void hal_interrupt_set_level(int vector, int level)
{
}

uint32_t hal_default_isr(int vector, uint32_t data)
{
  return 0;
}

uint32_t hal_interrupt_handlers[HAL_ISR_COUNT]={[0 ... HAL_ISR_COUNT-1]=(uint32_t)hal_default_isr};
uint32_t hal_interrupt_data[HAL_ISR_COUNT];

#if !defined(__thumb__)
void irq_handler_resolver(void) __attribute__ ((interrupt));
#endif
void irq_handler_resolver(void)
{
  int v;
  uint32_t f,d;

  v=hal_IRQ_handler();
  if (v==HAL_INTERRUPT_NONE) return;
  f=hal_interrupt_handlers[v];
  d=hal_interrupt_data[v];
  ((hal_isr)f)(v,d);  
  hal_interrupt_acknowledge(v);
}

int request_irq(unsigned int irqnum, irq_handler_t handler, unsigned long flags,
                const char *name, void *context)
{
  HAL_INTERRUPT_ATTACH(irqnum, handler, context);
  HAL_INTERRUPT_UNMASK(irqnum);
  return irqnum;
}

void free_irq(unsigned int irqnum,void *ctx)
{
  HAL_INTERRUPT_MASK(irqnum);
  HAL_INTERRUPT_DETACH(irqnum, NULL);
}
