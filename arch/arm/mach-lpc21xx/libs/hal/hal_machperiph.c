#include <system_def.h>
#include <cpu_def.h>
#include <hal_machperiph.h>

unsigned int system_frequency = FOSC; /*!< System Clock Frequency (Core Clock)  */

void system_clock_init(void)
{
  // set PLL multiplier & divisor.
  // values computed from config.h
  PLLCFG = PLLCFG_MSEL | PLLCFG_PSEL;

  // enable PLL
  PLLCON = PLLCON_PLLE;
  PLLFEED = 0xAA;                       // Make it happen.  These two updates
  PLLFEED = 0x55;                       // MUST occur in sequence.

  // wait for PLL lock
  while (!(PLLSTAT & PLLSTAT_LOCK))
    continue;

  // enable & connect PLL
  PLLCON = PLLCON_PLLE | PLLCON_PLLC;
  PLLFEED = 0xAA;                       // Make it happen.  These two updates
  PLLFEED = 0x55;                       // MUST occur in sequence.

  system_frequency=CCLK;

  // setup & enable the MAM
  MAMCR = 0;
  MAMTIM = MAMTIM_CYCLES;
  MAMCR = MAMCR_FULL;

  // set the peripheral bus speed
  // value computed from config.h
  VPBDIV = VPBDIV_VALUE;                  // set the peripheral bus clock speed
}

void lpc_watchdog_feed()
{
  unsigned long flags;

  save_and_cli(flags);
  WDFEED = 0xAA;
  WDFEED = 0x55;
  restore_flags(flags);
}

void lpc_watchdog_init(int on,int timeout_ms)
{
  if (!on) return;
  WDTC = PCLK/(1000/timeout_ms);
  WDMOD = 0x03;				   /* Enable watchdog timer and reset */
}
