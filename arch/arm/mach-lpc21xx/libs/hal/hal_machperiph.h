#ifndef _HAL_MACHPERIPH_H
#define _HAL_MACHPERIPH_H

extern unsigned int system_frequency; /*!< System Clock Frequency (Core Clock)  */

void system_clock_init(void);

void lpc_watchdog_init(int on,int timeout_ms);
void lpc_watchdog_feed();

#endif /* _HAL_MACHPERIPH_H */

