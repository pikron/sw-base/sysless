.macro	code32_fnc_header fncname
	.global	\fncname
#if defined(__thumb__) && !defined(__THUMB_INTERWORK__)
        .code 16
	.align	0
        .thumb_func
	.type	\fncname, %function
\fncname:
	bx	pc
	nop
        .code 32
	.type	_code_32_\fncname, %function
_code_32_\fncname:
#else
        .code 32
	.align	0
	.type	\fncname, %function
\fncname:
#endif

.endm

        .text

	code32_fnc_header irq_fnc_sti
	mrs	r0, cpsr
	bic	r0, r0, #128
	msr	cpsr_c, r0
	bx	lr

	code32_fnc_header irq_fnc_cli
	mrs	r0, cpsr
	orr	r0, r0, #128
	msr     cpsr_c, r0
	bx	lr

	code32_fnc_header irq_fnc_save_and_cli
	mrs	r0, cpsr
	orr	r1, r0, #128
	msr     cpsr_c, r1
	bx	lr

	code32_fnc_header irq_fnc_save_flags
	mrs	r0, cpsr
	bx	lr

	code32_fnc_header irq_fnc_restore_flags
	msr	cpsr_c, r0
	bx	lr
