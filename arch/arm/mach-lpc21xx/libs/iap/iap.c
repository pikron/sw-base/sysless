#include <system_def.h>
#include <cpu_def.h>
#include <hal_machperiph.h>

#define CMD_SUCCESS 0
#define BUSY 11

#define IAP_CMD_PREPARE 	50
#define IAP_CMD_WRITE 		51
#define IAP_CMD_ERASE 		52
#define IAP_CMD_READ_PARTID	54

unsigned int command[5];
unsigned int result[2];

extern void iap_asm_entry (unsigned int *,unsigned int *);
#define iap_entry iap_asm_entry

#ifdef INC_LPC210x_H
inline int addr2sec(unsigned long addr)
{
  return addr/0x2000;
}
#elif defined INC_LPC214x_H || defined INC_LPC2348_H
inline int addr2sec(unsigned long addr)
{
  if (addr<0x8000) return (addr>>12);
  else if (addr<0x78000) return (addr>>15)+7;
       else return 22+((addr&0x7fff)>>12); 
}
#else
#error "Undefined type of CPU for function addr2sec!"
#endif

int lpcisp_read_partid() 
{
  command[0] = IAP_CMD_READ_PARTID;
  iap_entry(command, result);
  return result[1];
}

int lpcisp_prepare_sectors(unsigned char start, unsigned char end)
{
  command[0] = IAP_CMD_PREPARE;
  command[1] = start;
  command[2] = end;
  command[3] = system_frequency/1000;

  iap_entry(command, result);

  return (CMD_SUCCESS == *result);
}

int lpcisp_erase_sectors(unsigned char start, unsigned char end)
{
  command[0] = IAP_CMD_ERASE;
  command[1] = start;
  command[2] = end;
  command[3] = system_frequency/1000;

  iap_entry(command, result);

  return (CMD_SUCCESS == *result);
}

int lpcisp_erase(void *addr, int len)
{
  int start,end;
  unsigned long flags;
  
  start=addr2sec((unsigned long)addr);
  end=addr2sec((unsigned long)addr+len-1);
  
  if (end<start) return 0;

  save_and_cli(flags);

  lpcisp_prepare_sectors(start,end);
  if (CMD_SUCCESS != *result) return 0;

  lpcisp_erase_sectors(start,end);

  restore_flags(flags);

  return (CMD_SUCCESS == *result);
}

int lpcisp_write(void *addr_des, const void *addr_src, int len)
{
  int start,end;
  unsigned long flags;
  
  start=addr2sec((unsigned long)addr_des);
  end=start;

  save_and_cli(flags);

  lpcisp_prepare_sectors(start,end);
  if (CMD_SUCCESS != *result) return 0;

  command[0] = IAP_CMD_WRITE;
  command[1] = (unsigned int)addr_des;
  command[2] = (unsigned int)addr_src;
  command[3] = len;
  command[4] = system_frequency/1000;

  iap_entry(command, result);

  restore_flags(flags);

  return (CMD_SUCCESS == *result);
}


