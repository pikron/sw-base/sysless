#ifndef _LPCIAP_H
#define _LPCIAP_H

int lpcisp_read_partid();
int lpcisp_erase(void *addr, int len);
int lpcisp_write(void *addr_des, const void *addr_src, int len);

#endif  /* _LPCIAP_H */
