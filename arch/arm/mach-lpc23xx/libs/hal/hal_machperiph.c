#include <system_def.h>
#include <cpu_def.h>
#include <hal_machperiph.h>

unsigned int system_frequency = FOSC; /*!< System Clock Frequency (Core Clock)  */

void system_clock_init(void)
{
  
  // oscilator must be running, before PLL changes
  SCS = SCS_GPIOM | SCS_OSCEN;
  // wait for main clock to be ready to use
  while (!(SCS & SCS_OSCSTAT))
    continue;   
  
  CLKSRCSEL = 1;                          //source is XTAL
  
  // set PLL multiplier & divisor.
  // values computed from config.h
  PLLCFG = PLLCFG_MSEL | PLLCFG_PSEL;
  PLLFEED = 0xAA;                       // Make it happen.  These two updates
  PLLFEED = 0x55;                       // MUST occur in sequence.
  
  // enable PLL
  PLLCON = PLLCON_PLLE;
  PLLFEED = 0xAA;                       // Make it happen.  These two updates
  PLLFEED = 0x55;                       // MUST occur in sequence.
  
  // Change the CPU Clock Divider setting for the operation with the PLL. It's critical to do this before connecting the PLL.
  CCLKCFG = HCLK_DIV_CPU;               //only 0 and odd values have to be used
  USBCLKCFG = HCLK_DIV_USB;             //only 0 and odd values have to be used

  // wait for PLL lock
  while (!(PLLSTAT & PLLSTAT_LOCK))
    continue;
  
  // enable & connect PLL
  PLLCON = PLLCON_PLLE | PLLCON_PLLC;
  PLLFEED = 0xAA;                       // Make it happen.  These two updates
  PLLFEED = 0x55;                       // MUST occur in sequence. 
  
  system_frequency=CCLK;

  // setup & enable the MAM
  MAMCR = 0;
  MAMTIM = MAMTIM_CYCLES;
  MAMCR = MAMCR_FULL;
}

void lpc_watchdog_feed()
{
  unsigned long flags;

  save_and_cli(flags);
  WDFEED = 0xAA;
  WDFEED = 0x55;
  restore_flags(flags);
}

void lpc_watchdog_init(int on,int timeout_ms)
{
  if (!on) return;
  WDCLKSEL=1; //PCLK as WDT source
  /* FIXME - only seconds are used for WDT */
  WDTC=(PCLK/4)*(timeout_ms/1000);
  WDMOD = 0x03;				   /* Enable watchdog timer and reset */
}
