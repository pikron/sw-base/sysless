/**
 * main header for CPU nRF51822
 */

#ifndef _ARCH_NRF51822_H_
#define _ARCH_NRF51822_H_

#include "nRF51.h"
#include "nRF51_bitfields.h"

/* additional useful definitions which are not defined in nRF51.h */
#define IRQn_SIZE    26  /* number of external IRQs */

#endif /* _ARCH_NRF51822_H_ */

