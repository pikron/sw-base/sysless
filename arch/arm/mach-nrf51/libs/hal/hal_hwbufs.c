#include <stdint.h>
#include <string.h>
#include <hal_machperiph.h>

extern char _hwbufs;
extern char _ehwbufs;
extern char __hwbuf_end_memory;

void *nrf_reserve_hwbuf(unsigned long size)
{
  static char *hwbuf_ram_avail=&_ehwbufs;
  char *prev_avail;

  /*printf("usb reserve %lu at 0x%lx\n",size,(unsigned long)usb_ram_avail);*/

  if(!hwbuf_ram_avail || ((hwbuf_ram_avail+size)>(&__hwbuf_end_memory)))
    return NULL;

//  prev_avail=(char *)(((uintptr_t)hwbuf_ram_avail+3)&~3);
  prev_avail=(char *)hwbuf_ram_avail; // 1 byte alignment
  hwbuf_ram_avail += ((size+3)>>2)<<2;

  return prev_avail;
}
