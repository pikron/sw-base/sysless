
#include <system_def.h>
#include <cpu_def.h>
#include <hal_machperiph.h>


///*----------------------------------------------------------------------------
//  Check the register settings
// *----------------------------------------------------------------------------*/
//#define CHECK_RANGE(val, min, max)                ((val < min) || (val > max))
//#define CHECK_RSVD(val, mask)                     (val & mask)

///* Clock Configuration -------------------------------------------------------*/
//#if (CHECK_RSVD((SCS_Val),       ~0x00000030))
//   #error "SCS: Invalid values of reserved bits!"
//#endif

//#if (CHECK_RANGE((CLKSRCSEL_Val), 0, 2))
//   #error "CLKSRCSEL: Value out of range!"
//#endif

//#if (CHECK_RSVD((PLL0CFG_Val),   ~0x00FF7FFF))
//   #error "PLL0CFG: Invalid values of reserved bits!"
//#endif

//#if (CHECK_RSVD((PLL1CFG_Val),   ~0x0000007F))
//   #error "PLL1CFG: Invalid values of reserved bits!"
//#endif

//#if ((CCLKCFG_Val != 0) && (((CCLKCFG_Val - 1) % 2)))
//   #error "CCLKCFG: CCLKSEL field does not contain only odd values or 0!"
//#endif

//#if (CHECK_RSVD((USBCLKCFG_Val), ~0x0000000F))
//   #error "USBCLKCFG: Invalid values of reserved bits!"
//#endif

//#if (CHECK_RSVD((PCLKSEL0_Val),   0x000C0C00))
//   #error "PCLKSEL0: Invalid values of reserved bits!"
//#endif

//#if (CHECK_RSVD((PCLKSEL1_Val),   0x03000300))
//   #error "PCLKSEL1: Invalid values of reserved bits!"
//#endif

//#if (CHECK_RSVD((PCONP_Val),      0x10100821))
//   #error "PCONP: Invalid values of reserved bits!"
//#endif

///* Flash Accelerator Configuration -------------------------------------------*/
//#if (CHECK_RSVD((FLASHCFG_Val), ~0x0000F07F))
//   #error "FLASHCFG: Invalid values of reserved bits!"
//#endif

/*----------------------------------------------------------------------------
  Clock Variable definitions
 *----------------------------------------------------------------------------*/
unsigned int system_frequency = HCLK; /*!< System Clock Frequency (Core Clock)  */

void system_clock_init(void)
{
#if (CLOCK_SETUP)                       /* Clock Setup                        */
  // set HFCLK
  #if (HFCLK_XTAL==32000000UL)
    NRF_CLOCK->XTALFREQ = CLOCK_XTALFREQ_XTALFREQ_32MHz;
  #else
    NRF_CLOCK->XTALFREQ = CLOCK_XTALFREQ_XTALFREQ_16MHz;
  #endif
  // NRF datasheet refences to HFCLKSRC register, but it is not described !!!
  
  // set LFCLK
  NRF_CLOCK->LFCLKSRC =  LFCLK_SOURCE;

  // run HFCLK
  #if (HFCLK_RUNONSTART)
    NRF_CLOCK->TASKS_HFCLKSTART = 1;
    while ((NRF_CLOCK->HFCLKSTAT & CLOCK_HFCLKSTAT_STATE_Msk)==CLOCK_HFCLKSTAT_STATE_NotRunning) ;
  #endif
  // run LFCLK
  #if (LFCLK_RUNONSTART)
    NRF_CLOCK->TASKS_LFCLKSTART = 1;
    while ((NRF_CLOCK->LFCLKSTAT & CLOCK_LFCLKSTAT_STATE_Msk)==CLOCK_LFCLKSTAT_STATE_NotRunning) ;
    // TODO: LFCLK calibration ???
  #endif
#endif

}

static uint32_t watchdog_enb_mask = 0;

void watchdog_feed(int src)
{
  unsigned long flags;

  save_and_cli(flags);
  NRF_WDT->RR[src] = 0x6E524635;
  restore_flags(flags);
}

void watchdog_init(int n_src, int timeout_ms)
{
  if (!n_src) return;
  /* enable N sources for watchdog */
  watchdog_enb_mask = (1<<n_src)-1;
  uint32_t rren_val = watchdog_enb_mask;
  NRF_WDT->RREN = rren_val;
  /* set WDT to pause when sleeping or halting */
  NRF_WDT->CONFIG = 0;
  /* set timeout */
  NRF_WDT->CRV = ((timeout_ms*32768)/1000)-1;
  /* enable interrupt */
  // NRF_WDT->INTENSET = 1;
  /* enable WDT */
  NRF_WDT->TASKS_START = 1;  /* Enable watchdog timer */
}

uint32_t watchdog_enabled()
{
  return watchdog_enb_mask;
}
