#ifndef _HAL_MACHPERIPH_H
#define _HAL_MACHPERIPH_H

extern unsigned int system_frequency;

void system_clock_init(void);
//void system_clock_config(void);

void watchdog_init(int n_src, int timeout_ms);
void watchdog_feed();
uint32_t watchdog_enabled();

//#if CONFIG_HAL_WITH_HWBUFS
  void *nrf_reserve_hwbuf(unsigned long size);
//#endif

#endif /* _HAL_MACHPERIPH_H */

