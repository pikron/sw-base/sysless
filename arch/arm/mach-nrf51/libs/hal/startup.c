/****************************************************************************//**
 * @file :    startup.c
 * @brief :   CMSIS Cortex-M0 Core Startup File for nRF51822
 * @version : V1.0
 * @date :    2015/01/04
 *
 ******************************************************************************/

// Mod by nio for the .fastcode part

#include "cpu_def.h"
#include "nRF51822.h"

#define WEAK __attribute__ ((weak))
//*****************************************************************************
//
// Forward declaration of the default fault handlers.
//
//*****************************************************************************
/* System exception vector handler */
void WEAK 		Reset_Handler(void);             /* Reset Handler */
void WEAK 		NMI_Handler(void);               /* NMI Handler */
void WEAK 		HardFault_Handler(void);         /* Hard Fault Handler */
void WEAK 		SVC_Handler(void);               /* SVCall Handler */
void WEAK 		PendSV_Handler(void);            /* PendSV Handler */
void WEAK 		SysTick_Handler(void);           /* SysTick Handler */

/* External interrupt vector handler */
void WEAK   PWR_CLK_IRQHandler(void);   /*!<   0  POWER_CLOCK */
void WEAK   RADIO_IRQHandler(void);     /*!<   1  RADIO */
void WEAK   UART0_IRQHandler(void);     /*!<   2  UART0 */
void WEAK   SPI0_TWI0_IRQHandler(void); /*!<   3  SPI0_TWI0 */
void WEAK   SPI1_TWI1_IRQHandler(void); /*!<   4  SPI1_TWI1 */
void WEAK   GPIOTE_IRQHandler(void);    /*!<   6  GPIOTE */
void WEAK   ADC_IRQHandler(void);       /*!<   7  ADC */
void WEAK   TIMER0_IRQHandler(void);    /*!<   8  TIMER0 */
void WEAK   TIMER1_IRQHandler(void);    /*!<   9  TIMER1 */
void WEAK   TIMER2_IRQHandler(void);    /*!<  10  TIMER2 */
void WEAK   RTC0_IRQHandler(void);      /*!<  11  RTC0 */
void WEAK   TEMP_IRQHandler(void);      /*!<  12  TEMP */
void WEAK   RNG_IRQHandler(void);       /*!<  13  RNG */
void WEAK   ECB_IRQHandler(void);       /*!<  14  ECB */
void WEAK   CCM_AAR_IRQHandler(void);   /*!<  15  CCM_AAR */
void WEAK   WDT_IRQHandler(void);       /*!<  16  WDT */
void WEAK   RTC1_IRQHandler(void);      /*!<  17  RTC1 */
void WEAK   QDEC_IRQHandler(void);      /*!<  18  QDEC */
void WEAK   LPCOMP_IRQHandler(void);    /*!<  19  LPCOMP */
void WEAK   SWI0_IRQHandler(void);      /*!<  20  SWI0 */
void WEAK   SWI1_IRQHandler(void);      /*!<  21  SWI1 */
void WEAK   SWI2_IRQHandler(void);      /*!<  22  SWI2 */
void WEAK   SWI3_IRQHandler(void);      /*!<  23  SWI3 */
void WEAK   SWI4_IRQHandler(void);      /*!<  24  SWI4 */
void WEAK   SWI5_IRQHandler(void);      /*!<  25  SWI5 */

void WEAK   __bbconf_pt_magic(void);
void WEAK   __bbconf_pt_addr(void);

/* Exported types --------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
extern unsigned long _etext;
extern unsigned long _sidata;		/* start address for the initialization values of the .data section. defined in linker script */
extern unsigned long _sdata;		/* start address for the .data section. defined in linker script */
extern unsigned long _edata;		/* end address for the .data section. defined in linker script */

extern unsigned long _sifastcode;		/* start address for the initialization values of the .fastcode section. defined in linker script */
extern unsigned long _sfastcode;		/* start address for the .fastcode section. defined in linker script */
extern unsigned long _efastcode;		/* end address for the .fastcode section. defined in linker script */

extern unsigned long _sbss;			/* start address for the .bss section. defined in linker script */
extern unsigned long _ebss;			/* end address for the .bss section. defined in linker script */

extern void _estack;		/* init value for the stack pointer. defined in linker script */

extern void (_setup_board)(void);	        /* setup_board adress function */
//void WEAK   _setup_board(void);

extern unsigned long _mem_app_start;

#if NRF_USE_USER_ASAP_PROC
  extern void hal_user_asap_proc(void);
#endif /* NRF_USE_USER_ASAP_PROC */


/* Private typedef -----------------------------------------------------------*/
/* function prototypes ------------------------------------------------------*/
void Reset_Handler(void) __attribute__((__interrupt__));
extern int main(void);

typedef void (*FNC)(void);
FNC fnc_entry;

/******************************************************************************
*
* The minimal vector table for a Cortex M3.  Note that the proper constructs
* must be placed on this to ensure that it ends up at physical address
* 0x0000.0000.
*
******************************************************************************/

extern unsigned long _stack;

__attribute__ ((section(".isr_vector")))
void (* const g_pfnVectors[])(void) = {
  (void (*)(void))&_stack,   /* The initial stack pointer */
  Reset_Handler,             /* Reset Handler */
  NMI_Handler,               /* NMI Handler */
  HardFault_Handler,         /* Hard Fault Handler */
  0,                         /* Reserved */
  0,                         /* Reserved */
  0,                         /* Reserved */
  0,                         /* Reserved */
  __bbconf_pt_magic,         /* Reserved or BBCONF_MAGIC_ADDR */
  __bbconf_pt_addr,          /* Reserved or BBCONF_PTPTR_ADDR */
  0,                         /* Reserved */
  SVC_Handler,               /* SVCall Handler */
  0,                         /* Reserved */
  0,                         /* Reserved */
  PendSV_Handler,            /* PendSV Handler */
  SysTick_Handler,           /* SysTick Handler */

  // External Interrupts
  PWR_CLK_IRQHandler,        /*!<   0  POWER_CLOCK */
  RADIO_IRQHandler,        /*!<   1  RADIO */
  UART0_IRQHandler,        /*!<   2  UART0 */
  SPI0_TWI0_IRQHandler,        /*!<   3  SPI0_TWI0 */
  SPI1_TWI1_IRQHandler,        /*!<   4  SPI1_TWI1 */
  0,                         /* 5 - Unused */
  GPIOTE_IRQHandler,        /*!<   6  GPIOTE */
  ADC_IRQHandler,        /*!<   7  ADC */
  TIMER0_IRQHandler,        /*!<   8  TIMER0 */
  TIMER1_IRQHandler,        /*!<   9  TIMER1 */
  TIMER2_IRQHandler,        /*!<  10  TIMER2 */
  RTC0_IRQHandler,        /*!<  11  RTC0 */
  TEMP_IRQHandler,        /*!<  12  TEMP */
  RNG_IRQHandler,        /*!<  13  RNG */
  ECB_IRQHandler,        /*!<  14  ECB */
  CCM_AAR_IRQHandler,        /*!<  15  CCM_AAR */
  WDT_IRQHandler,        /*!<  16  WDT */
  RTC1_IRQHandler,        /*!<  17  RTC1 */
  QDEC_IRQHandler,        /*!<  18  QDEC */
  LPCOMP_IRQHandler,        /*!<  19  LPCOMP */
  SWI0_IRQHandler,        /*!<  20  SWI0 */
  SWI1_IRQHandler,        /*!<  21  SWI1 */
  SWI2_IRQHandler,        /*!<  22  SWI2 */
  SWI3_IRQHandler,        /*!<  23  SWI3 */
  SWI4_IRQHandler,        /*!<  24  SWI4 */
  SWI5_IRQHandler,        /*!<  25  SWI5 */
};

/*******************************************************************************
* Function Name  : Reset_Handler
* Description    : This is the code that gets called when the processor first starts execution
*		       following a reset event.  Only the absolutely necessary set is performed,
*		       after which the application supplied main() routine is called.
* Input          :
* Output         :
* Return         :
*******************************************************************************/

void Reset_Handler(void)
{
  unsigned long *pulDest;
  unsigned long *pulSrc;

#if NRF_USE_USER_ASAP_PROC
  hal_user_asap_proc();
#endif /* NRF_USE_USER_ASAP_PROC */
  /* disable all external IRQs */
  NVIC->ICER[0] = (1<<IRQn_SIZE)-1;
#if NRF_CPU_HWID_REV1
  /* workaround errata for CA/CO version of nRF chip */
  if ((NRF_FICR->CONFIGID & 0xffff)==0x001D) {
    NRF_POWER->RAMON = 0x0f;
  }
#endif /* NRF_CPU_HWID_REV1 */

  //
  // Copy the data segment initializers from flash to SRAM in ROM mode
  //
  if (&_sidata != &_sdata) {	// only if needed
    pulSrc = &_sidata;
    for(pulDest = &_sdata; pulDest < &_edata; ) {
      *(pulDest++) = *(pulSrc++);
    }
  }

  // Copy the .fastcode code from ROM to SRAM
  if (&_sifastcode != &_sfastcode) {	// only if needed
    pulSrc = &_sifastcode;
    for(pulDest = &_sfastcode; pulDest < &_efastcode; ) {
      *(pulDest++) = *(pulSrc++);
    }
  }

  //
  // Zero fill the bss segment.
  //
  for(pulDest = &_sbss; pulDest < &_ebss; )
  {
    *(pulDest++) = 0;
  }

  // copy initial values and set irq table
  // TODO: pridat podporu pro vzdalenou tabulku v RAM
//    if(irq_handler_table && irq_table_size) {
//        int i;
//        pulSrc = (unsigned long*)g_pfnVectors;
//        pulDest = (unsigned long*)irq_handler_table;
//        for(i = irq_table_size; i--; ) {
//            *(pulDest++) = *(pulSrc++);
//        }
//        /*SCB->VTOR=0x10000000;*/
//        SCB->VTOR=(uint32_t)irq_handler_table;
//    }

  //if (_setup_board!=0)
  _setup_board();

  //
  // Call the application's entry point.
  //
  main();

  /* disable interrupts */
  NVIC->ICER[0] = (1<<IRQn_SIZE)-1;
  __set_MSP(*(unsigned long *)(&_mem_app_start));         /* Stack pointer */
  fnc_entry = (FNC)*(unsigned long *)(&_mem_app_start+1); /* Reset handler */
  fnc_entry();
}

//*****************************************************************************
//
// Provide weak aliases for each Exception handler to the Default_Handler.
// As they are weak aliases, any function with the same name will override
// this definition.
//
//*****************************************************************************
#pragma weak SVC_Handler = Default_Handler                /* SVCall Handler */
#pragma weak PendSV_Handler = Default_Handler             /* PendSV Handler */
#pragma weak SysTick_Handler = Default_Handler            /* SysTick Handler */

/* External interrupt vector handler */
#pragma weak PWR_CLK_IRQHandler = Default_Handler   /*!<   0  POWER_CLOCK */
#pragma weak RADIO_IRQHandler = Default_Handler     /*!<   1  RADIO */
#pragma weak UART0_IRQHandler = Default_Handler     /*!<   2  UART0 */
#pragma weak SPI0_TWI0_IRQHandler = Default_Handler /*!<   3  SPI0_TWI0 */
#pragma weak SPI1_TWI1_IRQHandler = Default_Handler /*!<   4  SPI1_TWI1 */
#pragma weak GPIOTE_IRQHandler = Default_Handler    /*!<   6  GPIOTE */
#pragma weak ADC_IRQHandler = Default_Handler       /*!<   7  ADC */
#pragma weak TIMER0_IRQHandler = Default_Handler    /*!<   8  TIMER0 */
#pragma weak TIMER1_IRQHandler = Default_Handler    /*!<   9  TIMER1 */
#pragma weak TIMER2_IRQHandler = Default_Handler    /*!<  10  TIMER2 */
#pragma weak RTC0_IRQHandler = Default_Handler      /*!<  11  RTC0 */
#pragma weak TEMP_IRQHandler = Default_Handler      /*!<  12  TEMP */
#pragma weak RNG_IRQHandler = Default_Handler       /*!<  13  RNG */
#pragma weak ECB_IRQHandler = Default_Handler       /*!<  14  ECB */
#pragma weak CCM_AAR_IRQHandler = Default_Handler   /*!<  15  CCM_AAR */
#pragma weak WDT_IRQHandler = Default_Handler       /*!<  16  WDT */
#pragma weak RTC1_IRQHandler = Default_Handler      /*!<  17  RTC1 */
#pragma weak QDEC_IRQHandler = Default_Handler      /*!<  18  QDEC */
#pragma weak LPCOMP_IRQHandler = Default_Handler    /*!<  19  LPCOMP */
#pragma weak SWI0_IRQHandler = Default_Handler      /*!<  20  SWI0 */
#pragma weak SWI1_IRQHandler = Default_Handler      /*!<  21  SWI1 */
#pragma weak SWI2_IRQHandler = Default_Handler      /*!<  22  SWI2 */
#pragma weak SWI3_IRQHandler = Default_Handler      /*!<  23  SWI3 */
#pragma weak SWI4_IRQHandler = Default_Handler      /*!<  24  SWI4 */
#pragma weak SWI5_IRQHandler = Default_Handler      /*!<  25  SWI5 */

//*****************************************************************************
//
// This is the code that gets called when the processor receives an unexpected
// interrupt.  This simply enters an infinite loop, preserving the system state
// for examination by a debugger.
//
//*****************************************************************************
void Default_Handler(void) {
	// Go into an infinite loop.
	//
	while (1) {
	}
}


//#pragma weak _setup_board = default_setup_board                /* default setup_board */

//void default_setup_board(void)
//{
//  // nothing to do
//}
