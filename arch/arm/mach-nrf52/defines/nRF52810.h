/**
 * main header for CPU nRF52810
 */

#ifndef _ARCH_NRF52810_H_
#define _ARCH_NRF52810_H_

#define MACH_PART NRF52810
#define NRF52810

#include "nrf52810_description.h"
#include "nrf52810_bitfields.h"

#endif /* _ARCH_NRF52810_H_ */
