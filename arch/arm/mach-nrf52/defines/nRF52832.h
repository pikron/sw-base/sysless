/**
 * main header for CPU nRF52832
 */

#ifndef _ARCH_NRF52832_H_
#define _ARCH_NRF52832_H_

#define MACH_PART NRF52832
#define NRF52832

#include "nrf52832_description.h"
#include "nrf52832_bitfields.h"

#endif /* _ARCH_NRF52832_H_ */
