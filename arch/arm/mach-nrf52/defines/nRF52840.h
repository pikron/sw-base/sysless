/**
 * main header for CPU nRF52840
 */

#ifndef _ARCH_NRF52840_H_
#define _ARCH_NRF52840_H_

#define MACH_PART NRF52840
#define NRF52840

#define IRQn_SIZE    48  /* number of external IRQs */

#include "nrf52840_description.h"
#include "nrf52840_bitfields.h"

#endif /* _ARCH_NRF52840_H_ */
