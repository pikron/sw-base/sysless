#include <stdint.h>
#include <string.h>
#include <hal_machperiph.h>

extern char _eeasydma;

void *rambuf_reserve(unsigned long size)
{
  static char *rambuf_next=&_eeasydma;
  char *rambuf_prev;

  if(!rambuf_next)
    return NULL;

  rambuf_prev=(char *)rambuf_next; // 1 byte alignment
  rambuf_next += ((size+3)>>2)<<2;

  return rambuf_prev;
}
