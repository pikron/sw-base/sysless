/****************************************************************************//**
 * @file :    startup.c
 * @brief :   CMSIS Cortex-M4 Core Startup File for nRF528xx
 * @version : V1.0
 * @date :    2018/08
 *
 ******************************************************************************/

// Mod by nio for the .fastcode part

#include "cpu_def.h"
#include "nRF52.h"

#define WEAK __attribute__ ((weak))
//*****************************************************************************
//
// Forward declaration of the default fault handlers.
//
//*****************************************************************************
/* System exception vector handler */
void WEAK   Reset_Handler(void);             /* Reset Handler */
void WEAK   NMI_Handler(void);               /* NMI Handler */
void WEAK   HardFault_Handler(void);         /* Hard Fault Handler */
void WEAK   MemoryManagement_Handler(void);  /* Memory Management */
void WEAK   BusFault_Handler(void);          /*  */
void WEAK   UsageFault_Handler(void);        /*  */
void WEAK   SVC_Handler(void);               /* SVCall Handler */
void WEAK   DebugMon_Handler(void);          /*  */
void WEAK   PendSV_Handler(void);            /* PendSV Handler */
void WEAK   SysTick_Handler(void);           /* SysTick Handler */

/* External interrupt vector handler */
void WEAK   PWR_CLK_IRQHandler(void);   /*!<   0  POWER_CLOCK */
void WEAK   RADIO_IRQHandler(void);     /*!<   1  RADIO */
void WEAK   UART0_IRQHandler(void);     /*!<   2  UART0 */
void WEAK   SPI0_TWI0_IRQHandler(void); /*!<   3  SPI0_TWI0 (SPIM/SPIS/SPI/TWI) */
void WEAK   SPI1_TWI1_IRQHandler(void); /*!<   4  SPI1_TWI1 (SPIM/SPIS/SPI/TWI) */
void WEAK   NFCT_IRQHandler(void);      /*!<   5  NFC */
void WEAK   GPIOTE_IRQHandler(void);    /*!<   6  GPIOTE */
//void WEAK   ADC_IRQHandler(void);       /*!<   7  ADC */
void WEAK   SAADC_IRQHandler(void);     /*!<   7  ADC */
void WEAK   TIMER0_IRQHandler(void);    /*!<   8  TIMER0 */
void WEAK   TIMER1_IRQHandler(void);    /*!<   9  TIMER1 */
void WEAK   TIMER2_IRQHandler(void);    /*!<  10  TIMER2 */
void WEAK   RTC0_IRQHandler(void);      /*!<  11  RTC0 */
void WEAK   TEMP_IRQHandler(void);      /*!<  12  TEMP */
void WEAK   RNG_IRQHandler(void);       /*!<  13  RNG */
void WEAK   ECB_IRQHandler(void);       /*!<  14  ECB */
void WEAK   CCM_AAR_IRQHandler(void);   /*!<  15  CCM_AAR */
void WEAK   WDT_IRQHandler(void);       /*!<  16  WDT */
void WEAK   RTC1_IRQHandler(void);      /*!<  17  RTC1 */
void WEAK   QDEC_IRQHandler(void);      /*!<  18  QDEC */
void WEAK   LPCOMP_IRQHandler(void);    /*!<  19  LPCOMP */
void WEAK   SWI0_IRQHandler(void);      /*!<  20  SWI0 */
void WEAK   SWI1_IRQHandler(void);      /*!<  21  SWI1 */
void WEAK   SWI2_IRQHandler(void);      /*!<  22  SWI2 */
void WEAK   SWI3_IRQHandler(void);      /*!<  23  SWI3 */
void WEAK   SWI4_IRQHandler(void);      /*!<  24  SWI4 */
void WEAK   SWI5_IRQHandler(void);      /*!<  25  SWI5 */
void WEAK   TIMER3_IRQHandler(void);    /*!<  26  TIMER3 */
void WEAK   TIMER4_IRQHandler(void);    /*!<  27  TIMER4 */
void WEAK   PWM0_IRQHandler(void);      /*!<  28  PWM0 */
void WEAK   PDM_IRQHandler(void);       /*!<  29  PDM */
void WEAK   MWU_IRQHandler(void);       /*!<  32  */
void WEAK   PWM1_IRQHandler(void);      /*!<  33  */
void WEAK   PWM2_IRQHandler(void);      /*!<  34  */
void WEAK   SPI2_IRQHandler(void);      /*!<  35  (SPIM/SPIS/SPI) */
void WEAK   RTC2_IRQHandler(void);      /*!<  36  */
void WEAK   I2S_IRQHandler(void);       /*!<  37  */
void WEAK   FPU_IRQHandler(void);       /*!<  38  */
void WEAK   USBD_IRQHandler(void);      /*!<  39  */
void WEAK   UARTE1_IRQHandler(void);    /*!<  40  */
void WEAK   QSPI_IRQHandler(void);      /*!<  41  */
void WEAK   CRYPTOCELL_IRQHandler(void);  /*!< 42 */
void WEAK   PWM3_IRQHandler(void);      /*!<  45  */
void WEAK   SPIM3_IRQHandler(void);     /*!<  47  (SPIM) */

void WEAK   __bbconf_pt_magic(void);
void WEAK   __bbconf_pt_addr(void);

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
extern unsigned long _etext;
extern unsigned long _sidata;   /* start address for the initialization values of the .data section. defined in linker script */
extern unsigned long _sdata;    /* start address for the .data section. defined in linker script */
extern unsigned long _edata;    /* end address for the .data section. defined in linker script */

extern unsigned long _sifastcode;   /* start address for the initialization values of the .fastcode section. defined in linker script */
extern unsigned long _sfastcode;    /* start address for the .fastcode section. defined in linker script */
extern unsigned long _efastcode;    /* end address for the .fastcode section. defined in linker script */

extern unsigned long _sbss;     /* start address for the .bss section. defined in linker script */
extern unsigned long _ebss;     /* end address for the .bss section. defined in linker script */

extern void _estack;    /* init value for the stack pointer. defined in linker script */

extern void (_setup_board)(void);         /* setup_board adress function */
//void WEAK   _setup_board(void);

extern unsigned long _mem_app_start;

#if NRF_USE_USER_ASAP_PROC
  extern void hal_user_asap_proc(void);
#endif /* NRF_USE_USER_ASAP_PROC */


/* Private typedef -----------------------------------------------------------*/
/* function prototypes ------------------------------------------------------*/
void Reset_Handler(void) __attribute__((__interrupt__));
extern int main(void);

typedef void (*FNC)(void);
FNC fnc_entry;

void HardFault_Handler(void) __attribute__((__interrupt__));

/******************************************************************************
*
* The minimal vector table for a Cortex M4.  Note that the proper constructs
* must be placed on this to ensure that it ends up at physical address
* 0x0000.0000.
*
******************************************************************************/

extern unsigned long _stack;

__attribute__ ((section(".isr_vector")))
void (* const g_pfnVectors[])(void) = {
  (void (*)(void))&_stack,    /* The initial stack pointer */
  Reset_Handler,              /* Reset Handler */
  NMI_Handler,                /* NMI Handler */
  HardFault_Handler,          /* Hard Fault Handler */
  MemoryManagement_Handler,   /* Memory Management */
  BusFault_Handler,           /*  */
  UsageFault_Handler,         /*  */
  0,                          /* Reserved */
  __bbconf_pt_magic,          /* Reserved or BBCONF_MAGIC_ADDR */
  __bbconf_pt_addr,           /* Reserved or BBCONF_PTPTR_ADDR */
  0,                          /* Reserved */
  SVC_Handler,                /* SVCall Handler */
  DebugMon_Handler,           /*  */
  0,                          /* Reserved */
  PendSV_Handler,             /* PendSV Handler */
  SysTick_Handler,            /* SysTick Handler */

  // External Interrupts
  PWR_CLK_IRQHandler,         /*!<   0  POWER_CLOCK */
  RADIO_IRQHandler,           /*!<   1  RADIO */
  UART0_IRQHandler,           /*!<   2  UART0 */
  SPI0_TWI0_IRQHandler,       /*!<   3  SPI0_TWI0 */
  SPI1_TWI1_IRQHandler,       /*!<   4  SPI1_TWI1 */
  NFCT_IRQHandler,            /*!<   5  NFC */
  GPIOTE_IRQHandler,          /*!<   6  GPIOTE */
  SAADC_IRQHandler,           /*!<   7  ADC */
  TIMER0_IRQHandler,          /*!<   8  TIMER0 */
  TIMER1_IRQHandler,          /*!<   9  TIMER1 */
  TIMER2_IRQHandler,          /*!<  10  TIMER2 */
  RTC0_IRQHandler,            /*!<  11  RTC0 */
  TEMP_IRQHandler,            /*!<  12  TEMP */
  RNG_IRQHandler,             /*!<  13  RNG */
  ECB_IRQHandler,             /*!<  14  ECB */
  CCM_AAR_IRQHandler,         /*!<  15  CCM_AAR */
  WDT_IRQHandler,             /*!<  16  WDT */
  RTC1_IRQHandler,            /*!<  17  RTC1 */
  QDEC_IRQHandler,            /*!<  18  QDEC */
  LPCOMP_IRQHandler,          /*!<  19  LPCOMP */
  SWI0_IRQHandler,            /*!<  20  SWI0 */
  SWI1_IRQHandler,            /*!<  21  SWI1 */
  SWI2_IRQHandler,            /*!<  22  SWI2 */
  SWI3_IRQHandler,            /*!<  23  SWI3 */
  SWI4_IRQHandler,            /*!<  24  SWI4 */
  SWI5_IRQHandler,            /*!<  25  SWI5 */
  TIMER3_IRQHandler,          /*!<  26  TIMER3 */
  TIMER4_IRQHandler,          /*!<  27  TIMER4 */
  PWM0_IRQHandler,            /*!<  28  PWM0 */
  PDM_IRQHandler,             /*!<  29  PDM */
  0,                          /* Reserved */
  0,                          /* Reserved */
  MWU_IRQHandler,
  PWM1_IRQHandler,
  PWM2_IRQHandler,
  SPI2_IRQHandler,
  RTC2_IRQHandler,
  I2S_IRQHandler,
  FPU_IRQHandler,
  USBD_IRQHandler,
  UARTE1_IRQHandler,
  QSPI_IRQHandler,
  CRYPTOCELL_IRQHandler,
  0,                          /* Reserved */
  0,                          /* Reserved */
  PWM3_IRQHandler,
  0,                          /* Reserved */
  SPIM3_IRQHandler,
};

/*******************************************************************************
* Function Name  : Reset_Handler
* Description    : This is the code that gets called when the processor first starts execution
*          following a reset event.  Only the absolutely necessary set is performed,
*          after which the application supplied main() routine is called.
* Input          :
* Output         :
* Return         :
*******************************************************************************/

void Reset_Handler(void)
{
  unsigned long *pulDest;
  unsigned long *pulSrc;

#if NRF_USE_USER_ASAP_PROC
  hal_user_asap_proc();
#endif /* NRF_USE_USER_ASAP_PROC */
  /* disable all external IRQs */
#if IRQn_SIZE<32
  NVIC->ICER[0] = (1<<IRQn_SIZE)-1;
#else
  NVIC->ICER[0] = 0xffffffff;
  NVIC->ICER[1] = (1<<(IRQn_SIZE-32))-1;
#endif

  // enable FPU if present and used
#if (__FPU_PRESENT == 1U) && (__FPU_USED == 1U)
  __ASM volatile ("LDR.W   R0, =0xE000ED88");       // ; CPACR is located at address 0xE000ED88
  __ASM volatile ("LDR     R1, [R0]");              // ; Read CPACR
  __ASM volatile ("ORR     R1, R1, #(0xF << 20)");  // ; Set bits 20-23 to enable CP10 and CP11 coprocessors
  __ASM volatile ("STR     R1, [R0]");              // ; Write back the modified value to the CPACR
  __ASM volatile ("DSB");                           // ; wait for store to complete
  __ASM volatile ("ISB");                           // ;reset pipeline now the FPU is enabled
#endif /* FPU */

  //
  // Copy the data segment initializers from flash to SRAM in ROM mode
  //
  if (&_sidata != &_sdata) {  // only if needed
    pulSrc = &_sidata;
    for(pulDest = &_sdata; pulDest < &_edata; ) {
      *(pulDest++) = *(pulSrc++);
    }
  }

  // Copy the .fastcode code from ROM to SRAM
  if (&_sifastcode != &_sfastcode) {  // only if needed
    pulSrc = &_sifastcode;
    for(pulDest = &_sfastcode; pulDest < &_efastcode; ) {
      *(pulDest++) = *(pulSrc++);
    }
  }

  //
  // Zero fill the bss segment.
  //
  for(pulDest = &_sbss; pulDest < &_ebss; )
  {
    *(pulDest++) = 0;
  }

  // copy initial values and set irq table
  // TODO: pridat podporu pro vzdalenou tabulku v RAM
//    if(irq_handler_table && irq_table_size) {
//        int i;
//        pulSrc = (unsigned long*)g_pfnVectors;
//        pulDest = (unsigned long*)irq_handler_table;
//        for(i = irq_table_size; i--; ) {
//            *(pulDest++) = *(pulSrc++);
//        }
//        /*SCB->VTOR=0x10000000;*/
//        SCB->VTOR=(uint32_t)irq_handler_table;
//    }

  //if (_setup_board!=0)
  _setup_board();

  //
  // Call the application's entry point.
  //
  main();

  /* disable interrupts */
#if IRQn_SIZE<32
  NVIC->ICER[0] = (1<<IRQn_SIZE)-1;
#else
  NVIC->ICER[0] = 0xffffffff;
  NVIC->ICER[0] = (1<<(IRQn_SIZE-32))-1;
#endif
  __set_MSP(*(unsigned long *)(&_mem_app_start));         /* Stack pointer */
  fnc_entry = (FNC)*(unsigned long *)(&_mem_app_start+1); /* Reset handler */
  fnc_entry();
}

//*****************************************************************************
//
// Provide weak aliases for each Exception handler to the Default_Handler.
// As they are weak aliases, any function with the same name will override
// this definition.
//
//*****************************************************************************
#pragma weak NMI_Handler        = Default_Handler           /* NMI Handler */
//#pragma weak HardFault_Handler  = Default_Handler           /* Hard Fault Handler */
#pragma weak MemoryManagement_Handler = Default_Handler     /* Memory Management */
#pragma weak BusFault_Handler   = Default_Handler           /*  */
#pragma weak UsageFault_Handler = Default_Handler           /*  */
#pragma weak SVC_Handler        = Default_Handler           /* SVCall Handler */
#pragma weak DebugMon_Handler   = Default_Handler           /*  */
#pragma weak PendSV_Handler     = Default_Handler           /* PendSV Handler */
#pragma weak SysTick_Handler    = Default_Handler           /* SysTick Handler */

/* External interrupt vector handler */
#pragma weak PWR_CLK_IRQHandler   = Default_Handler     /*!<   0  POWER_CLOCK */
#pragma weak RADIO_IRQHandler     = Default_Handler     /*!<   1  RADIO */
#pragma weak UART0_IRQHandler     = Default_Handler     /*!<   2  UART0 */
#pragma weak SPI0_TWI0_IRQHandler = Default_Handler     /*!<   3  SPI0_TWI0 */
#pragma weak SPI1_TWI1_IRQHandler = Default_Handler     /*!<   4  SPI1_TWI1 */
#pragma weak NFCT_IRQHandler      = Default_Handler     /*!<   5  NFC */
#pragma weak GPIOTE_IRQHandler    = Default_Handler     /*!<   6  GPIOTE */
#pragma weak SAADC_IRQHandler     = Default_Handler     /*!<   7  ADC */
#pragma weak TIMER0_IRQHandler    = Default_Handler     /*!<   8  TIMER0 */
#pragma weak TIMER1_IRQHandler    = Default_Handler     /*!<   9  TIMER1 */
#pragma weak TIMER2_IRQHandler    = Default_Handler     /*!<  10  TIMER2 */
#pragma weak RTC0_IRQHandler      = Default_Handler     /*!<  11  RTC0 */
#pragma weak TEMP_IRQHandler      = Default_Handler     /*!<  12  TEMP */
#pragma weak RNG_IRQHandler       = Default_Handler     /*!<  13  RNG */
#pragma weak ECB_IRQHandler       = Default_Handler     /*!<  14  ECB */
#pragma weak CCM_AAR_IRQHandler   = Default_Handler     /*!<  15  CCM_AAR */
#pragma weak WDT_IRQHandler       = Default_Handler     /*!<  16  WDT */
#pragma weak RTC1_IRQHandler      = Default_Handler     /*!<  17  RTC1 */
#pragma weak QDEC_IRQHandler      = Default_Handler     /*!<  18  QDEC */
#pragma weak LPCOMP_IRQHandler    = Default_Handler     /*!<  19  LPCOMP */
#pragma weak SWI0_IRQHandler      = Default_Handler     /*!<  20  SWI0 */
#pragma weak SWI1_IRQHandler      = Default_Handler     /*!<  21  SWI1 */
#pragma weak SWI2_IRQHandler      = Default_Handler     /*!<  22  SWI2 */
#pragma weak SWI3_IRQHandler      = Default_Handler     /*!<  23  SWI3 */
#pragma weak SWI4_IRQHandler      = Default_Handler     /*!<  24  SWI4 */
#pragma weak SWI5_IRQHandler      = Default_Handler     /*!<  25  SWI5 */
#pragma weak TIMER3_IRQHandler    = Default_Handler     /*!<  26  TIMER3 */
#pragma weak TIMER4_IRQHandler    = Default_Handler     /*!<  27  TIMER4 */
#pragma weak PWM0_IRQHandler      = Default_Handler     /*!<  28  PWM0 */
#pragma weak PDM_IRQHandler       = Default_Handler     /*!<  29  PDM */
#pragma weak MWU_IRQHandler       = Default_Handler     /*!<  32  */
#pragma weak PWM1_IRQHandler      = Default_Handler     /*!<  33  */
#pragma weak PWM2_IRQHandler      = Default_Handler     /*!<  34  */
#pragma weak SPI2_IRQHandler      = Default_Handler     /*!<  35  */
#pragma weak RTC2_IRQHandler      = Default_Handler     /*!<  36  */
#pragma weak I2S_IRQHandler       = Default_Handler     /*!<  37  */
#if !((__FPU_PRESENT == 1U) && (__FPU_USED == 1U))
  #pragma weak FPU_IRQHandler       = Default_Handler     /*!<  38  */
#endif /* no FPU */
#pragma weak USBD_IRQHandler      = Default_Handler     /*!<  39  */
#pragma weak UARTE1_IRQHandler    = Default_Handler     /*!<  40  */
#pragma weak QSPI_IRQHandler      = Default_Handler     /*!<  41  */
#pragma weak CRYPTOCELL_IRQHandler = Default_Handler    /*!< 42 */
#pragma weak PWM3_IRQHandler      = Default_Handler     /*!<  45  */
#pragma weak SPIM3_IRQHandler     = Default_Handler     /*!<  47  */

//*****************************************************************************
//
// This is the code that gets called when the processor receives an unexpected
// interrupt.  This simply enters an infinite loop, preserving the system state
// for examination by a debugger.
//
//*****************************************************************************
#if defined(CONFIG_DEFEXCEPT_LEDACT) && CONFIG_DEFEXCEPT_LEDACT
  #define DEFEXCEPT_LEDFUNC   OUTSET
  #define DEFEXCEPT_LEDFUNC2  OUTCLR
#else
  #define DEFEXCEPT_LEDFUNC   OUTCLR
  #define DEFEXCEPT_LEDFUNC2  OUTSET
#endif

void Default_Handler(void) {
  uint32_t ai0 = NVIC->IABR[0], ai1 = NVIC->IABR[1];
  #ifdef CONFIG_DEFEXCEPT_LEDPIN
    #if CONFIG_DEFEXCEPT_LEDPIN>=32
      NRF_P1->DEFEXCEPT_LEDFUNC = 1<<(CONFIG_DEFEXCEPT_LEDPIN-32);
    #else
      NRF_P0->DEFEXCEPT_LEDFUNC = 1<<(CONFIG_DEFEXCEPT_LEDPIN);
    #endif
  #endif
  // Go into an infinite loop.
  //
  while (1) {
    for(int i = 0;i<ai0;i++) {
  #ifdef CONFIG_DEFEXCEPT_LEDPIN
    #if CONFIG_DEFEXCEPT_LEDPIN>=32
      NRF_P1->OUTCLR = 1<<(CONFIG_DEFEXCEPT_LEDPIN-32);
    #else
      NRF_P0->OUTCLR = 1<<(CONFIG_DEFEXCEPT_LEDPIN);
    #endif
  #endif
    }
    for(int i = 0;i<ai1;i++) {
  #ifdef CONFIG_DEFEXCEPT_LEDPIN
    #if CONFIG_DEFEXCEPT_LEDPIN>=32
      NRF_P1->OUTSET = 1<<(CONFIG_DEFEXCEPT_LEDPIN-32);
    #else
      NRF_P0->OUTSET = 1<<(CONFIG_DEFEXCEPT_LEDPIN);
    #endif
  #endif
    }
  }
}

void HardFault_Handler(void) {
#ifdef CONFIG_DEFEXCEPT_LEDPIN
  uint32_t d = 0;
  while (1) {
    d++;
    if (d>=0x20000000) {
      d = 0;
      #if CONFIG_DEFEXCEPT_LEDPIN>=32
        NRF_P1->DEFEXCEPT_LEDFUNC2 = 1<<(CONFIG_DEFEXCEPT_LEDPIN-32);
      #else
        NRF_P0->DEFEXCEPT_LEDFUNC2 = 1<<(CONFIG_DEFEXCEPT_LEDPIN);
      #endif
    } else if (d==0x10000000) {
      #if CONFIG_DEFEXCEPT_LEDPIN>=32
        NRF_P1->DEFEXCEPT_LEDFUNC = 1<<(CONFIG_DEFEXCEPT_LEDPIN-32);
      #else
        NRF_P0->DEFEXCEPT_LEDFUNC = 1<<(CONFIG_DEFEXCEPT_LEDPIN);
      #endif
    }
  }
#else
  while(1) {
  }
#endif
}

//#pragma weak _setup_board = default_setup_board                /* default setup_board */

//void default_setup_board(void)
//{
//  // nothing to do
//}
