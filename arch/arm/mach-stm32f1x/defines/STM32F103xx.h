/**
 * stm32f103xx.h - common header for including to projects
 */

#ifndef _STM32F103XX_HEADER_FILE_
#define _STM32F103XX_HEADER_FILE_

#define IRQn_SIZE    43  /* number of external IRQs */

/* stm32f103xx belongs to STM32F10X_MD group */
#define STM32F10X_MD

#include "stm32f10x.h"
#include "stm32f10x_bitfields.h"

/* memory map and list of peripherals in STM32F103xx (according to informations in datasheet for STM32F103CB )
 *
 *    Address      Peripheral
 * ---------  Timers  ----------------------------------------------------------
 * 0x4001 2C00     TIM1 - Advanced-control timer
 * 0x4000 0000     TIM2 - General-purpose timer
 * 0x4000 0400     TIM3 - General-purpose timer
 * 0x4000 0800     TIM4 - General-purpose timer
 * 0x4000 2800     RTC  - Real-time clock
 * 0x4000 3000     IWDG - Independent watchdog
 * 0x4000 2C00     WWDG - Window watchdog
 * ---------  GPIOs  -----------------------------------------------------------
 * 0x4001 0800     Port A - General-purpose I/Os A
 * 0x4001 0C00     Port B - General-purpose I/Os B
 * 0x4001 1000     Port C - General-purpose I/Os C
 * 0x4001 1400     Port D - General-purpose I/Os D
 * 0x4001 1800     Port E - General-purpose I/Os E
 * 0x4001 0000     AFIO - Alternate-function I/Os
 * 0x4001 0400     EXTI - External Interrupts
 * ---------  Analog-to-digital converter---------------------------------------
 * 0x4001 2400     ADC1 - Analog-to-digital converter
 * 0x4001 2800     ADC2 - Analog-to-digital converter
 * ---------  Communication  ---------------------------------------------------
 * 0x4001 3800     USART1 - Universal synchronous asynchronous receiver transmitter
 * 0x4000 4400     USART2 - Universal synchronous asynchronous receiver transmitter
 * 0x4000 4800     USART3 - Universal synchronous asynchronous receiver transmitter
 * 0x4001 3000     SPI1 - Serial peripheral interface
 * 0x4000 3800     SPI2 - Serial peripheral interface
 * 0x4000 5400     I2C1 - Inter-integrated circuit interface
 * 0x4000 5800     I2C2 - Inter-integrated circuit interface
 * 0x4000 6400     CAN1 (bxCAN) - Basec Extended CAN
 * 0x4000 5C00     USB  - USB registers
 * ( 0x4000 6000     shared 512 byte USB/CAN SRAM )
 * ---------  Others  ---------------------------------------------------
 * 0x4002 0000     DMA - Direct memory access controller
 * 0x4000 6C00     BKP - Backup registers
 * 0x4000 7000     PWR - Power control
 * 0x4002 1000     RCC - Reset and clock control
 * 0x4002 2000     Flash Interface
 * 0x4002 3000     CRC - CRC calculation unit
 * 
 */


#endif /* _STM32F103XX_HEADER_FILE_ */
