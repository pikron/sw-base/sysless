#include <cpu_def.h>
#include <STM32F103xx.h>
#include <hal_gpio.h>

int hal_pin_conf_set(unsigned gpio, int conf)
{
  uint32_t val;
  uint32_t msk1, msk2;
  gpio &= ~PORT_CONF_MASK;
  unsigned half = hal_gpio_get_port_num(gpio)>>(HAL_GPIO_PORT_PINS_BITS-1); 
  __IO uint32_t *pset = &(hal_gpio_get_base(gpio)->CRL);
  pset += half;
  unsigned pinbit = 4 * ( gpio & (PORT_PIN_IDX_MASK>>1) );

  val = *pset;
  msk1 = ~(0x0f<<pinbit);
  msk2 = (((conf & PORT_CONF_FULLSETTINGS_MASK)>>PORT_CONF_FULLSETTINGS_SHIFT)<<pinbit);
  // set direction, mode, open-drain at once
  *pset = (val & msk1) | msk2;
  // set pull up/down for input-pupd or init high/low for gpio
  if (((conf & PORT_CONF_DIR_MASK)!=PORT_CONF_DIR_IN) ||
      (((conf & PORT_CONF_DIR_MASK)==PORT_CONF_DIR_IN) &&
       ((conf & PORT_CONF_INMODE_MASK)==PORT_CONF_INMODE_PUPD)) ) {
    if ((conf & PORT_CONF_INPUPD_MASK) == PORT_CONF_INPUPD_UP)
      hal_gpio_get_base(gpio)->ODR |= (1<<(gpio & PORT_PIN_IDX_MASK));
    else
      hal_gpio_get_base(gpio)->ODR &= ~(1<<(gpio & PORT_PIN_IDX_MASK));
  }

  return 0;
}
