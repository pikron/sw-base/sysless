#ifndef _HAL_GPIO_H_
#define _HAL_GPIO_H_

#include <cpu_def.h>
#include <STM32F103xx.h>
#include <hal_gpio_def.h>

static inline
GPIO_TypeDef *hal_gpio_get_portidx_base(unsigned pidx)
{
  char *p = (char*)GPIOA_BASE;
  p += ((char*)GPIOB_BASE - (char*)GPIOA_BASE) * pidx;
  return (GPIO_TypeDef *)p;
}

static inline
unsigned hal_gpio_get_port_num(unsigned gpio)
{
  return ((gpio & PORT_IDX_MASK) >> PORT_SHIFT);
}

static inline
GPIO_TypeDef *hal_gpio_get_base(unsigned gpio)
{
  return hal_gpio_get_portidx_base(hal_gpio_get_port_num(gpio));
}

static inline
int hal_gpio_get_value(unsigned gpio)
{
  return ((hal_gpio_get_base(gpio)->IDR) >> (gpio & PORT_PIN_IDX_MASK)) & 1;
}

static inline
void hal_gpio_set_value(unsigned gpio, int value)
{
  if(value)
    hal_gpio_get_base(gpio)->BSRR = 1 << (gpio & PORT_PIN_IDX_MASK);
  else
    hal_gpio_get_base(gpio)->BRR = 1 << (gpio & PORT_PIN_IDX_MASK);
}

int hal_pin_conf_set(unsigned gpio, int conf);

static inline
int hal_pin_conf(unsigned gpio)
{
  return hal_pin_conf_set(gpio, gpio);
}

#endif /*_HAL_GPIO_H_*/
