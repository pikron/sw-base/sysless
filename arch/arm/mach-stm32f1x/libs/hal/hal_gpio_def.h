#ifndef _HAL_GPIO_DEF_H_
#define _HAL_GPIO_DEF_H_

/* PORT CONF = | cfg[31:16] | xxx | port_idx [6:4] | pin_idx [3:0] | */
#define HAL_GPIO_PORT_BITS 3        /* max. 8 ports */
#define HAL_GPIO_PORT_PINS_BITS 4   /* each port has 16 pins maximally */

#ifndef PORT_SHIFT
  #define PORT_SHIFT          HAL_GPIO_PORT_PINS_BITS
#endif

#ifndef PORT_IDX_MASK
  #define PORT_IDX_MASK       (((1<<HAL_GPIO_PORT_BITS)-1)<<HAL_GPIO_PORT_PINS_BITS)
#endif

#ifndef PORT_PIN_IDX_MASK
  #define PORT_PIN_IDX_MASK       ((1<<HAL_GPIO_PORT_PINS_BITS)-1)
#endif


#ifndef PORT_PIN
  #define PORT_PIN(p,n,conf)  (((p)<<PORT_SHIFT) | (n) | (conf))
  #define PORT_CONF_MASK      0xffff0000
#endif

#define PORT_CONF_FULLSETTINGS_MASK   0x000f0000
#define PORT_CONF_FULLSETTINGS_SHIFT  16

#define PORT_CONF_DIR_MASK      0x00030000
#define PORT_CONF_DIR_IN        0x00000000
#define PORT_CONF_DIR_OUT_10MHZ 0x00010000
#define PORT_CONF_DIR_OUT_2MHZ  0x00020000
#define PORT_CONF_DIR_OUT_50MHZ 0x00030000

// open drain = CNF0
//#define PORT_CONF_OD_MASK   0x01000000
//#define PORT_CONF_OD_OFF    0x00000000  // push-pull output or analog input
//#define PORT_CONF_OD_ON     0x01000000  // open-drain output or floating input

// output mode - GPIO/alternate function (CNF[1:0])
#define PORT_CONF_OUTMODE_MASK      0x000c0000
#define PORT_CONF_OUTMODE_GPIO_PDPU 0x00000000 // GPIO push-pull
#define PORT_CONF_OUTMODE_GPIO_OD   0x00040000 // GPIO open drain
#define PORT_CONF_OUTMODE_AFNC_PDPU 0x00080000 // Alternate function push-pull
#define PORT_CONF_OUTMODE_AFNC_OD   0x000c0000 // Alternate function open drain
// input mode - Analog/digital input (CNF[1:0])
#define PORT_CONF_INMODE_MASK       0x000c0000
#define PORT_CONF_INMODE_ADC        0x00000000 // Analog input
#define PORT_CONF_INMODE_FLOAT      0x00040000 // Floating input
#define PORT_CONF_INMODE_PUPD       0x00080000 // Pull down/up according to ODR register
// input pull up/ pull down
#define PORT_CONF_INPUPD_MASK       0x10000000
#define PORT_CONF_INPUPD_DOWN       0x00000000
#define PORT_CONF_INPUPD_UP         0x10000000
// output initial high/low state
#define PORT_CONF_INIT_MASK 0x10000000
#define PORT_CONF_INIT_LOW  0x00000000
#define PORT_CONF_INIT_HIGH 0x10000000


#define PORT_CONF_ADC_IN     (PORT_CONF_DIR_IN | PORT_CONF_INMODE_ADC)

#define PORT_CONF_GPIO_IN    (PORT_CONF_DIR_IN | PORT_CONF_INMODE_FLOAT)
#define PORT_CONF_GPIO_IN_PU (PORT_CONF_DIR_IN | PORT_CONF_INMODE_PUPD | PORT_CONF_INPUPD_UP)
#define PORT_CONF_GPIO_IN_PD (PORT_CONF_DIR_IN | PORT_CONF_INMODE_PUPD | PORT_CONF_INPUPD_DOWN)

#define PORT_CONF_GPIO_OUT_LO    (PORT_CONF_DIR_OUT_10MHZ | PORT_CONF_OUTMODE_GPIO_PDPU | PORT_CONF_INIT_LOW)
#define PORT_CONF_GPIO_OUT_HI    (PORT_CONF_DIR_OUT_10MHZ | PORT_CONF_OUTMODE_GPIO_PDPU | PORT_CONF_INIT_HIGH)
#define PORT_CONF_GPIO_OUT_LO_OD (PORT_CONF_DIR_OUT_10MHZ | PORT_CONF_OUTMODE_GPIO_OD | PORT_CONF_INIT_LOW)
#define PORT_CONF_GPIO_OUT_HI_OD (PORT_CONF_DIR_OUT_10MHZ | PORT_CONF_OUTMODE_GPIO_OD | PORT_CONF_INIT_HIGH)

#define PORT_CONF_AFNC       (PORT_CONF_DIR_OUT_10MHZ | PORT_CONF_OUTMODE_AFNC_PDPU)
#define PORT_CONF_AFNC_OD    (PORT_CONF_DIR_OUT_10MHZ | PORT_CONF_OUTMODE_AFNC_OD)


#endif /*_HAL_GPIO_H_*/
