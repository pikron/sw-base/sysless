
#include <system_def.h>
#include <cpu_def.h>
#include <hal_machperiph.h>

///*----------------------------------------------------------------------------
//  Clock Variable definitions
// *----------------------------------------------------------------------------*/
//uint32_t SystemCoreClock = __CORE_CLK;/*!< System Clock Frequency (Core Clock)*/
//uint32_t PeripheralClock = __PER_CLK; /*!< Peripheral Clock Frequency (Pclk)  */
//uint32_t EMCClock		 = __EMC_CLK; /*!< EMC Clock Frequency 				  */
//uint32_t USBClock 		 = (48000000UL);		  /*!< USB Clock Frequency - this value will
//									be updated after call SystemCoreClockUpdate, should be 48MHz*/

//unsigned int system_frequency = __CORE_CLK;/*!< System Clock Frequency (Core Clock)*/
//unsigned int peripheral_frequency = __PER_CLK;/*!< Peripheral Clock Frequency (Pclk)  */



// uses IWDG
void stm_watchdog_feed()
{
  unsigned long flags;

  save_and_cli(flags);
  IWDG->KR = 0xAAAA;
  restore_flags(flags);
}
// STM32F1x uses 40kHz clock input
void stm_watchdog_init(int on,int timeout_ms)
{
  int chck, pr = 0;
  if (!on) return;
  IWDG->KR = 0x5555; /* enable access to PR and RLR */
  for (chck=0x1000;chck<=0x40000;chck=chck<<1) {
    if (10*timeout_ms<chck) {
      IWDG->PR = pr;
      IWDG->RLR = timeout_ms>>pr;
      break;
    }
    pr++;
  }
  if (pr>6) return;   /* error - required timeout is out of range */

  IWDG->KR = 0xCCCC; /* start watchdog */
}
