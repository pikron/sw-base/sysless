#ifndef _HAL_MACHPERIPH_H
#define _HAL_MACHPERIPH_H

//extern unsigned int system_frequency;
#define system_frequency SystemCoreClock
//extern unsigned int peripheral_frequency;
#define PCLK peripheral_frequency

//#define system_clock_init SetSysClock
//#define system_clock_update_info SystemCoreClockUpdate

/* STM32 has registers for enabling/disabling/reseting individual peripherals */
/* defined all peripherals from all STM32F1x series */
#define PERIPHERAL2_TIMER11  BIT(21)
#define PERIPHERAL2_TIMER10  BIT(20)
#define PERIPHERAL2_TIMER9   BIT(19)
#define PERIPHERAL2_ADC3     BIT(15)
#define PERIPHERAL2_USART1   BIT(14)
#define PERIPHERAL2_TIMER8   BIT(13)
#define PERIPHERAL2_SPI1     BIT(12)
#define PERIPHERAL2_TIMER1   BIT(11)
#define PERIPHERAL2_ADC2     BIT(10)
#define PERIPHERAL2_ADC1     BIT(9)
#define PERIPHERAL2_GPIOG    BIT(8)
#define PERIPHERAL2_GPIOF    BIT(7)
#define PERIPHERAL2_GPIOS    BIT(6)
#define PERIPHERAL2_GPIOD    BIT(5)
#define PERIPHERAL2_GPIOC    BIT(4)
#define PERIPHERAL2_GPIOB    BIT(3)
#define PERIPHERAL2_GPIOA    BIT(2)
#define PERIPHERAL2_AFIO     BIT(0)

#define PERIPHERAL1_DAC      BIT(29)
#define PERIPHERAL1_PWR      BIT(28)
#define PERIPHERAL1_BKP      BIT(27)
#define PERIPHERAL1_CAN      BIT(25)
#define PERIPHERAL1_USB      BIT(23)
#define PERIPHERAL1_I2C2     BIT(22)
#define PERIPHERAL1_I2C1     BIT(21)
#define PERIPHERAL1_UART5    BIT(20)
#define PERIPHERAL1_UART4    BIT(19)
#define PERIPHERAL1_USART3   BIT(18)
#define PERIPHERAL1_USART2   BIT(17)
#define PERIPHERAL1_SPI3     BIT(15)
#define PERIPHERAL1_SPI2     BIT(14)
#define PERIPHERAL1_WWDG     BIT(11)
#define PERIPHERAL1_TIMER14  BIT(8)
#define PERIPHERAL1_TIMER13  BIT(7)
#define PERIPHERAL1_TIMER12  BIT(6)
#define PERIPHERAL1_TIMER7   BIT(5)
#define PERIPHERAL1_TIMER6   BIT(4)
#define PERIPHERAL1_TIMER5   BIT(3)
#define PERIPHERAL1_TIMER4   BIT(2)
#define PERIPHERAL1_TIMER3   BIT(1)
#define PERIPHERAL1_TIMER2   BIT(0)

static inline
void stm_enable_peripherals(uint32_t periph1, uint32_t periph2)
{
  if (periph1) RCC->APB1ENR |= periph1;
  if (periph2) RCC->APB2ENR |= periph2;
}
static inline
void stm_disable_peripherals(uint32_t periph1, uint32_t periph2)
{
  if (periph1) RCC->APB1ENR &= ~periph1;
  if (periph2) RCC->APB2ENR &= ~periph2;
}
static inline
void stm_reset_peripherals(uint32_t periph1, uint32_t periph2)
{
  if (periph1) {
    RCC->APB1RSTR |= periph1;
    RCC->APB1RSTR &= ~periph1;
  }
  if (periph2) {
    RCC->APB2RSTR |= periph2;
    RCC->APB2RSTR &= ~periph2;
  }
}

/* watchdog uses IWDG */
void stm_watchdog_init(int on,int timeout_ms);
void stm_watchdog_feed();

void *stm_reserve_usb_ram(unsigned long size);
void *stm_reserve_can_ram(unsigned long size);

#endif /* _HAL_MACHPERIPH_H */

