/****************************************************************************//**
 * @file :    startup_STM32F1 based on startup_LPC17xx.c
 ******************************************************************************/

// Mod by nio for the .fastcode part

#include "cpu_def.h"
#include "STM32F103xx.h"

#define WEAK __attribute__ ((weak))
//*****************************************************************************
//
// Forward declaration of the default fault handlers.
//
//*****************************************************************************
/* System exception vector handler */
void WEAK  Reset_Handler(void);             /* Reset Handler */
void WEAK  NMI_Handler(void);               /* NMI Handler */
void WEAK  HardFault_Handler(void);         /* Hard Fault Handler */
void WEAK  MemManage_Handler(void);         /* MPU Fault Handler */
void WEAK  BusFault_Handler(void);          /* Bus Fault Handler */
void WEAK  UsageFault_Handler(void);        /* Usage Fault Handler */
void WEAK  SVC_Handler(void);               /* SVCall Handler */
void WEAK  DebugMon_Handler(void);          /* Debug Monitor Handler */
void WEAK  PendSV_Handler(void);            /* PendSV Handler */
void WEAK  SysTick_Handler(void);           /* SysTick Handler */

/* External interrupt vector handler */
void WEAK  WWDG_IRQHandler(void);              /*!< Window WatchDog Interrupt                            */
void WEAK  PVD_IRQHandler(void);               /*!< PVD through EXTI Line detection Interrupt            */
void WEAK  TAMPER_IRQHandler(void);            /*!< Tamper Interrupt                                     */
void WEAK  RTC_IRQHandler(void);               /*!< RTC global Interrupt                                 */
void WEAK  FLASH_IRQHandler(void);             /*!< FLASH global Interrupt                               */
void WEAK  RCC_IRQHandler(void);               /*!< RCC global Interrupt                                 */
void WEAK  EXTI0_IRQHandler(void);             /*!< EXTI Line0 Interrupt                                 */
void WEAK  EXTI1_IRQHandler(void);             /*!< EXTI Line1 Interrupt                                 */
void WEAK  EXTI2_IRQHandler(void);             /*!< EXTI Line2 Interrupt                                 */
void WEAK  EXTI3_IRQHandler(void);             /*!< EXTI Line3 Interrupt                                 */
void WEAK  EXTI4_IRQHandler(void);             /*!< EXTI Line4 Interrupt                                 */
void WEAK  DMA1_Channel1_IRQHandler(void);     /*!< DMA1 Channel 1 global Interrupt                      */
void WEAK  DMA1_Channel2_IRQHandler(void);     /*!< DMA1 Channel 2 global Interrupt                      */
void WEAK  DMA1_Channel3_IRQHandler(void);     /*!< DMA1 Channel 3 global Interrupt                      */
void WEAK  DMA1_Channel4_IRQHandler(void);     /*!< DMA1 Channel 4 global Interrupt                      */
void WEAK  DMA1_Channel5_IRQHandler(void);     /*!< DMA1 Channel 5 global Interrupt                      */
void WEAK  DMA1_Channel6_IRQHandler(void);     /*!< DMA1 Channel 6 global Interrupt                      */
void WEAK  DMA1_Channel7_IRQHandler(void);     /*!< DMA1 Channel 7 global Interrupt                      */

void WEAK  ADC1_2_IRQHandler(void);            /*!< ADC1 and ADC2 global Interrupt                       */
void WEAK  USB_HP_CAN1_TX_IRQHandler(void);    /*!< USB Device High Priority or CAN1 TX Interrupts       */
void WEAK  USB_LP_CAN1_RX0_IRQHandler(void);   /*!< USB Device Low Priority or CAN1 RX0 Interrupts       */
void WEAK  CAN1_RX1_IRQHandler(void);          /*!< CAN1 RX1 Interrupt                                   */
void WEAK  CAN1_SCE_IRQHandler(void);          /*!< CAN1 SCE Interrupt                                   */
void WEAK  EXTI9_5_IRQHandler(void);           /*!< External Line[9:5] Interrupts                        */
void WEAK  TIM1_BRK_IRQHandler(void);          /*!< TIM1 Break Interrupt                                 */
void WEAK  TIM1_UP_IRQHandler(void);           /*!< TIM1 Update Interrupt                                */
void WEAK  TIM1_TRG_COM_IRQHandler(void);      /*!< TIM1 Trigger and Commutation Interrupt               */
void WEAK  TIM1_CC_IRQHandler(void);           /*!< TIM1 Capture Compare Interrupt                       */
void WEAK  TIM2_IRQHandler(void);              /*!< TIM2 global Interrupt                                */
void WEAK  TIM3_IRQHandler(void);              /*!< TIM3 global Interrupt                                */
void WEAK  TIM4_IRQHandler(void);              /*!< TIM4 global Interrupt                                */
void WEAK  I2C1_EV_IRQHandler(void);           /*!< I2C1 Event Interrupt                                 */
void WEAK  I2C1_ER_IRQHandler(void);           /*!< I2C1 Error Interrupt                                 */
void WEAK  I2C2_EV_IRQHandler(void);           /*!< I2C2 Event Interrupt                                 */
void WEAK  I2C2_ER_IRQHandler(void);           /*!< I2C2 Error Interrupt                                 */
void WEAK  SPI1_IRQHandler(void);              /*!< SPI1 global Interrupt                                */
void WEAK  SPI2_IRQHandler(void);              /*!< SPI2 global Interrupt                                */
void WEAK  USART1_IRQHandler(void);            /*!< USART1 global Interrupt                              */
void WEAK  USART2_IRQHandler(void);            /*!< USART2 global Interrupt                              */
void WEAK  USART3_IRQHandler(void);            /*!< USART3 global Interrupt                              */
void WEAK  EXTI15_10_IRQHandler(void);         /*!< External Line[15:10] Interrupts                      */
void WEAK  RTCAlarm_IRQHandler(void);          /*!< RTC Alarm through EXTI Line Interrupt                */
void WEAK  USBWakeUp_IRQHandler(void);         /*!< USB Device WakeUp from suspend through EXTI Line Interrupt */


void WEAK  __bbconf_pt_magic(void);
void WEAK  __bbconf_pt_addr(void);

/* Exported types --------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
extern unsigned long _etext;
extern unsigned long _sidata;   /* start address for the initialization values of the .data section. defined in linker script */
extern unsigned long _sdata;    /* start address for the .data section. defined in linker script */
extern unsigned long _edata;    /* end address for the .data section. defined in linker script */

extern unsigned long _sifastcode;   /* start address for the initialization values of the .fastcode section. defined in linker script */
extern unsigned long _sfastcode;    /* start address for the .fastcode section. defined in linker script */
extern unsigned long _efastcode;    /* end address for the .fastcode section. defined in linker script */

extern unsigned long _sbss;     /* start address for the .bss section. defined in linker script */
extern unsigned long _ebss;     /* end address for the .bss section. defined in linker script */

extern void _estack;    /* init value for the stack pointer. defined in linker script */


extern void (_setup_board)(void);         /* setup_board adress function */
//void WEAK   _setup_board(void);

extern unsigned long _mem_app_start;



/* Private typedef -----------------------------------------------------------*/
/* function prototypes ------------------------------------------------------*/
void Reset_Handler(void) __attribute__((__interrupt__));
extern int main(void);

typedef void (*FNC)(void);
FNC fnc_entry;

/******************************************************************************
*
* The minimal vector table for a Cortex M3.  Note that the proper constructs
* must be placed on this to ensure that it ends up at physical address
* 0x0000.0000.
*
******************************************************************************/

extern unsigned long _stack;

__attribute__ ((section(".isr_vector")))
void (* const g_pfnVectors[])(void) =
{
        (void (*)(void))&_stack,   /* The initial stack pointer */
        Reset_Handler,             /* Reset Handler */
        NMI_Handler,               /* NMI Handler */
        HardFault_Handler,         /* Hard Fault Handler */
        MemManage_Handler,         /* MPU Fault Handler */
        BusFault_Handler,          /* Bus Fault Handler */
        UsageFault_Handler,        /* Usage Fault Handler */
        0,                         /* Reserved */
        __bbconf_pt_magic,         /* Reserved or BBCONF_MAGIC_ADDR */
        __bbconf_pt_addr,          /* Reserved or BBCONF_PTPTR_ADDR */
        0,                         /* Reserved */
        SVC_Handler,               /* SVCall Handler */
        DebugMon_Handler,          /* Debug Monitor Handler */
        0,                         /* Reserved */
        PendSV_Handler,            /* PendSV Handler */
        SysTick_Handler,           /* SysTick Handler */

    // External Interrupts
        WWDG_IRQHandler,              /*!< Window WatchDog */
        PVD_IRQHandler,               /*!< PVD through EXTI Line detection */
        TAMPER_IRQHandler,            /*!< Tamper */
        RTC_IRQHandler,               /*!< RTC */
        FLASH_IRQHandler,             /*!< FLASH */
        RCC_IRQHandler,               /*!< RCC  */
        EXTI0_IRQHandler,             /*!< EXTI Line0 */
        EXTI1_IRQHandler,             /*!< EXTI Line1 */
        EXTI2_IRQHandler,             /*!< EXTI Line2 */
        EXTI3_IRQHandler,             /*!< EXTI Line3 */
        EXTI4_IRQHandler,             /*!< EXTI Line4 */
        DMA1_Channel1_IRQHandler,     /*!< DMA1 Channel 1 */
        DMA1_Channel2_IRQHandler,     /*!< DMA1 Channel 2 */
        DMA1_Channel3_IRQHandler,     /*!< DMA1 Channel 3 */
        DMA1_Channel4_IRQHandler,     /*!< DMA1 Channel 4 */
        DMA1_Channel5_IRQHandler,     /*!< DMA1 Channel 5 */
        DMA1_Channel6_IRQHandler,     /*!< DMA1 Channel 6 */
        DMA1_Channel7_IRQHandler,     /*!< DMA1 Channel 7 */

        ADC1_2_IRQHandler,            /*!< ADC1 and ADC2 */
        USB_HP_CAN1_TX_IRQHandler,    /*!< USB Device High Priority or CAN1 TX */
        USB_LP_CAN1_RX0_IRQHandler,   /*!< USB Device Low Priority or CAN1 RX0 */
        CAN1_RX1_IRQHandler,          /*!< CAN1 RX1 */
        CAN1_SCE_IRQHandler,          /*!< CAN1 SCE */
        EXTI9_5_IRQHandler,           /*!< External Line[9:5] */
        TIM1_BRK_IRQHandler,          /*!< TIM1 Break */
        TIM1_UP_IRQHandler,           /*!< TIM1 Update */
        TIM1_TRG_COM_IRQHandler,      /*!< TIM1 Trigger and Commutation */
        TIM1_CC_IRQHandler,           /*!< TIM1 Capture Compare */
        TIM2_IRQHandler,              /*!< TIM2 global */
        TIM3_IRQHandler,              /*!< TIM3 global */
        TIM4_IRQHandler,              /*!< TIM4 global */
        I2C1_EV_IRQHandler,           /*!< I2C1 Event */
        I2C1_ER_IRQHandler,           /*!< I2C1 Error */
        I2C2_EV_IRQHandler,           /*!< I2C2 Event */
        I2C2_ER_IRQHandler,           /*!< I2C2 Error */
        SPI1_IRQHandler,              /*!< SPI1 global */
        SPI2_IRQHandler,              /*!< SPI2 global */
        USART1_IRQHandler,            /*!< USART1 global */
        USART2_IRQHandler,            /*!< USART2 global */
        USART3_IRQHandler,            /*!< USART3 global */
        EXTI15_10_IRQHandler,         /*!< External Line[15:10] */
        RTCAlarm_IRQHandler,          /*!< RTC Alarm through EXTI Line */
        USBWakeUp_IRQHandler,         /*!< USB Device WakeUp from suspend through EXTI Line */
};

/*******************************************************************************
* Function Name  : Reset_Handler
* Description    : This is the code that gets called when the processor first starts execution
*          following a reset event.  Only the absolutely necessary set is performed,
*          after which the application supplied main() routine is called.
* Input          :
* Output         :
* Return         :
*******************************************************************************/

void Reset_Handler(void)
{
  unsigned long *pulDest;
  unsigned long *pulSrc;

  NVIC->ICER[0] = 0xffffffff;
  NVIC->ICER[1] = 0x000007ff;

  //
  // Copy the data segment initializers from flash to SRAM in ROM mode
  //

  if (&_sidata != &_sdata) {  // only if needed
    pulSrc = &_sidata;
    for(pulDest = &_sdata; pulDest < &_edata; ) {
      *(pulDest++) = *(pulSrc++);
    }
  }

  // Copy the .fastcode code from ROM to SRAM

  if (&_sifastcode != &_sfastcode) {  // only if needed
    pulSrc = &_sifastcode;
    for(pulDest = &_sfastcode; pulDest < &_efastcode; ) {
      *(pulDest++) = *(pulSrc++);
    }
  }

  //
  // Zero fill the bss segment.
  //
  for(pulDest = &_sbss; pulDest < &_ebss; )
  {
      *(pulDest++) = 0;
  }

  // copy initial values and set irq table
  if(irq_handler_table && irq_table_size) {
    int i;
    pulSrc = (unsigned long*)g_pfnVectors;
    pulDest = (unsigned long*)irq_handler_table;
    for(i = irq_table_size; i--; ) {
        *(pulDest++) = *(pulSrc++);
    }
    /*SCB->VTOR=0x10000000;*/
    SCB->VTOR=(uint32_t)irq_handler_table;
  }

  //if (_setup_board!=0)
  _setup_board();

  //
  // Call the application's entry point.
  //
  main();

  /* disable interrupts */
  NVIC->ICER[0] = 0xffffffff;
  NVIC->ICER[1] = 0x000007ff;
  __set_MSP(*(unsigned long *)(&_mem_app_start));         /* Stack pointer */
  fnc_entry = (FNC)*(unsigned long *)(&_mem_app_start+1); /* Reset handler */
  fnc_entry();
}

//*****************************************************************************
//
// Provide weak aliases for each Exception handler to the Default_Handler.
// As they are weak aliases, any function with the same name will override
// this definition.
//
//*****************************************************************************
#pragma weak MemManage_Handler = Default_Handler          /* MPU Fault Handler */
#pragma weak BusFault_Handler = Default_Handler           /* Bus Fault Handler */
#pragma weak UsageFault_Handler = Default_Handler         /* Usage Fault Handler */
#pragma weak SVC_Handler = Default_Handler                /* SVCall Handler */
#pragma weak DebugMon_Handler = Default_Handler           /* Debug Monitor Handler */
#pragma weak PendSV_Handler = Default_Handler             /* PendSV Handler */
#pragma weak SysTick_Handler = Default_Handler            /* SysTick Handler */

/* External interrupt vector handler */
#pragma weak WWDG_IRQHandler = Default_Handler              /*!< Window WatchDog */
#pragma weak PVD_IRQHandler = Default_Handler               /*!< PVD through EXTI Line detection */
#pragma weak TAMPER_IRQHandler = Default_Handler            /*!< Tamper */
#pragma weak RTC_IRQHandler = Default_Handler               /*!< RTC */
#pragma weak FLASH_IRQHandler = Default_Handler             /*!< FLASH */
#pragma weak RCC_IRQHandler = Default_Handler               /*!< RCC  */
#pragma weak EXTI0_IRQHandler = Default_Handler             /*!< EXTI Line0 */
#pragma weak EXTI1_IRQHandler = Default_Handler             /*!< EXTI Line1 */
#pragma weak EXTI2_IRQHandler = Default_Handler             /*!< EXTI Line2 */
#pragma weak EXTI3_IRQHandler = Default_Handler             /*!< EXTI Line3 */
#pragma weak EXTI4_IRQHandler = Default_Handler             /*!< EXTI Line4 */
#pragma weak DMA1_Channel1_IRQHandler = Default_Handler     /*!< DMA1 Channel 1 */
#pragma weak DMA1_Channel2_IRQHandler = Default_Handler     /*!< DMA1 Channel 2 */
#pragma weak DMA1_Channel3_IRQHandler = Default_Handler     /*!< DMA1 Channel 3 */
#pragma weak DMA1_Channel4_IRQHandler = Default_Handler     /*!< DMA1 Channel 4 */
#pragma weak DMA1_Channel5_IRQHandler = Default_Handler     /*!< DMA1 Channel 5 */
#pragma weak DMA1_Channel6_IRQHandler = Default_Handler     /*!< DMA1 Channel 6 */
#pragma weak DMA1_Channel7_IRQHandler = Default_Handler     /*!< DMA1 Channel 7 */

#pragma weak ADC1_2_IRQHandler = Default_Handler            /*!< ADC1 and ADC2 */
#pragma weak USB_HP_CAN1_TX_IRQHandler = Default_Handler    /*!< USB Device High Priority or CAN1 TX */
#pragma weak USB_LP_CAN1_RX0_IRQHandler = Default_Handler   /*!< USB Device Low Priority or CAN1 RX0 */
#pragma weak CAN1_RX1_IRQHandler = Default_Handler          /*!< CAN1 RX1 */
#pragma weak CAN1_SCE_IRQHandler = Default_Handler          /*!< CAN1 SCE */
#pragma weak EXTI9_5_IRQHandler = Default_Handler           /*!< External Line[9:5] */
#pragma weak TIM1_BRK_IRQHandler = Default_Handler          /*!< TIM1 Break */
#pragma weak TIM1_UP_IRQHandler = Default_Handler           /*!< TIM1 Update */
#pragma weak TIM1_TRG_COM_IRQHandler = Default_Handler      /*!< TIM1 Trigger and Commutation */
#pragma weak TIM1_CC_IRQHandler = Default_Handler           /*!< TIM1 Capture Compare */
#pragma weak TIM2_IRQHandler = Default_Handler              /*!< TIM2 global */
#pragma weak TIM3_IRQHandler = Default_Handler              /*!< TIM3 global */
#pragma weak TIM4_IRQHandler = Default_Handler              /*!< TIM4 global */
#pragma weak I2C1_EV_IRQHandler = Default_Handler           /*!< I2C1 Event */
#pragma weak I2C1_ER_IRQHandler = Default_Handler           /*!< I2C1 Error */
#pragma weak I2C2_EV_IRQHandler = Default_Handler           /*!< I2C2 Event */
#pragma weak I2C2_ER_IRQHandler = Default_Handler           /*!< I2C2 Error */
#pragma weak SPI1_IRQHandler = Default_Handler              /*!< SPI1 global */
#pragma weak SPI2_IRQHandler = Default_Handler              /*!< SPI2 global */
#pragma weak USART1_IRQHandler = Default_Handler            /*!< USART1 global */
#pragma weak USART2_IRQHandler = Default_Handler            /*!< USART2 global */
#pragma weak USART3_IRQHandler = Default_Handler            /*!< USART3 global */
#pragma weak EXTI15_10_IRQHandler = Default_Handler         /*!< External Line[15:10] */
#pragma weak RTCAlarm_IRQHandler = Default_Handler          /*!< RTC Alarm through EXTI Line */
#pragma weak USBWakeUp_IRQHandler = Default_Handler         /*!< USB Device WakeUp from suspend through EXTI Line */

//*****************************************************************************
//
// This is the code that gets called when the processor receives an unexpected
// interrupt.  This simply enters an infinite loop, preserving the system state
// for examination by a debugger.
//
//*****************************************************************************
void Default_Handler(void) {
  // Go into an infinite loop.
  //
  while (1) {
  }
}

/* empty setup_board function if non is defined in another library */
//#pragma weak _setup_board = default_setup_board                /* default setup_board */
//void default_setup_board(void)
//{
//  // nothing to do
//}
