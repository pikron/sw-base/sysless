#ifndef _ADS1X46_H
#define _ADS1X46_H

#define ADS_REG_BCS             0x00
#define RBCS1                   (1<<7)
#define RBCS0                   (1<<6)

#define ADS_REG_VBIAS           0x01
#define RVBIAS1                 (1<<1)
#define RVBIAS0                 (1<<0)

#define ADS_REG_MUX1            0x02
#define RCLKSTAT                (1<<7)
#define RMUXCAL2                (1<<2)
#define RMUXCAL1                (1<<1)
#define RMUXCAL0                (1<<0)

#define ADS_REG_SYS0            0x03
#define RPGA2                   (1<<6)
#define RPGA1                   (1<<5)
#define RPGA0                   (1<<4)
#define RDR3                    (1<<3)
#define RDR2                    (1<<2)
#define RDR1                    (1<<1)
#define RDR0                    (1<<0)

#define ADS_REG_OFC0            0x04
#define ADS_REG_OFC1            0x05
#define ADS_REG_OFC2            0x06
#define ADS_REG_FSC0            0x07
#define ADS_REG_FSC1            0x08
#define ADS_REG_FSC2            0x09
#define ADS_REG_ID              0x0A
#define RDRDY_MODE              (1<<3)

#define ADS_CMD_WAKEUP          0x00
#define ADS_CMD_SLEEP           0x02
#define ADS_CMD_SYNC            0x04
#define ADS_CMD_RESET           0x06
#define ADS_CMD_PO_INIT         0x0E
#define ADS_CMD_NOP             0xFF

#define ADS_CMD_RDATA           0x12
#define ADS_CMD_RDATAC          0x14
#define ADS_CMD_SDATAC          0x16

#define ADS_CMD_RREG            0x20
#define ADS_CMD_WREG            0x40

#define ADS_CMD_SYSOCAL         0x60
#define ADS_CMD_SYSGCAL         0x61
#define ADS_CMD_SELFCAL         0x62


#endif /* _ADS1X46_H */

