#ifndef _BYTESWAP_H
#define _BYTESWAP_H	1

#if defined(__KEIL__)
#define __bswap_16(x) ( (((x) << 8) & 0xFF00) | (((x) >> 8) & 0x00FF) )
#else
#define __bswap_16(x) ({unsigned short __x=(x); \
			(((__x>>8)&0xff)|((__x&0xff)<<8)); })
#endif

#if defined(__KEIL__)
    //todo
#else
#define __bswap_32(x) ({unsigned long __y=(x); \
			(__bswap_16(__y>>16)|__bswap_16(__y)<<16); })
#endif

#define bswap_16(x) __bswap_16 (x)

#define bswap_32(x) __bswap_32 (x)

#endif /* byteswap.h */
