#ifndef	_ENDIAN_H
#define	_ENDIAN_H	1

#define	__LITTLE_ENDIAN	1234
#define	__BIG_ENDIAN	4321
#define	__PDP_ENDIAN	3412

#if defined(__i386__) || defined(SDCC) || defined(__SDCC) || defined (__ARMEL__)
#define __BYTE_ORDER __LITTLE_ENDIAN
#endif

#if defined(__H8300__) || defined(__H8500__) || defined (__H8300H__) ||  defined(__W65__) || defined (__H8300S__) || defined (__m68k__) || defined (__ARMEB__) || defined(__KEIL__)
#define __BYTE_ORDER __BIG_ENDIAN
#endif

#endif	/* endian.h */
