#ifndef _KEYVAL_ID_HIS_H_
#define _KEYVAL_ID_HISC_H_

#include "keyvalpb.h"

#define KVPB_KEYID_BLINDER_POSSITION    0x20
#define KVPB_KEYID_BLINDER_OPENTIME     0x21
#define KVPB_KEYID_BLP_UP		0x22
#define KVPB_KEYID_BLP_DOWN		0x23
#define KVPB_KEYID_HOMEBELL_ADDR        0x30

#endif /* _KEYVAL_ID_HIS_H_ */

