#ifndef _LT_TIMER_H
#define _LT_TIMER_H

#include <stdint.h>
#include <lt_timer_types.h>

#include <cpu_def.h>
#include <system_def.h>

//timers

#ifndef LT_TIMER_VAR_LOC
#define LT_TIMER_VAR_LOC
#endif

static lt_ticks_t LT_TIMER_VAR_LOC last_ticks;
static lt_mstime_t LT_TIMER_VAR_LOC actual_msec; 

/* Declaration of ulan light timers */

#define lt_get_msbase()     (1000/SYS_TIMER_HZ)		/* in ms */
#define lt_get_ticks()      (get_sys_timer_ticks())

static inline void
lt_mstime_update()
{
  lt_ticks_t LT_TIMER_VAR_LOC act_ticks;
  lt_mstime_t LT_TIMER_VAR_LOC msec_diff;

  act_ticks=lt_get_ticks();
  msec_diff=((lt_tidiff_t)(act_ticks-last_ticks))*lt_get_msbase();
  last_ticks=act_ticks;

  actual_msec+=msec_diff;
}


#define LT_TIMER_DEC(cust_prefix) \
\
extern lt_mstime_t LT_TIMER_VAR_LOC cust_prefix##_last_expired; \
static inline void \
cust_prefix##_init() \
{\
  lt_mstime_update();\
  cust_prefix##_last_expired=actual_msec;\
}\
static inline int \
cust_prefix##_expired(lt_mstime_t expiration) \
{\
  lt_mstime_update();\
  \
  if ((lt_msdiff_t)(actual_msec-cust_prefix##_last_expired)>=expiration) {\
    cust_prefix##_last_expired=actual_msec;\
    return 1;\
  }\
  \
  return 0;\
}

#define LT_TIMER_IMP(cust_prefix) \
\
lt_mstime_t LT_TIMER_VAR_LOC cust_prefix##_last_expired; \


#endif /* _LT_TIMER_H */
