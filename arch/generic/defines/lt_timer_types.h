#ifndef _LT_TIMER_TYPES_H
#define _LT_TIMER_TYPES_H

//timers

#if defined(SDCC) || defined(__SDCC)
typedef unsigned char lt_ticks_t;
typedef char lt_tidiff_t;
typedef unsigned int lt_mstime_t;
typedef int lt_msdiff_t;
#define LT_TIMER_VAR_LOC DATA
#else
typedef unsigned int lt_ticks_t;
typedef int lt_tidiff_t;
typedef unsigned long lt_mstime_t;
typedef signed long lt_msdiff_t;
#endif

#endif /* _LT_TIMER_TYPES_H */
