#ifndef _GENPOSIX_CPU_DEF_H
#define _GENPOSIX_CPU_DEF_H

#ifndef CODE
  #define CODE
#endif

#ifndef XDATA
  #define XDATA
#endif

#ifndef DATA
  #define DATA
#endif

#define set_bit(nr,v)    (__sync_fetch_and_or(v,(1<<(nr))))
#define clear_bit(nr,v)  (__sync_fetch_and_and(v,~(1<<(nr))))
#define test_and_set_bit(nr,v) \
  ({ typeof(*(v)) __bitmsk = 1<<(nr); \
     __sync_fetch_and_or(v,__bitmsk)&__bitmsk?1:0; \
  })

#define atomic_clear_mask_b1(mask, v) __sync_fetch_and_and(v,~(mask))
#define atomic_set_mask_b1(mask, v)   __sync_fetch_and_or(v,(mask))

#define atomic_clear_mask_w1(mask, v) __sync_fetch_and_and(v,~(mask))
#define atomic_set_mask_w1(mask, v)   __sync_fetch_and_or(v,(mask))

#define atomic_clear_mask_w(mask, v)  __sync_fetch_and_and(v,~(mask))
#define atomic_set_mask_w(mask, v)    __sync_fetch_and_or(v,(mask))

#define atomic_clear_mask(mask, v)    __sync_fetch_and_and(v,~(mask))
#define atomic_set_mask(mask, v)      __sync_fetch_and_or(v,(mask))

/* Arithmetic functions */
#define sat_add_slsl(__x,__y)                                   \
        do {                                                    \
                typeof(__x) tmp;                                \
                tmp = (__x) + (__y);                            \
                if ((__x) > 0 && (__y) > 0 && tmp < 0)          \
                        tmp = +0x7fffffff;                      \
                else if ((__x) < 0 && (__y) < 0 && tmp >= 0)    \
                        tmp = -0x80000000;                      \
                (__x) = tmp;                                    \
        } while (0)

#ifndef __memory_barrier
#define __memory_barrier() \
__asm__ __volatile__("": : : "memory")
#endif

/*masked fields macros*/
#define __val2mfld(mask,val) (((mask)&~((mask)<<1))*(val)&(mask))
#define __mfld2val(mask,val) (((val)&(mask))/((mask)&~((mask)<<1)))

#endif /* _GENPOSIX_CPU_DEF_H */
