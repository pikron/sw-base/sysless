/* procesor H8S/2638 ver 1.1  */
#include <stdint.h>
#include <cpu_def.h>
#include <mcu_regs.h>
//#include <periph/chmod_lcd.h>
//#include <periph/sgm_lcd.h>
#include <system_def.h>
#include <string.h>


#ifndef DEB_LED_INIT
#define DEB_LED_INIT()
#define DEB_LED_OFF(num)
#define DEB_LED_ON(num)
#endif

#define BOOT_TEST
#define APPLICATION_START

/*#define USE_FONT_6x8*/

#ifndef HIT_LOAD_BAUD
  #define HIT_LOAD_BAUD 0
#endif

/* hack for start of main, should use crt0.o instead */
/* Used in boot mode to start main(). */
__asm__ /*__volatile__*/(
	".global _start_hack\n\t"
	"_start_hack : \n\t"
	"mov.l  #0xffdffe,sp\n\t"
	"jsr	_main\n"
	"0: bra 0b\n"
	);

void exit(int status)
{
  while(1);
}

void deb_wr_hex(long hex, short digs);

char data_test[]={'D','A','T','A',0};

 /*
 *----------------------------------------------------------
 */
void deb_wr_hex(long hex, short digs)
{
  char c;
  while(digs--){
    c=((hex>>(4*digs))&0xf)+'0';
    if(c>'9') c+='A'-'9'-1;  
  }
}

static void deb_led_out(char val)
{
  if (val&1)
    DEB_LED_ON(0);
  else
    DEB_LED_OFF(0);
  if (val&2)
    DEB_LED_ON(1);
  else
    DEB_LED_OFF(1);
  if (val&4)
    DEB_LED_ON(2);
  else
    DEB_LED_OFF(2);
  if (val&8)
    DEB_LED_ON(3);
  else
    DEB_LED_OFF(3);
}


#ifdef BOOT_TEST

#include <boot_fn.h>

void boot_test()
{
  /*set power on for SCI0 and SCI1 module*/
  *SYS_MSTPCRB&=~MSTPCRB_SCI0m;
  *SYS_MSTPCRB&=~MSTPCRB_SCI1m;

 #if 0
  SCIInit(HIT_LOAD_BAUD);

  SCISend('B');
  SCISend('B');
  SCISend(':');

 #endif

  /* switch off SCI2 module*/
  *SYS_MSTPCRB|=MSTPCRB_SCI2m;
  
  *DIO_PADR |= 0x0f;
  *DIO_PADDR = 0x0f;

  if(!HIT_LOAD_BAUD) {
    long bauddet;   
    bauddet=SCIAutoBaud();
    deb_wr_hex(bauddet,4);
  }

 
  ProgMode(HIT_LOAD_BAUD);
}

#endif /* BOOT_TEST */

inline int call_address(unsigned long addr)
{
  typedef int (*my_call_t)(void);
  my_call_t my_call=(my_call_t)addr;
  return my_call();  
}

/*
 *-----------------------------------------------------------
 */


/* Only for debuging */
void deb_led_blink() {
  while(1) {
    deb_led_out(1);
    FlWait(1*1000000);
    deb_led_out(2);
    FlWait(1*1000000);
  };
};

int main()
{
  uint8_t *p;

  _setup_board(); /* Provided in bspbase library of each board */

  p=(uint8_t*)&deb_wr_hex;
  if(p>=IRAM_START) p=" IRAM";
#ifdef SRAM_START
  else if(p>=SRAM_START) p=" SRAM";
#endif
#ifdef XRAM_START
  else if(p>=XRAM_START) p=" XRAM";
#endif
  else if(p>(uint8_t*)0x4000l) p=" FLSHU";
  else p=" FLSHB";


#if 0		/* FLASH timing test */
  do{
    deb_led_out(~0);
    FlWait(1l);
    deb_led_out(~1);
    FlWait(2l);
    deb_led_out(~2);
    FlWait(10l);
    deb_led_out(~3);
    FlWait(20l);
  }while(1);
#endif

#ifdef APPLICATION_START
  if(((*FLM_FLMCR1) & FLMCR1_FWEm)==0) {
    if (*((unsigned long *)0x4000)!=0xffffffff){
      call_address(0x4000);
    }
  }
#endif /* APPLICATION_START */
 
  deb_led_out(15);
  FlWait(1*100000);
  deb_led_out(3);
 
#ifdef BOOT_TEST
  boot_test();
#endif /* BOOT_TEST */ 
 
  return 0;
};


