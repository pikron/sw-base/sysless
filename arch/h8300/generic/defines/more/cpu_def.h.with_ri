#ifndef _H8S_CPU_DEF_H
#define _H8S_CPU_DEF_H

/* atomic access routines */

#define __xcase_xop_bit(nr,v,__aln,__aconaddr,__acondata) \
	({ volatile __typeof(*v) *__pv =(__typeof(*v) *)(v); \
	   unsigned short __nr=(nr); \
	   char *__pb=(char*)__pv; \
	   __pb+=sizeof(*__pv)-1-(__nr)/8; \
	   __asm__ __volatile__ ( __aln " /*mybit*/\n" : \
		"=m" (*__pb) : __acondata ((nr)&7), __aconaddr (__pb)); })

#define __constant_set_bit(nr,v) \
		__xcase_xop_bit(nr,v," bset %1,@%u2\n","i","i") 

#define __constantdata_set_bit(nr,v) \
		__xcase_xop_bit(nr,v," bset %1,@%2\n","r","i") 

#define __generic_set_bit(nr,v) \
		__xcase_xop_bit(nr,v," bset %1l,@%2\n","r","r") 

#define set_bit(nr,v) \
	(__builtin_constant_p(nr) ? \
	   __builtin_constant_p(v) ?\
	     __constant_set_bit(nr, v) : \
	     __constantdata_set_bit(nr, v) : \
	   __generic_set_bit(nr, v))

#define __constant_clear_bit(nr,v) \
		__xcase_xop_bit(nr,v," bclr %1,@%u2\n","i","i") 

#define __constantdata_clear_bit(nr,v) \
		__xcase_xop_bit(nr,v," bclr %1,@%2\n","r","i") 

#define __generic_clear_bit(nr,v) \
		__xcase_xop_bit(nr,v," bclr %1l,@%2\n","r","r") 

#define clear_bit(nr,v) \
	(__builtin_constant_p(nr) ? \
	   __builtin_constant_p(v) ?\
	     __constant_clear_bit(nr, v) : \
	     __constantdata_clear_bit(nr, v) : \
	   __generic_clear_bit(nr, v))

#define atomic_clear_mask_b1(mask, v) \
	({ volatile char *__pv=(char*)(v); \
	   if(__builtin_constant_p(v)) \
	     __asm__ __volatile__("bclr %V1,@%u2 /*mymask*/\n" : "=m" (*__pv) : "P" (mask),"i"(v)); \
	   else \
	     __asm__ __volatile__("bclr %V1,@%2 /*mymask*/\n" : "=m" (*__pv) : "P" (mask),"r"(__pv)); \
	})

#define atomic_set_mask_b1(mask, v) \
	({ volatile char *__pv=(char*)(v); \
	   if(__builtin_constant_p(v)) \
	     __asm__ __volatile__("bset %V1,@%u2 /*mymask*/\n" : "=m" (*__pv) : "P" (mask),"i"(v)); \
	   else \
	     __asm__ __volatile__("bset %V1,@%2 /*mymask*/\n" : "=m" (*__pv) : "P" (mask),"r"(__pv)); \
	})

#define __xcase_xop_mask_w1(mask,v,__aln,__aconaddr,__acondata) \
	({ volatile char *__pv=(char*)(v); \
	   unsigned __mask=(mask); \
	   if((__mask)&0xff) __pv++; else __mask>>=8; \
	   __asm__ __volatile__(__aln " /*mymask*/\n": "=m" (*__pv) : __acondata (__mask), __aconaddr (__pv)); \
	})


#define __constant_atomic_clear_mask_w1(mask, v) \
	__xcase_xop_mask_w1(mask,v,"bclr %V1,@%u2\n","i","P")

#define __generic_atomic_clear_mask_w1(mask, v) \
	__xcase_xop_mask_w1(mask,v,"bclr %V1,@%2\n","r","P")

#define atomic_clear_mask_w1(mask, v) \
	(__builtin_constant_p(mask) ? \
	   __builtin_constant_p(v) ?\
	     __constant_atomic_clear_mask_w1(mask, v) : \
	     __generic_atomic_clear_mask_w1(mask, v) : \
	   error_to_use_for_nonconstant_mask())

#define __constant_atomic_set_mask_w1(mask, v) \
	__xcase_xop_mask_w1(mask,v,"bset %V1,@%u2\n","i","P")

#define __generic_atomic_set_mask_w1(mask, v) \
	__xcase_xop_mask_w1(mask,v,"bset %V1,@%2\n","r","P")

#define atomic_set_mask_w1(mask, v) \
	(__builtin_constant_p(mask) ? \
	   __builtin_constant_p(v) ?\
	     __constant_atomic_set_mask_w1(mask, v) : \
	     __generic_atomic_set_mask_w1(mask, v) : \
	   error_to_use_for_nonconstant_mask())

/*

#define atomic_clear_mask(mask, v) \
	__asm__ __volatile__("and.l %1,%0" : "=m" (*(v)) : "id" (~(mask)),"0"(*(v)))

#define atomic_set_mask(mask, v) \
	__asm__ __volatile__("or.l %1,%0" : "=m" (*(v)) : "id" (mask),"0"(*(v)))

#define atomic_clear_mask_w(mask, v) \
	__asm__ __volatile__("and.w %1,%0" : "=m" (*(v)) : "id" (~(mask)),"0"(*(v)))

#define atomic_set_mask_w(mask, v) \
	__asm__ __volatile__("or.w %1,%0" : "=m" (*(v)) : "id" (mask),"0"(*(v)))

#define atomic_clear_mask_b(mask, v) \
	__asm__ __volatile__("and.b %1,%0" : "=m" (*(v)) : "id" (~(mask)),"0"(*(v)))

#define atomic_set_mask_b(mask, v) \
	__asm__ __volatile__("or.b %1,%0" : "=m" (*(v)) : "id" (mask),"0"(*(v)))
*/


/* Port access routines */

#define readb(addr) \
    ({ unsigned char __v = (*(volatile unsigned char *) (addr)); __v; })
#define readw(addr) \
    ({ unsigned short __v = (*(volatile unsigned short *) (addr)); __v; })
#define readl(addr) \
    ({ unsigned int __v = (*(volatile unsigned int *) (addr)); __v; })

#define writeb(b,addr) (void)((*(volatile unsigned char *) (addr)) = (b))
#define writew(b,addr) (void)((*(volatile unsigned short *) (addr)) = (b))
#define writel(b,addr) (void)((*(volatile unsigned int *) (addr)) = (b))

/* Arithmetic functions */

#define sat_add_slsl(__x,__y) \
    __asm__ ("	add.l %2,%0\n" \
	"	bvc 2f:8\n" \
	"	bpl 1f:8\n" \
	"	mov.l #0x7fffffff:32,%0\n" \
	"	bt  2f:8\n" \
	"1:	mov.l #0x80000000:32,%0\n" \
	"2:\n" \
      : "=r"(__x) \
      : "0" ((long)__x), "r" ((long)__y) : "cc"); \

#define sat_sub_slsl(__x,__y) \
    __asm__ ("	sub.l %2,%0\n" \
	"	bvc 2f:8\n" \
	"	bpl 1f:8\n" \
	"	mov.l #0x7fffffff:32,%0\n" \
	"	bt  2f:8\n" \
	"1:	mov.l #0x80000000:32,%0\n" \
	"2:\n" \
      : "=r"(__x) \
      : "0" ((long)__x), "r" ((long)__y) : "cc"); \

#define div_us_ulus(__x,__y) \
  ({ \
    unsigned long __z=(__x); \
    __asm__ ("divxu.w %2,%0": "=r"(__z) \
      : "0" (__z), "r" ((unsigned short)(__y)) : "cc"); \
    (unsigned short)__z; \
  })

#define div_ss_slss(__x,__y) \
  ({ \
    unsigned long __z=(__x); \
    __asm__ ("divxs.w %2,%0": "=r"(__z) \
      : "0" (__z), "r" ((unsigned short)(__y)) : "cc"); \
    (unsigned short)__z; \
  })

#define muldiv_us(__x,__y,__z) \
  div_ss_slss((long)(__x)*(__y),__z)

#define muldiv_ss(__x,__y,__z) \
  div_us_ulus((unsigned long)(__x)*(__y),__z)

/* Power down modes support */

#define __cpu_sleep() __asm__ __volatile__ ("sleep": : : "memory")

/* IRQ handling code */

#ifdef CONFIG_USE_EXR_LEVELS

#define __sti() __asm__ __volatile__ ("andc #0xf8,exr": : : "memory")

#define __cli() __asm__ __volatile__ ("orc  #0x07,exr": : : "memory")

#define __save_flags(x) \
  do{ \
    unsigned short __exr; \
    __asm__ __volatile__("stc exr,%0":"=m" (__exr) :  :"memory"); \
    (x)=__exr; \
  }while(0)

#define __restore_flags(x) \
  do{ \
    unsigned short __exr=(x); \
    __asm__ __volatile__("ldc %0,exr": :"m" (__exr) :"memory"); \
  }while(0)


#else /* CONFIG_USE_EXR_LEVELS */

#define __sti() __asm__ __volatile__ ("andc #0x7f,ccr": : : "memory")

#define __cli() __asm__ __volatile__ ("orc  #0x80,ccr": : : "memory")

#define __save_flags(x) \
  do{ \
    unsigned short __ccr; \
    __asm__ __volatile__("stc ccr,%0":"=m" (__ccr) :  :"memory"); \
    (x)=__ccr; \
  }while(0)

#define __restore_flags(x) \
  do{ \
    unsigned short __ccr=(x); \
    __asm__ __volatile__("ldc %0,ccr": :"m" (__ccr) :"cc","memory"); \
  }while(0)

#endif /* CONFIG_USE_EXR_LEVELS */

#define __get_vbr(x) 0

#define __get_sp(x) __asm__ __volatile__("mov.l sp,%0":"=r" (x) : :"cc")

#define __memory_barrier() \
__asm__ __volatile__("": : : "memory")

#define cli() __cli()
#define sti() __sti()

#define save_flags(x) __save_flags(x)
#define restore_flags(x) __restore_flags(x)
#define save_and_cli(flags)   do { save_flags(flags); cli(); } while(0)

#define NR_IRQS 256

/* this struct defines the way the registers are stored on the
   stack during a system call. */

/*

#if 0   
struct pt_regs {
  long     d1;
  long     d2;
  long     d3;
  long     d4;
  long     d5;
  long     a0;
  long     a1;
  long     a2;
  long     d0;
  long     orig_d0;
  unsigned short sr;
  unsigned long  pc;
  unsigned format :  4; 
  unsigned vector : 12;
};
#else
struct pt_regs {
  long     d0;
  long     d1;
  long     d2;
  long     d3;
  long     d4;
  long     d5;
  long     d6;
  long     d7;
  long     a0;
  long     a1;
  long     a2;
  long     a3;
  long     a4;
  long     a5;
  long     a6;
  unsigned short sr;
  unsigned long  pc;
  unsigned format :  4;
  unsigned vector : 12;
};
#endif
                                 
typedef struct irq_handler {
  void            (*handler)(int, void *, struct pt_regs *);
  unsigned long   flags;
  void            *dev_id;
  const char      *devname;
  struct irq_handler *next;
} irq_handler_t;

irq_handler_t *irq_array[NR_IRQS];
void          *irq_vec[NR_IRQS];

int add_irq_handler(int vectno,irq_handler_t *handler);
*/

void *excptvec_get(int vectnum);

void *excptvec_set(int vectnum,void *vect);

int excptvec_initfill(void *fill_vect, int force_all);

#define __val2mfld(mask,val) (((mask)&~((mask)<<1))*(val)&(mask))
#define __mfld2val(mask,val) (((val)&(mask))/((mask)&~((mask)<<1)))

#endif /* _H8S_CPU_DEF_H */
