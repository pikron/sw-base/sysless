/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  bit_iic.c - bit-bang IIC communication automata interface 
 
  Copyright (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#include <stdint.h>
#include <cpu_def.h>
//#include <h8s2633h.h>
#include <mcu_regs.h>
#include <system_def.h>
#include <malloc.h>
#include <string.h>
#include <periph/iic_ifc.h>

volatile void FlWait(long n);

#define bit_iic_wait FlWait

/* must be >= 7 */
#define BIIC_DELAY_H 10
#define BIIC_DELAY_L 10
#define BIIC_DELAY_S 10

#define MBIT_SET(mbit,val) do {\
	if(val) atomic_set_mask_b1(0?mbit, 1?mbit);\
	else atomic_clear_mask_b1(0?mbit, 1?mbit);\
    } while(0)
     
#define MBIT_GET(mbit) \
	(*(1?mbit) & (0?mbit))

/* P3.4 SDA, P3.5 SCL */

#define BIT_IIC_PORT_O DIO_P3DR
#define BIT_IIC_PORT_I DIO_PORT3
#define BIT_IIC_PORT_DIR DIO_P3DDR
#define BIT_IIC_SDAm 0x10
#define BIT_IIC_SCLm 0x20

#if 0

#define BIIC_INIT_PORT() do {\
	SHADOW_REG_SET(DIO_P3DDR,BIT_IIC_SDAm|BIT_IIC_SCLm); \
	*DIO_P3ODR|=BIT_IIC_SDAm|BIT_IIC_SCLm; \
    } while(0)

#define BIIC_SCL_SET(val) do {\
	if(val) atomic_set_mask_b1(BIT_IIC_SCLm, BIT_IIC_PORT_O);\
	else atomic_clear_mask_b1(BIT_IIC_SCLm, BIT_IIC_PORT_O);\
    } while(0)
#define BIIC_SCL_GET()    (*BIT_IIC_PORT_I & BIT_IIC_SCLm) 

#define BIIC_SDA_SET(val) do {\
	if(val) atomic_set_mask_b1(BIT_IIC_SDAm, BIT_IIC_PORT_O);\
	else atomic_clear_mask_b1(BIT_IIC_SDAm, BIT_IIC_PORT_O);\
    } while(0)
#define BIIC_SDA_GET()    (*BIT_IIC_PORT_I & BIT_IIC_SDAm) 

#else

#define BIIC_INIT_PORT() do {\
	SHADOW_REG_CLR(DIO_P3DDR,BIT_IIC_SDAm|BIT_IIC_SCLm); \
	*DIO_P3ODR|=BIT_IIC_SDAm|BIT_IIC_SCLm; \
	atomic_clear_mask_b1(BIT_IIC_SCLm, BIT_IIC_PORT_O);\
	atomic_clear_mask_b1(BIT_IIC_SDAm, BIT_IIC_PORT_O);\
    } while(0)

#define BIIC_SCL_SET(val) do {\
	if(val) SHADOW_REG_CLR(DIO_P3DDR,BIT_IIC_SCLm);\
	else SHADOW_REG_SET(DIO_P3DDR,BIT_IIC_SCLm);\
    } while(0)
#define BIIC_SCL_GET()    (*BIT_IIC_PORT_I & BIT_IIC_SCLm) 

#define BIIC_SDA_SET(val) do {\
	if(val) SHADOW_REG_CLR(DIO_P3DDR,BIT_IIC_SDAm);\
	else SHADOW_REG_SET(DIO_P3DDR,BIT_IIC_SDAm);\
    } while(0)
#define BIIC_SDA_GET()    (*BIT_IIC_PORT_I & BIT_IIC_SDAm) 

#endif

iic_ifc_t bit_iic_ifc;

/********************************************************************/
/* Generic IIC functions */


void iic_queue_msg(iic_msg_head_t **queue, iic_msg_head_t *msg)
{
  unsigned short saveif;
  iic_msg_head_t *prev, *next;
  save_and_cli(saveif);
  if(msg->on_queue){
    if(msg->next==msg){
      if(*msg->on_queue==msg)
        *msg->on_queue=NULL;
    }else{
      msg->next->prev=msg->prev;
      msg->prev->next=msg->next;
      if(*msg->on_queue==msg)
        *msg->on_queue=msg->next;
    }
  }
  if((msg->on_queue=queue)!=NULL){
    if((next=*queue)!=NULL){
      msg->prev=prev=next->prev;
      msg->next=next;
      next->prev=msg;
      prev->next=msg;
    }else{
      *queue=msg->prev=msg->next=msg;
    }
  }
  restore_flags(saveif);
  return;
}

int iic_master_msg_ins(iic_ifc_t *ifc, iic_msg_head_t *msg)
{
  if(!(ifc->flags&IIC_IFC_ON)) return -1;
  if(!msg->tx_buf) msg->flags&=~IIC_MSG_MS_TX;
  if(!msg->rx_buf) msg->flags&=~IIC_MSG_MS_RX;
  iic_queue_msg(&ifc->master_queue,msg);
  ifc->ctrl_fnc(ifc,IIC_CTRL_MS_RQ,NULL);
  return 0;
}

int iic_master_msg_rem(iic_ifc_t *ifc, iic_msg_head_t *msg)
{
  unsigned short saveif;
  save_and_cli(saveif);
  iic_queue_msg(NULL,msg);
  if(msg==ifc->msg_act)
    ifc->msg_act=NULL;
  restore_flags(saveif);
  return 0;
}

int iic_flush_all(iic_ifc_t *ifc)
{
  unsigned short saveif;
  iic_msg_head_t *msg, *next;
  iic_msg_head_t *queue[3];
  int quenum;

  save_and_cli(saveif);
    queue[0]=ifc->master_queue;
    queue[1]=ifc->slave_queue;
    queue[2]=ifc->proc_queue;
    ifc->master_queue=NULL;
    ifc->slave_queue=NULL;
    ifc->proc_queue=NULL;
    ifc->msg_act=NULL;
  restore_flags(saveif);
  for(quenum=0;quenum<3;quenum++){
    msg=queue[quenum];
    if(!msg) continue;
    msg->prev->next=NULL;
    for(;msg;msg=next){
      next=msg->next;
      msg->flags|=IIC_MSG_FAIL;
      msg->on_queue=NULL;
      if((msg->flags&IIC_MSG_CB_PROC) && (msg->callback))
	msg->callback(ifc,IIC_MSG_CB_PROC,msg);
    }
  }
  return 0;
}

/********************************************************************/
/* BitBang IIC specific functions */

static int bit_iic_ms_start(struct iic_ifc *ifc)
{
  BIIC_SDA_SET(1);
  bit_iic_wait(BIIC_DELAY_S);
  BIIC_SCL_SET(1);
  bit_iic_wait(BIIC_DELAY_S);
  BIIC_SDA_SET(0);
  bit_iic_wait(BIIC_DELAY_S);
  BIIC_SCL_SET(0);
  return 0;
}

static int bit_iic_ms_stop(struct iic_ifc *ifc)
{
  BIIC_SDA_SET(0);
  bit_iic_wait(BIIC_DELAY_S);
  BIIC_SCL_SET(1);
  bit_iic_wait(BIIC_DELAY_S);
  BIIC_SDA_SET(1);
  return 0;
}

static int bit_iic_ms_serb(struct iic_ifc *ifc, int data, int ack)
{
  int bc, td;
  int sda_in;
  /*printf("Data %02X ack %d ",data,ack);*/
  data=(data<<1)|(ack?0:1);
  for(bc=9;bc--;){
    BIIC_SDA_SET(data&0x100);
    bit_iic_wait(BIIC_DELAY_L);
    td=1000;
    BIIC_SCL_SET(1);
    do{
      do{
        if(!td--) return -1;
      }while(!BIIC_SCL_GET());
    }while(!BIIC_SCL_GET());
    sda_in=0;
    if(BIIC_SDA_GET()) sda_in++;
    if(BIIC_SDA_GET()) sda_in++;
    if(BIIC_SDA_GET()) sda_in++;
    data=(data<<1) | (sda_in>=2?1:0);
    bit_iic_wait(BIIC_DELAY_H);
    BIIC_SCL_SET(0);
  }
  data=((data>>1)&0xff) | (data&1? 0x100: 0);
  /*printf("-> data %02X ack %d\n",data&0xff,data&0x100?1:0);*/
  return data;
}

static int bit_iic_ms_rxtx(struct iic_ifc *ifc, iic_msg_head_t *msg)
{
  /*unsigned saveif;*/
  int ret=0;
  int i;
  /*save_and_cli(saveif);*/
  do {
    if(msg->flags&IIC_MSG_MS_TX){
      bit_iic_ms_start(ifc);
      ret=bit_iic_ms_serb(ifc,msg->addr&~1,0);
      if(ret&0x100) {ret=-1; break;}
      for(i=0;i<msg->tx_rq;i++){
        ret=bit_iic_ms_serb(ifc,msg->tx_buf[i],0);
        if(ret&0x100) {ret=-1; break;}
      }
      msg->tx_len=i;
    }
    if((msg->flags&IIC_MSG_MS_RX) && (ret>=0)){
      bit_iic_ms_start(ifc);
      ret=bit_iic_ms_serb(ifc,msg->addr|1,0);
      if(ret&0x100) {ret=-1; break;}
      for(i=0;i<msg->rx_rq;i++){
        ret=bit_iic_ms_serb(ifc,0xff,i+1<msg->rx_rq);
	if(ret<0) {ret=-1; break;}
	msg->rx_buf[i]=ret;
      }
      msg->rx_len=i;
    }
    if(ret>0) ret=0;
  } while(0);
  bit_iic_ms_stop(ifc);
  /*restore_flags(saveif);*/
  return ret;
}

static int bit_iic_sfnc_default(struct iic_ifc *ifc, int code)
{
  int ret;
  iic_msg_head_t *msg=ifc->master_queue;
  if(msg==NULL) return 0;
  if((msg->flags&IIC_MSG_CB_START) && (msg->callback))
    msg->callback(ifc,IIC_MSG_CB_START,msg);

  ret=bit_iic_ms_rxtx(ifc, msg);
  if(ret<0)
    msg->flags|=IIC_MSG_FAIL;
    
  if((msg->flags&IIC_MSG_CB_END) && (msg->callback))
    msg->callback(ifc,IIC_MSG_CB_END,msg);
  
  if((msg->flags&IIC_MSG_REPEAT) && !(msg->flags&IIC_MSG_FAIL)){
    ifc->master_queue=msg->next;
  }else{
    iic_queue_msg(msg->flags&IIC_MSG_NOPROC?NULL:&ifc->proc_queue,msg);
  }
  msg->flags|=IIC_MSG_FINISHED;
  return 0;
}

static int bit_iic_ctrl_fnc(struct iic_ifc *ifc, int ctrl, void *p)
{
  switch(ctrl){
    case IIC_CTRL_MS_RQ:
      if(!(ifc->flags&IIC_IFC_ON))
        return -1;
      if(!ifc->master_queue)
        return 0;
      bit_iic_sfnc_default(ifc, 0);
      return 0;
    default:
      return -1;
  }
  return 0;
}

int bit_iic_poll(void)
{
  iic_ifc_t *ifc=&bit_iic_ifc;
  iic_msg_head_t *msg;

  if((msg=ifc->proc_queue)!=NULL){
    iic_queue_msg(NULL,msg);
    if((msg->flags&IIC_MSG_CB_PROC) && (msg->callback))
      msg->callback(ifc,IIC_MSG_CB_PROC,msg);
  }  
  return 0;
}

int bit_iic_init(void)
{
  iic_ifc_t *ifc=&bit_iic_ifc;
  ifc->self_addr=0x10;

  atomic_clear_mask_b1(IIC_IFC_ON,&ifc->flags);

  /* Setup IIC interface */

  ifc->master_queue=NULL;
  ifc->slave_queue=NULL;  
  ifc->sfnc_act=bit_iic_sfnc_default;
  ifc->ctrl_fnc=bit_iic_ctrl_fnc;
  ifc->poll_fnc=bit_iic_poll;

  BIIC_INIT_PORT();
  
  atomic_set_mask_b1(IIC_IFC_ON,&ifc->flags);

  return 0;
}

iic_ifc_t *iic_find_ifc(char *name, int number)
{
  int ret;
  if(number&0xff) return NULL;
  if(!(bit_iic_ifc.flags&IIC_IFC_ON)){
    ret=bit_iic_init();
    if(ret<0) return NULL;
  }
  return &bit_iic_ifc;
}

/********************************************************************/
/* Test code */

#include <utils.h>
#include <stdio.h>
#include <ctype.h>
#include <cmd_proc.h>

#define TEST_BUF 32

#if 0
/* connection of PCF8582 at addr 0xA0 */

/* connection of MA_KBD at addr 0x7A  */
/* display ctrl  */ {/*TEC*/0x51,/*CON*/0x1,/*addr*/0x10}
/* display ctrl  */ {/*TEC*/0x51,/*SREP*/0x2,/*PUSH*/0x7,/*REP*/0x4}
/* keyboard state*/ {/*TEK*/0x52} */
/* clear display */ {/*TED*/0x53,/*CLR*/0x1}
/* cursor show   */ {/*TED*/0x53,/*CP */0x2,/*CP_T*/1,/*CP_X*/3,/*CP_Y*/1}
/* beep          */ {/*TED*/0x53,/*BEEP*/0x3,/*time*/10}
/* show on LED   */ {/*TED*/0x53,/*LED*/0x4,/*LED*/0x55,/*BLINK*/0x44,/*HI*/0x5}
/* print line    */ {/*TED*/0x53,/*PRT*/0x81,'A','H','O','J'}

IICMST: INIT
IICMST: 0A4 TX (30,31,32,33,34,35,36)
IICPOLL:
IICMST: 0A4 TX (31)
IICPOLL:
IICMST: RX 4
IICPOLL:
IICMST: 0A4 TX (2F) RX 8
IICPOLL:
IICSTAT?

IICMST: 0A0 TX (0) RX 16
IICPOLL:
IICMST: 0A0 TX (55,33,44,55,66)
IICPOLL:
IICMST: 0A0 TX (56) RX 3
IICPOLL:
IICMST: 0A0 TX (56)
IICPOLL:
IICMST: 0A0 RX 6
IICPOLL:

IICMST: 07A TX (53,01)
IICPOLL:
IICMST: 07A TX (53,81,41,31,32)
IICPOLL:
IICMST: 07A RX 5
IICPOLL:
IICMST: 07A TX (53,81,41,31,32) RX 5
IICPOLL:
IICMST: 016 TX(1,2,3)
IICMST: DISP1
IICPOLL:MSRQ
IICPOLL:
IICSTAT?
IICSTAT: SDA=1
IICSTAT: SCL=1
IICSTAT: SDA=0
IICSTAT: SCL=0

p (void(*)(void))iic_hisl_buf[0]@256
set $ICCR0=(volatile char*)0xFFFF78
p /x *$ICCR0

#endif

uint8_t test_buf_tx[TEST_BUF]={0x10,0x11,0x22,0x33};
uint8_t test_buf_rx[TEST_BUF];

int test_callback(struct iic_ifc *ifc, int code, struct iic_msg_head *msg);

iic_msg_head_t test_msg={
    tx_buf:test_buf_tx,
    rx_buf:test_buf_rx,

    tx_rq:1,
    rx_rq:3,
    addr:0xA0,
    flags:IIC_MSG_MS_TX|IIC_MSG_MS_RX|IIC_MSG_CB_PROC,
    callback:test_callback
  };


int test_callback(struct iic_ifc *ifc, int code, struct iic_msg_head *msg)
{
  int i;
  
  if(code!=IIC_MSG_CB_PROC) return 0;
  printf("IIC!%s %02X ",msg->flags&IIC_MSG_SLAVE?"SLV":"MST",msg->addr);
  if(msg->flags&IIC_MSG_FAIL) printf("FAIL ");
  if(msg->flags&IIC_MSG_TX){
    printf("TX(");
    for(i=0;i<msg->tx_len;i++) printf("%s%02X",i?",":"",msg->tx_buf[i]);
    printf(")"); 
  }
  if(msg->flags&IIC_MSG_RX){
    printf("RX(");
    for(i=0;i<msg->rx_len;i++) printf("%s%02X",i?",":"",msg->rx_buf[i]);
    printf(")"); 
  }
  printf("\n");
  return 0;
}


unsigned t_mkbd_cnt=0;

uint8_t t_mkbd_buf_tx[TEST_BUF];
uint8_t t_mkbd_buf_rx[TEST_BUF];

int t_mkbd_callback(struct iic_ifc *ifc, int code, struct iic_msg_head *msg);

iic_msg_head_t t_mkbd_msg={
    tx_buf:t_mkbd_buf_tx,
    rx_buf:t_mkbd_buf_rx,

    tx_rq:1,
    rx_rq:0,
    addr:0x7A,
    flags:IIC_MSG_MS_TX|IIC_MSG_REPEAT|\
    IIC_MSG_CB_PROC|IIC_MSG_CB_START|IIC_MSG_CB_END,
    callback:t_mkbd_callback
  };


int t_mkbd_callback(struct iic_ifc *ifc, int code, struct iic_msg_head *msg)
{
  unsigned u,v;
  uint8_t *p;
  
  if(code==IIC_MSG_CB_PROC) return test_callback(ifc, code, msg);
  switch(code){
    case IIC_MSG_CB_START :
      p=msg->tx_buf;
      *(p++)=0x53; *(p++)=0x81;
      v=t_mkbd_cnt;
      for(u=10000;u;u/=10){
        *(p++)=v/u+'0'; v%=u;
      }
      msg->tx_rq=p-msg->tx_buf;
      t_mkbd_cnt++;
      break;
  }
  return 0;
}


int test_rd_arr(char **ps, uint8_t *buf, int n)
{
  long val;
  int c;
  int i;
  
  if(si_fndsep(ps,"({")<0) return -CMDERR_BADSEP;
  i=0;
  si_skspace(ps);
  if((**ps!=')') && (**ps!='}')) do{
    if(i>=n) return -CMDERR_BADPAR;
    if(si_long(ps,&val,16)<0) return -CMDERR_BADPAR; 
    buf[i]=val;
    i++;
    if((c=si_fndsep(ps,",)}"))<0) return -CMDERR_BADSEP;
  }while(c==',');
  return i;
}

int cmd_do_iicmst(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  iic_ifc_t *ifc=&bit_iic_ifc;
  iic_msg_head_t *msg=&test_msg;
  char cmd[10];
  int i;
  char *p;
  long val;
  char msgchg=0;

  if(*param[2]!=':') return -CMDERR_OPCHAR;

  if(!(ifc->flags&IIC_IFC_ON)){
    if(bit_iic_init()<0)
      return -CMDERR_BADCFG;
  }
  
  iic_master_msg_rem(ifc,msg);
  msg->flags&=~IIC_MSG_FAIL;
  p=param[3];
  
  si_skspace(&p);
  if(isdigit(*p)){
    if(si_long(&p,&val,16)<0) return -CMDERR_BADPAR; 
    msg->addr=val;
  }
  do{
    si_skspace(&p);
    if(!*p) break;
    si_alnumn(&p,cmd,8);
    if(!strcmp(cmd,"TX")){
      msg->flags|=IIC_MSG_MS_TX;
      if(!msgchg) msg->flags&=~IIC_MSG_MS_RX;
      msgchg=1;
      i=test_rd_arr(&p, msg->tx_buf, TEST_BUF);
      if(i<0) return i;
      msg->tx_rq=i;
    }else if(!strcmp(cmd,"RX")){
      msg->flags|=IIC_MSG_MS_RX;
      if(!msgchg) msg->flags&=~IIC_MSG_MS_TX;
      msgchg=1;
      if(si_long(&p,&val,0)<0) return -CMDERR_BADPAR; 
      msg->rx_rq=val;
    }else if(!strcmp(cmd,"INIT")){
      atomic_clear_mask_b1(IIC_IFC_ON,&ifc->flags);
      iic_flush_all(ifc);
      return (bit_iic_init()<0)?-CMDERR_BADCFG:0;
    }else if(!strcmp(cmd,"DISP1")){
      msg=&t_mkbd_msg;
      iic_master_msg_rem(ifc,msg);
      t_mkbd_cnt=123;
      iic_master_msg_ins(ifc,msg);
      return 0;
    }else return -CMDERR_BADPAR;
  }while(1);
  
  /*
  test_msg.tx_rq=4;
  test_msg.rx_rq=3;
  test_msg.addr=0xA0;
  test_msg.flags=IIC_MSG_MS_TX;
  */
  
  iic_master_msg_ins(ifc,msg);
  
  cmd_io_write(cmd_io,param[0],param[2]-param[0]);
  cmd_io_putc(cmd_io,'=');
  cmd_io_write(cmd_io,param[3],strlen(param[3]));
  return 0; 
}

int cmd_do_iicpoll(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  iic_ifc_t *ifc=&bit_iic_ifc;
  char *p;
  char str[20];
  int ret;
  if(*param[2]!=':') return -CMDERR_OPCHAR;
  if(!(ifc->flags&IIC_IFC_ON)) return -CMDERR_BADCFG;
  p=param[3];
  si_skspace(&p);
  si_alnumn(&p,str,8);
  if(!strcmp(str,"MSRQ"))
    bit_iic_ctrl_fnc(ifc, IIC_CTRL_MS_RQ, NULL);

  ret=bit_iic_poll();

  cmd_io_write(cmd_io,param[0],param[2]-param[0]);
  cmd_io_putc(cmd_io,'=');
  i2str(str,(long)ret,0,0);
  cmd_io_write(cmd_io,str,strlen(str));
  return 0; 
}

int cmd_do_iicstat(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int opchr=*param[2];
  char *p=param[3];
  char s[10];
  long val;

  switch(opchr){
    case ':' :
      si_skspace(&p);
      si_alphan(&p,s,10);
      si_fndsep(&p,"=");
      si_skspace(&p);
      si_long(&p,&val,0);
      if(!strcmp(s,"SDA"))
        BIIC_SDA_SET(val?1:0);
      else if(!strcmp(s,"SCL"))
        BIIC_SCL_SET(val?1:0);
      else return -CMDERR_BADPAR;
      break;

    case '?' :
      printf("SDA=%d SCL=%d\n",BIIC_SDA_GET()?1:0,BIIC_SCL_GET()?1:0);
      break;

    default:
      return -CMDERR_OPCHAR;
  }
  return 0; 
}


cmd_des_t const cmd_des_iicmst={0, CDESM_OPCHR,
			"IICMST","IIC master communication request",
			cmd_do_iicmst,{}};

cmd_des_t const cmd_des_iicpoll={0, CDESM_OPCHR,
			"IICPOLL","IIC poll",
			cmd_do_iicpoll,{}};

cmd_des_t const cmd_des_iicstat={0, CDESM_OPCHR,
			"IICSTAT","IIC status",
			cmd_do_iicstat,{}};

const cmd_des_t *cmd_iic_test[]={
  &cmd_des_iicmst,
  &cmd_des_iicpoll,
  &cmd_des_iicstat,
  NULL
};
