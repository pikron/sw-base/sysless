/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  h8_iic.h - IIC communication automata for H8S 2633 microcontroller
             this version suffers by some timing related problems
 
  Copyright (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#include <stdint.h>
#include <cpu_def.h>
#include <mcu_regs.h>
#include <system_def.h>
#include <malloc.h>
#include <string.h>
#include <periph/iic_ifc.h>

#define IIC_WITH_IRQ
//define IIC_MSRX_WITH_WAIT

#ifdef IIC_WITH_IRQ
  #define DEB_LOG(x,args...)
#else
  #if 0
    #define DEB_LOG printf
    #include <stdio.h>
  #else
    #define DEB_LOG(x,args...)
  #endif
#endif

#if 1
  #define IIC_HISL_SIZE 256
  void *iic_hisl_buf[IIC_HISL_SIZE];
  int iic_hisl_ptr=0;
  #define IIC_HISL(val) \
    do { \
      if(iic_hisl_ptr>=sizeof(iic_hisl_buf)/sizeof(iic_hisl_buf[0])) \
        iic_hisl_ptr=0; \
      iic_hisl_buf[iic_hisl_ptr++]=(void*)val; \
    } while(0)
#else
  #define IIC_HISL(fnc,val) 
#endif

#define IIC_ICCR   IIC_ICCR0
#define IIC_ICSR   IIC_ICSR0
#define IIC_ICDR   IIC_ICDR0
#define IIC_ICMR   IIC_ICMR0
#define IIC_SAR    IIC_SAR0
#define IIC_SARX   IIC_SARX0

#define EXCPTVEC_IICI EXCPTVEC_IICI0

#define iicx_pwr_on()  (*SYS_MSTPCRB&=~MSTPCRB_IIC0m)

#define iicx_registers_enable() atomic_set_mask_b1(SCRX_IICEm,IIC_SCRX)

#ifdef IIC_WITH_IRQ
  #define IIC_ICCR_SET(scp,iric,bbsy,acke,trs,mst) \
    do { \
       *IIC_ICCR=(scp?ICCR_SCPm:0)|(iric?ICCR_IRICm:0)|(bbsy?ICCR_BBSYm:0)| \
                 (acke?ICCR_ACKEm:0)|(trs?ICCR_TRSm:0)|(mst?ICCR_MSTm:0)| \
                 (ICCR_IEICm*1)|(ICCR_ICEm*1); \
    } while(0)
#else /* IIC_WITH_IRQ */
  #define IIC_ICCR_SET(scp,iric,bbsy,acke,trs,mst) \
    do { \
       *IIC_ICCR=(scp?ICCR_SCPm:0)|(iric?ICCR_IRICm:0)|(bbsy?ICCR_BBSYm:0)| \
                 (acke?ICCR_ACKEm:0)|(trs?ICCR_TRSm:0)|(mst?ICCR_MSTm:0)| \
                 (ICCR_IEICm*0)|(ICCR_ICEm*1); \
    } while(0)
#endif /* IIC_WITH_IRQ */

volatile void FlWait(long n);

iic_ifc_t iicx_ifc;

/********************************************************************/
/* Generic IIC functions */


void iic_queue_msg(iic_msg_head_t **queue, iic_msg_head_t *msg)
{
  unsigned short saveif;
  iic_msg_head_t *prev, *next;
  save_and_cli(saveif);
  if(msg->on_queue){
    if(msg->next==msg){
      if(*msg->on_queue==msg)
        *msg->on_queue=NULL;
    }else{
      msg->next->prev=msg->prev;
      msg->prev->next=msg->next;
      if(*msg->on_queue==msg)
        *msg->on_queue=msg->next;
    }
  }
  if((msg->on_queue=queue)!=NULL){
    if((next=*queue)!=NULL){
      msg->prev=prev=next->prev;
      msg->next=next;
      next->prev=msg;
      prev->next=msg;
    }else{
      *queue=msg->prev=msg->next=msg;
    }
  }
  restore_flags(saveif);
  return;
}

int iic_master_msg_ins(iic_ifc_t *ifc, iic_msg_head_t *msg)
{
  if(!(ifc->flags&IIC_IFC_ON)) return -1;
  if(!msg->tx_buf) msg->flags&=~IIC_MSG_MS_TX;
  if(!msg->rx_buf) msg->flags&=~IIC_MSG_MS_RX;
  iic_queue_msg(&ifc->master_queue,msg);
  ifc->ctrl_fnc(ifc,IIC_CTRL_MS_RQ,NULL);
  return 0;
}

int iic_master_msg_rem(iic_ifc_t *ifc, iic_msg_head_t *msg)
{
  unsigned short saveif;
  save_and_cli(saveif);
  iic_queue_msg(NULL,msg);
  if(msg==ifc->msg_act)
    ifc->msg_act=NULL;
  restore_flags(saveif);
  return 0;
}

int iicx_flush_all(iic_ifc_t *ifc)
{
  unsigned short saveif;
  iic_msg_head_t *msg, *next;
  iic_msg_head_t *queue[3];
  int quenum;

  save_and_cli(saveif);
    queue[0]=ifc->master_queue;
    queue[1]=ifc->slave_queue;
    queue[2]=ifc->proc_queue;
    ifc->master_queue=NULL;
    ifc->slave_queue=NULL;
    ifc->proc_queue=NULL;
    ifc->msg_act=NULL;
  restore_flags(saveif);
  for(quenum=0;quenum<3;quenum++){
    msg=queue[quenum];
    if(!msg) continue;
    msg->prev->next=NULL;
    for(;msg;msg=next){
      next=msg->next;
      msg->flags|=IIC_MSG_FAIL;
      msg->on_queue=NULL;
      if((msg->flags&IIC_MSG_CB_PROC) && (msg->callback))
	msg->callback(ifc,IIC_MSG_CB_PROC,msg);
    }
  }
  return 0;
}


/********************************************************************/
/* Hitachi specific IIC functions */

static int iicx_sfnc_default(struct iic_ifc *ifc, int code);
static int iicx_sfnc_ms_start(struct iic_ifc *ifc, int code);
static int iicx_sfnc_ms_tx(struct iic_ifc *ifc, int code);
static int iicx_sfnc_ms_rx0(struct iic_ifc *ifc, int code);
static int iicx_sfnc_ms_rx(struct iic_ifc *ifc, int code);
static int iicx_sfnc_ms_rx2(struct iic_ifc *ifc, int code);
#ifdef IIC_MSRX_WITH_WAIT
static int iicx_sfnc_ms_rx1(struct iic_ifc *ifc, int code); /*with WAIT only*/
static int iicx_sfnc_ms_rx3(struct iic_ifc *ifc, int code); /*with WAIT only*/
#endif /* IIC_MSRX_WITH_WAIT */
static int iicx_sfnc_ms_end(struct iic_ifc *ifc, int code);
static int iicx_sfnc_ms_err(struct iic_ifc *ifc, int code);

/* default handling and iddle state */
static int iicx_sfnc_default(struct iic_ifc *ifc, int code)
{
  iic_msg_head_t *msg;
  uint8_t v_icsr;
  
  ifc->msg_act=NULL;
  v_icsr=*IIC_ICSR;
  if(v_icsr&ICSR_ALm)
    *IIC_ICSR&=~ICSR_ALm;
  if(v_icsr&ICSR_ACKBm)
    *IIC_ICSR&=~ICSR_ACKBm;
 #ifdef IIC_MSRX_WITH_WAIT
  *IIC_ICMR&=~ICMR_WAITm;
 #endif /* IIC_MSRX_WITH_WAIT */
  if(!(code&ICCR_BBSYm)){
    /* mstx[2] Test the status of the SCL and SDA lines. */
    if((msg=ifc->master_queue)!=NULL){
      DEB_LOG("iicx_sfnc_default: START rq\n");
      /* mstx[3] Select master transmit mode. */
      IIC_ICCR_SET(/*SCP*/1,/*IRIC*/1,/*BBSY*/0,/*ACKE*/1,/*TRS*/1,/*MST*/1);
      /* mstx[4] Issue a start condition. */
      IIC_ICCR_SET(/*SCP*/0,/*IRIC*/0,/*BBSY*/1,/*ACKE*/1,/*TRS*/1,/*MST*/1);
      ifc->sfnc_act=iicx_sfnc_ms_start;
      return 0;
    }
  }else{
    if(code&ICCR_MSTm){
      /* Master mode */
      DEB_LOG("iicx_sfnc_default: STOP rq\n");
      IIC_ICCR_SET(/*SCP*/0,/*IRIC*/0,/*BBSY*/0,/*ACKE*/1,/*TRS*/1,/*MST*/1);
      return 0;
    }else{
      /* Slave mode */
      if(code&ICCR_TRSm){
        /* Transmit */
        DEB_LOG("iicx_sfnc_default: slave TX\n");
      
      }else{
        /* Receive */
        DEB_LOG("iicx_sfnc_default: slave RX\n");
    
      }
    }
  }
  ifc->sfnc_act=iicx_sfnc_default;
  atomic_clear_mask_b1(ICCR_IRICm,IIC_ICCR);
  return 0;
}

/* master start condition generated => send ADDR+R/W */
/* mstx[5] IRQ after wait for generation of start condition. */
static int iicx_sfnc_ms_start(struct iic_ifc *ifc, int code)
{
  iic_msg_head_t *msg;
  unsigned saveif;

  msg=ifc->master_queue;
  if(!msg || !(code&ICCR_MSTm))
    return iicx_sfnc_default(ifc, code);
  
  if((msg->flags&IIC_MSG_CB_START) && (msg->callback))
    msg->callback(ifc,IIC_MSG_CB_START,msg);
  
  ifc->msg_act=msg;
  if(msg->flags&IIC_MSG_MS_TX){
    DEB_LOG("iicx_sfnc_ms_start: master TX\n");
    /* mstx[6] Set transmit data for the first byte (slave address +R/W). */
    /*     (Perform ICDR write and IRIC flag clear operations consecutively.) */
    save_and_cli(saveif);
    *IIC_ICDR=msg->addr&~1;
    IIC_ICCR_SET(/*SCP*/1,/*IRIC*/0,/*BBSY*/1,/*ACKE*/1,/*TRS*/1,/*MST*/1);
    restore_flags(saveif);
    ifc->sfnc_act=iicx_sfnc_ms_tx;
    msg->tx_len=0;
  }else if(msg->flags&IIC_MSG_MS_RX){
    DEB_LOG("iicx_sfnc_ms_start: master RX\n");
    /* mstx[6] Set transmit data for the first byte (slave address +R/W). */
    /*     (Perform ICDR write and IRIC flag clear operations consecutively.) */
    save_and_cli(saveif);
    *IIC_ICDR=msg->addr|1;
    IIC_ICCR_SET(/*SCP*/1,/*IRIC*/0,/*BBSY*/1,/*ACKE*/1,/*TRS*/1,/*MST*/1);
    restore_flags(saveif);
    ifc->sfnc_act=iicx_sfnc_ms_rx;
    msg->rx_len=0;
  }else{
    return iicx_sfnc_ms_end(ifc, code);
  }

  return 0;
}

/* master data transmission */
/* mstx[7] or mstx[10] IRQ after wait for 1 byte to be transmitted. */
static int iicx_sfnc_ms_tx(struct iic_ifc *ifc, int code)
{
  iic_msg_head_t *msg=ifc->msg_act;
  uint8_t v_icsr;
  unsigned saveif;
  
  /* mstx[8] Test for acknowledgement by the designated slave device. */
  v_icsr=*IIC_ICSR;
  
  if(!msg || !(code&ICCR_MSTm) || (v_icsr&ICSR_ALm))
    return iicx_sfnc_default(ifc, code);
    
  if(v_icsr&ICSR_ACKBm)
    return iicx_sfnc_ms_err(ifc, code);
  
  /* mstx[11] Test for end of transfer. */
  if(msg->tx_len>=msg->tx_rq){
    if(!(msg->flags&IIC_MSG_MS_RX)) {
      /* *IIC_ICDR=0xff; */ /* suggested dummy write */
      return iicx_sfnc_ms_end(ifc, code);
    }
    DEB_LOG("iicx_sfnc_ms_tx: repeted start\n");

    FlWait(50);

    /* mstx[4] Issue a start condition. */
    IIC_ICCR_SET(/*SCP*/0,/*IRIC*/0,/*BBSY*/1,/*ACKE*/1,/*TRS*/1,/*MST*/1);
    ifc->sfnc_act=iicx_sfnc_ms_rx0;
    return 0;
  }
  
  DEB_LOG("iicx_sfnc_ms_tx: data send\n");
  /* mstx[9] Set transmit data for the second and subsequent bytes. */
  /* (Perform ICDR write and IRIC flag clear operations consecutively. */
  save_and_cli(saveif);
  *IIC_ICDR=msg->tx_buf[msg->tx_len++];
  IIC_ICCR_SET(/*SCP*/1,/*IRIC*/0,/*BBSY*/1,/*ACKE*/1,/*TRS*/1,/*MST*/1);
  restore_flags(saveif);
  return 0;
}

/* repeated start send => send ADDR+R */
/* mstx[5] Wait for generation of start condition. */
static int iicx_sfnc_ms_rx0(struct iic_ifc *ifc, int code)
{
  iic_msg_head_t *msg;
  unsigned saveif;

  msg=ifc->msg_act;
  if(!msg || !(code&ICCR_MSTm))
    return iicx_sfnc_default(ifc, code);

  DEB_LOG("iicx_sfnc_ms_rx0: master RX\n");
  /* mstx[6] Set transmit data for the first byte (slave address +R/W). */
  /*     (Perform ICDR write and IRIC flag clear operations consecutively.) */
  save_and_cli(saveif);
  *IIC_ICDR=msg->addr|1;
  IIC_ICCR_SET(/*SCP*/1,/*IRIC*/0,/*BBSY*/1,/*ACKE*/1,/*TRS*/1,/*MST*/1);
  restore_flags(saveif);
  ifc->sfnc_act=iicx_sfnc_ms_rx;
  msg->rx_len=0;

  return 0;
}

/* ADDR+R/W sent => prepare for receive */
/* mstx[7] IRQ after wait for 1 byte to be transmitted. */
static int iicx_sfnc_ms_rx(struct iic_ifc *ifc, int code)
{
  uint8_t dummy;
  uint8_t v_icsr;
  unsigned saveif;
  
  iic_msg_head_t *msg=ifc->msg_act;
  
  /* mstx[8] Test for acknowledgement by the designated slave device. */
  v_icsr=*IIC_ICSR;
  
  if(!msg || !(code&ICCR_MSTm) || (v_icsr&ICSR_ALm))
    return iicx_sfnc_default(ifc, code);
    
  if(*IIC_ICSR&ICSR_ACKBm)
    return iicx_sfnc_ms_err(ifc, code);
  
  DEB_LOG("iicx_sfnc_ms_rx: prepare\n");
  /* msrx[1] Select receive mode. */
  IIC_ICCR_SET(/*SCP*/1,/*IRIC*/1,/*BBSY*/1,/*ACKE*/1,/*TRS*/0,/*MST*/1);
  *IIC_ICSR&=~ICSR_ACKBm;
 #ifdef IIC_MSRX_WITH_WAIT
  *IIC_ICMR|=ICMR_WAITm;
 #else /* IIC_MSRX_WITH_WAIT */
  if(msg->rx_rq<=1){
    *IIC_ICSR|=ICSR_ACKBm;
  }
 #endif /* IIC_MSRX_WITH_WAIT */

  /* msrx[2] Start receiving.The first read is a dummy read. */
  /*      (Perform ICDR read and IRIC flag clear operations consecutively. */
  /* *IIC_ICDR=0xff; */
  
  /* FlWait(50); */

  save_and_cli(saveif);
  dummy=*IIC_ICDR;
  dummy=*IIC_ICCR;
  IIC_ICCR_SET(/*SCP*/1,/*IRIC*/0,/*BBSY*/1,/*ACKE*/1,/*TRS*/0,/*MST*/1);
  restore_flags(saveif);
 #ifdef IIC_MSRX_WITH_WAIT
  ifc->sfnc_act=iicx_sfnc_ms_rx1;
 #else /* IIC_MSRX_WITH_WAIT */
  ifc->sfnc_act=iicx_sfnc_ms_rx2;
 #endif /* IIC_MSRX_WITH_WAIT */
  return 0;
}

#ifdef IIC_MSRX_WITH_WAIT
/* master data reception, wait before ACK */
/* msrx[3] or msrx[8] IRQ after wait for the first byte to be received. */
static int iicx_sfnc_ms_rx1(struct iic_ifc *ifc, int code)
{
  iic_msg_head_t *msg=ifc->msg_act;

  if(!msg || !(code&ICCR_MSTm))
    return iicx_sfnc_default(ifc, code);

  if(msg->rx_rq<=msg->rx_len+1){
    /* msrx[10] Set acknowledge data for the last receive. */
    *IIC_ICSR|=ICSR_ACKBm;
    IIC_ICCR_SET(/*SCP*/1,/*IRIC*/1,/*BBSY*/1,/*ACKE*/0,/*TRS*/1,/*MST*/1);
    /* [11] Clear IRIC flag (clear wait). */
    IIC_ICCR_SET(/*SCP*/1,/*IRIC*/0,/*BBSY*/1,/*ACKE*/0,/*TRS*/1,/*MST*/1);
    ifc->sfnc_act=iicx_sfnc_ms_rx3;
  }else{
    /* msrx[4] or msrx[9] Clear IRIC flag (clear wait). */
    IIC_ICCR_SET(/*SCP*/1,/*IRIC*/0,/*BBSY*/1,/*ACKE*/1,/*TRS*/0,/*MST*/1);
    ifc->sfnc_act=iicx_sfnc_ms_rx2;
  }
  return 0;
}
#endif /* IIC_MSRX_WITH_WAIT */

/* master data reception */
/* msrx[5] or msrx[12] IRQ after wait for 1 byte to be received. */
static int iicx_sfnc_ms_rx2(struct iic_ifc *ifc, int code)
{
  iic_msg_head_t *msg=ifc->msg_act;
  
  if(!msg || !(code&ICCR_MSTm))
    return iicx_sfnc_default(ifc, code);
    
  DEB_LOG("iicx_sfnc_ms_rx2: data received\n");
  /* msrx[6] Read the receive data. */
  msg->rx_buf[msg->rx_len++]=*IIC_ICDR;

 #ifndef IIC_MSRX_WITH_WAIT
  if(msg->rx_len>=msg->rx_rq){
    return iicx_sfnc_ms_end(ifc, code);
  }
  
  if(msg->rx_rq<=msg->rx_len+1){
    *IIC_ICSR|=ICSR_ACKBm;
  }
 #endif /* IIC_MSRX_WITH_WAIT */
  /* msrx[7] Clear IRIC flag. */
  IIC_ICCR_SET(/*SCP*/1,/*IRIC*/0,/*BBSY*/1,/*ACKE*/1,/*TRS*/0,/*MST*/1);
 #ifdef IIC_MSRX_WITH_WAIT
  ifc->sfnc_act=iicx_sfnc_ms_rx1;
 #endif /* IIC_MSRX_WITH_WAIT */
  return 0;
}

#ifdef IIC_MSRX_WITH_WAIT
/* master last data reception */
/* msrx[12] IRQ after wait for 1 byte to be received. */
static int iicx_sfnc_ms_rx3(struct iic_ifc *ifc, int code)
{
  iic_msg_head_t *msg=ifc->msg_act;
  
  if(!msg || !(code&ICCR_MSTm))
    return iicx_sfnc_default(ifc, code);

  DEB_LOG("iicx_sfnc_ms_rx3: last data received\n");

  /* msrx[13] Clear wait mode. Read receive data. Clear IRIC flag. */
  /*       (Perform IRIC flag clearing while WAIT =0.) */
  *IIC_ICMR&=~ICMR_WAITm;
  msg->rx_buf[msg->rx_len++]=*IIC_ICDR;
  
  /* msrx[13a] and msrx[14] Issue a stop condition. */
  return iicx_sfnc_ms_end(ifc, code);
}
#endif /* IIC_MSRX_WITH_WAIT */

/* master transaction finished => generate STOP */
static int iicx_sfnc_ms_end(struct iic_ifc *ifc, int code)
{
  iic_msg_head_t *msg=ifc->msg_act;
  DEB_LOG("iicx_sfnc_ms_end: master mode end\n");
  
  FlWait(50);
  
  /* mstx[12] or msrx[13a+14] Issue a stop condition. */
  *IIC_ICMR&=~ICMR_WAITm; /* ?!?!?! */
  /*atomic_clear_mask_b1(ICCR_IRICm,IIC_ICCR);*/ /* ?!?!?! */
  IIC_ICCR_SET(/*SCP*/0,/*IRIC*/0,/*BBSY*/0,/*ACKE*/1,/*TRS*/1,/*MST*/1);
  ifc->sfnc_act=iicx_sfnc_default;
  if((msg->flags&IIC_MSG_CB_END) && (msg->callback))
    msg->callback(ifc,IIC_MSG_CB_END,msg);
  if(msg->flags&IIC_MSG_REPEAT){
    ifc->master_queue=msg->next;
  }else{
    iic_queue_msg(msg->flags&IIC_MSG_NOPROC?NULL:&ifc->proc_queue,msg);
  }
  msg->flags|=IIC_MSG_FINISHED;
  return 0;
}

/* master mode error condition */
static int iicx_sfnc_ms_err(struct iic_ifc *ifc, int code)
{
  iic_msg_head_t *msg=ifc->msg_act;
  DEB_LOG("iicx_sfnc_ms_err: master mode end\n");
  *IIC_ICSR&=~(ICSR_ACKBm|ICSR_ALm);

  FlWait(50);

  *IIC_ICMR&=~ICMR_WAITm; /* ?!?!?! */
  atomic_clear_mask_b1(ICCR_IRICm,IIC_ICCR); /* ?!?!?! */
  IIC_ICCR_SET(/*SCP*/0,/*IRIC*/0,/*BBSY*/0,/*ACKE*/1,/*TRS*/1,/*MST*/1);
  ifc->failed=ifc->sfnc_act;
  ifc->sfnc_act=iicx_sfnc_default;
  msg->flags|=IIC_MSG_FAIL;
  if((msg->flags&IIC_MSG_CB_END) && (msg->callback))
    msg->callback(ifc,IIC_MSG_CB_END,msg);
  if(msg->flags&IIC_MSG_FAIL){
    iic_queue_msg(&ifc->proc_queue,msg);
  }else if(msg->flags&IIC_MSG_REPEAT){
    ifc->master_queue=msg->next;
  }else{
    iic_queue_msg(msg->flags&IIC_MSG_NOPROC?NULL:&ifc->proc_queue,msg);
  }
  msg->flags|=IIC_MSG_FINISHED;
  return 0;
}

void iicx_isr(void) __attribute__ ((interrupt_handler));

void iicx_isr(void)
{
  uint8_t c;
  int ret;
  iic_ifc_t *ifc=&iicx_ifc;
  
  iicx_registers_enable();
  c=*IIC_ICCR;
  if(!(c&ICCR_IRICm)) return;
  /*FlWait(50);*/
  IIC_HISL(ifc->sfnc_act); IIC_HISL((long)c);
  ret=ifc->sfnc_act(ifc,c);
}

int iicx_poll(void)
{
  iic_ifc_t *ifc=&iicx_ifc;
  iic_msg_head_t *msg;

 #ifndef IIC_WITH_IRQ
  { 
    long i;
    for(i=0;i<100000;i++)
      iicx_isr();
  }
 #endif /* IIC_WITH_IRQ */
  if((msg=ifc->proc_queue)!=NULL){
    iic_queue_msg(NULL,msg);
    if((msg->flags&IIC_MSG_CB_PROC) && (msg->callback))
      msg->callback(ifc,IIC_MSG_CB_PROC,msg);
  }  
  return 0;
}

static int iicx_ctrl_fnc(struct iic_ifc *ifc, int ctrl, void *p)
{
  unsigned short saveif;
  switch(ctrl){
    case IIC_CTRL_MS_RQ:
      if(!(ifc->flags&IIC_IFC_ON))
        return -1;
      if(!ifc->master_queue)
        return 0;
      save_and_cli(saveif);
      iicx_registers_enable();
      if(!(*IIC_ICCR&ICCR_BBSYm)){
        IIC_ICCR_SET(/*SCP*/0,/*IRIC*/0,/*BBSY*/1,/*ACKE*/1,/*TRS*/1,/*MST*/1);
        ifc->sfnc_act=iicx_sfnc_ms_start;
      }
      restore_flags(saveif);
      DEB_LOG("iicx_ctrl_fnc: START rq\n");
      return 0;
    default:
      return -1;
  }
  return 0;
}

int iicx_init(void)
{
  iic_ifc_t *ifc=&iicx_ifc;
  ifc->self_addr=0x10;

  atomic_clear_mask_b1(IIC_IFC_ON,&ifc->flags);

  /* Power on IIC interface */
  iicx_pwr_on();
  /* Enable access to ICCR, ICSR, ICDR/SARX, ICMR/SAR */
  /* atomic_set_mask_b1(SCRX_IICEm,IIC_SCRX); */
  iicx_registers_enable();
  *IIC_ICCR=0;
  
  *IIC_SAR=ifc->self_addr;  /* self address */
  *IIC_SARX=0x01; /* disabled */
  *IIC_ICCR=ICCR_ICEm;
  /* Baudrate selection  fi/256 */
  atomic_set_mask_b1(SCRX_IICX1m,IIC_SCRX);
  *IIC_ICMR=__val2mfld(ICMR_CKSm,7);

  ifc->master_queue=NULL;
  ifc->slave_queue=NULL;  
  ifc->sfnc_act=iicx_sfnc_default;
  ifc->ctrl_fnc=iicx_ctrl_fnc;
  ifc->poll_fnc=iicx_poll;

  excptvec_set(EXCPTVEC_IICI,iicx_isr);

  atomic_set_mask_b1(IIC_IFC_ON,&ifc->flags);

 #ifdef IIC_WITH_IRQ
  *IIC_ICCR=ICCR_ICEm|ICCR_IEICm;
 #endif /* IIC_WITH_IRQ */

  return 0;
}

iic_ifc_t *iic_find_ifc(char *name, int number)
{
  int ret;
  if(number&0xff) return NULL;
  if(!(iicx_ifc.flags&IIC_IFC_ON)){
    ret=iicx_init();
    if(ret<0) return NULL;
  }
  return &iicx_ifc;
}

/********************************************************************/
/* Test code */

#include <utils.h>
#include <stdio.h>
#include <ctype.h>
#include <cmd_proc.h>

#define TEST_BUF 32

#if 0
/* support support@hmse.com */
/* connection of PCF8582 at addr 0xA0 */

/* connection of MA_KBD at addr 0x7A  */
/* display ctrl  */ {/*TEC*/0x51,/*CON*/0x1,/*addr*/0x10}
/* display ctrl  */ {/*TEC*/0x51,/*SREP*/0x2,/*PUSH*/0x7,/*REP*/0x4}
/* keyboard state*/ {/*TEK*/0x52} */
/* clear display */ {/*TED*/0x53,/*CLR*/0x1}
/* cursor show   */ {/*TED*/0x53,/*CP */0x2,/*CP_T*/1,/*CP_X*/3,/*CP_Y*/1}
/* beep          */ {/*TED*/0x53,/*BEEP*/0x3,/*time*/10}
/* show on LED   */ {/*TED*/0x53,/*LED*/0x4,/*LED*/0x55,/*BLINK*/0x44,/*HI*/0x5}
/* print line    */ {/*TED*/0x53,/*PRT*/0x81,'A','H','O','J'}

IICMST: INIT
IICMST: 0A4 TX (30,31,32,33,34,35,36)
IICPOLL:
IICMST: 0A4 TX (31)
IICMST: 0A4 TX (31) RX 4
IICPOLL:
IICMST: RX 4
IICPOLL:
IICMST: 0A4 TX (2F) RX 8
IICPOLL:
IICSTAT?

IICMST: 07A TX (53,01)
IICPOLL:
IICMST: 07A TX (53,81,41,31,32)
IICPOLL:
IICMST: 07A RX 5
IICPOLL:
IICMST: 07A TX (53,81,41,31,32) RX 5
IICPOLL:
IICMST: 016 TX(1,2,3)
IICMST: 016 RX 10
IICMST: DISP1
IICPOLL:MSRQ
IICPOLL:
IICSTAT?
IICSTAT: ICCR=0xB0
IICSTAT: ICCR=0xF0
IICSTAT: ICCR=0x80
IICSTAT: ICCR=0xb6
IICSTAT: ICCR=0xb1
IICSTAT: ICCR=0xb2
IICSTAT: ICCR=0xb0
IICSTAT: ICSR=0x00
IICSTAT: DDCSWR=0x03
IICSTAT: ICDR=0xA4

p (void(*)(void))iic_hisl_buf[0]@256
set $ICCR0=(volatile char*)0xFFFF78
p /x *$ICCR0

#endif

uint8_t test_buf_tx[TEST_BUF]={0x10,0x11,0x22,0x33};
uint8_t test_buf_rx[TEST_BUF];

int test_callback(struct iic_ifc *ifc, int code, struct iic_msg_head *msg);

iic_msg_head_t test_msg={
    tx_buf:test_buf_tx,
    rx_buf:test_buf_rx,

    tx_rq:1,
    rx_rq:3,
    addr:0xA0,
    flags:IIC_MSG_MS_TX|IIC_MSG_MS_RX|IIC_MSG_CB_PROC,
    callback:test_callback
  };


int test_callback(struct iic_ifc *ifc, int code, struct iic_msg_head *msg)
{
  int i;
  
  if(code!=IIC_MSG_CB_PROC) return 0;
  printf("IIC!%s %02X ",msg->flags&IIC_MSG_SLAVE?"SLV":"MST",msg->addr);
  if(msg->flags&IIC_MSG_FAIL) printf("FAIL ");
  if(msg->flags&IIC_MSG_TX){
    printf("TX(");
    for(i=0;i<msg->tx_len;i++) printf("%s%02X",i?",":"",msg->tx_buf[i]);
    printf(")"); 
  }
  if(msg->flags&IIC_MSG_RX){
    printf("RX(");
    for(i=0;i<msg->rx_len;i++) printf("%s%02X",i?",":"",msg->rx_buf[i]);
    printf(")"); 
  }
  printf("\n");
  return 0;
}


unsigned t_mkbd_cnt=0;

uint8_t t_mkbd_buf_tx[TEST_BUF];
uint8_t t_mkbd_buf_rx[TEST_BUF];

int t_mkbd_callback(struct iic_ifc *ifc, int code, struct iic_msg_head *msg);

iic_msg_head_t t_mkbd_msg={
    tx_buf:t_mkbd_buf_tx,
    rx_buf:t_mkbd_buf_rx,

    tx_rq:1,
    rx_rq:0,
    addr:0x7A,
    flags:IIC_MSG_MS_TX|IIC_MSG_REPEAT|\
    IIC_MSG_CB_PROC|IIC_MSG_CB_START|IIC_MSG_CB_END,
    callback:t_mkbd_callback
  };


int t_mkbd_callback(struct iic_ifc *ifc, int code, struct iic_msg_head *msg)
{
  unsigned u,v;
  uint8_t *p;
  
  if(code==IIC_MSG_CB_PROC) return test_callback(ifc, code, msg);
  switch(code){
    case IIC_MSG_CB_START :
      p=msg->tx_buf;
      *(p++)=0x53; *(p++)=0x81;
      v=t_mkbd_cnt;
      for(u=10000;u;u/=10){
        *(p++)=v/u+'0'; v%=u;
      }
      msg->tx_rq=p-msg->tx_buf;
      t_mkbd_cnt++;
      break;
  }
  return 0;
}


int test_rd_arr(char **ps, uint8_t *buf, int n)
{
  long val;
  int c;
  int i;
  
  if(si_fndsep(ps,"({")<0) return -CMDERR_BADSEP;
  i=0;
  si_skspace(ps);
  if((**ps!=')') && (**ps!='}')) do{
    if(i>=n) return -CMDERR_BADPAR;
    if(si_long(ps,&val,16)<0) return -CMDERR_BADPAR; 
    buf[i]=val;
    i++;
    if((c=si_fndsep(ps,",)}"))<0) return -CMDERR_BADSEP;
  }while(c==',');
  return i;
}

int cmd_do_iicmst(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  iic_ifc_t *ifc=&iicx_ifc;
  iic_msg_head_t *msg=&test_msg;
  char cmd[10];
  int i;
  char *p;
  long val;
  char msgchg=0;

  if(*param[2]!=':') return -CMDERR_OPCHAR;

  if(!(ifc->flags&IIC_IFC_ON)){
    if(iicx_init()<0)
      return -CMDERR_BADCFG;
  }
  
  iic_master_msg_rem(ifc,msg);
  msg->flags&=~IIC_MSG_FAIL;
  p=param[3];
  
  si_skspace(&p);
  if(isdigit(*p)){
    if(si_long(&p,&val,16)<0) return -CMDERR_BADPAR; 
    msg->addr=val;
  }
  do{
    si_skspace(&p);
    if(!*p) break;
    si_alnumn(&p,cmd,8);
    if(!strcmp(cmd,"TX")){
      msg->flags|=IIC_MSG_MS_TX;
      if(!msgchg) msg->flags&=~IIC_MSG_MS_RX;
      msgchg=1;
      i=test_rd_arr(&p, msg->tx_buf, TEST_BUF);
      if(i<0) return i;
      msg->tx_rq=i;
    }else if(!strcmp(cmd,"RX")){
      msg->flags|=IIC_MSG_MS_RX;
      if(!msgchg) msg->flags&=~IIC_MSG_MS_TX;
      msgchg=1;
      if(si_long(&p,&val,0)<0) return -CMDERR_BADPAR; 
      msg->rx_rq=val;
    }else if(!strcmp(cmd,"INIT")){
      atomic_clear_mask_b1(IIC_IFC_ON,&ifc->flags);
      iicx_flush_all(ifc);
      return (iicx_init()<0)?-CMDERR_BADCFG:0;
    }else if(!strcmp(cmd,"DISP1")){
      msg=&t_mkbd_msg;
      iic_master_msg_rem(ifc,msg);
      t_mkbd_cnt=123;
      iic_master_msg_ins(ifc,msg);
      return 0;
    }else return -CMDERR_BADPAR;
  }while(1);
  
  /*
  test_msg.tx_rq=4;
  test_msg.rx_rq=3;
  test_msg.addr=0xA0;
  test_msg.flags=IIC_MSG_MS_TX;
  */
  
  iic_master_msg_ins(ifc,msg);
  
  cmd_io_write(cmd_io,param[0],param[2]-param[0]);
  cmd_io_putc(cmd_io,'=');
  cmd_io_write(cmd_io,param[3],strlen(param[3]));
  return 0; 
}

int cmd_do_iicpoll(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  iic_ifc_t *ifc=&iicx_ifc;
  char *p;
  char str[20];
  int ret;
  if(*param[2]!=':') return -CMDERR_OPCHAR;
  if(!(ifc->flags&IIC_IFC_ON)) return -CMDERR_BADCFG;
  p=param[3];
  si_skspace(&p);
  si_alnumn(&p,str,8);
  if(!strcmp(str,"MSRQ"))
    iicx_ctrl_fnc(ifc, IIC_CTRL_MS_RQ, NULL);

  ret=iicx_poll();

  cmd_io_write(cmd_io,param[0],param[2]-param[0]);
  cmd_io_putc(cmd_io,'=');
  i2str(str,(long)ret,0,0);
  cmd_io_write(cmd_io,str,strlen(str));
  return 0; 
}

int cmd_do_iicstat(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  uint8_t v_iccr, v_icsr;
  int opchr=*param[2];
  char *p=param[3];
  char s[10];
  volatile uint8_t *pb;
  long val;
  unsigned short saveif;

  switch(opchr){
    case ':' :
      si_skspace(&p);
      si_alphan(&p,s,10);
      si_fndsep(&p,"=");
      si_skspace(&p);
      si_long(&p,&val,0);
      if(!strcmp(s,"ICCR"))
        pb=IIC_ICCR;
      else if(!strcmp(s,"ICSR"))
        pb=IIC_ICSR;
      else if(!strcmp(s,"ICDR"))
        pb=IIC_ICDR;
      else if(!strcmp(s,"DDCSWR"))
        pb=IIC_DDCSWR;
      else return -CMDERR_BADPAR;
      save_and_cli(saveif);
      iicx_registers_enable();
      *pb=val;
      restore_flags(saveif);
      break;

    case '?' :
      save_and_cli(saveif);
      iicx_registers_enable();
      v_iccr=*IIC_ICCR;
      v_icsr=*IIC_ICSR;
      restore_flags(saveif);

      printf("ICCR=%02X ICSR=%02X\n",v_iccr,v_icsr);
      break;

    default:
      return -CMDERR_OPCHAR;
  }
  return 0; 
}


cmd_des_t const cmd_des_iicmst={0, CDESM_OPCHR,
			"IICMST","IIC master communication request",
			cmd_do_iicmst,{}};

cmd_des_t const cmd_des_iicpoll={0, CDESM_OPCHR,
			"IICPOLL","IIC poll",
			cmd_do_iicpoll,{}};

cmd_des_t const cmd_des_iicstat={0, CDESM_OPCHR,
			"IICSTAT","IIC status",
			cmd_do_iicstat,{}};

const cmd_des_t *cmd_iic_test[]={
  &cmd_des_iicmst,
  &cmd_des_iicpoll,
  &cmd_des_iicstat,
  NULL
};
