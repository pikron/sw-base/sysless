/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  iic_ifc.h - IIC communication automat interface 
 
  Copyright (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#ifndef _H8_IIC_H_
#define _H8_IIC_H_

struct iic_ifc;

#define IIC_MSG_TX       0x001
#define IIC_MSG_RX       0x002
#define IIC_MSG_MS_TX    IIC_MSG_TX
#define IIC_MSG_MS_RX    IIC_MSG_RX
#define IIC_MSG_SL_TX    IIC_MSG_TX
#define IIC_MSG_SL_RX    IIC_MSG_RX
#define IIC_MSG_SLAVE    0x004
#define IIC_MSG_FAIL     0x008
#define IIC_MSG_REPEAT   0x010
#define IIC_MSG_NOPROC   0x020
#define IIC_MSG_FINISHED 0x040
#define IIC_MSG_CB_START 0x100
#define IIC_MSG_CB_END   0x200
#define IIC_MSG_CB_PROC  0x400

typedef struct iic_msg_head {
    uint16_t flags;	/* message flags */
    uint8_t  sl_cmd;	/* command for slave queue lookup */
    uint8_t  sl_msk;	/* sl_cmd match mask */
    uint16_t addr;	/* message destination address */
    uint16_t tx_rq;	/* requested TX transfer length */
    uint16_t rx_rq;	/* requested RX transfer length */
    uint16_t tx_len;	/* finished TX transfer length */
    uint16_t rx_len;	/* finished RX transfer length */
    uint8_t *tx_buf;	/* pointer to TX data */
    uint8_t *rx_buf;	/* pointer to RX data */
    struct iic_msg_head *prev;
    struct iic_msg_head *next;
    struct iic_msg_head **on_queue;
    int (*callback)(struct iic_ifc *ifc, int code, struct iic_msg_head *msg);
    void *private;
  } iic_msg_head_t;

typedef int (iic_sfnc_t)(struct iic_ifc *ifc, int code);
typedef int (iic_ctrl_fnc_t)(struct iic_ifc *ifc, int ctrl, void *p);

#define IIC_IFC_ON 1

typedef struct iic_ifc {
    uint8_t flags;
    uint16_t self_addr;
    iic_msg_head_t *master_queue;
    iic_msg_head_t *slave_queue;
    iic_msg_head_t *proc_queue;
    iic_msg_head_t *msg_act;
    iic_sfnc_t *sfnc_act;
    void *failed;
    iic_ctrl_fnc_t *ctrl_fnc;
    int (*poll_fnc)(void);
  } iic_ifc_t;

#define IIC_CTRL_MS_RQ 1

iic_ifc_t *iic_find_ifc(char *name, int number);
int iic_master_msg_ins(iic_ifc_t *ifc, iic_msg_head_t *msg);
int iic_master_msg_rem(iic_ifc_t *ifc, iic_msg_head_t *msg);
int iicx_flush_all(iic_ifc_t *ifc);

#endif /* _H8_IIC_H_ */
