/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  sci_rs232.h - H8S SCI interrupt driven RS-232 interface
 
  Copyright (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002 by PiKRON Ltd. http://www.pikron.com

*******************************************************************/

#ifndef _ID_RS232_H_
#define _ID_RS232_H_

#include <periph/sci_regs.h>
//*************************************************************
#define SCI_CHAR_XON  0x11
#define SCI_CHAR_XOFF 0x13
#define SCI_RS232_MODEA SCI_SMR_8N1
/* If the input buffer has only SCI_RS232_BUF_FULLTG bytes free, flow
 * control is used to stop the sender. */
#define SCI_RS232_BUF_FULLTG (50) 
#define RS232_BAUD_RAW 0xff00
#define SCI_RSFLC_HW  0x1
#define SCI_RSFLC_XON 0x2

#define SCI_RSFL_ROR   0x1	/* Overrun error */ /*fronta znaku je plna - doslo k prepsani (RXI)*/
#define SCI_RSFL_RFE   0x2	/* Framing error */
#define SCI_RSFL_RPE   0x4	/* Parity error */
#define SCI_RSFL_SXOFF 0x10	/* Request to send XOFF */
#define SCI_RSFL_SXON  0x20	/* Request to send XON */
#define SCI_RSFL_TFCDI 0x40	/* Transmission disabled */
#define SCI_RSFL_RFCDI 0x80	/* Reception disenabled */
#define SCI_RSFL_TIP   0x100	/* Transmittion at Progress */
#define SCI_RSFL_TWCTS 0x200	/* Delaying Tx to CTS enabled */

#define m_TDR()
#define m_RDR()
#define m_SMR()
#define m_SCMR()
#define m_SCR()
#define m_SSR()
#define m_BRR()

typedef struct{
    uint8_t *buf_beg; //start of adress structur
    uint8_t *buf_end; //end of adress structur - beg+sizeof(struct)
    uint8_t *ip;      //actual position at queue
    uint8_t *op;      //position first unread char of queue
} sci_que_t;

typedef struct sci_info {
  struct sci_regs *regs;
  int sci_rs232_baud;
  int sci_rs232_mode;
  int sci_rs232_flowc;
  short sci_rs232_flags;
  int sci_rs232_irq_cnt;

  /* Functions */
  void (*sci_rs232_init)(void); /* Poweres this port on and setup interrupt handlers */
  int  (*sci_rs232_rxd_pin)(void);  /* Reads the state of RxD pin */
  void (*sci_rs232_rts_true)(void); /* Sets RTS */
  void (*sci_rs232_rts_false)(void); /* Clears RTS */
  int  (*sci_rs232_cts)(void);  /* Reads CTS */

  /* Queues */
  sci_que_t sci_rs232_que_in;
  sci_que_t sci_rs232_que_out;
  uint8_t *sci_rs232_buf_in;
  int sci_rs232_buf_in_size;
  uint8_t *sci_rs232_buf_out;
  int sci_rs232_buf_out_size;
} sci_info_t;

int sci_rs232_sendch(int c, int chan);
int sci_rs232_recch(int chan);
int sci_rs232_sendstr(const char *s, int chan);

int sci_rs232_que_out_free(int chan);
int sci_rs232_que_in_ready(int chan);
int sci_rs232_setmode(long int baud, int mode, int flowc, int chan);

void sci_rs232_eri_isr(sci_info_t *sci);
void sci_rs232_rxi_isr(sci_info_t *sci);
void sci_rs232_txi_isr(sci_info_t *sci);
void sci_rs232_tei_isr(sci_info_t *sci);

/* HACK: Include machine specific definitions */
#include <periph/sci_channels.h>


#endif /* _ID_RS232_H_ */

/* Local variables: */
/* c-basic-offset:2 */
/* End: */
