/* Macros for definitions of default buffers for sci channels. */
#ifndef _SCIBUF_H
#define _SCIBUF_H

#define DECLARE_SCI_BUFS(chan)                  \
extern char sci_rs232_buf_in_##chan[];      \
extern int sci_rs232_buf_in_##chan##_size;  \
extern char sci_rs232_buf_out_##chan[];     \
extern int sci_rs232_buf_out_##chan##_size;


#define DEFINE_SCI_DEFAULT_BUFS(chan)                                           \
char sci_rs232_buf_in_##chan[64];                                           \
int sci_rs232_buf_in_##chan##_size = sizeof(sci_rs232_buf_in_##chan);   \
char sci_rs232_buf_out_##chan[128];                                         \
int sci_rs232_buf_out_##chan##_size = sizeof(sci_rs232_buf_out_##chan);


#endif
