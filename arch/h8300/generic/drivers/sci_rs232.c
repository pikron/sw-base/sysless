/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  sci_rs232.c - H8S SCI interrupt driven RS-232 interface
 
  Copyright (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002 by PiKRON Ltd. http://www.pikron.com
            (C) 2005 by Petr Kovacik <kovacp1@fel.cvut.cz>
            (C) 2005 by Michal Sojka <wentasah@centrum.cz>

 The COLAMI components can be used and copied according to next
 license alternatives
   - MPL - Mozilla Public License
   - GPL - GNU Public License

 *******************************************************************/

#include <stdint.h>
#include <cpu_def.h>
#include <mcu_regs.h>
#include <system_def.h>
#include <periph/sci_rs232.h>
#include <periph/sci_channels.h>

/* put character c into queue, if full return -1 */
inline int sci_que_put(sci_que_t *q, int c)
{
  uint8_t *p;
  p=q->ip;
  *(p++)=c;
  if(p==q->buf_end) p=q->buf_beg;
  if(p==q->op) return -1;
  q->ip=p;
  return c;
}

/* get character from queue, if empty return -1 */
inline int sci_que_get(sci_que_t *q)
{
  uint8_t *p;
  int  c;
  p=q->op;
  if(p==q->ip) return -1;
  c=*(p++);
  if(p==q->buf_end) p=q->buf_beg;
  q->op=p;
  return c;
}


/*return free space in queue*/
inline int sci_que_freecnt(sci_que_t *q)
{
  uint8_t *ip=q->ip,*op=q->op;
  return op-ip-1+(op<=ip?q->buf_end-q->buf_beg:0);
}

inline int sci_que_incnt(sci_que_t *q)
{
  uint8_t *ip=q->ip,*op=q->op;
  return ip-op+(ip<op?q->buf_end-q->buf_beg:0);
}

/* Actual interrupt handled - called from port specific ISR. */
void sci_rs232_eri_isr(sci_info_t *sci)
{
  struct sci_regs *regs = sci->regs;

  if(regs->rs232_ssr&SSR_ORERm) sci->sci_rs232_flags|=SCI_RSFL_ROR;
  if(regs->rs232_ssr&SSR_FERm) sci->sci_rs232_flags|=SCI_RSFL_RFE;      
  if(regs->rs232_ssr&SSR_PERm) sci->sci_rs232_flags|=SCI_RSFL_RPE;
  regs->rs232_ssr=~(SSR_PERm|SSR_FERm|SSR_ORERm);
}

/* Actual interrupt handled - called from port specific ISR. */
void sci_rs232_rxi_isr(sci_info_t *sci)
{
  int val;
  struct sci_regs *regs = sci->regs;

  regs->rs232_ssr&=~SSR_RDRFm;	/* clear receiption flag */

  val=regs->rs232_rdr;
  sci->sci_rs232_irq_cnt++;
  if(sci->sci_rs232_flowc&SCI_RSFLC_XON)
  {
    if(val==SCI_CHAR_XON)
      sci->sci_rs232_flags&=~SCI_RSFL_TFCDI;
    else if(val==SCI_CHAR_XOFF)
      sci->sci_rs232_flags|=SCI_RSFL_TFCDI;
    return;
  }
  if(sci_que_put(&sci->sci_rs232_que_in,val)<0)
    sci->sci_rs232_flags|=SCI_RSFL_ROR;
  if((!(sci->sci_rs232_flags&SCI_RSFL_RFCDI))&&
     (sci_que_freecnt(&sci->sci_rs232_que_in)<=SCI_RS232_BUF_FULLTG)) {
    sci->sci_rs232_flags|=SCI_RSFL_RFCDI;
    if(sci->sci_rs232_flowc&SCI_RSFLC_XON) {
      sci->sci_rs232_flags&=~SCI_RSFL_SXON;
      sci->sci_rs232_flags|=SCI_RSFL_SXOFF;
      atomic_clear_mask_b1(SCR_TEIEm,&regs->rs232_scr);
      atomic_set_mask_b1(SCR_TEm,&regs->rs232_scr);
      atomic_set_mask_b1(SCR_TIEm,&regs->rs232_scr);
    }
    if(sci->sci_rs232_flowc&SCI_RSFLC_HW) sci->sci_rs232_rts_false();
  }
}

/* Actual interrupt handled - called from port specific ISR. */
void sci_rs232_txi_isr(sci_info_t *sci)
{ 
  short val;
  struct sci_regs *regs = sci->regs;

  if(!(sci->sci_rs232_flowc&SCI_RSFLC_HW)||sci->sci_rs232_cts())
  {
    /* Clear to send */
    sci->sci_rs232_flowc&=~SCI_RSFL_TWCTS;
    if(sci->sci_rs232_flags&(SCI_RSFL_SXON|SCI_RSFL_SXOFF)) {
      /* There is an active request to send either XON or XOFF */
      val=sci->sci_rs232_flags&SCI_RSFL_SXON?SCI_CHAR_XON:SCI_CHAR_XOFF;
      sci->sci_rs232_flags&=~(SCI_RSFL_SXON|SCI_RSFL_SXOFF);
      sci->sci_rs232_flags|=SCI_RSFL_TIP;
      regs->rs232_tdr=val;
      atomic_set_mask_b1(SCR_TIEm,&regs->rs232_scr);
      return;
    } else {
      /* Send ordinary char */
      if(!(sci->sci_rs232_flags&SCI_RSFL_TFCDI)) {
        if((val=sci_que_get(&sci->sci_rs232_que_out))>=0)
        {
          sci->sci_rs232_flags|=SCI_RSFL_TIP;
          regs->rs232_tdr=val;
          regs->rs232_ssr &=~SSR_TDREm; 
          atomic_set_mask_b1(SCR_TIEm,&regs->rs232_scr); /**/
          return;
        }
      }
    }
  }
  else
  {
    /* sci->sci_rs232_flowc & SCI_RSFLC_HW && !CTS */
    /* Wait for clear to send. */
    sci->sci_rs232_flowc|=SCI_RSFL_TWCTS;
  }

  /* No character was sent. */
  sci->sci_rs232_flags&=~SCI_RSFL_TIP;
  atomic_clear_mask_b1(SCR_TIEm,&regs->rs232_scr);
  atomic_set_mask_b1(SCR_TEIEm,&regs->rs232_scr);
}


/* Actual interrupt handled - called from port specific ISR. */
void sci_rs232_tei_isr(sci_info_t *sci)
{
  struct sci_regs *regs = sci->regs;

  regs->rs232_ssr&=~SSR_TENDm;
    
  atomic_clear_mask_b1(SCR_TEIEm,&regs->rs232_scr);
  atomic_clear_mask_b1(SCR_TIEm,&regs->rs232_scr);
}

/**
 * sci_rs232_sendch - Write one character to RS232 output queue
 * @c:		output character
 *
 * Returns written character @c in case of success, returns -1
 * if queue is full.
 */

int sci_rs232_sendch(int c, int chan)
{
  sci_info_t *sci = sci_rs232_chan_array[chan];
  struct sci_regs *regs = sci->regs;

  /* FIXME: testing only */
/*   regs->rs232_tdr=c; */
/*   regs->rs232_ssr &=~SSR_TDREm; */
/*   atomic_set_mask_b1(SCR_TIEm,&regs->rs232_scr); /\**\/ */
/*   atomic_set_mask_b1(SCR_TEm,&regs->rs232_scr); */
/*   return c; */

  if(sci_que_put(&sci->sci_rs232_que_out,c)<0) {
    if((sci->sci_rs232_flags&SCI_RSFL_TWCTS)&&sci->sci_rs232_cts()) {
      atomic_set_mask_b1(SCR_TEm,&regs->rs232_scr);
      atomic_set_mask_b1(SCR_TIEm,&regs->rs232_scr);
    }
    c=-1;
  }
  if(sci->sci_rs232_flags&SCI_RSFL_TIP) return c;
  if(sci->sci_rs232_flags&SCI_RSFL_TFCDI) return c;
  if((sci->sci_rs232_flowc&SCI_RSFLC_HW)&&!sci->sci_rs232_cts()) {
    atomic_set_mask_w1(SCI_RSFL_TWCTS,&sci->sci_rs232_flags);
    return c;
  }
  atomic_set_mask_b1(SCR_TEm,&regs->rs232_scr);
  atomic_set_mask_b1(SCR_TIEm,&regs->rs232_scr);
  return c;
}
/**
 * sci_rs232_recch - Reads one character from RS232 input queue
 *
 * Returns received character in case of success, returns -1
 * if queue is empty.
 */
int sci_rs232_recch(int chan)
{
  int val;
  sci_info_t *sci = sci_rs232_chan_array[chan];
  struct sci_regs *regs = sci->regs;

  if((sci->sci_rs232_flags&SCI_RSFL_TWCTS)&&sci->sci_rs232_cts()) {
    atomic_set_mask_b1(SCR_TEm,&regs->rs232_scr);
    atomic_set_mask_b1(SCR_TIEm,&regs->rs232_scr);
  }
  val=sci_que_get(&sci->sci_rs232_que_in);

  if(sci->sci_rs232_flags&SCI_RSFL_RFCDI)
  {
    if(sci_que_freecnt(&sci->sci_rs232_que_in)>=SCI_RS232_BUF_FULLTG+8)
    {
      if(sci->sci_rs232_flowc&SCI_RSFLC_HW)
        sci->sci_rs232_rts_true();
      __memory_barrier();
      atomic_clear_mask_w1(SCI_RSFL_RFCDI,&sci->sci_rs232_flags);
      if(sci->sci_rs232_flowc&SCI_RSFLC_XON)
      {
        atomic_set_mask_w1(SCI_RSFL_SXON,&sci->sci_rs232_flags);
        if(!(sci->sci_rs232_flags&SCI_RSFL_TIP))
        {
          atomic_set_mask_b1(SCR_TEm,&regs->rs232_scr);
          atomic_set_mask_b1(SCR_TIEm,&regs->rs232_scr);
        }
      }
      __memory_barrier();
    }
  }
  return val;
}

/**
 * sci_rs232_sendstr - Sends string through RS232 line
 * @s:		String to send
 *
 * This function sends string @s, if output queue is full,
 * it returns count of successfully sent characters.
 */
int sci_rs232_sendstr(const char *s, int chan)
{
  int cnt=0;
  while(*s)
  {
    if(sci_rs232_sendch((unsigned char)(*(s++)), chan)<0) break;
    cnt++;
  }
  return cnt;
}

#if 1
/**
 * sci_rs232_que_out_free - Returns free space in output queue
 */
int sci_rs232_que_out_free(int chan)
{
  sci_info_t *sci = sci_rs232_chan_array[chan];
  return sci_que_freecnt(&sci->sci_rs232_que_out);
}

/**
 * sci_rs232_que_in_ready - Returns number of ready characters in input queue
 */
int sci_rs232_que_in_ready(int chan)
{
  sci_info_t *sci = sci_rs232_chan_array[chan];
  return sci_que_incnt(&sci->sci_rs232_que_in);
}

#endif
/**
 * sci_rs232_setmode - Sets RS232 line parameters
 * @baud:	Requested baud-rate or -1 for no change
 * @mode:	Mode of RS232 communication or -1 for no change.
 *		Predefined mode constant %SCI_SMR_8N1 describes
 *		eight bit mode, with no parity and one stop-bit.
 * @flowc:	Value 0 means no flow-control, value 1 requests hardware
 *		flow-control with RTS/CTS and value 2 enables software
 *		XON/XOFF flow control.
 */


int sci_rs232_setmode(long int baud, int mode, int flowc, int chan)
{  
  unsigned divisor;
  char cks;
  sci_info_t *sci = sci_rs232_chan_array[chan];
  struct sci_regs *regs = sci->regs;
  
  if(baud==-1) baud=sci->sci_rs232_baud;
  if(mode==-1) mode=sci->sci_rs232_mode;
  if(flowc==-1) flowc=sci->sci_rs232_flowc;


  /*disable SCI interrupts and Rx/Tx machine*/
  regs->rs232_scr=0;

  /* Power this port on and setup interrupt handlers. */
  sci->sci_rs232_init();        

  __memory_barrier();

  sci->sci_rs232_baud=baud;
  sci->sci_rs232_mode=mode;
  sci->sci_rs232_flowc=flowc;
  sci->sci_rs232_flags=0;

  /* set baudrate */
  cks=0;
  if((baud&RS232_BAUD_RAW)!=RS232_BAUD_RAW){
    divisor=div_us_ulus((CPU_SYS_HZ/16),baud);
    while(divisor>=512){
      if(++cks>=4) return -1;
      divisor>>=2;
    }
    divisor=(divisor+1)>>1;
  }else{
    divisor=baud&0xff;
  }
  regs->rs232_brr=divisor-1;
  
  regs->rs232_smr=(SMR_CKSxm&cks)|mode;
  regs->rs232_scmr=0;

  regs->rs232_ssr&=0;

  sci->sci_rs232_que_in.buf_beg = sci->sci_rs232_buf_in;
  sci->sci_rs232_que_in.buf_end = sci->sci_rs232_que_in.buf_beg+sizeof(sci->sci_rs232_buf_in);
  sci->sci_rs232_que_in.ip = sci->sci_rs232_que_in.buf_beg;
  sci->sci_rs232_que_in.op = sci->sci_rs232_que_in.buf_beg;

  sci->sci_rs232_que_out.buf_beg = sci->sci_rs232_buf_out;
  sci->sci_rs232_que_out.buf_end = sci->sci_rs232_que_out.buf_beg+sizeof(sci->sci_rs232_buf_out);
  sci->sci_rs232_que_out.ip = sci->sci_rs232_que_out.buf_beg;
  sci->sci_rs232_que_out.op = sci->sci_rs232_que_out.buf_beg;

  if(sci->sci_rs232_flowc&SCI_RSFLC_HW){
    /* Enable output for RTS P3.2 */
    sci->sci_rs232_rts_true();
  }

  sci->sci_rs232_irq_cnt=0;

  volatile int i;
  for (i = 0; i<30000; i++);

  /* SCR_TEm */
  regs->rs232_scr|=SCR_REm;

  __memory_barrier();

  atomic_set_mask_b1(SCR_RIEm,&regs->rs232_scr);
  return 1;
}

/* Local variables: */
/* c-basic-offset:2 */
/* End: */
