#include <stdint.h>
#include <cpu_def.h>
#include <mcu_regs.h>
#include <system_def.h>
#include <string.h>
#include "boot_fn.h"

/*#define USE_FONT_6x8*/

#ifndef HIT_LOAD_BAUD
  #define HIT_LOAD_BAUD 0
#endif

void exit(int status)
{
  while(1);
}

extern char __boot_fn_start;
extern char __boot_fn_end;

void RelocatedProgMode(unsigned long where, unsigned baud)
{ 
  void (*ProgMode_ptr)(unsigned baud);
  unsigned long reloc_offs=where-(unsigned long)&__boot_fn_start;
  size_t reloc_size=&__boot_fn_end-&__boot_fn_start;
  ProgMode_ptr=&ProgMode;
  (uint8_t*)ProgMode_ptr+=reloc_offs;
  memcpy((char*)where,&__boot_fn_start,reloc_size);
  (*ProgMode_ptr)(baud);
}

void flash_loader(void)
{
  SCIInit(HIT_LOAD_BAUD);

  if((uint8_t*)&__boot_fn_start<(uint8_t*)0xff0000)
    RelocatedProgMode(0xffb000,HIT_LOAD_BAUD);
   else
    ProgMode(HIT_LOAD_BAUD);
}

inline int call_address(unsigned long addr)
{
  typedef int (*my_call_t)(void);
  my_call_t my_call=(my_call_t)addr;
  return my_call();  
}

int main()
{
  /* Internal RAM enabled, advanced interrupt mode */
  /* *SYS_SYSCR = 1*SYSCR_RAMEm | 1*SYSCR_INTM1m ; */

  /* Remap 4kB of RAM from 0xffd000-0xffdfff to 0x0-0xfff */
  /* *FLM_RAMER= 1*RAMER_RAMSm | 0&RAMER_RAMxm */
  /* Sideefect - sets Flash software protection */
 
  /* Enables access to flash control registers */
  *IIC_SCRX |= SCRX_FLSHEm;
  
  /* set shadow registers */
  DIO_P1DDR_shadow=0;
  DIO_P3DDR_shadow=0;
    
  /* Setup system clock oscilator */
  /* PLL mode x4, */
  /* *SYS_LPWRCR=2&LPWRCR_STCxm; */
  /* PLL mode x2, */
  /* *SYS_LPWRCR=1&LPWRCR_STCxm; */
  { const char clkrat2stc[]={0,0/*1*/,1/*2*/,1,2/*4*/,2,2,2,3/*8*/};
    *SYS_LPWRCR=LPWRCR_STCxm&(LPWRCR_STC0m*
    		clkrat2stc[(CPU_SYS_HZ+CPU_REF_HZ/2)/CPU_REF_HZ]);
  }
  /* No clock disable, immediate change, busmaster high-speed */
  *SYS_SCKCR=(0*SCKCR_PSTOPm)|(1*SCKCR_STCSm)|(0&SCKCR_SCKxm);

 #ifdef USE_FONT_6x8
  /* set 6x8 pixel font */
  //*DIO_P7DR |=0x10;
 #else /* USE_FONT_6x8 */
  /* set 8x8 pixel font */
  //*DIO_P7DR &=~0x10;
 #endif /* USE_FONT_6x8 */
  //SHADOW_REG_SET(DIO_P7DDR,0x10);

  /* Setup chipselect outputs CS4 CS5 CS6 */
  //*DIO_P7DR |=1|2|4;
  //SHADOW_REG_SET(DIO_P7DDR,1|2|4);

  /* Setup chipselect outputs CS3 CS2 CS1 CS0 */
  //*DIO_PGDR |=2|4|8|0x10;
  //SHADOW_REG_SET(DIO_PGDDR,2|4|8|0x10);

  /* setup chipselect 1 - XRAM */
  *BUS_ABWCR&=~ABWCR_ABW1m;	/* 16 bit width */
  *BUS_ASTCR&=~ASTCR_AST1m;	/* 2 states access */
  *BUS_WCRL&=~(WCRL_W11m|WCRL_W10m);/* 0 additional wait states */

  /* setup chipselect 2 - SGM_LCD */
  *BUS_ABWCR|=ABWCR_ABW2m;	/* 8 bit width */
  *BUS_ASTCR|=ASTCR_AST2m;	/* 3 states access */
  *BUS_WCRL&=~(WCRL_W21m|WCRL_W20m);/* 0 additional wait states */
  *BUS_WCRL|=1*WCRL_W21m;	/* 0/1 additional wait state */

  /* setup chipselect 3 - SRAM */
  *BUS_ABWCR|=ABWCR_ABW3m;	/* 8 bit width */
  *BUS_ASTCR|=ASTCR_AST3m;	/* 3 states access */
  *BUS_WCRL&=~(WCRL_W31m|WCRL_W30m);/* 0 additional wait states */

  /* setup chipselect 4 - IDE */
  *BUS_ABWCR&=~ABWCR_ABW4m;	/* 16 bit width */
  *BUS_ASTCR|=ASTCR_AST4m;	/* 3 states access */
  *BUS_WCRH&=~(WCRH_W41m|WCRH_W40m);/* 0 additional wait states */

  /* setup chipselect 5 - IDE */
  *BUS_ABWCR&=~ABWCR_ABW5m;	/* 16 bit width */
  *BUS_ASTCR|=ASTCR_AST5m;	/* 3 states access */
  *BUS_WCRH&=~(WCRH_W51m|WCRH_W50m);/* 0 additional wait states */

  /* setup chipselect 6 - KL41 */
  *BUS_ABWCR|=ABWCR_ABW6m;	/* 8 bit width */
  *BUS_ASTCR|=ASTCR_AST6m;	/* 3 states access */
  *BUS_WCRH=WCRH_W61m|WCRH_W60m;	/* 3 additional wait states */

 	 /* crross cs wait| rd/wr wait    | no burst and DRAM */
  *BUS_BCRH=0*BCRH_ICIS1m | 0*BCRH_ICIS0m;
 	 /* release      | no DMAC buffer | no external wait */
  /* ****************************************************  */ //*BUS_BCRL=0*BCRL_BRLEm | 0*BCRL_WDBEm | 0*BCRL_WAITEm;
  *DIO_PCDDR=0xff;		/* A0-A7 are outputs */
  *DIO_PBDDR=0xff;		/* A8-A15 are outputs */
  /* Setup full 20 address lines */
  *DIO_PADR|=0x0f;
  *DIO_PADDR=0x0f;		/* A16-A19 are outputs */
 	 /* number of address output signals */
  *SYS_PFCR=__val2mfld(PFCR_AExm,20-8);

  /* Stop all modules */
  *SYS_MSTPCRA=0xff;
  *SYS_MSTPCRB=0xff;
  *SYS_MSTPCRC=0xff;

  /*set power on for SCI4 module*/
  *SYS_MSTPCRC&=~MSTPCRC_SCI4m;

  /* show something on debug leds */
  *DIO_P1DR=0xf-1;
  SHADOW_REG_SET(DIO_P1DDR,0x0f);

  /* Disable SCI 2 */
  /* Off TxD2 on Port PA.1 */
  /* Off RxD2 on Port PA.2 */
  *SCI_SCR2=0;
  *DIO_PADR|=0x06;
  *DIO_PADDR=0x01;
  *SCI_SMR2=0;
 
  /* Stop SCI4 communication */
  // SCI4 is not aviable 
  //*SCI_SCR4=0;
  //*SCI_SMR4=0;

  /* Output TxD4 on Port P3.7, TxD0 on P3.0 */
  /*	    RTS4 on Port P3.2		    */
  /* Input  RxD4 on Port P3.6, RxD0 on P3.1 */
  /*        CTS4 on Port P3.3		    */
  *DIO_P3DR|=0xc5;
  SHADOW_REG_SET(DIO_P3DDR,0x85);

  /* Enables access to flash control registers */
  *IIC_SCRX |= SCRX_FLSHEm;

  if(((*FLM_FLMCR1) & FLMCR1_FWEm)!=0){
    flash_loader();
  }
  
  if (*((unsigned long *)0x4000)!=0xffffffff){
    call_address(0x4000);
  }
  
  if (*((unsigned long *)0x200000)==0xff0055aa){
    call_address(0x200004);
  }

  flash_loader();
  return 0;
};
