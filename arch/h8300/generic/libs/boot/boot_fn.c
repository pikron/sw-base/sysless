#include <stdint.h>
#include <cpu_def.h>
#include <system_def.h>
//#include <h8s2633h.h>
#include <mcu_regs.h>
#include "boot_fn.h"

#undef WITH_EXTERNAL_FLASH

#if 1
 #define DEB_WR_HEX(...)
#else
 #define DEB_WR_HEX(_hex,_digs) deb_wr_hex(_hex,_digs);
 void deb_wr_hex(long hex, short digs);
#endif

#if 1
  #define DEB_BLOG_INIT
  #define DEB_BLOG(...)
#else
  #define DEB_BLOG_INIT  do{ *(long*)0x280000=0x280000+4; } while(0)
  #define DEB_BLOG(_val) do{ *((*(long**)0x280000)++)=_val; } while(0)
#endif

#ifdef WITH_EXTERNAL_FLASH
  #define EXTERNAL_FLASH_START 0x40000
  #define EXTERNAL_FLASH_END (0x40000+0xfffff)
  int ExtFlProgRow(uint8_t *addr, uint8_t *data);
  int ExtFlErase(uint8_t *addr);
#endif /*WITH_EXTERNAL_FLASH*/


/* Enable watchdog with selected system clock prescaller */
/* 2, 64, 128, 512, 2048, 8192, 32768, 131072 */
void wdg_enable(int psel)
{
  /* Enable power-on  reset of WDT owerflow */
  *WDT_WRSTCSRw=(0x5a00 | 1*WRSTCSR_RSTEm | 0*WRSTCSR_RSTSm);
  /* Select watchdog function and input clocks  */
  *WDT_WTCSR0w=(0xa500 | 1*WTCSR0_WTITm |(psel & WTCSR0_CKSxm));
  *WDT_WTCSR0w=(0xa500 | 1*WTCSR0_WTITm | 1*WTCSR0_TMEm | (psel & WTCSR0_CKSxm));
}

/* Disable watchdog */
void wdg_disable()
{
  *WDT_WTCSR0w=(0xa500);
}

/* Clear watchdog */
void wdg_clear()
{
  *WDT_WTCNT0w=(0x5a00);
}

#define TO_TEXT __attribute__ ((section (".text")))

static const unsigned long
  flash_blocks[] TO_TEXT =
	{0x00000,0x01000,0x02000,0x03000,0x04000,0x05000,0x06000,0x07000,
	 0x08000,0x10000,0x20000,0x30000,0x40000,0};

static const int
  flash_block_count TO_TEXT =
	sizeof(flash_blocks)/sizeof(unsigned long)-2;

#if 0
volatile void FlWait(long n)
{
  long i=0;
  volatile long x;
  while (i<n*6){
    i++;
    x+=i;
  }	
}
#else
volatile void FlWait(long n);

__asm__ (
".global _FlWait\n"
"_FlWait:\n"
#if (CPU_SYS_HZ>16000000)
"	shll.l	er0\n"
#endif
"	mov.w	#1,r1\n"
"	bra	2f:8\n"
"1:	dec.w	#1,r1\n"
"	bne	1b:8\n"
"	nop\n"
"	mov.w	#2,r1\n"
"2:	dec.l	#1,er0\n"
"	bne	1b:8\n"
"	rts\n"
);

#endif

int FlAdr2Blk(unsigned long adr)
{
  int bl=0;
  unsigned long *blocks;
  blocks = &flash_blocks[0];

  if(adr<blocks[0]) return -1;
  while(blocks[bl+1]){
    if(adr<blocks[bl+1]) return bl;
    bl++;
  }
  return -1;
}

/* Check if block number is blank */
int FlTest(int bl)
{
  uint16_t *p, *pe;
  unsigned long *blocks;
  blocks = &flash_blocks[0];
  
  if(bl>=flash_block_count) return -2;
  if(bl<0) return -2;

  /* No software control over Flash/External select */
  /* *BCRL=(*BCRL & (EAE ^ 0x0ff)); */

  p=(uint16_t*)blocks[bl];
  pe=(uint16_t*)blocks[bl+1];
  while(p<pe){
    *p=0xffff;
    FlWait(2);
    if (*p!=0xffff) return -1;
    p++;
  }
  return 0;
}

/* Erase block number */
int FlErase(int bl)
{
  int n=100; /*N*/
  if(bl>=flash_block_count) return -EBOOT_BLNUM_HIGH;
  if(bl<0) return -EBOOT_BLNUM_LOW;

  if(FlTest(bl)==0) return 0;

  if((*FLM_FLMCR1 & FLMCR1_FWEm)==0) return -EBOOT_NO_FWE;

  *FLM_FLMCR1=FLMCR1_SWE1m;
  FlWait(1);		/*x*/
  if(bl<8){
    *FLM_EBR1=(1 << bl);
    *FLM_EBR2=0;
  }else{
    *FLM_EBR1=0;
    *FLM_EBR2=(1 << (bl-8));
  }
  while(n>0){
    n--;
    if(*FLM_FLMCR2 & FLMCR2_FLERm) goto fls_error;
    wdg_enable(4+1);
    *FLM_FLMCR1|=FLMCR1_ESU1m;
    FlWait(100);		/*y*/
    *FLM_FLMCR1|=FLMCR1_E1m;
    FlWait(5000);		/*z=max10000*/
    *FLM_FLMCR1&=~FLMCR1_E1m;
    FlWait(10);			/*alpha*/
    *FLM_FLMCR2&=~FLMCR1_ESU1m;
    FlWait(10);			/*betha*/
    wdg_disable();
    if(*FLM_FLMCR2 & FLMCR2_FLERm) goto fls_error;
    *FLM_FLMCR1|=FLMCR1_EV1m;
    FlWait(6);			/*gamma*/
    if(FlTest(bl)==0){
      *FLM_FLMCR1&=FLMCR1_SWE1m; /*clear EV1*/
      FlWait(4);		/*ny*/
      *FLM_FLMCR1=0;
      return 0;
    }
    *FLM_FLMCR1&=FLMCR1_SWE1m; /*clear EV1*/
    FlWait(4);			/*ny*/
  }
  *FLM_FLMCR1=0;
  FlWait(100);			/*x1*/
  return -EBOOT_ERASE_FAILURE;
  
 fls_error:
  *FLM_FLMCR1=0;
  return -EBOOT_FLASH_ERROR;
}

void FlProgPulse(int time_zx)
{
  wdg_enable(3+1);
  *FLM_FLMCR1|=FLMCR1_PSU1m;
  FlWait(50);			/*y*/
  *FLM_FLMCR1|=FLMCR1_P1m;
  FlWait(time_zx);		/*z0,z1 or z2*/
  *FLM_FLMCR1&=~FLMCR1_P1m;
  FlWait(5);			/*alpha*/
  *FLM_FLMCR1&=FLMCR1_SWE1m; /*clear PSU1*/
  FlWait(5);			/*betha*/
  wdg_disable();
}

/* Program data to address */
int FlProgRow(uint8_t *adr, uint8_t *data)
{
  uint8_t prog_data[FLASH_ROW];
  int i;
  int m;
  int n;
  uint8_t *x;
  uint8_t c,d;
  if((unsigned long)adr & (FLASH_ROW-1)) return -EBOOT_ROW_BEGIN;
  if((*FLM_FLMCR1 & FLMCR1_FWEm)==0 ) return -EBOOT_NO_FWE;
 #ifdef WITH_EXTERNAL_FLASH
  if(((uint32_t)adr>=EXTERNAL_FLASH_START)&&
     ((uint32_t)adr<=EXTERNAL_FLASH_END)){
    return ExtFlProgRow(adr,data);
  }
 #endif /*WITH_EXTERNAL_FLASH*/

  x=adr;
  for(i=FLASH_ROW;i--;x++){
    if(*x!=0xff) return -EBOOT_ROW_NOT_ERASED;
  }
  x=data;
  for(i=0;i<FLASH_ROW;i++,x++) prog_data[i]=*x;

  *FLM_FLMCR1=FLMCR1_SWE1m;
  FlWait(1);			/*x0*/

  n=0;
  while(n<100){			/*N1+N2<1000*/
    n++;
    m=0;
    i=0;
    x=adr;
    for(i=0;i<FLASH_ROW;i++,x++) *x=prog_data[i];
    
    FlProgPulse(n>6?150:25); 	/*z0<30 or z2<200 if n>N1*/

    /* Program-Verify Mode */
    *FLM_FLMCR1|=FLMCR1_PV1m;
    FlWait(4);			/*gamma*/
    i=0;
    x=adr;
    for(i=0;i<FLASH_ROW;i+=2,x+=2){
      *(uint16_t*)x=0xffff;
      FlWait(2);		/*epsilon*/
      *(uint16_t*)(prog_data+i)=*(uint16_t*)x;
    } 
    *FLM_FLMCR1&=FLMCR1_SWE1m; /*clear PV1*/
    FlWait(2);			/*ny*/
    if(n<=6){			/*N1*/
      x=adr;
      for(i=0;i<FLASH_ROW;i++,x++){
        c=prog_data[i]; 
	d=data[i];
	if((~c&d)&0xff) goto fls_error;
	if(c!=d) {
	  m=1;	     /* Reprogram needed */
          /* DEB_BLOG(0xEE000000+(long)x); */
          /* DEB_BLOG(0xEF000000+(uint16_t)(c<<8)+(uint8_t)d); */
	}
        *x=d|c;
	prog_data[i]=d|~c;
      }
      
      FlProgPulse(7);		/*z1<10*/
      
    }else{
      for(i=0;i<FLASH_ROW;i++){
        c=prog_data[i]; 
	d=data[i];
	if(c!=d) m=1; /* Reprogram needed */
	if((~c&d)&0xff) goto fls_error;
	prog_data[i]=d|~c;
      }
    }
    if(m==0){
      *FLM_FLMCR1=0;
      FlWait(100);		/*x1*/
      DEB_BLOG(0xED000000+n);
      return 0;
    }
  }
  *FLM_FLMCR1=0;
  FlWait(100);			/*x1*/
  return -EBOOT_PROG_FAILURE;

 fls_error:
  *FLM_FLMCR1=0;
  return -EBOOT_FLASH_VERIFY;
}

int FlPrepBlk(unsigned long badr, unsigned long len)
{
  int bl, blend, res;
  bl=FlAdr2Blk(badr);
  blend=FlAdr2Blk(badr+len-1);
  if((bl<0)||(blend<0)) return -EBOOT_BLOCKADDR;
  for(;bl<=blend;bl++){
    if(FlTest(bl)){
      res=FlErase(bl);
      if(res<0) return res;
    }
  }
  return 0;
}


#if 0
#define RS232_TDR  SCI_TDR4
#define RS232_RDR  SCI_RDR4
#define RS232_SMR  SCI_SMR4
#define RS232_SCMR SCI_SCMR4
#define RS232_SCR  SCI_SCR4
#define RS232_SSR  SCI_SSR4
#define RS232_BRR  SCI_BRR4
#define RS232_RXD_PIN ((*DIO_PORT3)&(1<<6))

#elif 0
#define RS232_TDR  SCI_TDR2
#define RS232_RDR  SCI_RDR2
#define RS232_SMR  SCI_SMR2
#define RS232_SCMR SCI_SCMR2
#define RS232_SCR  SCI_SCR2
#define RS232_SSR  SCI_SSR2
#define RS232_BRR  SCI_BRR2
#define RS232_RXD_PIN ((*DIO_PORTA)&(1<<2))

#else
#define RS232_TDR  SCI_TDR1
#define RS232_RDR  SCI_RDR1
#define RS232_SMR  SCI_SMR1
#define RS232_SCMR SCI_SCMR1
#define RS232_SCR  SCI_SCR1
#define RS232_SSR  SCI_SSR1
#define RS232_BRR  SCI_BRR1
#define RS232_RXD_PIN ((*DIO_PORT3)&(1<<4))
#endif

#define RS232_BAUD_RAW 0xff00

int SCIInit(unsigned baud)
{
  unsigned divisor;
  char cks;

  /*disable SCI interrupts and Rx/Tx machine*/
  *RS232_SCR=0;

  cks=0;
  if((baud&RS232_BAUD_RAW)!=RS232_BAUD_RAW){
    divisor=div_us_ulus((CPU_SYS_HZ/16+baud/2),baud);
    while(divisor>=512){
      if(++cks>=4) return -1;
      divisor>>=1;
    }
    divisor=(divisor+1)>>1;
  }else{
    divisor=baud&0xff;
  }
  *RS232_BRR=divisor-1;

  *RS232_SMR=(SMR_CKSxm&cks);
  *RS232_SCMR=0;
  FlWait(20000);
  *RS232_SCR=SCR_TEm|SCR_REm;
  return 0;
}

volatile int SCISend(unsigned char c)
{
  unsigned int i=50000;
  while((*RS232_SSR & SSR_TDREm)==0 && i>0) i--;
  if (i==0) return -1;
  *RS232_TDR=c;
  *RS232_SSR=~SSR_TDREm&0xff;
  return 0;
}

volatile int SCIReceive(unsigned char *c,unsigned int time)
{
  unsigned char ssr;
  if(time){
    while(!((ssr=*RS232_SSR) & SSR_RDRFm) && ((time--)>0))
      if(ssr&(SSR_ORERm|SSR_FERm)) break;
    if (time==0) return -1;
  }	
  else{
    while(!((ssr=*RS232_SSR) & SSR_RDRFm))
      if(ssr&(SSR_ORERm|SSR_FERm)) break;
  }	
  *c=*RS232_RDR;
  *RS232_SSR=~(SSR_RDRFm|SSR_MPBTm);
  if(ssr & (SSR_ORERm|SSR_FERm)){
    *RS232_SSR=~(SSR_ORERm|SSR_FERm|SSR_MPBTm);
    return -2;
  }
  return 0;
}

unsigned long GetAdr()
{
  unsigned char c;
  unsigned long a;
  SCIReceive(&c,0);
  a=((unsigned long)c << 24);
  SCIReceive(&c,0);
  a=a | (((unsigned long)c << 16) & 0xff0000);
  SCIReceive(&c,0);
  a=a | (((unsigned long)c << 8) & 0xff00);
  SCIReceive(&c,0);
  a=a | ((unsigned long)c & 0xff);
  SCISend((a >> 24) & 0xFF);
  SCISend((a >> 16) & 0xFF);
  SCISend((a >> 8) & 0xFF);
  SCISend(a & 0xFF);
  return a;
}

int SCIAutoBaud(void)
{
  int t;
  unsigned char wtn;
  
  /* Disable power-on  reset of WDT owerflow */
  *WDT_WRSTCSRw=(0x5a00 | 1*WRSTCSR_RSTEm | 0*WRSTCSR_RSTSm);
  /* Select watchdog function and input clocks  */
  *WDT_WTCSR0w=(0xa500 | 1*WTCSR0_WTITm |(1 & WTCSR0_CKSxm));
  *WDT_WTCSR0w=(0xa500 | 1*WTCSR0_WTITm | 1*WTCSR0_TMEm | (1 & WTCSR0_CKSxm));

  while(!RS232_RXD_PIN) *WDT_WTCNT0w=(0x5a00);
  while(RS232_RXD_PIN) *WDT_WTCNT0w=(0x5a00);

  t=0;
  while(1){
    wtn=*WDT_WTCNT0r;
    if(wtn>0xf0){
      *WDT_WTCNT0w=(0x5a00);
      t+=0xf0;
    }
    if(RS232_RXD_PIN){
      t+=wtn;
      break;
    }
  };

  /* Disable watchdog */
  *WDT_WTCSR0w=(0xa500);
  
  SCIInit(((t*2+7)/9)|RS232_BAUD_RAW);
  
  return t;
}

#ifdef WITH_EXTERNAL_FLASH

#define EXTFL_addr_mask   0x0ffffl
#define EXTFL_reg1_addr   (0x555*2l)
#define EXTFL_reg2_addr   (0x2aa*2l)
#define EXTFL_sec_size    0x10000 
#define EXTFL_width8      0 
#define EXTFL_cmd_unlock1 0xaaaa  /* reg1 */
#define EXTFL_cmd_unlock2 0x5555  /* reg2 */
#define EXTFL_cmd_rdid    0x9090  /* reg1 */
#define EXTFL_cmd_prog    0xa0a0  /* reg1 */
#define EXTFL_cmd_erase   0x8080  /* reg1 */
#define EXTFL_cmd_reset   0xf0f0  /* any */
#define EXTFL_erase_all   0x1010  /* reg1 */
#define EXTFL_erase_sec   0x3030  /* sector */
#define EXTFL_fault_bit   0x2020
#define EXTFL_manid       1 
#define EXTFL_devid       0x2258

#define FLASH_WR16(addr,val) (*(volatile uint16_t*)(addr)=(val))
#define FLASH_RD16(addr) (*(volatile uint16_t*)(addr))

/* Program data to address */
int ExtFlProgRow(uint8_t *addr, uint8_t *data)
{
  /*FLASH_ROW*/;
  int ret=0;
  int cnt=FLASH_ROW/2;
  uint16_t old,new,val;
  uint32_t a=(uint32_t)addr&~EXTFL_addr_mask;
  while(cnt--){
    val=*((uint16_t*)data)++;
    /* security sequence */
    FLASH_WR16(a+EXTFL_reg1_addr,EXTFL_cmd_unlock1);
    FlWait(2);
    FLASH_WR16(a+EXTFL_reg2_addr,EXTFL_cmd_unlock2);
    FlWait(2);
    /* program command */
    FLASH_WR16(a+EXTFL_reg1_addr,EXTFL_cmd_prog);
    FlWait(2);
    FLASH_WR16(addr,val);
    FlWait(2);
    /* wait for result */
    old=FLASH_RD16(addr);
    FlWait(2);
    while((new=FLASH_RD16(addr))!=old){
      FlWait(2);
      if((old&EXTFL_fault_bit)&&(new&EXTFL_fault_bit)){
	if((FLASH_RD16(addr))!=new) ret=-2;
	break;
      }
      old=new;
    }
    /* reset */
    FLASH_WR16(a,EXTFL_cmd_reset);
    FlWait(2);
    if(FLASH_RD16(addr)!=val) return -EBOOT_EXT_FLASH_VERIFY;
    ((uint16_t*)addr)++;
  }
  return 0;
}

int ExtFlErase(uint8_t *addr)
{
  uint16_t old,new;
  int ret=0;
  uint32_t a=(uint32_t)addr&~EXTFL_addr_mask;
  /* security sequence */
  FLASH_WR16(a+EXTFL_reg1_addr,EXTFL_cmd_unlock1);
  FlWait(2);
  FLASH_WR16(a+EXTFL_reg2_addr,EXTFL_cmd_unlock2);
  FlWait(2);
  /* erase command */
  FLASH_WR16(a+EXTFL_reg1_addr,EXTFL_cmd_erase);
  FlWait(2);
  /* security sequence */
  FLASH_WR16(a+EXTFL_reg1_addr,EXTFL_cmd_unlock1);
  FlWait(2);
  FLASH_WR16(a+EXTFL_reg2_addr,EXTFL_cmd_unlock2);
  FlWait(2);
  /* select erase range */
  a=(uint32_t)addr;
  FLASH_WR16(a+EXTFL_reg1_addr,EXTFL_erase_all);
  FlWait(2);
  old=FLASH_RD16(addr);
  FlWait(2);
  while((new=FLASH_RD16(addr))!=old){
    FlWait(2);
    if((old&EXTFL_fault_bit)&&(new&EXTFL_fault_bit)){
      if((FLASH_RD16(addr))!=new) ret=-2;
      break;
    }
    old=new;
  }
  /* reset */
  FLASH_WR16(a,EXTFL_cmd_reset);
  FlWait(2);
  if(FLASH_RD16(addr)!=0xffff) ret--;    
  return ret;
}

#endif /*WITH_EXTERNAL_FLASH*/

void Call(unsigned long adr)
{
  __asm__ /*__volatile__*/(
	 "jsr	@%0\n\t"
	 :
	 :"g" (adr)
	 :"memory","cc");
}

void ProgMode(unsigned baud)
{
  char buf[FLASH_ROW];
  unsigned long i;
  unsigned long j;
  unsigned char e;
  unsigned char c;
  unsigned char d;
  unsigned char cmd;
  unsigned long badr;
  unsigned long len;
  unsigned char *adr;
  int ret;

  DEB_BLOG_INIT;
  if(baud) SCIInit(baud);
  while(1){
    if(!baud) SCIAutoBaud();
    SCIReceive(&c,0);
    while(c!=0x55){
      SCISend(0);	/*c*/
      SCIReceive(&c,0);
    }
    SCISend(0xAA);
    SCIReceive(&cmd,0);
    DEB_WR_HEX(cmd,2); /*!!!*/
    if((cmd & 7) == (((cmd >> 3) & 7) ^ 7)){
      SCISend(cmd | 0x80);
      cmd&=7;
      if((cmd<=2)||(cmd==4)){
        /* memory download/upload/erase region */
	badr=GetAdr();
	len=GetAdr();
	if(cmd==0){
          /* memory download */
	  e=0x5a;
	  i=0;
          DEB_WR_HEX(badr,8); /*!!!*/
          DEB_WR_HEX(len,8);  /*!!!*/
	  adr=(unsigned char *)badr;
	  while(i++<len){
	    if(SCIReceive(&c,0)<0){
	      e=0xfd;
	      break;
	    }
	    *adr=c;
            SCISend(*adr);
            adr++;
	  }
	  SCISend(e);
	}
        else if (cmd==1){
          /* flash programming */
	  /* check and erase range */
	  /*if(FlPrepBlk(badr,len)<0) e=0xfc;*/
	  i=badr-(badr & 0xffffffe0);
	  j=0;
	  e=0x5a;
	  while((i--)>0){
	    buf[j++]=0xff;
	  }
	  adr=(unsigned char *)(badr & ~(FLASH_ROW-1));
	  j=(unsigned char *)badr-adr;
	  i=0;
	  while(i++<len){
	    if(SCIReceive(&c,0)<0){
	      e=0xfd;
              j=0;
	      break;
	    }
	    buf[j++]=c;
	    if(j==FLASH_ROW){
              DEB_WR_HEX((long)adr,6);  /*!!!*/
	      if((ret=FlProgRow(adr,buf))!=0) {
                e=-ret;
                j=0;
                break;
              }
              DEB_BLOG(0xEA000000|(uint32_t)adr|((uint8_t)j&0x7f));
              DEB_WR_HEX(j,2);  /*!!!*/
	      adr+=FLASH_ROW;
	      j=0;
	    }
	    SCISend(c);
	  }
	  if(j/*&&!(e&0x80)*/){
	    while(j<FLASH_ROW) buf[j++]=0xff;
            if((ret=FlProgRow(adr,buf))!=0) e=-ret;
	  }
	  SCISend(e);
        }else if (cmd==4){
	  /* check and erase region */
	  e=FlPrepBlk(badr,len);
	  if(!e) e=0x5a;
	  SCISend(e);
	}else{
	  /* upload memory */
	  i=0;
	  e=0x5a;
          DEB_WR_HEX(badr,8); /*!!!*/
          DEB_WR_HEX(len,8);  /*!!!*/
	  adr=(unsigned char *)badr;
	  while(i++<len){
	    d=*adr;
	    SCISend(d);
	    if(SCIReceive(&c,0)<0){
	      e=0xfd;
	      break;
	    }
	    if(c!=d) e=0xff;
	    adr++;
	  }
	  SCISend(e);
	}
      }else{
        /* erase block */
	if(cmd==3){
          SCIReceive(&c,0);
	  if (c<flash_block_count){
	    if(FlErase(c)==0) SCISend(0x5A);
			 else SCISend(0xFF);
         #ifdef WITH_EXTERNAL_FLASH
	  }else if(c==100){
            if(ExtFlErase((uint8_t*)EXTERNAL_FLASH_START)==0)
	       SCISend(0x5A); else SCISend(0xFF);
         #endif /*WITH_EXTERNAL_FLASH*/
	  }else SCISend(0xFE);
	}
	else if (cmd==6){
	  badr=GetAdr();
          DEB_WR_HEX(badr,8); /*!!!*/
	  Call(badr);
	}
	else if (cmd==7){
          wdg_enable(1+1);
	}
	else{
          SCISend(0xFF);
	}
      }
    }else{
      SCISend(0xFE);
    }
  }
}
