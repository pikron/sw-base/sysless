#ifndef _boot_fn_H
#define _boot_fn_H

#include <stdint.h>

/* Size of a flash row */
#define FLASH_ROW 128

/* Error codes (returned as negative numbers) */
#define EBOOT_PROG_FAILURE	1
#define EBOOT_EXT_FLASH_VERIFY	2
#define EBOOT_FLASH_VERIFY	3
#define EBOOT_ROW_NOT_ERASED	4
#define EBOOT_NO_FWE		5
#define EBOOT_ROW_BEGIN		6
#define EBOOT_BLOCKADDR		8
#define EBOOT_BLNUM_HIGH	9
#define EBOOT_BLNUM_LOW		10
#define EBOOT_FLASH_ERROR	11
#define EBOOT_ERASE_FAILURE	12

volatile void FlWait(long n);
void wdg_enable(int psel);
void wdg_disable();
void wdg_clear();
int FlTest(int bl);
int FlErase(int bl);
int FlProgRow(uint8_t *adr, uint8_t *data);
int SCIAutoBaud(void);
int SCIInit(unsigned baud);
volatile int SCISend(unsigned char c);
volatile int SCIReceive(unsigned char *c,unsigned int time);
unsigned long GetAdr();
void ProgMode(unsigned baud);

#endif /* _boot_fn_H */
