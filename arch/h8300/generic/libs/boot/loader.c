#include <stdint.h>
#include <cpu_def.h>
#include <system_def.h>
//#include <h8s2633h.h>
#include <h8s2639h.h>
#include "boot_fn.h"

/* hack for start of main, should use crt0.o instead */
__asm__ /*__volatile__*/(
	".global _start\n\t"
	"_start : \n\t"
	"mov.l #0xffdffe,sp\n\t"
	"jsr	_main\n\t"
	);

void exit(int status)
{
  while(1);
}

#define RS232_RXD_PIN ((*DIO_PORT3)&(1<<6))

int main()
{
  /* Disable SCI 2 */
  /* Off TxD2 on Port PA.1 */
  /* Off RxD2 on Port PA.2 */
  *SCI_SCR2=0;
 #ifndef FULL_XRAM_ADRBUS
  *DIO_PADR|=0x06;
  *DIO_PADDR=0x01;
 #endif /* FULL_XRAM_ADRBUS */

  /*set power on for SCI4 module*/
  *SYS_MSTPCRC&=~MSTPCRC_SCI4m;
  
  /* Output TxD4 on Port P3.7 */
  /* Input  RxD4 on Port P3.6 */
  *DIO_P3DR|=0xc0;
  SHADOW_REG_SET(DIO_P3DDR,0x80);
  
  ProgMode(0);

  return 0;
};


