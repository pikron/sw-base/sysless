/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  cmd_bth.c - interconnection of text command processor
                with RS-232 line
 
  Copyright (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#include <cmd_proc.h>
#include <bth_inface.h>

#define ED_LINE_CHARS 80

/* cmd_io_t cmd_io_bth_dev; */

/* char ed_line_chars_bth_in[ED_LINE_CHARS+1]; */
/* char ed_line_chars_bth_out[ED_LINE_CHARS+1]; */

/* ed_line_buf_t ed_line_buf_bth_in={ */
/*   flg:FL_ELB_ECHO, */
/*   inbuf:0, */
/*   alloc:sizeof(ed_line_chars_bth_in), */
/*   maxlen:0, */
/*   lastch:0, */
/*   buf:ed_line_chars_bth_in */
/* }; */

/* ed_line_buf_t ed_line_buf_bth_out={ */
/*   flg:FL_ELB_NOCRLF, */
/*   inbuf:0, */
/*   alloc:sizeof(ed_line_chars_bth_out), */
/*   maxlen:0, */
/*   lastch:0, */
/*   buf:ed_line_chars_bth_out */
/* }; */

/* cmd_io_t cmd_io_bth={ */
/*   putc:cmd_io_putc_ed_line, */
/*   getc:NULL, */
/*   write:cmd_io_write_bychar, */
/*   read:NULL, */
/*   priv:{ */
/*     ed_line:{ */
/*       in: &ed_line_buf_bth_in, */
/*       out:&ed_line_buf_bth_out, */
/*       io_stack:&cmd_io_bth_dev */
/*     } */
/*   } */
/* }; */

int cmd_io_putc_bth(struct cmd_io *cmd_io,int ch)
{ 
  return bth_inface_sendch(ch,0); //=0; //sci_bth_sendch(ch,IMPLIC_NUM_SERIAL_PORT);;
}

int cmd_io_getc_bth(struct cmd_io *cmd_io)
{ 
  return bth_inface_recch(0);//=-1; //sci_bth_recch(IMPLIC_NUM_SERIAL_PORT);
}

cmd_io_t cmd_io_bth={
  putc:cmd_io_putc_bth,
  getc:cmd_io_getc_bth,
  write:cmd_io_write_bychar,
  read:cmd_io_read_bychar
};
