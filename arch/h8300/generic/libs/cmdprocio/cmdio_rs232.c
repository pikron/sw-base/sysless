/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  cmd_rs232.c - interconnection of text command processor
                with RS-232 line
 
  Copyright (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - MPL - Mozilla Public License
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#include <cmd_proc.h>
#include <periph/sci_rs232.h>

static int cmd_io_putc_rs232(struct cmd_io *cmd_io,int ch)
{ 
  return sci_rs232_sendch(ch, sci_rs232_chan_default);
}

static int cmd_io_getc_rs232(struct cmd_io *cmd_io)
{ 
  return sci_rs232_recch(sci_rs232_chan_default);
}

cmd_io_t cmd_io_rs232={
  putc:cmd_io_putc_rs232,
  getc:cmd_io_getc_rs232,
  write:cmd_io_write_bychar,
  read:cmd_io_read_bychar
};

