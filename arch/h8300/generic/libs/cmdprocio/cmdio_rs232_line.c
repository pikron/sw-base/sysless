#include <cmd_proc.h>
#include <string.h>

#define ED_LINE_CHARS 80

char ed_line_in_rs232[ED_LINE_CHARS+1];
char ed_line_out_rs232[ED_LINE_CHARS+1];


ed_line_buf_t ed_line_buf_in_rs232={
    flg:FL_ELB_ECHO,
    inbuf:0,
    alloc:sizeof(ed_line_in_rs232),
    maxlen:0,
    lastch:0,
    buf:ed_line_in_rs232
};

ed_line_buf_t ed_line_buf_out_rs232={
    flg:FL_ELB_NOCRLF,
    inbuf:0,
    alloc:sizeof(ed_line_out_rs232),
    maxlen:0,
    lastch:0,
    buf:ed_line_out_rs232
};

extern cmd_io_t cmd_io_rs232;
const cmd_io_t cmd_io_rs232_line={
    putc:cmd_io_line_putc,
    getc:NULL,
    write:cmd_io_write_bychar,
    read:NULL,
    priv:{
        ed_line:{
            in: &ed_line_buf_in_rs232,
            out:&ed_line_buf_out_rs232,
            io_stack:&cmd_io_rs232
        }
    }
};
