#ifndef _COMMON_DEF_H
#define _COMMON_DEF_H

#define UNUSED __attribute ((unused))

#ifndef BIT
#define BIT(n)    (1 << n)
#endif
 
#endif /* _COMMON_DEF_H */
