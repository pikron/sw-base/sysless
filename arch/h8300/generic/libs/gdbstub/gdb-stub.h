/*
  Copyright (c) 1999 by      William A. Gatliff
  All rights reserved.     bgat@open-widgets.com

  See the file COPYING for details.

  This file is provided "as-is", and without any express
  or implied warranties, including, without limitation,
  the implied warranties of merchantability and fitness
  for a particular purpose.

  The author welcomes feedback regarding this file.
  
  Some changes by Petr Ledvina, ledvinap@kae.zcu.cz, 10/2001
*/

/* $Id: gdb.h,v 1.3 2001/11/04 20:07:12 ledvinap Exp $ */

#if !defined( GDB_H_INCLUDED )
#define GDB_H_INCLUDED


/* platform-specific stuff */
void gdb_putc ( char c );
char gdb_getc ( void );
short gdb_peek_register_file ( short id, long *val );
short gdb_poke_register_file ( short id, long val );
void gdb_step ( char *hargs );
void gdb_undo_step ( void );
void gdb_continue ( char *hargs );
void gdb_kill( void );
void gdb_return_from_exception( void );
void gdb_flush_cache(void *start, void *end);
#if USE_BREAKPOINTS
void gdb_insert_breakpoint( char *hargs );
void gdb_remove_breakpoint( char *hargs );
void gdb_breakpoint_init(void);
#endif
void gdb_platform_init();
int gdb_platform_enable(int en);

/* platform-neutral stuff */
long hex_to_long ( char h );
char lnibble_to_hex ( char i );
long hexbuf_to_long ( short len, const char *hexbuf );
short long_to_hexbuf ( long l, char *hexbuf, short pad );
unsigned char gdb_putstr ( short len, const char *buf );
void gdb_putmsg ( char c, const char *buf, short len );
short gdb_getmsg ( char *rxbuf );
void gdb_last_signal ( unsigned char sigval );
void gdb_expedited ( unsigned char sigval );
void gdb_read_memory ( const char *hargs );
void gdb_write_memory ( const char *hargs );
void gdb_console_output( short len, const char *buf );
void gdb_write_registers ( char *hargs );
void gdb_read_registers ( char *hargs );
void gdb_write_register ( char *hargs );
void gdb_monitor ( short sigval );
void gdb_handle_exception( long sigval );

void gdb_main(void);
#if GDB_HOOK_SUPPORT 
/* define structure to store hook functions and magics */
/* place the struct on some knowbn address so we can modigy it using gdb scripts */
struct gdb_hooksT {
  /* called when we enter exception */
  unsigned long exception_magic;
  int (*exception_fn)(long sigval);
  /* called when message from gdb is received, return nonzero when processed */
  unsigned long message_magic;
  int (*message_fn)(char* hargs);
  /* called after reset, maybe some initialization */
  unsigned long init_magic;
  int (*init_fn)(void);
};
#define GDB_HOOK_MAGIC 0xdeadbeef
#endif


#endif

