#ifndef H8S_2633_DEFS
#define H8S_2633_DEFS

#include <system_def.h>

/* use expedited response insead of just signal */
#define GDB_STATUS_EXPEDITED 1
/* fetch MAC from register file */
#define USE_MAC 1
/* support for interrupt mode 2 (unfinished, broken) */
#define USE_EXR 0
/* _USE_EXR_LEVELS */

/* hardware independend support */
#define HW_INDEPENDENT 1

/* compile all for ROM, not HDI monitor */
#define GDB_IN_ROM 1

/* compile in breakpoint support (SW breakpoints) */
#define USE_BREAKPOINTS 1
/* number of allocated breakpoints, HW+SW */
#define MAX_BREAKPOINTS 16
/* compile in support for PBC (hw breakpoints + watchpoints) */
#define USE_HW_BREAKPOINTS 1
/* support for patching stub with hooks */
/*#define GDB_HOOK_SUPPORT 1*/
#define GDB_HOOK_SUPPORT 0
/* serial port to talk to gdb */
/*#define GDB_SCI_PORT 4 */	/* RS232/485 */
#define GDB_SCI_PORT 1  	/* IRDA */
/* serial speed */
/*#define GDB_SCI_SPEED 115200U*/
#define GDB_SCI_SPEED 38400U
/* clock speed */
/*#define TARGET_CLOCK_HZ 18432000.0*/
//#define TARGET_CLOCK_HZ 12000000 /*CPU_SYS_HZ*/
//#define TARGET_CLOCK_HZ 11059200 /*CPU_SYS_HZ*/
#define TARGET_CLOCK_HZ CPU_SYS_HZ

#endif


