#ifndef H8S_2633_RMAP_H
#define H8S_2633_RMAP_H

#ifndef IN_ASM 

#define R_UCHAR(a)  (*(volatile unsigned char *)(a))
#define R_USHORT(a) (*(volatile unsigned short *)(a))
#define R_ULONG(a)  (*(volatile unsigned long *)(a))
/* IIC_SCRX&=~SCRX_IICEm */
#define SCI_REGENABLE_SCRX asm("bclr	#4, @0xFFFDB4:16")
 
#else /* for use in asm file */

#define R_UCHAR(a)  (a)
#define R_USHORT(a) (a)
#define R_ULONG(a)  (a)
/* IIC_SCRX&=~SCRX_IICEm */
#define SCI_REGENABLE_SCRX      bclr	#4, @0xFFFDB4:16

#endif

/* Module stop controll */
#define MSTPCRA         R_UCHAR(0xfffffde8)   /* module stop control C */
#define MSTPCRB         R_UCHAR(0xfffffde9)   /* module stop control C */
#define MSTPCRC         R_UCHAR(0xfffffdea)   /* module stop control C */

/* sci */

/* Use only definition for selected channel */
#if     GDB_SCI_PORT==0
# define SCI_REGENABLE SCI_REGENABLE_SCRX
# define SCI_BASE 0xffffff78
# define SCI_MSTR MSTPCRB
# define SCI_MSTPCR_BIT BIT(7)
#elif GDB_SCI_PORT==1
# define SCI_REGENABLE SCI_REGENABLE_SCRX
# define SCI_BASE 0xffffff80
# define SCI_MSTR MSTPCRB
# define SCI_MSTPCR_BIT BIT(6)
#elif GDB_SCI_PORT==2
# define SCI_REGENABLE 
# define SCI_BASE 0xffffff88
# define SCI_MSTR MSTPCRB
# define SCI_MSTPCR_BIT BIT(5)
#elif GDB_SCI_PORT==3
# define SCI_REGENABLE 
# define SCI_BASE 0xfffffdd0
# define SCI_MSTR MSTPCRC
# define SCI_MSTPCR_BIT BIT(7)
#elif GDB_SCI_PORT==4
# define SCI_REGENABLE 
# define SCI_BASE 0xfffffdd8
# define SCI_MSTR MSTPCRC
# define SCI_MSTPCR_BIT BIT(6)
#else
# error "You must define SCI port to uuse (GDB_SCI_PORT)"
#endif

#define SCI_SMR         R_UCHAR(SCI_BASE+0)   /* Serial mode       	*/
#define SCI_BRR         R_UCHAR(SCI_BASE+1)   /* Bit rate           	*/
#define SCI_SCR         R_UCHAR(SCI_BASE+2)   /* Serial control       */
#define SCI_TDR         R_UCHAR(SCI_BASE+3)   /* Transmit data        */
#define SCI_SSR         R_UCHAR(SCI_BASE+4)   /* Serial status        */
#define SCI_RDR         R_UCHAR(SCI_BASE+5)   /* Receive data         */
#define SCI_SCMR        R_UCHAR(SCI_BASE+6)   /* Smart card mode      */

#define SCI_PER  BIT(3)
#define SCI_FER  BIT(4) 
#define SCI_ORER BIT(5)

#define SCI_RDRF BIT(6)
#define SCI_TDRE BIT(7)

/* PC Break Controller */

#define PBC_BARA	R_ULONG(0xfffffe00)   /* Break Address A      */
#define PBC_BARB	R_ULONG(0xfffffe04)   /* Break Address B      */
#define PBC_BCRA	R_UCHAR(0xfffffe08)   /* Break Control A      */
#define PBC_BCRB	R_UCHAR(0xfffffe09)   /* Break Control B      */

#define PBC_BARX(idx)   (((volatile unsigned long *)0xfffffe00)[idx])
#define PBC_BCRX(idx)   (((volatile unsigned char *)0xfffffe08)[idx])

/* io ports */
#define P1DDR	        R_UCHAR(0xfffffe30)   /* Port 1 Data Direction Register */
#define P1DR	        R_UCHAR(0xffffff00)   /* Port 1 Data Register           */
#define PADDR	        R_UCHAR(0xfffffe39)   /* Port A Data Direction Register */
#define PBDDR	        R_UCHAR(0xfffffe3a)   /* Port B Data Direction Register */
#define PCDDR	        R_UCHAR(0xfffffe3b)   /* Port C Data Direction Register */

#define PGDDR           R_UCHAR(0xfffffe3f)   /* Port G Data Direction Register */
/* memory controll registers */
#define ABWCR	        R_UCHAR(0xfffffed0)   /* Bus width control	 */
#define ASTCR	        R_UCHAR(0xfffffed1)   /* Access state control	 */
#define WCRH	        R_UCHAR(0xfffffed2)   /* Wait control H	         */
#define WCRL	        R_UCHAR(0xfffffed3)   /* Wait control L	         */
#define BCRH	        R_UCHAR(0xfffffed4)   /* Bus control  H	         */
#define BCRL	        R_UCHAR(0xfffffed5)   /* Bus control  L	         */
#define MCR 	        R_UCHAR(0xfffffed6)   /* Memory control reg	 */
#define DRAMCR          R_UCHAR(0xfffffed7)   /* DRAM control register   */
#define RTCNT           R_UCHAR(0xfffffed8)   /* Refresh timer counter   */
#define RTCOR           R_UCHAR(0xfffffed9)   /* Refresh timer const     */
#define PFCR            R_UCHAR(0xfffffdeb)   /* Pin Function control	 */

/* Exception vectors */
#define EXCPTVEC_ERI0	80	/* SCI 0 */
#define EXCPTVEC_ERI1	84	/* SCI 1 */
#define EXCPTVEC_ERI2	88	/* SCI 2 */
#define EXCPTVEC_NMI	7
#define EXCPTVEC_TRAP2	10
#define EXCPTVEC_TRAP3	11
#define EXCPTVEC_PBC	27


#endif
