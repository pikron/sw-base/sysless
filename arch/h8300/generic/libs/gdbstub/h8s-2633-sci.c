#include "h8s-2633-defs.h"
#include "h8s-2633-rmap.h"
#include "h8s-2633-sci.h"

void gdb_sci_init(unsigned char baud, unsigned char config)
{
  SCI_REGENABLE;
  SCI_MSTR &= ~SCI_MSTPCR_BIT;

  SCI_SCR = 0x00;                          // clear TE & RE in SCR
  SCI_SMR = config;                        // set char length, parity, & # of stop bits
  SCI_BRR = baud;                          // set baud rate

  SCI_SCR = 0x30;                          // enable RE & TE                        
}

void gdb_putc(char ch)
{
  SCI_REGENABLE;
  while((SCI_SSR&SCI_TDRE)==0); // wait for empty buffer ... 
  SCI_TDR=ch;
  SCI_SSR&=~SCI_TDRE;
}

char gdb_getc(void)
{
  char ch;
  SCI_REGENABLE;
  do {
    if(SCI_SSR&(SCI_PER|SCI_FER|SCI_ORER)) {
      /* clear any detected errors */
      SCI_SSR &= ~(SCI_PER|SCI_FER|SCI_ORER);
    }
    /* wait for a byte */
  } while(!(SCI_SSR&SCI_RDRF));

  /* got one-- return it */
  ch = SCI_RDR;
  SCI_SSR &= ~SCI_RDRF;

  return ch;
}
