/*
  Copyright (c) 1999 by      William A. Gatliff
  All rights reserved.     bgat@open-widgets.com

  See the file COPYING for details.

  This file is provided "as-is", and without any express
  or implied warranties, including, without limitation,
  the implied warranties of merchantability and fitness
  for a particular purpose.

  The author welcomes feedback regarding this file.

  Mostly rewritten from sh2-704x.c by Petr Ledvina, 
  ledvinap@kae.zcu.cz, 2001/10
  
  BTW: Can't imagine houw could sh2 stub work ... 
    It would be probably good idea to compare it with this file
    and fix bugs
  
*/

/* $Id: h8s-2633.c,v 1.5 2001/11/05 06:10:08 ledvinap Exp $ */

#include "h8s-2633-defs.h"
#include "common_def.h"
#include "h8s-2633-rmap.h"
#include "gdb-stub.h"
#include "h8s-2633-sci.h"
#include "h8s-2633.h"

/* trapa #3, the code is <5><3><trapno><0> */
#define STEP_OPCODE 0x5730

/* TODO 8bit ccr and exr */
typedef struct {
  unsigned long  r[8];
  unsigned long  pc;
  unsigned long  ccr;
  unsigned long  exr;
#if USE_MAC
  unsigned long  mach;
  unsigned long  macl;
#endif
} gdb_register_file_T;

gdb_register_file_T gdb_register_file
/*__attribute__((section(".regfile")))*/;


/* stack for gdb, switch to this stack on start */
/* stack must containg RX and TX buffers */
#define GDB_STACK_SIZE 1024
unsigned short gdb_stack[GDB_STACK_SIZE] /*__attribute__ ((section (".stack")))*/;
/* flag that we are in gdb stub, ignore exceptions (bit 7, for TAS)*/
unsigned char gdb_in_gdb=0x80;  

/*
  Retrieves a register value from gdb_register_file.
  Returns the size of the register, in bytes,
  or zero if an invalid id is specified (which *will*
  happen--- gdb.c uses this functionality to tell
  how many registers we actually have).
*/
short gdb_peek_register_file ( short id, long *val )
{
  /* all our registers are longs */
  short retval = sizeof( long );


  switch( id ) {

  case 0:  case 1:  case 2:  case 3:
  case 4:  case 5:  case 6:  case 7:
    *val = gdb_register_file.r[id];
    break;

  case 8:
    *val = gdb_register_file.ccr;
    break;
  case 9:
    *val = gdb_register_file.pc;
    break;
  case 10:
    *val = gdb_register_file.exr;
    break;
#if USE_MAC
  case 11:
    *val = gdb_register_file.mach;
    break;
  case 12:
    *val = gdb_register_file.macl;
    break;
#endif
  default:
    retval = 0;
  }
  return retval;
}

/*
  Stuffs a register value into gdb_register_file.
  Returns the size of the register, in bytes,
  or zero if an invalid id is specified.
 */
short gdb_poke_register_file ( short id, long val )
{
  /* all our registers are longs */
  short retval = sizeof( long );


  switch( id ) {

  case 0:  case 1:  case 2:  case 3:
  case 4:  case 5:  case 6:  case 7:
    gdb_register_file.r[id] = val;
    break;

  case 8:   /* ccr */
    gdb_register_file.ccr = val;
    break;
  case 9:   /* pc */
    gdb_register_file.pc = val;
    break;
  case 10:  /* cycles */
    gdb_register_file.exr = val;
    break;
#if USE_MAC
  case 11:
    gdb_register_file.mach = val;
    break;
  case 12:
    gdb_register_file.macl = val;
    break;
#endif
  default:
    retval = 0;
  }
  return retval;
}

/* 
   table to clasify instruction according to first byte 
   Only first 128 entries, rest is always 4 
*/

#define D_FIRST 11
#define D_LD   11
#define D_BCC2 12 
#define D_BCC4 13
#define D_JMPR 14
#define D_JMPA 15
#define D_JMPI 16
#define D_BSR2 17
#define D_BSR4 18
#define D_JSRR 19
#define D_JSRA 20
#define D_JSRI 21
#define D_MV   22
#define D_RTS  23
#define D_RTE  24

unsigned char const opcode_0_to_length[128] = 
    { 
/*       0      1      2      3      4      5      6      7  
         8      9      a      b      c      d      e      f */
/* 0x */
         2,  D_LD,     2,     2,     2,     2,     2,     2,   
         2,     2,     2,     2,     2,     2,     2,     2, 
/* 1x */
         2,     2,     2,     2,     2,     2,     2,     2,   
         2,     2,     2,     2,     2,     2,     2,     2, 
/* 2x */
         2,     2,     2,     2,     2,     2,     2,     2,   
         2,     2,     2,     2,     2,     2,     2,     2, 
/* 3x */
         2,     2,     2,     2,     2,     2,     2,     2,   
         2,     2,     2,     2,     2,     2,     2,     2, 
/* 4x */
    D_BCC2,D_BCC2,D_BCC2,D_BCC2,D_BCC2,D_BCC2,D_BCC2,D_BCC2,   
    D_BCC2,D_BCC2,D_BCC2,D_BCC2,D_BCC2,D_BCC2,D_BCC2,D_BCC2, 
/* 5x */
         2,     2,     2,     2, D_RTS,D_BSR2, D_RTE,     2,   
    D_BCC4,D_JMPR,D_JMPA,D_JMPI,D_BSR4,D_JSRR,D_JSRA,D_JSRI, 
/* 6x */
         2,     2,     2,     2,     2,     2,     2,     2,   
         2,     2,  D_MV,  D_MV,     2,     2,     4,     4, 
/* 7x */
         2,     2,     2,     2,     2,     2,     2,     2,  
         8,     4,     6,     4,     4,     4,     4,     4 
    };  

/* test global ccr state, return if given condition is true */
int test_bcc(int op) {
  register char ccr=gdb_register_file.ccr;
#define C 0x01
#define V 0x02
#define Z 0x04
#define N 0x08
  switch(op) {
  case 0x0: return 1;
  case 0x1: return 0;
  case 0x2: return !(ccr&(C|Z));
  case 0x3: return ccr&(C|Z);
  case 0x4: return !(ccr&C);
  case 0x5: return ccr&C;
  case 0x6: return !(ccr&Z);
  case 0x7: return ccr&Z;
  case 0x8: return !(ccr&V);
  case 0x9: return ccr&V;
  case 0xa: return !(ccr&N);
  case 0xb: return ccr&N;
  case 0xc: return (ccr&N)? (ccr&V):!(ccr&V);
  case 0xd: return (ccr&N)?!(ccr&V):(ccr&V);
  case 0xe: return !(ccr&Z) && (ccr&N)? (ccr&V):!(ccr&V);
  case 0xf: return  (ccr&Z) || (ccr&N)?!(ccr&V): (ccr&V);
  }
#undef C
#undef V
#undef N
#undef Z
  return 0;
}

/*
  Analyzes the next instruction, to see where the program
  will go to when it runs.  Returns the destination address.
  
  Maybe some code opcodes may be used to automagicaly
  generate this ... 
  (There are some magic opcodes in gdb's opcodes, so don't forget
    to filter them )

  Instruction length is decoded using hardwired tests+table
  The table contains operation necesary for given prefix
  Length of non-jump instruction must be computed to gen next instr
  
  This code is partialy tested, the table was generated form /opcodes
  Probably some more intensive testing is neccesary
*/

long gdb_get_stepi_dest ( void )
{
  unsigned char op = ((unsigned char*)gdb_register_file.pc)[0];
  unsigned char op_t;
  long addr = gdb_register_file.pc;
  
  /* test if opcode > 0x80 -> 2b */ 
  if(op>=0x80) 
    return addr+2;
  
  /* fetch length from opcode table, dispatch results */
  op_t=opcode_0_to_length[op];
  switch(op_t) {
  case D_LD:   // prefix 01, ld and similar
    op=((unsigned char*)gdb_register_file.pc)[1];
    switch(op>>4) {
    case 0x0:
    case 0x4:
      op=((unsigned char*)gdb_register_file.pc)[2];
      switch(op) {
      case 0x6b: return addr+((((unsigned char*)gdb_register_file.pc)[3]&2)?8:6);
      case 0x6f: return addr+6;
      case 0x78: return addr+10;
      default: return addr+4;  
      }
    case 0x8:
    case 0xa:
      return addr+2;
    default:
      /* some invalid opcodes here, but ignore it ... */
      return addr+4;
    }
  case D_BCC2:
    if(test_bcc(op&0x0f))
      return addr+2+((signed char*)gdb_register_file.pc)[1];
    else return addr+2;
  case D_BCC4:
    if(test_bcc(((unsigned char*)gdb_register_file.pc)[1]>>4))
      return addr+4+((signed short*)gdb_register_file.pc)[1];
    else return addr+4;
  case D_JMPR:
  case D_JSRR:
    return gdb_register_file.r[((unsigned char*)gdb_register_file.pc)[1]>>4];
  case D_JMPA:
  case D_JSRA:
    return ((unsigned long*)gdb_register_file.pc)[0]&0x00ffffff;
  case D_JMPI:
  case D_JSRI:
    return 
      ((unsigned long*)(0))
      [((unsigned char*)gdb_register_file.pc)[1]]&0x00ffffff;
  case D_BSR2:
    return addr+2+((signed char*)gdb_register_file.pc)[1];
  case D_BSR4:
    return addr+4+((signed short*)gdb_register_file.pc)[1];
  case D_MV:
    switch(((unsigned char*)gdb_register_file.pc)[1]>>4) {
    case 0x1:
    case 0x2: 
    case 0xa: return addr+6;
    case 0x3: return addr+8;
    default:  return addr+4;
    }
  case D_RTS:
    return ((unsigned long*)gdb_register_file.r[7])[0]&0x00ffffff;
  case D_RTE:
#if USE_EXR
    return ((unsigned long*)(gdb_register_file.r[7]+2))[0]&0x00ffffff;
#else
    return ((unsigned long*)(gdb_register_file.r[7]+0))[0]&0x00ffffff;
#endif
  default:
    return addr+op_t;
  }
}

/* Write back data caches, and invalidates instruction caches */
/* NOTE: only used on SH4 for now */
void gdb_flush_cache(void *start UNUSED, void *end UNUSED)
{
}


/*
  Uses a TRAP to generate an exception
  after we execute the next instruction.
*/

short gdb_stepped_opcode=STEP_OPCODE;
unsigned long  gdb_step_pc;

void gdb_step ( char *hargs )
{
  long addr = 0;


  /* parse address, if any */
  while( *hargs != '#' )
    addr = ( addr << 4 ) + hex_to_long( *hargs++ );

  /* if we're stepping from an address, adjust pc (untested!) */
  /* TODO: test gdb_step when PC is supplied */
  if( addr ) gdb_register_file.pc = addr;

  /* determine where the target instruction will send us to */
  addr = gdb_get_stepi_dest();

  /* replace it */
  gdb_stepped_opcode = *(short*)addr;
  gdb_step_pc = addr;
  *(short*)addr = STEP_OPCODE;  /* FIXME: Use HW break if PC is in flash */
  
  /* we're all done now */
  gdb_return_from_exception();

  return;
}

/* 
   pc could be bad when stepping trapa #3 ( gdb breakpoint )
   or when other exception occurs in stepi 
   so remember address of replaced instruction separately
*/
void gdb_undo_step ( void )     /* FIXME: If using HW steps, undo hw steps. */
{
  /* quite bad idea to use 0 here, 0 is NOP */
  /* STEP_OPCODE should be ok               */
  if( gdb_stepped_opcode != STEP_OPCODE) {
    *(short*)gdb_step_pc = gdb_stepped_opcode;
    gdb_stepped_opcode = 0;
  }
  return;
}


/*
  Continue program execution at addr,
  or at the current pc if addr == 0.
*/
void gdb_continue ( char *hargs )
{
  long addr = 0;


  /* parse address, if any */
  while( *hargs != '#' )
    addr = ( addr << 4 ) + hex_to_long( *hargs++ );

  if( addr )
    gdb_register_file.pc = addr;

  gdb_return_from_exception();

  return;
}

/* breakpoint support 
   If we would like to support hw breakpoints, we needt suport 
   SW breakpoints to. Another solution would be to patch GDB
*/

#if USE_BREAKPOINTS

struct breakpoint {
  long addr;
  unsigned char len;
  signed char type;         /* BP type, -1 when free pos */ 
  unsigned short oldval;    /* replaced opcode for sw, channel for HW */  
} breakpoints[MAX_BREAKPOINTS];

#if USE_HW_BREAKPOINTS
signed char pbc_use[2];     /* breakpoint idx for given channel or -1 */
#endif

/* function to insert/remove brekpoints */
void gdb_breakpoint_1(int on, char* hargs) {
  char type;
  long addr=0;
  short len=0;
  short b_idx, f_idx;
  short i;
  unsigned char err;
  char tx_buf[2];
  unsigned char val;

  /* packet could be Z1,ADDR# or Z?,ADDR,LEN# */
  type=(*hargs++)-'0';
  /* skip , */
  hargs++;
  while( *hargs != ',' && *hargs!='#')
    addr = ( addr << 4 ) + hex_to_long( *hargs++ );
  if(*hargs==',') { 
    /* skip , */
    hargs++;
    while( *hargs != '#' )
      len = ( len << 4 ) + hex_to_long( *hargs++ );
  } else {
    /* length for FETCH */
    len=2;
  }
  /* find breakpoint entry or free space */
  b_idx=-1; 
  f_idx=-1;  /* free entry */
  for(i=0;i<MAX_BREAKPOINTS;i++) {
    if(breakpoints[i].type==type
       && breakpoints[i].addr==addr
       && breakpoints[i].len==len) {
      b_idx=i;
      break;
    }
    if(f_idx<0 && breakpoints[i].type<0)
      f_idx=i;
  }
  if(b_idx>=0) {  /* we found existing breakpoint */
    if(on) {  /* already inserted */
      ; // no op
    } else {  /* remove breakpoint */
      if(type==0) { /* remove sw breakpoint */
	*(unsigned short*)(addr)=breakpoints[b_idx].oldval;
      } else { /* remove HW breakpoint/watchpoint */
#if USE_HW_BREAKPOINTS
	/* just disable interrupt for given register */
	PBC_BCRX(breakpoints[b_idx].oldval)&=~BIT(0);
	/* and mark it free */
	pbc_use[breakpoints[b_idx].oldval]=-1;
#else
	err=1;
	goto ret_err;
#endif
      }
      breakpoints[b_idx].type=-1;
    }
  } else {  /* breakpoint not found */
    if(on) { /* insert new breakpoint */
      if(f_idx<0) { /* we run out of breakpoints */
	err=2;
	goto ret_err;
      }
      breakpoints[f_idx].type=type;
      breakpoints[f_idx].addr=addr;
      breakpoints[f_idx].len=len;
      if(type==0) { /* insert software breakpoint */
	breakpoints[f_idx].oldval=*(unsigned short*)(addr);
	*(unsigned short*)(addr)=STEP_OPCODE;
      } else { /* insert HW breakpoint */
#if USE_HW_BREAKPOINTS
	/* find free channel */
	if(pbc_use[0]<0) i=0;   /* FIXME: Add support for HW stepping. */
	else if(pbc_use[1]<0) i=1;
	else {
	  /* no channel free */
	  err=4;
	  goto ret_err;  
	}
	/* build control byte */
	val=BIT(0);   /* enabled (FIXME this could be problem for watchpoints
			 Better enable them just before leaving into code */
	/* add size bits */
	if     (len<=1<<0)  val|=0<<3;  /* nop */
	else if((addr&0x0001)+len<=1<<1)  val|=1<<3;
	else if((addr&0x0003)+len<=1<<2)  val|=2<<3;
	else if((addr&0x0007)+len<=1<<3)  val|=3<<3;
	else if((addr&0x000f)+len<=1<<4)  val|=4<<3;
	else if((addr&0x00ff)+len<=1<<8)  val|=5<<3;
	else if((addr&0x0fff)+len<=1<<12) val|=6<<3;
	else if((addr&0xffff)+len<=1L<<16) val|=7<<3;
	else { 
	  /* cant insert this watchpoint */
	  err=5;
	  goto ret_err;
	}
	/* add type bits */
	switch(type) {
	case 1: val|=0<<1; break;
	case 2: val|=2<<1; break;
	case 3: val|=1<<1; break;
	case 4: val|=3<<1; break;
	default:
	  err=5;
	  goto ret_err;
	}
	(volatile void)PBC_BCRX(i);   /* read hit flag to clear it in next instruction */
	PBC_BCRX(i)=val;
	PBC_BARX(i)=addr;
	breakpoints[f_idx].oldval=i;
	pbc_use[i]=f_idx;
#else
	err=2;
	goto ret_err;
#endif
      }
    } else {  /* gdb is trying to delete nonexisting breakpoint */
      err=3;
      goto ret_err;
    }
  }
  gdb_putmsg(0, "OK", 2);
  return;
 ret_err:
  tx_buf[0]=lnibble_to_hex(err>>4);
  tx_buf[1]=lnibble_to_hex(err&0xff);
  gdb_putmsg('E',tx_buf, 2);
}

void gdb_insert_breakpoint( char *hargs ) {
  gdb_breakpoint_1(1, hargs);
}

void gdb_remove_breakpoint( char *hargs ) {
  gdb_breakpoint_1(0, hargs);
}

/* initialize breakpoints table */
/* wake PBC */
void gdb_breakpoint_init(void) {
  short i;
  for(i=0;i<MAX_BREAKPOINTS;i++) {
    breakpoints[i].type=-1;
  }
#if USE_HW_BREAKPOINTS
  MSTPCRC&=~BIT(4);     /* enable PBC */
  pbc_use[0]=-1;
  pbc_use[1]=-1;
  PBC_BCRA=0;
  PBC_BCRB=0;
#endif
}

#endif /* USE_BREAKPOINTS */
/*
  Kills the current application.
  Simulates a reset by jumping to
  the address taken from the reset
  vector at address 0.
 */
void gdb_kill ( void )
{
  /* return control to monitor */
  /* skip this for now, HDI hates this ;-) */
  /* asm( "jmp @@0"); */
}

#if defined(HW_INDEPENDENT)&&HW_INDEPENDENT
#include "cpu_def.h"
#include "system_def.h"
/* #include "h8s2638h.h" */

/* extern void *excptvec_get(int vectnum); */
/* extern void *excptvec_set(int vectnum,void *vect); */
/* extern int excptvec_initfill(void *fill_vect, int force_all); */
#endif /* HW_INDEPENDENT */

/* initialize this target */
void gdb_platform_init() {
#if defined(HW_INDEPENDENT)&&HW_INDEPENDENT
  static const int sci_vect_tab[]=
    {EXCPTVEC_ERI0,EXCPTVEC_ERI1,EXCPTVEC_ERI2};
  int sci_vect=sci_vect_tab[GDB_SCI_PORT];
  
  excptvec_initfill(gdb_unhandled_isr,0);

  excptvec_set(sci_vect,gdb_scirxerr_isr);	/* ERIx */
  excptvec_set(sci_vect+1,gdb_scirx_isr);	/* RXIx */
  excptvec_set(sci_vect+2,gdb_unhandled_isr);	/* TXIx */
  excptvec_set(sci_vect+3,gdb_unhandled_isr);	/* TEIx */

  excptvec_set(EXCPTVEC_NMI,gdb_nmi_isr);	/* NMI */
  excptvec_set(EXCPTVEC_TRAP2,gdb_trapa2_isr);	/* TRAPA2 */
  excptvec_set(EXCPTVEC_TRAP3,gdb_trapa3_isr);	/* TRAPA3 */
  excptvec_set(EXCPTVEC_PBC,gdb_pbc_isr);	/* PCB */
  
  
#endif /* HW_INDEPENDENT */

  gdb_sci_init(SCI_BAUD(GDB_SCI_SPEED), SCI_8N1);
#if USE_BREAKPOINTS
  gdb_breakpoint_init();
#endif

}

/* initialize this target */
int gdb_platform_enable(int en) {
  int olden=!gdb_in_gdb;
  if(en) {
    SCI_SCR|=1<<6;
    gdb_in_gdb=0;
  }else{
    gdb_in_gdb=0x80;
  }
  return olden;
}

/* assembly function moved into separate file */

