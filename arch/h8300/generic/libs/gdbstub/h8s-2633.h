#ifndef H8S_2633_H
#define H8S_2633_H

extern void gdb_unhandled_isr ( void );
extern void gdb_pbc_isr ( void );
extern void gdb_trapa2_isr ( void );
extern void gdb_trapa3_isr ( void );
extern void gdb_nmi_isr ( void );
extern void gdb_scirx_isr ( void );
extern void gdb_scirxerr_isr ( void );
extern unsigned char gdb_in_gdb;

#endif
