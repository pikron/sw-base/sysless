/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  excptvec.c - exception and interrupt table manipulation for
               H8S 2633 - uses part of internal SRAM memory
	       to overlay beginning of flash memory -> this
	       enables to change interrupt vectors for
	       applicatons loaded into RAM
 
  Copyright (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#include <system_def.h>
#include <cpu_def.h>
#include <mcu_regs.h>
#include <string.h>
#include <h8300config.h>

void *excptvec_get(int vectnum)
{
  uint32_t *pvect;
  pvect=(uint32_t*)((uint32_t)(vectnum<<2)+0);
  return (void*)*pvect;
}


void *excptvec_set(int vectnum,void *vect)
{
  uint32_t *pvect;
  void *ovect;
  pvect=(uint32_t*)((uint32_t)(vectnum<<2)+0);
  ovect=(void*)*pvect;
  *pvect=(uint32_t)vect;
  return ovect;
}


int excptvec_initfill(void *fill_vect, int force_all)
{
  uint32_t *pvect;
  int i;
  uint32_t l;

#ifdef CONFIG_USE_EXR_LEVELS
  *SYS_SYSCR = *SYS_SYSCR | 1*SYSCR_INTM1m;
#endif
  if((*FLM_RAMER&(RAMER_RAMSm|RAMER_RAMxm))!=RAMER_RAMSm){
    memcpy((void*)0xffd000,(void*)0,0x1000);
    *FLM_RAMER=RAMER_RAMSm+0;
  }
  
  for(i=0,pvect=0;i<128;i++,pvect++){
    l=(uint32_t)*pvect;
    if((l==0)||(l==0xffffffff)||force_all)
      *pvect=(uint32_t)fill_vect;
  }
  
  return 0;
}

