/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  system_stub.c - stubs for system routines needed
                  by NewLib C library
 
  Copyright (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#include <stdint.h>
#include <system_def.h>
#include <sys/stat.h>
#include <cpu_def.h>
#include <errno.h>
#include <periph/sci_rs232.h>

/***********************************************************/

char inbyte()
{
  int ch;
  while((ch=sci_rs232_recch(sci_rs232_chan_default))<0)
    { /* wait for char */ ; }
  return ch;
}

int outbyte(unsigned char ch)
{
  int res;
  while((res=sci_rs232_sendch(ch, sci_rs232_chan_default))<0)
    { /* wait for send */ ; }
  return res;
}

/***********************************************************/

/*
 * close -- We don't need to do anything, but pretend we did.
 */
int _close (int fd)
{
  return (0);
}

/*
 * fstat -- Since we have no file system, we just return an error.
 */
int _fstat(int fd,struct stat *buf)
{
  buf->st_mode = S_IFCHR;       /* Always pretend to be a tty */
  buf->st_blksize = 0;
  return (0);
}

/*
 * getpid -- only one process, so just return 1.
 */
int _getpid()
{
  return 1;
}

/*
 * isatty -- returns 1 if connected to a terminal device,
 *           returns 0 if not. Since we're hooked up to a
 *           serial port, we'll say yes _AND return a 1.
 */
int isatty(int fd)
{
  return (1);
}

/*
 * kill -- go out via exit...
 */
int _kill(int pid,int sig)
{
  if(pid == 1)
    _exit(sig);
  return 0;
}


/*
 * lseek --  Since a serial port is non-seekable, we return an error.
 */
off_t _lseek(int fd, off_t offset, int whence)
{
  errno = ESPIPE;
  return ((off_t)-1);
}

/*
 * open -- open a file descriptor. We don't have a filesystem, so
 *         we return an error.
 */
int _open(const char *buf, int flags, int mode)
{
  errno = EIO;
  return (-1);
}

/*
 * print -- do a raw print of a string
 */
void _print(char *ptr)
{
  while (*ptr) {
    outbyte (*ptr++);
  }
}

/*
 * putnum -- print a 32 bit number in hex
 */
void _putnum(unsigned int num)
{
  char  buf[9];
  int   cnt;
  char  *ptr;
  int   digit;

  ptr = buf;
  for (cnt = 7 ; cnt >= 0 ; cnt--) {
    digit = (num >> (cnt * 4)) & 0xf;

    if (digit <= 9)
      *ptr++ = (char) ('0' + digit);
    else
      *ptr++ = (char) ('a' - 10 + digit);
  }

  *ptr = (char) 0;
  _print (buf);
}

/*
 * read  -- read bytes from the serial port. Ignore fd, since
 *          we only have stdin.
 */
int _read(int fd,char *buf,int nbytes)
{
  int i = 0;

  for (i = 0; i < nbytes; i++) {
    *(buf + i) = inbyte();
    if ((*(buf + i) == '\n') || (*(buf + i) == '\r')) {
      (*(buf + i + 1)) = 0;
      break;
    }
  }
  return (i);
}

char *heap_ptr=0;
char *heap_end=0;
extern char end;
extern char __heap_end;
#ifndef RESERVED_FOR_STACK
#define RESERVED_FOR_STACK 1024
#endif /*RESERVED_FOR_STACK*/

/*
 * sbrk -- changes heap size size. Get nbytes more
 *         RAM. We just increment a pointer in what's
 *         left of memory on the board.
 */
char *_sbrk(int nbytes)
{
  char        *base;

  if (!heap_ptr)
  {
    heap_ptr = (char *)&end;
    heap_end=(char*)&__heap_end;
  }

 #if 1
  if(heap_end){
    if (heap_end - heap_ptr < nbytes) {
      errno = ENOMEM;
      return ((char *)-1);
    }
  }
  #if 1
  else{
    long sp;
    __get_sp(sp);
    if (sp - (long)heap_ptr < nbytes + RESERVED_FOR_STACK) {
      errno = ENOMEM;
      return ((char *)-1);
    }
  }
  #endif
 #endif

  base = heap_ptr;
  heap_ptr += nbytes;
  return base;
}

/*
 * stat -- Since we have no file system, we just return an error.
 */
int _stat(const char *path, struct stat *buf)
{
  errno = EIO;
  return (-1);
}

/*
 * unlink -- since we have no file system,
 *           we just return an error.
 */
int _unlink(char * path)
{
  errno = EIO;
  return (-1);
}

/*
 * write -- write bytes to the serial port. Ignore fd, since
 *          stdout and stderr are the same. Since we have no filesystem,
 *          open will only return an error.
 */
int _write(int fd, char *buf, int nbytes)
{
  int i;

  for (i = 0; i < nbytes; i++) {
    if (*(buf + i) == '\n') {
      outbyte ('\r');
    }
    outbyte (*(buf + i));
  }
  return (nbytes);
}
