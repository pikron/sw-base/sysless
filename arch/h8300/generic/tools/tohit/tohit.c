#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <termios.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <getopt.h>
#include "tohit_fn.h"

#define DEBUG 0
#define HAS_GETOPT_LONG 1

int go_flg=0;
int reset_flg=0;
int break_flg=0;
int upload_flg=0;
int blockerase_flg=0;
int regerase_flg=0;

int command=TOHIT_WRITE;
int blockmode=0;
int erase_block=-1;
unsigned long mem_start=0;
unsigned long mem_length=0;
unsigned long go_addr=0;

#define MEM_BUF_LEN 0x40000

unsigned char mem_buf[MEM_BUF_LEN];

static void
usage(void)
{
  printf("usage: tohit <parameters> <send_file>\n");
  printf("  -d, --sdev <name>        name of RS232 device [%s]\n",tohit_sdev);
  printf("  -B, --baud <num>         RS232 baudrate [%d]\n",tohit_baud);
  printf("  -c, --command <num>      numeric command value (B means to use on-chip boot-mode algorithm)\n");
  printf("  -b, --blockmode <num>    block size\n");
  printf("  -w, --wait-reply <num>   time to wait for reply in ms\n");
  printf("  -e, --erase		     erase region defined by -s -l\n");
  printf("  -E, --blockerase <block> erase block\n");
  printf("  -s, --start  <addr>      start address of transfer\n");
  printf("  -l, --length <num>       length of upload block\n");
  printf("  -g, --go     <addr>      start program from address\n");
  printf("  -r, --reset              reset before download\n");
  printf("  -k, --break              send communication break character\n");
  printf("  -u, --upload             upload memory block [download]\n");
  printf("  -f, --format <format>    format of data file [intelhex]\n");
  printf("  -v, --verbose            increase verbosity level\n");
  printf("  -V, --version            show version\n");
  printf("  -h, --help               this usage screen\n");
}

int main(int argc, char **argv) 
{
  int i;
  FILE *F;
  
  static struct option long_opts[] = {
    { "sdev",  1, 0, 'd' },
    { "baud",  1, 0, 'B' },
    { "command",1,0, 'c' },
    { "blockmode",1,0,'b' },
    { "wait-reply",1,0,'w' },
    { "blockerase", 1, 0, 'E' },
    { "erase", 0, 0, 'e' },
    { "start", 1, 0, 's' },
    { "length",1, 0, 'l' },
    { "go",    1, 0, 'g' },
    { "reset", 0, 0, 'r' },
    { "break", 0, 0, 'k' },
    { "upload",0, 0, 'u' },
    { "format",1, 0, 'f' },
    { "version",0,0, 'V' },
    { "verbose",0,0, 'v' },
    { "help",  0, 0, 'h' },
    { 0, 0, 0, 0}
  };
  int opt;

 #ifndef HAS_GETOPT_LONG
  while ((opt = getopt(argc, argv, "d:B:c:b:w:E:es:l:g:rkuf:vVhD:")) != EOF)
 #else
  while ((opt = getopt_long(argc, argv, "d:B:c:b:w:E:es:l:g:rkuf:vVhD:",
			    &long_opts[0], NULL)) != EOF)
 #endif
  switch (opt) {
    case 'd':
      tohit_sdev=optarg;
      break;
    case 'B':
      tohit_baud = strtol(optarg,NULL,0);
      break;
    case 'c':
      if(optarg[0]=='B'){
        command=TOHIT_WRITEBB;
	break;
      }
      command = strtol(optarg,NULL,0);
      break;
    case 'b':
      blockmode = strtol(optarg,NULL,0);
      break;
    case 'w':
      tohit_waitrep = strtol(optarg,NULL,0)*1000;
      break;
    case 'E':
      erase_block = strtol(optarg,NULL,0);
      blockerase_flg=1;
      break;
    case 'e':
      regerase_flg=1;
      break;
    case 's':
      mem_start = strtol(optarg,NULL,0);
      break;
    case 'l':
      mem_length = strtol(optarg,NULL,0);
      break;
    case 'g':
      go_addr = strtol(optarg,NULL,0);
      go_flg = 1;
      break;
    case 'r':
      reset_flg = 1;
      break;
    case 'k':
      break_flg = 1;
      break;
    case 'u':
      upload_flg = 1;
      break;
    case 'f':
      break;
    case 'v':
      tohit_verbosity++;
      break;
    case 'V':
      fputs("tohit pre alpha\n", stdout);
      exit(0);
    case 'h':
    default:
      usage();
      exit(opt == 'h' ? 0 : 1);
  }

  if(break_flg){
    if(tohit_break()<0){
      fprintf(stderr,"Error in tohit_break\n");
      exit(1); 
    }
  }

  if(reset_flg){
    if(tohit_reset()<0){
      fprintf(stderr,"Error in tohit_reset\n");
      exit(1); 
    }
  }

  if(blockerase_flg){
    if(tohit_blockerase(erase_block)<0){
      fprintf(stderr,"Error in tohit_blockerase\n");
      exit(1); 
    }
  }

  if(regerase_flg){
    if(tohit_regerase(mem_start,mem_length)<0){
      fprintf(stderr,"Error in tohit_regerase\n");
      exit(1); 
    }
  }

  if(!upload_flg&&(optind<argc)){
    F=fopen(argv[optind++],"r");
    if(F==NULL){
      fprintf(stderr,"Error to open file for reading\n");
      exit(1); 
    }
    mem_length=fread(mem_buf,1,MEM_BUF_LEN,F);
    if(mem_length<0){
      fprintf(stderr,"Error to read mem contents\n");
      fclose(F);
      exit(1); 
    }
    fclose(F);
  
    if(blockmode){
      for(i=1;(i<blockmode)&&(i<128);i<<=1);
      blockmode=i;
    }
    if(tohit_writemem(command,mem_buf,mem_start,mem_length,blockmode)<0){
      fprintf(stderr,"Error in tohit_writemem\n");
      exit(1); 
    }

  }else 
  if(upload_flg&&(optind<argc)){
    if(command==0) command=TOHIT_READ;
    if(mem_length>MEM_BUF_LEN) mem_length=MEM_BUF_LEN;
    if(tohit_readmem(command,mem_buf,mem_start,mem_length,blockmode)<0){
      fprintf(stderr,"Error in tohit_readmem\n");
      exit(1); 
    }
    F=fopen(argv[optind++],"w");
    if(F==NULL){
      fprintf(stderr,"Error to open file for writting\n");
      exit(1); 
    }
    if(mem_length!=fwrite(mem_buf,1,mem_length,F)){
      fprintf(stderr,"Error to write mem contents to file\n");
      fclose(F);
      exit(1); 
    }
    fclose(F);
   }

  if(go_flg){
    if(tohit_goto(go_addr)<0){
      fprintf(stderr,"Error in tohit_goto\n");
      exit(1); 
    }
  }
  
  return 0;
}
