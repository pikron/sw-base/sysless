#ifndef _TOHIT_FN_H
#define _TOHIT_FN_H

/* cmd -1 write memory using ROM boot loader */
/* cmd 0 write memory */
/* cmd 1 write memory */
/* cmd 2 read memory */
/* cmd 3 erase flash block */
/* cmd 4 erase region*/
/* cmd 6 call address */
/* cmd 7 reset */

#define TOHIT_WRITEBB   (-1)
#define TOHIT_WRITE       0
#define TOHIT_WRITEFL	  1
#define TOHIT_READ	  2
#define TOHIT_ERASEBL	  3
#define TOHIT_ERASEREG	  4
#define TOHIT_GOTO	  6
#define TOHIT_RESET	  7

int rs232_setmode(int fd, int baud, int mode, int flowc);

int rs232_sendch(int fd,unsigned char c);

int rs232_recch(int fd);

int rs232_test(int fd,int time);

void tohit_sendi(int fd,long a, int bytes);

long tohit_reci(int fd, int bytes);

int tohit_sendichk(int fd,long a, int bytes);

int tohit_synchronize(int fd);

int tohit_open4cmd(char *sdev, int baud, int cmd);

int tohit_cmdrepchk(int fd);

int tohit_goto(unsigned long adr);

int tohit_writemem(int cmd, const unsigned char *buf, 
	           unsigned long adr, unsigned long size, int blockmode);

int tohit_readmem(int cmd, unsigned char *buf, 
	          unsigned long adr, unsigned long size, int blockmode);

int tohit_blockerase(int block);

int tohit_regerase(unsigned long adr, unsigned long size);


char *tohit_sdev;
int tohit_baud;
long tohit_waitrep;
extern int tohit_verbosity;

int tohit_reset(void);

int tohit_break(void);

#endif /* _TOHIT_FN_H */


