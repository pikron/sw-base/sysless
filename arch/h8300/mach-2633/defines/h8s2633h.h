/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
   h8s2633h.h - internal peripherals registers of H8S2633
 
  Copyright (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#ifndef _H82633H_H
#define _H82633H_H

#ifndef __ASSEMBLY__

#include <stdint.h>

#define __PORT8  (volatile uint8_t  * const)
#define __PORT16 (volatile uint16_t * const)
#define __PORT32 (volatile uint32_t * const)

#else	/* __ASSEMBLY__ */
#define __PORT8
#define __PORT16
#define __PORT32
#endif	/* __ASSEMBLY__ */

#define DA_DADR2	__PORT8 0xFFFDAC	/* DA Data Register 2 */ 
#define DA_DADR3	__PORT8 0xFFFDAD	/* DA Data Register 3 */ 
#define DA_DACR23	__PORT8 0xFFFDAE	/* DA Control Register 23 */ 
#define   DACR23_DAEm	0x20 
#define   DACR23_DAOE0m	0x40 
#define   DACR23_DAOE1m	0x80 
#define SCI_IrCR	__PORT8 0xFFFDB0	/* IrDA Control Register */ 
#define   IrCR_IrCKS0m	0x10 
#define   IrCR_IrCKS1m	0x20 
#define   IrCR_IrCKS2m	0x40 
#define   IrCR_IrEm	0x80 
#define IIC_SCRX	__PORT8 0xFFFDB4	/* Serial Control Register X */ 
#define   SCRX_FLSHEm	0x08 
#define   SCRX_IICEm	0x10 
#define   SCRX_IICX0m	0x20 
#define   SCRX_IICX1m	0x40 
#define IIC_DDCSWR	__PORT8 0xFFFDB5	/* DDC Switch Register */ 
#define   DDCSWR_CLR0m	0x01 
#define   DDCSWR_CLR1m	0x02 
#define   DDCSWR_CLR2m	0x04 
#define   DDCSWR_CLR3m	0x08 
#define   DDCSWR_IFm	0x10 
#define   DDCSWR_IEm	0x20 
#define   DDCSWR_SWm	0x40 
#define   DDCSWR_SWEm	0x80 
#define PWM_DACR0	__PORT8 0xFFFDB8	/* PWM (DA) Control Register 0 */ 
#define PWM_DADRAH0	__PORT8 0xFFFDB8	/* PWM (DA) Data Register AH0 */ 
#define   DADRAH0_DA6CKSm	0x01 
#define   DADRAH0_DA7OSm	0x02 
#define   DADRAH0_DA8m	0x04 
#define   DADRAH0_DA9m	0x08 
#define   DADRAH0_DA10m	0x10
#define   DADRAH0_DA11m	0x20
#define   DADRAH0_DA12m	0x40 
#define   DADRAH0_DA13m	0x80 
#define PWM_DADRAL0	__PORT8 0xFFFDB9	/* PWM (DA) Data Register AL0 */ 
#define   DADRAL0_CFSm	0x02 
#define   DADRAL0_DA0m	0x04 
#define   DADRAL0_DA1m	0x08 
#define   DADRAL0_DA2m	0x10 
#define   DADRAL0_DA3m	0x20 
#define   DADRAL0_DA4m	0x40 
#define   DADRAL0_DA5m	0x80 
#define PWM_DACNTH0	__PORT8 0xFFFDBA	/* PWM (DA) Counter H0 */ 
#define PWM_DADRBH0	__PORT8 0xFFFDBA	/* PWM (DA) Data Register BH0 */ 
#define   DADRBH0_DA6m	0x01 
#define   DADRBH0_DA7m	0x02 
#define   DADRBH0_DA8m	0x04 
#define   DADRBH0_DA9m	0x08 
#define   DADRBH0_DA10m	0x10 
#define   DADRBH0_DA11m	0x20 
#define   DADRBH0_DA12m	0x40 
#define   DADRBH0_DA13m	0x80 
#define PWM_DACNTL0	__PORT8 0xFFFDBB	/* PWM (DA) Counter L0 */ 
#define PWM_DADRBL0	__PORT8 0xFFFDBB	/* PWM (DA) Data Register BL0 */ 
#define   DADRBL0_REGSm	0x01 
#define   DADRBL0_CFSm	0x02 
#define   DADRBL0_DA0m	0x04 
#define   DADRBL0_DA1m	0x08 
#define   DADRBL0_DA2m	0x10 
#define   DADRBL0_DA3m	0x20 
#define   DADRBL0_DA4m	0x40 
#define   DADRBL0_DA5m	0x80 
#define PWM_DACR1	__PORT8 0xFFFDBC	/* PWM (DA) Control Register 1 */ 
#define PWM_DADRAH1	__PORT8 0xFFFDBC	/* PWM (DA) Data Register AH1 */ 
#define   DADRAH1_8m	0x01 
#define   DADRAH1_PWM1m	0x02 
#define   DADRAH1_DA6CKSm	0x04 
#define   DADRAH1_DA9OEBDA8OEADA7OSm	0x08 
#define   DADRAH1_DA10m	0x10 
#define   DADRAH1_DA11m	0x20 
#define   DADRAH1_DA12m	0x40 
#define   DADRAH1_DA13m	0x80 
#define PWM_DADRAL1	__PORT8 0xFFFDBD	/* PWM (DA) Data Register AL1 */ 
#define   DADRAL1_CFSm	0x02 
#define   DADRAL1_DA0m	0x04 
#define   DADRAL1_DA1m	0x08 
#define   DADRAL1_DA2m	0x10 
#define   DADRAL1_DA3m	0x20 
#define   DADRAL1_DA4m	0x40 
#define   DADRAL1_DA5m	0x80 
#define PWM_DACNTH1	__PORT8 0xFFFDBE	/* PWM (DA) Counter H1 */ 
#define PWM_DADRBH1	__PORT8 0xFFFDBE	/* PWM (DA) Data Register BH1 */ 
#define   DADRBH1_DA6m	0x01 
#define   DADRBH1_DA7m	0x02 
#define   DADRBH1_DA8m	0x04 
#define   DADRBH1_DA9m	0x08 
#define   DADRBH1_DA10m	0x10 
#define   DADRBH1_DA11m	0x20 
#define   DADRBH1_DA12m	0x40 
#define   DADRBH1_DA13m	0x80 
#define PWM_DACNTL1	__PORT8 0xFFFDBF	/* PWM (DA) Counter L1 */ 
#define PWM_DADRBL1	__PORT8 0xFFFDBF	/* PWM (DA) Data Register BL1 */ 
#define   DADRBL1_REGSm	0x01 
#define   DADRBL1_CFSm	0x02 
#define   DADRBL1_DA0m	0x04 
#define   DADRBL1_DA1m	0x08 
#define   DADRBL1_DA2m	0x10 
#define   DADRBL1_DA3m	0x20 
#define   DADRBL1_DA4m	0x40 
#define   DADRBL1_DA5m	0x80 
#define TMR_TCR2	__PORT8 0xFFFDC0	/* Timer Control Register 2 */ 
#define   TCR2_CKS0m	0x01 
#define   TCR2_CKS1m	0x02 
#define   TCR2_CKS2m	0x04 
#define   TCR2_CCLR0m	0x08 
#define   TCR2_CCLR1m	0x10 
#define   TCR2_OVIEm	0x20 
#define   TCR2_CMIEAm	0x40 
#define   TCR2_CMIEBm	0x80 
#define TMR_TCR3	__PORT8 0xFFFDC1	/* Timer Control Register 3 */ 
#define   TCR3_CKS0m	0x01 
#define   TCR3_CKS1m	0x02 
#define   TCR3_CKS2m	0x04 
#define   TCR3_CCLR0m	0x08 
#define   TCR3_CCLR1m	0x10 
#define   TCR3_OVIEm	0x20 
#define   TCR3_CMIEAm	0x40 
#define   TCR3_CMIEBm	0x80 
#define TMR_TCSR2	__PORT8 0xFFFDC2	/* Timer ControlStatus Register 2 */ 
#define   TCSR2_OS0m	0x01 
#define   TCSR2_OS1m	0x02 
#define   TCSR2_OS2m	0x04 
#define   TCSR2_OS3m	0x08 
#define   TCSR2_OVFm	0x20 
#define   TCSR2_CMFAm	0x40 
#define   TCSR2_CMFBm	0x80 
#define TMR_TCSR3	__PORT8 0xFFFDC3	/* Timer ControlStatus Register 3 */ 
#define   TCSR3_OS0m	0x01 
#define   TCSR3_OS1m	0x02 
#define   TCSR3_OS2m	0x04 
#define   TCSR3_OS3m	0x08 
#define   TCSR3_OVFm	0x20 
#define   TCSR3_CMFAm	0x40 
#define   TCSR3_CMFBm	0x80 
#define TMR_TCORA2	__PORT8 0xFFFDC4	/* Time Constant Register A2 */ 
#define TMR_TCORA3	__PORT8 0xFFFDC5	/* Time Constant Register A3 */ 
#define TMR_TCORB2	__PORT8 0xFFFDC6	/* Time Constant Register B2 */ 
#define TMR_TCORB3	__PORT8 0xFFFDC7	/* Time Constant Register B3 */ 
#define TMR_TCNT2	__PORT8 0xFFFDC8	/* Timer Counter 2 */ 
#define TMR_TCNT3	__PORT16 0xFFFDC9	/* Timer Counter 3 */ 
#define SCI_SMR3	__PORT8 0xFFFDD0	/* Serial Mode Register 3 */ 
#define SCI_SMR3	__PORT8 0xFFFDD0	/* Serial Mode Register 3 */ 
#define   SMR3_CKS0m	0x01 
#define   SMR3_CKS1m	0x02 
#define   SMR3_MPm	0x04 
#define   SMR3_STOPm	0x08 
#define   SMR3_OEm	0x10 
#define   SMR3_PEm	0x20 
#define   SMR3_CHRm	0x40 
#define   SMR3_CAm	0x80 
#define SCI_BRR3	__PORT8 0xFFFDD1	/* Bit Rate Register 3 */ 
#define SCI_SCR3	__PORT8 0xFFFDD2	/* Serial Control Register 3 */ 
#define   SCR3_CKE0m	0x01 
#define   SCR3_CKE1m	0x02 
#define   SCR3_TEIEm	0x04 
#define   SCR3_MPIEm	0x08 
#define   SCR3_REm	0x10 
#define   SCR3_TEm	0x20 
#define   SCR3_RIEm	0x40 
#define   SCR3_TIEm	0x80 
#define SCI_TDR3	__PORT8 0xFFFDD3	/* Transmit Data Register 3 */ 
#define SCI_SSR3	__PORT8 0xFFFDD4	/* Serial Status Register 3 */ 
#define   SSR3_MPBTm	0x01 
#define   SSR3_MPBm	0x02 
#define   SSR3_TENDm	0x04 
#define   SSR3_PERm	0x08 
#define   SSR3_FERm	0x10 
#define   SSR3_ORERm	0x20 
#define   SSR3_RDRFm	0x40 
#define   SSR3_TDREm	0x80 
#define SCI_RDR3	__PORT8 0xFFFDD5	/* Receive Data Register 3 */ 
#define SCI_SCMR3	__PORT8 0xFFFDD6	/* Smart Card Mode Register 3 */ 
#define   SCMR3_SMIFm	0x01 
#define   SCMR3_SINVm	0x04 
#define   SCMR3_SDIRm	0x08 
#define SCI_SMR4	__PORT8 0xFFFDD8	/* Serial Mode Register 4 */ 
#define SCI_SMR4	__PORT8 0xFFFDD8	/* Serial Mode Register 4 */ 
#define   SMR4_CKS0m	0x01 
#define   SMR4_CKS1m	0x02 
#define   SMR4_MPm	0x04 
#define   SMR4_STOPm	0x08 
#define   SMR4_OEm	0x10 
#define   SMR4_PEm	0x20 
#define   SMR4_CHRm	0x40 
#define   SMR4_CAm	0x80 
#define SCI_BRR4	__PORT8 0xFFFDD9	/* Bit Rate Register 4 */ 
#define SCI_SCR4	__PORT8 0xFFFDDA	/* Serial Control Register 4 */ 
#define   SCR4_CKE0m	0x01 
#define   SCR4_CKE1m	0x02 
#define   SCR4_TEIEm	0x04 
#define   SCR4_MPIEm	0x08 
#define   SCR4_REm	0x10 
#define   SCR4_TEm	0x20 
#define   SCR4_RIEm	0x40 
#define   SCR4_TIEm	0x80 
#define SCI_TDR4	__PORT8 0xFFFDDB	/* Transmit Data Register 4 */ 
#define SCI_SSR4	__PORT8 0xFFFDDC	/* Serial Status Register 4 */ 
#define   SSR4_MPBTm	0x01 
#define   SSR4_MPBm	0x02 
#define   SSR4_TENDm	0x04 
#define   SSR4_PERm	0x08 
#define   SSR4_FERm	0x10 
#define   SSR4_ORERm	0x20 
#define   SSR4_RDRFm	0x40 
#define   SSR4_TDREm	0x80 
#define SCI_RDR4	__PORT8 0xFFFDDD	/* Receive Data Register 4 */ 
#define SCI_SCMR4	__PORT8 0xFFFDDE	/* Smart Card Mode Register 4 */ 
#define   SCMR4_SMIFm	0x01 
#define   SCMR4_SINVm	0x04 
#define   SCMR4_SDIRm	0x08 
#define SYS_SBYCR	__PORT8 0xFFFDE4	/* Standby Control Register */ 
#define   SBYCR_OPEm	0x08 
#define   SBYCR_STS0m	0x10 
#define   SBYCR_SYS1m	0x20 
#define   SBYCR_STS2m	0x40 
#define   SBYCR_SSBYm	0x80 
#define SYS_SYSCR	__PORT8 0xFFFDE5	/* SYS Control Register */ 
#define   SYSCR_RAMEm	0x01 
#define   SYSCR_MRESEm	0x04 
#define   SYSCR_NMIEGm	0x08 
#define   SYSCR_INTM0m	0x10 
#define   SYSCR_INTM1m	0x20 
#define   SYSCR_MACSm	0x80 
#define SYS_SCKCR	__PORT8 0xFFFDE6	/* SYS Clock Control Register */ 
#define   SCKCR_SCK0m	0x01			/*  Bus master clock selection */ 
#define   SCKCR_SCK1m	0x02 			/*  0=full, 1=/2, 2=/4 3=/8 */
#define   SCKCR_SCK2m	0x04 			/*  4=/16, 5=/32 */
#define   SCKCR_SCKxm	0x07
#define   SCKCR_STCSm	0x08			/*   1=Immediately change, 0=at Stby */ 
#define   SCKCR_PSTOPm	0x80			/*   1=Clock Output Disable */ 
#define SYS_MDCR	__PORT8 0xFFFDE7	/* Mode Control Register */ 
#define   MDCR_MDS0m	0x01 
#define   MDCR_MDS1m	0x02 
#define   MDCR_MDS2m	0x04 
#define SYS_MSTPCRA	__PORT8 0xFFFDE8	/* Module Stop Control Register A */ 
#define   MSTPCRA_MSTPA0m	0x01 
#define   MSTPCRA_MSTPA1m	0x02 
#define   MSTPCRA_ADCm		0x02 
#define   MSTPCRA_MSTPA2m	0x04 
#define   MSTPCRA_DA01m		0x04 
#define   MSTPCRA_MSTPA3m	0x08 
#define   MSTPCRA_MSTPA4m	0x10 
#define   MSTPCRA_MSTPA5m	0x20 
#define   MSTPCRA_TPUm		0x20 
#define   MSTPCRA_MSTPA6m	0x40 
#define   MSTPCRA_MSTPA7m	0x80 
#define SYS_MSTPCRB	__PORT8 0xFFFDE9	/* Module Stop Control Register B */ 
#define   MSTPCRB_MSTPB0m	0x01 
#define   MSTPCRB_MSTPB1m	0x02 
#define   MSTPCRB_MSTPB2m	0x04 
#define   MSTPCRB_MSTPB3m	0x08 
#define   MSTPCRB_IIC1m		0x08 
#define   MSTPCRB_MSTPB4m	0x10 
#define   MSTPCRB_IIC0m		0x10 
#define   MSTPCRB_MSTPB5m	0x20 
#define   MSTPCRB_SCI2m		0x20 
#define   MSTPCRB_MSTPB6m	0x40 
#define   MSTPCRB_SCI1m		0x40 
#define   MSTPCRB_MSTPB7m	0x80 
#define   MSTPCRB_SCI0m		0x80 
#define SYS_MSTPCRC	__PORT8 0xFFFDEA	/* Module Stop Control Register C */ 
#define   MSTPCRC_MSTPC0m	0x01 
#define   MSTPCRC_MSTPC1m	0x02 
#define   MSTPCRC_MSTPC2m	0x04 
#define   MSTPCRC_MSTPC3m	0x08 
#define   MSTPCRC_MSTPC4m	0x10 
#define   MSTPCRC_MSTPC5m	0x20 
#define   MSTPCRC_DA23m		0x20 
#define   MSTPCRC_MSTPC6m	0x40 
#define   MSTPCRC_SCI4m		0x40 
#define   MSTPCRC_MSTPC7m	0x80 
#define   MSTPCRC_SCI3m		0x80 
#define SYS_PFCR	__PORT8 0xFFFDEB	/* Pin Function Control Register */ 
#define   PFCR_AE0m	0x01 
#define   PFCR_AE1m	0x02 
#define   PFCR_AE2m	0x04 
#define   PFCR_AE3m	0x08
#define   PFCR_AExm	0x0f
#define   PFCR_LCASSm	0x10 
#define   PFCR_BUZZEm	0x20 
#define   PFCR_CSS36m	0x40 
#define   PFCR_CSS07m	0x80 
#define SYS_LPWRCR	__PORT8 0xFFFDEC	/* Low-Power Control Register */ 
#define   LPWRCR_STC0m	0x01 			/*    */
#define   LPWRCR_STC1m	0x02 
#define   LPWRCR_STCxm	0x03
#define   LPWRCR_RFCUTm	0x08 
#define   LPWRCR_SUBSTPm 0x10 
#define   LPWRCR_NESELm	0x20 
#define   LPWRCR_LSONm	0x40 
#define   LPWRCR_DTONm	0x80 
#define PBC_BARA	__PORT32 0xFFFE00	/* Break Address Register A */ 
#define PBC_BARB	__PORT32 0xFFFE04	/* Break Address Register B */ 
#define PBC_BCRA	__PORT8 0xFFFE08	/* Break Control Register A */ 
#define   BCRA_BIEAm	0x01 
#define   BCRA_CSELA0m	0x02 
#define   BCRA_CSELA1m	0x04 
#define   BCRA_BAMRA0m	0x08 
#define   BCRA_BAMRA1m	0x10 
#define   BCRA_BAMRA2m	0x20 
#define   BCRA_CDAm	0x40 
#define   BCRA_CMFAm	0x80 
#define PBC_BCRB	__PORT8 0xFFFE09	/* Break Control Register B */ 
#define   BCRB_BIEBm	0x01 
#define   BCRB_CSELB0m	0x02 
#define   BCRB_CSELB1m	0x04 
#define   BCRB_BAMRB0m	0x08 
#define   BCRB_BAMRB1m	0x10 
#define   BCRB_BAMRB2m	0x20 
#define   BCRB_CDBm	0x40 
#define   BCRB_CMFBm	0x80 
#define INT_ISCRH	__PORT8 0xFFFE12	/* IRQ Sense Control Register H */ 
#define   ISCRH_IRQ4SCAm	0x01 
#define   ISCRH_IRQ4SCBm	0x02 
#define   ISCRH_IRQ5SCAm	0x04 
#define   ISCRH_IRQ5SCBm	0x08 
#define   ISCRH_IRQ6SCAm	0x10 
#define   ISCRH_IRQ6SCBm	0x20 
#define   ISCRH_IRQ7SCAm	0x40 
#define   ISCRH_IRQ7SCBm	0x80 
#define INT_ISCRL	__PORT8 0xFFFE13	/* IRQ Sense Control Register L */ 
#define   ISCRL_IRQ0SCAm	0x01 
#define   ISCRL_IRQ0SCBm	0x02 
#define   ISCRL_IRQ1SCAm	0x04 
#define   ISCRL_IRQ1SCBm	0x08 
#define   ISCRL_IRQ2SCAm	0x10 
#define   ISCRL_IRQ2SCBm	0x20 
#define   ISCRL_IRQ3SCAm	0x40 
#define   ISCRL_IRQ3SCBm	0x80 
#define INT_IER	__PORT8 0xFFFE14	/* IRQ Enable Register */ 
#define   IER_IRQ0Em	0x01 
#define   IER_IRQ1Em	0x02 
#define   IER_IRQ2Em	0x04 
#define   IER_IRQ3Em	0x08 
#define   IER_IRQ4Em	0x10 
#define   IER_IRQ5Em	0x20 
#define   IER_IRQ6Em	0x40 
#define   IER_IRQ7Em	0x80 
#define INT_ISR	__PORT8 0xFFFE15	/* IRQ Status Register */ 
#define   ISR_IRQ0Fm	0x01 
#define   ISR_IRQ1Fm	0x02 
#define   ISR_IRQ2Fm	0x04 
#define   ISR_IRQ3Fm	0x08 
#define   ISR_IRQ4Fm	0x10 
#define   ISR_IRQ5Fm	0x20 
#define   ISR_IRQ6Fm	0x40 
#define   ISR_IRQ7Fm	0x80 
#define DTC_DTCER	__PORT8 0xFFFE16	/* DTC Enable Register */ 
#define   DTCERA_DTCEA0m	0x01 
#define   DTCERA_DTCEA1m	0x02 
#define   DTCERA_DTCEA2m	0x04 
#define   DTCERA_DTCEA3m	0x08 
#define   DTCERA_DTCEA4m	0x10 
#define   DTCERA_DTCEA5m	0x20 
#define   DTCERA_DTCEA6m	0x40 
#define   DTCERA_DTCEA7m	0x80 
#define   DTCERB_DTCEB0m	0x01 
#define   DTCERB_DTCEB1m	0x02 
#define   DTCERB_DTCEB2m	0x04 
#define   DTCERB_DTCEB3m	0x08 
#define   DTCERB_DTCEB4m	0x10 
#define   DTCERB_DTCEB5m	0x20 
#define   DTCERB_DTCEB6m	0x40 
#define   DTCERB_DTCEB7m	0x80 
#define   DTCERC_DTCEC0m	0x01 
#define   DTCERC_DTCEC1m	0x02 
#define   DTCERC_DTCEC2m	0x04 
#define   DTCERC_DTCEC3m	0x08 
#define   DTCERC_DTCEC4m	0x10 
#define   DTCERC_DTCEC5m	0x20 
#define   DTCERC_DTCEC6m	0x40 
#define   DTCERC_DTCEC7m	0x80 
#define   DTCERD_DTCED0m	0x01 
#define   DTCERD_DTCED1m	0x02 
#define   DTCERD_DTCED2m	0x04 
#define   DTCERD_DTCED3m	0x08 
#define   DTCERD_DTCED4m	0x10 
#define   DTCERD_DTCED5m	0x20 
#define   DTCERD_DTCED6m	0x40 
#define   DTCERD_DTCED7m	0x80 
#define   DTCERE_DTCEE0m	0x01 
#define   DTCERE_DTCEE1m	0x02 
#define   DTCERE_DTCEE2m	0x04 
#define   DTCERE_DTCEE3m	0x08 
#define   DTCERE_DTCEE4m	0x10 
#define   DTCERE_DTCEE5m	0x20 
#define   DTCERE_DTCEE6m	0x40 
#define   DTCERE_DTCEE7m	0x80 
#define   DTCERF_DTCEF0m	0x01 
#define   DTCERF_DTCEF1m	0x02 
#define   DTCERF_DTCEF2m	0x04 
#define   DTCERF_DTCEF3m	0x08 
#define   DTCERF_DTCEF4m	0x10 
#define   DTCERF_DTCEF5m	0x20 
#define   DTCERF_DTCEF6m	0x40 
#define   DTCERF_DTCEF7m	0x80 
#define   DTCERI_DTCEI0m	0x01 
#define   DTCERI_DTCEI1m	0x02 
#define   DTCERI_DTCEI2m	0x04 
#define   DTCERI_DTCEI3m	0x08 
#define   DTCERI_DTCEI4m	0x10 
#define   DTCERI_DTCEI5m	0x20 
#define   DTCERI_DTCEI6m	0x40 
#define   DTCERI_DTCEI7m	0x80 
#define DTC_DTVECR	__PORT8 0xFFFE1F	/* DTC Vector Register */ 
#define   DTVECR_DTVEC0m	0x01 
#define   DTVECR_DTVEC1m	0x02 
#define   DTVECR_DTVEC2m	0x04 
#define   DTVECR_DTVEC3m	0x08 
#define   DTVECR_DTVEC4m	0x10 
#define   DTVECR_DTVEC5m	0x20 
#define   DTVECR_DTVEC6m	0x40 
#define   DTVECR_SWDTEm	0x80 
#define PPG_PCR	__PORT8 0xFFFE26	/* PPG Output Control Register */ 
#define   PCR_G0CMS0m	0x01 
#define   PCR_G0CMS1m	0x02 
#define   PCR_G1CMS0m	0x04 
#define   PCR_G1CMS1m	0x08 
#define   PCR_G2CMS0m	0x10 
#define   PCR_G2CMS1m	0x20 
#define   PCR_G3CMS0m	0x40 
#define   PCR_G3CMS1m	0x80 
#define PPG_PMR	__PORT8 0xFFFE27	/* PPG Output Mode Register */ 
#define   PMR_G0NOVm	0x01 
#define   PMR_G1NOVm	0x02 
#define   PMR_G2NOVm	0x04 
#define   PMR_G3NOVm	0x08 
#define   PMR_G0INVm	0x10 
#define   PMR_G1INVm	0x20 
#define   PMR_G2INVm	0x40 
#define   PMR_G3INVm	0x80 
#define PPG_NDERH	__PORT8 0xFFFE28	/* Next Data Enable Register H */ 
#define   NDERH_NDER8m	0x01 
#define   NDERH_NDER9m	0x02 
#define   NDERH_NDER10m	0x04 
#define   NDERH_NDER11m	0x08 
#define   NDERH_NDER12m	0x10 
#define   NDERH_NDER13m	0x20 
#define   NDERH_NDER14m	0x40 
#define   NDERH_NDER15m	0x80 
#define PPG_NDERL	__PORT8 0xFFFE29	/* Next Data Enable Register L */ 
#define   NDERL_NDER0m	0x01 
#define   NDERL_NDER1m	0x02 
#define   NDERL_NDER2m	0x04 
#define   NDERL_NDER3m	0x08 
#define   NDERL_NDER4m	0x10 
#define   NDERL_NDER5m	0x20 
#define   NDERL_NDER6m	0x40 
#define   NDERL_NDER7m	0x80 
#define PPG_PODRH	__PORT8 0xFFFE2A	/* Output Data Register H */ 
#define   PODRH_POD8m	0x01 
#define   PODRH_POD9m	0x02 
#define   PODRH_POD10m	0x04 
#define   PODRH_POD11m	0x08 
#define   PODRH_POD12m	0x10 
#define   PODRH_POD13m	0x20 
#define   PODRH_POD14m	0x40 
#define   PODRH_POD15m	0x80 
#define PPG_PODRL	__PORT8 0xFFFE2B	/* Output Data Register L */ 
#define   PODRL_POD0m	0x01 
#define   PODRL_POD1m	0x02 
#define   PODRL_POD2m	0x04 
#define   PODRL_POD3m	0x08 
#define   PODRL_POD4m	0x10 
#define   PODRL_POD5m	0x20 
#define   PODRL_POD6m	0x40 
#define   PODRL_POD7m	0x80 
#define   NDRH_NDR8m	0x01 
#define   NDRH_NDR9m	0x02 
#define   NDRH_NDR10m	0x04 
#define   NDRH_NDR11m	0x08 
#define   NDRH_NDR12m	0x10 
#define   NDRH_NDR13m	0x20 
#define   NDRH_NDR14m	0x40 
#define   NDRH_NDR15m	0x80 
#define   NDRL_NDR0m	0x01 
#define   NDRL_NDR1m	0x02 
#define   NDRL_NDR2m	0x04 
#define   NDRL_NDR3m	0x08 
#define   NDRL_NDR4m	0x10 
#define   NDRL_NDR5m	0x20 
#define   NDRL_NDR6m	0x40 
#define   NDRL_NDR7m	0x80 
#define PPG_NDRH	__PORT8 0xFFFE2E	/* Next Data Register H                               H'FE2C, */ 
#define   NDRH_NDR8m	0x01 
#define   NDRH_NDR9m	0x02 
#define   NDRH_NDR10m	0x04 
#define   NDRH_NDR11m	0x08 
#define PPG_NDRL	__PORT8 0xFFFE2F	/* Next Data Register L                               H'FE2D, */ 
#define   NDRL_NDR0m	0x01 
#define   NDRL_NDR1m	0x02 
#define   NDRL_NDR2m	0x04 
#define   NDRL_NDR3m	0x08 
#define DIO_P1DDR	__PORT8 0xFFFE30	/* DIO 1 Data Direction Register */ 
#define   P1DDR_P10DDRm	0x01 
#define   P1DDR_P11DDRm	0x02 
#define   P1DDR_P12DDRm	0x04 
#define   P1DDR_P13DDRm	0x08 
#define   P1DDR_P14DDRm	0x10 
#define   P1DDR_P15DDRm	0x20 
#define   P1DDR_P16DDRm	0x40 
#define   P1DDR_P17DDRm	0x80 
#define DIO_P3DDR	__PORT8 0xFFFE32	/* DIO 3 Data Direction Register */ 
#define   P3DDR_P30DDRm	0x01 
#define   P3DDR_P31DDRm	0x02 
#define   P3DDR_P32DDRm	0x04 
#define   P3DDR_P33DDRm	0x08 
#define   P3DDR_P34DDRm	0x10 
#define   P3DDR_P35DDRm	0x20 
#define   P3DDR_P36DDRm	0x40 
#define   P3DDR_P37DDRm	0x80 
#define DIO_P7DDR	__PORT8 0xFFFE36	/* DIO 7 Data Direction Register */ 
#define   P7DDR_P70DDRm	0x01 
#define   P7DDR_P71DDRm	0x02 
#define   P7DDR_P72DDRm	0x04 
#define   P7DDR_P73DDRm	0x08 
#define   P7DDR_P74DDRm	0x10 
#define   P7DDR_P75DDRm	0x20 
#define   P7DDR_P76DDRm	0x40 
#define   P7DDR_P77DDRm	0x80 
#define DIO_PADDR	__PORT8 0xFFFE39	/* DIO A Data Direction Register */ 
#define   PADDR_PA0DDRm	0x01 
#define   PADDR_PA1DDRm	0x02 
#define   PADDR_PA2DDRm	0x04 
#define   PADDR_PA3DDRm	0x08 
#define DIO_PBDDR	__PORT8 0xFFFE3A	/* DIO B Data Direction Register */ 
#define   PBDDR_PB0DDRm	0x01 
#define   PBDDR_PB1DDRm	0x02 
#define   PBDDR_PB2DDRm	0x04 
#define   PBDDR_PB3DDRm	0x08 
#define   PBDDR_PB4DDRm	0x10 
#define   PBDDR_PB5DDRm	0x20 
#define   PBDDR_PB6DDRm	0x40 
#define   PBDDR_PB7DDRm	0x80 
#define DIO_PCDDR	__PORT8 0xFFFE3B	/* DIO C Data Direction Register */ 
#define   PCDDR_PC0DDRm	0x01 
#define   PCDDR_PC1DDRm	0x02 
#define   PCDDR_PC2DDRm	0x04 
#define   PCDDR_PC3DDRm	0x08 
#define   PCDDR_PC4DDRm	0x10 
#define   PCDDR_PC5DDRm	0x20 
#define   PCDDR_PC6DDRm	0x40 
#define   PCDDR_PC7DDRm	0x80 
#define DIO_PDDDR	__PORT8 0xFFFE3C	/* DIO D Data Direction Register */ 
#define   PDDDR_PD0DDRm	0x01 
#define   PDDDR_PD1DDRm	0x02 
#define   PDDDR_PD2DDRm	0x04 
#define   PDDDR_PD3DDRm	0x08 
#define   PDDDR_PD4DDRm	0x10 
#define   PDDDR_PD5DDRm	0x20 
#define   PDDDR_PD6DDRm	0x40 
#define   PDDDR_PD7DDRm	0x80 
#define DIO_PEDDR	__PORT8 0xFFFE3D	/* DIO E Data Direction Register */ 
#define   PEDDR_PE0DDRm	0x01 
#define   PEDDR_PE1DDRm	0x02 
#define   PEDDR_PE2DDRm	0x04 
#define   PEDDR_PE3DDRm	0x08 
#define   PEDDR_PE4DDRm	0x10 
#define   PEDDR_PE5DDRm	0x20 
#define   PEDDR_PE6DDRm	0x40 
#define   PEDDR_PE7DDRm	0x80 
#define DIO_PFDDR	__PORT8 0xFFFE3E	/* DIO F Data Direction Register */ 
#define   PFDDR_PF0DDRm	0x01 
#define   PFDDR_PF1DDRm	0x02 
#define   PFDDR_PF2DDRm	0x04 
#define   PFDDR_PF3DDRm	0x08 
#define   PFDDR_PF4DDRm	0x10 
#define   PFDDR_PF5DDRm	0x20 
#define   PFDDR_PF6DDRm	0x40 
#define   PFDDR_PF7DDRm	0x80 
#define DIO_PGDDR	__PORT8 0xFFFE3F	/* DIO G Data Direction Register */ 
#define   PGDDR_PG0DDRm	0x01 
#define   PGDDR_PG1DDRm	0x02 
#define   PGDDR_PG2DDRm	0x04 
#define   PGDDR_PG3DDRm	0x08 
#define   PGDDR_PG4DDRm	0x10 
#define DIO_PAPCR	__PORT8 0xFFFE40	/* DIO A Pull-Up MOS Control Register */ 
#define   PAPCR_PA0PCRm	0x01 
#define   PAPCR_PA1PCRm	0x02 
#define   PAPCR_PA2PCRm	0x04 
#define   PAPCR_PA3PCRm	0x08 
#define DIO_PBPCR	__PORT8 0xFFFE41	/* DIO B Pull-Up MOS Control Register */ 
#define   PBPCR_PB0PCRm	0x01 
#define   PBPCR_PB1PCRm	0x02 
#define   PBPCR_PB2PCRm	0x04 
#define   PBPCR_PB3PCRm	0x08 
#define   PBPCR_PB4PCRm	0x10 
#define   PBPCR_PB5PCRm	0x20 
#define   PBPCR_PB6PCRm	0x40 
#define   PBPCR_PB7PCRm	0x80 
#define DIO_PCPCR	__PORT8 0xFFFE42	/* DIO C Pull-Up MOS Control Register */ 
#define   PCPCR_PC0PCRm	0x01 
#define   PCPCR_PC1PCRm	0x02 
#define   PCPCR_PC2PCRm	0x04 
#define   PCPCR_PC3PCRm	0x08 
#define   PCPCR_PC4PCRm	0x10 
#define   PCPCR_PC5PCRm	0x20 
#define   PCPCR_PC6PCRm	0x40 
#define   PCPCR_PC7PCRm	0x80 
#define DIO_PDPCR	__PORT8 0xFFFE43	/* DIO D Pull-Up MOS Control Register */ 
#define   PDPCR_PD0PCRm	0x01 
#define   PDPCR_PD1PCRm	0x02 
#define   PDPCR_PD2PCRm	0x04 
#define   PDPCR_PD3PCRm	0x08 
#define   PDPCR_PD4PCRm	0x10 
#define   PDPCR_PD5PCRm	0x20 
#define   PDPCR_PD6PCRm	0x40 
#define   PDPCR_PD7PCRm	0x80 
#define DIO_PEPCR	__PORT8 0xFFFE44	/* DIO E Pull-Up MOS Control Register */ 
#define   PEPCR_PE0PCRm	0x01 
#define   PEPCR_PE1PCRm	0x02 
#define   PEPCR_PE2PCRm	0x04 
#define   PEPCR_PE3PCRm	0x08 
#define   PEPCR_PE4PCRm	0x10 
#define   PEPCR_PE5PCRm	0x20 
#define   PEPCR_PE6PCRm	0x40 
#define   PEPCR_PE7PCRm	0x80 
#define DIO_P3ODR	__PORT8 0xFFFE46	/* DIO 3 Open-Drain Control Register */ 
#define   P3ODR_P30ODRm	0x01 
#define   P3ODR_P31ODRm	0x02 
#define   P3ODR_P32ODRm	0x04 
#define   P3ODR_P33ODRm	0x08 
#define   P3ODR_P34ODRm	0x10 
#define   P3ODR_P35ODRm	0x20 
#define   P3ODR_P36ODRm	0x40 
#define   P3ODR_P37ODRm	0x80 
#define DIO_PAODR	__PORT8 0xFFFE47	/* DIO A Open Drain Control Register */ 
#define   PAODR_PA0ODRm	0x01 
#define   PAODR_PA1ODRm	0x02 
#define   PAODR_PA2ODRm	0x04 
#define   PAODR_PA3ODRm	0x08 
#define DIO_PBODR	__PORT8 0xFFFE48	/* DIO B Open Drain Control Register */ 
#define   PBODR_PB0ODRm	0x01 
#define   PBODR_PB1ODRm	0x02 
#define   PBODR_PB2ODRm	0x04 
#define   PBODR_PB3ODRm	0x08 
#define   PBODR_PB4ODRm	0x10 
#define   PBODR_PB5ODRm	0x20 
#define   PBODR_PB6ODRm	0x40 
#define   PBODR_PB7ODRm	0x80 
#define DIO_PCODR	__PORT8 0xFFFE49	/* DIO C Open Drain Control Register */ 
#define   PCODR_PC0ODRm	0x01 
#define   PCODR_PC1ODRm	0x02 
#define   PCODR_PC2ODRm	0x04 
#define   PCODR_PC3ODRm	0x08 
#define   PCODR_PC4ODRm	0x10 
#define   PCODR_PC5ODRm	0x20 
#define   PCODR_PC6ODRm	0x40 
#define   PCODR_PC7ODRm	0x80 

/* TPU part 1 definitions start */
#define TPU_TPCR3	__PORT8 0xFFFE80	/* Timer Control Register 3 */ 
#define   TPCR3_TPSC0m	0x01 
#define   TPCR3_TPSC1m	0x02 
#define   TPCR3_TPSC2m	0x04 
#define   TPCR3_CKEG0m	0x08 
#define   TPCR3_CKEG1m	0x10 
#define   TPCR3_CCLR0m	0x20 
#define   TPCR3_CCLR1m	0x40 
#define   TPCR3_CCLR2m	0x80 
#define TPU_TPMDR3	__PORT8 0xFFFE81	/* Timer Mode Register 3 */ 
#define   TPMDR3_MD0m	0x01 
#define   TPMDR3_MD1m	0x02 
#define   TPMDR3_MD2m	0x04 
#define   TPMDR3_MD3m	0x08 
#define   TPMDR3_BFAm	0x10 
#define   TPMDR3_BFBm	0x20 
#define TPU_TPIOR3H	__PORT8 0xFFFE82	/* Timer IO Control Register 3H */ 
#define   TPIOR3H_IOA0m	0x01 
#define   TPIOR3H_IOA1m	0x02 
#define   TPIOR3H_IOA2m	0x04 
#define   TPIOR3H_IOA3m	0x08 
#define   TPIOR3H_IOB0m	0x10 
#define   TPIOR3H_IOB1m	0x20 
#define   TPIOR3H_IOB2m	0x40 
#define   TPIOR3H_IOB3m	0x80 
#define TPU_TPIOR3L	__PORT8 0xFFFE83	/* Timer IO Control Register 3L */ 
#define   TPIOR3L_IOC0m	0x01 
#define   TPIOR3L_IOC1m	0x02 
#define   TPIOR3L_IOC2m	0x04 
#define   TPIOR3L_IOC3m	0x08 
#define   TPIOR3L_IOD0m	0x10 
#define   TPIOR3L_IOD1m	0x20 
#define   TPIOR3L_IOD2m	0x40 
#define   TPIOR3L_IOD3m	0x80 
#define TPU_TPIER3	__PORT8 0xFFFE84	/* Timer INT Enable Register 3 */ 
#define   TPIER3_TGIEAm	0x01 
#define   TPIER3_TGIEBm	0x02 
#define   TPIER3_TGIECm	0x04 
#define   TPIER3_TGIEDm	0x08 
#define   TPIER3_TCIEVm	0x10 
#define   TPIER3_TTGEm	0x80 
#define TPU_TPSR3	__PORT8 0xFFFE85	/* Timer Status Register 3 (RD/WC) */ 
#define   TPSR3_TGFAm	0x01 
#define   TPSR3_TGFBm	0x02 
#define   TPSR3_TGFCm	0x04 
#define   TPSR3_TGFDm	0x08 
#define   TPSR3_TCFVm	0x10 
#define TPU_TPCNT3	__PORT16 0xFFFE86	/* Timer Counter 3 */ 
#define TPU_TPGR3A	__PORT16 0xFFFE88	/* Timer General Register 3A */ 
#define TPU_TPGR3B	__PORT16 0xFFFE8A	/* Timer General Register 3B */ 
#define TPU_TPGR3C	__PORT16 0xFFFE8C	/* Timer General Register 3C */ 
#define TPU_TPGR3D	__PORT16 0xFFFE8E	/* Timer General Register 3D */ 
#define TPU_TPCR4	__PORT8 0xFFFE90	/* Timer Control Register 4 */ 
#define   TPCR4_TPSC0m	0x01 
#define   TPCR4_TPSC1m	0x02 
#define   TPCR4_TPSC2m	0x04 
#define   TPCR4_CKEG0m	0x08 
#define   TPCR4_CKEG1m	0x10 
#define   TPCR4_CCLR0m	0x20 
#define   TPCR4_CCLR1m	0x40 
#define TPU_TPMDR4	__PORT8 0xFFFE91	/* Timer Mode Register 4 */ 
#define   TPMDR4_MD0m	0x01 
#define   TPMDR4_MD1m	0x02 
#define   TPMDR4_MD2m	0x04 
#define   TPMDR4_MD3m	0x08 
#define TPU_TPIOR4	__PORT8 0xFFFE92	/* Timer IO Control Register 4 */ 
#define   TPIOR4_IOA0m	0x01 
#define   TPIOR4_IOA1m	0x02 
#define   TPIOR4_IOA2m	0x04 
#define   TPIOR4_IOA3m	0x08 
#define   TPIOR4_IOB0m	0x10 
#define   TPIOR4_IOB1m	0x20 
#define   TPIOR4_IOB2m	0x40 
#define   TPIOR4_IOB3m	0x80 
#define TPU_TPIER4	__PORT8 0xFFFE94	/* Timer INT Enable Register 4 */ 
#define   TPIER4_TGIEAm	0x01 
#define   TPIER4_TGIEBm	0x02 
#define   TPIER4_TCIEVm	0x10 
#define   TPIER4_TCIEUm	0x20 
#define   TPIER4_TTGEm	0x80 
#define TPU_TPSR4	__PORT8 0xFFFE95	/* Timer Status Register 4 (RD/WC) */ 
#define   TPSR4_TGFAm	0x01 
#define   TPSR4_TGFBm	0x02 
#define   TPSR4_TCFVm	0x10 
#define   TPSR4_TCFUm	0x20 
#define   TPSR4_TCFDm	0x80 
#define TPU_TPCNT4	__PORT16 0xFFFE96	/* Timer Counter 4 */ 
#define TPU_TPGR4A	__PORT16 0xFFFE98	/* Timer General Register 4A */ 
#define TPU_TPGR4B	__PORT16 0xFFFE9A	/* Timer General Register 4B */ 
#define TPU_TPCR5	__PORT8 0xFFFEA0	/* Timer Control Register 5 */ 
#define   TPCR5_TPSC0m	0x01 
#define   TPCR5_TPSC1m	0x02 
#define   TPCR5_TPSC2m	0x04 
#define   TPCR5_CKEG0m	0x08 
#define   TPCR5_CKEG1m	0x10 
#define   TPCR5_CCLR0m	0x20 
#define   TPCR5_CCLR1m	0x40 
#define TPU_TPMDR5	__PORT8 0xFFFEA1	/* Timer Mode Register 5 */ 
#define   TPMDR5_MD0m	0x01 
#define   TPMDR5_MD1m	0x02 
#define   TPMDR5_MD2m	0x04 
#define   TPMDR5_MD3m	0x08 
#define TPU_TPIOR5	__PORT8 0xFFFEA2	/* Timer IO Control Register 5 */ 
#define   TPIOR5_IOA0m	0x01 
#define   TPIOR5_IOA1m	0x02 
#define   TPIOR5_IOA2m	0x04 
#define   TPIOR5_IOA3m	0x08 
#define   TPIOR5_IOB0m	0x10 
#define   TPIOR5_IOB1m	0x20 
#define   TPIOR5_IOB2m	0x40 
#define   TPIOR5_IOB3m	0x80 
#define TPU_TPIER5	__PORT8 0xFFFEA4	/* Timer INT Enable Register 5 */ 
#define   TPIER5_TGIEAm	0x01 
#define   TPIER5_TGIEBm	0x02 
#define   TPIER5_TCIEVm	0x10 
#define   TPIER5_TCIEUm	0x20 
#define   TPIER5_TTGEm	0x80 
#define TPU_TPSR5	__PORT8 0xFFFEA5	/* Timer Status Register 5 (RD/WC) */ 
#define   TPSR5_TGFAm	0x01 
#define   TPSR5_TGFBm	0x02 
#define   TPSR5_TCFVm	0x10 
#define   TPSR5_TCFUm	0x20 
#define   TPSR5_TCFDm	0x80 
#define TPU_TPCNT5	__PORT16 0xFFFEA6	/* Timer Counter 5 */ 
#define TPU_TPGR5A	__PORT16 0xFFFEA8	/* Timer General Register 5A */ 
#define TPU_TPGR5B	__PORT16 0xFFFEAA	/* Timer General Register 5B */ 
#define TPU_TPSTR	__PORT8 0xFFFEB0	/* Timer Start Register */ 
#define   TPSTR_CST0m	0x01 
#define   TPSTR_CST1m	0x02 
#define   TPSTR_CST2m	0x04 
#define   TPSTR_CST3m	0x08 
#define   TPSTR_CST4m	0x10 
#define   TPSTR_CST5m	0x20 
#define TPU_TPSYR	__PORT8 0xFFFEB1	/* Timer Synchro Register */ 
#define   TPSYR_SYNC0m	0x01 
#define   TPSYR_SYNC1m	0x02 
#define   TPSYR_SYNC2m	0x04 
#define   TPSYR_SYNC3m	0x08 
#define   TPSYR_SYNC4m	0x10 
#define   TPSYR_SYNC5m	0x20 
/* TPU part 1 definitions end */

#define INT_IPRA	__PORT8 0xFFFEC0	/* INT Priority Register A */ 
#define   IPRA_IRQ0m	0x70 
#define   IPRA_IRQ1m	0x07 
#define INT_IPRB	__PORT8 0xFFFEC1	/* INT Priority Register B */ 
#define   IPRB_IRQ23m	0x70 
#define   IPRB_IRQ45m	0x07 
#define INT_IPRC	__PORT8 0xFFFEC2	/* INT Priority Register C */ 
#define   IPRC_IRQ67m	0x70 
#define   IPRC_DTCm	0x07
#define INT_IPRD	__PORT8 0xFFFEC3	/* INT Priority Register D */ 
#define   IPRD_WDG0m	0x70 
#define   IPRD_REFRm	0x07
#define INT_IPRE	__PORT8 0xFFFEC4	/* INT Priority Register E */ 
#define   IPRE_PCBREAKm	0x70 
#define   IPRE_ADWDG1m	0x07
#define INT_IPRF	__PORT8 0xFFFEC5	/* INT Priority Register F */ 
#define   IPRF_TPU0m	0x70 
#define   IPRF_TPU1m	0x07
#define INT_IPRG	__PORT8 0xFFFEC6	/* INT Priority Register G */ 
#define   IPRG_TPU2m	0x70 
#define   IPRG_TPU3m	0x07
#define INT_IPRH	__PORT8 0xFFFEC7	/* INT Priority Register H */ 
#define   IPRH_TPU4m	0x70 
#define   IPRH_TPU5m	0x07
#define INT_IPRI	__PORT8 0xFFFEC8	/* INT Priority Register I */ 
#define   IPRI_TMR0m	0x70 
#define   IPRI_TMR1m	0x07
#define INT_IPRJ	__PORT8 0xFFFEC9	/* INT Priority Register J */ 
#define   IPRJ_DMACm	0x70 
#define   IPRJ_SCI0m	0x07
#define INT_IPRK	__PORT8 0xFFFECA	/* INT Priority Register K */ 
#define   IPRK_SCI1m	0x70 
#define   IPRK_SCI2m	0x07
#define INT_IPRL	__PORT8 0xFFFECB	/* INT Priority Register L */ 
#define   IPRL_TMR23m	0x70 
#define   IPRL_IICm	0x07
#define INT_IPRO	__PORT8 0xFFFECE	/* INT Priority Register O */ 
#define   IPRO_SCI3m	0x70 
#define   IPRO_SCI4m	0x07
#define BUS_ABWCR	__PORT8 0xFFFED0	/* Bus Width Control Register */ 
#define   ABWCR_ABW0m	0x01 
#define   ABWCR_ABW1m	0x02 
#define   ABWCR_ABW2m	0x04 
#define   ABWCR_ABW3m	0x08 
#define   ABWCR_ABW4m	0x10 
#define   ABWCR_ABW5m	0x20 
#define   ABWCR_ABW6m	0x40 
#define   ABWCR_ABW7m	0x80 
#define BUS_ASTCR	__PORT8 0xFFFED1	/* Access State Control Register */ 
#define   ASTCR_AST0m	0x01 
#define   ASTCR_AST1m	0x02 
#define   ASTCR_AST2m	0x04 
#define   ASTCR_AST3m	0x08 
#define   ASTCR_AST4m	0x10 
#define   ASTCR_AST5m	0x20 
#define   ASTCR_AST6m	0x40 
#define   ASTCR_AST7m	0x80 
#define BUS_WCRH	__PORT8 0xFFFED2	/* Wait Control Register H */ 
#define   WCRH_W40m	0x01 
#define   WCRH_W41m	0x02 
#define   WCRH_W50m	0x04 
#define   WCRH_W51m	0x08 
#define   WCRH_W60m	0x10 
#define   WCRH_W61m	0x20 
#define   WCRH_W70m	0x40 
#define   WCRH_W71m	0x80 
#define BUS_WCRL	__PORT8 0xFFFED3	/* Wait Control Register */ 
#define   WCRL_W00m	0x01 
#define   WCRL_W01m	0x02 
#define   WCRL_W10m	0x04 
#define   WCRL_W11m	0x08 
#define   WCRL_W20m	0x10 
#define   WCRL_W21m	0x20 
#define   WCRL_W30m	0x40 
#define   WCRL_W31m	0x80 
#define BUS_BCRH	__PORT8 0xFFFED4	/* Bus Control Register H */ 
#define   BCRH_RMST0m	0x01 
#define   BCRH_RMTS1m	0x02 
#define   BCRH_RMTS2m	0x04 
#define   BCRH_BRSTS0m	0x08 
#define   BCRH_BRSTS1m	0x10 
#define   BCRH_BRSTRMm	0x20 
#define   BCRH_ICIS0m	0x40 
#define   BCRH_ICIS1m	0x80 
#define BUS_BCRL	__PORT8 0xFFFED5	/* Bus Control Register L */ 
#define   BCRL_WAITEm	0x01 
#define   BCRL_WDBEm	0x02 
#define   BCRL_RCTSm	0x04 
#define   BCRL_DDSm	0x08 
#define   BCRL_OESm	0x10 
#define   BCRL_BREQOEm	0x40 
#define   BCRL_BRLEm	0x80 
#define BUS_MCR	__PORT8 0xFFFED6	/* Memory Control Register */ 
#define   MCR_RLW0m	0x01 
#define   MCR_RLW1m	0x02 
#define   MCR_MXC0m	0x04 
#define   MCR_MXC1m	0x08 
#define   MCR_CW2m	0x10 
#define   MCR_RCDMm	0x20 
#define   MCR_BEm	0x40 
#define   MCR_TPCm	0x80 
#define BUS_DRAMCR	__PORT8 0xFFFED7	/* DRAM Control Register */ 
#define   DRAMCR_CKS0m	0x01 
#define   DRAMCR_CKS1m	0x02 
#define   DRAMCR_CKS2m	0x04 
#define   DRAMCR_CMIEm	0x08 
#define   DRAMCR_CMFm	0x10 
#define   DRAMCR_RMODEm	0x20 
#define   DRAMCR_CBRMm	0x40 
#define   DRAMCR_RFSHEm	0x80 
#define BUS_RTCNT	__PORT8 0xFFFED8	/* Refresh Timer Counter */ 
#define BUS_RTCOR	__PORT8 0xFFFED9	/* Refresh Time Constant Register */ 
#define FLM_RAMER	__PORT8 0xFFFEDB	/* RAM Emulation Register */ 
#define   RAMER_RAM0m	0x01			/*   base address in 4kB step */ 
#define   RAMER_RAM1m	0x02 
#define   RAMER_RAM2m	0x04 
#define   RAMER_RAMxm	0x07 
#define   RAMER_RAMSm	0x08			/*   1 = remap RAM FFD000-FFDFFF */
#define DMAC_MAR0AH	__PORT16 0xFFFEE0	/* Memory Address Register 0AH */ 
#define DMAC_MAR0AL	__PORT16 0xFFFEE2	/* Memory Address Register 0AL */ 
#define DMAC_IOAR0A	__PORT16 0xFFFEE4	/* IO Address Register 0A */ 
#define DMAC_ETCR0A	__PORT16 0xFFFEE6	/* Transfer Count Register 0A */ 
#define DMAC_MAR0BH	__PORT16 0xFFFEE8	/* Memory Address Register 0BH */ 
#define DMAC_MAR0BL	__PORT16 0xFFFEEA	/* Memory Address Register 0BL */ 
#define DMAC_IOAR0B	__PORT16 0xFFFEEC	/* IO Address Register 0B */ 
#define DMAC_ETCR0B	__PORT16 0xFFFEEE	/* Transfer Count Register 0B */ 
#define DMAC_MAR1AH	__PORT8 0xFFFEF0	/* Memory Address Register 1AH */ 
#define DMAC_MAR1AL	__PORT8 0xFFFEF2	/* Memory Address Register 1AL */ 
#define DMAC_IOAR1A	__PORT8 0xFFFEF4	/* IO Address Register 1A */ 
#define DMAC_ETCR1A	__PORT8 0xFFFEF6	/* Transfer Count Register 1A */ 
#define DMAC_MAR1BH	__PORT8 0xFFFEF8	/* Memory Address Register 1BH */ 
#define DMAC_MAR1BL	__PORT8 0xFFFEFA	/* Memory Address Register 1BL */ 
#define DMAC_IOAR1B	__PORT8 0xFFFEFC	/* IO Address Register 1B */ 
#define DMAC_ETCR1B	__PORT8 0xFFFEFE	/* Transfer Count Register 1B */ 
#define DIO_P1DR	__PORT8 0xFFFF00	/* DIO 1 Data Register */ 
#define   P1DR_P10DRm	0x01 
#define   P1DR_P11DRm	0x02 
#define   P1DR_P12DRm	0x04 
#define   P1DR_P13DRm	0x08 
#define   P1DR_P14DRm	0x10 
#define   P1DR_P15DRm	0x20 
#define   P1DR_P16DRm	0x40 
#define   P1DR_P17DRm	0x80 
#define DIO_P3DR	__PORT8 0xFFFF02	/* DIO 3 Data Register */ 
#define   P3DR_P30DRm	0x01 
#define   P3DR_P31DRm	0x02 
#define   P3DR_P32DRm	0x04 
#define   P3DR_P33DRm	0x08 
#define   P3DR_P34DRm	0x10 
#define   P3DR_P35DRm	0x20 
#define   P3DR_P36DRm	0x40 
#define   P3DR_P37DRm	0x80 
#define DIO_P7DR	__PORT8 0xFFFF06	/* DIO 7 Data Register */ 
#define   P7DR_P70DRm	0x01 
#define   P7DR_P71DRm	0x02 
#define   P7DR_P72DRm	0x04 
#define   P7DR_P73DRm	0x08 
#define   P7DR_P74DRm	0x10 
#define   P7DR_P75DRm	0x20 
#define   P7DR_P76DRm	0x40 
#define   P7DR_P77DRm	0x80 
#define DIO_PADR	__PORT8 0xFFFF09	/* DIO A Data Register */ 
#define   PADR_PA0DRm	0x01 
#define   PADR_PA1DRm	0x02 
#define   PADR_PA2DRm	0x04 
#define   PADR_PA3DRm	0x08 
#define DIO_PBDR	__PORT8 0xFFFF0A	/* DIO B Data Register */ 
#define   PBDR_PB0DRm	0x01 
#define   PBDR_PB1DRm	0x02 
#define   PBDR_PB2DRm	0x04 
#define   PBDR_PB3DRm	0x08 
#define   PBDR_PB4DRm	0x10 
#define   PBDR_PB5DRm	0x20 
#define   PBDR_PB6DRm	0x40 
#define   PBDR_PB7DRm	0x80 
#define DIO_PCDR	__PORT8 0xFFFF0B	/* DIO C Data Register */ 
#define   PCDR_PC0DRm	0x01 
#define   PCDR_PC1DRm	0x02 
#define   PCDR_PC2DRm	0x04 
#define   PCDR_PC3DRm	0x08 
#define   PCDR_PC4DRm	0x10 
#define   PCDR_PC5DRm	0x20 
#define   PCDR_PC6DRm	0x40 
#define   PCDR_PC7DRm	0x80 
#define DIO_PDDR	__PORT8 0xFFFF0C	/* DIO D Data Register */ 
#define   PDDR_PD0DRm	0x01 
#define   PDDR_PD1DRm	0x02 
#define   PDDR_PD2DRm	0x04 
#define   PDDR_PD3DRm	0x08 
#define   PDDR_PD4DRm	0x10 
#define   PDDR_PD5DRm	0x20 
#define   PDDR_PD6DRm	0x40 
#define   PDDR_PD7DRm	0x80 
#define DIO_PEDR	__PORT8 0xFFFF0D	/* DIO E Data Register */ 
#define   PEDR_PE0DRm	0x01 
#define   PEDR_PE1DRm	0x02 
#define   PEDR_PE2DRm	0x04 
#define   PEDR_PE3DRm	0x08 
#define   PEDR_PE4DRm	0x10 
#define   PEDR_PE5DRm	0x20 
#define   PEDR_PE6DRm	0x40 
#define   PEDR_PE7DRm	0x80 
#define DIO_PFDR	__PORT8 0xFFFF0E	/* DIO F Data Register */ 
#define   PFDR_PF0DRm	0x01 
#define   PFDR_PF1DRm	0x02 
#define   PFDR_PF2DRm	0x04 
#define   PFDR_PF3DRm	0x08 
#define   PFDR_PF4DRm	0x10 
#define   PFDR_PF5DRm	0x20 
#define   PFDR_PF6DRm	0x40 
#define   PFDR_PF7DRm	0x80 
#define DIO_PGDR	__PORT8 0xFFFF0F	/* DIO G Data Register */ 
#define   PGDR_PG0DRm	0x01 
#define   PGDR_PG1DRm	0x02 
#define   PGDR_PG2DRm	0x04 
#define   PGDR_PG3DRm	0x08 
#define   PGDR_PG4DRm	0x10 

/* TPU part 2 definitions start */
#define TPU_TPCR0	__PORT8 0xFFFF10	/* Timer Control Register 0 */ 
#define   TPCR0_TPSC0m	0x01 
#define   TPCR0_TPSC1m	0x02 
#define   TPCR0_TPSC2m	0x04 
#define   TPCR0_CKEG0m	0x08 
#define   TPCR0_CKEG1m	0x10 
#define   TPCR0_CCLR0m	0x20 
#define   TPCR0_CCLR1m	0x40 
#define   TPCR0_CCLR2m	0x80 
#define TPU_TPMDR0	__PORT8 0xFFFF11	/* Timer Mode Register 0 */ 
#define   TPMDR0_MD0m	0x01 
#define   TPMDR0_MD1m	0x02 
#define   TPMDR0_MD2m	0x04 
#define   TPMDR0_MD3m	0x08 
#define   TPMDR0_BFAm	0x10 
#define   TPMDR0_BFBm	0x20 
#define TPU_TPIOR0H	__PORT8 0xFFFF12	/* Timer IO Control Register 0H */ 
#define   TPIOR0H_IOA0m	0x01 
#define   TPIOR0H_IOA1m	0x02 
#define   TPIOR0H_IOA2m	0x04 
#define   TPIOR0H_IOA3m	0x08 
#define   TPIOR0H_IOB0m	0x10 
#define   TPIOR0H_IOB1m	0x20 
#define   TPIOR0H_IOB2m	0x40 
#define   TPIOR0H_IOB3m	0x80 
#define TPU_TPIOR0L	__PORT8 0xFFFF13	/* Timer IO Control Register 0L */ 
#define   TPIOR0L_IOC0m	0x01 
#define   TPIOR0L_IOC1m	0x02 
#define   TPIOR0L_IOC2m	0x04 
#define   TPIOR0L_IOC3m	0x08 
#define   TPIOR0L_IOD0m	0x10 
#define   TPIOR0L_IOD1m	0x20 
#define   TPIOR0L_IOD2m	0x40 
#define   TPIOR0L_IOD3m	0x80 
#define TPU_TPIER0	__PORT8 0xFFFF14	/* Timer INT Enable Register 0 */ 
#define   TPIER0_TGIEAm	0x01 
#define   TPIER0_TGIEBm	0x02 
#define   TPIER0_TGIECm	0x04 
#define   TPIER0_TGIEDm	0x08 
#define   TPIER0_TCIEVm	0x10 
#define   TPIER0_TTGEm	0x80 
#define TPU_TPSR0	__PORT8 0xFFFF15	/* Timer Status Register 0 (RD/WC) */ 
#define   TPSR0_TGFAm	0x01 
#define   TPSR0_TGFBm	0x02 
#define   TPSR0_TGFCm	0x04 
#define   TPSR0_TGFDm	0x08 
#define   TPSR0_TCFVm	0x10 
#define TPU_TPCNT0	__PORT16 0xFFFF16	/* Timer Counter 0 */ 
#define TPU_TPGR0A	__PORT16 0xFFFF18	/* Timer General Register 0A */ 
#define TPU_TPGR0B	__PORT16 0xFFFF1A	/* Timer General Register 0B */ 
#define TPU_TPGR0C	__PORT16 0xFFFF1C	/* Timer General Register 0C */ 
#define TPU_TPGR0D	__PORT16 0xFFFF1E	/* Timer General Register 0D */ 
#define TPU_TPCR1	__PORT8 0xFFFF20	/* Timer Control Register 1 */ 
#define   TPCR1_TPSC0m	0x01 
#define   TPCR1_TPSC1m	0x02 
#define   TPCR1_TPSC2m	0x04 
#define   TPCR1_CKEG0m	0x08 
#define   TPCR1_CKEG1m	0x10 
#define   TPCR1_CCLR0m	0x20 
#define   TPCR1_CCLR1m	0x40 
#define TPU_TPMDR1	__PORT8 0xFFFF21	/* Timer Mode Register 1 */ 
#define   TPMDR1_MD0m	0x01 
#define   TPMDR1_MD1m	0x02 
#define   TPMDR1_MD2m	0x04 
#define   TPMDR1_MD3m	0x08 
#define TPU_TPIOR1	__PORT8 0xFFFF22	/* Timer IO Control Register 1 */ 
#define   TPIOR1_IOA0m	0x01 
#define   TPIOR1_IOA1m	0x02 
#define   TPIOR1_IOA2m	0x04 
#define   TPIOR1_IOA3m	0x08 
#define   TPIOR1_IOB0m	0x10 
#define   TPIOR1_IOB1m	0x20 
#define   TPIOR1_IOB2m	0x40 
#define   TPIOR1_IOB3m	0x80 
#define TPU_TPIER1	__PORT8 0xFFFF24	/* Timer INT Enable Register 1 */ 
#define   TPIER1_TGIEAm	0x01 
#define   TPIER1_TGIEBm	0x02 
#define   TPIER1_TCIEVm	0x10 
#define   TPIER1_TCIEUm	0x20 
#define   TPIER1_TTGEm	0x80 
#define TPU_TPSR1	__PORT8 0xFFFF25	/* Timer Status Register 1 (RD/WC) */ 
#define   TPSR1_TGFAm	0x01 
#define   TPSR1_TGFBm	0x02 
#define   TPSR1_TCFVm	0x10 
#define   TPSR1_TCFUm	0x20 
#define   TPSR1_TCFDm	0x80 
#define TPU_TPCNT1	__PORT16 0xFFFF26	/* Timer Counter 1 */ 
#define TPU_TPGR1A	__PORT16 0xFFFF28	/* Timer General Register 1A */ 
#define TPU_TPGR1B	__PORT16 0xFFFF2A	/* Timer General Register 1B */ 
#define TPU_TPCR2	__PORT8 0xFFFF30	/* Timer Control Register 2 */ 
#define   TPCR2_TPSC0m	0x01 
#define   TPCR2_TPSC1m	0x02 
#define   TPCR2_TPSC2m	0x04 
#define   TPCR2_CKEG0m	0x08 
#define   TPCR2_CKEG1m	0x10 
#define   TPCR2_CCLR0m	0x20 
#define   TPCR2_CCLR1m	0x40 
#define TPU_TPMDR2	__PORT8 0xFFFF31	/* Timer Mode Register 2 */ 
#define   TPMDR2_MD0m	0x01 
#define   TPMDR2_MD1m	0x02 
#define   TPMDR2_MD2m	0x04 
#define   TPMDR2_MD3m	0x08 
#define TPU_TPIOR2	__PORT8 0xFFFF32	/* Timer IO Control Register 2 */ 
#define   TPIOR2_IOA0m	0x01 
#define   TPIOR2_IOA1m	0x02 
#define   TPIOR2_IOA2m	0x04 
#define   TPIOR2_IOA3m	0x08 
#define   TPIOR2_IOB0m	0x10 
#define   TPIOR2_IOB1m	0x20 
#define   TPIOR2_IOB2m	0x40 
#define   TPIOR2_IOB3m	0x80 
#define TPU_TPIER2	__PORT8 0xFFFF34	/* Timer INT Enable Register 2 */ 
#define   TPIER2_TGIEAm	0x01 
#define   TPIER2_TGIEBm	0x02 
#define   TPIER2_TCIEVm	0x10 
#define   TPIER2_TCIEUm	0x20 
#define   TPIER2_TTGEm	0x80 
#define TPU_TPSR2	__PORT8 0xFFFF35	/* Timer Status Register 2 */ 
#define   TPSR2_TGFAm	0x01 
#define   TPSR2_TGFBm	0x02 
#define   TPSR2_TCFVm	0x10 
#define   TPSR2_TCFUm	0x20 
#define   TPSR2_TCFDm	0x80 
#define TPU_TPCNT2	__PORT16 0xFFFF36	/* Timer Counter 2 */ 
#define TPU_TPGR2A	__PORT16 0xFFFF38	/* Timer General Register 2A */ 
#define TPU_TPGR2B	__PORT16 0xFFFF3A	/* Timer General Register 2B */ 
/* TPU part 2 definitions end */

#define DMAC_DMAWER	__PORT8 0xFFFF60	/* DMA Write Enable Register */ 
#define   DMAWER_WE0Am	0x01 
#define   DMAWER_WE0Bm	0x02 
#define   DMAWER_WE1Am	0x04 
#define   DMAWER_WE1Bm	0x08 
#define DMAC_DMATCR	__PORT8 0xFFFF61	/* DMA Terminal Control Register */ 
#define   DMATCR_TEE0m	0x10 
#define   DMATCR_TEE1m	0x20 
#define DMAC_DMACR0A	__PORT8 0xFFFF62	/* DMA Control Register 0A */ 
#define   DMACR0A_DTF0m	0x01 
#define   DMACR0A_DTF1m	0x02 
#define   DMACR0A_DTF2m	0x04 
#define   DMACR0A_DTF3m	0x08 
#define   DMACR0A_DTDIRm	0x10 
#define   DMACR0A_RPEm	0x20 
#define   DMACR0A_DTIDm	0x40 
#define   DMACR0A_DTSZm	0x80 
#define DMAC_DMACR0B	__PORT8 0xFFFF63	/* DMA Control Register 0B */ 
#define   DMACR0B_DTF0m	0x01 
#define   DMACR0B_DTF1m	0x02 
#define   DMACR0B_DTF2m	0x04 
#define   DMACR0B_DTF3m	0x08 
#define   DMACR0B_DTDIRm	0x10 
#define   DMACR0B_RPEm	0x20 
#define   DMACR0B_DTIDm	0x40 
#define   DMACR0B_DTSZm	0x80 
#define DMAC_DMACR1A	__PORT8 0xFFFF64	/* DMA Control Register 1A */ 
#define   DMACR1A_DTF0m	0x01 
#define   DMACR1A_DTF1m	0x02 
#define   DMACR1A_DTF2m	0x04 
#define   DMACR1A_DTF3m	0x08 
#define   DMACR1A_DTDIRm	0x10 
#define   DMACR1A_RPEm	0x20 
#define   DMACR1A_DTIDm	0x40 
#define   DMACR1A_DTSZm	0x80 
#define DMAC_DMACR1B	__PORT8 0xFFFF65	/* DMA Control Register 1B */ 
#define   DMACR1B_DTF0m	0x01 
#define   DMACR1B_DTF1m	0x02 
#define   DMACR1B_DTF2m	0x04 
#define   DMACR1B_DTF3m	0x08 
#define   DMACR1B_DTDIRm	0x10 
#define   DMACR1B_RPEm	0x20 
#define   DMACR1B_DTIDm	0x40 
#define   DMACR1B_DTSZm	0x80 
#define DMAC_DMABCR	__PORT8 0xFFFF66	/* DMA Band Control Register */ 
#define   DMABCRH_DTA0Am	0x01 
#define   DMABCRH_DTA0Bm	0x02 
#define   DMABCRH_DTA1Am	0x04 
#define   DMABCRH_DTA1Bm	0x08 
#define   DMABCRH_SAE0m	0x10 
#define   DMABCRH_SAE1m	0x20 
#define   DMABCRH_FAE0m	0x40 
#define   DMABCRH_FAE1m	0x80 
#define   DMABCRL_DTIE0Am	0x01 
#define   DMABCRL_DTIE0Bm	0x02 
#define   DMABCRL_DTIE1Am	0x04 
#define   DMABCRL_DTIE1Bm	0x08 
#define   DMABCRL_DTE0Am	0x10 
#define   DMABCRL_DTE0Bm	0x20 
#define   DMABCRL_DTE1Am	0x40 
#define   DMABCRL_DTE1Bm	0x80 
#define TMR_TCR0	__PORT8 0xFFFF68	/* Timer Control Register 0 */ 
#define   TCR0_CKS0m	0x01 
#define   TCR0_CKS1m	0x02 
#define   TCR0_CKS2m	0x04 
#define   TCR0_CCLR0m	0x08 
#define   TCR0_CCLR1m	0x10 
#define   TCR0_OVIEm	0x20 
#define   TCR0_CMIEAm	0x40 
#define   TCR0_CMIEBm	0x80 
#define TMR_TCR1	__PORT8 0xFFFF69	/* Timer Control Register 1 */ 
#define   TCR1_CKS0m	0x01 
#define   TCR1_CKS1m	0x02 
#define   TCR1_CKS2m	0x04 
#define   TCR1_CCLR0m	0x08 
#define   TCR1_CCLR1m	0x10 
#define   TCR1_OVIEm	0x20 
#define   TCR1_CMIEAm	0x40 
#define   TCR1_CMIEBm	0x80 
#define TMR_TCSR0	__PORT8 0xFFFF6A	/* Timer ControlStatus Register 0 */ 
#define   TCSR0_OS0m	0x01 
#define   TCSR0_OS1m	0x02 
#define   TCSR0_OS2m	0x04 
#define   TCSR0_OS3m	0x08 
#define   TCSR0_ADTEm	0x10 
#define   TCSR0_OVFm	0x20 
#define   TCSR0_CMFAm	0x40 
#define   TCSR0_CMFBm	0x80 
#define TMR_TCSR1	__PORT8 0xFFFF6B	/* Timer ControlStatus Register 1 */ 
#define   TCSR1_OS0m	0x01 
#define   TCSR1_OS1m	0x02 
#define   TCSR1_OS2m	0x04 
#define   TCSR1_OS3m	0x08 
#define   TCSR1_OVFm	0x20 
#define   TCSR1_CMFAm	0x40 
#define   TCSR1_CMFBm	0x80 
#define TMR_TCORA0	__PORT8 0xFFFF6C	/* Time Constant Register A0 */ 
#define TMR_TCORA1	__PORT8 0xFFFF6D	/* Time Constant Register A1 */ 
#define TMR_TCORB0	__PORT8 0xFFFF6E	/* Time Constant Register B0 */ 
#define TMR_TCORB1	__PORT8 0xFFFF6F	/* Time Constant Register B1 */ 
#define TMR_TCNT0	__PORT8 0xFFFF70	/* Timer Counter 0 */ 
#define TMR_TCNT1	__PORT8 0xFFFF71	/* Timer Counter 1 */ 

/* WDT0 register definitions start */
#define WDT_WTCSR0r	__PORT8 0xFFFF74	/* Timer ControlStatus Register 0 (RD/WC7) */ 
#define WDT_WTCSR0w	__PORT16 0xFFFF74	/*   writte address - password 0xa500  */ 
#define   WTCSR0_CKS0m	0x01 
#define   WTCSR0_CKS1m	0x02 
#define   WTCSR0_CKS2m	0x04 
#define   WTCSR0_CKSxm	0x07
#define   WTCSR0_TMEm	0x20 
#define   WTCSR0_WTITm	0x40 
#define   WTCSR0_WOVFm	0x80 
#define WDT_WTCNT0r	__PORT8 0xFFFF75	/* Timer Counter 0 (RD) */ 
#define WDT_WTCNT0w	__PORT16 0xFFFF74	/*   writte address - password 0x5a00  */ 
#define WDT_WRSTCSRr	__PORT8 0xFFFF77	/* Reset ControlStatus Register (RD/WC7) */ 
#define WDT_WRSTCSRw	__PORT16 0xFFFF76	/*   clear WOVF - password 0xa500 */ 
						/*   set bits   - password 0x5a00 */ 
#define   WRSTCSR_RSTSm	0x20 
#define   WRSTCSR_RSTEm	0x40 
#define   WRSTCSR_WOVFm	0x80 
/* WDT0 register definitions end */

#define IIC_ICCR0	__PORT8 0xFFFF78	/* I2C Bus Control Register */ 
#define SCI_SMR0	__PORT8 0xFFFF78	/* Serial Mode Register 0 */ 
#define Smart_SMR0	__PORT8 0xFFFF78	/* Serial Mode Register 0 */ 
#define   SMR0_CKS0m	0x01 
#define   SMR0_CKS1m	0x02 
#define   SMR0_MPm	0x04 
#define   SMR0_STOPm	0x08 
#define   SMR0_OEm	0x10 
#define   SMR0_PEm	0x20 
#define   SMR0_CHRm	0x40 
#define   SMR0_CAm	0x80 
#define IIC_ICSR0	__PORT8 0xFFFF79	/* I2C Bus Status Register */ 
#define SCI_BRR0	__PORT8 0xFFFF79	/* Bit Rate Register 0 */ 
#define SCI_SCR0	__PORT8 0xFFFF7A	/* Serial Control Register 0 */ 
#define   SCR0_CKE0m	0x01 
#define   SCR0_CKE1m	0x02 
#define   SCR0_TEIEm	0x04 
#define   SCR0_MPIEm	0x08 
#define   SCR0_REm	0x10 
#define   SCR0_TEm	0x20 
#define   SCR0_RIEm	0x40 
#define   SCR0_TIEm	0x80 
#define SCI_TDR0	__PORT8 0xFFFF7B	/* Transmit Data Register 0 */ 
#define SCI_SSR0	__PORT8 0xFFFF7C	/* Serial Status Register 0 */ 
#define   SSR0_MPBTm	0x01 
#define   SSR0_MPBm	0x02 
#define   SSR0_TENDm	0x04 
#define   SSR0_PERm	0x08 
#define   SSR0_FERm	0x10 
#define   SSR0_ORERm	0x20 
#define   SSR0_RDRFm	0x40 
#define   SSR0_TDREm	0x80 
#define SCI_RDR0	__PORT8 0xFFFF7D	/* Receive Data Register 0 */ 
#define IIC_ICDR0	__PORT8 0xFFFF7E	/* I2C Bus Data Register */ 
#define IIC_SARX0	__PORT8 0xFFFF7E	/* 2nd Slave Address Register */ 
#define SCI_SCMR0	__PORT8 0xFFFF7E	/* Smart Card Mode Register 0 */ 
#define   SCMR0_SMIFm	0x01 
#define   SCMR0_SINVm	0x04 
#define   SCMR0_SDIRm	0x08 
#define IIC_ICMR0	__PORT8 0xFFFF7F	/* I2C Bus Mode Register */ 
#define IIC_SAR0	__PORT8 0xFFFF7F	/* Slave Address Register */ 
#define   ICMR0_BC0FSm	0x01 
#define   ICMR0_BC1m	0x02 
#define   ICMR0_BC2m	0x04 
#define   ICMR0_CKS0m	0x08 
#define   ICMR0_CKS1m	0x10 
#define   ICMR0_CKS2m	0x20 
#define   ICMR0_WAITm	0x40 
#define   ICMR0_MLSm	0x80 
#define IIC_ICCR1	__PORT8 0xFFFF80	/* I2C Bus Control Register */ 
#define Interface_SMR1	__PORT8 0xFFFF80	/* Serial Mode Register 1 */ 
#define SCI_SMR1	__PORT8 0xFFFF80	/* Serial Mode Register 1 */ 
#define   SMR1_CKS0m	0x01 
#define   SMR1_CKS1m	0x02 
#define   SMR1_MPm	0x04 
#define   SMR1_STOPm	0x08 
#define   SMR1_OEm	0x10 
#define   SMR1_PEm	0x20 
#define   SMR1_CHRm	0x40 
#define   SMR1_CAm	0x80 
#define IIC_ICSR1	__PORT8 0xFFFF81	/* I2C Bus Status Register */ 
#define SCI_BRR1	__PORT8 0xFFFF81	/* Bit Rate Register 1 */ 
#define SCI_SCR1	__PORT8 0xFFFF82	/* Serial Control Register 1 */ 
#define   SCR1_CKE0m	0x01 
#define   SCR1_CKE1m	0x02 
#define   SCR1_TEIEm	0x04 
#define   SCR1_MPIEm	0x08 
#define   SCR1_REm	0x10 
#define   SCR1_TEm	0x20 
#define   SCR1_RIEm	0x40 
#define   SCR1_TIEm	0x80 
#define SCI_TDR1	__PORT8 0xFFFF83	/* Transmit Data Register 1 */ 
#define SCI_SSR1	__PORT8 0xFFFF84	/* Serial Status Register 1 */ 
#define   SSR1_MPBTm	0x01 
#define   SSR1_MPBm	0x02 
#define   SSR1_TENDm	0x04 
#define   SSR1_PERm	0x08 
#define   SSR1_FERm	0x10 
#define   SSR1_ORERm	0x20 
#define   SSR1_RDRFm	0x40 
#define   SSR1_TDREm	0x80 
#define SCI_RDR1	__PORT8 0xFFFF85	/* Receive Data Register 1 */ 
#define IIC_ICDR1	__PORT8 0xFFFF86	/* I2C Bus Data Register */ 
#define IIC_SARX1	__PORT8 0xFFFF86	/* 2nd Slave Address Register */ 
#define SCI_SCMR1	__PORT8 0xFFFF86	/* Smart Card Mode Register 1 */ 
#define   SCMR1_SMIFm	0x01 
#define   SCMR1_SINVm	0x04 
#define   SCMR1_SDIRm	0x08 
#define IIC_ICMR1	__PORT8 0xFFFF87	/* I2C Bus Mode Register */ 
#define IIC_SAR1	__PORT8 0xFFFF87	/* Slave Address Register */ 
#define   ICMR1_BC0FSm	0x01 
#define   ICMR1_BC1m	0x02 
#define   ICMR1_BC2m	0x04 
#define   ICMR1_CKS0m	0x08 
#define   ICMR1_CKS1m	0x10 
#define   ICMR1_CKS2m	0x20 
#define   ICMR1_WAITm	0x40 
#define   ICMR1_MLSm	0x80 
#define SCI_SMR2	__PORT8 0xFFFF88	/* Serial Mode Register 2 */ 
#define   SMR2_CKS0m	0x01 
#define   SMR2_CKS1m	0x02 
#define   SMR2_MPm	0x04 
#define   SMR2_STOPm	0x08 
#define   SMR2_OEm	0x10 
#define   SMR2_PEm	0x20 
#define   SMR2_CHRm	0x40 
#define   SMR2_CAm	0x80 
#define SCI_BRR2	__PORT8 0xFFFF89	/* Bit Rate Register 2 */ 
#define SCI_SCR2	__PORT8 0xFFFF8A	/* Serial Control Register 2 */ 
#define   SCR2_CKE0m	0x01 
#define   SCR2_CKE1m	0x02 
#define   SCR2_TEIEm	0x04 
#define   SCR2_MPIEm	0x08 
#define   SCR2_REm	0x10 
#define   SCR2_TEm	0x20 
#define   SCR2_RIEm	0x40 
#define   SCR2_TIEm	0x80 
#define SCI_TDR2	__PORT8 0xFFFF8B	/* Transmit Data Register 2 */ 
#define SCI_SSR2	__PORT8 0xFFFF8C	/* Serial Status Register 2 */ 
#define   SSR2_MPBTm	0x01 
#define   SSR2_MPBm	0x02 
#define   SSR2_TENDm	0x04 
#define   SSR2_PERm	0x08 
#define   SSR2_FERm	0x10 
#define   SSR2_ORERm	0x20 
#define   SSR2_RDRFm	0x40 
#define   SSR2_TDREm	0x80 
#define SCI_RDR2	__PORT8 0xFFFF8D	/* Receive Data Register 2 */ 
#define SCI_SCMR2	__PORT8 0xFFFF8E	/* Smart Card Mode Register 2 */ 
#define   SCMR2_SMIFm	0x01 
#define   SCMR2_SINVm	0x04 
#define   SCMR2_SDIRm	0x08
#define AD_ADDRA	__PORT16 0xFFFF90	/* AD Data Register A */ 
#define AD_ADDRAH	__PORT8 0xFFFF90	/* AD Data Register AH */ 
#define   ADDRAH_AD2m	0x01 
#define   ADDRAH_AD3m	0x02 
#define   ADDRAH_AD4m	0x04 
#define   ADDRAH_AD5m	0x08 
#define   ADDRAH_AD6m	0x10 
#define   ADDRAH_AD7m	0x20 
#define   ADDRAH_AD8m	0x40 
#define   ADDRAH_AD9m	0x80 
#define AD_ADDRAL	__PORT8 0xFFFF91	/* AD Data Register AL */ 
#define   ADDRAL_AD0m	0x40 
#define   ADDRAL_AD1m	0x80 
#define AD_ADDRB	__PORT16 0xFFFF92	/* AD Data Register B */ 
#define AD_ADDRBH	__PORT8 0xFFFF92	/* AD Data Register BH */ 
#define   ADDRBH_AD2m	0x01 
#define   ADDRBH_AD3m	0x02 
#define   ADDRBH_AD4m	0x04 
#define   ADDRBH_AD5m	0x08 
#define   ADDRBH_AD6m	0x10 
#define   ADDRBH_AD7m	0x20 
#define   ADDRBH_AD8m	0x40 
#define   ADDRBH_AD9m	0x80 
#define AD_ADDRBL	__PORT8 0xFFFF93	/* AD Data Register BL */ 
#define   ADDRBL_AD0m	0x40 
#define   ADDRBL_AD1m	0x80 
#define AD_ADDRC	__PORT16 0xFFFF94	/* AD Data Register C */ 
#define AD_ADDRCH	__PORT8 0xFFFF94	/* AD Data Register CH */ 
#define   ADDRCH_AD2m	0x01 
#define   ADDRCH_AD3m	0x02 
#define   ADDRCH_AD4m	0x04 
#define   ADDRCH_AD5m	0x08 
#define   ADDRCH_AD6m	0x10 
#define   ADDRCH_AD7m	0x20 
#define   ADDRCH_AD8m	0x40 
#define   ADDRCH_AD9m	0x80 
#define AD_ADDRCL	__PORT8 0xFFFF95	/* AD Data Register CL */ 
#define   ADDRCL_AD0m	0x40 
#define   ADDRCL_AD1m	0x80 
#define AD_ADDRD	__PORT16 0xFFFF96	/* AD Data Register D */ 
#define AD_ADDRDH	__PORT8 0xFFFF96	/* AD Data Register DH */ 
#define   ADDRDH_AD2m	0x01 
#define   ADDRDH_AD3m	0x02 
#define   ADDRDH_AD4m	0x04 
#define   ADDRDH_AD5m	0x08 
#define   ADDRDH_AD6m	0x10 
#define   ADDRDH_AD7m	0x20 
#define   ADDRDH_AD8m	0x40 
#define   ADDRDH_AD9m	0x80 
#define AD_ADDRDL	__PORT8 0xFFFF97	/* AD Data Register DL */ 
#define   ADDRDL_AD0m	0x40 
#define   ADDRDL_AD1m	0x80 
#define AD_ADCSR	__PORT8 0xFFFF98	/* AD ControlStatus Register */ 
#define   ADCSR_CH0m	0x01 
#define   ADCSR_CH1m	0x02 
#define   ADCSR_CH2m	0x04 
#define   ADCSR_CH3m	0x08 
#define   ADCSR_CHm	0x0f
#define   ADCSR_SCANm	0x10 
#define   ADCSR_ADSTm	0x20 
#define   ADCSR_ADIEm	0x40 
#define   ADCSR_ADFm	0x80 
#define AD_ADCR	__PORT8 0xFFFF99	/* AD Control Register */ 
#define   ADCR_CKS0m	0x04 
#define   ADCR_CKS1m	0x08 
#define   ADCR_CKSm	0x0C 
#define   ADCR_TRGS0m	0x40 
#define   ADCR_TRGS1m	0x80 
#define   ADCR_TRGSm	0xC0 

/* WDT1 register definitions start */
#define WDT_WTCSR1r	__PORT8 0xFFFFA2	/* Timer ControlStatus Register 1 (RD/WC7) */ 
#define WDT_WTCSR1w	__PORT16 0xFFFFA2	/*   writte address - password 0xa500  */ 
#define   WTCSR1_CKS0m	0x01 
#define   WTCSR1_CKS1m	0x02 
#define   WTCSR1_CKS2m	0x04 
#define   WTCSR1_RSTm	0x08 
#define   WTCSR1_PSSm	0x10 
#define   WTCSR1_TMEm	0x20 
#define   WTCSR1_WTITm	0x40 
#define   WTCSR1_OVFm	0x80 
#define WDT_WTCNT1r	__PORT8 0xFFFFA3	/* Timer Counter 1  (RD) */ 
#define WDT_WTCNT1w	__PORT16 0xFFFFA2	/*   writte address - password 0x5a00  */ 
/* WDT1 register definitions end */

#define DA_DADR0	__PORT8 0xFFFFA4	/* DA Data Register 0 */ 
#define DA_DADR1	__PORT8 0xFFFFA5	/* DA Data Register 1 */ 
#define DA_DACR01	__PORT8 0xFFFFA6	/* DA Control Register 01 */ 
#define   DACR01_DAEm	0x20 
#define   DACR01_DAOE0m	0x40 
#define   DACR01_DAOE1m	0x80 

/*Flash memory subsystem definitions start */
#define FLM_FLMCR1	__PORT8 0xFFFFA8	/* Flash Memory Control Register 1 */ 
#define   FLMCR1_P1m	0x01			/*   Transition to program mode */
#define   FLMCR1_E1m	0x02			/*   Transition to erase mode */
#define   FLMCR1_PV1m	0x04 			/*   Transition to program-verify mode */
#define   FLMCR1_EV1m	0x08 			/*   Transition to erase-verify mode */
#define   FLMCR1_PSU1m	0x10 			/*   Program setup when FWE = 1 and SWE1 = 1*/
#define   FLMCR1_ESU1m	0x20 			/*   Erase setup when FWE = 1 and SWE1 = 1 */
#define   FLMCR1_SWE1m	0x40			/*   1= enable writes when FWE=1 */ 
#define   FLMCR1_FWEm	0x80			/*   1 = programming enabled by FWE pin */ 
#define FLM_FLMCR2	__PORT8 0xFFFFA9	/* Flash Memory Control Register 2 */ 
#define   FLMCR2_FLERm	0x80 			/*   Flash memory modification error */
#define FLM_EBR1	__PORT8 0xFFFFAA	/* Erase Block Register 1 */ 
#define   EBR1_EB0m	0x01			/*   Selects block to erase */
#define   EBR1_EB1m	0x02 
#define   EBR1_EB2m	0x04 
#define   EBR1_EB3m	0x08 
#define   EBR1_EB4m	0x10 
#define   EBR1_EB5m	0x20 
#define   EBR1_EB6m	0x40 
#define   EBR1_EB7m	0x80 
#define FLM_EBR2	__PORT8 0xFFFFAB	/* Erase Block Register 2 */ 
#define   EBR2_EB8m	0x01 
#define   EBR2_EB9m	0x02 
#define   EBR2_EB10m	0x04 
#define   EBR2_EB11m	0x08 
#define FLM_FLPWCR	__PORT8 0xFFFFAC	/* Flash Memory Power Control Register */ 
#define   FLPWCR_PDWNDm	0x80 
/*Flash memory subsystem definitions end */

#define DIO_PORT1	__PORT8 0xFFFFB0	/* DIO 1 Register */ 
#define   PORT1_P10m	0x01 
#define   PORT1_P11m	0x02 
#define   PORT1_P12m	0x04 
#define   PORT1_P13m	0x08 
#define   PORT1_P14m	0x10 
#define   PORT1_P15m	0x20 
#define   PORT1_P16m	0x40 
#define   PORT1_P17m	0x80 
#define DIO_PORT3	__PORT8 0xFFFFB2	/* DIO 3 Register */ 
#define   PORT3_P30m	0x01 
#define   PORT3_P31m	0x02 
#define   PORT3_P32m	0x04 
#define   PORT3_P33m	0x08 
#define   PORT3_P34m	0x10 
#define   PORT3_P35m	0x20 
#define   PORT3_P36m	0x40 
#define   PORT3_P37m	0x80 
#define DIO_PORT4	__PORT8 0xFFFFB3	/* DIO 4 Register */ 
#define   PORT4_P40m	0x01 
#define   PORT4_P41m	0x02 
#define   PORT4_P42m	0x04 
#define   PORT4_P43m	0x08 
#define   PORT4_P44m	0x10 
#define   PORT4_P45m	0x20 
#define   PORT4_P46m	0x40 
#define   PORT4_P47m	0x80 
#define DIO_PORT7	__PORT8 0xFFFFB6	/* DIO 7 Register */ 
#define   PORT7_P70m	0x01 
#define   PORT7_P71m	0x02 
#define   PORT7_P72m	0x04 
#define   PORT7_P73m	0x08 
#define   PORT7_P74m	0x10 
#define   PORT7_P75m	0x20 
#define   PORT7_P76m	0x40 
#define   PORT7_P77m	0x80 
#define DIO_PORT9	__PORT8 0xFFFFB8	/* DIO 9 Register */ 
#define   PORT9_P90m	0x01 
#define   PORT9_P91m	0x02 
#define   PORT9_P92m	0x04 
#define   PORT9_P93m	0x08 
#define   PORT9_P94m	0x10 
#define   PORT9_P95m	0x20 
#define   PORT9_P96m	0x40 
#define   PORT9_P97m	0x80 
#define DIO_PORTA	__PORT8 0xFFFFB9	/* DIO A Register */ 
#define   PORTA_PA0m	0x01 
#define   PORTA_PA1m	0x02 
#define   PORTA_PA2m	0x04 
#define   PORTA_PA3m	0x08 
#define DIO_PORTB	__PORT8 0xFFFFBA	/* DIO B Register */ 
#define   PORTB_PB0m	0x01 
#define   PORTB_PB1m	0x02 
#define   PORTB_PB2m	0x04 
#define   PORTB_PB3m	0x08 
#define   PORTB_PB4m	0x10 
#define   PORTB_PB5m	0x20 
#define   PORTB_PB6m	0x40 
#define   PORTB_PB7m	0x80 
#define DIO_PORTC	__PORT8 0xFFFFBB	/* DIO C Register */ 
#define   PORTC_PC0m	0x01 
#define   PORTC_PC1m	0x02 
#define   PORTC_PC2m	0x04 
#define   PORTC_PC3m	0x08 
#define   PORTC_PC4m	0x10 
#define   PORTC_PC5m	0x20 
#define   PORTC_PC6m	0x40 
#define   PORTC_PC7m	0x80 
#define DIO_PORTD	__PORT8 0xFFFFBC	/* DIO D Register */ 
#define   PORTD_PD0m	0x01 
#define   PORTD_PD1m	0x02 
#define   PORTD_PD2m	0x04 
#define   PORTD_PD3m	0x08 
#define   PORTD_PD4m	0x10 
#define   PORTD_PD5m	0x20 
#define   PORTD_PD6m	0x40 
#define   PORTD_PD7m	0x80 
#define DIO_PORTE	__PORT8 0xFFFFBD	/* DIO E Register */ 
#define   PORTE_PE0m	0x01 
#define   PORTE_PE1m	0x02 
#define   PORTE_PE2m	0x04 
#define   PORTE_PE3m	0x08 
#define   PORTE_PE4m	0x10 
#define   PORTE_PE5m	0x20 
#define   PORTE_PE6m	0x40 
#define   PORTE_PE7m	0x80 
#define DIO_PORTF	__PORT8 0xFFFFBE	/* DIO F Register */ 
#define   PORTF_PF0m	0x01 
#define   PORTF_PF1m	0x02 
#define   PORTF_PF2m	0x04 
#define   PORTF_PF3m	0x08 
#define   PORTF_PF4m	0x10 
#define   PORTF_PF5m	0x20 
#define   PORTF_PF6m	0x40 
#define   PORTF_PF7m	0x80 
#define DIO_PORTG	__PORT8 0xFFFFBF	/* DIO G Register */ 
#define   PORTG_PG0m	0x01 
#define   PORTG_PG1m	0x02 
#define   PORTG_PG2m	0x04 
#define   PORTG_PG3m	0x08 
#define   PORTG_PG4m	0x10 

/* exception vectors numbers */

#define EXCPTVEC_POWRES	0
#define EXCPTVEC_MANRES	1
#define EXCPTVEC_TRACE	5
#define EXCPTVEC_DIRTRANS 6
#define EXCPTVEC_NMI	7
#define EXCPTVEC_TRAP0	8
#define EXCPTVEC_TRAP1	9
#define EXCPTVEC_TRAP2	10
#define EXCPTVEC_TRAP3	11
#define EXCPTVEC_IRQ0	16
#define EXCPTVEC_IRQ1	17
#define EXCPTVEC_IRQ2	18
#define EXCPTVEC_IRQ3	19
#define EXCPTVEC_IRQ4	20
#define EXCPTVEC_IRQ5	21
#define EXCPTVEC_IRQ6	22
#define EXCPTVEC_IRQ7	23
#define EXCPTVEC_SWDEND	24
#define EXCPTVEC_WOVI0	25
#define EXCPTVEC_CMI	26
#define EXCPTVEC_PBC	27
#define EXCPTVEC_ADI	28
#define EXCPTVEC_WOVI1	29
#define EXCPTVEC_TGI0A	32	/* TPU 0 */
#define EXCPTVEC_TGI0B	33
#define EXCPTVEC_TGI0C	34
#define EXCPTVEC_TGI0D	35
#define EXCPTVEC_TCI0V	36
#define EXCPTVEC_TGI1A	40	/* TPU 1 */
#define EXCPTVEC_TGI1B	41
#define EXCPTVEC_TCI1V	42
#define EXCPTVEC_TCI1U	43
#define EXCPTVEC_TGI2A	44	/* TPU 2 */
#define EXCPTVEC_TGI2B	45
#define EXCPTVEC_TCI2V	46
#define EXCPTVEC_TCI2U	47
#define EXCPTVEC_TGI3A	48	/* TPU 3 */
#define EXCPTVEC_TGI3B	49
#define EXCPTVEC_TGI3C	50
#define EXCPTVEC_TGI3D	51
#define EXCPTVEC_TCI3V	52
#define EXCPTVEC_TGI4A	56	/* TPU 4 */
#define EXCPTVEC_TGI4B	57
#define EXCPTVEC_TCI4V	58
#define EXCPTVEC_TCI4U	59
#define EXCPTVEC_TGI5A	60	/* TPU 5 */
#define EXCPTVEC_TGI5B	61
#define EXCPTVEC_TCI5V	62
#define EXCPTVEC_TCI5U	63
#define EXCPTVEC_CMIA0	64	/* 8 bit tim 0 */
#define EXCPTVEC_CMIB0	65
#define EXCPTVEC_OVI0	66
#define EXCPTVEC_CMIA1	68	/* 8 bit tim 1 */
#define EXCPTVEC_CMIB1	69
#define EXCPTVEC_OVI1	70
#define EXCPTVEC_DEND0A	72	/* DMAC */
#define EXCPTVEC_DEND0B	73
#define EXCPTVEC_DEND1A	74
#define EXCPTVEC_DEND1B	75
#define EXCPTVEC_ERI0	80	/* SCI 0 */
#define EXCPTVEC_RXI0	81
#define EXCPTVEC_TXI0	82
#define EXCPTVEC_TEI0	83
#define EXCPTVEC_ERI1	84	/* SCI 1 */
#define EXCPTVEC_RXI1	85
#define EXCPTVEC_TXI1	86
#define EXCPTVEC_TEI1	87
#define EXCPTVEC_ERI2	88	/* SCI 2 */
#define EXCPTVEC_RXI2	89
#define EXCPTVEC_TXI2	90
#define EXCPTVEC_TEI2	91
#define EXCPTVEC_CMIA2	92	/* 8 bit tim 2 */
#define EXCPTVEC_CMIB2	93
#define EXCPTVEC_OVI2	94
#define EXCPTVEC_CMIA3	96	/* 8 bit tim 3 */
#define EXCPTVEC_CMIB3	97
#define EXCPTVEC_OVI3	98
#define EXCPTVEC_IICI0	100	/* IIC 0 */
#define EXCPTVEC_DDCSW1	101
#define EXCPTVEC_IICI1	102	/* IIC 1 */
#define EXCPTVEC_ERI3	120	/* SCI 3 */
#define EXCPTVEC_RXI3	121
#define EXCPTVEC_TXI3	122
#define EXCPTVEC_TEI3	123
#define EXCPTVEC_ERI4	124	/* SCI 4 */
#define EXCPTVEC_RXI4	125
#define EXCPTVEC_TXI4	126
#define EXCPTVEC_TEI4	127

/* SCI common registers and bits start */

/*   Receive Data Register (RDR) */
/*   Transmit Data Register (TDR) */
/*   Serial Mode Register (SMR) */
#define   SMR_CKS0m	0x01
#define   SMR_CKS1m	0x02
#define   SMR_CKSxm	0x03 	/* Clock 3=/64, 2=/16, 1=/4, 0=/1 */
#define   SMR_MPm	0x04 	/* 1=Multiprocessor format selected */
#define   SMR_STOPm	0x08 	/* 1=2 stop bits, 0=1 stop bit */
#define   SMR_OEm	0x10 	/* 1=Odd parity, 0=Even */
#define   SMR_PEm	0x20 	/* 1=Parity addition and checking enabled */
#define   SMR_CHRm	0x40 	/* 1=7-bit data, 0=8-bit */
#define   SMR_CAm	0x80 	/* 1=Clocked, 0=Asynchronous */
#define	  SCI_SMR_8N1	(0|0|0)
#define	  SCI_SMR_7N1	(SMR_CHRm|0|0)
#define	  SCI_SMR_8N2	(0       |0|SMR_STOPm)
#define	  SCI_SMR_7N2	(SMR_CHRm|0|SMR_STOPm)
#define	  SCI_SMR_8E1	(0       |SMR_PEm|0)
#define	  SCI_SMR_7E1	(SMR_CHRm|SMR_PEm|0)
#define	  SCI_SMR_8O1	(0       |SMR_PEm|SMR_OEm)
#define	  SCI_SMR_7O1	(SMR_CHRm|SMR_PEm|SMR_OEm)
/*   Serial Control Register (SCR) */
#define   SCR_CKE0m	0x01 	/* Clock Enable */
#define   SCR_CKE1m	0x02 	/*  */
#define   SCR_TEIEm	0x04 	/* Transmit end interrupt (TEI) */
#define   SCR_MPIEm	0x08 	/* Only multiprocessor RXI interrupt enabled */
#define   SCR_REm	0x10 	/* Reception enabled */
#define   SCR_TEm	0x20 	/* Transmission enabled* */
#define   SCR_RIEm	0x40 	/* RXI interrupt requests enabled */ 
#define   SCR_TIEm	0x80	/* TXI interrupt requests enabled */ 
/*   Serial Status Register (SSR) */
#define   SSR_MPBTm	0x01 	/* Value to send as bit 8  */
#define   SSR_MPBm	0x02 	/* MP Bit 8 received value */
#define   SSR_TENDm	0x04 	/* Transmit End */
#define   SSR_PERm	0x08 	/* Parity error */
#define   SSR_FERm	0x10 	/* Framing error */
#define   SSR_ORERm	0x20 	/* Receive overflow */
#define   SSR_RDRFm	0x40 	/* Set when reception ends normally */
#define   SSR_TDREm	0x80 	/* Set when TDR empty or SCR_TE=0 */
/*   Bit Rate Register (BRR) */
/*     for async set to N=Fsys/(32*2^(2n)*baud)-1  where n=SMR_CKS */
/*     for sync set to  N=Fsys/(4*2^(2n)*baud)-1 */
/*   Smart Card Mode Register (SCMR) */
#define   SCMR_SMIFm	0x01 	/* 1=Smart card interface enabled */
#define   SCMR_SINVm	0x04 	/* 1=TDR contents inverted */
#define   SCMR_SDIRm	0x08 	/* 1=MSB-first, 0=LSB-first */
/*   I2C Bus Mode / Slave Address Register (ICMR/SAR)*/ 
/*     only for SCI0 and SCI1 */
#define   ICMR_BC0m	0x01	/* Bit Counter */
#define   ICMR_BC1m	0x02 
#define   ICMR_BC2m	0x04 
#define   ICMR_BCm	(ICMR_BC0m|ICMR_BC1m|ICMR_BC2m)
#define   ICMR_CKS0m	0x08	/* Serial Clock Select */
#define   ICMR_CKS1m	0x10 
#define   ICMR_CKS2m	0x20 
#define   ICMR_CKSm	(ICMR_CKS0m|ICMR_CKS1m|ICMR_CKS2m) 
#define   ICMR_WAITm	0x40	/* 1 .. Wait between data and acknowledge */
#define   ICMR_MLSm	0x80	/* 0 .. MSB-first / 1 .. LSB-first */ 
/*   I2C Bus Control Register (ICCR) */
#define   ICCR_SCPm	0x01	/* Write 0 with BBSY to start/stop */
#define   ICCR_IRICm	0x02	/* 1 => interrupt requested */
#define   ICCR_BBSYm	0x04	/* 1 => bus is busy */
#define   ICCR_ACKEm	0x08	/* 1 => stop when no ACK detected */
#define   ICCR_TRSm	0x10	/* 1 .. transmit / 0 .. receive */
#define   ICCR_MSTm	0x20	/* 1 .. master mode / 0 .. slave mode */
#define   ICCR_IEICm	0x40	/* Interrupts enabled */
#define   ICCR_ICEm	0x80	/* 1 .. IIC enabled (ICMR,ICDR accessible) */
				/* 0 .. IIC disabled (SAR,SARX accessible) */
/*   IIC Bus Status Register (ICSR) */
#define   ICSR_ACKBm    0x01	/* Acknowledge Bit */
#define   ICSR_ADZm     0x02	/* General Call Address Recognition */
#define   ICSR_AASm     0x04	/* Slave Address Recognition */
#define   ICSR_ALm      0x08	/* Arbitration Lost */
#define   ICSR_AASXm    0x10	/* Second Slave Address Recognition */
#define   ICSR_IRTRm    0x20	/* Continuous Transmission/Reception Interrupt */
#define   ICSR_STOPm    0x40	/* Normal Stop Condition Detection Flag */
#define   ICSR_ESTPm    0x80	/* Error Stop Condition Detection Flag */

/* SCI common registers and bits end */

/* TPU common registers and bits start */

/*   Timer control register  (TPCR) */
#define   TPCR_TPSCm	0x07 	/* Clock sources */
#define   TPCR_TPSC_F1	0x00 	/*   fi clock/1 */
#define   TPCR_TPSC_F4	0x01 	/*   fi clock/4 */
#define   TPCR_TPSC_F16	0x02 	/*   fi clock/16 */
#define   TPCR_TPSC_F64	0x03 	/*   fi clock/64 */
#define   TPCR_TPSC_CA	0x04 	/*   TCLKA */
#define   TPCR_TPSC_012CB 0x05 	/*   TCLKB (only 012) */
#define   TPCR_TPSC_02CC 0x06 	/*   TCLKC (only 02) */
#define   TPCR_TPSC_45CC 0x05 	/*   TCLKC (only 45) */
#define   TPCR_TPSC_05CD 0x07 	/*   TCLKD (only 05) */
#define   TPCR_TPSC_135F256 0x06 /*   fi clock/256 (only 135) */
				/*   fi clock/1024 (only 234) */
				/*   fi clock/4096 (only 3) */
#define   TPCR_CKEGm	0x018 	/* Clock edge */
#define   TPCR_CKEG_RIS	0x000 	/*   Rising edge */
#define   TPCR_CKEG_FAL	0x008 	/*   Falling edge */
#define   TPCR_CKEG_BOTH 0x018 	/*   Both edges */
#define   TPCR_CCLRm	0xe0 	/* Counter clearing source */
#define   TPCR_CCLR_DIS	0x00 	/*   disabled */
#define   TPCR_CCLR_TGRA 0x20 	/*   source TGRA compare match/input capture */
#define   TPCR_CCLR_TGRB 0x40 	/*   source TGRB compare match/input capture */
#define   TPCR_CCLR_SYNC 0x60 	/*   synchronous clear by TSYR_SYNC */
#define   TPCR_CCLR_TGRC 0xa0 	/*   source TGRC compare match/input capture */
#define   TPCR_CCLR_TGRD 0xc0 	/*   source TGRD compare match/input capture */

/*   Timer mode register  (TMDR) */
#define   TPMDR_MDm	0x0f 	/* timer operating mode */
#define   TPMDR_MD_NORMAL 0x00 	/*   normal */
#define   TPMDR_MD_PWM1	 0x02 	/*   PWM 1 */
#define   TPMDR_MD_PWM2	 0x03 	/*   PWM 2 */
#define   TPMDR_MD_PHACN1 0x04 	/*   phase counting 1 (only 1245) */
#define   TPMDR_MD_PHACN2 0x05 	/*   phase counting 2 (only 1245) */
#define   TPMDR_MD_PHACN3 0x06 	/*   phase counting 3 (only 1245) */
#define   TPMDR_MD_PHACN4 0x07 	/*   phase counting 4 (only 1245) */
#define   TPMDR_BFAm	 0x10 	/* TGRA, TGRC together for buffer operation */
#define   TPMDR_BFBm	 0x20 	/* TGRB, TGRD together for buffer operation */
/* Timer I/O control register (TPIORx / TPIORxH+TPIORxL) */
/* Timer interrupt enable register (TPIER) */
#define   TPIER_TGIEAm	0x01 	/* TGRA comp/capt flag (TGFA) */
#define   TPIER_TGIEBm	0x02 	/* TGRB comp/capt flag (TGFB) */
#define   TPIER_TGIECm	0x04 	/* TGRC comp/capt flag (TGFC) */
#define   TPIER_TGIEDm	0x08 	/* TGRD comp/capt flag (TGFD) */
#define   TPIER_TCIEVm	0x10	/* Overflow interrupt enable (TCIEV) */
#define   TPIER_TCIEUm	0x20	/* Underflow int. enable (TCIEU) (only 1245) */
#define   TPIER_TTGEm	0x80	/* TGRA action starts AD converter */
/* Timer status register (TPSR) clear by write only */
#define   TPSR_TGFAm	0x01	/* TGRA comp/capt, can initiate DTC */
#define   TPSR_TGFBm	0x02	/* TGRB comp/capt, can initiate DTC */
#define   TPSR_TGFCm	0x04	/* TGRC comp/capt, can initiate DTC */
#define   TPSR_TGFDm	0x08	/* TGRD comp/capt, can initiate DTC */
#define   TPSR_TCFVm	0x10	/* overflow */
#define   TPSR_TCFUm	0x20	/* underflow */
#define   TPSR_TCFDm	0x80 	/* 0=count down, 1=count up */
/* Timer counter (TPCNT) */
/* Timer general registers (TPGRxA,TPGRxB,TPGRxC,TPGRxD */

/* TPU common registers and bits end */


/* h8s2638.h compatibility */
#define TPU_TSR1 TPU_TPSR1
#define TSR1_TCFVm TPSR1_TCFVm
#define TPU_TCR1 TPU_TPCR1
#define TPU_TMDR1 TPU_TPMDR1
#define TPU_TIER1 TPU_TPIER1
#define TPU_TSTR TPU_TPSTR
#define TSTR_CST1m TPSTR_CST1m
#define TIER1_TCIEVm TPIER1_TCIEVm


#endif /* _H82633H_H */
