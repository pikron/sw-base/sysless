#include <periph/sci_rs232.h>
#include <periph/sci_channels.h>

sci_info_t *sci_rs232_chan_array[] = {
    &sci_rs232_chan0,
    &sci_rs232_chan1,
    &sci_rs232_chan2
};

int sci_rs232_chan_count = sizeof(sci_rs232_chan_array)/sizeof(*sci_rs232_chan_array);
