/*******************************************************************
  Components for embedded applications builded for
  CVUT FEL.

   h8s2639h.h - internal peripherals registers of H8S2630,H8S2636,
                                                  H8S2638,H8S2639
   internal comment: ver 1.2 (added HCAN masks)
 *******************************************************************/

#ifndef _H82639H_H
#define _H82639H_H

#ifndef __ASSEMBLY__

#include <stdint.h>

#define __PORT8  (volatile uint8_t  * const)
#define __PORT16 (volatile uint16_t * const)
#define __PORT32 (volatile uint32_t * const)

#else	/* __ASSEMBLY__ */
#define __PORT8
#define __PORT16
#define __PORT32
#endif	/* __ASSEMBLY__ */

/* Module DTC */
//#define DTC_MRA	__PORT8 0x?????	/* DTC Mode Register A */ 
//#define   MRA_SZm	0x01 
//#define   MRA_DTSm	0x02 
//#define   MRA_MD0m	0x04 
//#define   MRA_MD1m	0x08 
//#define   MRA_DM0m	0x10 
//#define   MRA_DM1m	0x20 
//#define   MRA_SM0m	0x40 
//#define   MRA_SM1m	0x80 
//#define DTC_MRB	__PORT8 0x????	/* DTC Mode Register B */ 
//#define   MRB_DISELm	0x40 
//#define   MRB_CHNEm	0x80 
//#define DTC_SAR	__PORT?? 0x????	/* DTC Source Address Register */ 
//#define DTC_DAR	__PORT?? 0x????	/* DTC Destination Address Register */ 
//#define DTC_CRA	__PORT16 0x????	/* DTC Transfer Count Register A */ 
//#define DTC_CRB	__PORT16 0x????	/* DTC Transfer Count Register B */ 


/* Module HCAN1 and HCAN2 */
		/* Configuration registers for HCAN0 and HCAN1 */
#define HCAN0_MCR	__PORT8 0xFFFFF800	/* HCAN0 Master Control Register */
#define HCAN1_MCR	__PORT8 0xFFFFFA00	/* HCAN1 Master Control Register */
#define   MCR_MCR0m	0x01 
#define   MCR_MCR1m	0x02 
#define   MCR_MCR2m	0x04 
#define   MCR_MCR5m	0x20 
#define   MCR_MCR7m	0x80 
#define HCAN0_GSR	__PORT8 0xFFFFF801	/* HCAN0 General Status Register */
#define HCAN1_GSR	__PORT8 0xFFFFFA01	/* HCAN1 General Status Register */
#define   GSR_GSR0m	0x01 			/* Bus Off Flag */
#define   GSR_GSR1m	0x02 			/* Transmit/Receive Warning Flag */
#define   GSR_GSR2m	0x04			/* Message Transmission Status Flag */
#define   GSR_GSR3m	0x08			/* Reset Status Bit */
#define HCAN0_BCR	__PORT16 0xFFFFF802	/* HCAN0 Bit Configuration Register */
#define HCAN1_BCR	__PORT16 0xFFFFFA02	/* HCAN1 Bit Configuration Register */
#define   BCR_BRPm	0x3f00			/* Baud Rate Prescaler (BRP) bits 8-13 */
#define   BCR_BCR0m	0x0100			/* Baud Rate Prescaler (BRP) - bit 8 */
#define   BCR_BCR1m	0x0200			/* Baud Rate Prescaler (BRP) - bit 9 */
#define   BCR_BCR2m	0x0400			/* Baud Rate Prescaler (BRP) - bit 10 */
#define   BCR_BCR3m	0x0800			/* Baud Rate Prescaler (BRP) - bit 11 */
#define   BCR_BCR4m	0x1000			/* Baud Rate Prescaler (BRP) - bit 12 */
#define   BCR_BCR5m	0x2000			/* Baud Rate Prescaler (BRP) - bit 13 */
#define   BCR_SJWm	0xc000			/* Resynchronization Jump Width (SJW) */
#define   BCR_BCR6m	0x4000			/* Resynchronization Jump Width - bit 14 */
#define   BCR_BCR7m	0x8000			/* Resynchronization Jump Width - bit 15 */
#define   BCR_BCR15m	0x8000			/* Bit Sample Point (BSP) */
#define   BCR_TSEG1m	0x000f			/* Time Segment 1 (TSEG1) bits 0-3 */
#define   BCR_BCR8m	0x0001			/* Time Segment 1 (TSEG1) - bit 0 */
#define   BCR_BCR9m	0x0002			/* Time Segment 1 (TSEG1) - bit 1 */
#define   BCR_BCR10m	0x0004			/* Time Segment 1 (TSEG1) - bit 2 */
#define   BCR_BCR11m	0x0008			/* Time Segment 1 (TSEG1) - bit 3 */
#define   BCR_TSEG2m	0x0070			/* Time Segment 2 (TSEG2) bits 4-6 */
#define   BCR_BCR12m	0x0010			/* Time Segment 2 (TSEG2) - bit 4 */
#define   BCR_BCR13m	0x0020			/* Time Segment 2 (TSEG2) - bit 5 */
#define   BCR_BCR14m	0x0040			/* Time Segment 2 (TSEG2) - bit 6 */
#define HCAN0_BCRL	__PORT8 0xFFFFF802	/* HCAN0 Bit Configuration Register L */
#define HCAN1_BCRL	__PORT8 0xFFFFFA02	/* HCAN1 Bit Configuration Register L */
#define   BCRL_BCR0m	0x01			/* Time Segment 1 (TSEG1) bits 0-3 (BCR0-3) */
#define   BCRL_BCR1m	0x02
#define   BCRL_BCR2m	0x04
#define   BCRL_BCR3m	0x08
#define   BCRL_BCR4m	0x10			/* Time Segment 2 (TSEG2) bits 4-6 (BCR4-6) */
#define   BCRL_BCR5m	0x20
#define   BCRL_BCR6m	0x40
#define   BCRL_BCR15m	0x80			/* Bit Sample Point (BSP) */
#define HCAN0_BCRH	__PORT8 0xFFFFF803	/* HCAN0 Bit Configuration Register H */
#define HCAN1_BCRH	__PORT8 0xFFFFFA03	/* HCAN1 Bit Configuration Register H */
#define   BCRH_BCR0m	0x01			/* Baud Rate Prescaler (BRP) bits 8-13 */
#define   BCRH_BCR1m	0x02
#define   BCRH_BCR2m	0x04
#define   BCRH_BCR3m	0x08
#define   BCRH_BCR4m	0x10
#define   BCRH_BCR5m	0x20
#define   BCRH_BCR6m	0x40			/* Resynchronization Jump Width bits 14-15 */
#define   BCRH_BCR7m	0x80
#define HCAN0_MBCR	__PORT16 0xFFFFF804	/* HCAN0 Mailbox Configuration Register */
#define HCAN1_MBCR	__PORT16 0xFFFFFA04	/* HCAN1 Mailbox Configuration Register */
#define   MBCR_MBCR8m	0x0001			/* 0 = Corresponding mailbox(8) is set for transmission */
#define   MBCR_MBCR9m	0x0002
#define   MBCR_MBCR10m	0x0004
#define   MBCR_MBCR11m	0x0008
#define   MBCR_MBCR12m	0x0010
#define   MBCR_MBCR13m	0x0020
#define   MBCR_MBCR14m	0x0040
#define   MBCR_MBCR15m	0x0080
#define   MBCR_MBCR1m	0x0200
#define   MBCR_MBCR2m	0x0400
#define   MBCR_MBCR3m	0x0800
#define   MBCR_MBCR4m	0x0100
#define   MBCR_MBCR5m	0x0200
#define   MBCR_MBCR6m	0x0400
#define   MBCR_MBCR7m	0x0800
#define HCAN0_TXPR	__PORT16 0xFFFFF806	/* HCAN0 Transmit Wait Register */
#define HCAN1_TXPR	__PORT16 0xFFFFFA06	/* HCAN1 Transmit wait register */
#define   TXPR_TXPR8m	0x0001
#define   TXPR_TXPR9m	0x0002
#define   TXPR_TXPR10m	0x0004
#define   TXPR_TXPR11m	0x0008
#define   TXPR_TXPR12m	0x0010
#define   TXPR_TXPR13m	0x0020
#define   TXPR_TXPR14m	0x0040
#define   TXPR_TXPR15m	0x0080
#define   TXPR_TXPR1m	0x0200
#define   TXPR_TXPR2m	0x0400
#define   TXPR_TXPR3m	0x0800
#define   TXPR_TXPR4m	0x1000
#define   TXPR_TXPR5m	0x2000
#define   TXPR_TXPR6m	0x4000
#define   TXPR_TXPR7m	0x8000
#define HCAN0_TXCR	__PORT16 0xFFFFF808	/* HCAN0 Transmit wait cancel register */
#define HCAN1_TXCR	__PORT16 0xFFFFFA08	/* HCAN1 Transmit wait cancel register */
#define   TXCR_TXCR8m	0x0001
#define   TXCR_TXCR9m	0x0002
#define   TXCR_TXCR10m	0x0004
#define   TXCR_TXCR11m	0x0008
#define   TXCR_TXCR12m	0x0010
#define   TXCR_TXCR13m	0x0020
#define   TXCR_TXCR14m	0x0040
#define   TXCR_TXCR15m	0x0080
#define   TXCR_TXCR1m	0x0200
#define   TXCR_TXCR2m	0x0400
#define   TXCR_TXCR3m	0x0800
#define   TXCR_TXCR4m	0x1000
#define   TXCR_TXCR5m	0x2000
#define   TXCR_TXCR6m	0x4000
#define   TXCR_TXCR7m	0x8000
#define HCAN0_TXACK	__PORT16 0xFFFFF80A	/* HCAN0 Transmit Acknowledge Register */
#define HCAN1_TXACK	__PORT16 0xFFFFFA0A	/* HCAN1 Transmit Acknowledge Register */
#define   TXACK_TXACK8m	0x0001
#define   TXACK_TXACK9m	0x0002
#define   TXACK_TXACK10m	0x0004
#define   TXACK_TXACK11m	0x0008
#define   TXACK_TXACK12m	0x0010
#define   TXACK_TXACK13m	0x0020
#define   TXACK_TXACK14m	0x0040
#define   TXACK_TXACK15m	0x0080
#define   TXACK_TXACK1m	0x0200
#define   TXACK_TXACK2m	0x0400
#define   TXACK_TXACK3m	0x0800
#define   TXACK_TXACK4m	0x1000
#define   TXACK_TXACK5m	0x2000
#define   TXACK_TXACK6m	0x4000
#define   TXACK_TXACK7m	0x8000
#define HCAN0_ABACK	__PORT16 0xFFFFF80C	/* HCAN0 Abort Acknowledge Register */
#define HCAN1_ABACK	__PORT16 0xFFFFFA0C	/* HCAN1 Abort Acknowledge Register */
#define   ABACK_ABACK8m	0x0001
#define   ABACK_ABACK9m	0x0002
#define   ABACK_ABACK10m	0x0004
#define   ABACK_ABACK11m	0x0008
#define   ABACK_ABACK12m	0x0010
#define   ABACK_ABACK13m	0x0020
#define   ABACK_ABACK14m	0x0040
#define   ABACK_ABACK15m	0x0080
#define   ABACK_ABACK1m	0x0200
#define   ABACK_ABACK2m	0x0400
#define   ABACK_ABACK3m	0x0800
#define   ABACK_ABACK4m	0x1000
#define   ABACK_ABACK5m	0x2000
#define   ABACK_ABACK6m	0x4000
#define   ABACK_ABACK7m	0x8000
#define HCAN0_RXPR	__PORT16 0xFFFFF80E	/* HCAN0 Receive Complete Register */
#define HCAN1_RXPR	__PORT16 0xFFFFFA0E	/* HCAN1 Receive Complete Register */
#define   RXPR_RXPR8m	0x0001
#define   RXPR_RXPR9m	0x0002
#define   RXPR_RXPR10m	0x0004
#define   RXPR_RXPR11m	0x0008
#define   RXPR_RXPR12m	0x0010
#define   RXPR_RXPR13m	0x0020
#define   RXPR_RXPR14m	0x0040
#define   RXPR_RXPR15m	0x0080
#define   RXPR_RXPR0m	0x0100
#define   RXPR_RXPR1m	0x0200
#define   RXPR_RXPR2m	0x0400
#define   RXPR_RXPR3m	0x0800
#define   RXPR_RXPR4m	0x1000
#define   RXPR_RXPR5m	0x2000
#define   RXPR_RXPR6m	0x4000
#define   RXPR_RXPR7m	0x8000
#define HCAN0_RFPR	__PORT16 0xFFFFF810	/* HCAN0 Remote Request Register */
#define HCAN1_RFPR	__PORT16 0xFFFFFA10	/* HCAN1 Remote Request Register */
#define   RFPR_RFPR8m	0x0001
#define   RFPR_RFPR9m	0x0002
#define   RFPR_RFPR10m	0x0004
#define   RFPR_RFPR11m	0x0008
#define   RFPR_RFPR12m	0x0010
#define   RFPR_RFPR13m	0x0020
#define   RFPR_RFPR14m	0x0040
#define   RFPR_RFPR15m	0x0080
#define   RFPR_RFPR0m	0x0100
#define   RFPR_RFPR1m	0x0200
#define   RFPR_RFPR2m	0x0400
#define   RFPR_RFPR3m	0x0800
#define   RFPR_RFPR4m	0x1000
#define   RFPR_RFPR5m	0x2000
#define   RFPR_RFPR6m	0x4000
#define   RFPR_RFPR7m	0x8000
#define HCAN0_IRR	__PORT16 0xFFFFF812	/* HCAN0 Interrupt Register */
#define HCAN1_IRR	__PORT16 0xFFFFFA12	/* HCAN1 Interrupt Register */
#define   IRR_IRR0m	0x0100
#define   IRR_IRR1m	0x0200
#define   IRR_IRR2m	0x0400
#define   IRR_IRR3m	0x0800
#define   IRR_IRR4m	0x1000
#define   IRR_IRR5m	0x2000
#define   IRR_IRR6m	0x4000
#define   IRR_IRR7m	0x8000
#define   IRR_IRR8m	0x0001
#define   IRR_IRR9m	0x0002
#define   IRR_IRR12m	0x0010
#define HCAN0_IRRL	__PORT8 0xFFFFF812	/* HCAN0 Interrupt Register L */
#define HCAN1_IRRL	__PORT8 0xFFFFFA12	/* HCAN1 Interrupt Register L */
#define   IRRL_IRR0m	0x01
#define   IRRL_IRR1m	0x02
#define   IRRL_IRR2m	0x04
#define   IRRL_IRR3m	0x08
#define   IRRL_IRR4m	0x10
#define   IRRL_IRR5m	0x20
#define   IRRL_IRR6m	0x40
#define   IRRL_IRR7m	0x80
#define HCAN0_IRRH	__PORT8 0xFFFFF813	/* HCAN0 Interrupt Register H */
#define HCAN1_IRRH	__PORT8 0xFFFFFA13	/* HCAN0 Interrupt Register H */
#define   IRRH_IRR8m	0x01
#define   IRRH_IRR9m	0x02
#define   IRRH_IRR12m	0x10
#define HCAN0_MBIMR	__PORT16 0xFFFFF814	/* HCAN0 Mailbox Interrupt Mask Register */
#define HCAN1_MBIMR	__PORT16 0xFFFFFA14	/* HCAN1 Mailbox Interrupt Mask Register */
#define   MBIMR_MBIMR8m	0x0001
#define   MBIMR_MBIMR9m	0x0002
#define   MBIMR_MBIMR10m	0x0004
#define   MBIMR_MBIMR11m	0x0008
#define   MBIMR_MBIMR12m	0x0010
#define   MBIMR_MBIMR13m	0x0020
#define   MBIMR_MBIMR14m	0x0040
#define   MBIMR_MBIMR15m	0x0080
#define   MBIMR_MBIMR0m	0x0100
#define   MBIMR_MBIMR1m	0x0200
#define   MBIMR_MBIMR2m	0x0400
#define   MBIMR_MBIMR3m	0x0800
#define   MBIMR_MBIMR4m	0x1000
#define   MBIMR_MBIMR5m	0x2000
#define   MBIMR_MBIMR6m	0x4000
#define   MBIMR_MBIMR7m	0x8000
#define HCAN0_IMR	__PORT16 0xFFFFF816	/* HCAN0 Interrupt Mask Register */
#define HCAN1_IMR	__PORT16 0xFFFFFA16	/* HCAN1 Interrupt Mask Register */
#define   IMR_IMR8m	0x0001
#define   IMR_IMR9m	0x0002
#define   IMR_IMR12m	0x0010
#define   IMR_IMR1m	0x0200
#define   IMR_IMR2m	0x0400
#define   IMR_IMR3m	0x0800
#define   IMR_IMR4m	0x1000
#define   IMR_IMR5m	0x2000
#define   IMR_IMR6m	0x4000
#define   IMR_IMR7m	0x8000
#define HCAN0_IMRL	__PORT8 0xFFFFF816	/* HCAN0 Interrupt Mask Register L */
#define HCAN1_IMRL	__PORT8 0xFFFFFA16	/* HCAN1 Interrupt Mask Register L */
#define   IMRL_IMR1m	0x02
#define   IMRL_IMR2m	0x04
#define   IMRL_IMR3m	0x08
#define   IMRL_IMR4m	0x10
#define   IMRL_IMR5m	0x20
#define   IMRL_IMR6m	0x40
#define   IMRL_IMR7m	0x80
#define HCAN0_IMRH	__PORT8 0xFFFFF817	/* HCAN0 Interrupt Mask Register H */
#define HCAN1_IMRH	__PORT8 0xFFFFFA17	/* HCAN1 Interrupt Mask Register H */
#define   IMRH_IMR8m	0x01
#define   IMRH_IMR9m	0x02
#define   IMRH_IMR12m	0x10
#define HCAN0_REC	__PORT8 0xFFFFF818	/* HCAN0 Receive Error Counter */
#define HCAN1_REC	__PORT8 0xFFFFFA18	/* HCAN1 Receive Error Counter */
#define HCAN0_TEC	__PORT8 0xFFFFF819	/* HCAN0 Transmit Error Counter */
#define HCAN1_TEC	__PORT8 0xFFFFFA19	/* HCAN1 Transmit Error Counter */
#define HCAN0_UMSR	__PORT16 0xFFFFF81A	/* HCAN0 Unread Message Status Register */
#define HCAN1_UMSR	__PORT16 0xFFFFFA1A	/* HCAN1 Unread Message Status Register */
#define   UMSR_UMSR8m	0x0001
#define   UMSR_UMSR9m	0x0002
#define   UMSR_UMSR10m	0x0004
#define   UMSR_UMSR11m	0x0008
#define   UMSR_UMSR12m	0x0010
#define   UMSR_UMSR13m	0x0020
#define   UMSR_UMSR14m	0x0040
#define   UMSR_UMSR15m	0x0080
#define   UMSR_UMSR0m	0x0100
#define   UMSR_UMSR1m	0x0200
#define   UMSR_UMSR2m	0x0400
#define   UMSR_UMSR3m	0x0800
#define   UMSR_UMSR4m	0x1000
#define   UMSR_UMSR5m	0x2000
#define   UMSR_UMSR6m	0x4000
#define   UMSR_UMSR7m	0x8000
#define HCAN0_LAFML	__PORT16 0xFFFFF81C	/* HCAN0 Local Acceptance Filter Masks L */
#define HCAN1_LAFML	__PORT16 0xFFFFFA1C	/* HCAN1 Local Acceptance Filter Masks L */
#define HCAN0_LAFMH	__PORT16 0xFFFFF81E	/* HCAN0 Local Acceptance Filter Masks H */
#define HCAN1_LAFMH	__PORT16 0xFFFFFA1E	/* HCAN1 Local Acceptance Filter Masks H */
		/* Message Control and Data registers (MC0 to MC15) and (MD0 to MD15)  for HCAN0 and HCAN1 */
#define HCAN0_MC0	__PORT8 0xFFFFF820	/* Message Control 0 */
#define HCAN1_MC0	__PORT8 0xFFFFFA20
#define HCAN0_MC1	__PORT8 0xFFFFF828	/* Message Control 1 */
#define HCAN1_MC1	__PORT8 0xFFFFFA28
#define HCAN0_MC2	__PORT8 0xFFFFF830	/* Message Control 2 */
#define HCAN1_MC2	__PORT8 0xFFFFFA30
#define HCAN0_MC3	__PORT8 0xFFFFF838	/* Message Control 3 */
#define HCAN1_MC3	__PORT8 0xFFFFFA38
#define HCAN0_MC4	__PORT8 0xFFFFF840	/* Message Control 4 */
#define HCAN1_MC4	__PORT8 0xFFFFFA40
#define HCAN0_MC5	__PORT8 0xFFFFF848	/* Message Control 5 */
#define HCAN1_MC5	__PORT8 0xFFFFFA48
#define HCAN0_MC6	__PORT8 0xFFFFF850	/* Message Control 6 */
#define HCAN1_MC6	__PORT8 0xFFFFFA50
#define HCAN0_MC7	__PORT8 0xFFFFF858	/* Message Control 7 */
#define HCAN1_MC7	__PORT8 0xFFFFFA58
#define HCAN0_MC8	__PORT8 0xFFFFF860	/* Message Control 8 */
#define HCAN1_MC8	__PORT8 0xFFFFFA60
#define HCAN0_MC9	__PORT8 0xFFFFF868	/* Message Control 9 */
#define HCAN1_MC9	__PORT8 0xFFFFFA68
#define HCAN0_MC10	__PORT8 0xFFFFF870	/* Message Control 10 */
#define HCAN1_MC10	__PORT8 0xFFFFFA70
#define HCAN0_MC11	__PORT8 0xFFFFF878	/* Message Control 11 */
#define HCAN1_MC11	__PORT8 0xFFFFFA78
#define HCAN0_MC12	__PORT8 0xFFFFF880	/* Message Control 12 */
#define HCAN1_MC12	__PORT8 0xFFFFFA80
#define HCAN0_MC13	__PORT8 0xFFFFF888	/* Message Control 13 */
#define HCAN1_MC13	__PORT8 0xFFFFFA88
#define HCAN0_MC14	__PORT8 0xFFFFF890	/* Message Control 14 */
#define HCAN1_MC14	__PORT8 0xFFFFFA90
#define HCAN0_MC15	__PORT8 0xFFFFF898	/* Message Control 15 */
#define HCAN1_MC15	__PORT8 0xFFFFFA98
#define HCAN0_MD0	__PORT8 0xFFFFF8B0	/* Message Data 0 */
#define HCAN1_MD0	__PORT8 0xFFFFFAB0
#define HCAN0_MD1	__PORT8 0xFFFFF8B8	/* Message Data 1 */
#define HCAN1_MD1	__PORT8 0xFFFFFAB8
#define HCAN0_MD2	__PORT8 0xFFFFF8C0	/* Message Data 2 */
#define HCAN1_MD2	__PORT8 0xFFFFFAC0
#define HCAN0_MD3	__PORT8 0xFFFFF8C8	/* Message Data 3 */
#define HCAN1_MD3	__PORT8 0xFFFFFAC8
#define HCAN0_MD4	__PORT8 0xFFFFF8D0	/* Message Data 4 */
#define HCAN1_MD4	__PORT8 0xFFFFFAD0
#define HCAN0_MD5	__PORT8 0xFFFFF8D8	/* Message Data 5 */
#define HCAN1_MD5	__PORT8 0xFFFFFAD8
#define HCAN0_MD6	__PORT8 0xFFFFF8E0	/* Message Data 6 */
#define HCAN1_MD6	__PORT8 0xFFFFFAE0
#define HCAN0_MD7	__PORT8 0xFFFFF8E8	/* Message Data 7 */
#define HCAN1_MD7	__PORT8 0xFFFFFAE8
#define HCAN0_MD8	__PORT8 0xFFFFF8F0	/* Message Data 8 */
#define HCAN1_MD8	__PORT8 0xFFFFFAF0
#define HCAN0_MD9	__PORT8 0xFFFFF8F8	/* Message Data 9 */
#define HCAN1_MD9	__PORT8 0xFFFFFAF8
#define HCAN0_MD10	__PORT8 0xFFFFF900	/* Message Data 10 */
#define HCAN1_MD10	__PORT8 0xFFFFFB00
#define HCAN0_MD11	__PORT8 0xFFFFF908	/* Message Data 11 */
#define HCAN1_MD11	__PORT8 0xFFFFFB08
#define HCAN0_MD12	__PORT8 0xFFFFF910	/* Message Data 12 */
#define HCAN1_MD12	__PORT8 0xFFFFFB10
#define HCAN0_MD13	__PORT8 0xFFFFF918	/* Message Data 13 */
#define HCAN1_MD13	__PORT8 0xFFFFFB18
#define HCAN0_MD14	__PORT8 0xFFFFF920	/* Message Data 14 */
#define HCAN1_MD14	__PORT8 0xFFFFFB20
#define HCAN0_MD15	__PORT8 0xFFFFF928	/* Message Data 15 */
#define HCAN1_MD15	__PORT8 0xFFFFFB28

/* Motor control PWM timer 1 */
#define PWM_PWCR1 __PORT8 0xFFFFFC00	    /* PWM control register 1 */
#define   PWCR1_CKS0m	0x01 
#define   PWCR1_CKS1m	0x02 
#define   PWCR1_CKS2m	0x04 
#define   PWCR1_CKS_F1	0x00 
#define   PWCR1_CKS_F2	0x01
#define   PWCR1_CKS_F4	0x02 
#define   PWCR1_CKS_F8	0x03 
#define   PWCR1_CKS_F16	0x07 
#define   PWCR1_CSTm	0x08 
#define   PWCR1_CMFm	0x10 
#define   PWCR1_IEm	0x20 
#define PWM_PWOCR1	__PORT8 0xFFFFFC02	/* PWM Output Control Register 1 */  
#define   PWOCR1_OE1Am	0x01 
#define   PWOCR1_OE1Bm	0x02 
#define   PWOCR1_OE1Cm	0x04 
#define   PWOCR1_OE1Dm	0x08 
#define   PWOCR1_OE1Em	0x10 
#define   PWOCR1_OE1Fm	0x20 
#define   PWOCR1_OE1Gm	0x40 
#define   PWOCR1_OE1Hm	0x80 
#define PWM_PWPR1	__PORT8 0xFFFFFC04	/* PWM Polarity Register 1 */ 
#define   PWPR1_OPS1Am	0x01 
#define   PWPR1_OPS1Bm	0x02 
#define   PWPR1_OPS1Cm	0x04 
#define   PWPR1_OPS1Dm	0x08 
#define   PWPR1_OPS1Em	0x10 
#define   PWPR1_OPS1Fm	0x20 
#define   PWPR1_OPS1Gm	0x40 
#define   PWPR1_OPS1Hm	0x80 
#define PWM_PWCYR1	__PORT16 0xFFFFFC06	/* PWM Cycle Register 1 */ 
#define PWM_PWBFR1A	__PORT16 0xFFFFFC08	/* PWM Buffer Register 1A */ 
#define   PWBFR1A_DT8m	0x0100 
#define   PWBFR1A_DT9m	0x0200 
#define   PWBFR1A_DTxm	0x03ff 
#define   PWBFR1A_OTSm	0x1000 
#define PWM_PWBFR1C	__PORT16 0xFFFFFC0A	/* PWM Buffer Register 1C */ 
#define   PWBFR1C_DT8m	0x0100 
#define   PWBFR1C_DT9m	0x0200 
#define   PWBFR1C_DTxm	0x03ff 
#define   PWBFR1C_OTSm	0x1000 
#define PWM_PWBFR1E	__PORT16 0xFFFFFC0C	/* PWM Buffer Register 1E */ 
#define   PWBFR1E_DT8m	0x0100 
#define   PWBFR1E_DT9m	0x0200 
#define   PWBFR1E_DTxm	0x03ff 
#define   PWBFR1E_OTSm	0x1000 
#define PWM_PWBFR1G	__PORT16 0xFFFFFC0E	/* PWM Buffer Register 1G */ 
#define   PWBFR1G_DT8m	0x0100 
#define   PWBFR1G_DT9m	0x0200 
#define   PWBFR1G_DTxm	0x03ff 
#define   PWBFR1G_OTSm	0x1000 
/* Motor control PWM timer 2 */
#define PWM_PWCR2	__PORT8 0xFFFFFC10	/* PWM Control Register 2 */ 
#define   PWCR2_CKS0m	0x01 
#define   PWCR2_CKS1m	0x02 
#define   PWCR2_CKS2m	0x04 
#define   PWCR2_CKS_F1	0x00 
#define   PWCR2_CKS_F2	0x01
#define   PWCR2_CKS_F4	0x02 
#define   PWCR2_CKS_F8	0x03 
#define   PWCR2_CKS_F16	0x07 
#define   PWCR2_CSTm	0x08 
#define   PWCR2_CMFm	0x10 
#define   PWCR2_IEm	0x20 
#define PWM_PWOCR2	__PORT8 0xFFFFFC12	/* PWM Output Control Register 2 */ 
#define   PWOCR2_OE2Am	0x01 
#define   PWOCR2_OE2Bm	0x02 
#define   PWOCR2_OE2Cm	0x04 
#define   PWOCR2_OE2Dm	0x08 
#define   PWOCR2_OE2Em	0x10 
#define   PWOCR2_OE2Fm	0x20 
#define   PWOCR2_OE2Gm	0x40 
#define   PWOCR2_OE2Hm	0x80 
#define PWM_PWPR2	__PORT8 0xFFFFFC14	/* PWM Polarity Register 2 */ 
#define   PWPR2_OPS2Am	0x01 
#define   PWPR2_OPS2Bm	0x02 
#define   PWPR2_OPS2Cm	0x04 
#define   PWPR2_OPS2Dm	0x08 
#define   PWPR2_OPS2Em	0x10 
#define   PWPR2_OPS2Fm	0x20 
#define   PWPR2_OPS2Gm	0x40 
#define   PWPR2_OPS2Hm	0x80 
#define PWM_PWCYR2	__PORT16 0xFFFFFC16	/* PWM Cycle Register 2 */ 
#define PWM_PWBFR2A	__PORT16 0xFFFFFC18	/* PWM Buffer Register 2A */
#define   PWBFR2A_DT8m	0x0100 
#define   PWBFR2A_DT9m	0x0200 
#define   PWBFR2A_DTxm	0x03ff 
#define   PWBFR2A_TDSm	0x1000 
#define PWM_PWBFR2B	__PORT16 0xFFFFFC1A	/* PWM Buffer Register 2B */ 
#define   PWBFR2B_DT8m	0x0100 
#define   PWBFR2B_DT9m	0x0200 
#define   PWBFR2B_DTxm	0x03ff 
#define   PWBFR2B_TDSm	0x1000 
#define PWM_PWBFR2C	__PORT16 0xFFFFFC1C	/* PWM Buffer Register 2C */
#define   PWBFR2C_DT8m	0x0100 
#define   PWBFR2C_DT9m	0x0200 
#define   PWBFR2C_DTxm	0x03ff 
#define   PWBFR2C_TDSm	0x1000 
#define PWM_PWBFR2D	__PORT16 0xFFFFFC1E	/* PWM Buffer Register 2E */ 
#define   PWBFR2D_DT8m	0x0100 
#define   PWBFR2D_DT9m	0x0200 
#define   PWBFR2D_DTxm	0x03ff 
#define   PWBFR2D_TDSm	0x1000 
/* Port H and J Registers */
#define DIO_PHDDR	__PORT8 0xFFFFFC20	/* DIO H Data Direction Register */ 
#define   PHDDR_PH0DDRm	0x01 
#define   PHDDR_PH1DDRm	0x02 
#define   PHDDR_PH2DDRm	0x04 
#define   PHDDR_PH3DDRm	0x08 
#define   PHDDR_PH4DDRm	0x10 
#define   PHDDR_PH5DDRm	0x20 
#define   PHDDR_PH6DDRm	0x40 
#define   PHDDR_PH7DDRm	0x80 
#define DIO_PJDDR	__PORT8 0xFFFFFC21	/* DIO J Data Direction Register */ 
#define   PJDDR_PJ0DDRm	0x01 
#define   PJDDR_PJ1DDRm	0x02 
#define   PJDDR_PJ2DDRm	0x04 
#define   PJDDR_PJ3DDRm	0x08 
#define   PJDDR_PJ4DDRm	0x10 
#define   PJDDR_PJ5DDRm	0x20 
#define   PJDDR_PJ6DDRm	0x40 
#define   PJDDR_PJ7DDRm	0x80 
#define DIO_PHDR	__PORT8 0xFFFFFC24	/* DIO H Data Register */ 
#define   PHDR_PH0DRm	0x01 
#define   PHDR_PH1DRm	0x02 
#define   PHDR_PH2DRm	0x04 
#define   PHDR_PH3DRm	0x08 
#define   PHDR_PH4DRm	0x10 
#define   PHDR_PH5DRm	0x20 
#define   PHDR_PH6DRm	0x40 
#define   PHDR_PH7DRm	0x80 
#define DIO_PJDR	__PORT8 0xFFFFFC25	/* DIO J Data Register */ 
#define   PJDR_PJ0DRm	0x01 
#define   PJDR_PJ1DRm	0x02 
#define   PJDR_PJ2DRm	0x04 
#define   PJDR_PJ3DRm	0x08 
#define   PJDR_PJ4DRm	0x10 
#define   PJDR_PJ5DRm	0x20 
#define   PJDR_PJ6DRm	0x40 
#define   PJDR_PJ7DRm	0x80 
#define DIO_PORTH	__PORT8 0xFFFFFC28	/* DIO H Register */ 
#define   PORTH_PH0m	0x01 
#define   PORTH_PH1m	0x02 
#define   PORTH_PH2m	0x04 
#define   PORTH_PH3m	0x08 
#define   PORTH_PH4m	0x10 
#define   PORTH_PH5m	0x20 
#define   PORTH_PH6m	0x40 
#define   PORTH_PH7m	0x80 
#define DIO_PORTJ	__PORT8 0xFFFFFC29	/* DIO J Register */ 
#define   PORTJ_PJ0m	0x01 
#define   PORTJ_PJ1m	0x02 
#define   PORTJ_PJ2m	0x04 
#define   PORTJ_PJ3m	0x08 
#define   PORTJ_PJ4m	0x10 
#define   PORTJ_PJ5m	0x20 
#define   PORTJ_PJ6m	0x40 
#define   PORTJ_PJ7m	0x80 

/* Module IIC valid in 2630,2638 and 2639 */
#define IIC_SCRX	__PORT8 0xFFFFFDB4	/* Serial Control Register X */ 
#define   SCRX_IICEm	0x10 
#define   SCRX_IICX0m	0x20 
#define   SCRX_IICX1m	0x40 
#define IIC_DDCSWR	__PORT8 0xFFFFFDB5	/* DDC Switch Register */ 
#define   DDCSWR_CLR0m  0x01 
#define   DDCSWR_CLR1m  0x02 
#define   DDCSWR_CLR2m  0x04 
#define   DDCSWR_CLR3m  0x08 
#define   DDCSWR_IFm    0x10
#define   DDCSWR_IEm    0x20
#define   DDCSWR_SWm    0x40
#define   DDCSWR_SWEm   0x80
/* Module System */
#define SYS_SBYCR	__PORT8 0xFFFFFDE4	/* Standby Control Register */ 
#define   SBYCR_OPEm	0x08 
#define   SBYCR_STS0m	0x10 
#define   SBYCR_STS1m	0x20 
#define   SBYCR_STS2m	0x40 
#define   SBYCR_SSBYm	0x80 
#define SYS_SYSCR	__PORT8 0xFFFFFDE5	/* SYS Control Register */ 
#define   SYSCR_RAMEm	0x01 
#define   SYSCR_NMIEGm	0x08 
#define   SYSCR_INTM0m	0x10 
#define   SYSCR_INTM1m	0x20 
#define   SYSCR_MACSm	0x80 
#define SYS_SCKCR	__PORT8 0xFFFFFDE6	/* SYS Clock Control Register */ 
#define   SCKCR_SCK0m	0x01			/*  Bus master clock selection */ 
#define   SCKCR_SCK1m	0x02 			/*  0=full, 1=/2, 2=/4 3=/8 */
#define   SCKCR_SCK2m	0x04 			/*  4=/16, 5=/32 */
#define   SCKCR_SCKxm	0x07
#define   SCKCR_STCSm	0x08			/*   1=Immediately change, 0=at Stby */ 
#define   SCKCR_PSTOPm	0x80			/*   1=Clock Output Disable */ 
#define SYS_MDCR	__PORT8 0xFFFFFDE7	/* Mode Control Register */ 
#define   MDCR_MDS0m	0x01 
#define   MDCR_MDS1m	0x02 
#define   MDCR_MDS2m	0x04 
#define SYS_PFCR	__PORT8 0xFFFFFDEB	/* Pin Function Control Register */ 
#define   PFCR_AE0m	0x01 
#define   PFCR_AE1m	0x02 
#define   PFCR_AE2m	0x04 
#define   PFCR_AE3m	0x08 
#define   PFCR_AExm	0x0f 
#define SYS_LPWRCR	__PORT8 0xFFFFFDEC	/* Low-Power Control Register */ 
#define   LPWRCR_STC0m	0x01 
#define   LPWRCR_STC1m	0x02 
#define   LPWRCR_STCxm	0x03
#define   LPWRCR_RFCUTm	0x08 
#define   LPWRCR_SUBSTPm	0x10 
#define   LPWRCR_NESELm	0x20 
#define   LPWRCR_LSONm	0x40 
#define   LPWRCR_DTONm	0x80 
/* Module PC Break Controller */
#define PBC_BARA	__PORT32 0xFFFFFE00	/* Break Address Register A */ 
#define PBC_BARB	__PORT32 0xFFFFFE04	/* Break Address Register B */ 
#define PBC_BCRA	__PORT8 0xFFFFFE08	/* Break Control Register A */ 
#define   BCRA_BIEAm	0x01 
#define   BCRA_CSELA0m	0x02 
#define   BCRA_CSELA1m	0x04 
#define   BCRA_BAMRA0m	0x08 
#define   BCRA_BAMRA1m	0x10 
#define   BCRA_BAMRA2m	0x20 
#define   BCRA_CDAm	0x40 
#define   BCRA_CMFAm	0x80 
#define PBC_BCRB	__PORT8 0xFFFFFE09	/* Break Control Register B */ 
#define   BCRB_BIEAm	0x01 
#define   BCRB_CSELA0m	0x02 
#define   BCRB_CSELA1m	0x04 
#define   BCRB_BAMRA0m	0x08 
#define   BCRB_BAMRA1m	0x10 
#define   BCRB_BAMRA2m	0x20 
#define   BCRB_CDAm	0x40 
#define   BCRB_CMFAm	0x80 
/* Module Interrupt Controller Registers */ 
#define INT_ISCRH	__PORT8 0xFFFFFE12	/* IRQ Sence Control Register H */ 
#define   ISCRH_IRQ4SCAm	0x01 
#define   ISCRH_IRQ4SCBm	0x02 
#define   ISCRH_IRQ5SCAm	0x04 
#define   ISCRH_IRQ5SCBm	0x08 
#define INT_ISCRL	__PORT8 0xFFFFFE13	/* IRQ Sence Control Register L */ 
#define   ISCRL_IRQ0SCAm	0x01 
#define   ISCRL_IRQ0SCBm	0x02 
#define   ISCRL_IRQ1SCAm	0x04 
#define   ISCRL_IRQ1SCBm	0x08 
#define   ISCRL_IRQ2SCAm	0x10 
#define   ISCRL_IRQ2SCBm	0x20 
#define   ISCRL_IRQ3SCAm	0x40 
#define   ISCRL_IRQ3SCBm	0x80 
#define INT_IER	__PORT8 0xFFFFFE14	/* IRQ Enable Register */ 
#define   IER_IRQ0Em	0x01 
#define   IER_IRQ1Em	0x02 
#define   IER_IRQ2Em	0x04 
#define   IER_IRQ3Em	0x08 
#define   IER_IRQ4Em	0x10 
#define   IER_IRQ5Em	0x20 
#define INT_ISR	__PORT8 0xFFFFFE15	/* IRQ Status Register */ 
#define   ISR_IRQ0Fm	0x01 
#define   ISR_IRQ1Fm	0x02 
#define   ISR_IRQ2Fm	0x04 
#define   ISR_IRQ3Fm	0x08 
#define   ISR_IRQ4Fm	0x10 
#define   ISR_IRQ5Fm	0x20 

#define DTC_DTCERA	__PORT8 0xFFFFFE16	/* DTC Enable Register A */ 
#define   DTCERA_DTCEA0m	0x01 
#define   DTCERA_DTCEA1m	0x02 
#define   DTCERA_DTCEA2m	0x04 
#define   DTCERA_DTCEA3m	0x08 
#define   DTCERA_DTCEA4m	0x10 
#define   DTCERA_DTCEA5m	0x20 
#define   DTCERA_DTCEA6m	0x40 
#define   DTCERA_DTCEA7m	0x80 
#define DTC_DTCERB	__PORT8 0xFFFFFE17	/* DTC Enable Register B */ 
#define   DTCERB_DTCEB0m	0x01 
#define   DTCERB_DTCEB1m	0x02 
#define   DTCERB_DTCEB2m	0x04 
#define   DTCERB_DTCEB3m	0x08 
#define   DTCERB_DTCEB4m	0x10 
#define   DTCERB_DTCEB5m	0x20 
#define   DTCERB_DTCEB6m	0x40 
#define   DTCERB_DTCEB7m	0x80 
#define DTC_DTCERC	__PORT8 0xFFFFFE18	/* DTC Enable Register C */ 
#define   DTCERC_DTCEC0m	0x01 
#define   DTCERC_DTCEC1m	0x02 
#define   DTCERC_DTCEC2m	0x04 
#define   DTCERC_DTCEC3m	0x08 
#define   DTCERC_DTCEC4m	0x10 
#define   DTCERC_DTCEC5m	0x20 
#define   DTCERC_DTCEC6m	0x40 
#define   DTCERC_DTCEC7m	0x80 
#define DTC_DTCERD	__PORT8 0xFFFFFE19	/* DTC Enable Register D */ 
#define   DTCERD_DTCED0m	0x01 
#define   DTCERD_DTCED1m	0x02 
#define   DTCERD_DTCED2m	0x04 
#define   DTCERD_DTCED3m	0x08 
#define   DTCERD_DTCED4m	0x10 
#define   DTCERD_DTCED5m	0x20 
#define   DTCERD_DTCED6m	0x40 
#define   DTCERD_DTCED7m	0x80 
#define DTC_DTCERE	__PORT8 0xFFFFFE1A	/* DTC Enable Register E */ 
#define   DTCERE_DTCEE0m	0x01 
#define   DTCERE_DTCEE1m	0x02 
#define   DTCERE_DTCEE2m	0x04 
#define   DTCERE_DTCEE3m	0x08 
#define   DTCERE_DTCEE4m	0x10 
#define   DTCERE_DTCEE5m	0x20 
#define   DTCERE_DTCEE6m	0x40 
#define   DTCERE_DTCEE7m	0x80 
#define DTC_DTCERF	__PORT8 0xFFFFFE1B	/* DTC Enable Register F */ 
#define   DTCERF_DTCEF0m	0x01 
#define   DTCERF_DTCEF1m	0x02 
#define   DTCERF_DTCEF2m	0x04 
#define   DTCERF_DTCEF3m	0x08 
#define   DTCERF_DTCEF4m	0x10 
#define   DTCERF_DTCEF5m	0x20 
#define   DTCERF_DTCEF6m	0x40 
#define   DTCERF_DTCEF7m	0x80 
#define DTC_DTCERG	__PORT8 0xFFFFFE1C	/* DTC Enable Register G */ 
#define   DTCERG_DTCEG0m	0x01 
#define   DTCERG_DTCEG1m	0x02 
#define   DTCERG_DTCEG2m	0x04 
#define   DTCERG_DTCEG3m	0x08 
#define   DTCERG_DTCEG4m	0x10 
#define   DTCERG_DTCEG5m	0x20 
#define   DTCERG_DTCEG6m	0x40 
#define   DTCERG_DTCEG7m	0x80 
#define DTC_DTVECR	__PORT8 0xFFFFFE1F	/* DTC Vector Register */ 
#define   DTVECR_DTVEC0m	0x01 
#define   DTVECR_DTVEC1m	0x02 
#define   DTVECR_DTVEC2m	0x04 
#define   DTVECR_DTVEC3m	0x08 
#define   DTVECR_DTVEC4m	0x10 
#define   DTVECR_DTVEC5m	0x20 
#define   DTVECR_DTVEC6m	0x40 
#define   DTVECR_SWDTEm	0x80 
/* Module Programmable Pulse Generator */
#define PPG_PCR	__PORT8 0xFFFFFE26	/* PPG Output Control Register */ 
#define   PCR_G0CMS0m	0x01 
#define   PCR_G0CMS1m	0x02 
#define   PCR_G1CMS0m	0x04 
#define   PCR_G1CMS1m	0x08 
#define   PCR_G2CMS0m	0x10 
#define   PCR_G2CMS1m	0x20 
#define   PCR_G3CMS0m	0x40 
#define   PCR_G3CMS1m	0x80 
#define PPG_PMR	__PORT8 0xFFFFFE27	/* PPG Output Mode Register */ 
#define   PMR_G0NOVm	0x01 
#define   PMR_G1NOVm	0x02 
#define   PMR_G2NOVm	0x04 
#define   PMR_G3INVm	0x08 
#define   PMR_G0INVm	0x10 
#define   PMR_G1INVm	0x20 
#define   PMR_G2INVm	0x40 
#define   PMM_G3INVm	0x80 
#define PPG_NDERH	__PORT8 0xFFFFFE28	/* Next Data Enable Register H */ 
#define   NDERH_NDER8m	0x01 
#define   NDERH_NDER9m	0x02 
#define   NDERH_NDER10m	0x04 
#define   NDERH_NDER11m	0x08 
#define   NDERH_NDER12m	0x10 
#define   NDERH_NDER13m	0x20 
#define   NDERH_NDER14m	0x40 
#define   NDERH_NDER15m	0x80 
#define PPG_NDERL	__PORT8 0xFFFFFE29	/* Next Data Enable Register L */ 
#define   NDERL_NDER0m	0x01 
#define   NDERL_NDER1m	0x02 
#define   NDERL_NDER2m	0x04 
#define   NDERL_NDER3m	0x08 
#define   NDERL_NDER4m	0x10 
#define   NDERL_NDER5m	0x20 
#define   NDERL_NDER6m	0x40 
#define   NDERL_NDER7m	0x80 
#define PPG_PODRH	__PORT8 0xFFFFFE2A	/* Output Data Register H */ 
#define   PODRH_POD8m	0x01 
#define   PODRH_POD9m	0x02 
#define   PODRH_POD10m	0x04 
#define   PODRH_POD11m	0x08 
#define   PODRH_POD12m	0x10 
#define   PODRH_POD13m	0x20 
#define   PODRH_POD14m	0x40 
#define   PODRH_POD15m	0x80 
#define PPG_PODRL	__PORT8 0xFFFFFE2B	/* Output Data Register L */ 
#define   PODRL_POD0m	0x01 
#define   PODRL_POD1m	0x02 
#define   PODRL_POD2m	0x04 
#define   PODRL_POD3m	0x08 
#define   PODRL_POD4m	0x10 
#define   PODRL_POD5m	0x20 
#define   PODRL_POD6m	0x40 
#define   PODRL_POD7m	0x80 
//#define PPG_NDRH	__PORT8 0xFFFFFE2C	/* Next data register H */  /* Use when group2 and group3 have the same output trigger selected */
#define   NDRH_NDR8m	0x01			/* Use for group2 if group3 have different output triger to group2 */
#define   NDRH_NDR9m	0x02 
#define   NDRH_NDR10m	0x04 
#define   NDRH_NDR11m	0x08 
#define   NDRH_NDR12m	0x10 
#define   NDRH_NDR13m	0x20 
#define   NDRH_NDR14m	0x40 
#define   NDRH_NDR15m	0x80 
//#define PPG_NDRL	__PORT8 0xFFFFFE2D	/* Next data register L */  /* Use when group2 and group3 have the same output trigger selected */
#define   NDRL_NDR0m	0x01 			/* Use for group2 if group3 have different output triger to group2 */
#define   NDRL_NDR1m	0x02 
#define   NDRL_NDR2m	0x04 
#define   NDRL_NDR3m	0x08 
#define   NDRL_NDR4m	0x10 
#define   NDRL_NDR5m	0x20 
#define   NDRL_NDR6m	0x40 
#define   NDRL_NDR7m	0x80 
//#define PPG_NDRH	__PORT8 0xFFFFFE2E	/* Next Data Register H */ /* Use for group3 if group2 and group3 have different triggers */
#define   NDRH_NDR8m	0x01 
#define   NDRH_NDR9m	0x02 
#define   NDRH_NDR10m	0x04 
#define   NDRH_NDR11m	0x08 
//#define PPG_NDRL	__PORT8 0xFFFFFE2F	/* Next Data Register L */ /* Use for group3 if group2 and group3 have different triggers */
#define   NDRL_NDR0m	0x01 
#define   NDRL_NDR1m	0x02 
#define   NDRL_NDR2m	0x04 
#define   NDRL_NDR3m	0x08 
/* Module Port */
#define DIO_P1DDR	__PORT8 0xFFFFFE30	/* DIO 1 Data Direction Register */ 
#define   P1DDR_P10DDRm	0x01 
#define   P1DDR_P11DDRm	0x02 
#define   P1DDR_P12DDRm	0x04 
#define   P1DDR_P13DDRm	0x08 
#define   P1DDR_P14DDRm	0x10 
#define   P1DDR_P15DDRm	0x20 
#define   P1DDR_P16DDRm	0x40 
#define   P1DDR_P17DDRm	0x80 
#define DIO_P3DDR	__PORT8 0xFFFFFE32	/* DIO 3 Data Direction Register */ 
#define   P3DDR_P30DDRm	0x01 
#define   P3DDR_P31DDRm	0x02 
#define   P3DDR_P32DDRm	0x04 
#define   P3DDR_P33DDRm	0x08 
#define   P3DDR_P34DDRm	0x10 
#define   P3DDR_P35DDRm	0x20 
#define DIO_PADDR	__PORT8 0xFFFFFE39	/* DIO A Data Direction Register */ 
#define   PADDR_PA0DDRm	0x01 
#define   PADDR_PA1DDRm	0x02 
#define   PADDR_PA2DDRm	0x04 
#define   PADDR_PA3DDRm	0x08 
#define DIO_PBDDR	__PORT8 0xFFFFFE3A	/* DIO B Data Direction Register */ 
#define   PBDDR_PB0DDRm	0x01 
#define   PBDDR_PB1DDRm	0x02 
#define   PBDDR_PB2DDRm	0x04 
#define   PBDDR_PB3DDRm	0x08 
#define   PBDDR_PB4DDRm	0x10 
#define   PBDDR_PB5DDRm	0x20 
#define   PBDDR_PB6DDRm	0x40 
#define   PBDDR_PB7DDRm	0x80 
#define DIO_PCDDR	__PORT8 0xFFFFFE3B	/* DIO C Data Direction Register */ 
#define   PCDDR_PC0DDRm	0x01 
#define   PCDDR_PC1DDRm	0x02 
#define   PCDDR_PC2DDRm	0x04 
#define   PCDDR_PC3DDRm	0x08 
#define   PCDDR_PC4DDRm	0x10 
#define   PCDDR_PC5DDRm	0x20 
#define   PCDDR_PC6DDRm	0x40 
#define   PCDDR_PC7DDRm	0x80 
#define DIO_PDDDR	__PORT8 0xFFFFFE3C	/* DIO D Data Direction Register */ 
#define   PDDDR_PD0DDRm	0x01 
#define   PDDDR_PD1DDRm	0x02 
#define   PDDDR_PD2DDRm	0x04 
#define   PDDDR_PD3DDRm	0x08 
#define   PDDDR_PD4DDRm	0x10 
#define   PDDDR_PD5DDRm	0x20 
#define   PDDDR_PD6DDRm	0x40 
#define   PDDDR_PD7DDRm	0x80 
#define DIO_PEDDR	__PORT8 0xFFFFFE3D	/* DIO E Data Direction Register */ 
#define   PEDDR_PE0DDRm	0x01 
#define   PEDDR_PE1DDRm	0x02 
#define   PEDDR_PE2DDRm	0x04 
#define   PEDDR_PE3DDRm	0x08 
#define   PEDDR_PE4DDRm	0x10 
#define   PEDDR_PE5DDRm	0x20 
#define   PEDDR_PE6DDRm	0x40 
#define   PEDDR_PE7DDRm	0x80 
#define DIO_PFDDR	__PORT8 0xFFFFFE3E	/* DIO F Data Direction Register */ 
#define   PFDDR_PF0DDRm	0x01 
#define   PFDDR_PF3DDRm	0x08 
#define   PFDDR_PF4DDRm	0x10 
#define   PFDDR_PF5DDRm	0x20 
#define   PFDDR_PF6DDRm	0x40 
#define   PFDDR_PF7DDRm	0x80 
#define DIO_PAPCR	__PORT8 0xFFFFFE40	/* DIO A MOS Pull-Up Control Register */ 
#define   PAPCR_PA0PCRm	0x01 
#define   PAPCR_PA1PCRm	0x02 
#define   PAPCR_PA2PCRm	0x04 
#define   PAPCR_PA3PCRm	0x08 
#define DIO_PBPCR	__PORT8 0xFFFFFE41	/* DIO B MOS Pull-Up Control Register */ 
#define   PBPCR_PB0PCRm	0x01 
#define   PBPCR_PB1PCRm	0x02 
#define   PBPCR_PB2PCRm	0x04 
#define   PBPCR_PB3PCRm	0x08 
#define   PBPCR_PB4PCRm	0x10 
#define   PBPCR_PB5PCRm	0x20 
#define   PBPCR_PB6PCRm	0x40 
#define   PBPCR_PB7PCRm	0x80 
#define DIO_PCPCR	__PORT8 0xFFFFFE42	/* DIO C MOS Pull-Up Control Register */ 
#define   PCPCR_PC0PCRm	0x01 
#define   PCPCR_PC1PCRm	0x02 
#define   PCPCR_PC2PCRm	0x04 
#define   PCPCR_PC3PCRm	0x08 
#define   PCPCR_PC4PCRm	0x10 
#define   PCPCR_PC5PCRm	0x20 
#define   PCPCR_PC6PCRm	0x40 
#define   PCPCR_PC7PCRm	0x80 
#define DIO_PDPCR	__PORT8 0xFFFFFE43	/* DIO D MOS Pull-Up Control Register */ 
#define   PDPCR_PD0PCRm	0x01 
#define   PDPCR_PD1PCRm	0x02 
#define   PDPCR_PD2PCRm	0x04 
#define   PDPCR_PD3PCRm	0x08 
#define   PDPCR_PD4PCRm	0x10 
#define   PDPCR_PD5PCRm	0x20 
#define   PDPCR_PD6PCRm	0x40 
#define   PDPCR_PD7PCRm	0x80 
#define DIO_PEPCR	__PORT8 0xFFFFFE44	/* DIO E MOS Pull-Up Control Register */ 
#define   PEPCR_PE0PCRm	0x01 
#define   PEPCR_PE1PCRm	0x02 
#define   PEPCR_PE2PCRm	0x04 
#define   PEPCR_PE3PCRm	0x08 
#define   PEPCR_PE4PCRm	0x10 
#define   PEPCR_PE5PCRm	0x20 
#define   PEPCR_PE6PCRm	0x40 
#define   PEPCR_PE7PCRm	0x80 
#define DIO_P3ODR	__PORT8 0xFFFFFE46	/* DIO 3 Open Drain Control Register */ 
#define   P3ODR_P30ODRm	0x01 
#define   P3ODR_P31ODRm	0x02 
#define   P3ODR_P32ODRm	0x04 
#define   P3ODR_P33ODRm	0x08 
#define   P3ODR_P34ODRm	0x10 
#define   P3ODR_P35ODRm	0x20 
#define DIO_PAODR	__PORT8 0xFFFFFE47	/* DIO A Open Drain Control Register */ 
#define   PAODR_PA0ODRm	0x01 
#define   PAODR_PA1ODRm	0x02 
#define   PAODR_PA2ODRm	0x04 
#define   PAODR_PA3ODRm	0x08 
#define DIO_PBODR	__PORT8 0xFFFFFE48	/* DIO B Open Drain Control Register */ 
#define   PBODR_PB0ODRm	0x01 
#define   PBODR_PB1ODRm	0x02 
#define   PBODR_PB2ODRm	0x04 
#define   PBODR_PB3ODRm	0x08 
#define   PBODR_PB4ODRm	0x10 
#define   PBODR_PB5ODRm	0x20 
#define   PBODR_PB6ODRm	0x40 
#define   PBODR_PB7ODRm	0x80 
#define DIO_PCODR	__PORT8 0xFFFFFE49	/* DIO C Open Drain Control Register */ 
#define   PCODR_PC0ODRm	0x01 
#define   PCODR_PC1ODRm	0x02 
#define   PCODR_PC2ODRm	0x04 
#define   PCODR_PC3ODRm	0x08 
#define   PCODR_PC4ODRm	0x10 
#define   PCODR_PC5ODRm	0x20 
#define   PCODR_PC6ODRm	0x40 
#define   PCODR_PC7ODRm	0x80 
/* Module Time pulse unit */
#define TPU_TCR3	__PORT8 0xFFFFFE80	/* Timer Control Register 3 */ 
#define   TCR3_TPSC0m	0x01 
#define   TCR3_TPSC1m	0x02 
#define   TCR3_TPSC2m	0x04 
#define   TCR3_CKEG0m	0x08 
#define   TCR3_CKEG1m	0x10 
#define   TCR3_CCLR0m	0x20 
#define   TCR3_CCLR1m	0x40 
#define   TCR3_CCLR2m	0x80 
#define TPU_TMDR3	__PORT8 0xFFFFFE81	/* Timer Mode Register 3 */ 
#define   TMDR3_MD0m	0x01 
#define   TMDR3_MD1m	0x02 
#define   TMDR3_MD2m	0x04 
#define   TMDR3_MD3m	0x08 
#define   TMDR3_BFAm	0x10 
#define   TMDR3_BFBm	0x20 
#define TPU_TIOR3H	__PORT8 0xFFFFFE82	/* Timer IO Control Register 3H */ 
#define   TIOR3H_IOA0m	0x01 
#define   TIOR3H_IOA1m	0x02 
#define   TIOR3H_IOA2m	0x04 
#define   TIOR3H_IOA3m	0x08 
#define   TIOR3H_IOB0m	0x10 
#define   TIOR3H_IOB1m	0x20 
#define   TIOR3H_IOB2m	0x40 
#define   TIOR3H_IOB3m	0x80 
#define TPU_TIOR3L	__PORT8 0xFFFFFE83	/* Timer IO Control Register 3L */ 
#define   TIOR3L_IOC0m	0x01 
#define   TIOR3L_IOC1m	0x02 
#define   TIOR3L_IOC2m	0x04 
#define   TIOR3L_IOC3m	0x08 
#define   TIOR3L_IOD0m	0x10 
#define   TIOR3L_IOD1m	0x20 
#define   TIOR3L_IOD2m	0x40 
#define   TIOR3L_IOD3m	0x80 
#define TPU_TIER3	__PORT8 0xFFFFFE84	/* -Timer INT Enable Register 3 */ 
#define   TIER3_TGIEAm	0x01 
#define   TIER3_TGIEBm	0x02 
#define   TIER3_TGIECm	0x04 
#define   TIER3_TGIEDm	0x08 
#define   TIER3_TCIEVm	0x10 
#define   TIER3_TTGEm	0x80 
#define TPU_TSR3	__PORT8 0xFFFFFE85	/* Timer Status Register 3 */ 
#define   TSR3_TGFAm	0x01 
#define   TSR3_TGFBm	0x02 
#define   TSR3_TGFCm	0x04 
#define   TSR3_TGFDm	0x08 
#define   TSR3_TCFVm	0x10 
#define TPU_TCNT3	__PORT16 0xFFFFFE86	/* Timer Counter 3 */ 
#define TPU_TGR3A	__PORT16 0xFFFFFE88	/* Timer General Register 3A */ 
#define TPU_TGR3B	__PORT16 0xFFFFFE8A	/* Timer General Register 3B */ 
#define TPU_TGR3C	__PORT16 0xFFFFFE8C	/* Timer General Register 3C */ 
#define TPU_TGR3D	__PORT16 0xFFFFFE8E	/* Timer General Register 3D */ 
#define TPU_TCR4	__PORT8 0xFFFFFE90	/* Timer Control Register 4 */ 
#define   TCR4_TPSC0m	0x01 
#define   TCR4_TPSC1m	0x02 
#define   TCR4_TPSC2m	0x04 
#define   TCR4_CKEG0m	0x08 
#define   TCR4_CKEG1m	0x10 
#define   TCR4_CCLR0m	0x20 
#define   TCR4_CCLR1m	0x40 
#define TPU_TMDR4	__PORT8 0xFFFFFE91	/* Timer Mode Register 4 */ 
#define   TMDR4_MD0m	0x01 
#define   TMDR4_MD1m	0x02 
#define   TMDR4_MD2m	0x04 
#define   TMDR4_MD3m	0x08 
#define TPU_TIOR4	__PORT8 0xFFFFFE92	/* Timer IO Control Register 4 */ 
#define   TIOR4_IOA0m	0x01 
#define   TIOR4_IOA1m	0x02 
#define   TIOR4_IOA2m	0x04 
#define   TIOR4_IOA3m	0x08 
#define   TIOR4_IOB0m	0x10 
#define   TIOR4_IOB1m	0x20 
#define   TIOR4_IOB2m	0x40 
#define   TIOR4_IOB3m	0x80 
#define TPU_TIER4	__PORT8 0xFFFFFE94	/* Timer INT Enable Register 4 */ 
#define   TIER4_TGIEAm	0x01 
#define   TIER4_TGIEBm	0x02 
#define   TIER4_TCIEVm	0x10 
#define   TIER4_TCIEUm	0x20 
#define   TIER4_TTGEm	0x80 
#define TPU_TSR4	__PORT8 0xFFFFFE95	/* Timer Status Register 4 */ 
#define   TSR4_TGFAm	0x01 
#define   TSR4_TGFBm	0x02 
#define   TSR4_TCFVm	0x10 
#define   TSR4_TCFUm	0x20 
#define   TSR4_TCFDm	0x80 
#define TPU_TCNT4	__PORT16 0xFFFFFE96	/* Timer Counter 4 */ 
#define TPU_TGR4A	__PORT16 0xFFFFFE98	/* Timer General Register 4A */ 
#define TPU_TGR4B	__PORT16 0xFFFFFE9A	/* Timer General Register 4B */ 
#define TPU_TCR5	__PORT8 0xFFFFFEA0	/* Timer Control Register 5 */ 
#define   TCR5_TPSC0m	0x01 
#define   TCR5_TPSC1m	0x02 
#define   TCR5_TPSC2m	0x04 
#define   TCR5_CKEG0m	0x08 
#define   TCR5_CKEG1m	0x10 
#define   TCR5_CCLR0m	0x20 
#define   TCR5_CCLR1m	0x40 
#define TPU_TMDR5	__PORT8 0xFFFFFEA1	/* Timer Mode Register 5 */ 

#define TPU_TIOR5	__PORT8 0xFFFFFEA2	/* Timer IO Control Register 5 */ 
#define   TIOR5_IOA0m	0x01 
#define   TIOR5_IOA1m	0x02 
#define   TIOR5_IOA2m	0x04 
#define   TIOR5_IOA3m	0x08 
#define   TIOR5_IOB0m	0x10 
#define   TIOR5_IOB1m	0x20 
#define   TIOR5_IOB2m	0x40 
#define   TIOR5_IOB3m	0x80 
#define TPU_TIER5	__PORT8 0xFFFFFEA4	/* Timer INT Enable Register 5 */ 
#define   TIER5_TGIEAm	0x01 
#define   TIER5_TGIEBm	0x02 
#define   TIER5_TCIEVm	0x10 
#define   TIER5_TCIEUm	0x20 
#define   TIER5_TTGEm	0x80 
#define TPU_TSR5	__PORT8 0xFFFFFEA5	/* Timer Status Register 5 */ 
#define   TSR5_TGFAm	0x01 
#define   TSR5_TGFBm	0x02 
#define   TSR5_TCFVm	0x10 
#define   TSR5_TCFUm	0x20 
#define   TSR5_TCFDm	0x80 
#define TPU_TCNT5	__PORT16 0xFFFFFEA6	/* Timer Counter 5 */ 
#define TPU_TGR5A	__PORT16 0xFFFFFEA8	/* Timer General Register 5A */ 
#define TPU_TGR5B	__PORT16 0xFFFFFEAA	/* Timer General Register 5B */ 
#define TPU_TSTR	__PORT8 0xFFFFFEB0	/* Timer Start Register */ 
#define   TSTR_CST0m	0x01 
#define   TSTR_CST1m	0x02 
#define   TSTR_CST2m	0x04 
#define   TSTR_CST3m	0x08 
#define   TSTR_CST4m	0x10 
#define   TSTR_CST5m	0x20 
#define TPU_TSYR	__PORT8 0xFFFFFEB1	/* Timer Synchro Register */ 
#define   TSYR_SYNC0m	0x01 
#define   TSYR_SYNC1m	0x02 
#define   TSYR_SYNC2m	0x04 
#define   TSYR_SYNC3m	0x08 
#define   TSYR_SYNC4m	0x10 
#define   TSYR_SYNC5m	0x20 
/* Module Interrupt */
#define INT_IPRA	__PORT8 0xFFFFFEC0	/* Interrupt Priority Register A */
#define   IPRA_IPR0m	0x01 
#define   IPRA_IPR1m	0x02 
#define   IPRA_IPR2m	0x04 
#define   IPRA_IPR4m	0x10 
#define   IPRA_IPR5m	0x20 
#define   IPRA_IPR6m	0x40 
#define INT_IPRB	__PORT8 0xFFFFFEC1	/* Interrupt Priority Register B */
#define   IPRB_IPR0m	0x01 
#define   IPRB_IPR1m	0x02 
#define   IPRB_IPR2m	0x04 
#define   IPRB_IPR4m	0x10 
#define   IPRB_IPR5m	0x20 
#define   IPRB_IPR6m	0x40 
#define INT_IPRC	__PORT8 0xFFFFFEC2	/* Interrupt Priority Register C */
#define   IPRC_IPR0m	0x01 
#define   IPRC_IPR1m	0x02 
#define   IPRC_IPR2m	0x04 
#define INT_IPRD	__PORT8 0xFFFFFEC3	/* Interrupt Priority Register D */
#define   IPRD_IPR4m	0x10 
#define   IPRD_IPR5m	0x20 
#define   IPRD_IPR6m	0x40 
#define INT_IPRE	__PORT8 0xFFFFFEC4	/* Interrupt Priority Register E */
#define   IPRE_IPR0m	0x01 
#define   IPRE_IPR1m	0x02 
#define   IPRE_IPR2m	0x04 
#define   IPRE_IPR4m	0x10 
#define   IPRE_IPR5m	0x20 
#define   IPRE_IPR6m	0x40 
#define INT_IPRF	__PORT8 0xFFFFFEC5	/* Interrupt Priority Register F */
#define   IPRF_IPR0m	0x01 
#define   IPRF_IPR1m	0x02 
#define   IPRF_IPR2m	0x04 
#define   IPRF_IPR4m	0x10 
#define   IPRF_IPR5m	0x20 
#define   IPRF_IPR6m	0x40 
#define INT_IPRG	__PORT8 0xFFFFFEC6	/* Interrupt Priority Register G */
#define   IPRG_IPR0m	0x01 
#define   IPRG_IPR1m	0x02 
#define   IPRG_IPR2m	0x04 
#define   IPRG_IPR4m	0x10 
#define   IPRG_IPR5m	0x20 
#define   IPRG_IPR6m	0x40 
#define INT_IPRH	__PORT8 0xFFFFFEC7	/* Interrupt Priority Register H */
#define   IPRH_IPR0m	0x01 
#define   IPRH_IPR1m	0x02 
#define   IPRH_IPR2m	0x04 
#define   IPRH_IPR4m	0x10 
#define   IPRH_IPR5m	0x20 
#define   IPRH_IPR6m	0x40 
#define INT_IPRJ	__PORT8 0xFFFFFEC9	/* Interrupt Priority Register J */
#define   IPRJ_IPR0m	0x01 
#define   IPRJ_IPR1m	0x02 
#define   IPRJ_IPR2m	0x04 
#define INT_IPRK	__PORT8 0xFFFFFECA	/* Interrupt Priority Register K */
#define   IPRK_IPR0m	0x01 
#define   IPRK_IPR1m	0x02 
#define   IPRK_IPR2m	0x04 
#define   IPRK_IPR4m	0x10 
#define   IPRK_IPR5m	0x20 
#define   IPRK_IPR6m	0x40 
#define INT_IPRM	__PORT8 0xFFFFFECC	/* Interrupt Priority Register M */ 
#define   IPRM_IPR0m	0x01 
#define   IPRM_IPR1m	0x02 
#define   IPRM_IPR2m	0x04 
#define   IPRM_IPR4m	0x10 
#define   IPRM_IPR5m	0x20 
#define   IPRM_IPR6m	0x40 
/* Module BUS controler */ 
#define BUS_ABWCR	__PORT8 0xFFFFFED0	/* Bus Width Control Register */ 
#define   ABWCR_ABW0m	0x01 
#define   ABWCR_ABW1m	0x02 
#define   ABWCR_ABW2m	0x04 
#define   ABWCR_ABW3m	0x08 
#define   ABWCR_ABW4m	0x10 
#define   ABWCR_ABW5m	0x20 
#define   ABWCR_ABW6m	0x40 
#define   ABWCR_ABW7m	0x80 
#define BUS_ASTCR	__PORT8 0xFFFFFED1	/* Access State Control Register */ 
#define   ASTCR_AST0m	0x01 
#define   ASTCR_AST1m	0x02 
#define   ASTCR_AST2m	0x04 
#define   ASTCR_AST3m	0x08 
#define   ASTCR_AST4m	0x10 
#define   ASTCR_AST5m	0x20 
#define   ASTCR_AST6m	0x40 
#define   ASTCR_AST7m	0x80 
#define BUS_WCRH	__PORT8 0xFFFFFED2	/* Wait Control Register H */ 
#define   WCRH_W40m	0x01 
#define   WCRH_W41m	0x02 
#define   WCRH_W50m	0x04 
#define   WCRH_W51m	0x08 
#define   WCRH_W60m	0x10 
#define   WCRH_W61m	0x20 
#define   WCRH_W70m	0x40 
#define   WCRH_W71m	0x80 
#define BUS_WCRL	__PORT8 0xFFFFFED3	/* Wait Control Register L */ 
#define   WCRL_W00m	0x01 
#define   WCRL_W01m	0x02 
#define   WCRL_W10m	0x04 
#define   WCRL_W11m	0x08 
#define   WCRL_W20m	0x10 
#define   WCRL_W21m	0x20 
#define   WCRL_W30m	0x40 
#define   WCRL_W31m	0x80 
#define BUS_BCRH	__PORT8 0xFFFFFED4	/* Bus Control Register H */ 
#define   BCRH_BRSTS0m	0x08 
#define   BCRH_BRSTS1m	0x10 
#define   BCRH_BRSTRMm	0x20 
#define   BCRH_ICIS0m	0x40 
#define   BCRH_ICIS1m	0x80 
#define BUS_BCRL	__PORT8 0xFFFFFED5	/* Bus Control Register L */ 
#define   BCRL_WDBEm	0x02 
/* Module Flash */
#define FLM_RAMER	__PORT8 0xFFFFFEDB	/* RAM Emulation Register */ 
#define   RAMER_RAM0m	0x01 
#define   RAMER_RAM1m	0x02 
#define   RAMER_RAM2m	0x04
#define   RAMER_RAMxm	0x07  
#define   RAMER_RAMSm	0x08 

/* Module Port */
#define DIO_P1DR	__PORT8 0xFFFFFF00	/* DIO 1 Data Register */ 
#define   P1DR_P10DRm	0x01 
#define   P1DR_P11DRm	0x02 
#define   P1DR_P12DRm	0x04 
#define   P1DR_P13DRm	0x08 
#define   P1DR_P14DRm	0x10 
#define   P1DR_P15DRm	0x20 
#define   P1DR_P16DRm	0x40 
#define   P1DR_P17DRm	0x80 
#define DIO_P3DR	__PORT8 0xFFFFFF02	/* DIO 3 Data Register */ 
#define   P3DR_P30DRm	0x01 
#define   P3DR_P31DRm	0x02 
#define   P3DR_P32DRm	0x04 
#define   P3DR_P33DRm	0x08 
#define   P3DR_P34DRm	0x10 
#define   P3DR_P35DRm	0x20 
#define DIO_PADR	__PORT8 0xFFFFFF09	/* DIO A Data Register */ 
#define   PADR_PA0DRm	0x01 
#define   PADR_PA1DRm	0x02 
#define   PADR_PA2DRm	0x04 
#define   PADR_PA3DRm	0x08 
#define DIO_PBDR	__PORT8 0xFFFFFF0A	/* DIO B Data Register */ 
#define   PBDR_PB0DRm	0x01 
#define   PBDR_PB1DRm	0x02 
#define   PBDR_PB2DRm	0x04 
#define   PBDR_PB3DRm	0x08 
#define   PBDR_PB4DRm	0x10 
#define   PBDR_PB5DRm	0x20 
#define   PBDR_PB6DRm	0x40 
#define   PBDR_PB7DRm	0x80 
#define DIO_PCDR	__PORT8 0xFFFFFF0B	/* DIO C Data Register */ 
#define   PCDR_PC0DRm	0x01 
#define   PCDR_PC1DRm	0x02 
#define   PCDR_PC2DRm	0x04 
#define   PCDR_PC3DRm	0x08 
#define   PCDR_PC4DRm	0x10 
#define   PCDR_PC5DRm	0x20 
#define   PCDR_PC6DRm	0x40 
#define   PCDR_PC7DRm	0x80 
#define DIO_PDDR	__PORT8 0xFFFFFF0C	/* DIO D Data Register */ 
#define   PDDR_PD0DRm	0x01 
#define   PDDR_PD1DRm	0x02 
#define   PDDR_PD2DRm	0x04 
#define   PDDR_PD3DRm	0x08 
#define   PDDR_PD4DRm	0x10 
#define   PDDR_PD5DRm	0x20 
#define   PDDR_PD6DRm	0x40 
#define   PDDR_PD7DRm	0x80 
#define DIO_PEDR	__PORT8 0xFFFFFF0D	/* DIO E Data Register */ 
#define   PEDR_PE0DRm	0x01 
#define   PEDR_PE1DRm	0x02 
#define   PEDR_PE2DRm	0x04 
#define   PEDR_PE3DRm	0x08 
#define   PEDR_PE4DRm	0x10 
#define   PEDR_PE5DRm	0x20 
#define   PEDR_PE6DRm	0x40 
#define   PEDR_PE7DRm	0x80 
#define DIO_PFDR	__PORT8 0xFFFFFF0E	/* DIO F Data Register */ 
#define   PFDR_PF0DRm	0x01 
#define   PFDR_PF1DRm	0x02 
#define   PFDR_PF2DRm	0x04 
#define   PFDR_PF3DRm	0x08 
#define   PFDR_PF4DRm	0x10 
#define   PFDR_PF5DRm	0x20 
#define   PFDR_PF6DRm	0x40 
#define   PFDR_PF7DRm	0x80 

/* Module Time pulse unit */  //see other definitions at the end of the file
#define TPU_TCR0	__PORT8 0xFFFFFF10	/* Timer Control Register 0 */ 
#define   TCR0_TPSC0m	0x01 
#define   TCR0_TPSC1m	0x02 
#define   TCR0_TPSC2m	0x04 
#define   TCR0_CKEG0m	0x08 
#define   TCR0_CKEG1m	0x10 
#define   TCR0_CCLR0m	0x20 
#define   TCR0_CCLR1m	0x40 
#define   TCR0_CCLR2m	0x80 
#define TPU_TMDR0	__PORT8 0xFFFFFF11	/* Timer Mode Register 0 */ 
#define   TMDR0_MD0m	0x01 
#define   TMDR0_MD1m	0x02 
#define   TMDR0_MD2m	0x04 
#define   TMDR0_MD3m	0x08 
#define   TMDR0_BFAm	0x10 
#define   TMDR0_BFBm	0x20 
#define TPU_TIOR0H	__PORT8 0xFFFFFF12	/* Timer IO Control Register 0H */ 
#define   TIOR0H_IOA0m	0x01 
#define   TIOR0H_IOA1m	0x02 
#define   TIOR0H_IOA2m	0x04 
#define   TIOR0H_IOA3m	0x08 
#define   TIOR0H_IOB0m	0x10 
#define   TIOR0H_IOB1m	0x20 
#define   TIOR0H_IOB2m	0x40 
#define   TIOR0H_IOB3m	0x80 
#define TPU_TIOR0L	__PORT8 0xFFFFFF13	/* Timer IO Control Register 0L */ 
#define   TIOR0L_IOC0m	0x01 
#define   TIOR0L_IOC1m	0x02 
#define   TIOR0L_IOC2m	0x04 
#define   TIOR0L_IOC3m	0x08 
#define   TIOR0L_IOD0m	0x10 
#define   TIOR0L_IOD1m	0x20 
#define   TIOR0L_IOD2m	0x40 
#define   TIOR0L_IOD3m	0x80 
#define TPU_TIER0	__PORT8 0xFFFFFF14	/* Timer INT Enable Register 0 */ 
#define   TIER0_TGIEAm	0x01 
#define   TIER0_TGIEBm	0x02 
#define   TIER0_TGIECm	0x04 
#define   TIER0_TGIEDm	0x08 
#define   TIER0_TCIEVm	0x10 
#define   TIER0_TTGEm	0x80 
#define TPU_TSR0	__PORT8 0xFFFFFF15	/* Timer Status Register 0 */ 
#define   TSR0_TGFAm	0x01 
#define   TSR0_TGFBm	0x02 
#define   TSR0_TGFCm	0x04 
#define   TSR0_TGFDm	0x08 
#define   TSR0_TCFVm	0x10 
#define TPU_TCNT0	__PORT16 0xFFFFFF16	/* Timer Counter 0 */ 
#define TPU_TGR0A	__PORT16 0xFFFFFF18	/* Timer General Register 0A */ 
#define TPU_TGR0B	__PORT16 0xFFFFFF1A	/* Timer General Register 0B */ 
#define   by_standbym	0x02 
#define   data_tom	0x02 
#define   data_tom	0x02 
#define   to_slavem	0x02 
#define TPU_TGR0C	__PORT16 0xFFFFFF1C	/* Timer General Register 0C */ 
#define TPU_TGR0D	__PORT16 0xFFFFFF1E	/* Timer General Register 0D */ 
#define TPU_TCR1	__PORT8 0xFFFFFF20	/* Timer Control Register 1 */ 
#define   TCR1_TPSC0m	0x01 
#define   TCR1_TPSC1m	0x02 
#define   TCR1_TPSC2m	0x04 
#define   TCR1_CKEG0m	0x08 
#define   TCR1_CKEG1m	0x10 
#define   TCR1_CCLR0m	0x20 
#define   TCR1_CCLR1m	0x40 
#define TPU_TMDR1	__PORT8 0xFFFFFF21	/* Timer Mode Register 1 */ 
#define   TMDR1_MD0m	0x01 
#define   TMDR1_MD1m	0x02 
#define   TMDR1_MD2m	0x04 
#define   TMDR1_MD3m	0x08 
#define TPU_TIOR1	__PORT8 0xFFFFFF22	/* Timer IO Control Register 1 */ 
#define   TIOR1_IOA0m	0x01 
#define   TIOR1_IOA1m	0x02 
#define   TIOR1_IOA2m	0x04 
#define   TIOR1_IOA3m	0x08 
#define   TIOR1_IOB0m	0x10 
#define   TIOR1_IOB1m	0x20 
#define   TIOR1_IOB2m	0x40 
#define   TIOR1_IOB3m	0x80 
#define TPU_TIER1	__PORT8 0xFFFFFF24	/* Timer INT Enable Register 1 */ 
#define   TIER1_TGIEAm	0x01 
#define   TIER1_TGIEBm	0x02 
#define   TIER1_TCIEVm	0x10 
#define   TIER1_TCIEUm	0x20 
#define   TIER1_TTGEm	0x80 
#define TPU_TSR1	__PORT8 0xFFFFFF25	/* Timer Status Register 1 */ 
#define   TSR1_TGFAm	0x01 
#define   TSR1_TGFBm	0x02 
#define   TSR1_TCFVm	0x10 
#define   TSR1_TCFUm	0x20 
#define   TSR1_TCFDm	0x80 
#define TPU_TCNT1	__PORT16 0xFFFFFF26	/* Timer Counter 1 */ 
#define TPU_TGR1A	__PORT16 0xFFFFFF28	/* Timer General Register 1A */ 
#define TPU_TGR1B	__PORT16 0xFFFFFF2A	/* Timer General Register 1B */ 
#define TPU_TCR2	__PORT8 0xFFFFFF30	/* Timer Control Register 2 */ 
#define   TCR2_TPSC0m	0x01 
#define   TCR2_TPSC1m	0x02 
#define   TCR2_TPSC2m	0x04 
#define   TCR2_CKEG0m	0x08 
#define   TCR2_CKEG1m	0x10 
#define   TCR2_CCLR0m	0x20 
#define   TCR2_CCLR1m	0x40 
#define TPU_TMDR2	__PORT8 0xFFFFFF31	/* Timer Mode Register 2 */ 
#define   TMDR2_MD0m	0x01 
#define   TMDR2_MD1m	0x02 
#define   TMDR2_MD2m	0x04 
#define   TMDR2_MD3m	0x08 
#define TPU_TIOR2	__PORT8 0xFFFFFF32	/* Timer IO Control Register 2 */ 
#define   TIOR2_IOA0m	0x01 
#define   TIOR2_IOA1m	0x02 
#define   TIOR2_IOA2m	0x04 
#define   TIOR2_IOA3m	0x08 
#define   TIOR2_IOB0m	0x10 
#define   TIOR2_IOB1m	0x20 
#define   TIOR2_IOB2m	0x40 
#define   TIOR2_IOB3m	0x80 
#define TPU_TIER2	__PORT8 0xFFFFFF34	/* Timer INT Enable Register 2 */ 
#define   TIER2_TGIEAm	0x01 
#define   TIER2_TGIEBm	0x02 
#define   TIER2_TCIEVm	0x10 
#define   TIER2_TCIEUm	0x20 
#define   TIER2_TTGEm	0x80 
#define TPU_TSR2	__PORT8 0xFFFFFF35	/* Timer Status Register 2 */ 
#define   TSR2_TGFAm	0x01 
#define   TSR2_TGFBm	0x02 
#define   TSR2_TCFVm	0x10 
#define   TSR2_TCFUm	0x20 
#define   TSR2_TCFDm	0x80 
#define TPU_TCNT2	__PORT16 0xFFFFFF36	/* Timer Counter 2 */ 
#define TPU_TGR2A	__PORT16 0xFFFFFF38	/* Timer General Register 2A */ 
#define TPU_TGR2B	__PORT16 0xFFFFFF3A	/* Timer General Register 2B */ 

/* Module Watchdog timer */
/* WDT0 register definitions start */
#define WDT_WTCSR0r	__PORT8 0xFFFFFF74	/* Timer ControlStatus Register 0 (RD/WC7) */ 
#define WDT_WTCSR0w	__PORT16 0xFFFFFF74	/*   writte address - password 0xa500  */ 
#define   WTCSR0_CKS0m	0x01 
#define   WTCSR0_CKS1m	0x02 
#define   WTCSR0_CKS2m	0x04 
#define   WTCSR0_CKSxm	0x07
#define   WTCSR0_TMEm	0x20 
#define   WTCSR0_WTITm	0x40 
#define   WTCSR0_WOVFm	0x80 
#define WDT_WTCNT0r	__PORT8 0xFFFFFF75	/* Timer Counter 0 (RD) */ 
#define WDT_WTCNT0w	__PORT16 0xFFFFFF74	/*   writte address - password 0x5a00  */ 
#define WDT_WRSTCSRr	__PORT8 0xFFFFFF77	/* Reset ControlStatus Register (RD/WC7) */ 
#define WDT_WRSTCSRw	__PORT16 0xFFFFFF76	/*   clear WOVF - password 0xa500 */ 
						/*   set bits   - password 0x5a00 */ 
#define   WRSTCSR_RSTSm	0x20 
#define   WRSTCSR_RSTEm	0x40 
#define   WRSTCSR_WOVFm	0x80 
/* WDT0 register definitions end */

/* SCI common registers and bits start */

/*   Receive Data Register (RDR) */
/*   Transmit Data Register (TDR) */
/*   Serial Mode Register (SMR) */
#define   SMR_CKS0m	0x01
#define   SMR_CKS1m	0x02
#define   SMR_CKSxm	0x03 	/* Clock 3=/64, 2=/16, 1=/4, 0=/1 */
#define   SMR_MPm	0x04 	/* 1=Multiprocessor format selected */
#define   SMR_STOPm	0x08 	/* 1=2 stop bits, 0=1 stop bit */
#define   SMR_OEm	0x10 	/* 1=Odd parity, 0=Even */
#define   SMR_PEm	0x20 	/* 1=Parity addition and checking enabled */
#define   SMR_CHRm	0x40 	/* 1=7-bit data, 0=8-bit */
#define   SMR_CAm	0x80 	/* 1=Clocked, 0=Asynchronous */
#define	  SCI_SMR_8N1	(0|0|0)
#define	  SCI_SMR_7N1	(SMR_CHRm|0|0)
#define	  SCI_SMR_8N2	(0       |0|SMR_STOPm)
#define	  SCI_SMR_7N2	(SMR_CHRm|0|SMR_STOPm)
#define	  SCI_SMR_8E1	(0       |SMR_PEm|0)
#define	  SCI_SMR_7E1	(SMR_CHRm|SMR_PEm|0)
#define	  SCI_SMR_8O1	(0       |SMR_PEm|SMR_OEm)
#define	  SCI_SMR_7O1	(SMR_CHRm|SMR_PEm|SMR_OEm)
/*   Serial Control Register (SCR) */
#define   SCR_CKE0m	0x01 	/* Clock Enable */
#define   SCR_CKE1m	0x02 	/*  */
#define   SCR_TEIEm	0x04 	/* Transmit end interrupt (TEI) */
#define   SCR_MPIEm	0x08 	/* Only multiprocessor RXI interrupt enabled */
#define   SCR_REm	0x10 	/* Reception enabled */
#define   SCR_TEm	0x20 	/* Transmission enabled* */
#define   SCR_RIEm	0x40 	/* RXI interrupt requests enabled */ 
#define   SCR_TIEm	0x80	/* TXI interrupt requests enabled */ 
/*   Serial Status Register (SSR) */
#define   SSR_MPBTm	0x01 	/* Value to send as bit 8  */
#define   SSR_MPBm	0x02 	/* MP Bit 8 received value */
#define   SSR_TENDm	0x04 	/* Transmit End */
#define   SSR_PERm	0x08 	/* Parity error */
#define   SSR_FERm	0x10 	/* Framing error */
#define   SSR_ORERm	0x20 	/* Receive overflow */
#define   SSR_RDRFm	0x40 	/* Set when reception ends normally */
#define   SSR_TDREm	0x80 	/* Set when TDR empty or SCR_TE=0 */
/*   Bit Rate Register (BRR) */
/*     for async set to N=Fsys/(32*2^(2n)*baud)-1  where n=SMR_CKS */
/*     for sync set to  N=Fsys/(4*2^(2n)*baud)-1 */
/*   Smart Card Mode Register (SCMR) */
#define   SCMR_SMIFm	0x01 	/* 1=Smart card interface enabled */
#define   SCMR_SINVm	0x04 	/* 1=TDR contents inverted */
#define   SCMR_SDIRm	0x08 	/* 1=MSB-first, 0=LSB-first */
/*   I2C Bus Mode / Slave Address Register (ICMR/SAR)*/ 
/*     only for SCI0 and SCI1 */
#define   ICMR_BC0m	0x01	/* Bit Counter */
#define   ICMR_BC1m	0x02 
#define   ICMR_BC2m	0x04 
#define   ICMR_BCm	(ICMR_BC0m|ICMR_BC1m|ICMR_BC2m)
#define   ICMR_CKS0m	0x08	/* Serial Clock Select */
#define   ICMR_CKS1m	0x10 
#define   ICMR_CKS2m	0x20 
#define   ICMR_CKSm	(ICMR_CKS0m|ICMR_CKS1m|ICMR_CKS2m) 
#define   ICMR_WAITm	0x40	/* 1 .. Wait between data and acknowledge */
#define   ICMR_MLSm	0x80	/* 0 .. MSB-first / 1 .. LSB-first */ 
/*   I2C Bus Control Register (ICCR) */
#define   ICCR_SCPm	0x01	/* Write 0 with BBSY to start/stop */
#define   ICCR_IRICm	0x02	/* 1 => interrupt requested */
#define   ICCR_BBSYm	0x04	/* 1 => bus is busy */
#define   ICCR_ACKEm	0x08	/* 1 => stop when no ACK detected */
#define   ICCR_TRSm	0x10	/* 1 .. transmit / 0 .. receive */
#define   ICCR_MSTm	0x20	/* 1 .. master mode / 0 .. slave mode */
#define   ICCR_IEICm	0x40	/* Interrupts enabled */
#define   ICCR_ICEm	0x80	/* 1 .. IIC enabled (ICMR,ICDR accessible) */
				/* 0 .. IIC disabled (SAR,SARX accessible) */
/*   IIC Bus Status Register (ICSR) */
#define   ICSR_ACKBm    0x01	/* Acknowledge Bit */
#define   ICSR_ADZm     0x02	/* General Call Address Recognition */
#define   ICSR_AASm     0x04	/* Slave Address Recognition */
#define   ICSR_ALm      0x08	/* Arbitration Lost */
#define   ICSR_AASXm    0x10	/* Second Slave Address Recognition */
#define   ICSR_IRTRm    0x20	/* Continuous Transmission/Reception Interrupt */
#define   ICSR_STOPm    0x40	/* Normal Stop Condition Detection Flag */
#define   ICSR_ESTPm    0x80	/* Error Stop Condition Detection Flag */

/* SCI common registers and bits end */


#define SCI_SMR0	__PORT8 0xFFFFFF78	/* Serial Mode Register 0 */ 
#define   SMR0_CKS0m	0x01 
#define   SMR0_CKS1m	0x02 
#define   SMR0_MPm	0x04 
#define   SMR0_STOPm	0x08 
#define   SMR0_OEm	0x10 
#define   SMR0_PEm	0x20 
#define   SMR0_CHRm	0x40 
#define   SMR0_CAm	0x80 
#define Smart_SMR0	__PORT8 0xFFFFFF78	/* Smart Card Mode Register 0 */ 
#define IIC_ICCR0	__PORT8 0xFFFFFF78	/* I2C Bus Control Register */
#define SCI_BRR0	__PORT8 0xFFFFFF79	/* Bit Rate Register 0 */ 
#define Smart_BRR0	__PORT8 0xFFFFFF79	/* Bit Rate Register 0 */
#define IIC_ICSR0	__PORT8 0xFFFFFF79	/* I2C Bus Status Register */ 
#define SCI_SCR0	__PORT8 0xFFFFFF7A	/* Serial Control Register 0 */ 
#define Smart_SCR0	__PORT8 0xFFFFFF7A	/* Serial Control Register 0 */ 
#define   SCR0_CKE0m	0x01 
#define   SCR0_CKE1m	0x02 
#define   SCR0_TEIEm	0x04 
#define   SCR0_MPIEm	0x08 
#define   SCR0_REm	0x10 
#define   SCR0_TEm	0x20 
#define   SCR0_RIEm	0x40 
#define   SCR0_TIEm	0x80 
#define SCI_TDR0	__PORT8 0xFFFFFF7B	/* Transmit Data Register 0 */ 
#define Smart_TDR0	__PORT8 0xFFFFFF7B	/* Transmit Data Register 0 */ 
#define SCI_SSR0	__PORT8 0xFFFFFF7C	/* Serial Status Register 0 */ 
#define   SSR0_MPBTm	0x01 
#define   SSR0_MPBm	0x02 
#define   SSR0_TENDm	0x04 
#define   SSR0_PERm	0x08 
#define   SSR0_FERm	0x10 
#define   SSR0_ORERm	0x20 
#define   SSR0_RDRFm	0x40 
#define   SSR0_TDREm	0x80 
#define Smart_SSR0	__PORT8 0xFFFFFF7C	/* Serial Status Register 0 */ 
#define SCI_RDR0	__PORT8 0xFFFFFF7D	/* Receive Data Register 0 */ 
#define SCI_SCMR0	__PORT8 0xFFFFFF7E	/* Smart Card Mode Register 0 */ 
#define   SCMR0_SMIFm	0x01 
#define   SCMR0_SINVm	0x04 
#define   SCMR0_SDIRm	0x08 
#define Smart_SCMR0	__PORT8 0xFFFFFF7E	/* Smart Card Mode Register 0 */
#define IIC_ICDR0	__PORT8 0xFFFFFF7E	/* I2C Bus Data Register */ 
#define IIC_SARX0	__PORT8 0xFFFFFF7E	/* 2nd Slave Address Register */ 
#define IIC_ICMR0	__PORT8 0xFFFFFF7F	/* I2C Bus Mode Register */ 
#define   ICMR0_BC0FSm	0x01 
#define   ICMR0_BC1m	0x02 
#define   ICMR0_BC2m	0x04 
#define   ICMR0_CKS0m	0x08 
#define   ICMR0_CKS1m	0x10 
#define   ICMR0_CKS2m	0x20 
#define   ICMR0_WAITm	0x40 
#define   ICMR0_MLSm	0x80 
#define IIC_SAR0	__PORT8 0xFFFFFF7F	/* Slave Address Register */ 
#define SCI_SMR1	__PORT8 0xFFFFFF80	/* Serial Mode Register 1 */
#define   SMR1_CKS0m	0x01 
#define   SMR1_CKS1m	0x02 
#define   SMR1_MPm	0x04 
#define   SMR1_STOPm	0x08 
#define   SMR1_OEm	0x10 
#define   SMR1_PEm	0x20 
#define   SMR1_CHRm	0x40 
#define   SMR1_CAm	0x80  
#define IIC_ICCR1	__PORT8 0xFFFFFF80	/* I2C Bus Control Register */ 
#define Smart_SMR1	__PORT8 0xFFFFFF80	/* Serial Mode Register 1 */ 
#define SCI_BRR1	__PORT8 0xFFFFFF81	/* Bit Rate Register 1 */ 
#define Smart_BRR1	__PORT8 0xFFFFFF81	/* Bit Rate Register 1 */ 
#define IIC_ICSR1	__PORT8 0xFFFFFF81	/* I2C Bus Status Register */ 
#define SCI_SCR1	__PORT8 0xFFFFFF82	/* Serial Control Register 1 */ 
#define   SCR1_CKE0m	0x01 
#define   SCR1_CKE1m	0x02 
#define   SCR1_TEIEm	0x04 
#define   SCR1_MPIEm	0x08 
#define   SCR1_REm	0x10 
#define   SCR1_TEm	0x20 
#define   SCR1_RIEm	0x40 
#define   SCR1_TIEm	0x80 
#define Smart_SCR1	__PORT8 0xFFFFFF82	/* Serial Control Register 1 */ 
#define SCI_TDR1	__PORT8 0xFFFFFF83	/* Transmit Data Register 1 */ 
#define Smart_TDR1	__PORT8 0xFFFFFF83	/* Transmit Data Register 1 */ 
#define SCI_SSR1	__PORT8 0xFFFFFF84	/* Serial Status Register 1 */ 
#define   SSR1_MPBTm	0x01 
#define   SSR1_MPBm	0x02 
#define   SSR1_TENDm	0x04 
#define   SSR1_PERm	0x08 
#define   SSR1_FERm	0x10 
#define   SSR1_ORERm	0x20 
#define   SSR1_RDRFm	0x40 
#define   SSR1_TDREm	0x80
#define Smart_SSR1	__PORT8 0xFFFFFF84	/* Serial Status Register 1 */ 
#define SCI_RDR1	__PORT8 0xFFFFFF85	/* Receive Data Register 1 */ 
#define Smart_RDR1	__PORT8 0xFFFFFF85	/* Receive Data Register 1 */ 
#define SCI_SCMR1	__PORT8 0xFFFFFF86	/* Smart Card Mode Register 1 */ 
#define   SCMR1_SMIFm	0x01 
#define   SCMR1_SINVm	0x04 
#define   SCMR1_SDIRm	0x08 
#define IIC_ICDR1	__PORT8 0xFFFFFF86	/* I2C Bus Data Register */ 
#define IIC_SARX1	__PORT8 0xFFFFFF86	/* 2nd Slave Address Register */ 
#define IIC_ICMR1	__PORT8 0xFFFFFF87	/* -I2C Bus Mode Register */ 
#define   ICMR1_BC0FSm	0x01 
#define   ICMR1_BC1m	0x02 
#define   ICMR1_BC2m	0x04 
#define   ICMR1_CKS0m	0x08 
#define   ICMR1_CKS1m	0x10 
#define   ICMR1_CKS2m	0x20 
#define   ICMR1_WAITm	0x40 
#define   ICMR1_MLSm	0x80 
#define IIC_SAR1	__PORT8 0xFFFFFF87	/* Slave Address Register */ 
#define SCI_SMR2	__PORT8 0xFFFFFF88	/* Serial Mode Register 2 */ 
#define   SMR2_CKS0m	0x01 
#define   SMR2_CKS1m	0x02 
#define   SMR2_MPm	0x04 
#define   SMR2_STOPm	0x08 
#define   SMR2_OEm	0x10 
#define   SMR2_PEm	0x20 
#define   SMR2_CHRm	0x40 
#define   SMR2_CAm	0x80 
#define Smart_SMR2	__PORT8 0xFFFFFF88	/* Serial Mode Register 2 */ 
#define SCI_BRR2	__PORT8 0xFFFFFF89	/* Bit Rate Register 2 */ 
#define Smart_BRR2	__PORT8 0xFFFFFF89	/* Bit Rate Register 2 */ 
#define SCI_SCR2	__PORT8 0xFFFFFF8A	/* Serial Control Register 2 */ 
#define   SCR2_CKE0m	0x01 
#define   SCR2_CKE1m	0x02 
#define   SCR2_TEIEm	0x04 
#define   SCR2_MPIEm	0x08 
#define   SCR2_REm	0x10 
#define   SCR2_TEm	0x20 
#define   SCR2_RIEm	0x40 
#define   SCR2_TIEm	0x80 
#define Smart_SCR2	__PORT8 0xFFFFFF8A	/* Serial Control Register 2 */ 
#define SCI_TDR2	__PORT8 0xFFFFFF8B	/* Transmit Data Register 2 */ 
#define Smart_TDR2	__PORT8 0xFFFFFF8B	/* Transmit Data Register 2 */ 
#define SCI_SSR2	__PORT8 0xFFFFFF8C	/* Serial Status Register 2 */ 
#define   SSR2_MPBTm	0x01 
#define   SSR2_MPBm	0x02 
#define   SSR2_TENDm	0x04 
#define   SSR2_PERm	0x08 
#define   SSR2_FERm	0x10 
#define   SSR2_ORERm	0x20 
#define   SSR2_RDRFm	0x40 
#define   SSR2_TDREm	0x80 
#define Smart_SSR2	__PORT8 0xFFFFFF8C	/* Serial Status Register 2 */ 
#define SCI_RDR2	__PORT8 0xFFFFFF8D	/* Receive Data Register 2 */ 
#define SCI_SCMR2	__PORT8 0xFFFFFF8E	/* Smart Card Mode Register 2 */ 
#define SCI_SCMR2	__PORT8 0xFFFFFF8E	/* Smart Card Mode Register 2 */ 
#define   SCMR2_SMIFm	0x01 
#define   SCMR2_SINVm	0x04 
#define   SCMR2_SDIRm	0x08

/* Module A/D Converter */
#define AD_ADDRAH	__PORT8 0xFFFFFF90	/* AD Data Register AH */ 
#define   ADDRAH_AD2m	0x01 
#define   ADDRAH_AD3m	0x02 
#define   ADDRAH_AD4m	0x04 
#define   ADDRAH_AD5m	0x08 
#define   ADDRAH_AD6m	0x10 
#define   ADDRAH_AD7m	0x20 
#define   ADDRAH_AD8m	0x40 
#define   ADDRAH_AD9m	0x80 
#define AD_ADDRAL	__PORT8 0xFFFFFF91	/* AD Data Register AL*/ 
#define   ADDRAL_AD0m	0x40 
#define   ADDRAL_AD1m	0x80 
#define AD_ADDRBH	__PORT8 0xFFFFFF92	/* AD Data Register BH*/ 
#define   ADDRBH_AD2m	0x01 
#define   ADDRBH_AD3m	0x02 
#define   ADDRBH_AD4m	0x04 
#define   ADDRBH_AD5m	0x08 
#define   ADDRBH_AD6m	0x10 
#define   ADDRBH_AD7m	0x20 
#define   ADDRBH_AD8m	0x40 
#define   ADDRBH_AD9m	0x80 
#define AD_ADDRBL	__PORT8 0xFFFFFF93	/* AD Data Register BL*/ 
#define   ADDRBL_AD0m	0x40 
#define   ADDRBL_AD1m	0x80 
#define AD_ADDRCH	__PORT8 0xFFFFFF94	/* AD Data Register CH */ 
#define   ADDRCH_AD2m	0x01 
#define   ADDRCH_AD3m	0x02 
#define   ADDRCH_AD4m	0x04 
#define   ADDRCH_AD5m	0x08 
#define   ADDRCH_AD6m	0x10 
#define   ADDRCH_AD7m	0x20 
#define   ADDRCH_AD8m	0x40 
#define   ADDRCH_AD9m	0x80 
#define AD_ADDRCL	__PORT8 0xFFFFFF95	/* AD Data Register CH */ 
#define   ADDRCL_AD0m	0x40 
#define   ADDRCL_AD1m	0x80 
#define AD_ADDRDH	__PORT8 0xFFFFFF96	/* AD Data Register DH */ 
#define   ADDRDH_AD2m	0x01 
#define   ADDRDH_AD3m	0x02 
#define   ADDRDH_AD4m	0x04 
#define   ADDRDH_AD5m	0x08 
#define   ADDRDH_AD6m	0x10 
#define   ADDRDH_AD7m	0x20 
#define   ADDRDH_AD8m	0x40 
#define   ADDRDH_AD9m	0x80 
#define AD_ADDRDL	__PORT8 0xFFFFFF97	/* AD Data Register DL */ 
#define   ADDRDL_AD0m	0x40 
#define   ADDRDL_AD1m	0x80 
#define AD_ADCSR	__PORT8 0xFFFFFF98	/* AD ControlStatus Register */ 
#define   ADCSR_CH0m	0x01 
#define   ADCSR_CH1m	0x02 
#define   ADCSR_CH2m	0x04 
#define   ADCSR_CH3m	0x08 
#define   ADCSR_SCANm	0x10 
#define   ADCSR_ADSTm	0x20 
#define   ADCSR_ADIEm	0x40 
#define   ADCSR_ADFm	0x80 
#define AD_ADCR	__PORT8 0xFFFFFF99	/* AD Control Register */ 
#define   ADCR_CKS0m	0x04 
#define   ADCR_CKS1m	0x08 
#define   ADCR_TRGS0m	0x40 
#define   ADCR_TRGS1m	0x80 
/* Module Timer*/
#define TMR_TCSR1	__PORT8 0xFFFFFFA2	/* (R/W) Timer ControlStatus Register 1 */ 
#define   TCSR1_CKS0m	0x01 
#define   TCSR1_CKS1m	0x02 
#define   TCSR1_CKS2m	0x04 
#define   TCSR1_OVFm	0x80 
#define TMR_TCNT1	__PORT8 0xFFFFFFA3	/* (R) Timer Counter 1 */ 
/* Module A/D Converter */
#define DA_DADR0	__PORT8 0xFFFFFFA4	/* DA Data Register 0 */ 
#define DA_DADR1	__PORT8 0xFFFFFFA5	/* DA Data Register 1 */ 
#define DA_DACR01	__PORT8 0xFFFFFFA6	/*  DA Control Register 01 */ 
#define   DACR01_DAEm	0x20 
#define   DACR01_DAOE0m	0x40 
#define   DACR01_DAOE1m	0x80 
/* Module Flash Memory */
#define FLM_FLMCR1	__PORT8 0xFFFFFFA8	/* Flash Memory Control Register 1 */ 
#define   FLMCR1_Pm	0x01 			/*   Transition to program mode */
#define   FLMCR1_Em	0x02 			/*   Transition to erase mode */
#define   FLMCR1_PVm	0x04 			/*   Transition to program-verify mode */ 
#define   FLMCR1_EVm	0x08 			/*   Transition to erase-verify mode */
#define   FLMCR1_PSUm	0x10 			/*   Program setup when FWE = 1 and SWE1 = 1*/ 
#define   FLMCR1_ESUm	0x20 			/*   Erase setup when FWE = 1 and SWE1 = 1 */ 
#define   FLMCR1_SWEm	0x40 			/*   1= enable writes when FWE=1 */ 
#define   FLMCR1_FWEm	0x80 			/*   1 = programming enabled by FWE pin */
#define FLM_FLMCR2	__PORT8 0xFFFFFFA9	/* Flash Memory Control Register 2 */ 
#define   FLMCR2_FLERm	0x80 			/*   Flash memory modification error */ 
#define FLM_EBR1	__PORT8 0xFFFFFFAA	/* Erase Block Register 1 */ 
#define   EBR1_EB0m	0x01 			/*   Selects block to erase */
#define   EBR1_EB1m	0x02 
#define   EBR1_EB2m	0x04 
#define   EBR1_EB3m	0x08 
#define   EBR1_EB4m	0x10 
#define   EBR1_EB5m	0x20 
#define   EBR1_EB6m	0x40 
#define   EBR1_EB7m	0x80 
#define FLM_EBR2	__PORT8 0xFFFFFFAB	/* Erase Block Register 2 */ 
#define   EBR2_EB8m	0x01 
#define   EBR2_EB9m	0x02 
#define   EBR2_EB10m	0x04 
#define   EBR2_EB11m	0x08 
#define   EBR2_EB12m	0x10 			/* Valid on the H8S/2630. On the H8S/2638 and H8S/2639 these bits are reserved and only 0 */
#define   EBR2_EB13m	0x20 			/* Valid on the H8S/2630. On the H8S/2638 and H8S/2639 these bits are reserved and only 0 */
#define FLM_FLPWCR	__PORT8 0xFFFFFFAC	/* Flash Memory Power Control Register */ 
#define   FLPWCR_PDWNDm	0x80 
/* Module Port */
#define DIO_PORT1	__PORT8 0xFFFFFFB0	/* DIO 1 Register */ 
#define   PORT1_P10m	0x01 
#define   PORT1_P11m	0x02 
#define   PORT1_P12m	0x04 
#define   PORT1_P13m	0x08 
#define   PORT1_P14m	0x10 
#define   PORT1_P15m	0x20 
#define   PORT1_P16m	0x40 
#define   PORT1_P17m	0x80 
#define DIO_PORT3	__PORT8 0xFFFFFFB2	/* DIO 3 Register */ 
#define   PORT3_P30m	0x01 
#define   PORT3_P31m	0x02 
#define   PORT3_P32m	0x04 
#define   PORT3_P33m	0x08 
#define   PORT3_P34m	0x10 
#define   PORT3_P35m	0x20 
#define DIO_PORT4	__PORT8 0xFFFFFFB3	/* DIO 4 Register */ 
#define   PORT4_P40m	0x01 
#define   PORT4_P41m	0x02 
#define   PORT4_P42m	0x04 
#define   PORT4_P43m	0x08 
#define   PORT4_P44m	0x10 
#define   PORT4_P45m	0x20 
#define   PORT4_P46m	0x40 
#define   PORT4_P47m	0x80 
#define DIO_PORT9	__PORT8 0xFFFFFFB8	/* DIO 9 Register */ 
#define   PORT9_P90m	0x01 
#define   PORT9_P91m	0x02 
#define   PORT9_P92m	0x04 
#define   PORT9_P93m	0x08 
#define DIO_PORTA	__PORT8 0xFFFFFFB9	/* DIO A Register */ 
#define   PORTA_PA0m	0x01 
#define   PORTA_PA1m	0x02 
#define   PORTA_PA2m	0x04 
#define   PORTA_PA3m	0x08 
#define DIO_PORTB	__PORT8 0xFFFFFFBA	/* DIO B Register */ 
#define   PORTB_PB0m	0x01 
#define   PORTB_PB1m	0x02 
#define   PORTB_PB2m	0x04 
#define   PORTB_PB3m	0x08 
#define   PORTB_PB4m	0x10 
#define   PORTB_PB5m	0x20 
#define   PORTB_PB6m	0x40 
#define   PORTB_PB7m	0x80 
#define DIO_PORTC	__PORT8 0xFFFFFFBB	/* DIO C Register */ 
#define   PORTC_PC0m	0x01 
#define   PORTC_PC1m	0x02 
#define   PORTC_PC2m	0x04 
#define   PORTC_PC3m	0x08 
#define   PORTC_PC4m	0x10 
#define   PORTC_PC5m	0x20 
#define   PORTC_PC6m	0x40 
#define   PORTC_PC7m	0x80 
#define DIO_PORTD	__PORT8 0xFFFFFFBC	/* DIO D Register */ 
#define   PORTD_PD0m	0x01 
#define   PORTD_PD1m	0x02 
#define   PORTD_PD2m	0x04 
#define   PORTD_PD3m	0x08 
#define   PORTD_PD4m	0x10 
#define   PORTD_PD5m	0x20 
#define   PORTD_PD6m	0x40 
#define   PORTD_PD7m	0x80 
#define DIO_PORTE	__PORT8 0xFFFFFFBD	/* DIO E Register */ 
#define   PORTE_PE0m	0x01 
#define   PORTE_PE1m	0x02 
#define   PORTE_PE2m	0x04 
#define   PORTE_PE3m	0x08 
#define   PORTE_PE4m	0x10 
#define   PORTE_PE5m	0x20 
#define   PORTE_PE6m	0x40 
#define   PORTE_PE7m	0x80 
#define DIO_PORTF	__PORT8 0xFFFFFFBE	/* DIO F Register */ 
#define   PORTF_PF0m	0x01 
#define   PORTF_PF3m	0x08 
#define   PORTF_PF4m	0x10 
#define   PORTF_PF5m	0x20 
#define   PORTF_PF6m	0x40 
#define   PORTF_PF7m	0x80 

// aditional definition


#define IIC_SCRX	__PORT8 0xFFFFFDB4	/* Serial Control Register X */ 
#define   SCRX_FLSHEm	0x08 
#define   SCRX_IICEm	0x10 
#define   SCRX_IICX0m	0x20 
#define   SCRX_IICX1m	0x40 


#define SYS_LPWRCR	__PORT8 0xFFFFFDEC	/* Low-Power Control Register */ 
#define   LPWRCR_STC0m	0x01 			/*    */
#define   LPWRCR_STC1m	0x02 
#define   LPWRCR_STCxm	0x03
#define   LPWRCR_RFCUTm	0x08 
#define   LPWRCR_SUBSTPm 0x10 
#define   LPWRCR_NESELm	0x20 
#define   LPWRCR_LSONm	0x40 
#define   LPWRCR_DTONm	0x80 



/* define serial control registers */
#define SCI_SMR2	__PORT8 0xFFFFFF88	/* Serial Mode Register 2 */ 
#define   SMR2_CKS0m	0x01 
#define   SMR2_CKS1m	0x02 
#define   SMR2_MPm	0x04 
#define   SMR2_STOPm	0x08 
#define   SMR2_OEm	0x10 
#define   SMR2_PEm	0x20 
#define   SMR2_CHRm	0x40 
#define   SMR2_CAm	0x80 
#define SCI_BRR2	__PORT8 0xFFFFFF89	/* Bit Rate Register 2 */ 
#define SCI_SCR2	__PORT8 0xFFFFFF8A	/* Serial Control Register 2 */ 
#define   SCR2_CKE0m	0x01 
#define   SCR2_CKE1m	0x02 
#define   SCR2_TEIEm	0x04 
#define   SCR2_MPIEm	0x08 
#define   SCR2_REm	0x10 
#define   SCR2_TEm	0x20 
#define   SCR2_RIEm	0x40 
#define   SCR2_TIEm	0x80 
#define SCI_TDR2	__PORT8 0xFFFFFF8B	/* Transmit Data Register 2 */ 
#define SCI_SSR2	__PORT8 0xFFFFFF8C	/* Serial Status Register 2 */ 
#define   SSR2_MPBTm	0x01 
#define   SSR2_MPBm	0x02 
#define   SSR2_TENDm	0x04 
#define   SSR2_PERm	0x08 
#define   SSR2_FERm	0x10 
#define   SSR2_ORERm	0x20 
#define   SSR2_RDRFm	0x40 
#define   SSR2_TDREm	0x80 
#define SCI_RDR2	__PORT8 0xFFFFFF8D	/* Receive Data Register 2 */ 
#define SCI_SCMR2	__PORT8 0xFFFFFF8E	/* Smart Card Mode Register 2 */ 
#define   SCMR2_SMIFm	0x01 
#define   SCMR2_SINVm	0x04 
#define   SCMR2_SDIRm	0x08
/* END define serial control registers  */


/* Module Stop Control Register */
#define SYS_MSTPCRA	__PORT8 0xFFFFFDE8	/* Module Stop Control Register A */ 
#define   MSTPCRA_MSTPA0m	0x01 
#define   MSTPCRA_MSTPA1m	0x02 
#define   MSTPCRA_ADCm		0x02 
#define   MSTPCRA_MSTPA2m	0x04 
#define   MSTPCRA_DA01m		0x04 
#define   MSTPCRA_MSTPA3m	0x08 
#define   MSTPCRA_PPGm		0x08 
#define   MSTPCRA_MSTPA4m	0x10 
#define   MSTPCRA_MSTPA5m	0x20 
#define   MSTPCRA_TPUm		0x20 
#define   MSTPCRA_MSTPA6m	0x40 
#define   MSTPCRA_DTCm		0x40 
#define   MSTPCRA_MSTPA7m	0x80 
#define SYS_MSTPCRB	__PORT8 0xFFFFFDE9	/* Module Stop Control Register B */ 
#define   MSTPCRB_MSTPB0m	0x01 
#define   MSTPCRB_MSTPB1m	0x02 
#define   MSTPCRB_MSTPB2m	0x04 
#define   MSTPCRB_MSTPB3m	0x08 
#define   MSTPCRB_IIC1m		0x08 
#define   MSTPCRB_MSTPB4m	0x10 
#define   MSTPCRB_IIC0m		0x10 
#define   MSTPCRB_MSTPB5m	0x20 
#define   MSTPCRB_SCI2m		0x20 
#define   MSTPCRB_MSTPB6m	0x40 
#define   MSTPCRB_SCI1m		0x40 
#define   MSTPCRB_MSTPB7m	0x80 
#define   MSTPCRB_SCI0m		0x80 
#define SYS_MSTPCRC	__PORT8 0xFFFFFDEA	/* Module Stop Control Register C */ 
#define   MSTPCRC_MSTPC0m	0x01 
#define   MSTPCRC_MSTPC1m	0x02 
#define   MSTPCRC_MSTPC2m	0x04 
#define   MSTPCRC_HCAN1m	0x04 
#define   MSTPCRC_MSTPC3m	0x08 
#define   MSTPCRC_HCAN0m	0x08 
#define   MSTPCRC_MSTPC4m	0x10 
#define   MSTPCRC_PBCm		0x10 
#define   MSTPCRC_MSTPC5m	0x20 
#define   MSTPCRC_MSTPC6m	0x40 
#define   MSTPCRC_SCI4m		0x40 
#define   MSTPCRC_MSTPC7m	0x80 
#define   MSTPCRC_SCI3m		0x80
#define SYS_MSTPCRD	__PORT8 0xFFFFFC60	/* Module Stop Control Register D */ 
#define   MSTPCRD_MSTPD7m	0x80 
#define   MSTPCRD_PWMm		0x80 
 /* END Module Stop Control Register */

 /* start Flash register compatibility with 2633 programs (only different names of existing FLMCR1 bits)*/ /* // comented are the same */
//#define FLM_FLMCR1	__PORT8 0xFFFFFFA8	/* Flash Memory Control Register 1 */ 
#define   FLMCR1_P1m	0x01			/*   Transition to program mode */
#define   FLMCR1_E1m	0x02			/*   Transition to erase mode */
#define   FLMCR1_PV1m	0x04 			/*   Transition to program-verify mode */
#define   FLMCR1_EV1m	0x08 			/*   Transition to erase-verify mode */
#define   FLMCR1_PSU1m	0x10 			/*   Program setup when FWE = 1 and SWE1 = 1*/
#define   FLMCR1_ESU1m	0x20 			/*   Erase setup when FWE = 1 and SWE1 = 1 */
#define   FLMCR1_SWE1m	0x40			/*   1= enable writes when FWE=1 */ 
//#define   FLMCR1_FWEm	0x80			/*   1 = programming enabled by FWE pin */ 
//#define FLM_FLMCR2	__PORT8 0xFFFFFFA9	/* Flash Memory Control Register 2 */ 
//#define   FLMCR2_FLERm	0x80 			/*   Flash memory modification error */
 /* end Flash register compatibility with 2633 (only different names of FLMCR1 bits)*/ 
 
/* exception vectors numbers */ // nechat schvalit !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#define EXCPTVEC_POWRES	0
#define EXCPTVEC_MANRES	1
#define EXCPTVEC_TRACE	5
#define EXCPTVEC_DIRTRANS 6
#define EXCPTVEC_NMI	7
#define EXCPTVEC_TRAP0	8
#define EXCPTVEC_TRAP1	9
#define EXCPTVEC_TRAP2	10
#define EXCPTVEC_TRAP3	11
#define EXCPTVEC_IRQ0	16
#define EXCPTVEC_IRQ1	17
#define EXCPTVEC_IRQ2	18
#define EXCPTVEC_IRQ3	19
#define EXCPTVEC_IRQ4	20
#define EXCPTVEC_IRQ5	21
#define EXCPTVEC_IRQ6	22
#define EXCPTVEC_IRQ7	23
#define EXCPTVEC_SWDEND	24
#define EXCPTVEC_WOVI0	25
#define EXCPTVEC_CMI	26
#define EXCPTVEC_PBC	27
#define EXCPTVEC_ADI	28
#define EXCPTVEC_WOVI1	29
#define EXCPTVEC_TGI0A	32	/* TPU 0 */
#define EXCPTVEC_TGI0B	33
#define EXCPTVEC_TGI0C	34
#define EXCPTVEC_TGI0D	35
#define EXCPTVEC_TCI0V	36
#define EXCPTVEC_TGI1A	40	/* TPU 1 */
#define EXCPTVEC_TGI1B	41
#define EXCPTVEC_TCI1V	42
#define EXCPTVEC_TCI1U	43
#define EXCPTVEC_TGI2A	44	/* TPU 2 */
#define EXCPTVEC_TGI2B	45
#define EXCPTVEC_TCI2V	46
#define EXCPTVEC_TCI2U	47
#define EXCPTVEC_TGI3A	48	/* TPU 3 */
#define EXCPTVEC_TGI3B	49
#define EXCPTVEC_TGI3C	50
#define EXCPTVEC_TGI3D	51
#define EXCPTVEC_TCI3V	52
#define EXCPTVEC_TGI4A	56	/* TPU 4 */
#define EXCPTVEC_TGI4B	57
#define EXCPTVEC_TCI4V	58
#define EXCPTVEC_TCI4U	59
#define EXCPTVEC_TGI5A	60	/* TPU 5 */
#define EXCPTVEC_TGI5B	61
#define EXCPTVEC_TCI5V	62
#define EXCPTVEC_TCI5U	63
#define EXCPTVEC_CMIA0	64	/* 8 bit tim 0 */
#define EXCPTVEC_CMIB0	65
#define EXCPTVEC_OVI0	66
#define EXCPTVEC_CMIA1	68	/* 8 bit tim 1 */
#define EXCPTVEC_CMIB1	69
#define EXCPTVEC_OVI1	70
#define EXCPTVEC_DEND0A	72	/* DMAC */
#define EXCPTVEC_DEND0B	73
#define EXCPTVEC_DEND1A	74
#define EXCPTVEC_DEND1B	75
#define EXCPTVEC_ERI0	80	/* SCI 0 */
#define EXCPTVEC_RXI0	81
#define EXCPTVEC_TXI0	82
#define EXCPTVEC_TEI0	83
#define EXCPTVEC_ERI1	84	/* SCI 1 */
#define EXCPTVEC_RXI1	85
#define EXCPTVEC_TXI1	86
#define EXCPTVEC_TEI1	87
#define EXCPTVEC_ERI2	88	/* SCI 2 */
#define EXCPTVEC_RXI2	89
#define EXCPTVEC_TXI2	90
#define EXCPTVEC_TEI2	91
#define EXCPTVEC_CMIA2	92	/* 8 bit tim 2 */
#define EXCPTVEC_CMIB2	93
#define EXCPTVEC_OVI2	94
#define EXCPTVEC_CMIA3	96	/* 8 bit tim 3 */
#define EXCPTVEC_CMIB3	97
#define EXCPTVEC_OVI3	98
#define EXCPTVEC_IICI0	100	/* IIC 0 */
#define EXCPTVEC_DDCSW1	101
#define EXCPTVEC_IICI1	102	/* IIC 1 */
#define EXCPTVEC_ERI3	120	/* SCI 3 */
#define EXCPTVEC_RXI3	121
#define EXCPTVEC_TXI3	122
#define EXCPTVEC_TEI3	123
#define EXCPTVEC_ERI4	124	/* SCI 4 */
#define EXCPTVEC_RXI4	125
#define EXCPTVEC_TXI4	126
#define EXCPTVEC_TEI4	127 
 
 /*   Timer control register  (TPCR) */
#define   TPCR_TPSCm	0x07 	/* Clock sources */
#define   TPCR_TPSC_F1	0x00 	/*   fi clock/1 */
#define   TPCR_TPSC_F4	0x01 	/*   fi clock/4 */
#define   TPCR_TPSC_F16	0x02 	/*   fi clock/16 */
#define   TPCR_TPSC_F64	0x03 	/*   fi clock/64 */
#define   TPCR_TPSC_CA	0x04 	/*   TCLKA */
#define   TPCR_TPSC_012CB 0x05 	/*   TCLKB (only 012) */
#define   TPCR_TPSC_02CC 0x06 	/*   TCLKC (only 02) */
#define   TPCR_TPSC_45CC 0x05 	/*   TCLKC (only 45) */
#define   TPCR_TPSC_05CD 0x07 	/*   TCLKD (only 05) */
#define   TPCR_TPSC_135F256 0x06 /*   fi clock/256 (only 135) */
#define   TPCR_TPSC_2F1024 0x07 /*   fi clock/1024 (only 2) */
#define   TPCR_TPSC_3F1024 0x05 /*   fi clock/1024 (only 3) */
#define   TPCR_TPSC_4F1024 0x06 /*   fi clock/1024 (only 4) */
#define   TPCR_TPSC_3F4096 0x07    /*   fi clock/4096 (only 3) */
#define   TPCR_CKEGm	0x018 	/* Clock edge */
#define   TPCR_CKEG_RIS	0x000 	/*   Rising edge */
#define   TPCR_CKEG_FAL	0x008 	/*   Falling edge */
#define   TPCR_CKEG_BOTH 0x018 	/*   Both edges */
#define   TPCR_CCLRm	0xe0 	/* Counter clearing source */
#define   TPCR_CCLR_DIS	0x00 	/*   disabled */
#define   TPCR_CCLR_TGRA 0x20 	/*   source TGRA compare match/input capture */
#define   TPCR_CCLR_TGRB 0x40 	/*   source TGRB compare match/input capture */
#define   TPCR_CCLR_SYNC 0x60 	/*   synchronous clear by TSYR_SYNC */
#define   TPCR_CCLR_TGRC 0xa0 	/*   source TGRC compare match/input capture */
#define   TPCR_CCLR_TGRD 0xc0 	/*   source TGRD compare match/input capture */

/*   Timer mode register  (TMDR) */
#define   TPMDR_MDm	0x0f 	/* timer operating mode */
#define   TPMDR_MD_NORMAL 0x00 	/*   normal */
#define   TPMDR_MD_PWM1	 0x02 	/*   PWM 1 */
#define   TPMDR_MD_PWM2	 0x03 	/*   PWM 2 */
#define   TPMDR_MD_PHACN1 0x04 	/*   phase counting 1 (only 1245) */
#define   TPMDR_MD_PHACN2 0x05 	/*   phase counting 2 (only 1245) */
#define   TPMDR_MD_PHACN3 0x06 	/*   phase counting 3 (only 1245) */
#define   TPMDR_MD_PHACN4 0x07 	/*   phase counting 4 (only 1245) */
#define   TPMDR_BFAm	 0x10 	/* TGRA, TGRC together for buffer operation */
#define   TPMDR_BFBm	 0x20 	/* TGRB, TGRD together for buffer operation */

#endif /* _H82639H_H */
