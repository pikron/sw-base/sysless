/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  sci_channels.h - UART communication for H2638 microcontroller

   (C) 2005 by Michal Sojka <wentasah@centrum.cz>

 The COLAMI components can be used and copied according to next
 license alternatives
   - MPL - Mozilla Public License
   - GPL - GNU Public License

 *******************************************************************/
#ifndef _SCI_PORTS_H
#define _SCI_PORTS_H

#include <periph/sci_rs232.h>
#include <system_def.h>

extern sci_info_t sci_rs232_chan0, sci_rs232_chan1, sci_rs232_chan2;

extern sci_info_t *sci_rs232_chan_array[];

#ifndef SCI_RS232_CHAN_DEFAULT
#define SCI_RS232_CHAN_DEFAULT 0
#endif

/** 
 * This variable selects the default channel for use by IO functions
 * (prtinf etc.). You can change the value of this variable in your
 * application or by defining SCI_RS232_CHAN_DEFAULT symbol (probably
 * in system_def.h).
 */
extern int sci_rs232_chan_default;

#endif
