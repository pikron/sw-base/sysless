/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  sci_regs.h - UART communication for H2638 microcontroller

   (C) 2005 by Michal Sojka <wentasah@centrum.cz>
   (C) 2005 by Petr Kovacik <kovacp1@fel.cvut.cz>

 The COLAMI components can be used and copied according to next
 license alternatives
   - MPL - Mozilla Public License
   - GPL - GNU Public License

 *******************************************************************/

#ifndef _SCI_REGS_H
#define _SCI_REGS_H

#include <stdint.h>

struct sci_regs {
    volatile uint8_t  rs232_smr;
    volatile uint8_t  rs232_brr;
    volatile uint8_t  rs232_scr;
    volatile uint8_t  rs232_tdr;
    volatile uint8_t  rs232_ssr;
    volatile uint8_t  rs232_rdr;
    volatile uint8_t  rs232_scmr;
};

  
#endif /* _SCI_REGS_H */
