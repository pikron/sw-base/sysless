#include <periph/sci_rs232.h>
#include <periph/sci_channels.h>
#include <h8s2638h.h>
#include <cpu_def.h>
#include <periph/sci_rs232_bufs.h>

void sci_rs232_eri_isr_0(void) __attribute__ ((interrupt_handler));
void sci_rs232_rxi_isr_0(void) __attribute__ ((interrupt_handler));
void sci_rs232_txi_isr_0(void) __attribute__ ((interrupt_handler));
void sci_rs232_tei_isr_0(void) __attribute__ ((interrupt_handler));

void sci_rs232_eri_isr_0() { sci_rs232_eri_isr(&sci_rs232_chan0); }
void sci_rs232_rxi_isr_0() { sci_rs232_rxi_isr(&sci_rs232_chan0); }
void sci_rs232_txi_isr_0() { sci_rs232_txi_isr(&sci_rs232_chan0); }
void sci_rs232_tei_isr_0() { sci_rs232_tei_isr(&sci_rs232_chan0); }

int sci_rs232_rxd_pin_0() { return (*DIO_PORT3)&(1<<1); }

DECLARE_SCI_BUFS(0)

void sci_rs232_init_0() 
{ 
  sci_rs232_chan0.sci_rs232_buf_in       = sci_rs232_buf_in_0;
  sci_rs232_chan0.sci_rs232_buf_in_size  = sci_rs232_buf_in_0_size;
  sci_rs232_chan0.sci_rs232_buf_out      = sci_rs232_buf_out_0;
  sci_rs232_chan0.sci_rs232_buf_out_size = sci_rs232_buf_out_0_size;

  *SYS_MSTPCRB&=~MSTPCRB_SCI0m; 

  excptvec_set(EXCPTVEC_ERI0, sci_rs232_eri_isr_0);
  excptvec_set(EXCPTVEC_RXI0, sci_rs232_rxi_isr_0);
  excptvec_set(EXCPTVEC_TXI0, sci_rs232_txi_isr_0);
  excptvec_set(EXCPTVEC_TEI0, sci_rs232_tei_isr_0);
}

sci_info_t sci_rs232_chan0 = {
  .regs = (struct sci_regs *)SCI_SMR0,
  .sci_rs232_baud = 9600,
  .sci_rs232_mode = SCI_SMR_8N1,
  .sci_rs232_flowc = 0,
  .sci_rs232_init = sci_rs232_init_0,
  .sci_rs232_rxd_pin = sci_rs232_rxd_pin_0,
};
    

/* Local variables: */
/* c-basic-offset:2 */
/* End: */
