#include <periph/sci_rs232.h>
#include <periph/sci_channels.h>
#include <h8s2638h.h>
#include <cpu_def.h>
#include <periph/sci_rs232_bufs.h>

void sci_rs232_eri_isr_1(void) __attribute__ ((interrupt_handler));
void sci_rs232_rxi_isr_1(void) __attribute__ ((interrupt_handler));
void sci_rs232_txi_isr_1(void) __attribute__ ((interrupt_handler));
void sci_rs232_tei_isr_1(void) __attribute__ ((interrupt_handler));

void sci_rs232_eri_isr_1() { sci_rs232_eri_isr(&sci_rs232_chan1); }
void sci_rs232_rxi_isr_1() { sci_rs232_rxi_isr(&sci_rs232_chan1); }
void sci_rs232_txi_isr_1() { sci_rs232_txi_isr(&sci_rs232_chan1); }
void sci_rs232_tei_isr_1() { sci_rs232_tei_isr(&sci_rs232_chan1); }

int sci_rs232_rxd_pin_1() { return (*DIO_PORT3)&(1<<2); }

DECLARE_SCI_BUFS(1)

void sci_rs232_init_1() 
{ 
  sci_rs232_chan1.sci_rs232_buf_in       = sci_rs232_buf_in_1;
  sci_rs232_chan1.sci_rs232_buf_in_size  = sci_rs232_buf_in_1_size;
  sci_rs232_chan1.sci_rs232_buf_out      = sci_rs232_buf_out_1;
  sci_rs232_chan1.sci_rs232_buf_out_size = sci_rs232_buf_out_1_size;

  *SYS_MSTPCRB&=~MSTPCRB_SCI1m;
  
  excptvec_set(EXCPTVEC_ERI1, sci_rs232_eri_isr_1);
  excptvec_set(EXCPTVEC_RXI1, sci_rs232_rxi_isr_1);
  excptvec_set(EXCPTVEC_TXI1, sci_rs232_txi_isr_1);
  excptvec_set(EXCPTVEC_TEI1, sci_rs232_tei_isr_1);
    
}

sci_info_t sci_rs232_chan1 = {
  .regs = (struct sci_regs *)SCI_SMR1,
  .sci_rs232_baud = 9600,
  .sci_rs232_mode = SCI_SMR_8N1,
  .sci_rs232_flowc = 0,
  .sci_rs232_init = sci_rs232_init_1,
  .sci_rs232_rxd_pin = sci_rs232_rxd_pin_1
};
    

/* Local variables: */
/* c-basic-offset:2 */
/* End: */
