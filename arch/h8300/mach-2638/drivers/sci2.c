#include <periph/sci_rs232.h>
#include <periph/sci_channels.h>
#include <h8s2638h.h>
#include <cpu_def.h>
#include <periph/sci_rs232_bufs.h>

void sci_rs232_eri_isr_2(void) __attribute__ ((interrupt_handler));
void sci_rs232_rxi_isr_2(void) __attribute__ ((interrupt_handler));
void sci_rs232_txi_isr_2(void) __attribute__ ((interrupt_handler));
void sci_rs232_tei_isr_2(void) __attribute__ ((interrupt_handler));

void sci_rs232_eri_isr_2() { sci_rs232_eri_isr(&sci_rs232_chan2); }
void sci_rs232_rxi_isr_2() { sci_rs232_rxi_isr(&sci_rs232_chan2); }
void sci_rs232_txi_isr_2() { sci_rs232_txi_isr(&sci_rs232_chan2); }
void sci_rs232_tei_isr_2() { sci_rs232_tei_isr(&sci_rs232_chan2); }

int sci_rs232_rxd_pin_2() { return (*DIO_PORTA)&(1<<2); }

DECLARE_SCI_BUFS(2)

void sci_rs232_init_2() 
{ 
  sci_rs232_chan2.sci_rs232_buf_in       = sci_rs232_buf_in_2;
  sci_rs232_chan2.sci_rs232_buf_in_size  = sci_rs232_buf_in_2_size;
  sci_rs232_chan2.sci_rs232_buf_out      = sci_rs232_buf_out_2;
  sci_rs232_chan2.sci_rs232_buf_out_size = sci_rs232_buf_out_2_size;

  *SYS_MSTPCRB&=~MSTPCRB_SCI2m; 

  excptvec_set(EXCPTVEC_ERI2, sci_rs232_eri_isr_2);
  excptvec_set(EXCPTVEC_RXI2, sci_rs232_rxi_isr_2);
  excptvec_set(EXCPTVEC_TXI2, sci_rs232_txi_isr_2);
  excptvec_set(EXCPTVEC_TEI2, sci_rs232_tei_isr_2);
    
}

sci_info_t sci_rs232_chan2 = {
  .regs = (struct sci_regs *)SCI_SMR2,
  .sci_rs232_baud = 9600,
  .sci_rs232_mode = SCI_SMR_8N1,
  .sci_rs232_flowc = 0,
  .sci_rs232_init = sci_rs232_init_2,
  .sci_rs232_rxd_pin = sci_rs232_rxd_pin_2
};
    

/* Local variables: */
/* c-basic-offset:2 */
/* End: */
