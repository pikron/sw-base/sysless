/**
 * @file   sci_channels.c
 * @author Michal Sojka
 * @date   Thu Jan 12 15:45:02 2006
 * 
 * @brief Default list of SCI channels.
 * 
 * If an application declares #sci_rs232_chan_array itself, code and
 * data structures for only used channels is linked with that
 * application.
 */

#include <periph/sci_rs232.h>
#include <periph/sci_channels.h>

sci_info_t *sci_rs232_chan_array[] = {
    &sci_rs232_chan0,
    &sci_rs232_chan1,
    &sci_rs232_chan2
};

/* FIXME: Is this the correct place for this declaration? */
int sci_rs232_chan_count = sizeof(sci_rs232_chan_array)/sizeof(*sci_rs232_chan_array);
