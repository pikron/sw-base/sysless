/**
 * @file   sci_default.c
 * @author Michal Sojka
 * @date   Thu Jan 12 15:41:09 2006
 * 
 * @brief  Definition of default serial channel.
 * 
 * This file defines only a signle variable
 * #sci_rs232_chan_default. The reason for this is that if this
 * variable is defined by an application this file is not linked with
 * an application and thus application can override this default.
 */

#include <periph/sci_channels.h>

/**
 * The Index of default serial channel.
 *
 * This variable contains an index to #sci_rs232_chan_array. The
 * channel corresponding to this channel is used by many functions
 * such as printf.
 */
int sci_rs232_chan_default = SCI_RS232_CHAN_DEFAULT;
