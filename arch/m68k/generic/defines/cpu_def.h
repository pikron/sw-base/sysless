#ifndef _M68K_CPU_DEF_H
#define _M68K_CPU_DEF_H

/* atomic access routines */

#define atomic_clear_mask(mask, v) \
	__asm__ __volatile__("andl %1,%0" : "+m" (*(v)) : "id" (~(mask)))

#define atomic_set_mask(mask, v) \
	__asm__ __volatile__("orl %1,%0" : "+m" (*(v)) : "id" (mask))

#define atomic_clear_mask_w(mask, v) \
	__asm__ __volatile__("andw %1,%0" : "+m" (*(v)) : "id" (~(mask)))

#define atomic_set_mask_w(mask, v) \
	__asm__ __volatile__("orw %1,%0" : "+m" (*(v)) : "id" (mask))

#define atomic_clear_mask_b(mask, v) \
	__asm__ __volatile__("andb %1,%0" : "+m" (*(v)) : "id" (~(mask)))

#define atomic_set_mask_b(mask, v) \
	__asm__ __volatile__("orb %1,%0" : "+m" (*(v)) : "id" (mask))

/* atomic bit manipulation routines */



/* Port access routines */

#define readb(addr) \
    ({ unsigned char __v = (*(volatile unsigned char *) (addr)); __v; })
#define readw(addr) \
    ({ unsigned short __v = (*(volatile unsigned short *) (addr)); __v; })
#define readl(addr) \
    ({ unsigned int __v = (*(volatile unsigned int *) (addr)); __v; })

#define writeb(b,addr) (void)((*(volatile unsigned char *) (addr)) = (b))
#define writew(b,addr) (void)((*(volatile unsigned short *) (addr)) = (b))
#define writel(b,addr) (void)((*(volatile unsigned int *) (addr)) = (b))

/* Arithmetic functions */

#define sat_add_slsl(__x,__y) \
    __asm__ ("	add.l %2,%0\n" \
	"	bvcs 2f\n" \
	"	bpls 1f\n" \
	"	move.l #0x7fffffff,%0\n" \
	"	bras 2f\n" \
	"1:	move.l #0x80000000,%0\n" \
	"2:\n" \
      : "=r"(__x) \
      : "0" ((long)__x), "r" ((long)__y) : "cc"); \

#define sat_sub_slsl(__x,__y) \
    __asm__ ("	sub.l %2,%0\n" \
	"	bvcs 2f\n" \
	"	bpls 1f\n" \
	"	move.l #0x7fffffff,%0\n" \
	"	bras 2f\n" \
	"1:	move.l #0x80000000,%0\n" \
	"2:\n" \
      : "=r"(__x) \
      : "0" ((long)__x), "r" ((long)__y) : "cc"); \

#define div_us_ulus(__x,__y) \
  ({ \
    unsigned long __z=(__x); \
    __asm__ ("divu %2,%0": "=r"(__z) \
      : "0" (__z), "r" ((unsigned short)(__y)) : "cc"); \
    (unsigned short)__z; \
  })

#define div_ss_slss(__x,__y) \
  ({ \
    unsigned long __z=(__x); \
    __asm__ ("divs %2,%0": "=r"(__z) \
      : "0" (__z), "r" ((unsigned short)(__y)) : "cc"); \
    (unsigned short)__z; \
  })

#define muldiv_us(__x,__y,__z) \
  div_ss_slss((long)(__x)*(__y),__z)

#define muldiv_ss(__x,__y,__z) \
  div_us_ulus((unsigned long)(__x)*(__y),__z)

/* IRQ handling code */

#define __sti() __asm__ __volatile__ ("andiw #0xf8ff,%/sr": : : "memory")
#define __cli() __asm__ __volatile__ ("oriw  #0x0700,%/sr": : : "memory")

#define __save_flags(x) \
__asm__ __volatile__("movew %/sr,%0":"=d" (x) : /* no input */ :"memory")

#define __restore_flags(x) \
__asm__ __volatile__("movew %0,%/sr": /* no outputs */ :"d" (x) : "memory")

#define __get_vbr(x) \
__asm__ __volatile__("movec %/vbr,%0":"=a" (x) : /* no input */ :"memory")

#define __set_vbr(x) \
__asm__ __volatile__("movec %0,%/vbr": /* no outputs */ :"a" (x) : "memory")

#define __get_usp(x) \
__asm__ __volatile__("move %/usp,%0" : "=a" (x));

#define __set_usp(x) \
__asm__ __volatile__("move %0,%/usp" : : "a" (x));

#define __memory_barrier() \
__asm__ __volatile__("": : : "memory")

#define cli() __cli()
#define sti() __sti()
#define save_flags(x) __save_flags(x)
#define restore_flags(x) __restore_flags(x)
#define save_and_cli(flags)   do { save_flags(flags); cli(); } while(0)

#define NR_IRQS 256

/* this struct defines the way the registers are stored on the
   stack during a system call. */


#if 0   
struct pt_regs {
  long     d1;
  long     d2;
  long     d3;
  long     d4;
  long     d5;
  long     a0;
  long     a1;
  long     a2;
  long     d0;
  long     orig_d0;
  /* long     stkadj; */
  unsigned short sr;
  unsigned long  pc;
  unsigned format :  4; /* frame format specifier */
  unsigned vector : 12; /* vector offset */
};
#else
struct pt_regs {
  long     d0;
  long     d1;
  long     d2;
  long     d3;
  long     d4;
  long     d5;
  long     d6;
  long     d7;
  long     a0;
  long     a1;
  long     a2;
  long     a3;
  long     a4;
  long     a5;
  long     a6;
  unsigned short sr;
  unsigned long  pc;
  unsigned format :  4; /* frame format specifier */
  unsigned vector : 12; /* vector offset */
};
#endif
                                 
typedef struct irq_handler {
  void            (*handler)(int, void *, struct pt_regs *);
  unsigned long   flags;
  void            *dev_id;
  const char      *devname;
  struct irq_handler *next;
  short		  vectno;
} irq_handler_t;

#define	IRQH_ON_LIST	0x100	/* handler is used */

irq_handler_t *irq_array[NR_IRQS];
void          *irq_vec[NR_IRQS];

int add_irq_handler(int vectno,irq_handler_t *handler);

int test_irq_handler(int vectno,const irq_handler_t *handler);

void irq_redirect2vector(int vectno,struct pt_regs *regs);

#define __val2mfld(mask,val) (((mask)&~((mask)<<1))*(val)&(mask))
#define __mfld2val(mask,val) (((val)&(mask))/((mask)&~((mask)<<1)))

#endif /* _M68K_CPU_DEF_H */
