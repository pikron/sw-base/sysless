#ifndef _CTM4_H_
#define _CTM4_H_


#include <cpu_def.h>


/* CTM4_CRB (CTM4 Control Register Block) base address of the CTM4
   control registers */
#if defined(SIM_MM_VAL)&&(SIM_MM_VAL == 0)
#define CTM4_CRB 0x7ff400
#else
#undef SIM_MM_VAL
#define SIM_MM_VAL 1
#define CTM4_CRB 0xfff400
#endif
   
   
#define CTM4_BIUMCR (volatile uint16_t * const)(0x00 + CTM4_CRB)
                                /* BIUSM Module Configuration Register */
#define    CTM4_STOP 0x8000	/*    STOP Low-Power Stop Mode Enable */
#define    CTM4_FRZ  0x4000	/*    FREEZE Assertion Response */
#define    CTM4_VECT 0x1800     /*    Bits [7:6] of Interrupt Vector Base Number */
#define    CTM4_IARB 0x0700     /*    Bits [2:0] Interrupt Arbitration Identification ID */
#define    CTM4_TBRS 0x0021     /*    Time Base Register Bus Select Bits */
#define       CTM4_TBRS_TBB1 0x0000
#define       CTM4_TBRS_TBB2 0x0001
#define       CTM4_TBRS_TBB3 0x0020
#define       CTM4_TBRS_TBB4 0x0021

#define CTM4_BIUTEST (volatile uint16_t * const)(0x02 + CTM4_CRB)
                                /* BIUSM Test Register */

#define CTM4_BIUTBR (volatile uint16_t * const)(0x04 + CTM4_CRB)
                                /* BIUSM Time Base Register */

#define CTM4_CPCR (volatile uint16_t * const)(0x08 + CTM4_CRB)
                                /* CPSM Control Register */
#define    CTM4_PRUN 0x0008     /*     Prescaler Running */
#define    CTM4_DIV23 0x0004    /*     Divide By 2/Divide By 3 */
#define    CTM4_PSEL 0x0003     /*     Prescaler Division Ratio Select */

#define CTM4_CPTR (volatile uint16_t * const)(0x0A + CTM4_CRB)
                                /* CPSM Test Register */

#define CTM4_MCSM2SIC (volatile uint16_t * const)(0x10 + CTM4_CRB)
                               /* MCSM2 Status/Interrupt/Control Register */
#define    CTM4_MCSM_COF   0x8000  /* Counter Overflow Flag */
#define    CTM4_MCSM_IL    0x7000  /* Interrupt Level Field */
#define    CTM4_MCSM_IARB3 0x0800  /* Interrupt Arbitration Bit 3 */
#define    CTM4_MCSM_DRVA  0x0200  /* Drive Time Base Bus A */
#define    CTM4_MCSM_DRVB  0x0100  /* Drive Time Base Bus B */
#define    CTM4_MCSM_IN2   0x0080  /* Clock Input Pin Status */
#define    CTM4_MCSM_IN1   0x0040  /* Modulus Load Input Pin Status */
#define    CTM4_MCSM_EDGEN 0x0020  /* Modulus Load Edge Sensitivity Bits */
#define    CTM4_MCSM_EDGEP 0x0010  /* Modulus Load Edge Sensitivity Bits */
#define    CTM4_MCSM_CLK   0x0007  /* Counter Clock Select Field */

#define CTM4_MCSM2CNT (volatile uint16_t * const)(0x12 + CTM4_CRB)
                               /* MCSM2 Counter */

#define CTM4_MCSM2ML (volatile uint16_t * const)(0x14 + CTM4_CRB)
                               /* MCSM2 Modulus Latch */

#define CTM4_DASM3SIC (volatile uint16_t * const)(0x18 + CTM4_CRB)
                               /* DASM3 Status/Interrupt/Control Register */
#define    CTM4_DASM_FLAG  0x8000  /* Capture or Compare Event Flag */
#define    CTM4_DASM_IL    0x7000  /* Interrupt Level Field */
#define    CTM4_DASM_IARB3 0x0800  /* Interrupt Arbitration Bit 3 */
#define    CTM4_DASM_WOR   0x0200  /* Wired-OR Mode */
#define    CTM4_DASM_BSL   0x0100  /* Bus Select 0=A / 1=B */
#define    CTM4_DASM_IN    0x0080  /* Input Pin Status */
#define    CTM4_DASM_FORCA 0x0040  /* Force A */
#define    CTM4_DASM_FORCB 0x0020  /* Force B */
#define    CTM4_DASM_EDPOL 0x0010  /* Edge Polarity Bit */
#define    CTM4_DASM_MODE  0x000f  /* DASM Mode Select */
#define       CTM4_DASM_DIS   0x0  /*     Disabled */
#define       CTM4_DASM_IPWM  0x1  /*     Input pulse width measurement */
#define       CTM4_DASM_IPM   0x2  /*     Input measurement period */
#define       CTM4_DASM_IC    0x3  /*     Input capture */
#define       CTM4_DASM_OCB   0x4  /*     Output compare, flag on B compare */
#define       CTM4_DASM_OCAB  0x5  /*     Output compare, flag on A and B compare */
#define       CTM4_DASM_PWM16 0x8  /*     Output pulse width modulation */
#define       CTM4_DASM_PWM15 0x9  /*     Output pulse width modulation */
#define       CTM4_DASM_PWM14 0xa  /*     Output pulse width modulation */
#define       CTM4_DASM_PWM13 0xb  /*     Output pulse width modulation */
#define       CTM4_DASM_PWM12 0xc  /*     Output pulse width modulation */
#define       CTM4_DASM_PWM11 0xd  /*     Output pulse width modulation */
#define       CTM4_DASM_PWM9  0xe  /*     Output pulse width modulation */
#define       CTM4_DASM_PWM7  0xf  /*     Output pulse width modulation */

#define CTM4_DASM3A (volatile uint16_t * const)(0x1A + CTM4_CRB)
                               /* DASM3 Register A */

#define CTM4_DASM3B (volatile uint16_t * const)(0x1C + CTM4_CRB)
                               /* DASM3 Register B */

#define CTM4_DASM4SIC (volatile uint16_t * const)(0x20 + CTM4_CRB)
                               /* DASM4 Status/Interrupt/Control Register */

#define CTM4_DASM4A (volatile uint16_t * const)(0x22 + CTM4_CRB)
                               /* DASM4 Register A */

#define CTM4_DASM4B (volatile uint16_t * const)(0x24 + CTM4_CRB)
                               /* DASM4 Register B */

#define CTM4_PWM5SIC (volatile uint16_t * const)(0x28 + CTM4_CRB)
                               /* PWMSM5 Status/Interrupt/Control Register */
#define    CTM4_PWM_FLAG   0x8000  /* Period Completion Status */
#define    CTM4_PWM_IL     0x7000  /* Interrupt Level Field */
#define    CTM4_PWM_IARB3  0x0800  /* Interrupt Arbitration Bit 3 */
#define    CTM4_PWM_PIN    0x0080  /* Output Pin Status */
#define    CTM4_PWM_LOAD   0x0020  /* Period and Pulse Width Register Load Control */
#define    CTM4_PWM_POL    0x0010  /* Output Pin Polarity Control */
#define    CTM4_PWM_EN     0x0008  /* PWMSM Enable */
#define    CTM4_PWM_CLK    0x0007  /* Clock Rate Selection */

#define CTM4_PWM5A (volatile uint16_t * const)(0x2A + CTM4_CRB)
                               /* PWMSM5 Period */

#define CTM4_PWM5B (volatile uint16_t * const)(0x2C + CTM4_CRB)
                               /* PWMSM5 Pulse Width */

#define CTM4_PWM5C (volatile uint16_t * const)(0x2E + CTM4_CRB)
                               /* PWMSM5 Counter */

#define CTM4_PWM6SIC (volatile uint16_t * const)(0x30 + CTM4_CRB)
                               /* PWMSM6 Status/Interrupt/Control Register */

#define CTM4_PWM6A (volatile uint16_t * const)(0x32 + CTM4_CRB)
                               /* PWMSM6 Period */

#define CTM4_PWM6B (volatile uint16_t * const)(0x34 + CTM4_CRB)
                               /* PWMSM6 Pulse Width */

#define CTM4_PWM6C (volatile uint16_t * const)(0x36 + CTM4_CRB)
                               /* PWMSM6 Counter */

#define CTM4_PWM7SIC (volatile uint16_t * const)(0x38 + CTM4_CRB)
                               /* PWMSM7 Status/Interrupt/Control Register */

#define CTM4_PWM7A (volatile uint16_t * const)(0x3A + CTM4_CRB)
                               /* PWMSM7 Period */

#define CTM4_PWM7B (volatile uint16_t * const)(0x3C + CTM4_CRB)
                               /* PWMSM7 Pulse Width */

#define CTM4_PWM7C (volatile uint16_t * const)(0x3E + CTM4_CRB)
                               /* PWMSM7 Counter */

#define CTM4_PWM8SIC (volatile uint16_t * const)(0x40 + CTM4_CRB)
                               /* PWMSM8 Status/Interrupt/Control Register */

#define CTM4_PWM8A (volatile uint16_t * const)(0x42 + CTM4_CRB)
                               /* PWMSM8 Period */

#define CTM4_PWM8B (volatile uint16_t * const)(0x44 + CTM4_CRB)
                               /* PWMSM8 Pulse Width */

#define CTM4_PWM8C (volatile uint16_t * const)(0x46 + CTM4_CRB)
                               /* PWMSM8 Counter */

#define CTM4_DASM9SIC (volatile uint16_t * const)(0x48 + CTM4_CRB)
                               /* DASM9 Status/Interrupt/Control Register */

#define CTM4_DASM9A (volatile uint16_t * const)(0x4A + CTM4_CRB)
                               /* DASM9 Register A */

#define CTM4_DASM9B (volatile uint16_t * const)(0x4C + CTM4_CRB)
                               /* DASM9 Register B */

#define CTM4_DASM10SIC (volatile uint16_t * const)(0x50 + CTM4_CRB)
                               /* DASM10 Status/Interrupt/Control Register */

#define CTM4_DASM10A (volatile uint16_t * const)(0x52 + CTM4_CRB)
                               /* DASM10 Register A */

#define CTM4_DASM10B (volatile uint16_t * const)(0x54 + CTM4_CRB)
                               /* DASM10 Register B */

#define CTM4_MCSM11SIC (volatile uint16_t * const)(0x58 + CTM4_CRB)
                               /* MCSM11 Status/Interrupt/Control Register */

#define CTM4_MCSM11CNT (volatile uint16_t * const)(0x5A + CTM4_CRB)
                               /* MCSM11 Counter */

#define CTM4_MCSM11ML (volatile uint16_t * const)(0x5C + CTM4_CRB)
                               /* MCSM11 Modulus Latch */

#define CTM4_FCSM12SIC (volatile uint16_t * const)(0x60 + CTM4_CRB)
                               /* FCSM12 Status/Interrupt/Control Register */
#define    CTM4_FSCM_COF   0x8000  /* Counter Overflow Flag */
#define    CTM4_FSCM_IL    0x7000  /* Interrupt Level */
#define    CTM4_FSCM_IARB3 0x0800  /* Interrupt Arbitration Bit 3 */
#define    CTM4_FSCM_DRVA  0x0200  /* Drive Time Base Bus A */
#define    CTM4_FSCM_DRVB  0x0100  /* Drive Time Base Bus B */
#define    CTM4_FSCM_IN    0x0080  /* Clock Input Pin Status */
#define    CTM4_FSCM_CLK   0x0007  /* Counter Clock Select Field */

#define CTM4_FCSM12CNT (volatile uint16_t * const)(0x62 + CTM4_CRB)
                               /* FCSM12 Counter */

#endif /* _CTM4_H_ */
