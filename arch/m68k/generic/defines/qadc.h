#ifndef _QADC_H_
#define _QADC_H_


#include <cpu_def.h>


/* QADC_CRB (QADC Control Register Block) base address of the QADC
   control registers */
#if defined(SIM_MM_VAL)&&(SIM_MM_VAL == 0)
#define QADC_CRB 0x7ff200
#else
#undef SIM_MM_VAL
#define SIM_MM_VAL 1
#define QADC_CRB 0xfff200
#endif
   
   
#define QADC_MCR (volatile uint16_t * const)(0x00 + QADC_CRB)
                                /* Module Configuration Register (S) */
#define    QADC_STOP 0x8000	/*    STOP Low-Power Stop Mode Enable */
#define    QADC_FRZ  0x4000	/*    FREEZE Assertion Response */
#define    QADC_SUPV 0x0080	/*    SUPV Supervisor/Unrestricted Data Space */
#define    QADC_IARB 0x000f     /*    Interrupt Arbitration ID */

#define QADC_TEST (volatile uint16_t * const)(0x02 + QADC_CRB)
                                /* QADC Test Register (S) */

#define QADC_INT (volatile uint16_t * const)(0x04 + QADC_CRB)
                                /* QADC Interrupt Register (S) */
#define    QADC_IRLQ1 0x7000	/*    Queue 1 Interrupt Level */
#define    QADC_IRLQ2 0x0700	/*    Queue 2 Interrupt Level */
#define    QADC_IVB   0x00ff	/*    Interrupt Vector Base */

#define QADC_PORTQ (volatile uint16_t * const)(0x06 + QADC_CRB)
                                /* Port A/B data (S/U) */

#define QADC_PORTQA (volatile uint8_t * const)(0x06 + QADC_CRB)
                                /* Port A Data (S/U) */

#define QADC_PORTQB (volatile uint8_t * const)(0x07 + QADC_CRB)
                                /* Port B Data (S/U) */

#define QADC_DDRQA (volatile uint16_t * const)(0x08 + QADC_CRB)
                                /* Port Data Direction Register (S/U) */

#define QADC_QACR0 (volatile uint16_t * const)(0x0A + QADC_CRB)
                                /* Control Register 0 (S/U) */
#define    QADC_MUX   0x8000	/*    Externally Multiplexed Mode */
#define    QADC_PSH   0x01f0	/*    Prescaler Clock High Time */
#define    QADC_PSA   0x0008	/*    Prescaler Add a Tick */
#define    QADC_PSL   0x0007	/*    Prescaler Clock Low Time */

#define QADC_QACR1 (volatile uint16_t * const)(0x0C + QADC_CRB)
                                /* Control Register 1 (S/U) */
#define    QADC_CIE1  0x8000	/*    Queue 1 Completion Interrupt Enable */
#define    QADC_PIE1  0x4000	/*    Queue 1 Pause Interrupt Enable */
#define    QADC_SSE1  0x2000	/*    Queue 1 Single-Scan Enable */
#define    QADC_MQ1   0x0700	/*    Queue 1 Operating Mode */

#define QADC_QACR2 (volatile uint16_t * const)(0x0E + QADC_CRB)
                                /* Control Register 1 (S/U) */
#define    QADC_CIE2  0x8000	/*    Queue 2 Completion Interrupt Enable */
#define    QADC_PIE2  0x4000	/*    Queue 2 Pause Interrupt Enable */
#define    QADC_SSE2  0x2000	/*    Queue 2 Single-Scan Enable */
#define    QADC_MQ2   0x1f00	/*    Queue 2 Operating Mode */
#define    QADC_RES   0x0080	/*    Queue 2 Resume */
#define    QADC_BQ2   0x003f	/*    Beginning of Queue 2 */

#define QADC_QASR (volatile uint16_t * const)(0x10 + QADC_CRB)
                                /* Status Register 1 (S/U) */
#define    QADC_CF1   0x8000	/*    Queue 1 Completion Flag */
#define    QADC_PF1   0x4000	/*    Queue 1 Pause Flag */
#define    QADC_CF2   0x2000	/*    Queue 2 Completion Flag */
#define    QADC_PF2   0x1000	/*    Queue 2 Pause Flag */
#define    QADC_TOR1  0x0800	/*    Queue 1 Trigger Overrun */
#define    QADC_TOR2  0x0400	/*    Queue 2 Trigger Overrun */
#define    QADC_QS    0x03c0	/*    Queue Status */
#define    QADC_CWP   0x003f	/*    Command Word Pointer */

#define QADC_CCW ((volatile uint16_t * const)(0x30 + QADC_CRB)) /*  28 entries */
                                /* Conversion Command Word Table (S/U) */
#define    QADC_P     0x0200	/*    Pause */
#define    QADC_BYP   0x0100	/*    Sample Amplifier Bypass */
#define    QADC_IST   0x00c0	/*    Input Sample Time */
#define    QADC_CHAN  0x003f	/*    Channel Number */

#define QADC_RJURR ((volatile uint16_t * const)(0xB0 + QADC_CRB)) /*  28 entries */
                                /* Right Justified, Unsigned Result Register (S/U) */

#define QADC_LJSRR ((volatile uint16_t * const)(0x130 + QADC_CRB)) /*  28 entries */
                                /* Left Justified, Signed Result Register (S/U) */

#define QADC_LJURR ((volatile uint16_t * const)(0x1B0 + QADC_CRB)) /*  28 entries */
                                /* Left Justified, Unsigned Result Register (S/U) */

#endif /* _QADC_H_ */
