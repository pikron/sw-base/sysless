/*
 *-------------------------------------------------------------------
 *
 *   QSM -- Queued Serial Module
 *
 * The QSM contains two serial interfaces: (a) the queued serial
 * peripheral interface (QSPI) and the serial communication interface
 * (SCI). The QSPI provides peripheral expansion and/or interprocessor
 * communication through a full-duplex, synchronous, three-wire bus. A
 * self contained RAM queue permits serial data transfers without CPU
 * intervention and automatic continuous sampling. The SCI provides a
 * standard non-return to zero mark/space format with wakeup functions
 * to allow the CPU to run uninterrupted until woken
 *
 * For more information, refer to Motorola's "Modular Microcontroller
 * Family Queued Serial Module Reference Manual" (Motorola document
 * QSMRM/AD). 
 *
 * This file has been created by John S. Gwynne for support of 
 * Motorola's 68332 MCU in the efi332 project.
 * 
 * Redistribution and use in source and binary forms are permitted
 * provided that the following conditions are met:
 * 1. Redistribution of source code and documentation must retain
 *    the above authorship, this list of conditions and the
 *    following disclaimer.
 * 2. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 *
 * This software is provided "AS IS" without warranty of any kind,
 * either expressed or implied, including, but not limited to, the
 * implied warranties of merchantability, title and fitness for a
 * particular purpose.
 *
 *------------------------------------------------------------------
 *
 *  qsm.h,v 1.3 1995/12/19 20:03:32 joel Exp
 */

#ifndef _QSM_H_
#define _QSM_H_


#include <cpu_def.h>


/* SAM-- shift and mask */
#undef  SAM
#define SAM(a,b,c) ((a << b) & c)


/* QSM_CRB (QSM Control Register Block) base address of the QSM
   control registers */
#if defined(SIM_MM_VAL)&&(SIM_MM_VAL == 0)
#define QSM_CRB 0x7ffc00
#else
#undef SIM_MM_VAL
#define SIM_MM_VAL 1
#define QSM_CRB 0xfffc00
#endif 


#define QSM_CR (volatile uint16_t * const)(0x00 + QSM_CRB)
				/* QSM Configuration Register */
#define    QSM_STOP 0x8000	/*    Stop Enable */
#define    QSM_FRZ  0x6000	/*    Freeze Control */
#define    QSM_SUPV 0x0080	/*    Supervisor/Unrestricted */
#define	   QSM_IARB 0x000f	/*    Interrupt Arbitration */


#define QSM_QTEST (volatile uint16_t * const)(0x02 + QSM_CRB)
				/* QSM Test Register */
/* Used only for factor testing */


#define QSM_QILR (volatile uint8_t * const)(0x04 + QSM_CRB)
				/* QSM Interrupt Level Register */
#define    QSM_ILQSPI 0x38	/*    Interrupt Level for QSPI */
#define    QSM_ILSCI  0x07	/*    Interrupt Level for SCI */


#define QSM_QIVR (volatile uint8_t * const)(0x05 + QSM_CRB)
				/* QSM Interrupt Vector Register */
#define    QSM_INTV   0xff	/*    Interrupt Vector Number */


#define QSM_SCCR0 (volatile uint16_t * const)(0x08 + QSM_CRB)
				/* SCI Control Register 0 */
#define    QSM_QSM_SCBR  0x1fff	/*    SCI Baud Rate */


#define QSM_SCCR1 (volatile uint16_t * const)(0x0a + QSM_CRB)
				/* SCI Control Register 1 */
#define    QSM_LOOPS  0x4000	/*    Loop Mode */
#define    QSM_WOMS   0x2000	/*    Wired-OR Mode for SCI Pins */
#define    QSM_ILT    0x1000	/*    Idle-Line Detect Type */
#define    QSM_PT     0x0800	/*    Parity Type */
#define    QSM_PE     0x0400	/*    Parity Enable */
#define    QSM_M      0x0200	/*    Mode Select */
#define    QSM_WAKE   0x0100	/*    Wakeup by Address Mark */
#define    QSM_TIE    0x0080	/*    Transmit Interrupt Enable */
#define    QSM_TCIE   0x0040	/*    Transmit Complete Interrupt Enable */
#define    QSM_RIE    0x0020	/*    Receiver Interrupt Enable */
#define    QSM_ILIE   0x0010	/*    Idle-Line Interrupt Enable */
#define    QSM_TE     0x0008	/*    Transmitter Enable */
#define    QSM_RE     0x0004	/*    Receiver Enable */
#define    QSM_RWU    0x0002	/*    Receiver Wakeup */
#define    QSM_SBK    0x0001	/*    Send Break */


#define QSM_SCSR (volatile uint16_t * const)(0x0c + QSM_CRB)
				/* SCI Status Register */
#define    QSM_TDRE   0x0100	/*    Transmit Data Register Empty */
#define    QSM_TC     0x0080	/*    Transmit Complete */
#define    QSM_RDRF   0x0040	/*    Receive Data Register Full */
#define    QSM_RAF    0x0020	/*    Receiver Active */
#define    QSM_IDLE   0x0010	/*    Idle-Line Detected */
#define    QSM_OR     0x0008	/*    Overrun Error */
#define    QSM_NF     0x0004	/*    Noise Error Flag */
#define    QSM_FE     0x0002	/*    Framing Error */
#define    QSM_PF     0x0001	/*    Parity Error */


#define QSM_SCDR (volatile uint16_t * const)(0x0e + QSM_CRB)
				/* SCI Data Register */


#define QSM_PORTQS (volatile uint8_t * const)(0x15 + QSM_CRB)
				/* Port QS Data Register */

#define QSM_PQSPAR (volatile uint8_t * const)(0x16 + QSM_CRB)
				/* PORT QS Pin Assignment Rgister */
/* Any bit cleared (zero) defines the corresponding pin to be an I/O
   pin. Any bit set defines the corresponding pin to be a QSPI
   signal. */
/* note: PQS2 is a digital I/O pin unless the SPI is enabled in which
   case it becomes the SPI serial clock SCK. */
/* note: PQS7 is a digital I/O pin unless the SCI transmitter is
   enabled in which case it becomes the SCI serial output TxD. */
#define QSM_Fun 0x0
#define QSM_Dis 0x1
/*
 * PQSPAR Field     | QSM Function | Discrete I/O pin
 *------------------+--------------+------------------   */
#define PQSPA0   0  /*   MISO      |      PQS0           */
#define PQSPA1   1  /*   MOSI      |      PQS1           */
#define PQSPA2   2  /*   SCK       |      PQS2 (see note)*/
#define PQSPA3   3  /*   PCSO/!SS  |      PQS3           */
#define PQSPA4   4  /*   PCS1      |      PQS4           */
#define PQSPA5   5  /*   PCS2      |      PQS5           */
#define PQSPA6   6  /*   PCS3      |      PQS6           */
#define PQSPA7   7  /*   TxD       |      PQS7 (see note)*/


#define QSM_DDRQS  (volatile uint8_t * const)(0x17 + QSM_CRB)
				/* PORT QS Data Direction Register */
/* Clearing a bit makes the corresponding pin an input; setting a bit
   makes the pin an output. */


#define QSM_SPCR0 (volatile uint16_t * const)(0x18 + QSM_CRB)
				/* QSPI Control Register 0 */
#define    QSM_MSTR   0x8000	/*    Master/Slave Mode Select */
#define    QSM_WOMQ   0x4000	/*    Wired-OR Mode for QSPI Pins */
#define    QSM_BITS   0x3c00	/*    Bits Per Transfer */
#define    QSM_CPOL   0x0200	/*    Clock Polarity */
#define    QSM_CPHA   0x0100	/*    Clock Phase */
#define    QSM_SPBR   0x00ff	/*    Serial Clock Baud Rate */


#define QSM_SPCR1 (volatile uint16_t * const)(0x1a + QSM_CRB)
				/* QSPI Control Register 1 */
#define    QSM_SPE    0x8000	/*    QSPI Enable */
#define    QSM_DSCKL  0x7f00	/*    Delay before SCK */
#define    QSM_DTL    0x00ff	/*    Length of Delay after Transfer */


#define QSM_SPCR2 (volatile uint16_t * const)(0x1c + QSM_CRB)
				/* QSPI Control Register 2 */
#define    QSM_SPIFIE 0x8000	/*    SPI Finished Interrupt Enable */
#define    QSM_WREN   0x4000	/*    Wrap Enable */
#define    QSM_WRTO   0x2000	/*    Wrap To */
#define    QSM_ENDQP  0x0f00	/*    Ending Queue Pointer */
#define    QSM_NEWQP  0x000f	/*    New Queue Pointer Value */


#define QSM_SPCR3 (volatile uint8_t * const)(0x1e + QSM_CRB)
				/* QSPI Control Register 3 */
#define    QSM_LOOPQ  0x0400	/*    QSPI Loop Mode */
#define    QSM_HMIE   0x0200	/*    HALTA and MODF Interrupt Enable */
#define    QSM_HALT   0x0100	/*    Halt */


#define QSM_SPSR (volatile uint8_t * const)(0x1f + QSM_CRB)
				/* QSPI Status Register */
#define    QSM_SPIF   0x0080	/*    QSPI Finished Flag */
#define    QSM_MODF   0x0040	/*    Mode Fault Flag */
#define    QSM_HALTA  0x0020	/*    Halt Acknowlwdge Flag */
#define    QSM_CPTQP  0x000f	/*    Completed Queue Pointer */

#define QSM_QSPIRR ((volatile uint16_t * const)(0x100 + QSM_CRB))
				/* QSPI Receive Data RAM */
#define QSM_QSPITR ((volatile uint16_t * const)(0x120 + QSM_CRB))
				/* QSPI Transmit Data RAM */
#define QSM_QSPICR ((volatile uint8_t * const)(0x140 + QSM_CRB))
				/* QSPI Command RAM */
#define    QSM_CONT   0x80	/*    chipselects remain asserted */
#define    QSM_BITSE  0x40	/*    num of bits from SPCR0 */
#define    QSM_DT     0x20	/*    delay after transfer */
#define    QSM_DSCK   0x10	/*    SPCR1 specifies delay after PCS */
#define    QSM_PCS    0x0f	/*    peripheral chip select */

#endif /* _QSM_H_ */
