/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  tpu_func.h - timing processor unit (TPU) mode functions

  Copyright (C) 2001-2003 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2001-2003 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#ifndef _TPU_FUNC_H_
#define _TPU_FUNC_H_

#include <periph/tpu_sup.h>

/*******************************************************************/
/* QOM - Queued Output Match - Full description TPUPN01 */

/* 15-9 REF_ADDR 8 B 7-1 LAST_OFF_ADDR 0 A */
#define QOM_CHANNEL_CONTROL   0
/* 15-8 LOOP_CNT 7-1 OFF_PTR 0 C */
#define QOM_CHANNEL_STATUS    1
#define QOM_OFFSET_1          2

int tpu_qom_init(int chan, int prio, int init_val, int time_base,
                 int start_ref, unsigned short *transitions,
		 int count, int loop_cnt, int ref_addr);

/*******************************************************************/
/* NITC - New Input Capture/Input Transition Counter - TPUPN08 */

#define NITC_CHANNEL_CONTROL      0
/* 15-12 START_LINK_CHANNEL 11-8 LINK_CHANNEL_COUNT 7-1 PARAM_ADDR 0 */
#define NITC_PARAM_ADDR_AND_LINK  1
#define NITC_MAX_COUNT            2
#define NITC_TRANS_COUNT          3
#define NITC_FINAL_TRANS_TIME     4
#define NITC_LAST_TRANS_TIME      5

#define NITC_CHCTRL_FIXED         0x03
#define NITC_CHCTRL_RISING_EDGE   0x04
#define NITC_CHCTRL_FALLING_EDGE  0x08
#define NITC_CHCTRL_CAPT_TCR1     0x00
#define NITC_CHCTRL_CAPT_TCR2     0x40

int tpu_nitc_init(int chan, int prio, int edges, int tcr_source,
                  int param_addr, int continual, int max_count);


/*******************************************************************/
/* FQD - Fast Quadrature Decode - TPUPN02 */

#define FQD_EDGE_TIME          0
#define FQD_POSITION_COUNT     1
#define FQD_TCR1_VALUE         2
#define FQD_CHAN_PINSTATE      3
#define FQD_CORR_PINSTATE_ADDR 4
#define FQD_EDGE_TIME_LSB_ADDR 5

int tpu_fqd_init(int chan, int chan_peer, int prio);

static inline int
tpu_fqd_read_count(int chan)
{
  return *tpu_param_addr(chan,FQD_POSITION_COUNT);
}

#endif /*_TPU_FUNC_H_*/
