/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  tpu_fqd.c - TPU FQD - Fast Quadrature Decode
              Full description TPUPN02

  Copyright (C) 2001-2003 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2001-2003 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#include <stdint.h>
#include <system_def.h>
#include <sim.h>
#include <tpu.h>
#include <periph/tpu_sup.h>
#include <periph/tpu_func.h>

int tpu_fqd_init(int chan, int chan_peer, int prio)
{
  if(chan>15) return -1;

  tpu_set_cpr(chan,0);		/* disable chanel function */
  tpu_set_cpr(chan_peer,0);	/* disable chanel function */
  tpu_set_cfsr(chan,0x6);	/* FQD - Fast Quadrature Decode */
  tpu_set_cfsr(chan_peer,0x6);	/* FQD - Fast Quadrature Decode */
  tpu_set_hsqr(chan,0x0);	/* Primary Chanel */
  tpu_set_hsqr(chan_peer,0x1);	/* Secondary Chanel */

  *tpu_param_addr(chan,FQD_EDGE_TIME_LSB_ADDR)=
          ((int)tpu_param_addr(chan,FQD_EDGE_TIME)&0xff)+1;
  *tpu_param_addr(chan_peer,FQD_EDGE_TIME_LSB_ADDR)=
          ((int)tpu_param_addr(chan,FQD_EDGE_TIME)&0xff)+1;
  *tpu_param_addr(chan,FQD_CORR_PINSTATE_ADDR)=
          (int)tpu_param_addr(chan_peer,FQD_CHAN_PINSTATE)&0xff;
  *tpu_param_addr(chan_peer,FQD_CORR_PINSTATE_ADDR)=
          (int)tpu_param_addr(chan,FQD_CHAN_PINSTATE)&0xff;

  *tpu_param_addr(chan,FQD_POSITION_COUNT)=0;
  tpu_set_cpr(chan,prio);
  tpu_set_cpr(chan_peer,prio);
  tpu_set_hsrr(chan,3);
  tpu_set_hsrr(chan_peer,3);

  return 0;
}
