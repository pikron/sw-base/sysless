/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  tpu_nitc.c - TPU NITC - New Input Capture/Input Transition Counter
              Full description TPUPN08

  Copyright (C) 2001-2003 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2001-2003 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#include <stdint.h>
#include <system_def.h>
#include <sim.h>
#include <tpu.h>
#include <periph/tpu_sup.h>
#include <periph/tpu_func.h>

int tpu_nitc_init(int chan, int prio, int edges, int tcr_source,
                  int param_addr, int continual, int max_count)
{
  unsigned control;
  if(chan>15) return -1;

  tpu_set_cpr(chan,0);		/* disable chanel function */
  tpu_set_ier(chan,0);		/* disable MCU interrupt  */
  tpu_set_cfsr(chan,0xA);	/* NITC - New input transition counter */

  control=NITC_CHCTRL_FIXED;
  if(edges&NITC_CHCTRL_RISING_EDGE)  control|=NITC_CHCTRL_RISING_EDGE;
  if(edges&NITC_CHCTRL_FALLING_EDGE) control|=NITC_CHCTRL_FALLING_EDGE;
  if(tcr_source){
    if(tcr_source==2)                control|=NITC_CHCTRL_CAPT_TCR2;
    else                             control|=NITC_CHCTRL_CAPT_TCR1;
  }
  *tpu_param_addr(chan,NITC_CHANNEL_CONTROL)=control;
  *tpu_param_addr(chan,NITC_PARAM_ADDR_AND_LINK)=param_addr&0x7e;
  *tpu_param_addr(chan,NITC_MAX_COUNT)=max_count;
  *tpu_param_addr(chan,NITC_TRANS_COUNT)=0;

  /* 0 .. Single shot, 1 .. Continual, */
  /* 2 .. Single with Links, 3 .. Continual with Links */
  tpu_set_hsqr(chan,continual?1:0);
  /* 1 .. Initialize TCR mode, 2 .. Initialize parameter mode */
  if(tcr_source)
    tpu_set_hsrr(chan,1);
  else
    tpu_set_hsrr(chan,2);
  tpu_clear_cisr(chan);
  tpu_set_cpr(chan,prio);

  return 0;
}
