/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  tpu_qom.c - TPU QOM - Queued Output Match
              Full description TPUPN01

  Copyright (C) 2001-2003 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2001-2003 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#include <stdint.h>
#include <system_def.h>
#include <sim.h>
#include <tpu.h>
#include <periph/tpu_sup.h>
#include <periph/tpu_func.h>


int tpu_qom_init(int chan, int prio, int init_val, int time_base,
                 int start_ref, unsigned short *transitions,
		 int count, int loop_cnt, int ref_addr)
{
  int i;
  int continuous=0;
  int bit_a, bit_b, bit_c;
  int last_off_addr=0;
  int off_ptr=0;

  if(chan>15) return -1;

  tpu_set_cpr(chan,0);		/* disable chanel function */
  tpu_set_ier(chan,0);		/* disable MCU interrupt  */
  tpu_set_cfsr(chan,0xE);	/* QOM - Queued Output Match */

  bit_a=time_base;		/* 0 .. TCR1, 1 .. TCR2 */

  /* B:C Reference for First Match */
  /* 00 Immediate TCR Value */
  /* 01 Last Event Time */
  /* 10 Value Pointed to by REF_ADDR */
  /* 11 Last Event Time */
  bit_b=0;
  bit_c=0;

  if(!transitions || !count){
    count=1;
    *tpu_param_addr(chan,QOM_OFFSET_1)=init_val&1;
    last_off_addr=(int)tpu_param_addr(chan,QOM_OFFSET_1);
  }

  if((loop_cnt<0) || !transitions){
    loop_cnt=0;
  }else if(loop_cnt==0){
    continuous=2;
  }else{
    continuous=1;
  }

  *tpu_param_addr(chan,QOM_CHANNEL_CONTROL)=
	(bit_a<<0)|(bit_b<<8)|((last_off_addr&0x7e)<<0)|((ref_addr&0x7e)<<8);
  *tpu_param_addr(chan,QOM_CHANNEL_STATUS)=
	(bit_c<<0)|((off_ptr&0x7e)<<0)|(loop_cnt<<8);

  if(transitions){
    for(i=0;i<count;i++)
      *tpu_param_addr(chan,QOM_OFFSET_1+i)=transitions[i];
    last_off_addr=(int)tpu_param_addr(chan,QOM_OFFSET_1+count-1);
  }

  /* 0 .. Single shot, 1 .. Loop Mode, */
  /* 2 .. Continuous Mode, 3 .. Continuous Mode */
  tpu_set_hsqr(chan,continuous);
  /* 1 .. Initialize, No Pin Change, 2 .. Initialize, Pin Low, */
  /* 3 .. Initialize, Pin High */
  tpu_set_hsrr(chan,init_val>=0?((init_val&1)|0x2):1);
  tpu_clear_cisr(chan);
  tpu_set_cpr(chan,prio);

  return 0;
}
