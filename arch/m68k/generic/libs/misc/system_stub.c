/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  system_stub.c - system stub routines for system-less
                  NEWLIB C library environment
 
  Copyright (C) 2001-2003 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2001-2003 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#include <stdint.h>
#include <system_def.h>
#include <sys/stat.h>
#include <errno.h>

/***********************************************************/
#include <periph/qsm_rs232.h>

char inbyte()
{
  int ch;
  while((ch=qsm_rs232_recch())<0)
    { /* wait for char */ ; }
  return ch;
}


int outbyte(unsigned char ch)
{
  int res;
  while((res=qsm_rs232_sendch(ch))<0)
    { /* wait for send */ ; }
  return res;
}


/***********************************************************/

/*
 * close -- We don't need to do anything, but pretend we did.
 */
int close (int fd)
{
  return (0);
}

/*
 * fstat -- Since we have no file system, we just return an error.
 */
int fstat(int fd,struct stat *buf)
{
  buf->st_mode = S_IFCHR;       /* Always pretend to be a tty */
  buf->st_blksize = 0;
  return (0);
}

/*
 * getpid -- only one process, so just return 1.
 */
int getpid()
{
  return 1;
}

/*
 * isatty -- returns 1 if connected to a terminal device,
 *           returns 0 if not. Since we're hooked up to a
 *           serial port, we'll say yes _AND return a 1.
 */
int isatty(int fd)
{
  return (1);
}

/*
 * kill -- go out via exit...
 */
int kill(int pid,int sig)
{
  if(pid == 1)
    _exit(sig);
  return 0;
}


/*
 * lseek --  Since a serial port is non-seekable, we return an error.
 */
off_t lseek(int fd, off_t offset, int whence)
{
  errno = ESPIPE;
  return ((off_t)-1);
}

/*
 * open -- open a file descriptor. We don't have a filesystem, so
 *         we return an error.
 */
int open(const char *buf, int flags, int mode)
{
  errno = EIO;
  return (-1);
}

/*
 * print -- do a raw print of a string
 */
void print(char *ptr)
{
  while (*ptr) {
    outbyte (*ptr++);
  }
}

/*
 * putnum -- print a 32 bit number in hex
 */
void putnum(unsigned int num)
{
  char  buf[9];
  int   cnt;
  char  *ptr;
  int   digit;

  ptr = buf;
  for (cnt = 7 ; cnt >= 0 ; cnt--) {
    digit = (num >> (cnt * 4)) & 0xf;

    if (digit <= 9)
      *ptr++ = (char) ('0' + digit);
    else
      *ptr++ = (char) ('a' - 10 + digit);
  }

  *ptr = (char) 0;
  print (buf);
}

/*
 * read  -- read bytes from the serial port. Ignore fd, since
 *          we only have stdin.
 */
int read(int fd,char *buf,int nbytes)
{
  int i = 0;

  for (i = 0; i < nbytes; i++) {
    *(buf + i) = inbyte();
    if ((*(buf + i) == '\n') || (*(buf + i) == '\r')) {
      (*(buf + i + 1)) = 0;
      break;
    }
  }
  return (i);
}

char *heap_ptr=0;
extern char _end;

/*
 * sbrk -- changes heap size size. Get nbytes more
 *         RAM. We just increment a pointer in what's
 *         left of memory on the board.
 */
char *sbrk(int nbytes)
{
  char        *base;

  if (!heap_ptr)
  {
    heap_ptr = (char *)&_end;
    /* heap_ptr = (char *)0x70000; */ /* !!!!!!!!!!! */
  }
  base = heap_ptr;
  heap_ptr += nbytes;

  return base;
/* FIXME: We really want to make sure we don't run out of RAM, but this
 *       isn't very portable.
 */
#if 0
  if ((RAMSIZE - heap_ptr - nbytes) >= 0) {
    base = heap_ptr;
    heap_ptr += nbytes;
    return (base);
  } else {
    errno = ENOMEM;
    return ((char *)-1);
  }
#endif
}

/*
 * stat -- Since we have no file system, we just return an error.
 */
int stat(const char *path, struct stat *buf)
{
  errno = EIO;
  return (-1);
}

/*
 * unlink -- since we have no file system,
 *           we just return an error.
 */
int unlink(char * path)
{
  errno = EIO;
  return (-1);
}

/*
 * write -- write bytes to the serial port. Ignore fd, since
 *          stdout and stderr are the same. Since we have no filesystem,
 *          open will only return an error.
 */
int write(int fd, char *buf, int nbytes)
{
  int i;

  for (i = 0; i < nbytes; i++) {
    if (*(buf + i) == '\n') {
      outbyte ('\r');
    }
    outbyte (*(buf + i));
  }
  return (nbytes);
}
