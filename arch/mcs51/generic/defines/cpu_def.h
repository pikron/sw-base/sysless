
#if defined(SDCC) || defined(__SDCC) // sdcc
  #ifndef NULL
    #define NULL 0x0000
  #endif /*NULL*/ 
#endif // keil, gcc ???

#ifndef CODE
  #define CODE __code
#endif

#ifndef XDATA
  #define XDATA __xdata
#endif

#ifndef DATA
  #define DATA __data
#endif

#define inline
