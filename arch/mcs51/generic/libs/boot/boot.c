#include <system_def.h>
#include <ul_lib/ulan.h>
#include <keyval_id.h>
#include <ul_drv_fnc.h>
#include <uldy_base.h>
#include <local_config.h>
#ifdef CONFIG_MISC_VECT
#include <vect.h>
#endif

#define HZ_TIMER    100               //100Hz pro casovac

KVPB_BLOCK_LOC kvpb_block_t kvpb_block_global;
UL_DYAC_VAR_LOC ul_dyac_t ul_dyac_global;

volatile uchar timer_str,jump_timer;
uchar XDATA ustatus;

/****************************************************************************/
//definice casovace
void timer(void) 
{
  if (!TF0) return;
  TF0=0;
  TH0=HZ2TMODE1H(HZ_TIMER);		        //nastav casovac0
  TL0=HZ2TMODE1L(HZ_TIMER);      
  if (timer_str!=0) {
    timer_str=5;
    ul_str();
  } else timer_str--;
  if (jump_timer) jump_timer--;
}

char ul_save_adr(uchar uaddr) UL_DYAC_REENTRANT
{
  kvpb_set_key(&kvpb_store,KVPB_KEYID_ULAN_ADDR,1,&uaddr);
  return 0;
}

char ul_save_sn(unsigned long usn) UL_DYAC_REENTRANT
{
  kvpb_set_key(&kvpb_store,KVPB_KEYID_ULAN_SN,4,&usn);
  return 0;
}

void setup_board(void)
{
}

void main(void) 
{
 #ifndef UL_WITHOUT_HANDLE
  kvpb_block_t *kvpb_block=&kvpb_block_global;
  ul_dyac_t *ul_dyac=&ul_dyac_global;
  ul_msginfo msginfo;
 #endif  /*UL_WITHOUT_HANDLE*/
  unsigned long usn=0L;
  uchar uaddr=62;

  ustatus=0;
  LED_GP=0;

  //********************
  // timers
  TMOD=0x21;                                    //citac0-16bitovy,1-8bitovy(reload)
  TH0=HZ2TMODE1H(HZ_TIMER);		        //nastav casovac0
  TL0=HZ2TMODE1L(HZ_TIMER);      
  TCON=0x10;                                    //nul. priz. casov0,1;spust casov0
#ifdef CPU_X2_MODE
  CKCON0=0x01;				        //X2 mod for at89c51ed2
#endif
#ifdef __REG1210_H__
  MCON|=1;              			//XDATA memory map to 0x8400
#endif
#ifdef WATCHDOG_ENABLED
  WATCHDOG_ON();
  WATCHDOG_SET_MS(1000); //1s
  WATCHDOG_REFRESH();
#endif

  //********************
  // keyval init
  kvpb_block->base=(CODE uint8_t*)KVPB_BASE;
  kvpb_block->size=KVPB_SIZE;
  kvpb_block->flags=KVPB_DESC_DOUBLE;
  if(kvpb_check(kvpb_block,1)<0) 
    while(1);

  kvpb_get_key(kvpb_block,KVPB_KEYID_ULAN_ADDR,1,&uaddr);
  kvpb_get_key(kvpb_block,KVPB_KEYID_ULAN_SN,4,&usn);

  //********************
  // uLan init
  ul_drv_set_adr(uaddr);
  ul_drv_set_bdiv(BAUD2BAUDDIV(19200));
  ul_drv_init();
  ul_fd=ul_open(0,0);

  /***********************************/
  // uLan dyac init
  uldy_init(ULDY_ARG_ul_dyac ULDY_ARG_ul_fd ul_save_sn,ul_save_adr,NULL,usn);
  ul_drv_set_status(ustatus,sizeof(ustatus));

  //********************
  // start
  jump_timer=200;

  while ((jump_timer) || (ul_dyac->boot_activated)) {

    /* serve interrupt rutine for uLan */
    ul_int();

    /* serve interrupt rutine for timer */
    timer();

    if((ul_inepoll(ul_fd)>0)||(ul_fd_wait(ul_fd,100)>0)){
      uldy_process_msg(ULDY_ARG_ul_dyac NULL);
    }

    /* test request for address */
    if (uldy_rqa(ULDY_ARG1_ul_dyac)) 
      uldy_addr_rq(ULDY_ARG1_ul_dyac);

  #ifdef WATCHDOG_ENABLED
    WATCHDOG_REFRESH();
  #endif

  }
  LED_GP=1;
  vec_jmp(IADDR_ASTARTUP);  //application startup code
}
