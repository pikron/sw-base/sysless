#ifndef _VECT_H
#define _VECT_H

#ifdef __cplusplus
/*extern "C" {*/
#endif

#if !defined(__SDCC_MODEL_LARGE) || !(defined(SDCC) || defined(__SDCC))
  #define VECT_FNC_REENTRANT
#else
  #define VECT_FNC_REENTRANT __reentrant
#endif

typedef __code void (*prot_int)(void) __interrupt;

void __code *vec_set(prot_int fnc,unsigned char vec) VECT_FNC_REENTRANT;
void vec_jmp(unsigned char vec); 

#define	IADDR_EXTI0     0x03
#define	IADDR_SYSFNC    0x06
#define	IADDR_TIMER0    0x0B
//#define	VADDR_?         0x0E
#define	IADDR_EXTI1     0x13
//#define	VADDR_?         0x16
#define	IADDR_TIMER1    0x1B
#define	IADDR_V_uL_ADD  0x1E
#define	IADDR_SINT      0x23
#define	IADDR_SINT      0x23
#define	IADDR_V_uL_FNC  0x26
#define	IADDR_SIIC      0x2B
#define	IADDR_TIMER2    0x2B
//#define	VADDR_?         0x2E
#define	IADDR_T2CAP0    0x33
//#define	VADDR_?         0x36
#define	IADDR_T2CAP1    0x3B
//#define	VADDR_?         0x3E
#define	IADDR_T2CAP2    0x43
//#define	VADDR_?         0x46
#define	IADDR_T2CAP3    0x4B
//#define	VADDR_?         0x4E
#define	IADDR_ADCINT    0x53
//#define	VADDR_?         0x56
#define	IADDR_T2CMP0    0x5B
//#define	VADDR_?         0x5E
#define	IADDR_T2CMP1    0x63
//#define	VADDR_?         0x66
#define	IADDR_T2CMP2    0x6B
//#define	VADDR_?         0x6E
//#define	IADDR_TIMER2    0x73
#define	IADDR_ASTARTUP    0x7B  

#ifdef __cplusplus
/*}*/ /* extern "C"*/
#endif

#endif /* _VECT_H */

