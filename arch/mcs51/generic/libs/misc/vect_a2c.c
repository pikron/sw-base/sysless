#include "vect.h"

#if defined(SDCC) || defined(__SDCC)
#ifndef __SDCC_MODEL_LARGE
__data unsigned char vec_param1;
#endif /* SDCC_MODEL_LARGE */
#endif /* SDCC */

#if defined(SDCC) || defined(__SDCC)
#ifndef __SDCC_MODEL_LARGE
void __code *vec_set(prot_int fnc,unsigned char vec) VECT_FNC_REENTRANT
{
  vec_param1=vec;
  __asm
	.globl vec_set
	mov  r4,_vec_param1
	lcall vec_set
	mov  dpl,r4
	mov  dph,r5
  __endasm;
}
#else
/* For large model
 * vec   Allocated to stack - offset -3  (the push _bp taken into account)
 * fnc Allocated to registers 
 */
void __code *vec_set(prot_int fnc,unsigned char vec) VECT_FNC_REENTRANT
{
  __asm
	.globl vec_set
	mov  a,sp
	add  a,#-3
	mov  r0,a
	mov  a,@r0
	mov  r4,a
	lcall vec_set
	mov  dpl,r4
	mov  dph,r5
  __endasm;
}
#endif /* SDCC_MODEL_LARGE */
#else
void __code *vec_set(void __code *fnc,unsigned char vec) VECT_FNC_REENTRANT
{
#pragma asm
	extrn code(VEC_SET)
	mov  dpl,r7	; fnc address into DPTR
	mov  dph,r6
        mov  a,r5       ; vec into r4
        mov  r4,a
	lcall VEC_SET	; 
	mov  a,r4
	mov  r7,a
	mov  a,r5	; return previous vector setting
	mov  r6,a
#pragma endasm
}
#endif /* SDCC */

#if defined(SDCC) || defined(__SDCC)
void vec_jmp(unsigned char vec) 
{
  __asm
	.globl vec_get
	mov   dph,#0
	clr  a
	jmp  @a+dptr
  __endasm;
}
#else /*SDCC*/
void vec_jmp(unsigned char vec) 
{
#pragma asm
	mov   dpl,r7
	mov   dph,#0
	clr  a
	jmp  @a+dptr
#pragma endasm
}
#endif
