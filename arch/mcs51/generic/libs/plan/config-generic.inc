
%DEFINE (DEBUG_FL) (1)        ; Povoleni vlozeni debug rutin
%DEFINE (PB_ON_IB) (0)        ; Proc Buf same as In Buf
%DEFINE (MASTER_E) (1)        ; Bude se pouzivat i master mode
%DEFINE (XDATA_FL) (1)        ; Je pripojena i vnejsi pamet
%DEFINE (VECTOR_FL)(0)        ; Nastav vektory pro vyvojovou verzi
%DEFINE (DR_EO_NG) (1)        ; Negovana hodnota DR_EO
%DEFINE (COMM_S)   (1)        ; Synchronizace TIRIS

SER_STACK_EXT  EQU  0        ; Pridavna hodnota pro stack

DR_EO   BIT   P3.7    ; Aktivni v 0 (dle DR_EO_NG)
LENG_IB EQU   16      ; Delka vstupniho bufferu v bytech
LENG_OB EQU   16      ; Delka vystupniho bufferu

