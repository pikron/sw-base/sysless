#include "plan.h"

//*****************************************************************************
unsigned char 
pl_send(unsigned char dadr,unsigned char req,unsigned char *pmsg,unsigned char len) {
  unsigned char i=0;

  uLF_ERR=0;
  BEG_OB[0]=dadr;
  BEG_OB[1]=req;
  BEG_OB[2]=(unsigned char)BEG_OB+3+len;
  while(i!=len) {BEG_OB[3+i]=*pmsg;i++;pmsg++;}
  BEG_OB[0]|=128;
  uL_SND();
  while ((BEG_OB[0]!=0) && (uLF_ERR==0));	//cekej na odvysilani
  return(!uLF_ERR);
}

//*****************************************************************************
unsigned char 
pl_recv(unsigned char *sadr,unsigned char *req,unsigned char *pmsg,unsigned char *len) {
  unsigned char i=0;

  *len=0;
  if ((BEG_IB[0] & 128)!=0) {			//prijmuty nove data ?
    *sadr=BEG_IB[0];
    *req=BEG_IB[1];
    *len=BEG_IB[2]-(unsigned char)(BEG_IB+3);
    while(i!=*len) {*pmsg=BEG_IB[3+i];i++;pmsg++;}
    BEG_IB[0]=0;				//odebrana data
    return 1;
  }
  return 0;
}
