;********************************************************************
;*            pLan komunikace - PLAN.ASM                            *
;*     Komunikacni rutiny pro 8051 bez vnejsi pameti		    *
;*                  Stav ke dni 17. 1.1995                          *
;*                      (C) Pisoft 1994                             *
;*                          Pavel Pisa Praha                        *
;********************************************************************

$INCLUDE(cf_plan.inc)

%IF (%VECTOR_FL) THEN (
  EXTRN    CODE(VEC_SET)
)FI

; Vnejsi rutina vyslani statusu musi volat ACK_CMD, SND_BEB,
; n.SND_CHR, SND_END a skocit na S_WAITD nebo skok na NAK_CMD
;EXTRN   CODE(uL_SNST,uL_IDB,uL_IDE)
EXTRN   CODE(_uL_IDSTR)       ; Jmeno modulu
; Rutiny volane z uL_SNST
PUBLIC  ACK_CMD,SND_BEB,SND_CHC,SND_END,S_WAITD,NAK_CMD,SND_Bi,SND_Bc
PUBLIC          REC_BEG,REC_CHR,REC_END,REC_CME,        REC_Bi
PUBLIC  uL_ADR,uL_CMD,uL_SA
PUBLIC  BEG_PB,END_PB,BEG_IB,END_IB,BEG_OB,END_OB
%IF (%COMM_S) THEN (
EXTRN   BIT(SYNCH_COMM)
)FI

; Rutiny pro spolupraci s PLAN mino preruseni
PUBLIC  uL_STR,_uL_INIT,uL_SND
PUBLIC  uLF_ERR,uLF_SN,uLF_RS,uLF_NB, uLF_NA

PLAN__C SEGMENT CODE
PLAN__D SEGMENT DATA
PLAN__I SEGMENT IDATA
PLAN__B SEGMENT DATA BITADDRESSABLE

S_SPEED EQU   3       ; Bd=OSC/12/16/S_SPEED
S_FRLN  EQU   1024    ; maximalni delka ramce

CSEG AT SINT
	JMP   S_INT
	JMP   uL_FNC

PCON    DATA  087H
FCON    DATA  0D1H
TF2	BIT   0C8H.7
;=================================================================
;

; Rizeni linky - prikazy

uL_ERRI EQU   0FFH ; Ignoruj vse doslo k chybe
uL_ERR  EQU   07FH ; Chyba v datech
uL_END  EQU   07CH ; Konec dat
uL_ARQ  EQU   07AH ; Konec dat - vysli ACK
uL_PRQ  EQU   079H ; Konec dat - proved prikaz
uL_AAP  EQU   076H ; ARQ + PRQ
uL_BEG  EQU   075H ; Zacatek dat

; Potvrzovaci zpravy
uL_ACK  EQU   019H ; Potvrzeni
uL_NAK  EQU   07FH ; Doslo k chybe
uL_WAK  EQU   025H ; Ted nemohu splnit

; Prikazy posilane linkou
; 00H .. 3FH    uloz do bufferu
; 40H .. 7FH    uloz do bufferu bez ACK
; 80H .. 9FH    okamzite proved a konci
; A0H .. BFH    proved a prijimej
; C0H .. FFH    proved a vysilej

uL_NCS  EQU   07FH ; Ridici sluzby site

uL_RES  EQU   080H ; Znovu inicializuj RS485
uL_SFT  EQU   081H ; Otestuj velikost volne pameti
uL_SYN  EQU   085H ; Sychronizace vysilani TIRIS (zapni EXT2 + flag synch_comm)
uL_SID  EQU   0F0H ; Predstav se
uL_SFI  EQU   0F1H ; Vysli velikost volne pameti v IB
uL_TF0  EQU   098H ; Konec krokovani
uL_TF1  EQU   099H ; Pocatek krokovani
uL_STP  EQU   09AH ; Krok
uL_DEB	EQU   09BH ; Dalsi prikazy pro debug
uL_SPC  EQU   0DAH ; Vysle PCL PCH PSW ACC

uL_RDM  EQU   0F8H ; Cte pamet typu   T T B B L L
uL_WRM  EQU   0B8H ; Zapise do pameti T T B B L L

uL_GST  EQU   0C1H ; Zavola rutinu vyslani statusu uL_PST

RSEG PLAN__B

uL_FLG: DS    1
uLF_ER0 BIT   uL_FLG.0  ; Chyba pri vysilani
uLF_ER1 BIT   uL_FLG.1  ; Chyba pri vysilani
uLF_ERR BIT   uL_FLG.2  ; Chyba pri vysilani
uLF_SN  BIT   uL_FLG.3  ; uLan vysila - master mode
uLF_RS  BIT   uL_FLG.4  ; Potreba vysilat
uLF_NB  BIT   uL_FLG.5  ; Zbernice neobsazena
uLF_NA  BIT   uL_FLG.6  ; Mazano kazdou akci
uLF_TRP BIT   uL_FLG.7  ; Jiny duvod preruseni

RSEG    PLAN__D

uL_ADR: DS    1  ;    Adresa daneho ucastnika site
uL_CMD: DS    1  ;    Prave prenaseny prikaz
uL_SA:  DS    1  ;    Adresa vysilace

SP_BUF: DS    5+SER_STACK_EXT    

RSEG    PLAN__I

BEG_IB: DS    LENG_IB         ; Input buffer  : SA CMD END
END_IB:

BEG_OB: DS    LENG_OB         ; Output buffer : DA CMD END
END_OB:

%IF (%PB_ON_IB) THEN (
BEG_PB	IDATA BEG_IB+3        ; PROC_BUF
END_PB  IDATA END_IB
)ELSE(
BEG_PB: DS    16              ; Proceed buffer
END_PB:
)FI

%IF (%XDATA_FL) THEN (
PLAN__X SEGMENT XDATA
RSEG	PLAN__X
XPR_BUF:DS    3
)FI

;=================================================================
;

RSEG    PLAN__C

USING	1

; Cekani na znak R0 - 1 znaku  jinak S_ERR
WTF_CHR:DJNZ  R0,SND_SPC
V2_ERR: JMP   S_ERR

; Test a docekani po dobu 1 znaku
SND_SPT:CLR   TI
	JNB   TXD,S_RET
SND_SPR:RET

; Ceka kdyz je nutne shodit DR_EO
%IF (%DR_EO_NG) THEN (
SND_SPS:JB   DR_EO,SND_SPR
)ELSE(
SND_SPS:JNB   DR_EO,SND_SPR
)FI

; Cekani po dobu 1 znaku
SND_SPC:CLR   SM2
	SETB  REN
%IF (%DR_EO_NG) THEN (
SND_SP1:CLR   DR_EO           ; *** Ceka 1 znak
)ELSE(
SND_SP1:SETB  DR_EO           ; *** Ceka 1 znak
)FI
	CLR   A
	SJMP  SND_CH2

SND_CTR:SETB  C		      ; *** Send control character
	SJMP  SND_CH1
SND_CHC:XRL   AR1,A           ; *** Send data + add chk sum
	INC   R1
SND_CHR:CLR   C               ; *** Send data character
SND_CH1:MOV   TB8,C           ; ACC .. character
	CLR   REN
%IF (%DR_EO_NG) THEN (
	SETB  DR_EO
)ELSE(
	CLR   DR_EO
)FI

SND_CH2:MOV   SBUF,A
	CLR   TI
	SJMP  S_RET

REC_CTR:SETB  C               ; *** Receive control character
	DB    74H	      ; MOV A,#d8
REC_CHR:CLR   C               ; *** Receive character
REC_CH1:MOV   SM2,C	      ; ACC .. rec. char
%IF (%DR_EO_NG) THEN (
	CLR   DR_EO	      ; CY  .. RB8
)ELSE(
	SETB  DR_EO	      ; CY  .. RB8
)FI

	SETB  REN
	CLR   TI

RS232:
S_RET:  JB    uLF_TRP,S_INT_T
S_RETI: MOV   A,SP
	XCH   A,SP_BUF
	MOV   SP,A
	POP   ACC
	POP   PSW
	RETI

; *************************************
; Interupt TI nebo RI
; *************************************

S_INT  :PUSH  PSW
	PUSH  ACC
	MOV   A,SP
	XCH   A,SP_BUF
	JZ    S_ERROR
	MOV   SP,A

S_INT_T:MOV   PSW,#AR0; Banka1
;	JB    DR_EI,RS232
        MOV   C,RB8           ; errata 89c51rd2, point 6 !!!!!!
        CLR   RB8             ; nutno dodrzet !!!!!!!!!!!!!!!!!
        JBC   RI,S_INT_2
	JNB   TI,S_RET
S_INT_1:CLR   uLF_NA
	RET
S_INT_2:JB    REN,S_INT_3     ; errata 89c51rd2, point 8
        SJMP  S_RETI
S_INT_3:SETB  AC	      ; Preruseni od RI => AC=1
	CLR   uLF_NB
	MOV   A,SBUF
        JNC   S_INT_1
	JNB   ACC.7,S_INT_1
	SETB  uLF_NB
	CJNE  A,#uL_ERRI,S_INT_1
S_ERROR:MOV   PSW,#AR0; Banka1
	CLR   uLF_SN
	JMP   S_ERR

; Doslo k chybe pri prijmu nebo vysilani

S_ERR:  MOV   SP,#SP_BUF
	JNB   uLF_SN,S_WAITD
	MOV   R0,#3           ; Pocet vyslani ERRI pri normalni chybe
	JNB   F0,S_ERR_1
	MOV   R0,#10          ; Pocet vyslani ERRI pri zavazne chybe
S_ERR_1:MOV   A,#uL_ERRI
	CALL  SND_CTR
	DJNZ  R0,S_ERR_1
S_ERR_2:INC   uL_FLG
	MOV   A,uL_FLG
	ANL   A,#3
	JNZ   S_END
	DEC   uL_FLG
S_ERR_3:SETB  uLF_ERR
	MOV   R0,#0FFH
	SJMP  S_ENDTE

; Rutina maze posledni zpravu

S_ENDT: MOV   R0,#0
S_ENDTE:MOV   SP,#SP_BUF
	ANL   uL_FLG,#0FCH
%IF (%MASTER_E) THEN (
	MOV   R0,#BEG_OB; Mazani posledni vysilane zpravy
	MOV   @R0,#0
)FI
S_ENDT1:;?JMP S_END     ; Neexistuje dalsi zprava
	CLR   uLF_RS

; Konec vysilani nebo prijmu

S_END:  MOV   SP,#SP_BUF
	JNB   uLF_SN,S_WAITD
	JB    uLF_NB,S_WAITD
	CALL  SND_SPS
	JB    AC,S_WAITD
	MOV   A,uL_ADR
	ORL   A,#80H
	CALL  SND_CTR
	SETB  uLF_NB

; Cekani na komunikaci

%IF (%DR_EO_NG) THEN (
S_WAITD:CLR   DR_EO
) ELSE (
S_WAITD:SETB  DR_EO
)FI
	MOV   SP,#SP_BUF
	CLR   TI
	SETB  F0
	SETB  REN
	SETB  TXD

; Cekai na vlastni adresu nebo pozadavek k vysilani

S_WAIT: CLR   uLF_SN
%IF (%MASTER_E) THEN (
	JNB   uLF_NB,S_WAIT1  ; Nelze vysilat
	JNB   uLF_RS,S_WAIT1  ; Neni potreba vysilat
	SETB  uLF_SN
	MOV   A,uL_ADR
	MOV   R0,A            ; Vlastni adresa
	SETB  C
	SUBB  A,R1            ;  - Posledni vysilajici
	ANL   A,#00FH         ; Token pasing delay
	ADD   A,#4            ; min 4
	JNB   F0,S_CONN1
	ADD   A,#010H         ; Prodlouzit - byla chyba
S_CONN1:MOV   R1,A
S_CONN2:CALL  SND_SPC         ; Cekani token pasing delay
	JB    AC,S_WAIT2
	DJNZ  R1,S_CONN2
	CALL  S_ARB	      ; Arbitrace pristupu
	JB    F0,S_WAIT1
SND_OB: MOV   R0,#BEG_OB      ; Nasledujici blok
	MOV   A,@R0
	JB    ACC.7,SND_OBV
	JMP   S_ENDT          ; Neexistuje
SND_OBV:MOV   R1,A
	INC   R0
	MOV   A,@R0
	INC   R0
	MOV   AR6,@R0
	INC   R0
	XCH   A,R0
	MOV   R2,A
	XRL   A,R6
	XCH   A,R1
	JMP   SND_OB1         ; Vysli blok
)ELSE(
	JMP   S_WAIT1         ; Cekani
)FI

S_WAIT1:MOV   C,uLF_NB
	CPL   C
	CALL  REC_CH1         ; Ceka se na znak
S_WAIT2:CLR   uLF_SN
	JNB   AC,S_WAITD      ; Vybuzovaci TI
	MOV   R1,A
	JNC   S_WAITD         ; Neni ridici
	JB    ACC.7,S_WAIT    ; Ukonceni vysilani
	MOV   R1,#0
	CALL  REC_BE2         ; Cekani na prikaz v R0
	SJMP  REC_CM1

REC_CMD:CALL  REC_BEG         ; Cekani na prikaz v R0
REC_CM1:MOV   A,R0
	JB    ACC.7,REC_CM2
	MOV   R0,#BEG_IB      ; Prijem do IB
	MOV   A,@R0
	JNZ   REC_CMEV
	MOV   R2,#BEG_IB+3
	MOV   R6,#END_IB
	CALL  REC_Bi
	JNC   REC_CMEV
	CALL  REC_EN2
        MOV   R1,A
	CALL  IB_ENDR
        MOV   A,R1
	JB    ACC.1,REC_CM0
V1_WAIT:JMP   S_WAITD

; Ukonceni prijmu do IB
IB_ENDR:MOV   R0,#BEG_IB
	MOV   A,uL_SA
	ORL   A,#80H
	MOV   @R0,A
	INC   R0
	INC   R0
	MOV   A,R2
	MOV   R6,A
	MOV   @R0,A
	DEC   R0
	MOV   A,uL_CMD
	MOV   @R0,A
	RET

REC_CMEV:JMP  REC_CME

REC_CM2:	              ; Prijem do PB
%IF (%PB_ON_IB) THEN (
	MOV   R0,#BEG_IB
	MOV   A,@R0
	JNZ   REC_CMEV
)FI	MOV   R2,#BEG_PB
	MOV   R6,#END_PB
	CALL  REC_Bi
	MOV   AR6,R2
	JNC   REC_CMEV
	CALL  REC_EN2
	JNB   ACC.1,V1_WAIT
REC_CM0:MOV   A,uL_CMD        ; Cislo prikazu
	CJNE  A,#uL_SID,REC_CM3

; Vysle svoji identifikaci
	CALL  ACK_CMD
	CALL  SND_BEB
        MOV   DPTR,#_uL_IDSTR
        CLR   A
        MOVC  A,@A+DPTR
        MOV   R2,A
	MOV   A,#1
        MOVC  A,@A+DPTR
        MOV   R3,A
	MOV   A,#3
        MOVC  A,@A+DPTR
        ADD   A,R2
        MOV   R6,A
        CLR   A
        ADDC  A,R3
        MOV   R7,A
REC_C_0:MOV   R5,AR4
	CALL  SND_Bc
REC_C_1:CALL  SND_END
	SJMP  V1_WAIT

REC_CM3:CJNE  A,#uL_GST,REC_CM4
;	JMP   uL_SNST
        JMP   S_WAITD
REC_CM4:

%IF (%DEBUG_FL) THEN (
; Krokovani pocatek
	CJNE  A,#uL_TF0,REC_CM5
	CLR   uLF_TRP       ; Ukonci krokovani
REC_C_2:CALL  ACK_CMD
	JMP   S_WAITD
REC_CM5:CJNE  A,#uL_TF1,REC_CM6
	SETB  uLF_TRP       ; Zacne krokovani
	SJMP  REC_C_2
REC_CM6:CJNE  A,#uL_STP,REC_CM7
	SETB  TI	    ; Provede jednu instrukci
	CALL  S_RETI
	CLR   TI
	CLR   A
	SJMP  REC_C_2
REC_CM7:CJNE  A,#uL_SPC,REC_CM8
	CALL  ACK_CMD
	CALL  SND_BEB
	MOV   A,SP_BUF      ; Vysle PCL PCH PSW ACC
	INC   A
	MOV   R6,A
	ADD   A,#-4
	MOV   R2,A
	CALL  SND_Bi
	JMP   REC_C_1
REC_CM8:CJNE  A,#uL_RDM,REC_CM9

; Vyslani pameti
	CALL  ACK_CMD
	CALL  SND_BEB
	CALL  S_PRPMM
	CJNE  R4,#1,S_RDM1
	CALL  SND_Bi
S_RDM1:
%IF (%XDATA_FL) THEN (
        CJNE  R4,#2,S_RDM2
	CALL  SND_Bx
S_RDM2:	CJNE  R4,#4,S_RDM3
	CALL  SND_Bs
)FI
S_RDM3:	CJNE  R4,#6,S_RDM4
	CALL  SND_Bc
S_RDM4:
	 JMP   REC_C_1

REC_CM9:CJNE  A,#uL_WRM,REC_C10
	CALL  ACK_CMD
	CALL  REC_BEG
	CJNE  R0,#uL_WRM AND 7FH,REC_CME
	CALL  S_PRPMM
	MOV   A,#1
	CJNE  R4,#1,S_WRM1
	CALL  REC_Bi
S_WRM1:
%IF (%XDATA_FL) THEN (
	CJNE  R4,#2,S_WRM2
	CALL  REC_Bx
S_WRM2: CJNE  R4,#4,S_WRM3
	CALL  REC_Bs
S_WRM3: CJNE  R4,#6,S_WRM4    ; zapis do code 89c51rd2?
	CALL  REC_Bc
	JNZ   REC_CME
	CALL  REC_END
	CLR   EA
	MOV   FCON,#050H      ; zapisovaci sequence
	MOV   FCON,#0A0H      ; zapisovaci sequence
S_WRM31:MOV   A,FCON	      ; cekej na dozapsani
	ANL   A,#01H
	JNZ   S_WRM31
	SETB  EA                
        JMP   S_WAITD
S_WRM4: JNZ   REC_CME
	CALL  REC_END
        JMP   S_WAITD
)FI

REC_C10:CJNE  A,#uL_DEB,REC_C11
;S_DEB:	CALL  S_INCP2
;	JZ    NAK_CMD
	CALL  ACK_CMD
	MOV   R0,#BEG_PB
        MOV   A,@R0
        INC   R0
	CJNE  A,#10H,S_DEB98
        INC   R0            ; Prikaz GO xxxx
        INC   R0
	MOV   A,SP_BUF      ; Zapise  PCL PCH
	ADD   A,#-3
	MOV   R1,A
        MOV   A,@R0
	MOV   @R1,A
        INC   R0
	INC   R1
        MOV   A,@R0
	MOV   @R1,A
S_DEB98:JMP   S_WAITD

)FI

REC_C11:
%IF (%COMM_S) THEN (
        CJNE  A,#uL_SYN,NAK_CMD
        CALL  ACK_CMD
	SETB  SYNCH_COMM     ; synchronizace TIRIS
;        SETB  TF2            ; zasynchronizovani
        SETB  IE1            ; zasynchronizovani
	JMP   S_WAITD
)FI

NAK_CMD:MOV   R1,#uL_NAK
	JMP   REC_EN4

WAK_CMD:MOV   R1,#uL_WAK
	JMP   REC_EN4

REC_CME:CALL  REC_ERR
	JMP   REC_CMD

%IF (%MASTER_E) THEN (
; Pripojeni mastera ke sbernici
; vstup vlastni adresy v R0

S_ARB: 	MOV   R1,#3+1         ; 3x2 bitu arbitrace
	CLR   uLF_NB
%IF (%DR_EO_NG) THEN (
S_ARB1:	CLR  DR_EO
)ELSE(
S_ARB1:	SETB  DR_EO
)FI
	JNB   RXD,S_ARBE
	JB    AC,S_ARBE
	CLR   TXD
	CALL  SND_CH1
	SETB  TXD
%IF (%DR_EO_NG) THEN (
	CLR   DR_EO
)ELSE(
	SETB  DR_EO
)FI
S_ARB2:	CALL  SND_CH2
	JB    AC,S_ARB2
	MOV   A,R0          ; Rotace adresy
	RR    A
	RR    A
	XCH   A,R0
	ANL   A,#3          ; Nejnizsi 2 bity
	INC   A
	MOV   R2,A	    ; Pocet cekani
	DJNZ  R1,S_ARB4
	RET
S_ARB3: JB    AC,S_ARBE
	CALL  SND_SPC
S_ARB4:	DJNZ  R2,S_ARB3
	SJMP  S_ARB1

S_ARBE: SETB  F0
	RET

SND_OB1:CJNE  R1,#0,SND_OB2
	CALL  SND_BEG
	SJMP  SND_OB3
SND_OB2:CALL  SND_BEG
	CALL  SND_Bi
SND_OB3:CALL  SND_END
        JMP   S_ENDT
)FI

; Prijem bloku do IDATA

REC_Bi: CLR   SM2	      ; R2  .. where
	SETB  REN	      ; X R45 .. buffer bot/top
%IF (%DR_EO_NG) THEN (
	CLR   DR_EO	      ; R6  .. end of buffer
)ELSE(
	SETB  DR_EO	      ; R6  .. end of buffer
)FI
	CLR   TI              ; R1  =  check sum
REC_Bi1:CALL  S_RET           ; CY  =  1 when ended by CTR
	JB    AC,REC_Bi2
	CLR   TI
	SJMP  REC_Bi1
REC_Bi2:JC    REC_BiR
	PUSH  AR1
	MOV   R1,AR2
	MOV   @R1,A
	POP   AR1
	XRL   AR1,A
	INC   R1
	INC   R2
	MOV   A,R2
	XRL   A,R6
	JNZ   REC_Bi1
	CLR   C
REC_BiR:RET

; Vyslani bloku z IDATA

SND_Bi: CALL  SND_SPS         ; R2  .. where
SND_Bi0:CLR   REN	      ; X R45 .. buffer bot/top
%IF (%DR_EO_NG) THEN (
	SETB  DR_EO	      ; R6  .. end of buffer
)ELSE(
	CLR   DR_EO	      ; R6  .. end of buffer
)FI
	CLR   RI              ; R1  =  check sum
	CLR   TB8
SND_Bi1:PUSH  AR1
	MOV   R1,AR2
	MOV   A,@R1
	MOV   SBUF,A
	POP   AR1
	XRL   AR1,A
	INC   R1
	INC   R2
	MOV   A,R2
	XRL   A,R6
	CLR   TI
	JZ    SND_BiR
SND_Bi2:CALL  S_RET
	JB    AC,SND_Bi2
	SJMP  SND_Bi1
SND_BiR:CALL  S_RET
	CLR   TI
	RET

; Vyslani bloku z CDATA

SND_Bc: CALL  SND_SPS         ; R23 .. where
SND_Bc0:CLR   REN	      ; R45 .. buffer bot/top
%IF (%DR_EO_NG) THEN (
	SETB  DR_EO	      ; R67 .. end of buffer
)ELSE(
	CLR   DR_EO	      ; R67 .. end of buffer
)FI
	CLR   RI              ; R1  =  check sum
	CLR   TB8
SND_Bc1:CALL  S_SNFC
	CLR   TI
	JZ    SND_BcR
SND_Bc2:CALL  S_RET
	JB    AC,SND_Bc2
	SJMP  SND_Bc1
SND_BcR:CALL  S_RET
	CLR   TI
	RET

%IF (%XDATA_FL) THEN (

; Prijem bloku do XDATA

REC_Bx: CLR   SM2	      ; R23 .. where
	SETB  REN	      ; R45 .. buffer bot/top
%IF (%DR_EO_NG) THEN (
	CLR   DR_EO	      ; R67 .. end of buffer
)ELSE(
	SETB  DR_EO	      ; R67 .. end of buffer
)FI
	CLR   TI              ; R1  =  check sum
REC_Bx1:CALL  S_RET           ; CY  =  1 when ended by CTR
	JB    AC,REC_Bx2
	CLR   TI
	SJMP  REC_Bx1
REC_Bx2:JC    REC_BxR
	CALL  S_RCTB1
	JNZ   REC_Bx1
	CLR   C
REC_BxR:RET

; Prijem bloku do CDATA (t89c51rd2)
REC_Bc: CLR   SM2	      ; R23 .. where
	SETB  REN	      ; R45 .. buffer bot/top
%IF (%DR_EO_NG) THEN (
	CLR   DR_EO	      ; R67 .. end of buffer
)ELSE(
	SETB  DR_EO	      ; R67 .. end of buffer
)FI
	CLR   TI              ; R1  =  check sum
REC_Bc1:CALL  S_RET           ; CY  =  1 when ended by CTR
	JB    AC,REC_Bc2
	CLR   TI
	SJMP  REC_Bc1
REC_Bc2:JC    REC_BcR
	CLR   EA
        MOV   FCON,#08H       ; zapis do programove pameti
	CALL  S_RCTB1
        MOV   FCON,#00H       ; vypni zapis do programove pameti
	SETB  EA
	JNZ   REC_Bc1
	CLR   C
REC_BcR:RET

; Vyslani bloku z XDATA
SND_Bx: CALL  SND_SPC         ; R23 .. where
SND_BX0:CLR   REN	      ; R45 .. buffer bot/top
%IF (%DR_EO_NG) THEN (
	SETB  DR_EO	      ; R67 .. end of buffer
)ELSE(
	CLR   DR_EO	      ; R67 .. end of buffer
)FI
	CLR   RI              ; R1  =  check sum
	CLR   TB8
SND_Bx1:CALL  S_SNFB
	CLR   TI
	JZ    SND_BxR
SND_Bx2:CALL  S_RET
	JB    AC,SND_Bx2
	SJMP  SND_Bx1
SND_BxR:CALL  S_RET
	CLR   TI
	RET

; Vyslani bloku z SDATA

SND_Bs: CALL  SND_SPC         ; R2  .. where
SND_Bs0:CLR   REN	      ; X R45 .. buffer bot/top
%IF (%DR_EO_NG) THEN (
	SETB  DR_EO	      ; R6  .. end of buffer
)ELSE(
	CLR   DR_EO	      ; R6  .. end of buffer
)FI
	CLR   RI              ; R1  =  check sum
	CLR   TB8
SND_Bs1:PUSH  DPL
	PUSH  DPH
	MOV   DPTR,#XPR_BUF
	MOV   A,#0E5H         ; MOV A,dir
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R2            ; adresa
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#022H         ; RET
	MOVX  @DPTR,A
	INC   DPTR
	POP   DPH
	POP   DPL
	DB    12H             ; CALL XPR_BUF
	DW    XPR_BUF
	MOV   SBUF,A
	XRL   AR1,A
	INC   R1
	INC   R2
	MOV   A,R2
	XRL   A,R6
	CLR   TI
	JZ    SND_BsR
SND_Bs2:CALL  S_RET
	JB    AC,SND_Bs2
	SJMP  SND_Bs1
SND_BsR:CALL  S_RET
	CLR   TI
	RET

; Prijem bloku do SDATA

REC_Bs: CLR   SM2	      ; R2  .. where
	SETB  REN	      ; X R45 .. buffer bot/top
%IF (%DR_EO_NG) THEN (
	CLR   DR_EO	      ; R6  .. end of buffer
)ELSE(
	SETB  DR_EO	      ; R6  .. end of buffer
)FI
	CLR   TI              ; R1  =  check sum
REC_Bs1:CALL  S_RET           ; CY  =  1 when ended by CTR
	JB    AC,REC_Bs2
	CLR   TI
	SJMP  REC_Bs1
REC_Bs2:JC    REC_BsR
	XRL   AR1,A
	INC   R1
	PUSH  DPL
	PUSH  DPH
	MOV   R3,A
	MOV   DPTR,#XPR_BUF
	MOV   A,#08BH         ; MOV dir,R3
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R2            ; adresa
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,#022H         ; RET
	MOVX  @DPTR,A
	INC   DPTR
	POP   DPH
	POP   DPL
	DB    12H             ; CALL XPR_BUF
	DW    XPR_BUF
	INC   R2
	MOV   A,R2
	XRL   A,R6
	JNZ   REC_Bs1
	CLR   C
REC_BsR:RET

S_SNFB: XCH   A,R3          ; Vyslani znaku z bufferu
	XCH   A,DPH
	XCH   A,R3
	XCH   A,R2
	XCH   A,DPL
	XCH   A,R2
	MOVX  A,@DPTR
	MOV   SBUF,A
	XCH   A,R1          ; Check sum
	XRL   A,R1
	INC   A
	XCH   A,R1
	SJMP  S_ACCT1

S_R0FB: XCH   A,R3          ; Naplneni R0 z bufferu
	XCH   A,DPH
	XCH   A,R3
	XCH   A,R2
	XCH   A,DPL
	XCH   A,R2
	MOVX  A,@DPTR
	MOV   R0,A
	SJMP  S_ACCT1

S_RCTB: MOV   A,SBUF        ; Prijem znaku do bufferu
S_RCTB1:XCH   A,R1          ; Check sum
	XRL   A,R1
	INC   A
	XCH   A,R1
S_ACCTB:XCH   A,R3
	XCH   A,DPH
	XCH   A,R3
	XCH   A,R2
	XCH   A,DPL
	XCH   A,R2
	MOVX  @DPTR,A
	SJMP  S_ACCT1
)FI

S_SNFC: XCH   A,R3          ; Vyslani znaku z CODE
	XCH   A,DPH
	XCH   A,R3
	XCH   A,R2
	XCH   A,DPL
	XCH   A,R2
	CLR   A
	MOVC  A,@A+DPTR
	MOV   SBUF,A
	XCH   A,R1          ; Check sum
	XRL   A,R1
	INC   A
	XCH   A,R1
	SJMP  S_ACCT1

S_ACCT1:XCH   A,R3
	XCH   A,DPH
	XCH   A,R3
	XCH   A,R2
	XCH   A,DPL
	XCH   A,R2

S_INCP: INC   R2            ; Pripraveni nasledujici adresy
	CJNE  R2,#0,S_INCP1
	INC   R3
S_INCP1:MOV   A,R5
	XRL   A,R3
	JNZ   S_INCP2
	MOV   A,R4
	MOV   R3,A
S_INCP2:MOV   A,R2          ; Kontrola prostoru pro data
	XRL   A,R6          ; ACC=0 => konec dat nebo preteceni IB
	JNZ   S_INCP3
	MOV   A,R3
	XRL   A,R7
S_INCP3:RET

; Vyslani konce ramce
; podle CMD vysle uL_ARQ, uL_AAP nebo uL_END

SND_END:MOV   A,uL_CMD        ; Vysilany prikaz
	XCH   A,R0
	MOV   A,uL_SA         ; Adresa spojeni
	JZ    SND_EN1
	JB    ACC.6,SND_EN1
	JNB   uLF_SN,SND_EN1
	XCH   A,R0
	MOV   R0,#uL_AAP
	JB    ACC.7,SND_EN2
	MOV   R0,#uL_ARQ
	JNB   ACC.6,SND_EN2
SND_EN1:MOV   R0,#uL_END
SND_EN2:MOV   A,R0
	XRL   AR1,A
	INC   R1
	CALL  SND_CTR         ; Vyslani zakonceni
	MOV   A,R1
	CALL  SND_CHR         ; Vyslani checksum
	CJNE  R0,#uL_END,SND_EN3
	RET
SND_EN3:MOV   R0,#5
SND_EN4:CALL  WTF_CHR
	JNB   AC,SND_EN4      ; Prijeti uL_ACK
	JC    V3_ERR
	CJNE  A,#uL_ACK,V3_ERR
	RET

; Vyslani uL_NAK pri chybe v nektere z  rutin zpracovani prikazu
REC_ERR:CALL  REC_CTR
	JNB   AC,REC_ERR
REC_ER2:CALL  CMP_END         ; Vstup s prijatym znakem v ACC a CY
	JC    V3_ERR
	MOV   R1,AR0
	MOV   R0,#5
REC_ER3:CALL  WTF_CHR         ; Prijem checksum
	JNB   AC,REC_EN3
	MOV   R0,AR1
	MOV   R1,#uL_NAK
	SJMP  REC_EN4

; Potvrzeni prikazu
ACK_CMD:MOV   R1,#uL_ACK
	SJMP  REC_EN4

; Nacteni konce ramce
; ret: ACC = CMP_END

REC_END:MOV   R0,#5
REC_EN1:CALL  WTF_CHR         ; Prijem zakonceni ramce
	JNB   AC,REC_EN1
REC_EN2:XRL   AR1,A           ; Vstup s prijatym znakem v ACC a CY
	INC   R1
	CALL  CMP_END
	JC    V3_ERR
	CALL  SND_SPC         ; Prijem checksum
	JB    AC,REC_EN3
	CALL  SND_SPC
	JB    AC,REC_EN3
	CALL  SND_SPC
	JB    AC,REC_EN3
	CALL  SND_SPC
	JB    AC,REC_EN3
REC_E3N:CALL  SND_SPC
	JB    AC,REC_EN3
	SJMP  V3_ERR
REC_EN3:JC    V3_ERR
	XRL   A,R1
	MOV   R1,#uL_NAK
	JNZ   REC_EN4         ; Pri chybe a ARQ nebo AAP vysle NAK
	MOV   R1,#uL_ACK      ; Pri   OK  a ARQ vysle ACK
	MOV   A,R0            ; Pri   OK  a AAP je R1=ACK ale vysle
	JB    ACC.1,REC_EN5   ;  az prooceed rutina
REC_EN4:MOV   A,R0
	JNB   ACC.0,REC_EN5
	SETB  REN
	CALL  SND_SPT
	CALL  SND_SPC
	MOV   A,R1
	CALL  SND_CHR
REC_EN5:CJNE  R1,#uL_NAK,REC_EN6
V3_ERR: JMP   S_ERR
REC_EN6:MOV   A,R0
	RET

; Pocatek ramce bez urceni Destignation Address

SND_BEB:MOV   A,uL_CMD        ; Vysilany prikaz
	ANL   A,#7FH
	MOV   R0,A
	MOV   A,#uL_BEG

; Vyslani zacatku ramce
; call: ACC  Destignation Address
;       R0   CoMmanD

SND_BEG:ANL   A,#07FH
	MOV   R1,A
	CALL  SND_SPT
	CALL  SND_SPS
	MOV   A,R0            ; Vysilany prikaz
	MOV   uL_CMD,A        ; Zapis CMD
	MOV   A,R1            ; Sdresa spojeni
	MOV   uL_SA,A         ; Zapis SA
	INC   R1
	CALL  SND_CTR         ; Destignation Address nebo uL_BEG
	MOV   A,uL_ADR        ; Vlastni adresa
	XRL   AR1,A
	INC   R1
	CALL  SND_CHR
	MOV   A,R0
	XRL   AR1,A
	INC   R1
	JMP   SND_CHR

; Nacteni zacatku ramce
; call: R1=uL_BEG je Selected WAIT jinak Pasive WAIT
; ret:  ACC  Source Address
;       R0   CoMmanD

REC_BEG:MOV   R1,#uL_BEG       ; Cekani ze SWAIT
	MOV   R0,#9
REC_BE1:CALL  WTF_CHR
	JNB   AC,REC_BE1
REC_BE2:JNC   V4_ERR
	JB    uLF_NB,V4_ERR    ; Cekani z  PWAIT s R1=0
	MOV   R0,A
	JZ    REC_BE3         ; Obecna adresa
	XRL   A,uL_ADR
	JZ    REC_BE3         ; Vlastni adresa
	CJNE  R1,#uL_BEG,V4_ERR
	CJNE  A,#uL_BEG,S_EWAIT    ; S_BEG a SWAIT
REC_BE3:MOV   AR1,R0          ; **********************
	INC   R1     	      ; V R1 se bude pocitat chksum
	MOV   R0,#5
REC_BE4:CALL  WTF_CHR
	JNB   AC,REC_BE4
	JC    V4_ERR
	XRL   AR1,A
	INC   R1
	MOV   R0,A
REC_BE5:CALL  REC_CHR
	JNB   AC,REC_BE5      ; Cekani na  CMD -prikaz
	JC    V4_ERR
	XRL   AR1,A
	INC   R1
	MOV   uL_CMD,A        ; Zapis CMD
	XCH   A,R0
	MOV   uL_SA,A	      ; Zapis SA
	RET

; Cekani na konec bloku pri REC_BEG a SWAIT
S_EWAIT:CALL  REC_CTR
	JNB   AC,S_EWAIT
	CALL  CMP_END
	JC    V4_ERR
	MOV   A,R0
	JNB   ACC.0,REC_BEG
	MOV   R0,#5
S_EWAI1:CALL  WTF_CHR
	JNB   AC,S_EWAI1
	JNC   REC_BEG
V4_ERR:	JMP   S_ERR

; Vrati v R0  0..S_END,1..S_ARQ,2..S_PRQ,3..S_AAP,JINAK CY
CMP_END:JNC   CMP_EN4
CMP_EN0:MOV   R0,#0
	CJNE  A,#uL_END,CMP_EN1
	RET
CMP_EN1:INC   R0
	CJNE  A,#uL_ARQ,CMP_EN2
	RET
CMP_EN2:INC   R0
	CJNE  A,#uL_PRQ,CMP_EN3
	RET
CMP_EN3:INC   R0
	CJNE  A,#uL_AAP,CMP_EN4
	RET
CMP_EN4:SETB  C
	RET

%IF (%DEBUG_FL) THEN (
; Priprava pro cteni a zapis pameti

S_PRPMM:MOV   R0,#BEG_PB
	MOV   A,@R0
	MOV   R4,A
	INC   R0
	MOV   A,@R0
	MOV   R7,A
	INC   R0
	MOV   A,@R0
	MOV   R2,A
	INC   R0
	MOV   A,@R0
	MOV   R3,A
	INC   R0
	MOV   A,@R0
	MOV   R6,A
	INC   R0
	MOV   A,@R0
	XCH   A,R7
	MOV   R0,A
	CJNE  R4,#9,S_PRPM2
	MOV   A,#SP_BUF
	SJMP  S_PRPM4
S_PRPM2:CJNE  R4,#8,S_PRPM3
	MOV   A,SP_BUF
	INC   A
	SJMP  S_PRPM4
S_PRPM3:CJNE  R4,#7,S_PRPM5
	MOV   R0,SP_BUF
	DEC   R0
	MOV   A,@R0
	ANL   A,#18H
S_PRPM4:ADD   A,R2
	MOV   R2,A
	MOV   R4,#1
S_PRPM5:MOV   AR5,R4
)FI
; Pricte k R67 R23

AR67R23:MOV   A,R6
	ADD   A,R2
	MOV   R6,A
	MOV   A,R7
	ADDC  A,R3
	MOV   R7,A
	CLR   C
	SUBB  A,R5
	JC    AR67R6R
	ADD   A,R4
	MOV   R7,A
AR67R6R:RET

;=================================================================

RSEG    PLAN__C

uL_FNC: SETB  F0
	RET

uL_STR: JBC   ES,uL_STRG
	RET
uL_STRG:MOV   C,EA
	CLR   EA
	JNB   uLF_NA,uL_STRE
	JNB   uLF_NB,uL_STRF
	JNB   uLF_RS,uL_STRF
	SETB  TI
uL_STRF:SETB  uLF_NB
uL_STRE:SETB  uLF_NA
	SETB  ES
	MOV   EA,C
	RET

uL_SND: SETB  uLF_RS
	JB    uLF_NB,uL_SN1
	JB    ES,uL_SN1
	SETB  TI
uL_SN1:	RET

_uL_INIT:CLR   ES            ; Funkce spusti RS485
 	CLR   ET1            ; R7 = baud divisor, R5 - adresa ucastnika
%IF (%DR_EO_NG) THEN (
	CLR   DR_EO
)ELSE(
	SETB  DR_EO
)FI
;	CLR   DR_EI
;	SETB  PS
;	CLR   PT1
	ANL   TMOD,#00FH
	ORL   TMOD,#020H
	MOV   SCON,#11010000B
	ORL   PCON,#10000000B ; Bd = OSC/12/16/(256-TH1)
	MOV   A,R7
	JNZ   uL_INI1
	MOV   A,#S_SPEED
uL_INI1:CPL   A
	INC   A
	MOV   TH1,A
	SETB  TR1
        MOV   A,R5            ;adresa
	MOV   uL_ADR,A
	CLR   A
	MOV   SP_BUF,A
	MOV   uL_FLG,A
	MOV   R0,#BEG_IB
	MOV   @R0,A
	MOV   R0,#BEG_OB
	MOV   @R0,A
	MOV   SBUF,#0
%IF (%VECTOR_FL) THEN (
	MOV   R4,#SINT
	MOV   DPTR,#S_INT
	CALL  VEC_SET
) FI
	SETB  ES
	RET

	END
