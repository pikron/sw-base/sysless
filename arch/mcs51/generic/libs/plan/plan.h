#ifndef _plan_h
#define _plan_h


//******************************************************************************
// Prikazy posilane linkou
// 00H .. 3FH    uloz do bufferu
// 40H .. 7FH    uloz do bufferu bez ACK
// 80H .. 9FH    okamzite proved a konci
// A0H .. BFH    proved a prijimej
// C0H .. FFH    proved a vysilej

//******************************************************************************
//Rizeni linky - prikazy
#define 	uL_ERRI 	0xFF 		//Ignoruj vse doslo k chybe
#define		uL_ERR  	0x7F 		//Chyba v datech
#define		uL_END  	0x7C		//Konec dat
#define		uL_ARQ  	0x7A		//Konec dat - vysli ACK
#define		uL_PRQ		0x79		//Konec dat - proved prikaz
#define		uL_AAP		0x76		//ARQ + PRQ
#define		uL_BEG		0x75H		//Zacatek dat

//******************************************************************************
//Potvrzovaci zpravy
#define		uL_ACK		0x19		//Potvrzeni
#define		uL_NAK		0x7F		//Doslo k chybe
#define		uL_WAK		0x25		//Ted nemohu splnit

//******************************************************************************
extern __code ACK_CMD(),SND_BEB(),SND_CHC(),SND_END(),S_WAITD(),NAK_CMD();
extern __code REC_BEG(),REC_CHR(),REC_END,REC_CME();
extern __code REC_Bi(),SND_Bi(),SND_Bc();
extern __data unsigned char uL_ADR,uL_CMD,uL_SA;
extern __idata unsigned char BEG_PB[],BEG_OB[],BEG_IB[];

//******************************************************************************
//Rutiny pro spolupraci s PLAN mino preruseni
extern __bit uLF_ERR,uLF_SN,uLF_RS,uLF_NB,uLF_NA;
extern __code uL_INIT(unsigned char bauderate,unsigned char sadr),uL_STR(),uL_SND();

typedef struct ul_idstr_t {
  char *name;
  char len; 
} ul_idstr_t;

//*****************************************************************************
unsigned char 
pl_send(unsigned char dadr,unsigned char req,unsigned char *pmsg,unsigned char len);
unsigned char 
pl_recv(unsigned char *sadr,unsigned char *req,unsigned char *pmsg,unsigned char *len);

#endif /* _plan_h */
