#include <cpu_def.h>
#include <plan.h>

#define __STRINGIFY(x)     #x              /* stringify without expanding x */
#define STRINGIFY(x)    __STRINGIFY(x)        /* expand x, then stringify */

#define NAME ".mt " \
	     STRINGIFY(APPID) \
	     " .uP " \
	     STRINGIFY(MACH) \
	     " .c " \
              __DATE__ " " __TIME__

ul_idstr_t CODE ul_idstr = {NAME, sizeof(NAME)-1};
