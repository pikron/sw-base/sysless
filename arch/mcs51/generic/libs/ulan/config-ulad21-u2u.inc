
%DEFINE (FOR_SDCC)       (1)     ; The SDCC ASX8051 is used
%DEFINE (VECTOR_FL)      (1)     ; Vyuzivaji se vektory preruseni v RAM
%DEFINE (WITH_INTR)	 (1)	 ; Pouziva se prerusovaci rutina pro odber znaku
%DEFINE (NEG_DR_EO)      (0)     ; Negovana hodnota DR_EO
%DEFINE (WITH_TAILS)     (1)     ; Povolit podporu tailovanych zprav
%DEFINE (CX_MERGED_FL)   (0)     ; CODE a XDATA zcela sloucena
%DEFINE (TGT_T89C51RD2)  (0)     ; Kod pro T89C51RD2
%DEFINE (TGT_MSC1210)    (1)     ; Kod pro MSC1210
%DEFINE (DY_ADDR)        (0)     ; Dynamicka adresace bez vektoru
%DEFINE (WITH_SYNCH)     (0)     ; Synchronizace casu

%DEFINE	(SPN)	         (1)     ; Cislo serioveho portu

SER_STACK_EXT  EQU   0	  	 ; Pridavna hodnota pro stack

END_RAM XDATA 0F000H   		 ; Konec pameti
LENG_IB EQU   32       		 ; Delka vstupniho bufferu v 256 bytu
LENG_OB EQU   8       		 ; Delka vystupniho bufferu
S_SPEED EQU   3       		 ; Bd=OSC/12/16/S_SPEED pro 19200 pri 11.0592 MHz
S_FRLN  EQU   1024    		 ; maximalni delka ramce

DR_EO   BIT   P3.5    		 ; Aktivni v dle NEG_DR_EO
;DR_EO   BIT   P3.4    		 ; Aktivni v dle NEG_DR_EO
;DR_EI   BIT   P1.6    		 ; Aktivni v 0 pro RS-486
                    		 ; Aktivni v 1 pro RS-232

%DEFINE	(CPU_SYS_HZ)  (18432000) ; Hodinova frekvence potrebna pro flashovani MSC
