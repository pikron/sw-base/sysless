#include <cpu_def.h>
#include "ul_drv_fnc.h"

#if defined(SDCC) || defined(__SDCC)
#ifndef __SDCC_MODEL_LARGE
__data uchar ul_dy_len;
#endif /* SDCC_MODEL_LARGE */
#endif /* SDCC */

#if defined(SDCC) || defined(__SDCC)
unsigned long ul_drv_get_sn() 
{
  __asm
	mov  dptr,#ser_num
	movx a,@dptr
	mov  r2,a
	inc  dptr
	movx a,@dptr
	mov  r3,a
	inc  dptr
	movx a,@dptr
	mov  b,a
	inc  dptr
	movx a,@dptr
	mov  dpl,r2
	mov  dph,r3
  __endasm;
}
#else
#error "unsuported compiler!"
#endif

#if defined(SDCC) || defined(__SDCC)
void ul_drv_set_sn(unsigned long sn) 
{
  __asm
	mov  r2,a
	mov  r3,dpl
	mov  r4,dph
	mov  dptr,#ser_num
	mov  a,r3
	movx @dptr,a
	inc  dptr
	mov  a,r4
	movx @dptr,a
	inc  dptr
	mov  a,b
	movx @dptr,a
	inc  dptr
	mov  a,r2
	movx @dptr,a
  __endasm;
}
#else
#error "unsuported compiler!"
#endif


#if defined(SDCC) || defined(__SDCC)
ul_idstr_t *ul_drv_get_idstr() 
{
  __asm
	mov  dptr,#_ul_idstr
	mov  b,#0x80
  __endasm;
}
#else
#error "unsuported compiler!"
#endif

#if defined(SDCC) || defined(__SDCC)
unsigned char ul_drv_get_adr()
{
  __asm
	mov  dptr,#ul_adr
	movx a,@dptr
	mov  dpl,a
  __endasm;
}
#else
#error "unsuported compiler!"
#endif

#if defined(SDCC) || defined(__SDCC)
unsigned char ul_drv_get_dysa()
{
  __asm
	mov  dptr,#ud_dysa
	movx a,@dptr
	mov  dpl,a
  __endasm;
}
#else
#error "unsuported compiler!"
#endif

#if defined(SDCC) || defined(__SDCC)
char ul_drv_rqa() UL_FNC_NAKED
{ 
  __asm
	.globl ul_t_rqa
	lcall ul_t_rqa
	mov   dpl,a
    #ifdef UL_WITH_NAKED
	ret
    #endif /*UL_WITH_NAKED*/
  __endasm;
}
#else
#error "unsuported compiler!"
#endif

#if defined(SDCC) || defined(__SDCC)
void ul_drv_clr_rq() 
{ 
  __asm
	mov  a,ul_fld
	anl  a,#0xF8
	mov  ul_fld,a
  __endasm;
}
#else
#error "unsuported compiler!"
#endif

#if defined(SDCC) || defined(__SDCC)
#ifndef __SDCC_MODEL_LARGE
void ul_drv_set_status(uchar UL_ARGPTRTYPE *status,uchar len) 
{
  ul_dy_len=len;
  __asm
	mov  r2,dpl
	mov  r3,dph
	mov  dptr,#ud_stad
	mov  a,r2
	movx @dptr,a
	inc  dptr
	mov  a,r3
	movx @dptr,a
	inc  dptr
	mov  a,_ul_dy_len
	movx @dptr,a   
  __endasm;
}
#else
void ul_drv_set_status(uchar UL_ARGPTRTYPE *status,uchar len) UL_FNC_REENTRANT UL_FNC_NAKED
{
  __asm
	mov  a,sp
    #ifdef UL_WITH_NAKED
	add  a,#-3
    #else /*UL_WITH_NAKED*/
	add  a,#-4
    #endif /*UL_WITH_NAKED*/
	mov  r0,a
	mov  a,@r0
	mov  r4,a
	mov  r2,dpl
	mov  r3,dph
	mov  dptr,#ud_stad
	mov  a,r2
	movx @dptr,a
	inc  dptr
	mov  a,r3
	movx @dptr,a
	inc  dptr
	mov  a,r4
	movx @dptr,a
    #ifdef UL_WITH_NAKED
	ret
    #endif /*UL_WITH_NAKED*/
  __endasm;
}
#endif /* SDCC_MODEL_LARGE */
#else
#error "unsuported compiler!"
#endif
