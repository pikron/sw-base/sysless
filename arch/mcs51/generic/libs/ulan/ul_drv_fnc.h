#ifndef _UL_DRV_FNC_H
#define _UL_DRV_FNC_H

#include <ul_lib/ulan.h>

#ifdef __cplusplus
/*extern "C" {*/
#endif

unsigned long ul_drv_get_sn();
void ul_drv_set_sn(unsigned long sn);
ul_idstr_t *ul_drv_get_idstr();
void ul_drv_set_status(uchar UL_ARGPTRTYPE *status,uchar len) UL_FNC_REENTRANT UL_FNC_NAKED;
unsigned char ul_drv_get_adr();
unsigned char ul_drv_get_dysa();
char ul_drv_rqa() UL_FNC_NAKED;
void ul_drv_clr_rq();

#ifdef __cplusplus
/*}*/ /* extern "C"*/
#endif

#endif /* _UL_DYAC_H */
