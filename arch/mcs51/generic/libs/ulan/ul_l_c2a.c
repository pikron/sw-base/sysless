/*******************************************************************
  uLan Communication - version for generic MCS51 core based MCU

  ul_l_c2a.c      - C language bindings to low level assembly
                    implementation

  Copyright holders and originators:
      (C) 2002-2003 by Pavel Pisa pisa@cmp.felk.cvut.cz
      (C) 2002-2003 by PiKRON Ltd. http://www.pikron.com

  The more uLan related information can be foud at next sites
            http://cmp.felk.cvut.cz/~pisa/ulan/
            http://www.pikron.com/en/chromul_over.html

  The uLan for small embedded devices can be used, modified
  and redistributed if at least conditions of one of the
  following licenses are met
     - GPL - General Public License
     - LGPL - Lesser General Public License
     - MPL - Mozilla Public License
     - other license conditions assigned by originators

  If significant code contributor disagrees with some of above
  licenses, he or she can delete such line/s, but if he deletes
  all lines, he is not allowed redistribute code in any binary
  or source form.

 *******************************************************************/


/*#define  UL_WITH_HANDLE*/

#include "ulan.h"

/*pragma LESS_PEDANTIC*/

#define SDCC_PTR_DATA 0x40
#define SDCC_PTR_XDATA 0x00
#define SDCC_PTR_CODE 0x80

#define KEIL_PTR_DATA 0x00
#define KEIL_PTR_XDATA 0x01
#define KEIL_PTR_CODE 0x02

#if defined(SDCC) || defined(__SDCC)
#ifndef __SDCC_MODEL_LARGE
__data int ul_c2a_len;
#endif /* SDCC_MODEL_LARGE */
#endif /*SDCC*/

#ifndef UL_WITH_HANDLE
ul_fd_t	ul_fd;
ul_msginfo __xdata msginfo;
#endif

ul_fd_t	ul_open(const char *dev_name, const char *options)
{
  return 1;
}

int ul_close(ul_fd_t ul_fd)
{
  return 0;
}

#if defined(SDCC) || defined(__SDCC)
ul_ssize_t __ul_read_avail() UL_FNC_NAKED
{
  __asm
	.globl ul_i_l
	clr  f0
	lcall ul_i_l
	mov  dpl,r4
	mov  dph,r5
    #ifdef UL_WITH_NAKED
	ret
    #endif /*UL_WITH_NAKED*/
  __endasm;
}
#else /* SDCC */

ul_ssize_t __ul_read_avail(void) UL_FNC_NAKED
{
  #pragma asm
	extrn code(UL_I_L)
	clr  f0
	call UL_I_L
	mov  a,r4
	mov  r7,a
	mov  a,r5
	mov  r6,a
  #pragma endasm
}
#endif /* SDCC */


#if defined(SDCC) || defined(__SDCC)
#ifndef __SDCC_MODEL_LARGE
/* ul_read function for small model */
ul_ssize_t __ul_read(void *buffer, ul_size_t size) UL_FNC_NAKED
{
  ul_c2a_len=size;
  __asm
	.globl ul_rd
	mov  r4,_ul_c2a_len
	mov  r5,_ul_c2a_len+1
	clr  f0
	jb    b.6,00001$ /*SDCC_PTR_DATA*/
	lcall ul_rd
	sjmp 00002$
00001$:	lcall ul_rdi
00002$:	jb   f0,00004$
	mov  dpl,_ul_c2a_len
	mov  dph,_ul_c2a_len+1
00003$:
    #ifdef UL_WITH_NAKED
	ret
    #else /*UL_WITH_NAKED*/
	sjmp 00005$
    #endif /*UL_WITH_NAKED*/
00004$:
	clr  c
	mov  a,_ul_c2a_len
	subb a,r4
	mov  dpl,a
	mov  a,_ul_c2a_len+1
	subb a,r5
	mov  dph,a
    #if 0
	orl  a,dpl
	jnz  00003$
	mov  dptr,#0xffff
    #endif
    #ifdef UL_WITH_NAKED
	ret
    #endif /*UL_WITH_NAKED*/
00005$:
  __endasm;
}
#else /* SDCC_MODEL_LARGE */
/* For large model
 * size   Allocated to stack - offset -4  (the push _bp taken into account)
 * buffer Allocated to registers 
 */
ul_ssize_t __ul_read(void *buffer, ul_size_t size) __reentrant UL_FNC_NAKED 
{
  __asm
	.globl ul_rd
	mov  a,sp
	clr  f0
	mov  a,sp
    #ifdef UL_WITH_NAKED
	add  a,#-3
    #else /*UL_WITH_NAKED*/
	add  a,#-4
    #endif /*UL_WITH_NAKED*/
	mov  r0,a
	mov  a,@r0
	mov  r4,a
	mov  r6,a
	inc  r0
	mov  a,@r0
	mov  r5,a
	mov  r7,a
	jb    b.6,00001$  /*SDCC_PTR_DATA*/
	lcall ul_rd
	sjmp 00002$
00001$:	lcall ul_rdi
00002$:	jb   f0,00004$
	mov  dpl,r6
	mov  dph,r7
00003$:
    #ifdef UL_WITH_NAKED
	ret
    #else /*UL_WITH_NAKED*/
	sjmp 00005$
    #endif /*UL_WITH_NAKED*/
00004$:
	clr  c
	mov  a,r6
	subb a,r4
	mov  dpl,a
	mov  a,r7
	subb a,r5
	mov  dph,a
    #if 0
	orl  a,dpl
	jnz  00003$
	mov  dptr,#0xffff
    #endif
    #ifdef UL_WITH_NAKED
	ret
    #endif /*UL_WITH_NAKED*/
00005$:
  __endasm;
}
#endif /* SDCC_MODEL_LARGE */
#else /* SDCC */

#pragma asm
;ul_ssize_t __ul_read(void *buffer, ul_size_t size) UL_FNC_NAKED UL_FNC_REENTRANT
;{
	extrn code(UL_RD,UL_RDi)
	public ___ul_read
___ul_read:
	mov  dpl,r1	; buffer address into DPTR
	mov  dph,r2
	mov  a,r4	; number of bytes to write
	mov  r6,a
	xch  a,r5	; into R45 little endian
	mov  r4,a
	mov  r7,a
	clr  f0
	mov  a,R3
/*	cjne A,#KEIL_PTR_XDATA,__ul_read_l1 */
	cjne A,#01h,__ul_read_l1
	call UL_RD	; call read => R45 number of remaining
	sjmp __ul_read_l2 ; bytes, DPTR moved after read data
__ul_read_l1:
	call UL_RDi
__ul_read_l2:
	jnb  f0,__ul_read_l3
	clr  c
	mov  a,r7
	subb a,r4
	mov  r7,a
	mov  a,r6	; compute number of read bytes => R76
	subb a,r5
	mov  r6,a
    #if 0
	orl  a,r7
	jnz  __ul_read_l3
	dec  r6		; return -1 if no byte read
	dec  r7
    #endif
__ul_read_l3:
	ret
;}
#pragma endasm
#endif /* SDCC */

#if defined(SDCC) || defined(__SDCC)
#ifndef __SDCC_MODEL_LARGE
/* ul_write function for small model */
ul_ssize_t __ul_write(const void *buffer, ul_size_t size) UL_FNC_NAKED
{
  ul_c2a_len=size;
  __asm
	.globl ul_wr
	.globl ul_wrc
	.globl ul_wri
	mov  r4,_ul_c2a_len
	mov  r5,_ul_c2a_len+1
	clr  f0
	jnb   b.6,00001$  /*SDCC_PTR_DATA*/
	lcall ul_wri
	sjmp 00003$
00001$:
	jnb   b.7,00002$  /*SDCC_PTR_CODE*/
	lcall ul_wrc
	sjmp 00003$
00002$:
	lcall ul_wr
00003$:
	jb   f0,00004$
	mov  dpl,_ul_c2a_len
	mov  dph,_ul_c2a_len+1
    #ifdef UL_WITH_NAKED
	ret
    #else /*UL_WITH_NAKED*/
	sjmp 00005$
    #endif /*UL_WITH_NAKED*/
00004$:
	mov  dptr,#0xffff
    #ifdef UL_WITH_NAKED
	ret
    #endif /*UL_WITH_NAKED*/
00005$:
  __endasm;
}
#else /* SDCC_MODEL_LARGE */
/* For large model
 * size   Allocated to stack - offset -4 (the push _bp taken into account)
 * buffer Allocated to registers
 */
ul_ssize_t __ul_write(const void *buffer, ul_size_t size) __reentrant UL_FNC_NAKED
{
  __asm
	.globl ul_wr
	.globl ul_wrc
	.globl ul_wri
	mov  a,sp
	clr  f0
	mov  a,sp
    #ifdef UL_WITH_NAKED
	add  a,#-3
    #else /*UL_WITH_NAKED*/
	add  a,#-4
    #endif /*UL_WITH_NAKED*/
	mov  r0,a
	mov  a,@r0
	mov  r4,a
	mov  r6,a
	inc  r0
	mov  a,@r0
	mov  r5,a
	mov  r7,a
	jnb   b.6,00001$  /*SDCC_PTR_DATA*/
	lcall ul_wri
	sjmp 00003$
00001$:
	jnb   b.7,00002$  /*SDCC_PTR_CODE*/
	lcall ul_wrc
	sjmp 00003$
00002$:
	lcall ul_wr
00003$:
	jb   f0,00004$
	mov  dpl,r6
	mov  dph,r7
    #ifdef UL_WITH_NAKED
	ret
    #else /*UL_WITH_NAKED*/
	sjmp 00005$
    #endif /*UL_WITH_NAKED*/
00004$:
	mov  dptr,#0xffff
    #ifdef UL_WITH_NAKED
	ret
    #endif /*UL_WITH_NAKED*/
00005$:
  __endasm;
}
#endif /* SDCC_MODEL_LARGE */
#else /* SDCC */

#pragma asm
;ul_ssize_t __ul_write(const void *buffer, ul_size_t size) UL_FNC_NAKED UL_FNC_REENTRANT
;{
	extrn code(UL_WR,UL_WRC,UL_WRI)
	public ___ul_write
___ul_write:
	mov  dpl,r1	; buffer address into DPTR
	mov  dph,r2
	mov  a,r4	; number of bytes to write
	mov  r6,a
	xch  a,r5	; into R45 little endian
	mov  r4,a
	mov  r7,a
	clr  f0		; call read => R45 number of remaining
	mov  a,R3
/*	cjne A,#KEIL_PTR_XDATA,__ul_write_l3 */
	cjne A,#01h,__ul_write_l3 
	call UL_WR	; bytes, DPTR moved after read data
	sjmp __ul_write_l2
__ul_write_l3:
/*	cjne A,#KEIL_PTR_CODE,__ul_write_l4 */
	cjne A,#02h,__ul_write_l4
	call UL_WRC
	sjmp __ul_write_l2
__ul_write_l4:
/*	cjne A,#KEIL_PTR_DATA,__ul_write_l2 */
	cjne A,#00h,__ul_write_l2
	call UL_WRI
__ul_write_l2:
	jnb  f0,__ul_write_l1
	mov  r6,#0ffh	; return -1 if there was problem
	mov  r7,#0ffh
__ul_write_l1:
	ret
;}
#pragma endasm
#endif /* SDCC */


#if defined(SDCC) || defined(__SDCC)
int	__ul_newmsg(const ul_msginfo UL_ARGPTRTYPE  *msginfo) UL_FNC_NAKED
{
  /* Dadr R4, Com R5, Status R6, Stamp R7 */
  __asm
	.globl ul_o_op
	.globl ul_o_new
	movx a,@dptr	/* dadr */
	mov  r4,a
	inc  dptr	/* sadr */
	/*movx a,@dptr*/
	/*uL_ISAD*/
	inc  dptr
	movx a,@dptr	/* cmd */
	mov  r5,a
	inc  dptr
	movx a,@dptr	/* flg */
	mov  r6,a
	inc  dptr
	movx a,@dptr	/* stamp */
	mov  r7,a
	clr  f0
	lcall ul_o_new
	mov  dptr,#0xffff
	jb   f0,00020$
	inc  dptr
00020$:
    #ifdef UL_WITH_NAKED
	ret
    #endif /*UL_WITH_NAKED*/
  __endasm;
}
#else /* SDCC */
#pragma asm
;int	__ul_newmsg(const ul_msginfo UL_ARGPTRTYPE  *msginfo) UL_FNC_NAKED
;{
;  /* Dadr R4, Com R5, Status R6, Stamp R7 */
	extrn code(UL_O_OP)
	extrn code(UL_O_NEW)
	public ___ul_newmsg
___ul_newmsg:	
	mov  dpl,r7
	mov  dph,r6
	movx a,@dptr	; dadr
	mov  r4,a
	inc  dptr	; sadr 
	inc  dptr
	movx a,@dptr	; cmd
	mov  r5,a
	inc  dptr
	movx a,@dptr	; flg
	mov  r6,a
	inc  dptr
	movx a,@dptr	; stamp
	mov  r7,a
	clr  f0
	call UL_O_NEW
	clr  a
	mov  r7,a
	mov  r6,a
	jnb  f0,__ul_newmsg_l1
	dec  r7
	dec  r6
__ul_newmsg_l1:
	ret
;}
#pragma endasm
#endif /* SDCC */

#if defined(SDCC) || defined(__SDCC)
int	__ul_o_close() UL_FNC_NAKED
{
  __asm
	.globl ul_o_cl
	clr  f0
	lcall ul_o_cl
	mov  dptr,#0xffff
	jb   f0,00020$
	inc  dptr
00020$:
    #ifdef UL_WITH_NAKED
	ret
    #endif /*UL_WITH_NAKED*/
  __endasm;
}
#else /* SDCC */
#pragma asm
;int	__ul_o_close(void) UL_FNC_NAKED
;{
	extrn code(UL_O_CL)
	public __ul_o_close
__ul_o_close:	
	clr  f0
	call UL_O_CL
	clr  a
	mov  r7,a
	mov  r6,a
	jnb  f0,__ul_o_close_l1
	dec  r7
	dec  r6
__ul_o_close_l1:
	ret
;}
#pragma endasm
#endif /* SDCC */

#if defined(SDCC) || defined(__SDCC)
int	__ul_i_close() UL_FNC_NAKED
{
  __asm
	.globl ul_i_cl
	clr  f0
	lcall ul_i_cl
	mov  dptr,#0xffff
	jb   f0,00020$
	inc  dptr
00020$:
    #ifdef UL_WITH_NAKED
	ret
    #endif /*UL_WITH_NAKED*/
  __endasm;
}
#else /* SDCC */
#pragma asm
;int	__ul_i_close(void) UL_FNC_NAKED
;{
	extrn code(UL_I_CL)
	public __ul_i_close
__ul_i_close:	
	clr  f0
	call UL_I_CL
	clr  a
	mov  r7,a
	mov  r6,a
	jnb  f0,___ul_i_close_l1
	dec  r7
	dec  r6
___ul_i_close_l1:
	ret
;}
#pragma endasm
#endif /* SDCC */

#if defined(SDCC) || defined(__SDCC)
int	__ul_tailmsg(const ul_msginfo UL_ARGPTRTYPE  *msginfo) UL_FNC_NAKED
{
  /* Dadr R4, Com R5, Status R6, Stamp R7 */
  __asm
	.globl ul_o_op
	clr  f0
	lcall ul_o_cl	/* close previous output message */
	movx a,@dptr	/* dadr */
	mov  r4,a
	inc  dptr	/* sadr */
	/*movx a,@dptr*/
	/*uL_ISAD*/
	inc  dptr
	movx a,@dptr	/* cmd */
	mov  r5,a
	inc  dptr
	movx a,@dptr	/* flg */	
	mov  r6,a
	inc  dptr
	movx a,@dptr	/* stamp */
	mov  r7,a
	lcall ul_o_op
	mov  dptr,#0xffff
	jb   f0,00020$
	inc  dptr
00020$:
    #ifdef UL_WITH_NAKED
	ret
    #endif /*UL_WITH_NAKED*/
  __endasm;
}
#else /* SDCC */
#pragma asm
;int	__ul_tailmsg(const ul_msginfo UL_ARGPTRTYPE  *msginfo) UL_FNC_NAKED
;{
	public ___ul_tailmsg
___ul_tailmsg:	
	mov  dpl,r7
	mov  dph,r6
	call UL_O_CL	; close previous output message
	movx a,@dptr	; dadr
	mov  r4,a
	inc  dptr	; sadr
	inc  dptr
	movx a,@dptr	; cmd
	mov  r5,a
	inc  dptr
	movx a,@dptr	; flg
	mov  r6,a
	inc  dptr
	movx a,@dptr	; stamp
	mov  r7,a
	call UL_O_OP
	clr  a
	mov  r7,a
	mov  r6,a
	jnb  f0,___ul_tailmsg_l1
	dec  r7
	dec  r6
___ul_tailmsg_l1:
	ret
;}
#pragma endasm
#endif /* SDCC */

#if defined(SDCC) || defined(__SDCC)
int	__ul_freemsg() UL_FNC_NAKED
{
  __asm
	.globl ul_o_op
	clr f0
	lcall ul_i_cl	/* close previous input message */
	clr f0
	lcall ul_o_cl	/* close previous output message */
	mov  dptr,#0xffff
	jb   f0,00020$
	inc  dptr
00020$:
    #ifdef UL_WITH_NAKED
	ret
    #endif /*UL_WITH_NAKED*/
  __endasm;
}
#else /* SDCC */
#pragma asm
;int	__ul_freemsg(void) UL_FNC_NAKED
;{
	public __ul_freemsg
__ul_freemsg:	
	clr  f0
	call UL_I_CL	; close previous input message
	clr  f0
	call UL_O_CL	; close previous output message
	clr  a
	mov  r7,a
	mov  r6,a
	jnb  f0,___ul_freemsg_l1
	dec  r7
	dec  r6
___ul_freemsg_l1:
	ret
;}
#pragma endasm
#endif /* SDCC */

#if defined(SDCC) || defined(__SDCC)
int __ul_acceptmsg(ul_msginfo UL_ARGPTRTYPE  *msginfo) UL_FNC_NAKED
{
  /* Dadr R4, Com R5, Status R6, Stamp R7 */
  __asm
	.globl ul_i_op
	.globl ul_i_l
	clr  f0
	lcall ul_i_op
	jb   f0,00010$
	mov  a,r3
	movx @dptr,a	/* dadr */
	inc  dptr
	mov  a,r4
	movx @dptr,a	/* sadr */
	inc  dptr
	mov  a,r5
	movx @dptr,a	/* cmd */
	inc  dptr
	mov  a,r6
	movx @dptr,a	/* flg */
	inc  dptr
	mov  a,r7
	movx @dptr,a	/* stamp */
	inc  dptr
	lcall ul_i_l	/* read length of data */
	mov  a,r4
	movx @dptr,a	/* len */
	inc  dptr
	mov  a,r5
	movx @dptr,a
	mov  dptr,#0
    #ifdef UL_WITH_NAKED
	ret
    #else /*UL_WITH_NAKED*/
	sjmp 00030$
    #endif /*UL_WITH_NAKED*/
00010$:
	mov  dptr,#0xffff
    #ifdef UL_WITH_NAKED
	ret
    #endif /*UL_WITH_NAKED*/
00030$:
  __endasm;
}
#else /* SDCC */
#pragma asm
;int __ul_acceptmsg(ul_msginfo UL_ARGPTRTYPE  *msginfo) UL_FNC_NAKED
;{
;  /* Dadr R4, Com R5, Status R6, Stamp R7 */
	extrn code(UL_I_OP)
	public ___ul_acceptmsg
___ul_acceptmsg:
	clr  f0
	mov  dpl,r7
	mov  dph,r6
	lcall UL_I_OP
	jb   f0,___ul_acceptmsg_l1
	mov  a,r3
	movx @dptr,a	; dadr
	inc  dptr
	mov  a,r4
	movx @dptr,a	; sadr
	inc  dptr
	mov  a,r5
	movx @dptr,a	; cmd
	inc  dptr
	mov  a,r6
	movx @dptr,a	; flg
	inc  dptr
	mov  a,r7
	movx @dptr,a	; stamp
	inc  dptr
	lcall UL_I_L	; read length of data
	mov  a,r5
	movx @dptr,a	; len is bigendian
	inc  dptr
	mov  a,r4
	movx @dptr,a
	clr  a
	mov  r7,a
	mov  r6,a
	ret
___ul_acceptmsg_l1:
	mov  r7,#0ffh
	mov  r6,#0ffh
	ret
;}
#pragma endasm
#endif /* SDCC */

int __ul_actailmsg(ul_msginfo UL_ARGPTRTYPE  *msginfo) UL_FNC_NAKED
{
  return -1;
}

int __ul_addfilt(const ul_msginfo UL_ARGPTRTYPE  *msginfo)
{
  return -1;
}

#if defined(SDCC) || defined(__SDCC)
int __ul_abortmsg()
{
  __asm
	.globl ul_o_abrt
	lcall ul_i_cl
	lcall ul_o_abrt
  __endasm;
  return 0;
}
#else /* SDCC */
#pragma asm
;int __ul_abortmsg(void)
;{
	public ___ul_abortmsg
___ul_abortmsg:
	extrn code(UL_O_ABRT)
	call UL_I_CL
	call UL_O_ABRT
	ret
;}
#pragma endasm
#endif /* SDCC */

#if defined(SDCC) || defined(__SDCC)
uchar __ul_inepoll() UL_FNC_NAKED
{
  __asm
	.globl ul_i_ne
	lcall ul_i_ne
	mov  dptr,#0
	jnc  00050$
	inc  dptr
00050$:
    #ifdef UL_WITH_NAKED
	ret
    #endif /*UL_WITH_NAKED*/
  __endasm;
}
#else /*SDCC*/
#pragma asm
;uchar __ul_inepoll(void) UL_FNC_NAKED
;{
	extrn code(UL_I_NE)
	public __ul_inepoll
__ul_inepoll:
	call UL_I_NE
	mov  r7,#0
	jnc  ___ul_inepoll_l1
	inc  r7
___ul_inepoll_l1:
	ret
;}
#pragma endasm
#endif /*SDCC*/

#if defined(SDCC) || defined(__SDCC)
char ul_drv_init()
{
  __asm
	.globl ul_init
	mov  r0,#0
	mov  r2,#0
	lcall ul_fnc	/* Init */
  __endasm;
  return 0;
}
#else /*SDCC*/
#pragma asm
;char ul_drv_init()
;{
	extrn code(UL_INIT)
	public ul_drv_init
ul_drv_init:
	mov  r0,#0
	mov  r2,#0
	jmp  UL_FNC	; Init
;  return 0;
;}
#pragma endasm
#endif

#if defined(SDCC) || defined(__SDCC)
char ul_drv_set_adr(uchar addr)
{
  __asm
	.globl ul_fnc
	mov  r0,#2
	mov  a,dpl
	lcall ul_fnc	/* Address */
  __endasm;
  return 0;
}
#else /*SDCC*/
#pragma asm
;char ul_drv_set_adr(uchar addr)
;{
	extrn code(UL_FNC)
	public _ul_drv_set_adr
_ul_drv_set_adr:
	mov  r0,#2
	mov  a,r7
	jmp  UL_FNC	; Address
;  return 0;
;}
#pragma endasm
#endif

#if defined(SDCC) || defined(__SDCC)
char ul_drv_set_bdiv(uchar addr)
{
  __asm
	.globl ul_fnc
	mov  r0,#1	/* 2 => 19200 Bd for 11059 kHz */
	mov  a,dpl	/* 5 => 19200 Bd for 18432 kHz */
	lcall ul_fnc	/* Baudrate */
  __endasm;
  return 0;
}
#else /*SDCC*/
#pragma asm
;char ul_drv_set_bdiv(uchar addr)
;{
	public _ul_drv_set_bdiv
_ul_drv_set_bdiv:
	mov  r0,#1	; 2 => 19200 Bd for 11059 kHz
	mov  a,r7	; 5 => 19200 Bd for 18432 kHz
	jmp  UL_FNC	; Baudrate
;  return 0;
;}
#pragma endasm
#endif

#ifndef  UL_WITHOUT_HANDLE

ul_ssize_t ul_read(ul_fd_t ul_fd,void *buffer, ul_size_t size)
{
  return __ul_read(buffer, size);
}

ul_ssize_t ul_write(ul_fd_t ul_fd,const void *buffer, ul_size_t size)
{
  return __ul_write(buffer, size);
}

int ul_newmsg(ul_fd_t ul_fd,const ul_msginfo UL_ARGPTRTYPE  *msginfo)
{
  return __ul_newmsg(msginfo);
}

int ul_tailmsg(ul_fd_t ul_fd,const ul_msginfo UL_ARGPTRTYPE  *msginfo)
{
  return __ul_tailmsg(msginfo);
}

int ul_freemsg(ul_fd_t ul_fd)
{
  return __ul_freemsg();
}

int ul_acceptmsg(ul_fd_t ul_fd,ul_msginfo UL_ARGPTRTYPE  *msginfo)
{
  return __ul_acceptmsg(msginfo);
}

int ul_actailmsg(ul_fd_t ul_fd,ul_msginfo UL_ARGPTRTYPE  *msginfo)
{
  return __ul_actailmsg(msginfo);
}

int ul_addfilt(ul_fd_t ul_fd,const ul_msginfo UL_ARGPTRTYPE  *msginfo)
{
  return __ul_addfilt(msginfo);
}

int ul_abortmsg(ul_fd_t ul_fd)
{
  return __ul_abortmsg();
}

uchar ul_inepoll(ul_fd_t ul_fd)
{
  return __ul_inepoll();
}
#endif /*UL_WITHOUT_HANDLE*/

int ul_drv_debflg(ul_fd_t ul_fd,int debug_msk)
{
  return 0;
}

int ul_fd_wait(ul_fd_t ul_fd, int wait_sec)
{
  return 0;
}

#if 0
int test_fnc(int i)
{
  __asm
	mov  a,#0xaa
	mov  b,#0xbb
	mov  r0,#0xc0
	mov  r1,#0xc1
	mov  r2,#0xc2
	mov  r3,#0xc3
	mov  r4,#0xc4
	mov  r5,#0xc5
	mov  r6,#0xc6
	mov  r7,#0xc7
	mov  dptr,#1
    #ifdef UL_WITH_NAKED
	ret
    #endif /*UL_WITH_NAKED*/
  __endasm;
}
#endif
