;$NOMOD51
;********************************************************************
;*                                                                  *
;* uLan Communication - version for generic MSC51 core based MCU    *
;*                                                                  *
;* ulan.asm        - low level assembly implementation of uLAN      *
;*                   protocol implementation for MCS51              *
;*                                                                  *
;*                                                                  *
;* Copyright holders and originators:                               *
;*     (C) 1991-2003 by Pavel Pisa pisa@cmp.felk.cvut.cz            *
;*     (C) 1992-2003 by PiSOFT Praha                                *
;*     (C) 1994-2003 by PiKRON Ltd. http://www.pikron.com           *
;*                                                                  *
;*   The more uLan related information can be foud at next sites    *
;*            http://cmp.felk.cvut.cz/~pisa/ulan/                   *
;*            http://www.pikron.com/en/chromul_over.html            *
;*                                                                  *
;* The uLan for small embedded devices can be used, modified        *
;* and redistributed if at least conditions of one of the           *
;* following licenses are met                                       *
;*     - GPL - General Public License                               *
;*     - LGPL - Lesser General Public License                       *
;*     - MPL - Mozilla Public License                               *
;*     - other license conditions assigned by originators           *
;*                                                                  *
;* If significant code contributor disagrees with some of above     *
;* licenses, he or she can delete such line/s, but if he deletes    *
;* all lines, he is not allowed redistribute code in any binary     *
;* or source form.                                                  *
;********************************************************************

$INCLUDE(cf_ulan.inc)

%IF (%TGT_MSC1210)THEN(
R_USEC  EQU   (%CPU_SYS_HZ/1000000)-1               ; (CPU_SYS_HZ / 1000000)-1
R_MSECL EQU   ((%CPU_SYS_HZ/1000)-1) / 256          ; ((CPU_SYS_HZ / 1000) - 1) mod 256
R_MSECH EQU   ((%CPU_SYS_HZ/1000)-1) / 256          ; ((CPU_SYS_HZ / 1000) - 1) / 256
MSC_WRITE_FLASH EQU 0FFD9H  ; DPTR = address, acc = data
)FI

%IF(NOT %SPN)THEN(
; Standardni seriovy port 8051
PCON    DATA  087H
SBUF_U	DATA  SBUF
SCON_U	DATA  SCON
SM2_U	BIT   SM2
REN_U	BIT   REN
TB8_U	BIT   TB8
RB8_U	BIT   RB8
TI_U	BIT   TI
PCON_U	DATA  PCON	; SMOD bit
RI_U	BIT   RI
ES_U	BIT   ES
RXD_U	BIT   RXD
TXD_U	BIT   TXD
SINT_U	CODE  SINT
)ELSE(
; Port 1 mikrokontroleru TI MSC1210
$INCLUDE(reg1210.inc)
SBUF_U	DATA  SBUF1
SCON_U	DATA  SCON1
SM2_U	BIT   SM21
REN_U	BIT   REN1
TB8_U	BIT   TB81
RB8_U	BIT   RB81
TI_U	BIT   TI1
RI_U	BIT   RI1
PCON_U	DATA  EICON	; SMOD bit
ES_U	BIT   ES1
RXD_U	BIT   RXD1
TXD_U	BIT   TXD1
SINT_U	CODE  SINT1
)FI

%IF (%FOR_SDCC) THEN (
SINT	CODE  00023H	      ; SDCC nema hodnotu definovanu
)FI

%IF (%VECTOR_FL) THEN (
  EXTRN    CODE(VEC_SET)
  V_uL_ADD EQU   1EH
  V_uL_FNC EQU   26H
)ELSE(
;  EXTRN    CODE(uL_R_BU,uL_R_CO)
)FI

EXTRN   CODE(_uL_IDSTR)       ; Jmeno modulu

PUBLIC  uL_IB_L,uL_OB_L
PUBLIC  uL_FNC,_uL_FNCC,uL_INIT
PUBLIC  _uL_STR,uL_STR,uL_ERR,uLF_ERR,uLF_INE,uLF_KMP,uLF_TRP
PUBLIC  uL_FLG,uL_FLH
PUBLIC  _uL_INT,S_INT,SER_STACK_B

PUBLIC  uL_O_OP,uL_O_NEW,uL_WR,uL_WRc,uL_WRi,uL_O_CL,uL_WRB,uL_WRB0,uL_O_LN,uL_O_ABRT
PUBLIC  uL_I_OP,uL_RD,uL_RDi,uL_I_CL,uL_RDB,uL_I_LN,uL_I_L,uL_I_NE

%IF (%DY_ADDR) THEN (
PUBLIC  uL_FLD,uLD_RQA,UD_DYSA,UD_STAD,UD_STLN,SER_NUM,uL_T_RQA
)FI

%IF (%WITH_SYNCH) THEN(
  EXTRN    CODE(_ul_synch)
)FI

; Funkce volane z rutin zpracovani prikazu
PUBLIC  ACK_CMD,SND_BEB,SND_CHC,SND_END,S_WAITD,NAK_CMD
PUBLIC          REC_BEG,REC_CHR,REC_END,REC_CME
PUBLIC  REC_Bi, SND_Bi, REC_Bx, SND_Bx, SND_Bc, S_R0FB, S_EQP
; Data
PUBLIC  uL_ADR,uL_SA,uL_DA,uL_CMD,uL_ST
PUBLIC  uL_ISAD,uL_IDAD,uL_ICOM,uL_IST,uL_ISTA
PUBLIC  uLF_RS,uLF_NB,uLF_NA

%IF(%TGT_T89C51RD2)THEN(
FCON    DATA  0D1H
SADDR   DATA  0A9H
)FI

SER___C SEGMENT CODE
SER___X SEGMENT XDATA
SER___B SEGMENT DATA BITADDRESSABLE
SER_STACK SEGMENT DATA

%IF (NOT %VECTOR_FL AND %WITH_INTR) THEN(
CSEG AT SINT_U
        JMP   S_INT
        JMP   uL_FNC
)FI

RSEG SER___X

;********************************************************************
; Datova oblast site uLAN

; Jen pro debugging, jinak zakomentovat
PUBLIC uL_HBIB,uL_SPD,uL_STA
PUBLIC P_NDB,H_BIB,H_EIB,P_NPD,P_AID,P_EID
PUBLIC P_AOB,H_BOB,H_EOB,P_NOB,P_AOD

%IF(%DY_ADDR)THEN(
UD_SFN:	DS    1		; prave ctena subfunkce
UD_DYSA:DS    1		; adresa serveru adres
UD_STAD:DS    2		; ukazatel na buffer statusu
UD_STLN:DS    1		; delka bufferu
SER_NUM:DS    4		; serial number
)FI

uL_HBIB:DS    1  ;    Vyssi cast adresy zacatku IB
uL_IB_L:DS    1  ;    Delka IB v 256
uL_OB_L:DS    1  ;    Delka OB v 256
uL_SPD: DS    1  ;    Delitel frekvence 57.6 kHz
uL_ADR: DS    1  ;    Vlastni adresa
uL_CMD: DS    1  ;    Prave prenaseny prikaz (IRQ)
uL_SA:  DS    1  ;    Adresa vysilace (IRQ)
uL_DA:  DS    1  ;    Adresa prijimace (IRQ)
uL_ST:  DS    1  ;    Status zpravy (IRQ)
uL_STA: DS    1  ;    Stamp zpravy (IRQ)
uL_FRLN:DS    2  ;    Maximalni delka ramce

; Prijem dat

P_NDB:  DS    2  ;R23 Ukazatel na pocatek prijimaneho bloku (IRQ)
H_BIB:  DS    1  ;R4  Pocatek vstupniho buferu
H_EIB:  DS    1  ;R5  Konec vstupniho buferu
P_NPD:  DS    2  ;R67 Ukazatel na pocatek 1. nezprac. bloku (FNC)
P_AID:  DS    2  ;    Ukazatel na zpracovavana data (FNC)

; Vysilani dat

P_AOB:  DS    2  ;R23 Ukazatel na vysilany blok	(IRQ)
H_BOB:  DS    1  ;R4  Pocatek vystupniho buferu
H_EOB:  DS    1  ;R5  Konec vystupniho buferu
P_NOB:  DS    2  ;R67 Ukazatel na zapisovany blok (FNC)
P_AOD:  DS    2  ;    Ukazatel na zapisovana data (FNC)
%IF(%WITH_TAILS)THEN(
P_AOT:	DS    2  ;    Ukazuje na prvni blok tailovane skupiny (FNC)
P_TO:	DS    2  ;    Pomocny ukazatel do OB pro taily (IRQ)
P_TI:	DS    2  ;    Pomocny ukazatel do IB pro taily (IRQ)
)FI

P_EID:  DS    2  ;    Zjisteny konec vstupnich dat
uL_ISAD:DS    1  ;    Promiskuitni SAdr zpracovavaneho bloku
uL_IDAD:DS    1  ;    DA zpracovavaneho bloku
uL_ICOM:DS    1  ;    Command zpracovavaneho bloku
uL_IST: DS    1  ;    Status zpracovavaneho bloku
uL_ISTA:DS    1  ;    Stamp - jedinecne cislo zpravy
uL_TMPB:DS    1  ;    Pomocny buffer pro uL_WRi a uL_RDi

PROC_BUF:DS   17 ; Buffer prikazu proceed
PROC_BUFE:

RSEG SER___B

uL_FLG: DS    1
uLF_ER0 BIT   uL_FLG.0  ; Chyba pri vysilani
uLF_ER1 BIT   uL_FLG.1  ; Chyba pri vysilani
uLF_ERR BIT   uL_FLG.2  ; Chyba pri vysilani
uLF_SN  BIT   uL_FLG.3  ; uLan vysila - master mode
uLF_RS  BIT   uL_FLG.4  ; Potreba vysilat
uLF_NB  BIT   uL_FLG.5  ; Zbernice neobsazena
uLF_NA  BIT   uL_FLG.6  ; Mazano kazdou akci
uLF_TRP BIT   uL_FLG.7  ; Jiny duvod preruseni

uL_FLH: DS    1
uLF_INE BIT   uL_FLH.0  ; Zprava presunuta do vstupni fronty
uLF_KMP BIT   uL_FLH.1  ; Keyboard macro in progress
uLF_REC BIT   uL_FLH.2  ; Zprava urcena pro tuto stanici
uLF_ARC BIT   uL_FLH.3  ; Prijimat vsechny zpravy na sbernici
%IF(%WITH_TAILS)THEN(
uLF_OWT BIT   uL_FLH.4  ; Pripravuje se vystupni zprava s tailem
uLF_TPF BIT   uL_FLH.5  ; Zpracovava se tailovani pod prerusenim
uLF_TMP BIT   uL_FLH.6  ; Pomocny
)FI

%IF(%DY_ADDR)THEN(
uL_FLD:DS    1
uLD_RQA	BIT   uL_FLD.2	; Pozadavek na pripojeni do site
uLD_SSIP BIT  uL_FLD.3	; Send status in progress
uLD_SCIP BIT  uL_FLD.4	; Status is changed
)FI

RSEG    SER_STACK
SER_STACK_B:
        DS    8+SER_STACK_EXT

XSEG AT END_RAM-256*LENG_IB-256*LENG_OB

BEG_IB: DS    100H*LENG_IB
END_IB:
BEG_OB: DS    100H*LENG_OB
END_OB:

%IF (%FOR_SDCC) THEN (
RSEG	SER___RB1
        DS    8
)FI

; Rizeni linky - prikazy
uL_ERRI EQU   0FFH ; Ignoruj vse doslo k chybe
uL_ERR  EQU   07FH ; Chyba v datech
uL_END  EQU   07CH ; Konec dat
uL_ARQ  EQU   07AH ; Konec dat - vysli ACK
uL_PRQ  EQU   079H ; Konec dat - proved prikaz
uL_AAP  EQU   076H ; ARQ + PRQ
uL_BEG  EQU   075H ; Zacatek dat

; Potvrzovaci zpravy
uL_ACK  EQU   019H ; Potvrzeni
uL_NAK  EQU   07FH ; Doslo k chybe
uL_WAK  EQU   025H ; Ted nemohu splnit

; Prikazy posilane linkou
; 00H .. 3FH    uloz do bufferu
; 40H .. 7FH    uloz do bufferu bez ACK
; 80H .. 9FH    okamzite proved a konci
; A0H .. BFH    proved a prijimej
; C0H .. FFH    proved a vysilej

uL_RES  EQU   080H ; Znovu inicializuj RS485
uL_SFT  EQU   081H ; Otestuj velikost volne pameti
uL_SYN  EQU   085H ; Sychronizace casu
uL_SID  EQU   0F0H ; Predstav se
uL_SFI  EQU   0F1H ; Vysli velikost volne pameti v IB
uL_TF0  EQU   098H ; Konec krokovani
uL_TF1  EQU   099H ; Pocatek krokovani
uL_STP  EQU   09AH ; Krok
uL_DEB  EQU   09BH ; Dalsi prikazy pro debug
uL_SPC  EQU   0DAH ; Vysle PCL PCH PSW ACC

uL_GST  EQU   0C1H ; Fast module get status

uL_RDM  EQU   0F8H ; Cte pamet typu   T T B B L L
uL_WRM  EQU   0B8H ; Zapise do pameti T T B B L L
uL_ERM  EQU   088H ; Vymaze pamet T T B B L L

; Status zprav
uLBF_NORE   EQU 040H ; Do not try to repeat if error occurs
uLBF_NOREb  EQU 6
uLBF_TAIL   EQU 020H ; Message has tail frame
uLBF_TAILb  EQU 5
uLBF_REC    EQU 010H ; Request receiption of block
uLBF_RECb   EQU 4
uLBF_FAIL   EQU 008H ; Message cannot be send - error
uLBF_PROC   EQU 004H ; Message succesfull send
uLBF_AAP    EQU 003H ; Request imediate proccessing of frame by receiver station with acknowledge
uLBF_PRQ    EQU 002H ; Request imediate proccessing of frame by receiver station
uLBF_ARQ    EQU 001H ; Request imediate acknowledge by receiving station
uLBF_END    EQU 000H ; No acknowledge or processing request

RSEG SER___C

USING   1

; Cekani na znak R0 - 1 znaku  jinak S_ERR
WTF_CHR:DJNZ  R0,SND_SPC
V2_ERR: JMP   S_ERR

; Cekani po dobu 1 znaku
SND_SPT:CLR   TI_U
        JNB   TXD_U,S_RET
        CLR   AC
        RET

; Cekani po dobu 1 znaku
SND_SPC:CLR   SM2_U
        SETB  REN_U
SND_SP1:
%IF (%NEG_DR_EO) THEN (
        CLR   DR_EO           ; *** Ceka 1 znak
)ELSE(
        SETB   DR_EO          ; *** Ceka 1 znak
)FI
        CLR   A
        SJMP  SND_CH2

SND_CTR:SETB  C               ; *** Send control character
        SJMP  SND_CH1
SND_CHC:XRL   AR1,A           ; *** Send data + add chk sum
        INC   R1
SND_CHR:CLR   C               ; *** Send data character
SND_CH1:MOV   TB8_U,C         ; ACC .. character
        CLR   REN_U
 %IF (%NEG_DR_EO) THEN (
        SETB   DR_EO
)ELSE(
        CLR   DR_EO
)FI
SND_CH2:MOV   SBUF_U,A
        CLR   TI_U
        SJMP  S_RET

REC_CTR:SETB  C               ; *** Receive control character
        DB    74H             ; MOV A,#d8
REC_CHR:CLR   C               ; *** Receive character
REC_CH1:MOV   SM2_U,C         ; ACC .. rec. char
 %IF (%NEG_DR_EO) THEN (
        CLR   DR_EO           ; CY  .. RB8_U
)ELSE(
        SETB  DR_EO           ; CY  .. RB8_U
)FI
        SETB  REN_U
        CLR   TI_U

RS232:
S_RET:  JB    uLF_TRP,S_INT_T
S_RETI: MOV   A,SP
        XCH   A,SER_STACK
        MOV   SP,A
        POP   DPH
        POP   DPL
        POP   ACC
        POP   PSW
        RETI

; *************************************
; Interupt TI_U nebo RI_U
; *************************************

_uL_INT:
S_INT  :PUSH  PSW
        PUSH  ACC
        PUSH  DPL
        PUSH  DPH
        MOV   A,SP
        XCH   A,SER_STACK
        JZ    S_ERROR
        MOV   SP,A

S_INT_T:MOV   PSW,#AR0; Banka1
;       JB    DR_EI,RS232
        MOV   C,RB8_U         ; errata 89c51rd2, point 6 !!!!!!
        CLR   RB8_U           ; nutno dodrzet !!!!!!!!!!!!!!!!!
        JBC   RI_U,S_INT_2
        JNB   TI_U,S_RET
S_INT_1:CLR   uLF_NA
        RET
S_INT_2:JB    REN_U,S_INT_3   ; errata 89c51rd2, point 8
        SJMP  S_RETI
S_INT_3:SETB  AC              ; Preruseni od RI_U => AC=1
        CLR   uLF_NB
        MOV   A,SBUF_U
        JNC   S_INT_1
        JNB   ACC.7,S_INT_1
        SETB  uLF_NB
        CJNE  A,#uL_ERRI,S_INT_1
S_ERROR:MOV   PSW,#AR0; Banka1
        CLR   uLF_SN
        JMP   S_ERR

; Prijem bloku do XDATA

REC_Bx: CLR   SM2_U           ; R23 .. where
        SETB  REN_U           ; R45 .. buffer bot/top
%IF (%NEG_DR_EO) THEN (
        CLR   DR_EO           ; R67 .. end of buffer
)ELSE(
        SETB   DR_EO          ; R67 .. end of buffer
)FI
        CLR   TI_U            ; R1  =  check sum
REC_Bx1:CALL  S_RET           ; CY  =  1 when ended by CTR
        JB    AC,REC_Bx2
        CLR   TI_U
        SJMP  REC_Bx1
REC_Bx2:JC    REC_BxR
        CALL  S_RCTB1
        JNZ   REC_Bx1
        CLR   C
REC_BxR:RET

; Prijem bloku do CDATA (t89c51rd2)
%IF(%TGT_T89C51RD2 OR %TGT_MSC1210)THEN(
REC_Bc: CLR   SM2_U           ; R23 .. where
        SETB  REN_U           ; R45 .. buffer bot/top
%IF (%NEG_DR_EO) THEN (
        CLR   DR_EO           ; R67 .. end of buffer
)ELSE(
        SETB   DR_EO          ; R67 .. end of buffer
)FI
        CLR   TI_U            ; R1  =  check sum
REC_Bc1:CALL  S_RET           ; CY  =  1 when ended by CTR
        JB    AC,REC_Bc2
        CLR   TI_U
        SJMP  REC_Bc1
REC_Bc2:JC    REC_BcR
	CLR   EA
%IF(%TGT_T89C51RD2) THEN (
        MOV   FCON,#08H       ; zapis do programove pameti
        CALL  S_RCTB1	      ; zapis hodnotu z komunikace
)FI
%IF(%TGT_MSC1210) THEN (
        CALL  S_RCTBF	      ; zapis hodnotu z komunikace
)FI
%IF(%TGT_T89C51RD2) THEN (
        MOV   FCON,#00H       ; vypni zapis do programove pameti
)FI
	SETB  EA
        JNZ   REC_Bc1
        CLR   C
REC_BcR:RET
)ELSE(
REC_Bc  CODE  REC_Bx
)FI

; Vyslani bloku z XDATA

SND_Bx: CALL  SND_SPC         ; R23 .. where
SND_BX0:CLR   REN_U           ; R45 .. buffer bot/top
%IF (%NEG_DR_EO) THEN (
        SETB   DR_EO          ; R67 .. end of buffer
)ELSE(
        CLR   DR_EO           ; R67 .. end of buffer
)FI
        CLR   RI_U            ; R1  =  check sum
        CLR   TB8_U
SND_Bx1:CALL  S_SNFB
        CLR   TI_U
        JZ    SND_BxR
SND_Bx2:CALL  S_RET
        JB    AC,SND_Bx2
        SJMP  SND_Bx1
SND_BxR:CALL  S_RET
        CLR   TI_U
        RET

%IF (%CX_MERGED_FL) THEN (
SND_Bc  CODE  SND_Bx
)ELSE(
; Vyslani bloku z CODE
SND_Bc: CALL  SND_SPC         ; R23 .. where
SND_Bc0:CLR   REN_U           ; R45 .. buffer bot/top
%IF (%NEG_DR_EO) THEN (       ; R67 .. end of buffer
        SETB   DR_EO          ; R1  =  check sum
)ELSE(
        CLR   DR_EO
)FI
        CLR   RI_U
        CLR   TB8_U
SND_Bc1:CALL  S_SNFC
        CLR   TI_U
        JZ    SND_BcR
SND_Bc2:CALL  S_RET
        JB    AC,SND_Bc2
        SJMP  SND_Bc1
SND_BcR:CALL  S_RET
        CLR   TI_U
        RET
)FI

; Prijem bloku do IDATA

REC_Bi: CLR   SM2_U           ; R2  .. where
        SETB  REN_U           ; X R45 .. buffer bot/top
%IF (%NEG_DR_EO) THEN (
        CLR   DR_EO           ; R6  .. end of buffer
)ELSE(
        SETB  DR_EO           ; R6  .. end of buffer
)FI
        CLR   TI_U            ; R1  =  check sum
REC_Bi1:CALL  S_RET           ; CY  =  1 when ended by CTR
        JB    AC,REC_Bi2
        CLR   TI_U
        SJMP  REC_Bi1
REC_Bi2:JC    REC_BiR
        PUSH  AR1
        MOV   R1,AR2
        MOV   @R1,A
        POP   AR1
        XRL   AR1,A
        INC   R1
        INC   R2
        MOV   A,R2
        XRL   A,R6
        JNZ   REC_Bi1
        CLR   C
REC_BiR:RET

; Vyslani bloku z IDATA

SND_Bi: CALL  SND_SPC         ; R2  .. where
SND_Bi0:CLR   REN_U           ; X R45 .. buffer bot/top
%IF (%NEG_DR_EO) THEN (
        SETB  DR_EO           ; R6  .. end of buffer
)ELSE(
        CLR   DR_EO           ; R6  .. end of buffer
)FI
        CLR   RI_U            ; R1  =  check sum
        CLR   TB8_U
SND_Bi1:PUSH  AR1
        MOV   R1,AR2
        MOV   A,@R1
        MOV   SBUF_U,A
        POP   AR1
        XRL   AR1,A
        INC   R1
        INC   R2
        MOV   A,R2
        XRL   A,R6
        CLR   TI_U
        JZ    SND_BiR
SND_Bi2:CALL  S_RET
        JB    AC,SND_Bi2
        SJMP  SND_Bi1
SND_BiR:CALL  S_RET
        CLR   TI_U
        RET

; Vyslani bloku z SDATA

SND_Bs: CALL  SND_SPC         ; R2  .. where
SND_Bs0:CLR   REN_U           ; X R45 .. buffer bot/top
%IF (%NEG_DR_EO) THEN (
        SETB  DR_EO           ; R6  .. end of buffer
)ELSE(
        CLR   DR_EO           ; R6  .. end of buffer
)FI
        CLR   RI_U            ; R1  =  check sum
        CLR   TB8_U
SND_Bs1:MOV   DPTR,#PROC_BUF
        MOV   A,#0E5H         ; MOV A,dir
        MOVX  @DPTR,A
        INC   DPTR
        MOV   A,R2            ; adresa
        MOVX  @DPTR,A
        INC   DPTR
        MOV   A,#022H         ; RET
        MOVX  @DPTR,A
        INC   DPTR
        DB    12H             ; CALL PROC_BUF
        DW    PROC_BUF
        MOV   SBUF_U,A
        XRL   AR1,A
        INC   R1
        INC   R2
        MOV   A,R2
        XRL   A,R6
        CLR   TI_U
        JZ    SND_BsR
SND_Bs2:CALL  S_RET
        JB    AC,SND_Bs2
        SJMP  SND_Bs1
SND_BsR:CALL  S_RET
        CLR   TI_U
        RET

; Prijem bloku do SDATA

REC_Bs: CLR   SM2_U           ; R2  .. where
        SETB  REN_U           ; X R45 .. buffer bot/top
%IF (%NEG_DR_EO) THEN (
        CLR   DR_EO           ; R6  .. end of buffer
)ELSE(
        SETB  DR_EO           ; R6  .. end of buffer
)FI
        CLR   TI_U            ; R1  =  check sum
REC_Bs1:CALL  S_RET           ; CY  =  1 when ended by CTR
        JB    AC,REC_Bs2
        CLR   TI_U
        SJMP  REC_Bs1
REC_Bs2:JC    REC_BsR
        XRL   AR1,A
        INC   R1
        MOV   R3,A
        MOV   DPTR,#PROC_BUF
        MOV   A,#08BH         ; MOV dir,R3
        MOVX  @DPTR,A
        INC   DPTR
        MOV   A,R2            ; adresa
        MOVX  @DPTR,A
        INC   DPTR
        MOV   A,#022H         ; RET
        MOVX  @DPTR,A
        INC   DPTR
        DB    12H             ; CALL PROC_BUF
        DW    PROC_BUF
        INC   R2
        MOV   A,R2
        XRL   A,R6
        JNZ   REC_Bs1
        CLR   C
REC_BsR:RET

; Vyslani konce ramce
; se zakoncenim pro odpoved uL_END

SND_END:MOV  A,#uL_END
	SJMP  SND_EN1

; Vyslani konce ramce
; podle CMD vysle uL_ARQ, uL_AAP nebo uL_END

SND_ENDST:
	MOV   DPTR,#uL_ST     ; Status zpravy
        MOVX  A,@DPTR
        ;JNB   ACC.uLBF_NOREb,SND_EN0
	JNB   ACC.6,SND_EN0   ; !!!! ASX8051 is broken
        ORL   uL_FLG,#003H    ; uz neopakovat vysilani
SND_EN0:MOVX  A,@DPTR
        ANL   A,#3
	ADD   A,#SND_ENt-SND_EN1
	MOVC  A,@A+PC	      ; uL_END,uL_ARQ,uL_PRQ,uL_AAP
SND_EN1:MOV   R0,A
        XRL   AR1,A
        INC   R1
        CALL  SND_CTR         ; Vyslani zakonceni
        MOV   A,R1
        CALL  SND_CHR         ; Vyslani checksum
        CJNE  R0,#uL_END,SND_EN3
        RET
SND_EN3:CJNE  R0,#uL_PRQ,SND_EN4
        RET
SND_EN4:MOV   R0,#5
SND_EN5:CALL  WTF_CHR
        JNB   AC,SND_EN5      ; Prijeti uL_ACK
        JC    SND_EN6
        CJNE  A,#uL_ACK,SND_EN6
        RET
SND_EN6:JMP   V3_ERR

SND_ENt:DB    uL_END,uL_ARQ,uL_PRQ,uL_AAP

; Vyslani uL_NAK pri chybe v nektere z rutin zpracovani prikazu
REC_ERR:CALL  REC_CTR
        JNB   AC,REC_ERR
REC_ER2:CALL  CMP_END         ; Vstup s prijatym znakem v ACC a CY
        JC    V3_ERR
        MOV   R1,AR0
        MOV   R0,#5
REC_ER3:CALL  WTF_CHR         ; Prijem checksum
        JNB   AC,REC_EN3
        MOV   R0,AR1
        MOV   R1,#uL_NAK
        SJMP  REC_EN4

; Potvrzeni prikazu
ACK_CMD:MOV   R1,#uL_ACK
        SJMP  REC_EN4

; Nacteni konce ramce
; ret: ACC = CMP_END

REC_END:MOV   R0,#5
REC_EN1:CALL  WTF_CHR         ; Prijem zakonceni ramce
        JNB   AC,REC_EN1
REC_EN2:XRL   AR1,A           ; Vstup s prijatym znakem v ACC a CY
        INC   R1
        CALL  CMP_END         ; Nastaveni R0 dle zpravy (0-3)
        JC    V3_ERR
        MOV   DPTR,#uL_ST     ; Nastav status zpravy
        MOV   A,R0
        MOVX  @DPTR,A
        CALL  SND_SPC         ; Prijem checksum
        JB    AC,REC_EN3
        CALL  SND_SPC
        JB    AC,REC_EN3
        CALL  SND_SPC
        JB    AC,REC_EN3
        CALL  SND_SPC
        JB    AC,REC_EN3
REC_E3N:CALL  SND_SPC
        JB    AC,REC_EN3
        SJMP  V3_ERR
REC_EN3:JC    V3_ERR
        XRL   A,R1
        JB    uLF_REC,REC_EN7 ; Zprava urcena pro tuto stanici
        MOV   R0,#0           ; Nastav ukonceni na END (neodpovidat)
REC_EN7:MOV   R1,#uL_NAK
        JNZ   REC_EN4         ; Pri chybe a ARQ nebo AAP vysle NAK
        MOV   R1,#uL_ACK      ; Pri   OK  a ARQ vysle ACK
        MOV   A,R0            ; Pri   OK  a AAP je R1=ACK ale vysle
        JB    ACC.1,REC_EN5   ;  az prooceed rutina
REC_EN4:MOV   A,R0
        JNB   ACC.0,REC_EN5
        CLR   ACC.0
        MOV   R0,A
        SETB  REN_U
        CALL  SND_SPT
        CALL  SND_SPC
        MOV   A,R1
        CALL  SND_CHR
REC_EN5:CJNE  R1,#uL_NAK,REC_EN6
V3_ERR: JMP   S_ERR
REC_EN6:MOV   A,R0
        RET

; Pocatek ramce bez urceni Destignation Address

SND_BEB:MOV   DPTR,#uL_CMD    ; Vysilany prikaz
        MOVX  A,@DPTR
        ANL   A,#7FH
        MOV   R0,A
        MOV   A,#uL_BEG

; Vyslani zacatku ramce
; call: ACC  Destignation Address
;       R0   CoMmanD

SND_BEG:ANL   A,#07FH
        MOV   R1,A
        CALL  SND_SPT
        CALL  SND_SPC
        MOV   A,R1
        INC   R1
        CALL  SND_CTR         ; Destignation Address nebo uL_BEG
        MOV   DPTR,#uL_SA     ; SA adresa
        MOVX  A,@DPTR
        XRL   AR1,A
        INC   R1
        CALL  SND_CHR
        MOV   A,R0
        XRL   AR1,A
        INC   R1
        JMP   SND_CHR


; Nacteni zacatku ramce
; ret:  ACC  Source Address
;       R0   CoMmanD

REC_BEG:MOV   R1,#uL_BEG      ; Cekani ze SWAIT
        MOV   R0,#9
REC_BE1:CALL  WTF_CHR
        JNB   AC,REC_BE1
REC_BE2:MOV   R0,A
        MOV   DPTR,#uL_STA    ; Nuluj stamp
        CLR   A
        MOVX  @DPTR,A
REC_BET:JNC   V4_ERR
        JB    uLF_NB,V3_ERR   ; Cekani z  PWAIT s R1=0
        MOV   A,R0
        MOV   DPTR,#uL_DA     ; Uloz DA
        MOVX  @DPTR,A
        SETB  uLF_REC         ; Zprava urcena pro tuto stanici
        JZ    REC_BE3         ; Vseobecna adresa
	MOV   DPTR,#uL_ADR    ; Porovnej s vlastni adresou
        MOVX  A,@DPTR
        XRL   A,R0
        JZ    REC_BE3	      ; Shoda adresy
        CLR   uLF_REC
        JNB   uLF_ARC,REC_BEU ; Neprijimat vsechny zpravy
	CJNE  R1,#uL_BEG,REC_BE3
        CJNE  R0,#uL_BEG,REC_BE3
REC_BEU:CJNE  R1,#uL_BEG,S_ERR
        CJNE  R0,#uL_BEG,S_EWAIT    ; S_BEG a SWAIT
        SETB  uLF_REC         ; Zprava urcena pro tuto stanici
REC_BE3:MOV   AR1,R0          ; **********************
        INC   R1              ; V R1 se bude pocitat chksum
        MOV   R0,#5
REC_BE4:CALL  WTF_CHR         ; Prijem SA
        JNB   AC,REC_BE4
        JC    S_ERR
        XRL   AR1,A
        INC   R1
        MOV   R0,A            ; R0=SA
REC_BE5:CALL  REC_CHR
        JNB   AC,REC_BE5      ; Cekani na  CMD - prikaz
        JC    S_ERR
        XRL   AR1,A
        INC   R1
        MOV   DPTR,#uL_CMD
        MOVX  @DPTR,A         ; Zapis CMD
        MOV   DPTR,#uL_SA
        XCH   A,R0
        MOVX  @DPTR,A         ; Zapis SA
        JMP   S_POPDP

; Cekani na konec bloku pri REC_BEG a SWAIT
S_EWAIT:CALL  REC_CTR
        JNB   AC,S_EWAIT
        CALL  CMP_END
        JC    S_ERR
        MOV   A,R0
        JNB   ACC.0,REC_BEG
        MOV   R0,#5
S_EWAI1:CALL  WTF_CHR
        JNB   AC,S_EWAI1
        JNC   REC_BEG
V4_ERR: JMP   S_ERR

; Vrati v R0  0..S_END,1..S_ARQ,2..S_PRQ,3..S_AAP,JINAK CY
CMP_END:JNC   CMP_EN4
CMP_EN0:MOV   R0,#0
        CJNE  A,#uL_END,CMP_EN1
        RET
CMP_EN1:INC   R0
        CJNE  A,#uL_ARQ,CMP_EN2
        RET
CMP_EN2:INC   R0
        CJNE  A,#uL_PRQ,CMP_EN3
        RET
CMP_EN3:INC   R0
        CJNE  A,#uL_AAP,CMP_EN4
        RET
CMP_EN4:SETB  C
        RET

; Doslo k chybe pri prijmu nebo vysilani

S_ERR:  MOV   SP,#SER_STACK
        JNB   uLF_SN,S_WAITD
        MOV   R0,#3           ; Pocet vyslani ERRI pri normalni chybe
        JNB   F0,S_ERR_1
        MOV   R0,#10          ; Pocet vyslani ERRI pri zavazne chybe
S_ERR_1:MOV   A,#uL_ERRI
        CALL  SND_CTR
        DJNZ  R0,S_ERR_1
S_ERR_2:INC   uL_FLG
        MOV   A,uL_FLG
        ANL   A,#3
        JNZ   S_END
        DEC   uL_FLG
S_ERR_3:SETB  uLF_ERR
        CALL  GET_OBA         ; otevreni vystupni zpravy a naplneni promenych
        CALL  GET_NXT         ; uL_SA,uL_DA, ...
        MOV   DPTR,#uL_ST     ; priznak neplatneho doruceni zpravy
        MOVX  A,@DPTR
        ORL   A,#uLBF_FAIL
        MOVX  @DPTR,A
        CALL  GET_IBA         ; otevreni vstupni zpravy
        CALL  IB_BEGR
	JZ    S_ERR_4	      ; IBfull
        CALL  IB_ENDR         ; zapis zpravy o nulove delce s prametry uL_SA, ...
S_ERR_4:SETB  uLF_INE         ; zprava ve vstupni fronte
	MOV   R0,#0FFH
        SJMP  S_ENDTE

; Rutina maze posledni zpravu

S_ENDT: MOV   R0,#0
S_ENDTE:MOV   SP,#SER_STACK
        ANL   uL_FLG,#0FCH
%IF (%NEG_DR_EO) THEN (
        CLR   DR_EO
)ELSE(
        SETB  DR_EO
)FI
        CALL  GET_OBA
S_ENDT1:CALL  GET_NXT
        JNB   ACC.7,S_ENDT3   ; Neexistuje
        CALL  SET_NXT
    %IF(%WITH_TAILS)THEN(
	JC    S_ENDT1	      ; Preskocit na konec tailovane skupiny
    )FI
        CALL  GET_NXT
	JB    ACC.7,S_END     ; Je dalsi zprava
S_ENDT3:CLR   uLF_RS	      ; Neni dalsi zprava

; Konec vysilani nebo prijmu

S_END:  MOV   SP,#SER_STACK
        JNB   uLF_SN,S_WAITD
        JB    uLF_NB,S_WAITD
        CALL  SND_SPT
        JB    AC,S_WAITD
        CALL  SND_SPC
        JB    AC,S_WAITD
        CALL  G_MADR
        ORL   A,#80H
        CALL  SND_CTR
        SETB  uLF_NB

; Cekani na komunikaci

S_WAITD:MOV   SP,#SER_STACK
        CLR   TI_U
        SETB  F0
        SETB  REN_U
        SETB  TXD_U

; Cekani na vlastni adresu nebo pozadavek k vysilani

S_WAIT: CLR   uLF_SN
        JNB   uLF_NB,S_WAIT1  ; Nelze vysilat
        JNB   uLF_RS,S_WAIT1  ; Neni potreba vysilat

        SETB  uLF_SN
        CALL  G_MADR
        MOV   R0,A            ; Vlastni adresa
        SETB  C
        SUBB  A,R1            ;  - Posledni vysilajici
        ANL   A,#00FH         ; Token pasing delay
        ADD   A,#4            ; min 4
        JNB   F0,S_CONN1
        ADD   A,#010H         ; Prodlouzit - byla chyba
S_CONN1:MOV   R1,A
S_CONN2:CALL  SND_SPC         ; Cekani token pasing delay
        JB    AC,S_WAIT2
        DJNZ  R1,S_CONN2
        CALL  S_ARB           ; Arbitrace pristupu
        JB    F0,S_WAIT1
SND_OB: CALL  GET_OBA         ; R23 R4 R5 do OB
    %IF(%WITH_TAILS)THEN(
	CLR   uLF_TPF
    )FI
SND_OB0:CALL  GET_NXT         ; R67 pristi zprava
        JB    ACC.7,SND_OBV   ; Neexistuje
        JMP   S_ENDT
SND_OBV:JMP   SND_OB1         ; Vysli blok

S_WAIT1:MOV   C,uLF_NB
        CPL   C
        CALL  REC_CH1         ; Ceka se na znak
S_WAIT2:CLR   uLF_SN
        JNB   AC,S_WAITD      ; Vybuzovaci TI_U
        MOV   R1,A
        JNC   S_WAITD         ; Neni ridici
        JB    ACC.7,S_WAIT    ; Ukonceni vysilani
        MOV   R1,#0
        CALL  REC_BE2         ; Cekani na prikaz v R0
        SJMP  REC_CM1

REC_CMD:CALL  REC_BEG         ; Cekani na prikaz v R0
REC_CM1:MOV   A,R0
        JB    ACC.7,REC_CM2
        CALL  GET_IBA         ; Prijem do IB
        CALL  IB_BEGR
        JZ    REC_CMEV
        CALL  REC_Bx
        JNC   REC_CMEV
        CALL  REC_EN2
        MOV   R1,A
        CALL  IB_ENDR           ; Prepisuje vse krome R1
        MOV   A,R1              ; v R0 vraci CMD
        SETB  uLF_INE
        JB    ACC.1,REC_CM3     ; PRQ nebo AAP
%IF (%VECTOR_FL) THEN (
        MOV   A,#8              ; Priznak IB do R0.3
REC_CM3:XCH   A,R0
        JMP   REC_CM8           ; Pro skok na uzivatelsky vektor
)ELSE(
        XCH   A,R0
;       CALL  uL_R_BU           ; Pro zpracovani zprav aplikaci

)FI
V1_WAIT:JMP   S_WAITD

%IF (NOT %VECTOR_FL) THEN (
REC_CM3:XCH   A,R0
        JMP   REC_CM8
)FI

REC_CMEV:JMP  REC_CME

REC_CM2:CALL  GET_PRB         ; Prijem do PROC_BUF
        JZ    REC_CMEV
        CALL  REC_Bx
        JNC   REC_CMEV
        CALL  REC_EN2
%IF (NOT %VECTOR_FL)THEN(
        JNB   ACC.1,V1_WAIT
)FI
        MOV   A,R2
        MOV   R6,A
        MOV   A,R3
        MOV   R7,A
        CALL  GET_PR1
REC_CM7:CALL  G_CMD
REC_CM8:                      ; Prikaz v ACC, zakonceni v R0
%IF (%VECTOR_FL)THEN(
;        JMP   V_uL_ADD        ; Moznost rozsirit funkce uzivatelem

uL_EADD:XCH   A,R0
        JB    ACC.3,V1_WAIT
        JNB   ACC.1,V1_WAIT
        XCH   A,R0
)ELSE(                        ; Pro zpracovani prikazu aplikaci
;        CALL  uL_R_CO
)FI                           ; Pocatek provadeni internich prikazu

        CJNE  A,#uL_SID,PR_CM20
; Vysle svoji identifikaci
        CALL  ACK_CMD
        CALL  SND_BEB
        MOV   DPTR,#_uL_IDSTR
%IF (%FOR_SDCC) THEN (  
        CLR   A               ; ptr Lo
        MOVC  A,@A+DPTR       
        MOV   R2,A
	MOV   A,#1            ; ptr Hi
        MOVC  A,@A+DPTR
        MOV   R3,A
	MOV   A,#3            ; len
        MOVC  A,@A+DPTR
)ELSE(                        
; KEIL
        MOV   A,#1            ; ptr Hi
        MOVC  A,@A+DPTR
        MOV   R3,A
        MOV   A,#2            ; ptr Lo
        MOVC  A,@A+DPTR
        MOV   R2,A
	MOV   A,#3            ; len
        MOVC  A,@A+DPTR
)FI      
        ADD   A,R2
        MOV   R6,A
        CLR   A
        ADDC  A,R3
        MOV   R7,A
PR_CM12:MOV   R5,AR4
        CALL  SND_Bc          ; SND_Bx
PR_CM19:CALL  SND_END	      ; Vysli zakonceni uL_END
        SJMP  V1_WAIT

; Test volne pameti
PR_CM20:CJNE  A,#uL_SFT,PR_CM30
        MOV   A,R0
        JNB   ACC.0,V1_WAIT
        CALL  GET_IBA
        CALL  LEN_DAT
        MOV   AR4,R0
        CALL  GET_PRB
        CALL  S_R0FB
        MOV   A,R4
        CLR   C
        SUBB  A,#6
        JNC   PR_CM22
        INC   R1
PR_CM22:CLR   C
        SUBB  A,R0
        PUSH  PSW
        CALL  SND_SPC
        CALL  S_R0FB
        POP   PSW
        MOV   A,R1
        SUBB  A,R0
        MOV   A,#uL_NAK
        JC    PR_CM24
        MOV   A,#uL_ACK
PR_CM24:CALL  SND_CHR
        JMP   S_WAITD

; Krokovani pocatek
PR_CM30:CJNE  A,#uL_TF0,PR_CM32
        CLR   uLF_TRP      ; Ukonci krokovani
PR_CM31:CALL  ACK_CMD
        JMP   S_WAITD
PR_CM32:CJNE  A,#uL_TF1,PR_CM33
        SETB  uLF_TRP       ; Zacne krokovani
        SJMP  PR_CM31
PR_CM33:CJNE  A,#uL_STP,PR_CM36
        SETB  TI_U          ; Provede jednu instrukci
        CALL  S_RETI
        CLR   TI_U
        CLR   A
        SJMP  PR_CM31
PR_CM36:CJNE  A,#uL_SPC,PR_CM40
        CALL  ACK_CMD
        CALL  SND_BEB
        MOV   A,SER_STACK   ; Vysle PCL PCH PSW ACC
        INC   A
        MOV   R6,A
        ADD   A,#-6
        MOV   R2,A
        CALL  SND_Bi
        JMP   PR_CM19

PR_CM40:CJNE  A,#uL_RDM,PR_CM50
; Vyslani pameti
        CALL  ACK_CMD
        CALL  SND_BEB
        CALL  S_PRPMM
        CJNE  R4,#1,S_RDM1
        CALL  SND_Bi
S_RDM1: CJNE  R4,#2,S_RDM2
        CALL  SND_Bx
S_RDM2: CJNE  R4,#4,S_RDM3
        CALL  SND_Bs
S_RDM3: CJNE  R4,#5,S_RDM4
        CALL  SND_Bc
S_RDM4: JMP   PR_CM19

PR_CM50:CJNE  A,#uL_WRM,PR_CM60
; Prijem do pameti
        CALL  ACK_CMD
        CALL  REC_BEG
        CJNE  R0,#uL_WRM AND 7FH,REC_CMEV1
S_WRM0: CALL  S_PRPMM
        MOV   A,#1
        CJNE  R4,#1,S_WRM1
        CALL  REC_Bi
S_WRM1: CJNE  R4,#2,S_WRM2
        CALL  REC_Bx
S_WRM2: CJNE  R4,#4,S_WRM3
        CALL  REC_Bs
S_WRM3: CJNE  R4,#5,S_WRM4    ; zapis do code 89c51rd2?
        CALL  REC_Bc
        JNZ   REC_CMEV1
        CALL  REC_END
%IF(%TGT_T89C51RD2)THEN(
        CLR   EA
        MOV   FCON,#050H      ; zapisovaci sequence
        MOV   FCON,#0A0H      ; zapisovaci sequence
S_WRM31:
	MOV   A,FCON          ; cekej na dozapsani
        ANL   A,#01H
        JNZ   S_WRM31
        SETB  EA
)FI	
        JMP   S_WAITD
S_WRM4: JNZ   REC_CME
        CALL  REC_END
        JMP   S_WAITD
REC_CMEV1:JMP  REC_CME

PR_CM60:CJNE  A,#uL_ERM,PR_CM70
; mazani pameti
        CALL  ACK_CMD
        CALL  S_PRPMM
        CJNE  R4,#5,S_ERM3
%IF(%TGT_MSC1210) THEN (
        MOV   MSECL,#R_MSECL
        MOV   MSECH,#R_MSECH
        CLR   EA
S_ERM1: CLR   C
	MOV   A,R2
	SUBB  A,R6
	MOV   A,R3
	SUBB  A,R7
	JNC   S_ERM2           ; R23>=R67
        MOV   A,#0FFH          ; value to write
        ORL   FMCON,#040h
        CALL  S_ACCTBF
        ANL   FMCON,#0BFh
	MOV   A,PDCON	       ; refresh watchdog when is enabled
	JB    ACC.2,S_ERM4    
	ORL   WDTCON,#0x20
	ANL   WDTCON,#0xDF
S_ERM4: MOV   A,#127           ; R23+128, page length is 128 
        ADD   A,R2             ; a increment is made in S_ACCTB
        MOV   R2,A
        JNC   S_ERM1
        INC   R3
        JMP   S_ERM1
S_ERM2: SETB  EA
)FI
S_ERM3:
        JMP   S_WAITD

PR_CM70:CJNE  A,#uL_DEB,PR_CM80
; Ladici prikazy
S_DEB:  CALL  S_EQP
        JZ    NAK_CMD
        CALL  ACK_CMD
        MOV   DPTR,#PROC_BUF
        MOVX  A,@DPTR
        INC   DPTR
        CJNE  A,#10H,S_DEB98
        INC   DPTR          ; Prikaz GO xxxx
        INC   DPTR
        MOV   A,SER_STACK   ; Zapise  PCL PCH
        ADD   A,#-5
        MOV   R1,A
        MOVX  A,@DPTR
        MOV   @R1,A
        INC   DPTR
        INC   R1
        MOVX  A,@DPTR
        MOV   @R1,A
    %IF(%TGT_T89C51RD2)THEN(
        MOV   DPTR,#uL_ADR    ; zalohuj s vlastni adresou pro presmerovani
        MOVX  A,@DPTR         ; na bootloader
        MOV   SADDR,A
    )FI
S_DEB98:JMP   S_WAITD

PR_CM80:
%IF (%WITH_SYNCH) THEN(
	CJNE  A,#uL_SYN,PR_CM90
	CALL   _ul_synch
	JMP   S_WAITD
)FI

PR_CM90:
%IF (%DY_ADDR)THEN(
        CJNE  A,#uL_GST,NAK_CMD
        JMP   UD_SNST	      ; Cteni statusu a podpora dynamicke adresace
)FI

NAK_CMD:MOV   R1,#uL_NAK
        JMP   REC_EN4

WAK_CMD:MOV   R1,#uL_WAK
        JMP   REC_EN4

REC_CME:CALL  REC_ERR
        JMP   REC_CMD

SND_OB1:CJNE  R1,#0,SND_OB2
        CALL  SND_BEG
        SJMP  SND_OB3
SND_OB2:CALL  SND_BEG
        CALL  SND_Bx
SND_OB3:CALL  SND_ENDST	      ; Vysli zakonceni podle uL_ST
        MOV   DPTR,#uL_DA     ; oprav adresu DA
        MOVX  A,@DPTR
        ANL   A,#07FH
        MOVX  @DPTR,A
        MOV   DPTR,#uL_ST     ; Status zpravy z predchoziho GET_NXT
        MOVX  A,@DPTR
        ORL   A,#uLBF_PROC    ; nastavit priznak doruceni zpravy
        MOVX  @DPTR,A
    %IF(%WITH_TAILS)THEN(
    	;MOV   C,ACC.uLBF_TAILb
    	MOV   C,ACC.5	      ; !!!! ASX8051 is broken
	MOV   uLF_TMP,C
        JB    uLF_TPF,SND_OB5
        JC    SND_OB4
    )FI
        CALL  GET_IBA         ; otevreni vstupni zpravy
	CALL  IB_BEGR
	JZ    SND_OBE	      ; IBfull
        CALL  IB_ENDR         ; zapis zpravy o nulove delce s vysilanymi parametry
SND_OBE:SETB  uLF_INE         ; zprava ve vstupni fronte
        JMP   S_ENDT

%IF(%WITH_TAILS)THEN(
SND_OB4:MOV   DPTR,#P_NDB
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	XCH   A,R0
        MOV   DPTR,#P_TI
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R0
	MOVX  @DPTR,A
SND_OB5:SETB  REN_U
    %IF (%NEG_DR_EO) THEN (
        CLR   DR_EO           ; Prepnout na prijem
    )ELSE(
        SETB   DR_EO          ; Prepnout na prijem
    )FI
        MOV   DPTR,#P_TO
	MOV   A,R2
	MOVX  @DPTR,A	      ; Zapamatovat si pozici v OB
	INC   DPTR
	MOV   A,R3
	MOVX  @DPTR,A
        CALL  GET_IBA         ; otevreni vstupni zpravy
        MOV   DPTR,#P_TI
	MOVX  A,@DPTR
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	CALL  IB_BEGR
	JZ    SND_OBF
	MOV   C,uLF_TPF
        CALL  IB_ENDRT        ; zapis zpravy o nulove delce s vysilanymi parametry
SND_OBF:SETB  uLF_TPF
SND_OB6:JB    uLF_TMP,SND_OB7

	MOV   DPTR,#P_TI
	MOVX  A,@DPTR
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
        MOV   DPTR,#P_NDB
	MOVX  A,@DPTR
	XCH   A,R2
	MOVX  @DPTR,A
	INC   DPTR
	MOVX  A,@DPTR
	XCH   A,R3
	MOVX  @DPTR,A
	MOV   DPL,R2
	MOV   DPH,R3
	MOVX  A,@DPTR
	ORL   A,#80H	      ; Onacit platnost vstupnich dat
	MOVX  @DPTR,A
        SETB  uLF_INE         ; zprava ve vstupni fronte
        JMP   S_ENDT
	
SND_OB7:
        MOV   DPTR,#P_TO
	MOVX  A,@DPTR
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
        MOV   DPTR,#H_BOB
	MOVX  A,@DPTR
	MOV   R4,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R5,A
	
	CALL  GET_NXT	      ; Dalsi ramec, AC=uLBF_RECb
	JNB   ACC.7,V5_ERR
	JB    AC,REC_TA1
	JMP   SND_OB1
	
REC_TA1:MOV   uLF_TMP,C	      ; Pokracuje dalsim ramcem 
	MOV   DPTR,#P_TO
	MOV   A,R6
	MOVX  @DPTR,A	      ; Zapamatovat si pozici v OB
	INC   DPTR
	MOV   A,R7
	MOVX  @DPTR,A

	MOV   R1,#uL_BEG      ; Cekani ze SWAIT
        MOV   R0,#9
REC_TA3:CALL  WTF_CHR
        JNB   AC,REC_TA3
	MOV   R0,A
        CALL  REC_BET         ; prijem tailu
        CALL  GET_IBA         ; Prijem do IB
        MOV   DPTR,#P_TI
	MOVX  A,@DPTR
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
        CALL  IB_BEGR
        JZ    REC_TAER
        CALL  REC_Bx
        JNC   REC_TAER
        CALL  REC_EN2
        MOV   DPTR,#uL_ST     ; priznak doruceni zpravy
        MOV   A,#(uLBF_REC OR uLBF_PROC)
	MOV   C,uLF_TMP
	;MOV   ACC.uLBF_TAILb,C
	MOV   ACC.5,C	      ; !!!! ASX8051 is broken
        MOVX  @DPTR,A
	SETB  C
        CALL  IB_ENDRT        ; Prepisuje vse krome R1
        JMP   SND_OB6

REC_TAER:CALL REC_ERR
V5_ERR:	JMP   S_ERR
)FI


; Priprava pro cteni a zapis pameti

S_PRPMM:MOV   DPTR,#PROC_BUF
        MOV   R0,#AR2
S_PRPM1:MOVX  A,@DPTR
        MOV   @R0,A
        INC   R0
        INC   DPTR
        CJNE  R0,#AR7+1,S_PRPM1
        MOV   A,R4
        XCH   A,R2
        MOV   R4,A
        MOV   A,R5
        XCH   A,R3
        MOV   R0,A
        CJNE  R4,#9,S_PRPM2
        MOV   A,#SER_STACK
        SJMP  S_PRPM4
S_PRPM2:CJNE  R4,#8,S_PRPM3
        MOV   A,SER_STACK
        INC   A
        SJMP  S_PRPM4
S_PRPM3:CJNE  R4,#7,S_PRPM5
        MOV   R0,SER_STACK
        DEC   R0
        MOV   A,@R0
        ANL   A,#18H
S_PRPM4:ADD   A,R2
        MOV   R2,A
        MOV   R4,#1
S_PRPM5:MOV   AR5,R4

; Pricte k R67 R23

AR67R23:MOV   A,R6
        ADD   A,R2
        MOV   R6,A
        MOV   A,R7
        ADDC  A,R3
        MOV   R7,A
        CLR   C
        SUBB  A,R5
        JC    AR67R6R
        ADD   A,R4
        MOV   R7,A
AR67R6R:RET

; Priprav IB pro prijem dat

IB_BEGR:MOV   R0,#7
IB_BEG1:CLR   A
        CALL  S_ACCTB
        JZ    IB_BEGG
        DJNZ  R0,IB_BEG1
IB_BEGG:RET

; Zakonceni prijmu do IB
; call: [R23] konec dat
;       uL_SA adresa vysilace
;       uL_DA adresa prijemce
; Nastavi P_NDB a ukazatel na dalsi blok
; ret:  [R23] zacatek dat
;       [R67] za konec dat
;       R0    prikaz CMD
;       nesmi mazat R1

IB_ENDR:SETB  C			; Zprava ma byt oznacena za platnou
	MOV   DPL,R2            ; Oznacit za prazdny
        MOV   DPH,R3            ; blok za prijatou zpravou
        CLR   A
        MOVX  @DPTR,A
        MOV   DPTR,#P_NDB
IB_END1:MOVX  A,@DPTR           ; Posunout ukazatel na pristi
        XCH   A,R2              ; prijimany blok za zpravu
        MOV   R6,A
        MOVX  @DPTR,A
        INC   DPTR
        MOVX  A,@DPTR
        XCH   A,R3              ; R23 na zacatek bloku
        MOV   R7,A              ; R67 za konec bloku
        MOVX  @DPTR,A
        MOV   DPTR,#uL_SA
        MOVX  A,@DPTR
        MOV   ACC.7,C           ; Ulozit SA or 80H a tim
        CALL  S_ACCTB           ; oznacit zpravu za platnou
        MOV   DPTR,#uL_DA
        MOVX  A,@DPTR
        CALL  S_ACCTB           ; Ulozit DA
        MOV   A,R6
        CALL  S_ACCTB           ; Ulozit ukazatel za zpravu
        MOV   A,R7              ; na pristi blok
        CALL  S_ACCTB
        MOV   DPTR,#uL_CMD
        MOVX  A,@DPTR
        CALL  S_ACCTB           ; Ulozit CMD zpravy
        MOV   DPTR,#uL_ST
        MOVX  A,@DPTR
        CALL  S_ACCTB           ; Ulozit status zpravy
        MOV   DPTR,#uL_STA
        MOVX  A,@DPTR
        CALL  S_ACCTB           ; Ulozit stamp zpravy
        RET
%IF(%WITH_TAILS)THEN(
; Verze IB_ENDR pro tailovani pracuje proti P_TI
; vstup CY informuje o stavu oznaceni zpravy za dokoncenou
IB_ENDRT:
	MOV   DPL,R2            ; Oznacit za prazdny
        MOV   DPH,R3            ; blok za prijatou zpravou
        CLR   A
        MOVX  @DPTR,A
        MOV   DPTR,#P_TI
	SJMP  IB_END1
)FI

; Naplni registry pointry do PROC_BUF
GET_PRB:MOV   R6,#LOW  PROC_BUFE ; Prijem do PROC_BUF
        MOV   R7,#HIGH PROC_BUFE
GET_PR1:MOV   R2,#LOW  PROC_BUF
        MOV   R3,#HIGH PROC_BUF
        MOV   AR5,R4
        RET

; R67 naplni ukazately na pristi blok
; uL_DA,uL_SA,uL_ST,uL_CMD
; ret: ACC .. DA
;      R0  .. command
;      R1  .. 0 pri nulove delce dat
;      CY=1 .. zprava s tailem
;      AC=1 .. zprava typu REC

GET_NXT:CALL  S_R0FB	      ; DAdr
        MOV   A,R0
	CLR   C
	JNB   ACC.7,GET_NX4
        MOV   DPTR,#uL_DA
        MOVX  @DPTR,A
        CALL  S_R0FB	      ; SAdr
        MOV   A,R0
        MOV   DPTR,#uL_SA
        MOVX  @DPTR,A
        CALL  S_R0FB	      ; Nacist ukazatel za zpravu
        MOV   A,R0
        MOV   R6,A
        CALL  S_R0FB 
        MOV   A,R0
        MOV   R7,A
        CALL  S_R0FB	      ; CMD
        MOV   A,R0
	MOV   R1,A
        MOV   DPTR,#uL_CMD 
        MOVX  @DPTR,A
        CALL  S_R0FB	      ; ST
        MOV   A,R0
        MOV   DPTR,#uL_ST
        MOVX  @DPTR,A
	;MOV   C,ACC.uLBF_RECb
	MOV   C,ACC.4         ; !!!! ASX8051 is broken
	MOV   AC,C
	;MOV   C,ACC.uLBF_TAILb
	MOV   C,ACC.5         ; !!!! ASX8051 is broken
	PUSH  PSW ; Nemelo by byt potreba
        CALL  S_R0FB          ; ACC - 0 pri nulove delce dat
        XCH   A,R1            ; presun do R1, CMD do A
        XCH   A,R0	      ; CMD konecne do R0
        MOV   DPTR,#uL_STA    ; Stamp
        MOVX  @DPTR,A
        MOV   DPTR,#uL_DA     ; DAdr
        MOVX  A,@DPTR
	POP   PSW ; Nemelo by byt potreba
GET_NX4:RET

; Posune ukazatel za aktualni vystupni zpravu
; Nesmi menit R0, R1, CY
SET_NXT:MOV   DPTR,#P_AOB
        MOV   A,R6
        MOV   R2,A
        MOVX  @DPTR,A
        INC   DPTR
        MOV   A,R7
        MOV   R3,A
        MOVX  @DPTR,A
        SJMP  S_POPDP

; Naplni registry pointry do OB

GET_OBA:MOV   DPTR,#P_AOB
        JMP   GET_BA1

; Naplni registry pointry do IB

GET_IBA:MOV   DPTR,#P_NDB
GET_BA1:MOV   R0,#AR2
GET_BA2:MOVX  A,@DPTR
        INC   DPTR
        MOV   @R0,A
        INC   R0
        CJNE  R0,#AR7+1,GET_BA2
        SJMP  S_POPDP

; Nacte prikaz CMD
G_CMD:  MOV   DPTR,#uL_CMD    ; Prikaz
        SJMP  G_MADR2

; Nacte SA
G_SA:   MOV   DPTR,#uL_SA     ; Vlastni SA
        SJMP  G_MADR2

; Nacte vlastni adresu
G_MADR: MOV   DPTR,#uL_ADR    ; Vlastni adresa
G_MADR2:MOVX  A,@DPTR
S_POPDP:RET

; Pripojeni mastera ke sbernici
; vstup vlastni adresy v R0

S_ARB:  MOV   R1,#3+1         ; 3x2 bitu arbitrace
        CLR   uLF_NB
S_ARB1:
%IF (%NEG_DR_EO) THEN (
        CLR   DR_EO
)ELSE(
        SETB  DR_EO
)FI
        JNB   RXD_U,S_ARBE
        JB    AC,S_ARBE
        CLR   TXD_U
        CALL  SND_CH1
        SETB  TXD_U
%IF (%NEG_DR_EO) THEN (
        CLR   DR_EO
)ELSE(
        SETB  DR_EO
)FI
S_ARB2: CALL  SND_CH2
        JB    AC,S_ARB2
        MOV   A,R0          ; Rotace adresy
        RR    A
        RR    A
        XCH   A,R0
        ANL   A,#3          ; Nejnizsi 2 bity
        INC   A
        MOV   R2,A          ; Pocet cekani
        DJNZ  R1,S_ARB4
        RET
S_ARB3: JB    AC,S_ARBE
        CALL  SND_SPC
S_ARB4: DJNZ  R2,S_ARB3
        SJMP  S_ARB1

S_ARBE: SETB  F0
        RET


LEN_DAT:CLR   C             ; Delka dat = R67-R23
        MOV   A,R6          ; Kdyz je vetsi nez 127 bytu
        SUBB  A,R2          ;       A = 80h+Delka/256
        MOV   R0,A          ; Jinak A = Delka
        MOV   A,R7
        SUBB  A,R3
        JNC   LEN_DA1
        CLR   C
        SUBB  A,R4
        ADD   A,R5
LEN_DA1:MOV   R1,A
        JNZ   LEN_DA2
        MOV   A,R0
        JNB   ACC.7,LEN_DA3
        CLR   A
LEN_DA2:ORL   A,#080H
LEN_DA3:RET

S_CHKSA:XCH   A,R1          ; Check sum
        XRL   A,R1
        INC   A
        XCH   A,R1
        RET

%IF (NOT %CX_MERGED_FL) THEN (
S_SNFC: MOV   DPH,R3          ; Vyslani znaku z CODE
        MOV   DPL,R2
        CLR   A
        MOVC  A,@A+DPTR
        SJMP  S_SNFB1
)FI

S_SNFB: MOV   DPH,R3          ; Vyslani znaku z bufferu
        MOV   DPL,R2
        MOVX  A,@DPTR
S_SNFB1:MOV   SBUF_U,A
        XCH   A,R1          ; Check sum
        XRL   A,R1
        INC   A
        XCH   A,R1
        SJMP  S_ACCT1

S_R0FB: MOV   DPH,R3          ; Naplneni R0 z bufferu
        MOV   DPL,R2
        MOVX  A,@DPTR
        MOV   R0,A
        SJMP  S_ACCT1

S_RCTB: MOV   A,SBUF_U      ; Prijem znaku do bufferu
S_RCTB1:XCH   A,R1          ; Check sum
        XRL   A,R1
        INC   A
        XCH   A,R1
S_ACCTB:MOV   DPH,R3
        MOV   DPL,R2
        MOVX  @DPTR,A
S_ACCT1:
S_INCP: INC   R2            ; Pripraveni nasledujici adresy
	MOV   A,R2
        JNZ   S_INCP1
        INC   R3
S_INCP1:MOV   A,R5
        XRL   A,R3
        JNZ   S_EQP
        MOV   A,R4
        MOV   R3,A
S_EQP:  MOV   A,R2          ; Kontrola prostoru pro data
        XRL   A,R6          ; ACC=0 => konec dat nebo preteceni IB
        JNZ   S_EQP1
        MOV   A,R3
        XRL   A,R7
S_EQP1: RET

%IF(%TGT_MSC1210) THEN (
S_RCTBF:XCH   A,R1          ; Check sum
        XRL   A,R1
        INC   A
        XCH   A,R1
S_ACCTBF:MOV   DPH,R3
        MOV   DPL,R2
        MOV   USEC,#R_USEC
	MOV   C,EAI
	CLR   EAI
        MOV   MWS,#01h      ; zapisuj do flash
        CALL  MSC_WRITE_FLASH
        MOV   MWS,#00h      ; konec zapisu
	MOV   EAI,C
        JMP   S_ACCT1
)FI


%IF (%DY_ADDR)THEN(
; *************************************
; Rutina vysilani statusu CMD=0C1H
UD_SNST:MOV   A,R0
	MOV   C,ACC.0
	MOV   F0,C
	CALL  S_EQP
	JZ    SNSTA04
	CALL  S_R0FB
	MOV   A,R0
	ANL   A,#0FCH		; Funkce 0-3, start of cycle 
	JNZ   SNSTA10
        JB    uLD_RQA,SNSTA04	; Snaha o zviditelneni
	INC   uL_FLD
	JNB   uLD_RQA,SNSTA04
SNSTA02:; ORL   uL_FLD,#7
	MOV   DPTR,#uL_SA
	MOVX  A,@DPTR
	MOV   DPTR,#UD_DYSA	; Server dynamickych adres
	MOVX  @DPTR,A
	MOV   A,R0
	JNZ   SNSTA03		; Nastavi vlastni adresu
	MOV   DPTR,#uL_ADR	; na nulu pri spatnem SN
	MOVX  @DPTR,A		; v dotazu nebo funkci 0
SNSTA03:
SNSTA04:JMP   SNSTAR

SNSTA10:ANL   A,#0F0H
	CJNE  A,#010H,SNSTA04
	MOV   A,R0		; Prikaz cteni udaju
	MOV   DPTR,#UD_SFN
	MOVX  @DPTR,A
	MOV   DPTR,#SER_NUM	; Kontrola serioveho cisla
	MOV   R1,#4
SNSTA11:CALL  S_EQP
	JZ    SNSTA12
	CALL  S_R0FB
	MOVX  A,@DPTR
	XRL   A,R0
	JNZ   SNSTA12
	INC   DPTR
	DJNZ  R1,SNSTA11
SNSTA12:MOV   R0,#0
	JNZ   SNSTA02		; Nesouhlasi cislo
SNSTA13:MOV   C,F0
	MOV   ACC.0,C
	MOV   R0,A
	CALL  ACK_CMD
	ANL   uL_FLD,#NOT 7
SNSTA20:CALL  SND_BEB
        MOV   R2,#LOW SER_NUM; ; Vysle SER_NUM
        MOV   R3,#HIGH SER_NUM
        MOV   R6,#LOW (SER_NUM+4)
        MOV   R7,#HIGH (SER_NUM+4)
        MOV   R5,AR4
        CALL  SND_Bx          
	JB    uLD_SCIP,SNSTA50
	SETB  uLD_SSIP
	MOV   DPTR,#UD_STAD	; ukazatel na buffer stavu
	MOVX  A,@DPTR		; do R23 a delka do R6
	MOV   R2,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R3,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   R6,A
	MOV   DPTR,#UD_SFN	; stavova subfunkce
	MOVX  A,@DPTR
	CJNE  A,#010H,SNSTA30
	MOV   A,R4		; Vyslani zakladnich udaju
	MOV   R5,A		; no buff wrap
	MOV   A,R6
	JZ    SNSTA50
	CJNE  A,#2,SNSTA28
SNSTA27:CALL  S_R0FB		; vysle 2 byte z UD_STAD
	MOV   A,R0
	MOV   R6,A
	CALL  S_R0FB
	MOV   A,R6
	CALL  SND_CHC ; mod
	MOV   A,R0
	CALL  SND_CHC ; chyby
	SJMP  SNSTA50
SNSTA28:ADD   A,R2
	MOV   R6,A
	CLR   A			; vysle UD_STLN byte z UD_STAD
	ADDC  A,R3
	MOV   R7,A
	CALL  SND_Bx
	SJMP  SNSTA50
SNSTA30:CJNE  A,#011H,SNSTA50
				; Vyslani servisnich udaju

SNSTA50:CALL  SND_END
	CLR   uLD_SSIP
SNSTAR: JMP   S_WAITD
;	JMP   NAK_CMD
)FI

VS_FNC1:JMP   S_FNC1
; *************************************
; Inicializace a sluzby RS-485
; *************************************
_uL_FNCC:XCH   A,R7         ; V R7 je cislo funkce
        MOV    R0,A
        XCH   A,R7          ; ACC nazpet
        SJMP  uL_FNC
uL_INIT:MOV   R0,#0
        MOV   R2,#0
uL_FNC: CJNE  R0,#0,VS_FNC1
S_FN0:  CLR   ES_U          ; Po nastaveni ryclosti a adresy se
        CLR   ET1           ; touto funkci spusti RS485
%IF (%NEG_DR_EO) THEN (
        CLR   DR_EO
)ELSE(
        SETB  DR_EO
)FI
;       CLR   DR_EI
        CLR   PT1
        ANL   TMOD,#00FH
        ORL   TMOD,#020H
        SETB  TR1
        MOV   DPTR,#uL_SPD
        MOVX  A,@DPTR
        JNZ   S_FN0_0
        CALL  S_FN1_0
S_FN0_0:MOV   TH1,A
S_FN6_0:MOV   SCON_U,#11010000B
        ORL   PCON_U,#10000000B ; Bd = OSC/12/16/(256-TH1)
        SETB  PS
        CLR   A             ; Vstupni bod pro uzivatelsky baud gen
        MOV   DPTR,#uL_CMD
        MOV   R1,#PROC_BUFE-uL_CMD
S_FN0_1:MOVX  @DPTR,A
        INC   DPTR
        DJNZ  R1,S_FN0_1
        MOV   A,R2          ; Zazalohovani autoinicializace
	PUSH  ACC
        MOV   DPTR,#uL_HBIB
        MOV   R1,#3
        CALL  LDRFDP
        POP   ACC
        JNZ   S_FN0_2	    ; Autoinicializace ? 
        CALL  S_FN3_0
	MOV   A,R2
S_FN0_2:MOV   DPTR,#P_NDB+1
        MOVX  @DPTR,A       ; P_NDB=BEG_IB
        INC   DPTR
        MOVX  @DPTR,A       ; H_BIB=BEG_IB
        MOV   DPTR,#P_NPD+1
        MOVX  @DPTR,A       ; P_NPD=BEG_IB
        INC   DPTR
        INC   DPTR
        MOVX  @DPTR,A       ; P_AID=BEG_IB
        MOV   DPH,A
        CLR   A
        MOV   DPL,A
        MOVX  @DPTR,A
        MOV   DPTR,#H_EIB
        MOV   A,R2
        ADD   A,R3
        MOVX  @DPTR,A       ; H_EIB=END_IB
        MOV   DPTR,#P_AOB+1
        MOVX  @DPTR,A       ; P_AOB=BEG_OB
        INC   DPTR
        MOVX  @DPTR,A       ; H_BOB=BEG_OB
        MOV   DPTR,#P_NOB+1
        MOVX  @DPTR,A       ; P_NOB=BEG_OB
        INC   DPTR
        INC   DPTR
        MOVX  @DPTR,A       ; P_AOD=BEG_OB
        MOV   DPH,A
        CLR   A
        MOV   DPL,A
        MOVX  @DPTR,A
        MOV   A,R2
        ADD   A,R3
        ADD   A,R4
        MOV   DPTR,#H_EOB   ; H_EOB=END_OB
        MOVX  @DPTR,A
        MOV   A,R2
        MOV   DPTR,#P_EID+1
        MOVX  @DPTR,A       ; P_EID=BEG_IB
        MOV   DPTR,#uL_FRLN
        MOV   A,#LOW  S_FRLN
        MOVX  @DPTR,A
        INC   DPTR
        MOV   A,#HIGH S_FRLN
        MOVX  @DPTR,A
    %IF(%WITH_TAILS)THEN(
        CLR   uLF_OWT
    )FI
S_FN0_6:CLR   A               ; Spusteni uLan communication
        MOV   SER_STACK,A
        MOV   uL_FLG,A
        MOV   uL_FLH,A
%IF (%VECTOR_FL)THEN(
        MOV   R4,#SINT_U
        MOV   DPTR,#S_INT
        CALL  VEC_SET
        MOV   R4,#V_uL_FNC
        MOV   DPTR,#uL_FNC
        CALL  VEC_SET
        MOV   R4,#V_uL_ADD
        MOV   DPTR,#uL_EADD
        CALL  VEC_SET
)ELSE(
%IF (0)THEN(            ; Test systemu bez slouceneho
V_uL_ADD EQU   1EH      ; CODE a XDATA na systemu se sloucenim
V_uL_FNC EQU   26H
EXTRN   CODE(VEC_SET)
        MOV   R4,#SINT_U
        MOV   DPTR,#S_INT
        CALL  VEC_SET
        MOV   R4,#V_uL_FNC
        MOV   DPTR,#uL_FNC
        CALL  VEC_SET
)FI)FI
%IF(%DY_ADDR)THEN(
	MOV   uL_FLD,#3	; uLD_RQA
	MOV   DPTR,#UD_STLN
	CLR   A
	MOVX  @DPTR,A
)FI
%IF (%WITH_INTR) THEN(
        SETB  ES_U
)FI
        RET

S_FNC1: CJNE  R0,#1,S_FNC2  ; Nastavi ryclost 57.6 kBd/ACC
S_FN1_0:MOV   DPTR,#uL_SPD
        JNZ   S_FN1_1
        MOV   A,#S_SPEED
S_FN1_1:CPL   A
        INC   A
        MOVX  @DPTR,A
        MOV   TH1,A
        RET

S_FNC2: CJNE  R0,#2,S_FNC3  ; Nastavuje adresu podle ACC
        MOV   DPTR,#uL_ADR
        MOVX  @DPTR,A
        RET

S_FNC3: CJNE  R0,#3,S_FNC4  ; Nastavi pocatek IB na R2
        CLR   ES_U
        CLR   A
        MOV   SER_STACK,A
        CJNE  R2,#0,S_FN3_1 ; delku IB na R3 a delku OB na R4
S_FN3_0:MOV   R2,#HIGH BEG_IB ; Kdyz je R2=0 provede se autoinicializace
        MOV   R3,#LENG_IB
        MOV   R4,#LENG_OB
S_FN3_1:MOV   DPTR,#uL_HBIB
        MOV   R1,#3
        JMP   SVRBDP

S_FNC4:

S_FNC5:

S_FNC6: CJNE  R0,#6,S_FNC7    ; Spusti komunikaci s baud generatorem
        CLR   ES_U            ; pripravenym uzivatelem
%IF (%NEG_DR_EO) THEN (
        CLR   DR_EO
)ELSE(
        SETB  DR_EO
)FI
        JMP   S_FN6_0

S_FNC7: CJNE  R0,#7,S_FNC8    ; Zapnuti/Vypnuti prijimani vsech zprav na sbernici
        CLR   uLF_ARC
        JZ    S_FNC71
        SETB  uLF_ARC
S_FNC71:RET

S_FNC8:

S_FNC10:CJNE  R0,#10h,S_FNC11
uL_O_OP:MOV   A,R5	      ; Otevre vystupni zpravu pro Dadr R4 s Com R5
	MOV   R6,#uLBF_AAP
	JB    ACC.7,uL_O_OP1
	MOV   R6,#uLBF_ARQ
	JB    ACC.6,uL_O_OP2
uL_O_OP1:CJNE R4,#0,uL_O_OP3
uL_O_OP2:DEC  R6	      ; No acknowledge uLBF_ACK
uL_O_OP3:MOV  R7,#0
uL_O_NEW:		      ; Dalsi parametry Status R6, Stamp R7
	PUSH  DPL	      ; pri promiskuitnim rezimu uLF_ARC
        PUSH  DPH	      ; je SAdr nastaveno na uL_ISAD
	MOV   DPTR,#P_NOB
        CALL  S_GER23
        CALL  S_PUR23
        MOV   A,R4            ; DAdr
        ANL   A,#7FH
        CALL  uL_WRB0
        MOV   DPTR,#uL_ADR    ; Vlastni Adresa do SAdr
	JNB   uLF_ARC,uL_O_OP6
        MOV   DPTR,#uL_ISAD   ; Promiskuitni adresa do SAdr
uL_O_OP6:MOVX A,@DPTR
        CALL  uL_WRB0
        CALL  uL_WRB0         ; End pointer Low
        CALL  uL_WRB0         ; End pointer High
        MOV   A,R5            ; Command
        CALL  uL_WRB0
        MOV   A,R6            ; Status
        CALL  uL_WRB0
        MOV   A,R7            ; Stamp
        CALL  uL_WRB0
	JNB   F0,uL_O_OP8
uL_O_ABRT1:
uL_O_OP7:MOV  DPTR,#P_NOB     ; Blokovani uL_O_CL v pripade
        CALL  S_GER23	      ; nespravne otevrene zpravy
        CALL  S_PUR23
uL_O_OP8:POP  DPH
        POP   DPL
        RET

S_GER23:MOVX  A,@DPTR
        MOV   R2,A
        INC   DPTR
        MOVX  A,@DPTR
        MOV   R3,A
        INC   DPTR
        RET

S_PUR23:MOV   A,R2
        MOVX  @DPTR,A
        INC   DPTR
        MOV   A,R3
        MOVX  @DPTR,A
        INC   DPTR
        RET

S_FNC11:CJNE  R0,#11h,S_FNC12
uL_WR:	JB    F0,uL_WRR       ; Zapise R45 bytu z @DP - Rusi R012345
uL_WR1: MOV   A,R4
        ORL   A,R5
        JZ    uL_WRR
        CALL  uL_WRB
        JB    F0,uL_WRR
        DEC   R4
        CJNE  R4,#-1,uL_WR1
        DEC   R5
        SJMP  uL_WR1
uL_WRR: RET

uL_I23O:INC   R2	      ; Posouva ukazatel R23 ve vystupnim
        CJNE  R2,#0,uL_I23R   ; cyklickem bufferu a prepisuje ho
        INC   R3	      ; do DP
        MOV   DPTR,#H_EOB
        MOVX  A,@DPTR
        XRL   A,R3
        JNZ   uL_I23R
        MOV   DPTR,#H_BOB
        MOVX  A,@DPTR
        MOV   R3,A
uL_I23R:MOV   DPL,R2
        MOV   DPH,R3
uL_O_CE:RET

S_FNC12:CJNE  R0,#12h,S_FNC13
uL_O_CL:JB    F0,uL_O_CE      ; Uzavre vystupni zpravu
        MOV   R6,DPL
        MOV   R7,DPH
        MOV   DPTR,#P_AOD
        MOVX  A,@DPTR	      ; R45 ukazatel za konec dat
        MOV   R4,A
        INC   DPTR
        MOVX  A,@DPTR
        MOV   R5,A
	MOV   DPL,R4
	MOV   DPH,A
        CLR   A
        MOVX  @DPTR,A	      ; Nastavi DAdr pristiho bloku na 0
        MOV   DPTR,#P_NOB     ; Pristi blok bude zacinat na R45
        MOVX  A,@DPTR
        MOV   R2,A
	MOV   R0,A
        MOV   A,R4
        MOVX  @DPTR,A
        INC   DPTR
        MOVX  A,@DPTR
        MOV   R3,A
	MOV   R1,A	      ; Zacatek aktualniho bloku do R01 a R23
        MOV   A,R5
        MOVX  @DPTR,A
        XRL   A,R3
        JNZ   uL_O_C1
        MOV   A,R4	      ; Vylouceni snahy o uL_O_CL
        XRL   A,R2	      ; bez predchoziho uL_O_OP
        JZ    uL_O_C6
uL_O_C1:CALL  uL_I23O	      ;Preskocit DAdr
        CALL  uL_I23O	      ;Preskocit SAdr
        MOV   A,R4
        MOVX  @DPTR,A	      ; Ulozi ukazatel na pristi blok
        CALL  uL_I23O
        MOV   A,R5
        MOVX  @DPTR,A
    %IF(%WITH_TAILS)THEN(
        CALL  uL_I23O
        CALL  uL_I23O	      ; Preskocit Command
	MOVX  A,@DPTR	      ; Status
        ;MOV   C,ACC.uLBF_TAILb
        MOV   C,ACC.5         ; !!!! ASX8051 is broken
        JNC   uL_O_C5
	JB    uLF_OWT,uL_O_C5
	MOV   DPTR,#P_AOT     ; Ulozit zacatek useku zprav
	MOV   A,R0	      ; spojenych tailovanim
	MOVX  @DPTR,A
	INC   DPTR
	MOV   A,R1
	MOVX  @DPTR,A
	SETB  uLF_OWT
uL_O_C4:MOV   DPL,R6
        MOV   DPH,R7
	RET
    )FI
uL_O_C5:MOV   DPL,R0	      ; Pocatek bloku DP=R01
        MOV   DPH,R1
	MOVX  A,@DPTR
        MOV   R4,A
        ORL   A,#80H          ; Nastavenim DAdr.8 oznaci
        MOVX  @DPTR,A	      ; blok za pripraveny k odeslani
    %IF(%WITH_TAILS)THEN(
        JC    uL_O_C4
	JNB   uLF_OWT,uL_O_C6
	CLR   uLF_OWT
        MOV   DPTR,#P_AOT
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPL,R0
	MOV   DPH,A
	MOVX  A,@DPTR
        ORL   A,#80H          ; Nastavi DAdr.8 prvniho bloku
        MOVX  @DPTR,A	      ; tailovaneho useku a tim zpusti
    )FI			      ; jeho zpracovani
uL_O_C6:MOV   DPL,R6
        MOV   DPH,R7
	SJMP  uL_O_ST

S_FNC13:CJNE  R0,#13h,S_FNC14
uL_WRB: MOVX  A,@DPTR         ; Zapise byte z @DP - Rusi R0123
uL_WRB0:MOV   R3,A
        MOV   R0,DPL
        MOV   R1,DPH
        JB    F0,uL_WRBE
        MOV   DPTR,#P_AOD
        MOVX  A,@DPTR
        MOV   R2,A
        INC   DPTR
        MOVX  A,@DPTR
        MOV   DPH,A
        XCH   A,R3
        MOV   DPL,R2
        MOVX  @DPTR,A
        INC   R2
        CJNE  R2,#0,uL_WRB1
        INC   R3
        MOV   DPTR,#H_EOB
        MOVX  A,@DPTR
        XRL   A,R3
        JNZ   uL_WRB1
        MOV   DPTR,#H_BOB
        MOVX  A,@DPTR
        MOV   R3,A
uL_WRB1:MOV   DPTR,#P_AOB+1
        MOVX  A,@DPTR
        XRL   A,R3
        JNZ   uL_WRB3
        MOV   DPTR,#P_AOB
        MOVX  A,@DPTR
        XRL   A,R2
        JNZ   uL_WRB3
        INC   DPTR
        MOVX  A,@DPTR
        XRL   A,R3
        JNZ   uL_WRB3
uL_WRBE:SETB  F0
        MOV   DPL,R0
        MOV   DPH,R1
        RET

uL_WRB3:MOV   DPTR,#P_AOD
uL_WRB4:MOV   A,R2
        MOVX  @DPTR,A
        INC   DPTR
        MOV   A,R3
        MOVX  @DPTR,A
        MOV   DPL,R0
        MOV   DPH,R1
        INC   DPTR
        RET

uL_O_ST:SETB  uLF_RS
        CLR   ES_U
        JNB   uLF_NB,uL_O_SU
        JB    uLF_SN,uL_O_SU
        SETB  TI_U
uL_O_SU:
%IF (%WITH_INTR) THEN(
	SETB  ES_U
)FI
	RET

S_FNC14:CJNE  R0,#14h,S_FNC15
uL_O_LN:MOV   R0,DPL
        MOV   R1,DPH
        MOV   DPTR,#P_AOD
        MOVX  A,@DPTR
        MOV   R2,A
        INC   DPTR
        MOVX  A,@DPTR
        MOV   R3,A
        MOV   DPTR,#P_AOB
        SETB  C
        MOVX  A,@DPTR
        SUBB  A,R2
        MOV   R2,A
        INC   DPTR
        MOVX  A,@DPTR
        INC   DPTR
        JMP   uL_O_L1

S_FNC15:CJNE  R0,#15h,S_FNC16
%IF (%CX_MERGED_FL) THEN (
uL_WRc: JMP   uL_WR
)ELSE(
uL_WRc: JB    F0,uL_WRcR      ; Zapise R45 bytu z CODE @DP
uL_WRc1:MOV   A,R4            ; Rusi R012345
        ORL   A,R5
        JZ    uL_WRcR
        CLR   A
        MOVC  A,@A+DPTR
        CALL  uL_WRB0
	JB    F0,uL_WRcR
        DEC   R4
        CJNE  R4,#-1,uL_WRc1
        DEC   R5
        SJMP  uL_WRc1
uL_WRcR:RET
)FI

S_FNC16:CJNE  R0,#16h,S_FNC17 ; abors msg
uL_O_ABRT:PUSH DPL
        PUSH  DPH
	JMP   uL_O_ABRT1


S_FNC23:CJNE  R0,#23h,S_FNC20
uL_RDB: MOV   R0,DPL          ; Nacte byte na @DP - Rusi R0123
        MOV   R1,DPH
        JB    F0,uL_WRBE
        MOV   DPTR,#P_AID
        MOVX  A,@DPTR
        MOV   R2,A
        INC   DPTR
        MOVX  A,@DPTR
        MOV   R3,A
        MOV   DPTR,#P_EID
        MOVX  A,@DPTR
        XRL   A,R2
        JNZ   uL_RDB1
        INC   DPTR
        MOVX  A,@DPTR
        XRL   A,R3
        JNZ   uL_RDB1
        JMP   uL_WRBE
uL_RDB1:MOV   DPL,R2
        MOV   DPH,R3
        MOVX  A,@DPTR
        MOV   DPL,R0
        MOV   DPH,R1
        MOVX  @DPTR,A
        INC   R2
        CJNE  R2,#0,uL_RDB2
        INC   R3
        MOV   DPTR,#H_EIB
        MOVX  A,@DPTR
        XRL   A,R3
        JNZ   uL_RDB2
        MOV   DPTR,#H_BIB
        MOVX  A,@DPTR
        MOV   R3,A
uL_RDB2:MOV   DPTR,#P_AID
	MOV   A,R2
        MOVX  @DPTR,A
        INC   DPTR
        MOV   A,R3
        MOVX  @DPTR,A
        MOV   DPL,R0
        MOV   DPH,R1
	MOVX  A,@DPTR
        INC   DPTR
        RET

S_FNC17:CJNE  R0,#17h,S_FNC23
uL_WRi: JB    F0,uL_WRiR      ; Zapise R45 bytu z IDATA dle DPL
	MOV   A,R4            ; Rusi R012345
        MOV   R5,DPL
        JZ    uL_WRiR
uL_WRi1:MOV   A,R5
	MOV   R0,A
	MOV   A,@R0
        CALL  uL_WRB0
	JB    F0,uL_WRiR
	INC   R5
        DJNZ  R4,uL_WRi1
uL_WRiR:MOV   DPL,R5
	MOV   R5,#0
	RET

S_FNC20:CJNE  R0,#20h,S_FNC21
uL_I_OP:PUSH  DPL             ; Otevre vstupni zpravu : vyplni
        PUSH  DPH             ; uL_ISAD,uL_IDAD,uL_ICOM,uL_IST,uL_ISTA
        JB    F0,uL_I_OR
        MOV   DPTR,#P_NPD
        CALL  S_GER23
        CALL  S_PUR23
	INC   R3
	MOV   DPTR,#P_EID
	CALL  S_PUR23
        MOV   DPTR,#uL_ISAD
        CALL  uL_RDB          ; SAdr
	MOV   R4,A
        JNB   ACC.7,uL_I_OE
        MOV   DPTR,#uL_ISAD
        ANL   A,#7FH	      ; Odmaskuj nejvyssi bit
	MOV   R4,A	      ; Drive bylo se 7 bitem
        MOVX  @DPTR,A
        MOV   DPTR,#uL_IDAD   ; DAdr
        CALL  uL_RDB
        ANL   A,#7FH	      ; Zrusit nejvyssi bit
	MOV   R7,A
        MOV   DPTR,#P_EID     ; Konec dat
        CALL  uL_RDB
        CALL  uL_RDB
        MOV   DPTR,#uL_ICOM   ; Command
        CALL  uL_RDB
	MOV   R5,A
        MOV   DPTR,#uL_IST    ; Status
        CALL  uL_RDB
	MOV   R6,A
        MOV   DPTR,#uL_ISTA   ; Stamp
        CALL  uL_RDB
	XCH   A,R7
	MOV   R3,A
uL_I_OR:POP   DPH
        POP   DPL
        RET

uL_I_OE:SETB  F0
        MOV   DPTR,#P_NPD
        CALL  S_GER23
        MOV   DPTR,#P_EID
        CALL  S_PUR23
        SJMP  uL_I_OR

S_FNC21:CJNE  R0,#21h,S_FNC22
uL_RD:	JB    F0,uL_RDR       ; Nacte R45 bytu na @DP - Rusi R012345
uL_RD1: MOV   A,R4
        ORL   A,R5
        JZ    uL_RDR
        CALL  uL_RDB
        JB    F0,uL_RDR
        DEC   R4
        CJNE  R4,#-1,uL_RD1
        DEC   R5
        SJMP  uL_RD1
uL_RDR: RET

S_FNC22:CJNE  R0,#22h,S_FNC24
uL_I_CL:MOV   R6,DPL          ; Uzavre vstupni zpravu
        MOV   R7,DPH
        JB    F0,uL_I_CR
        MOV   DPTR,#P_EID
        CALL  S_GER23
        MOV   DPTR,#P_NPD
        CALL  S_PUR23
uL_I_CR:MOV   DPL,R6
        MOV   DPH,R7
        RET

S_FNC24:CJNE  R0,#24h,S_FNC25
uL_I_LN:MOV   R0,DPL
        MOV   R1,DPH
        MOV   DPTR,#P_AID
        MOVX  A,@DPTR
        MOV   R2,A
        INC   DPTR
        MOVX  A,@DPTR
        MOV   R3,A
        MOV   DPTR,#P_EID
        CLR   C
        MOVX  A,@DPTR
        SUBB  A,R2
        MOV   R2,A
        INC   DPTR
        MOVX  A,@DPTR
        MOV   DPTR,#H_BIB
uL_O_L1:SUBB  A,R3
        MOV   R3,A
        JNC   uL_I_L1
        MOVX  A,@DPTR
        XCH   A,R3
        CLR   C
        SUBB  A,R4
        MOV   R3,A
        INC   DPTR
        MOVX  A,@DPTR
        ADD   A,R3
        MOV   R3,A
uL_I_L1:CLR   C
        MOV   A,R2
        SUBB  A,R4
        MOV   A,R3
        SUBB  A,R5
        JNC   uL_I_L3
uL_I_L2:SETB  F0
        MOV   A,R2
        MOV   R4,A
        MOV   A,R3
        MOV   R5,A
uL_I_L3:CLR   C
        MOV   DPTR,#uL_FRLN
        MOVX  A,@DPTR
        MOV   R2,A
        SUBB  A,R4
        INC   DPTR
        MOVX  A,@DPTR
        MOV   R3,A
        SUBB  A,R5
        JC    uL_I_L2
        MOV   DPL,R0
        MOV   DPH,R1
        RET

S_FNC25:CJNE  R0,#25h,S_FNC26   ; pocet byte do konce zpravy (delka zpravy)
uL_I_L: MOV   R0,DPL
        MOV   R1,DPH
        MOV   DPTR,#P_AID
        MOVX  A,@DPTR
        MOV   R4,A
        INC   DPTR
        MOVX  A,@DPTR
        MOV   R5,A
        MOV   DPTR,#P_EID
        CLR   C
        MOVX  A,@DPTR
        SUBB  A,R4
        MOV   R4,A
        INC   DPTR
        MOVX  A,@DPTR
        SUBB  A,R5
        MOV   R5,A
        JNC   uL_I_LL
        MOV   DPTR,#H_BIB     ; Nutna korekce cyklicnosti 
	MOVX  A,@DPTR	      ; R5=R5+(H_EIB-H_BIB)
	SUBB  A,R5	      ; R5=H_EIB-(H_BIB-R5-1)-1
	MOV   R5,A
	INC   DPTR
	MOVX  A,@DPTR ; H_EIB
	SETB  C
	SUBB  A,R5
	MOV   R5,A
uL_I_LL:MOV   DPL,R0
        MOV   DPH,R1
	RET

S_FNC26:CJNE  R0,#26h,S_FNC27 ; je na vstupu zprava
uL_I_NE:JBC   uLF_INE,uL_I_N2
uL_I_N1:CLR   CY
	RET
uL_I_N2:MOV   DPTR,#P_NPD
	MOVX  A,@DPTR
	MOV   R0,A
	INC   DPTR
	MOVX  A,@DPTR
	MOV   DPH,A
	MOV   DPL,R0
	MOVX  A,@DPTR
	JZ    uL_I_N1
	SETB  uLF_INE
	SETB  CY
	RET

S_FNC27:CJNE  R0,#27h,S_FNC28
uL_RDi: JB    F0,uL_RDiR      ; Cte R45 bytu z IDATA dle DPL
	MOV   A,R4            ; Rusi R012345
        MOV   R5,DPL
        JZ    uL_RDiR
uL_RDi1:MOV   DPTR,#uL_TMPB   ; Toto je nutne, aby uL_RDB nezapsalo
	CALL  uL_RDB          ; data na nahodnou adresu
	JB    F0,uL_RDiR
	MOV   R0,A
	MOV   A,R5
	XCH   A,R0
	MOV   @R0,A
        INC   R5
        DJNZ  R4,uL_RDi1
uL_RDiR:MOV   DPL,R5
	MOV   R5,#0
	RET

S_FNC28:
%IF (%DY_ADDR)THEN(
S_FNC30:CJNE  R0,#30h,S_FNC31
uL_T_RQA:CLR  A
	JNB  uLD_RQA,S_FNC30R
	DEC  uL_FLD
	INC  A
S_FNC30R:RET
)FI
S_FNC31:
	RET

; Nacte od R2 pres DP R1 registru
LDRFDP :MOV   A,PSW
        ANL   A,#018H
        MOV   R0,A
        INC   R0
LDRFDP1:INC   R0
        MOVX  A,@DPTR
        MOV   @R0,A
        INC   DPTR
        DJNZ  R1,LDRFDP1
        RET

; Ulozi od R2 pres DP R1 registru
SVRBDP :MOV   A,PSW
        ANL   A,#018H
        MOV   R0,A
        INC   R0
S_FN3_2:INC   R0
        MOV   A,@R0
        MOVX  @DPTR,A
        INC   DPTR
        DJNZ  R1,S_FN3_2
        RET

_uL_STR:
uL_STR: JBC   ES_U,uL_STRG
        RET
uL_STRG:JNB   uLF_NA,uL_STRE
        JNB   uLF_NB,uL_STRF
        JNB   uLF_RS,uL_STRF
        SETB  TI_U
uL_STRF:SETB  uLF_NB
uL_STRE:SETB  uLF_NA
%IF (%WITH_INTR) THEN(
        SETB  ES_U
)FI
        RET

        END
