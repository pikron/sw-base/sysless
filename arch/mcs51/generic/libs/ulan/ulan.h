#ifndef _ULAN_H
#define _ULAN_H

#ifdef __cplusplus
/*extern "C" {*/
#endif

#include <ul_lib/ul_utsubst.h>

#ifndef uchar
 #define uchar unsigned char
#endif

typedef int ul_ssize_t;
typedef unsigned int ul_size_t;
typedef int ul_fd_t;

typedef uchar ul_msg_adr_t;

typedef struct ul_idstr_t {
  char *name;
  char len; 
} ul_idstr_t;

#define UL_FD_INVALID 0

#ifndef UL_WITH_HANDLE
  #define UL_WITHOUT_HANDLE
#endif /*UL_WITH_HANDLE*/

#define UL_ARGPTRTYPE __xdata
//#define UL_ARGPTRTYPE

#undef UL_WITH_NAKED

#ifdef UL_WITH_NAKED
  #define UL_FNC_NAKED _naked
#else /*UL_WITH_NAKED*/
  #define UL_FNC_NAKED
#endif /*UL_WITH_NAKED*/

#if !defined(__SDCC_MODEL_LARGE) || !(defined(SDCC) || defined(__SDCC))
  #define UL_FNC_REENTRANT
#else
  #define UL_FNC_REENTRANT __reentrant
#endif

/*******************************************************************/

/* ul_mess_head flags definition defines proccessing 
   of message or its frames stored in bll */
/*#define UL_BFL_LOCK 0x00*/	/* locked message is pointed only once */
/*#define UL_BFL_MSST 0x00*/      /* Message must be received by some proccess */
/*#define UL_BFL_M2IN 0x00*/      /* After succesfull proccessing move to proc_bll */
/*#define UL_BFL_LNMM 0x00*/      /* Length of received frame must match expected len */
/*#define UL_BFL_VERL 0x00*/      /* Verify free space in buffer of destination station */
/*#define UL_BFL_REWA 0x00*/      /* If error occurs do wait with retry */

#define UL_BFL_NORE 0x40      /* Do not try to repeat if error occurs */
#define UL_BFL_TAIL 0x20      /* Multiframe message continues by next bll block */
#define UL_BFL_SND  0x00      /* Send this frame */
#define UL_BFL_REC  0x10      /* Receive answer frame into this bll block */
#define UL_BFL_FAIL 0x08      /* Message cannot be proccessed - error */
#define UL_BFL_PROC 0x04      /* Processed outgoing message */
#define UL_BFL_PRQ  0x02      /* Request imediate proccessing of frame by receiver station */
#define UL_BFL_ARQ  0x01      /* Request imediate acknowledge by receiving station */

typedef struct ul_msginfo {
	uchar	dadr;		/* destignation address */
	uchar	sadr;		/* source address */
	uchar	cmd;		/* command/socket number */
	uchar	flg;		/* message flags */
	uchar   stamp;		/* unigue message number */
	unsigned len;		/* length of frame */
} ul_msginfo;

/*******************************************************************/
/* command definitions */

/* standard command codes 
   00H .. 3FH    store to buffer 
   40H .. 7FH    store to buffer without ACK
   80H .. 9FH    proccess at onece
   A0H .. BFH    process with additional receive
   C0H .. FFH    process with additional send
*/

#define UL_CMD_RES	0x80	/* Reinitialize RS485 */
#define UL_CMD_SFT	0x81	/* Test free space in input buffer */
#define UL_CMD_SYN      0x85    /* Sychronization */
#define UL_CMD_SID	0xF0	/* Send identification */
#define UL_CMD_SFI	0xF1	/* Send amount of free space in IB */
#define UL_CMD_TF0	0x98	/* End of stepping */
#define UL_CMD_TF1	0x99	/* Begin of stepping */
#define UL_CMD_STP	0x9A	/* Do step */
#define UL_CMD_DEB	0x9B	/* Additional debug commands */
#define UL_CMD_SPC	0xDA	/* Send state - for 8051 PCL PCH PSW ACC */

#define UL_CMD_RDM	0xF8	/* Read memory   T T B B L L */
#define UL_CMD_WRM	0xB8	/* Write mamory  T T B B L L */
#define UL_CMD_ERM      0x88    /* Erase memory T T B B L L */

/* uLan parameter setup and initialization functions */
char	ul_drv_init(void);
char	ul_drv_set_adr(uchar addr);
char	ul_drv_set_bdiv(uchar bdiv);
void	ul_str(void);
void    ul_int(void);

/* uLan standard IO functions */
ul_fd_t	ul_open(const char *dev_name, const char *options);
int	ul_close(ul_fd_t ul_fd);
ul_ssize_t ul_read(ul_fd_t ul_fd,void *buffer, ul_size_t size);
ul_ssize_t ul_write(ul_fd_t ul_fd,const void *buffer, ul_size_t size);
int	ul_newmsg(ul_fd_t ul_fd,const ul_msginfo UL_ARGPTRTYPE  *msginfo);
int	ul_tailmsg(ul_fd_t ul_fd,const ul_msginfo UL_ARGPTRTYPE  *msginfo);
int	ul_freemsg(ul_fd_t ul_fd);
int	ul_acceptmsg(ul_fd_t ul_fd,ul_msginfo UL_ARGPTRTYPE  *msginfo);
int	ul_actailmsg(ul_fd_t ul_fd,ul_msginfo UL_ARGPTRTYPE  *msginfo);
int	ul_addfilt(ul_fd_t ul_fd,const ul_msginfo UL_ARGPTRTYPE  *msginfo);
int	ul_abortmsg(ul_fd_t ul_fd);
uchar	ul_inepoll(ul_fd_t ul_fd);
int	ul_drv_debflg(ul_fd_t ul_fd,int debug_msk);
int	ul_fd_wait(ul_fd_t ul_fd, int wait_sec);
int	ul_stroke(ul_fd_t ul_fd);
int	ul_setmyadr(ul_fd_t ul_fd, int newadr);
int	ul_setidstr(ul_fd_t ul_fd, const char *idstr);

#ifdef UL_WITHOUT_HANDLE
ul_ssize_t __ul_read_avail(void) UL_FNC_NAKED;
ul_ssize_t __ul_read(void *buffer, ul_size_t size) UL_FNC_NAKED UL_FNC_REENTRANT;
ul_ssize_t __ul_write(const void *buffer, ul_size_t size) UL_FNC_NAKED UL_FNC_REENTRANT;
int	__ul_newmsg(const ul_msginfo UL_ARGPTRTYPE  *msginfo) UL_FNC_NAKED;
int	__ul_tailmsg(const ul_msginfo UL_ARGPTRTYPE  *msginfo) UL_FNC_NAKED;
int	__ul_freemsg() UL_FNC_NAKED;
int	__ul_acceptmsg(ul_msginfo UL_ARGPTRTYPE  *msginfo) UL_FNC_NAKED;
int	__ul_actailmsg(ul_msginfo UL_ARGPTRTYPE  *msginfo) UL_FNC_NAKED;
int	__ul_addfilt(const ul_msginfo UL_ARGPTRTYPE  *msginfo);
int	__ul_abortmsg(void);
uchar	__ul_inepoll(void) UL_FNC_NAKED;
int	__ul_o_close(void) UL_FNC_NAKED;
int	__ul_i_close(void) UL_FNC_NAKED;

#define ul_read(ul_fd, buffer, size) __ul_read(buffer, size)
#define ul_write(ul_fd, buffer, size) __ul_write(buffer, size)
#define ul_newmsg(ul_fd, msginfo) __ul_newmsg(msginfo)
#define ul_tailmsg(ul_fd, msginfo) __ul_tailmsg(msginfo)
#define ul_freemsg(ul_fd) __ul_freemsg()
#define ul_acceptmsg(ul_fd, msginfo) __ul_acceptmsg(msginfo)
#define ul_actailmsg(ul_fd, msginfo) __ul_actailmsg(msginfo)
#define ul_addfilt(ul_fd, msginfo) __ul_addfilt(msginfo)
#define ul_abortmsg(ul_fd) __ul_abortmsg()
#define ul_inepoll(ul_fd) __ul_inepoll()
#define ul_i_close(ul_fd) __ul_i_close()
#define ul_o_close(ul_fd) __ul_o_close()
#define ul_stroke(ul_fd) ul_str() 
#define ul_setmyadr(ul_fd,newadr) ul_drv_set_adr(newadr)
#define ul_setidstr(ul_fd,idstr)

#endif /*UL_WITHOUT_HANDLE*/

/* simple message operations */

int ul_send_command(ul_fd_t ul_fd,int dadr,int cmd,int flg,
                    const void *buf,int len);
int ul_send_command_wait(ul_fd_t ul_fd,int dadr,int cmd,int flg,
                         const void *buf,int len);
int ul_send_query(ul_fd_t ul_fd,int dadr,int cmd,int flg,
                  const void *buf,int len);
int ul_send_query_wait(ul_fd_t ul_fd,int dadr,int cmd,int flg,
		       const void *bufin,int lenin,void **bufout,int *lenout);

/* basic uLan commands/services */

#define UL_CMD_OISV	0x10	/* Object Interface Service */
#define UL_CMD_LCDABS	0x4f	/* Absorbance data block */
#define UL_CMD_LCDMRK	0x4e	/* Mark */
#define UL_CMD_NCS	0x7f	/* Network Control Service */
#define UL_CMD_GST	0xc1	/* Fast module get status */

/* UL_CMD_NCS	Network Control Service */

#define ULNCS_ADR_RQ	  0xC0	  /* SN0 SN1 SN2 SN3 */
#define ULNCS_SET_ADDR	  0xC1	  /* SN0 SN1 SN2 SN3 NEW_ADR */
#define ULNCS_SID_RQ	  0xC2	  /* send serial num and ID string request */
#define ULNCS_SID_RPLY	  0xC3	  /* SN0 SN1 SN2 SN3 ID ... */
#define ULNCS_ADDR_NVSV	  0xC4	  /* SN0 SN1 SN2 SN3 - save addres to EEPROM */
#define ULNCS_BOOT_ACT    0xC5    /* SN0 SN1 SN2 SN3 */
#define ULNCS_BOOT_ACK    0xC6    /* SN0 SN1 SN2 SN3 */
#define ULNCS_SET_SN      0xE0    /* SN0 SN1 SN2 SN3 */

/* debugging support routines */
/*int uloi_debug_flg;*/

#define UL_LOGL_MASK (0xff)
#define UL_LOGL_CONT (0x1000)

/*typedef void (ul_log_fnc_t)(const char *domain, int level,
	const char *format, va_list ap);*/

/*void ul_log(const char *domain, int level,
	const char *format, ...);*/
//#define ul_log(domain,level,format, ...)

/*void ul_log_redir(ul_log_fnc_t *log_fnc, int add_flags);*/
//#define ul_log_redir(log_fnc, add_flags)

#ifdef UL_WITHOUT_HANDLE
  /* 
   * this is used to preserve space and speed consumed by creation
   * of many local instances of these variables on small MCU systems
   */
  extern ul_fd_t	ul_fd;
  extern UL_ARGPTRTYPE ul_msginfo msginfo;
#endif /*UL_WITH_HANDLE*/
        
#ifdef __cplusplus
/*}*/ /* extern "C"*/
#endif

#endif /* _ULAN_H */

