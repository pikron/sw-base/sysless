// Texas Instruments
// Name:	Reg1210.h
// Revision: 	1.0
// Description: Header file for TI MSC1210 microcontroller

///////////////////////////////////////////////////////////////////////////////
// Keyval
#define KVPB_CHUNK_SIZE 1

#if defined(SDCC) || defined(__SDCC)
  #include <at89c51ed2.h>
__sfr __at (0xD1) FCON        ;
__sfr __at (0x8F) CKCON0      ;
#else
  #ifdef __KEIL__
    #include <atmel/at89c51ed2.h>
  #else
    #error Unknown 8051 kompiler
  #endif
#endif
