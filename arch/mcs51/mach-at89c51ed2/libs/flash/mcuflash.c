#include "mcuflash.h"
#include "mcu_regs.h"

bit ea,flashed;

int flash_erase(void *base,int size) {
  ea=EA;EA=0;
  flashed=0;
  while (size--) {
    FCON=0x08;
    *(uint8_t __xdata*)base=0xff;
    FCON=0x0;
    flashed=1;
    if (((unsigned char)base&0x7f)==0x7f) { /* end of page */
      FCON=0x50;FCON=0xA0;
      while(FCON&1);
      flashed=0;
    }
    base=(uint8_t *)base+1;
  }
  if (flashed) {
    FCON=0x50;FCON=0xA0;
    while(FCON&1);
  }
  EA=ea;
  return 0;
}

int flash_copy(void *des,const void *src,int len) {
  ea=EA;EA=0;
  flashed=0;
  while(len--) {
    uint8_t __data val;
    val=*(uint8_t *)src;
    FCON=0x08;
    *(uint8_t __xdata*)des=val;
    FCON=0x0;
    flashed=1;
    if (((unsigned char)des&0x7f)==0x7f) { /* end of page */
      FCON=0x50;FCON=0xA0;
      while(FCON&1);
      flashed=0;
    }
    src=(uint8_t *)src+1;
    des=(uint8_t *)des+1;
  }
  if (flashed) {
    FCON=0x50;FCON=0xA0;
    while(FCON&1);
  }
  EA=ea;
  return len;
}

int flash_flush(void)
{
  return 0;
}
