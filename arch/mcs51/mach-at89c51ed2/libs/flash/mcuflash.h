#ifndef _MCUFLASH_H
#define _MCUFLASH_H

#ifdef __cplusplus
/*extern "C" {*/
#endif

#include <stdint.h>

int flash_erase(void *base,int size);
int flash_copy(void *des,const void *src,int len);
int flash_flush(void);

#ifdef __cplusplus
/*}*/ /* extern "C"*/
#endif

#endif /* _MSCFLASH_H */

