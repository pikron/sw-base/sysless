// Texas Instruments
// Name:	Reg1210.h
// Revision: 	1.0
// Description: Header file for TI MSC1210 microcontroller

///////////////////////////////////////////////////////////////////////////////
// Keyval
#define KVPB_CHUNK_SIZE 1

#if defined(SDCC) || defined(__SDCC)
  #include "reg1210.hsd"
#else
  #ifdef __KEIL__
    #include "reg1210.hke"
  #else
    #error Unknown 8051 kompiler
  #endif
#endif
