#include <system_def.h>
#include <cpu_def.h>

int
msc_adc_ready(void) {
   return AIE&0x20; 
}


long 
msc_adc_unipolar(void) {
  __asm
	mov  a,#0
	mov  b,_ADRESH
	mov  dph,_ADRESM
	mov  dpl,_ADRESL
  __endasm;

/*  #pragma asm
	mov     r4,#0
	mov     r5,ADRESH
	mov     r6,ADRESM
	mov     r7,ADRESL
  #pragma endasm*/
}


long 
msc_adc_bipolar(void) {
/*  #pragma asm
        mov     a,ADRESH
        mov     r5,a
	mov     c,acc.7
	subb    a,acc
        mov     r4,a
        mov     r6,ADRESM
        mov     r7,ADRESL
  #pragma endasm*/
}

long 
msc_adc_summer(void) {
/*  #pragma asm
        mov     r4,SUMR3
        mov     r5,SUMR2
        mov     r6,SUMR1
        mov     r7,SUMR0
  #pragma endasm*/
}


void
msc_adc_set_input(unsigned char admux, unsigned char adcon1) {


  ADMUX = admux;		

  //         7  6   5   4   3   2    1    0
  // ADCON1  -  POL SM1 SM0 -   CAL2 CAL1 CAL0
  ADCON1 = adcon1;	// bipolar, auto, self calibration, offset
}

int 
msc_adc_init(void) {
  PDCON &= 0xf7; 	//turn on adc
  			// ACLK min 32
  ACLK = 15;		// ACLK = 18.432MHz/(1+1)= 0.9216MHz
  
  // max 2047		// Data Rate = (ACLK + 1)/64/DECIMATION
//  DECIMATION = 1080;	// Data Rate = 18.432/2/64/14400 = 10.00Hz
  ADCON2=1080%256;
  ADCON3=1080/256;

  //         7   6    5      4     3    2    1    0
  // ADCON0  -   BOD  EVREF  VREFH EBUF PGA2 PGA1 PGA0
  ADCON0 = 0x30;	// Vref on 2.5V, Buff on, BOD off, PGA 1
  return 0;
}
