#ifndef _MSC_ADC_H
#define _MSC_ADC_H

#include <stdint.h>

#define MSC_ADC_LSB_BIPOLAR 298.0232e-9
#define MSC_ADC_LSB_UNIPOLAR 149.0116e-9
#define MSC_ADC_V2T_INTERNAL(x) (2664.7*(x)-282.14)

typedef struct msc_adc_input {
    uint8_t ain;		//input
    uint8_t adcon1;		//ad controler 1
    float avg_value;		//avg value
    uint8_t samples;		//period per avg value
    uint8_t samples_cnt;	//counter period per avg value
    uint8_t calibration;
  } msc_adc_input_t;

typedef struct msc_adc_input_list {
    uint8_t count;
    int8_t capacity;
    msc_adc_input_t **inputs;
    uint8_t input_position;
  } msc_adc_input_list_t;

int msc_adc_ready(void);
int msc_adc_init(void);
void msc_adc_set_input(unsigned char admux, unsigned char adcon1);
long msc_adc_unipolar(void);

#endif /* _MSC_ADC_H */
