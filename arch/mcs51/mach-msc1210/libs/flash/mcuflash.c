#include "mcuflash.h"
#include "mcu_regs.h"

#if defined(SDCC) || defined(__SDCC)
#ifndef __SDCC_MODEL_LARGE
__data char mscflash_fdata;
__data char mscflash_fdm;
#endif /* SDCC_MODEL_LARGE */
#endif /*SDCC*/
__bit ea,eai;

#if defined(SDCC) || defined(__SDCC)
#ifndef __SDCC_MODEL_LARGE
char __page_erase (int faddr, char fdata, char fdm) 
{
  mscflash_fdata=fdata;
  mscflash_fdm=fdm;
  ea=EA,eai=EAI;
  EA=0;EAI=0;   // Disable interrupts
  __asm
	mov  r7,dpl
	mov  r6,dph
	mov  a,_mscflash_fdata
	mov  r5,a
	mov  a,_mscflash_fdm
	mov  r3,a
	lcall 0xFFD7
	mov  dpl,r7
  __endasm;
  EAI=eai;EA=ea;
}
#else
/* For large model
 * vec   Allocated to stack - offset -4  (the push _bp taken into account)
 * fnc Allocated to registers 
 */
char __page_erase (int faddr, char fdata, char fdm) __reentrant
{
  ea=EA,eai=EAI;
  EA=0;EAI=0;   // Disable interrupts
  __asm
	mov  r7,dpl
	mov  r6,dph
	mov  a,sp
	add  a,#-4
	mov  r0,a
	mov  a,@r0
	mov  r3,a
	inc  r0
	mov  a,@r0
	mov  r5,a
	lcall 0xFFD7
	mov  dpl,r7
  __endasm;
  EAI=eai;EA=ea;
}
#endif /* SDCC_MODEL_LARGE */
#else /* SDCC */
char __page_erase (int faddr, char fdata, char fdm)
{
  ea=EA,eai=EAI;
  EA=0;EAI=0;   // Disable interrupts
  #pragma asm
	lcall 0xFFD7
  #pragma endasm
  EAI=eai;EA=ea;
}
#endif /* SDCC */


#if defined(SDCC) || defined(__SDCC)
#ifndef __SDCC_MODEL_LARGE
char __write_flash_chk (int faddr, char fdata, char fdm) 
{
  mscflash_fdata=fdata;
  mscflash_fdm=fdm;
  ea=EA,eai=EAI;
  EA=0;EAI=0;   // Disable interrupts
  __asm
	mov  r7,dpl
	mov  r6,dph
	mov  a,_mscflash_fdata
	mov  r5,a
	mov  a,_mscflash_fdm
	mov  r3,a
	lcall 0xFFDB
	mov  dpl,r7
  __endasm;
  EAI=eai;EA=ea;
}
#else
/* For large model
 * vec   Allocated to stack - offset -4  (the push _bp taken into account)
 * fnc Allocated to registers 
 */
char __write_flash_chk (int faddr, char fdata, char fdm) __reentrant
{
  ea=EA,eai=EAI;
  EA=0;EAI=0;   // Disable interrupts
  __asm
	mov  r7,dpl
	mov  r6,dph
	mov  a,sp
	add  a,#-4
	mov  r0,a
	mov  a,@r0
	mov  r3,a
	inc  r0
	mov  a,@r0
	mov  r5,a
	lcall 0xFFDB
	mov  dpl,r7
  __endasm;
  EAI=eai;EA=ea;
}
#endif /* SDCC_MODEL_LARGE */
#else /* SDCC */
char __write_flash_chk (int faddr, char fdata, char fdm) 
{
  ea=EA,eai=EAI;
  EA=0;EAI=0;   // Disable interrupts
  #pragma asm
	call 0FFDBH
  #pragma endasm
  EAI=eai;EA=ea;
}
#endif /* SDCC */

int flash_erase(void *base,int size) {
  size=size-((unsigned int)base&0x7f);
  base=(__xdata uint8_t*)((unsigned int)base&0xFF80);
  while (size) {
    page_erase ((unsigned int)base, 0xff, MSC_FPM);
    base=(uint8_t *)base+0x80;
    size-=0x80;
  }
  return 0;
}

int flash_copy(void *des,const void *src,int len) {
  while(len) {
    write_flash_chk ((unsigned int)des, *(uint8_t *)src, MSC_FPM);
    src=(uint8_t *)src+1;
    des=(uint8_t *)des+1; 
    len--;
  }
  return len;
}

int flash_flush(void)
{
  return 0;
}
