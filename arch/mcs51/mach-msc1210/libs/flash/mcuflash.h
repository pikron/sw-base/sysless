#ifndef _MCUFLASH_H
#define _MCUFLASH_H

#ifdef __cplusplus
/*extern "C" {*/
#endif

#include <stdint.h>

#if !defined(__SDCC_MODEL_LARGE) || !(defined(SDCC) || defined(__SDCC))
  #define MSC_FNC_REENTRANT
#else
  #define MSC_FNC_REENTRANT __reentrant
#endif

#define MSC_FDM		1			// Flash Data Memory
#define MSC_FPM		0			// Flash Program Memory

char page_erase (int faddr, char fdata, char fdm);
char write_flash_chk (int faddr, char fdata, char fdm);
int flash_erase(void *base,int size);
int flash_copy(void *des,const void *src,int len);
int flash_flush(void);

char __page_erase (int faddr, char fdata, char fdm) MSC_FNC_REENTRANT;
char __write_flash_chk (int faddr, char fdata, char fdm) MSC_FNC_REENTRANT;

#define page_erase(faddr,fdata,fdm) __page_erase(faddr,fdata,fdm)
#define write_flash_chk(faddr,fdata,fdm) __write_flash_chk(faddr,fdata,fdm)

#ifdef __cplusplus
/*}*/ /* extern "C"*/
#endif

#endif /* _MCUFLASH_H */

