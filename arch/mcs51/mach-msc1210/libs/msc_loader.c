#include <stdint.h>
#include <system_def.h>
#include <string.h>
#include <stdio.h>

#include "usb_loader.h"

#ifdef PDI_EP0_PACKET_SIZE
  #define LOADER_BUFFER_SIZE PDI_EP0_PACKET_SIZE
#else /*PDI_EP0_PACKET_SIZE*/
  #define LOADER_BUFFER_SIZE 128
#endif /*PDI_EP0_PACKET_SIZE*/

unsigned char loader_buffer[LOADER_BUFFER_SIZE];

int msc1210_erase(unsigned addr, unsigned len)
{
  unsigned end = addr + len - 1; 
  if(addr<0x8000) //0x1000)
    return -1;
  
  while(addr < end) {
    *((__xdata unsigned char *)addr) = 0;
  }
  return 0;
}

int msc1210_mass_erase(unsigned mode, unsigned addr)
{
  while( addr != 0xffff) {
    addr++;
    if ( !(addr & 0x00ff)) printf(" erase segm. 0x%X\n", addr);
    *((__xdata unsigned char *)addr) = 0;
  }
  return 0;
}


#if defined(SDCC) || defined(__SDCC)
void msc1210_goto( unsigned addr)
{
  _asm
    pop  acc
    pop  acc
    mov  a,dpl
    push acc
    mov  a,dph
    push acc
    ret
  _endasm;
}
#else /*SDCC*/
#pragma asm
;void msc1210_goto( unsigned addr)
;{
	public _msc1210_goto
_msc1210_goto:
	pop  acc
	pop  acc
	mov  a,r7
	push acc
	mov  a,r6
	push acc
	ret
;  return 0;
;}
#pragma endasm
#endif



void msc1210_reset_device( void)
{
  printf("...reset...\n");
}


int usb_msc1210_loader(usb_device_t *udev)
{
  unsigned long addr;
  unsigned len;

  switch ( udev->request.bRequest & USB_VENDOR_MASK) {

    case USB_VENDOR_GET_CAPABILITIES:
      printf("GET_CAPABILITIES\n");
      loader_buffer[0] = 0xAA; // test
      usb_send_control_data(udev, loader_buffer, 1);
      return 1;

    case USB_VENDOR_RESET_DEVICE:
      printf("RESET_DEVICE\n");
//      InitController();
//      InitTarget();
//      if (!GetDevice()) {
//        return -1;
//      }
      usb_ack_setup(&udev->ep0);
      msc1210_reset_device();
      return 1;

    case USB_VENDOR_GOTO:
      //printf( "GOTO 0x%X\n", udev->request.wValue);
      usb_ack_setup(&udev->ep0);
      msc1210_goto(udev->request.wValue);
      return 1;

    case USB_VENDOR_ERASE_MEMORY:
      printf( "ERASE 0x%X 0x%X\n", udev->request.wValue, udev->request.wIndex);
      usb_ack_setup(&udev->ep0);
      msc1210_erase(udev->request.wValue, udev->request.wIndex);
      return 1;

    case USB_VENDOR_MASS_ERASE:
      printf( "MASSERASE 0x%X 0x%X\n", udev->request.wIndex, udev->request.wValue);
      usb_ack_setup(&udev->ep0);
      msc1210_mass_erase(udev->request.wIndex, udev->request.wValue);
      return 1;
      
    case USB_VENDOR_GET_SET_MEMORY:
      addr = (udev->request.wValue & 0xffff) | (((unsigned long)udev->request.wIndex&0xffff)<<16);
      len = udev->request.wLength;
      //printf("GET_SET_MEMORY, addr=0x%lx, len=%d\n", (long)addr, len);
      if (( udev->request.bmRequestType & USB_DATA_DIR_MASK) == USB_DATA_DIR_FROM_HOST) {
        // read from HOST ???
        switch( udev->request.bRequest & USB_VENDOR_TARGET_MASK) {
          case USB_VENDOR_TARGET_XDATA:
            udev->ep0.ptr = (__xdata unsigned char *)addr;
            break;
          case USB_VENDOR_TARGET_DATA:
            udev->ep0.ptr = (__data unsigned char *)addr;
            break;
          default: return -1;
        }
        if ( len) usb_set_control_endfnc( udev, usb_ack_setup);
        else usb_ack_setup(&udev->ep0);
        return 1;
      } else {
        switch( udev->request.bRequest & USB_VENDOR_TARGET_MASK) {
          case USB_VENDOR_TARGET_XDATA:
            usb_send_control_data( udev, (__xdata unsigned char *)addr, len); //(void*)addr, len);
            break;
          case USB_VENDOR_TARGET_DATA:
            usb_send_control_data( udev, (__data unsigned char *)addr, len); //(void*)addr, len);
            break;
          default: return -1;
        }
        return 1;
      }
      break;
  }

  return 0;
}
