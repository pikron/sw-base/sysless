#include <stdio.h>
#include <unistd.h>


FILE * stdin  = NULL;
FILE * stdout = NULL;


int write(int fd, const void *buf, int count) {
  int i;

  for (i = 0;  i < count;  i++) {
    putchar(*((char*)buf++));
  }

  return count;
}

int read(int fd, void *buf, int count) {
  int i;

  for (i = 0;  i < count;  i++) {
    *((char*)buf + i) = getchar();
  }
  
  return count;
}

int fflush(FILE *stream) {
  return 0;
}


