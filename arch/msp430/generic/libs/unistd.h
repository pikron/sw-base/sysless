#ifndef UNISTD_H
#define UNISTD_H

int write(int fd, const void *buf, int count);
int read(int fd, void *buf, int count);

#endif

