#ifndef HARDWARE_H
#define HARDWARE_H


//HW UART registers
volatile unsigned int  UBAUD asm("0x0100");
volatile unsigned char UTX   asm("0x0102");
volatile unsigned char URX   asm("0x0104");
volatile unsigned char USTAT asm("0x0106");
volatile unsigned char UIE   asm("0x0107");


#endif
