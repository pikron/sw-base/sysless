#include "hardware.h"
#include "uart.h"

int putchar(int c) {
  while (USTAT & 0x20) {}
  UTX = c;
  return 0;
}

int getchar() {
  while (!(USTAT & 0x10)) {}
  return URX;
}

int isRxEmpty() {
  return (~USTAT) & 0x10;
}
