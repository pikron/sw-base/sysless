#ifndef UART_H
#define UART_H

int putchar(int c);

int getchar();

int isRxEmpty();

#endif

