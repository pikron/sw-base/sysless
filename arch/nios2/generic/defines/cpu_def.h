#ifndef _NIOS2_CPU_DEF_H
#define _NIOS2_CPU_DEF_H

#ifndef CODE
  #define CODE
#endif

#ifndef XDATA
  #define XDATA
#endif

#ifndef DATA
  #define DATA
#endif

#ifndef __memory_barrier
#define __memory_barrier() \
__asm__ __volatile__("": : : "memory")
#endif

#define sti() do { \
        int flags; \
        flags = __builtin_rdctl(0); \
        flags |= 1; \
        __builtin_wrctl(0, flags); \
    } while(0)

#define cli() do { \
        int flags; \
        flags = __builtin_rdctl(0); \
        flags &= ~1; \
        __builtin_wrctl(0, flags); \
    } while(0)

#define save_and_cli(flags) do { \
        flags = __builtin_rdctl(0); \
        __builtin_wrctl(0, flags & ~1); \
    } while(0)

#define save_flags(flags) do { \
        flags = __builtin_rdctl(0); \
    } while(0)

#define restore_flags(flags)  do { \
        __builtin_wrctl(0, flags); \
    } while(0)

/* atomic access routines */

//typedef unsigned long atomic_t;

static inline void atomic_clear_mask(unsigned long mask, volatile unsigned long *addr)
{
        unsigned long flags;

        save_and_cli(flags);
        *addr &= ~mask;
        restore_flags(flags);
}

static inline void atomic_set_mask(unsigned long mask, volatile unsigned long *addr)
{
        unsigned long flags;

        save_and_cli(flags);
        *addr |= mask;
        restore_flags(flags);
}

static inline void set_bit(int nr, volatile unsigned long *addr)
{
        unsigned long flags;

        save_and_cli(flags);
        *addr |= 1<<nr;
        restore_flags(flags);
}

static inline void clear_bit(int nr, volatile unsigned long *addr)
{
        unsigned long flags;

        save_and_cli(flags);
        *addr &= ~(1<<nr);
        restore_flags(flags);
}

static inline int test_bit(int nr, volatile unsigned long *addr)
{
        return ((*addr) & (1<<nr))?1:0;
}

static inline int test_and_set_bit(int nr, volatile unsigned long *addr)
{
        unsigned long flags;
        long m=(1<<nr);
        long r;

        save_and_cli(flags);
        r=*addr;
        *addr=r|m;
        restore_flags(flags);
        return r&m?1:0;
}

/* Arithmetic functions */
#define sat_add_slsl(__x,__y)                                   \
        do {                                                    \
                typeof(__x) tmp;                                \
                tmp = (__x) + (__y);                            \
                if ((__x) > 0 && (__y) > 0 && tmp < 0)          \
                        tmp = +0x7fffffff;                      \
                else if ((__x) < 0 && (__y) < 0 && tmp >= 0)    \
                        tmp = -0x80000000;                      \
                (__x) = tmp;                                    \
        } while (0)

/*masked fields macros*/
#define __val2mfld(mask,val) (((mask)&~((mask)<<1))*(val)&(mask))
#define __mfld2val(mask,val) (((val)&(mask))/((mask)&~((mask)<<1)))

#endif /* _NIOS2_CPU_DEF_H */
