/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  system_def.h - common cover for definition of hardware adresses,
                 registers, timing and other hardware dependant
		 parts of embedded hardware
 
  Copyright (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#ifndef _SYSTEM_DEF_H_
#define _SYSTEM_DEF_H_

#include <stdint.h>
#include <system_stub.h>
#include <LPC177x_8x.h>
#include <bspbase.h>
#include <hal_gpio_def.h>

#ifndef MACH_LPC178X
#define MACH_LPC178X
#endif
#ifndef MACH_LPC17XX
#define MACH_LPC17XX
#endif

#ifndef NULL
#define	NULL	0
#endif

#ifndef VER_CODE
#define VER_CODE(major,minor,patch) (major*0x10000+minor*0x100+patch)
#endif
/* Software version */
#define SW_VER_ID	"LX_CPU1"
#define SW_VER_MAJOR	0
#define SW_VER_MINOR	1
#define SW_VER_PATCH	0
#define SW_VER_CODE	VER_CODE(SW_VER_MAJOR,SW_VER_MINOR,SW_VER_PATCH)
/* Hardware version */
#define HW_VER_ID	"LX_CPU1"
#define HW_VER_MAJOR	1
#define HW_VER_MINOR	0
#define HW_VER_PATCH	0
#define HW_VER_CODE	VER_CODE(HW_VER_MAJOR,HW_VER_MINOR,HW_VER_PATCH)
/* Version of mechanical  */
#define MECH_VER_ID     "LX_CPU1"
#define MECH_VER_MAJOR  0
#define MECH_VER_MINOR  0
#define MECH_VER_PATCH  0
#define MECH_VER_CODE	VER_CODE(MECH_VER_MAJOR,MECH_VER_MINOR,MECH_VER_PATCH)

/*
//-------- <<< Use Configuration Wizard in Context Menu >>> ------------------
*/
/*--------------------- Clock Configuration ----------------------------------
//
// <e> Clock Configuration
//   <h> System Controls and Status Register (SCS)
//     <o1.0>       EMC_SHIFT: EMC Shift enable
//                     <0=> Static CS addresses match bus width; AD[1] = 0 for 32 bit, AD[0] = 0 for 16+32 bit
//                     <1=> Static CS addresses start at LSB 0 regardless of memory width
//     <o1.1>       EMC_RESET: EMC Reset disable
//                     <0=> EMC will be reset by any chip reset
//                     <1=> Portions of EMC will only be reset by POR or BOR
//     <o1.2>       EMC_BURST: EMC Burst disable
//     <o1.3>       MCIPWR_LEVEL: SD card interface signal SD_PWR Active Level selection
//                     <0=> SD_PWR is active low
//                     <1=> SD_PWR is active high
//     <o1.4>       OSCRANGE: Main Oscillator Range Select
//                     <0=>  1 MHz to 20 MHz
//                     <1=> 15 MHz to 25 MHz
//     <o1.5>       OSCEN: Main Oscillator enable
//   </h>
//
//   <h> Clock Source Select Register (CLKSRCSEL)
//     <o2.0>       CLKSRC: sysclk and PLL0 clock source selection
//                     <0=> Internal RC oscillator
//                     <1=> Main oscillator
//   </h>
//
//   <e3> PLL0 Configuration (Main PLL)
//     <h> PLL0 Configuration Register (PLL0CFG)
//                     <i> PLL out clock = (F_cco / (2 * P))
//                     <i> F_cco = (F_in * M * 2 * P)
//                     <i> F_in  must be in the range of 1 MHz to 25 MHz
//                     <i> F_cco must be in the range of 9.75 MHz to 160 MHz
//       <o4.0..4>   MSEL: PLL Multiplier Selection
//                     <i> M Value
//                     <1-32><#-1>
//       <o4.5..6> PSEL: PLL Divider Selection
//                     <i> P Value
//                     <0=> 1
//                     <1=> 2
//                     <2=> 4
//                     <3=> 8
//     </h>
//   </e>
//
//   <e5> PLL1 Configuration (Alt PLL)
//     <h> PLL1 Configuration Register (PLL1CFG)
//                     <i> PLL out clock = (F_cco / (2 * P))
//                     <i> F_cco = (F_in * M * 2 * P)
//                     <i> F_in  must be in the range of 1 MHz to 25 MHz
//                     <i> F_cco must be in the range of 9.75 MHz to 160 MHz
//       <o6.0..4>   MSEL: PLL Multiplier Selection
//                     <i> M Value
//                     <1-32><#-1>
//       <o6.5..6> PSEL: PLL Divider Selection
//                     <i> P Value
//                     <0=> 1
//                     <1=> 2
//                     <2=> 4
//                     <3=> 8
//     </h>
//   </e>
//
//   <h> CPU Clock Selection Register (CCLKSEL)
//     <o7.0..4>    CCLKDIV: CPU clock (CCLK) divider
//                     <i> 0: The divider is turned off. No clock will be provided to the CPU
//                     <i> n: The input clock is divided by n to produce the CPU clock
//                     <0-31>
//     <o7.8>       CCLKSEL: CPU clock divider input clock selection
//                     <0=> sysclk clock
//                     <1=> PLL0 clock
//   </h>
//
//   <h> USB Clock Selection Register (USBCLKSEL)
//     <o8.0..4>    USBDIV: USB clock (source PLL0) divider selection
//                     <0=> USB clock off
//                     <4=> PLL0 / 4 (PLL0 must be 192Mhz)
//                     <6=> PLL0 / 6 (PLL0 must be 288Mhz)
//     <o8.8..9>    USBSEL: USB clock divider input clock selection
//                     <i> When CPU clock is selected, the USB can be accessed
//                     <i> by software but cannot perform USB functions
//                     <0=> CPU clock
//                     <1=> PLL0 clock
//                     <2=> PLL1 clock
//   </h>
//
//   <h> EMC Clock Selection Register (EMCCLKSEL)
//     <o9.0>       EMCDIV: EMC clock selection
//                     <0=> CPU clock
//                     <1=> CPU clock / 2
//   </h>
//
//   <h> Peripheral Clock Selection Register (PCLKSEL)
//     <o10.0..4>   PCLKDIV: APB Peripheral clock divider
//                     <i> 0: The divider is turned off. No clock will be provided to APB peripherals
//                     <i> n: The input clock is divided by n to produce the APB peripheral clock
//                     <0-31>
//   </h>
//
//   <h> Power Control for Peripherals Register (PCONP)
//     <o11.0>      PCLCD: LCD controller power/clock enable
//     <o11.1>      PCTIM0: Timer/Counter 0 power/clock enable
//     <o11.2>      PCTIM1: Timer/Counter 1 power/clock enable
//     <o11.3>      PCUART0: UART 0 power/clock enable
//     <o11.4>      PCUART1: UART 1 power/clock enable
//     <o11.5>      PCPWM0: PWM0 power/clock enable
//     <o11.6>      PCPWM1: PWM1 power/clock enable
//     <o11.7>      PCI2C0: I2C 0 interface power/clock enable
//     <o11.8>      PCUART4: UART 4 power/clock enable
//     <o11.9>      PCRTC: RTC and Event Recorder power/clock enable
//     <o11.10>     PCSSP1: SSP 1 interface power/clock enable
//     <o11.11>     PCEMC: External Memory Controller power/clock enable
//     <o11.12>     PCADC: A/D converter power/clock enable
//     <o11.13>     PCCAN1: CAN controller 1 power/clock enable
//     <o11.14>     PCCAN2: CAN controller 2 power/clock enable
//     <o11.15>     PCGPIO: IOCON, GPIO, and GPIO interrupts power/clock enable
//     <o11.17>     PCMCPWM: Motor Control PWM power/clock enable
//     <o11.18>     PCQEI: Quadrature encoder interface power/clock enable
//     <o11.19>     PCI2C1: I2C 1 interface power/clock enable
//     <o11.20>     PCSSP2: SSP 2 interface power/clock enable
//     <o11.21>     PCSSP0: SSP 0 interface power/clock enable
//     <o11.22>     PCTIM2: Timer 2 power/clock enable
//     <o11.23>     PCTIM3: Timer 3 power/clock enable
//     <o11.24>     PCUART2: UART 2 power/clock enable
//     <o11.25>     PCUART3: UART 3 power/clock enable
//     <o11.26>     PCI2C2: I2C 2 interface power/clock enable
//     <o11.27>     PCI2S: I2S interface power/clock enable
//     <o11.28>     PCSDC: SD Card interface power/clock enable
//     <o11.29>     PCGPDMA: GPDMA function power/clock enable
//     <o11.30>     PCENET: Ethernet block power/clock enable
//     <o11.31>     PCUSB: USB interface power/clock enable
//   </h>
//
//   <h> Clock Output Configuration Register (CLKOUTCFG)
//     <o12.0..3>   CLKOUTSEL: Clock Source for CLKOUT Selection
//                     <0=> CPU clock
//                     <1=> Main Oscillator
//                     <2=> Internal RC Oscillator
//                     <3=> USB clock
//                     <4=> RTC Oscillator
//                     <5=> unused
//                     <6=> Watchdog Oscillator
//     <o12.4..7>   CLKOUTDIV: Output Clock Divider
//                     <1-16><#-1>
//     <o12.8>      CLKOUT_EN: CLKOUT enable
//   </h>
//
// </e>
*/

#define CLOCK_SETUP           1

/* OSCEN 1, MCIPWRAL 1, EMCBC 1, EMCSC 1*/
#define SCS_Val               0x00000025

#define CLKSRCSEL_Val         0x00000001        /* XTAL */

#define PLL0_SETUP            1
#define PLL0CFG_Val           0x0000000B        /* 144000000Hz - must be in the range 9.75HMz-160MHz */

#define PLL1_SETUP            0
#define PLL1CFG_Val           0x00000023

#define CCLKSEL_Val           0x00000102        /* ppl0clk/(CCLKCFG_Val+1)=72000000Hz */

#define USBCLK_SETUP          1
#define USBCLKSEL_Val         0x00000103        /* divide ppl0clk by 6 to 48MHz */

#define EMCCLKSEL_Val         0x00000000        /* EMC same clock as CPU; max 80MHz */

#define PCLKSEL_Val           0x00000002        /*  Peripherals clocks are CCLK/1; max 100 MHz*/
#define PCONP_Val             0xF4E8FF9E
#define CLKOUTCFG_Val         0x00000100        /* Procide CPU clock on pin */

#define PCONP_CLK_DIV(x) ((x)==0?4:((x)==1?1:((x)==2?2:8)))

/*--------------------- Flash Accelerator Configuration ----------------------
//
// <e> Flash Accelerator Configuration
//   <o1.12..15> FLASHTIM: Flash Access Time
//               <0=> 1 CPU clock (for CPU clock up to 20 MHz)
//               <1=> 2 CPU clocks (for CPU clock up to 40 MHz)
//               <2=> 3 CPU clocks (for CPU clock up to 60 MHz)
//               <3=> 4 CPU clocks (for CPU clock up to 80 MHz)
//               <4=> 5 CPU clocks (for CPU clock up to 100 MHz)
//               <5=> 6 CPU clocks (for any CPU clock)
// </e>
*/
#define FLASH_SETUP           1
#define FLASHCFG_Val          0x00003000 /* for upto 80 MHz */
                              /* 0x00004000 for 100 MHz */

/*----------------------------------------------------------------------------
  Define clocks
 *----------------------------------------------------------------------------*/
#define XTAL        (12000000UL)        /* Oscillator frequency               */
#define OSC_CLK     (      XTAL)        /* Main oscillator frequency          */
#define RTC_CLK     (   32000UL)        /* RTC oscillator frequency           */
#define IRC_OSC     (12000000UL)        /* Internal RC oscillator frequency   */
#define WDT_OSC     (  500000UL)        /* Internal WDT oscillator frequency  */

#define SYS_TIMER_HZ 	    1000

#define SDRAM_SPEED_48      0
#define SDRAM_SPEED_50      1
#define SDRAM_SPEED_60      2
#define SDRAM_SPEED_72      3

#define SDRAM_SPEED         SDRAM_SPEED_72

#define SDRAM_SIZE          0x2000000
#define SDRAM_BASE          0xA0000000 /*CS0*/

#define SDRAM_SETUP         1

#define MPU_SETUP           1 /* MPU required to make SDRAM executable  */


#ifndef BIT
#define BIT(n)              (1 << ((n) & 0x1f))
#endif

// Port Pin Definitions & Macros:    Description - initial conditions
#define PO_0_GPIO_PIN       PORT_PIN(0,0,PORT_CONF_GPIO_IN_PU)
#define CAN1_RX_PIN         PORT_PIN(0,0,PORT_CONF_FNC_1|PORT_CONF_IN_PU)
#define TXD3_PIN            PORT_PIN(0,0,PORT_CONF_FNC_2|PORT_CONF_OUT_HI_NORM)
#define SDA1_PIN            PORT_PIN(0,0,PORT_CONF_FNC_3|PORT_CONF_IN_PU|PORT_CONF_OD_ON)

#define PO_1_GPIO_PIN       PORT_PIN(0,1,PORT_CONF_GPIO_IN_PU)
#define CAN1_TX_PIN         PORT_PIN(0,1,PORT_CONF_FNC_1|PORT_CONF_OUT_HI_NORM)
#define RXD3_PIN            PORT_PIN(0,1,PORT_CONF_FNC_2|PORT_CONF_IN_PU)
#define SCL1_PIN            PORT_PIN(0,1,PORT_CONF_FNC_3|PORT_CONF_IN_PU|PORT_CONF_OD_ON)

#define TXD0_PIN            PORT_PIN(0,2,PORT_CONF_FNC_1|PORT_CONF_OUT_HI_NORM)
#define RXD0_PIN            PORT_PIN(0,3,PORT_CONF_FNC_1|PORT_CONF_IN_PU)
#define CAN2_RX_PIN         PORT_PIN(0,4,PORT_CONF_FNC_2|PORT_CONF_IN_PU)
#define CAN2_TX_PIN         PORT_PIN(0,5,PORT_CONF_FNC_2|PORT_CONF_OUT_HI_NORM)

#define PO_6_GPIO_PIN       PORT_PIN(0,6,PORT_CONF_GPIO_IN_PU)
#define T2MAT0_PIN          PORT_PIN(0,6,PORT_CONF_FNC_3|PORT_CONF_OUT_HI_NORM)

#define PO_7_GPIO_PIN       PORT_PIN(0,7,PORT_CONF_GPIO_IN_PU)
#define T2MAT1_PIN          PORT_PIN(0,7,PORT_CONF_FNC_3|PORT_CONF_OUT_HI_NORM)

#define XC_PROGRAM_PIN      PORT_PIN(0,8,PORT_CONF_GPIO_OUT_HI)
#define XC_DONE_PIN         PORT_PIN(0,9,PORT_CONF_GPIO_IN_PU)
#define SDA2_PIN            PORT_PIN(0,10,PORT_CONF_FNC_2|PORT_CONF_IN_PU|PORT_CONF_OD_ON)
#define SCL2_PIN            PORT_PIN(0,11,PORT_CONF_FNC_2|PORT_CONF_IN_PU|PORT_CONF_OD_ON)
#define USB_PPWR2_PIN       PORT_PIN(0,12,PORT_CONF_FNC_1|PORT_CONF_OUT_HI_NORM)
#define PO_13_GPIO_PIN      PORT_PIN(0,13,PORT_CONF_GPIO_IN_PU)
#define ADC7_PIN            PORT_PIN(0,13,PORT_CONF_FNC_3|PORT_CONF_MODE_ADC|PORT_CONF_DIR_IN)
#define PO_14_GPIO_PIN      PORT_PIN(0,14,PORT_CONF_GPIO_IN_PU)
#define TXD1_PIN /*ULAN*/   PORT_PIN(0,15,PORT_CONF_FNC_1|PORT_CONF_OUT_HI_NORM)
#define RXD1_PIN /*ULAN*/   PORT_PIN(0,16,PORT_CONF_FNC_1|PORT_CONF_IN_PU)
#define CTS1_PIN /*ULAN*/   PORT_PIN(0,17,PORT_CONF_FNC_1|PORT_CONF_IN_PU)
#define XC_INIT_PIN         PORT_PIN(0,18,PORT_CONF_GPIO_IN_PU|PORT_CONF_OD_ON)
#define DSR1_PIN /*ULAN*/   PORT_PIN(0,19,PORT_CONF_FNC_1|PORT_CONF_IN_PU)
#define XC_SUSPEND_PIN      PORT_PIN(0,20,PORT_CONF_GPIO_OUT_HI)
#define PO_21_GPIO_PIN      PORT_PIN(0,21,PORT_CONF_GPIO_IN_PU)
#define RTS1_PIN /*ULAN*/   PORT_PIN(0,22,PORT_CONF_FNC_1|PORT_CONF_OUT_HI_NORM)
#define PO_23_GPIO_PIN      PORT_PIN(0,23,PORT_CONF_GPIO_IN_PU)
#define ADC0_PIN            PORT_PIN(0,23,PORT_CONF_FNC_1|PORT_CONF_MODE_ADC|PORT_CONF_DIR_IN)
#define PO_24_GPIO_PIN      PORT_PIN(0,24,PORT_CONF_GPIO_IN_PU)
#define ADC1_PIN            PORT_PIN(0,24,PORT_CONF_FNC_1|PORT_CONF_MODE_ADC|PORT_CONF_DIR_IN)
#define PO_25_GPIO_PIN      PORT_PIN(0,25,PORT_CONF_GPIO_IN_PU)
#define ADC2_PIN            PORT_PIN(0,25,PORT_CONF_FNC_1|PORT_CONF_MODE_ADC|PORT_CONF_DIR_IN)
#define PO_26_GPIO_PIN      PORT_PIN(0,26,PORT_CONF_GPIO_IN_PU)
#define ADC3_PIN            PORT_PIN(0,26,PORT_CONF_FNC_1|PORT_CONF_MODE_ADC|PORT_CONF_DIR_IN)
#define SDA0_PIN            PORT_PIN(0,27,PORT_CONF_FNC_1|PORT_CONF_IN_PU|PORT_CONF_OD_ON)
#define SCL0_PIN            PORT_PIN(0,28,PORT_CONF_FNC_1|PORT_CONF_IN_PU|PORT_CONF_OD_ON)
#define USB1_DPLUS_PIN      PORT_PIN(0,29,PORT_CONF_FNC_1)
#define USB1_DMINUS_PIN     PORT_PIN(0,30,PORT_CONF_FNC_1)
#define USB2_DPLUS_PIN      PORT_PIN(0,31,PORT_CONF_FNC_1)
/* USB2_DMINUS_PIN only */
#define ENET_TXD0_PIN       PORT_PIN(1,0,PORT_CONF_FNC_1|PORT_CONF_OUT_LO_NORM)
#define ENET_TXD1_PIN       PORT_PIN(1,1,PORT_CONF_FNC_1|PORT_CONF_OUT_LO_NORM)
#define SD_CLK_PIN          PORT_PIN(1,2,PORT_CONF_FNC_2|PORT_CONF_OUT_HI_NORM)
#define SD_CMD_PIN          PORT_PIN(1,3,PORT_CONF_FNC_2|PORT_CONF_OUT_HI_NORM)
#define ENET_TX_EN_PIN      PORT_PIN(1,4,PORT_CONF_FNC_1|PORT_CONF_OUT_LO_NORM)
#define SD_DETECT_PIN       PORT_PIN(1,5,PORT_CONF_GPIO_IN_PU)
#define SD_DATA0_PIN        PORT_PIN(1,6,PORT_CONF_FNC_2|PORT_CONF_IN_PU)
#define SD_DATA1_PIN        PORT_PIN(1,7,PORT_CONF_FNC_2|PORT_CONF_IN_PU)
#define ENET_CRS_PIN        PORT_PIN(1,8,PORT_CONF_FNC_1|PORT_CONF_IN_PU)
#define ENET_RXD0_PIN       PORT_PIN(1,9,PORT_CONF_FNC_1|PORT_CONF_IN_PU)
#define ENET_RXD1_PIN       PORT_PIN(1,10,PORT_CONF_FNC_1|PORT_CONF_IN_PU)
#define SD_DATA2_PIN        PORT_PIN(1,11,PORT_CONF_FNC_2|PORT_CONF_IN_PU)
#define SD_DATA3_PIN        PORT_PIN(1,12,PORT_CONF_FNC_2|PORT_CONF_IN_PU)
#define ENET_RX_DV_PIN      PORT_PIN(1,13,PORT_CONF_FNC_1|PORT_CONF_IN_PU)
#define ENET_RX_ER_PIN      PORT_PIN(1,14,PORT_CONF_FNC_1|PORT_CONF_IN_PU)
#define ENET_RX_CLK_PIN     PORT_PIN(1,15,PORT_CONF_FNC_1|PORT_CONF_IN_PU)
#define ENET_MDC_PIN        PORT_PIN(1,16,PORT_CONF_FNC_1|PORT_CONF_OUT_LO_NORM)
#define ENET_MDIO_PIN       PORT_PIN(1,17,PORT_CONF_FNC_1|PORT_CONF_IN_PU)
#define T1CAP0_PIN          PORT_PIN(1,18,PORT_CONF_FNC_3|PORT_CONF_IN_PU)
#define T1CAP1_PIN          PORT_PIN(1,19,PORT_CONF_FNC_3|PORT_CONF_IN_PU)

#define AUX_OUT1_PIN        PORT_PIN(1,20,PORT_CONF_GPIO_OUT_HI)
#define LCD_G0_PIN          PORT_PIN(1,20,PORT_CONF_FNC_7|PORT_CONF_OUT_LO_NORM)

#define LCD_R4_PIN          PORT_PIN(1,21,PORT_CONF_FNC_6|PORT_CONF_OUT_LO_NORM)

#define AUX_OUT3_PIN        PORT_PIN(1,22,PORT_CONF_GPIO_OUT_HI)
#define LCD_G2_PIN          PORT_PIN(1,22,PORT_CONF_FNC_7|PORT_CONF_OUT_LO_NORM)

#define LCD_G3_PIN          PORT_PIN(1,23,PORT_CONF_FNC_7|PORT_CONF_OUT_LO_NORM)
#define LCD_G4_PIN          PORT_PIN(1,24,PORT_CONF_FNC_7|PORT_CONF_OUT_LO_NORM)
#define LCD_G5_PIN          PORT_PIN(1,25,PORT_CONF_FNC_7|PORT_CONF_OUT_LO_NORM)

#define AUX_OUT4_PIN        PORT_PIN(1,26,PORT_CONF_GPIO_OUT_HI)
#define LCD_B1_PIN          PORT_PIN(1,26,PORT_CONF_FNC_7|PORT_CONF_OUT_LO_NORM)

#define CLKOUT_PIN          PORT_PIN(1,27,PORT_CONF_FNC_4|PORT_CONF_OUT_LO_NORM)
#define LCD_B2_PIN          PORT_PIN(1,27,PORT_CONF_FNC_7|PORT_CONF_OUT_LO_NORM)

#define LED2_PIN            PORT_PIN(1,28,PORT_CONF_GPIO_OUT_HI)
#define LCD_B3_PIN          PORT_PIN(1,28,PORT_CONF_FNC_7|PORT_CONF_OUT_LO_NORM)

#define LED1_PIN            PORT_PIN(1,29,PORT_CONF_GPIO_OUT_HI)
#define LCD_B4_PIN          PORT_PIN(1,29,PORT_CONF_FNC_7|PORT_CONF_OUT_LO_NORM)

#define P1_30_GPIO_PIN      PORT_PIN(1,30,PORT_CONF_GPIO_IN_PU)
#define USB2_OVRCR_PIN      PORT_PIN(1,31,PORT_CONF_FNC_1|PORT_CONF_IN_PU)

#define P2_0_GPIO_PIN       PORT_PIN(2,0,PORT_CONF_GPIO_IN_PD)
#define LCD_PWR_PIN         PORT_PIN(2,0,PORT_CONF_FNC_7|PORT_CONF_OUT_LO_NORM)
#define ENET_PWRDOWN_PIN    PORT_PIN(2,0,PORT_CONF_OUT_LO_NORM)

#define P2_1_GPIO_PIN       PORT_PIN(2,1,PORT_CONF_GPIO_IN_PU)
#define LCD_LE_PIN          PORT_PIN(2,1,PORT_CONF_FNC_7|PORT_CONF_OUT_LO_NORM)

#define P2_2_GPIO_PIN       PORT_PIN(2,2,PORT_CONF_GPIO_IN_PU)
#define LCD_DCLK_PIN        PORT_PIN(2,2,PORT_CONF_FNC_7|PORT_CONF_OUT_LO_NORM)

#define LCD_FP_PIN          PORT_PIN(2,3,PORT_CONF_FNC_7|PORT_CONF_OUT_LO_NORM)
#define LCD_EN_PIN          PORT_PIN(2,4,PORT_CONF_FNC_7|PORT_CONF_OUT_LO_NORM)
#define LCD_LP_PIN          PORT_PIN(2,5,PORT_CONF_FNC_7|PORT_CONF_OUT_LO_NORM)
#define LCD_R1_PIN          PORT_PIN(2,6,PORT_CONF_FNC_7|PORT_CONF_OUT_LO_NORM)
#define LCD_R2_PIN          PORT_PIN(2,7,PORT_CONF_FNC_7|PORT_CONF_OUT_LO_NORM)
#define LCD_R3_PIN          PORT_PIN(2,8,PORT_CONF_FNC_7|PORT_CONF_OUT_LO_NORM)
#define USB1_CONNECT_PIN    PORT_PIN(2,9,PORT_CONF_FNC_1|PORT_CONF_OUT_HI_NORM)
#define USB1_PWR_SW_PIN     PORT_PIN(2,10,PORT_CONF_GPIO_OUT_HI)
#define EINT1_PIN /*ENET*/  PORT_PIN(2,11,PORT_CONF_FNC_1|PORT_CONF_IN_PU)
#define LCD_R0_PIN          PORT_PIN(2,12,PORT_CONF_FNC_5|PORT_CONF_OUT_LO_NORM)

#define AUX_IN1_PIN         PORT_PIN(2,13,PORT_CONF_GPIO_IN)
#define LCD_B0_PIN          PORT_PIN(2,13,PORT_CONF_FNC_7|PORT_CONF_OUT_LO_NORM)

#define T2CAP0_PIN /*ADC*/  PORT_PIN(2,14,PORT_CONF_FNC_3|PORT_CONF_IN_PU)
#define T2CAP1_PIN /*ADC*/  PORT_PIN(2,15,PORT_CONF_FNC_3|PORT_CONF_IN_PU)
#define EMC_CAS_PIN         PORT_PIN(2,16,PORT_CONF_FNC_1|PORT_CONF_OUT_HI_NORM)
#define EMC_RAS_PIN         PORT_PIN(2,17,PORT_CONF_FNC_1|PORT_CONF_OUT_HI_NORM)
#define EMC_CLK_PIN         PORT_PIN(2,18,PORT_CONF_FNC_1|PORT_CONF_OUT_HI_NORM)
#define SSP0_CS1_PIN        PORT_PIN(2,19,PORT_CONF_GPIO_OUT_HI)
#define EMC_DICS0_PIN       PORT_PIN(2,20,PORT_CONF_FNC_1|PORT_CONF_OUT_HI_NORM)
#define SSP0_CS2_PIN        PORT_PIN(2,21,PORT_CONF_GPIO_OUT_HI)
#define SCK0_PIN            PORT_PIN(2,22,PORT_CONF_FNC_2|PORT_CONF_OUT_LO_NORM)
#define SSP0_CS0_PIN        PORT_PIN(2,23,PORT_CONF_GPIO_OUT_HI) /* SSEL FNC_2 */
#define EMC_CKE_PIN         PORT_PIN(2,24,PORT_CONF_FNC_1|PORT_CONF_OUT_LO_NORM)
#define SSP0_CS3_PIN        PORT_PIN(2,25,PORT_CONF_GPIO_OUT_HI) /* SPI FLASH */
#define MISO0_PIN           PORT_PIN(2,26,PORT_CONF_FNC_2|PORT_CONF_IN_PU)
#define MOSI0_PIN           PORT_PIN(2,27,PORT_CONF_FNC_2|PORT_CONF_OUT_LO_NORM)
#define EMC_DQM0_PIN        PORT_PIN(2,28,PORT_CONF_FNC_1|PORT_CONF_OUT_HI_NORM)
#define EMC_DQM1_PIN        PORT_PIN(2,29,PORT_CONF_FNC_1|PORT_CONF_OUT_HI_NORM)
#define EMC_DQM2_PIN        PORT_PIN(2,30,PORT_CONF_FNC_1|PORT_CONF_OUT_HI_NORM)
#define EMC_DQM3_PIN        PORT_PIN(2,31,PORT_CONF_FNC_1|PORT_CONF_OUT_HI_NORM)

#define EMC_D0_PIN          PORT_PIN(3,0,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_D1_PIN          PORT_PIN(3,1,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_D2_PIN          PORT_PIN(3,2,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_D3_PIN          PORT_PIN(3,3,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_D4_PIN          PORT_PIN(3,4,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_D5_PIN          PORT_PIN(3,5,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_D6_PIN          PORT_PIN(3,6,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_D7_PIN          PORT_PIN(3,7,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_D8_PIN          PORT_PIN(3,8,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_D9_PIN          PORT_PIN(3,9,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_D10_PIN         PORT_PIN(3,10,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_D11_PIN         PORT_PIN(3,11,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_D12_PIN         PORT_PIN(3,12,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_D13_PIN         PORT_PIN(3,13,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_D14_PIN         PORT_PIN(3,14,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_D15_PIN         PORT_PIN(3,15,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_D16_PIN         PORT_PIN(3,16,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_D17_PIN         PORT_PIN(3,17,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_D18_PIN         PORT_PIN(3,18,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_D19_PIN         PORT_PIN(3,19,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_D20_PIN         PORT_PIN(3,20,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_D21_PIN         PORT_PIN(3,21,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_D22_PIN         PORT_PIN(3,22,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_D23_PIN         PORT_PIN(3,23,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_D24_PIN         PORT_PIN(3,24,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_D25_PIN         PORT_PIN(3,25,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_D26_PIN         PORT_PIN(3,26,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_D27_PIN         PORT_PIN(3,27,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_D28_PIN         PORT_PIN(3,28,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_D29_PIN         PORT_PIN(3,29,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_D30_PIN         PORT_PIN(3,30,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_D31_PIN         PORT_PIN(3,31,PORT_CONF_FNC_1|PORT_CONF_IN_REP)

#define EMC_A0_PIN          PORT_PIN(4,0,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_A1_PIN          PORT_PIN(4,1,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_A2_PIN          PORT_PIN(4,2,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_A3_PIN          PORT_PIN(4,3,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_A4_PIN          PORT_PIN(4,4,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_A5_PIN          PORT_PIN(4,5,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_A6_PIN          PORT_PIN(4,6,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_A7_PIN          PORT_PIN(4,7,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_A8_PIN          PORT_PIN(4,8,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_A9_PIN          PORT_PIN(4,9,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_A10_PIN         PORT_PIN(4,10,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_A11_PIN         PORT_PIN(4,11,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_A12_PIN         PORT_PIN(4,12,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_A13_PIN         PORT_PIN(4,13,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_A14_PIN         PORT_PIN(4,14,PORT_CONF_FNC_1|PORT_CONF_IN_REP)
#define EMC_A15_PIN         PORT_PIN(4,15,PORT_CONF_FNC_1|PORT_CONF_IN_REP)

#define XC_RDWR_PIN         PORT_PIN(4,16,PORT_CONF_GPIO_OUT_LO)
#define USB1_ID_PIN         PORT_PIN(4,17,PORT_CONF_GPIO_IN_PU)
#define SSP1_CS1_PIN        PORT_PIN(4,18,PORT_CONF_GPIO_OUT_HI)
#define SSP1_CS2_PIN        PORT_PIN(4,19,PORT_CONF_GPIO_OUT_HI)
#define SCK1_PIN            PORT_PIN(4,20,PORT_CONF_FNC_3|PORT_CONF_OUT_LO_NORM)
#define SSP1_CS0_PIN        PORT_PIN(4,21,PORT_CONF_GPIO_OUT_HI) /* SSEL FNC_3 */
#define MISO1_PIN           PORT_PIN(4,22,PORT_CONF_FNC_3|PORT_CONF_IN_PU)
#define MOSI1_PIN           PORT_PIN(4,23,PORT_CONF_FNC_3|PORT_CONF_OUT_LO_NORM)
#define EMC_OE_PIN          PORT_PIN(4,24,PORT_CONF_FNC_1|PORT_CONF_OUT_HI_NORM)
#define EMC_WE_PIN          PORT_PIN(4,25,PORT_CONF_FNC_1|PORT_CONF_OUT_HI_NORM)
#define EMC_BLS0_PIN        PORT_PIN(4,26,PORT_CONF_FNC_1|PORT_CONF_OUT_HI_NORM) /* XC PRG CCLK */
#define EMC_BLS1_PIN        PORT_PIN(4,27,PORT_CONF_FNC_1|PORT_CONF_OUT_HI_NORM)
#define EMC_BLS2_PIN        PORT_PIN(4,28,PORT_CONF_FNC_1|PORT_CONF_OUT_HI_NORM)

#define EMC_BLS3_PIN        PORT_PIN(4,29,PORT_CONF_FNC_1|PORT_CONF_OUT_HI_NORM)
#define LCD_G1_PIN          PORT_PIN(4,29,PORT_CONF_FNC_6|PORT_CONF_OUT_LO_NORM)

#define EMC_CS0_PIN         PORT_PIN(4,30,PORT_CONF_FNC_1|PORT_CONF_OUT_HI_NORM) /* XC */
#define EMC_CS1_PIN         PORT_PIN(4,31,PORT_CONF_FNC_1|PORT_CONF_OUT_HI_NORM) /* XC/LED3 */

#define T2MAT2_PIN /*ADC*/  PORT_PIN(5,0,PORT_CONF_FNC_3|PORT_CONF_OUT_LO_NORM)
#define T2MAT3_PIN /*ADC*/  PORT_PIN(5,1,PORT_CONF_FNC_3|PORT_CONF_OUT_LO_NORM)
#define P5_2_GPIO_PIN       PORT_PIN(5,2,PORT_CONF_GPIO_IN_PU)

#define P5_3_GPIO_PIN       PORT_PIN(5,3,PORT_CONF_GPIO_IN_PU)
#define AUX_IN2_PIN         PORT_PIN(5,3,PORT_CONF_GPIO_IN)
#define RXD4_PIN            PORT_PIN(5,3,PORT_CONF_FNC_4|PORT_CONF_IN_PU)

#define P5_4_GPIO_PIN       PORT_PIN(5,4,PORT_CONF_GPIO_IN_PU)
#define AUX_OUT2_PIN        PORT_PIN(5,4,PORT_CONF_GPIO_OUT_HI)
#define TXD4_PIN            PORT_PIN(5,4,PORT_CONF_FNC_4|PORT_CONF_OUT_HI_NORM)

#define INITIAL_PIN_SETUP_LIST \
	PO_0_GPIO_PIN, \
	PO_1_GPIO_PIN, \
	TXD0_PIN, \
	RXD0_PIN, \
	CAN2_RX_PIN, \
	CAN2_TX_PIN, \
	PO_6_GPIO_PIN, \
	PO_7_GPIO_PIN, \
	XC_PROGRAM_PIN, \
	XC_DONE_PIN, \
	SDA2_PIN, \
	SCL2_PIN, \
	USB_PPWR2_PIN, \
	PO_13_GPIO_PIN, \
	PO_14_GPIO_PIN, \
	TXD1_PIN, \
	RXD1_PIN, \
	CTS1_PIN, \
	XC_INIT_PIN, \
	DSR1_PIN, \
	XC_SUSPEND_PIN, \
	PO_21_GPIO_PIN, \
	RTS1_PIN, \
	PO_23_GPIO_PIN, \
	PO_24_GPIO_PIN, \
	PO_25_GPIO_PIN, \
	PO_26_GPIO_PIN, \
	SDA0_PIN, \
	SCL0_PIN, \
	USB1_DPLUS_PIN, \
	USB1_DMINUS_PIN, \
	USB2_DPLUS_PIN, \
	ENET_TXD0_PIN, \
	ENET_TXD1_PIN, \
	SD_CLK_PIN, \
	SD_CMD_PIN, \
	ENET_TX_EN_PIN, \
	SD_DETECT_PIN, \
	SD_DATA0_PIN, \
	SD_DATA1_PIN, \
	ENET_CRS_PIN, \
	ENET_RXD0_PIN, \
	ENET_RXD1_PIN, \
	SD_DATA2_PIN, \
	SD_DATA3_PIN, \
	ENET_RX_DV_PIN, \
	ENET_RX_ER_PIN, \
	ENET_RX_CLK_PIN, \
	ENET_MDC_PIN, \
	ENET_MDIO_PIN, \
	T1CAP0_PIN, \
	T1CAP1_PIN, \
	AUX_OUT1_PIN, \
	LCD_R4_PIN, \
	AUX_OUT3_PIN, \
	LCD_G3_PIN, \
	LCD_G4_PIN, \
	LCD_G5_PIN, \
	AUX_OUT4_PIN, \
	CLKOUT_PIN, \
	LED2_PIN, \
	LED1_PIN, \
	P1_30_GPIO_PIN, \
	USB2_OVRCR_PIN, \
	P2_0_GPIO_PIN, \
	P2_1_GPIO_PIN, \
	P2_2_GPIO_PIN, \
	LCD_FP_PIN, \
	LCD_EN_PIN, \
	LCD_LP_PIN, \
	LCD_R1_PIN, \
	LCD_R2_PIN, \
	LCD_R3_PIN, \
	USB1_CONNECT_PIN, \
	USB1_PWR_SW_PIN, \
	EINT1_PIN, \
	LCD_R0_PIN, \
	AUX_IN1_PIN, \
	T2CAP0_PIN, \
	T2CAP1_PIN, \
	EMC_CAS_PIN, \
	EMC_RAS_PIN, \
	EMC_CLK_PIN, \
	SSP0_CS1_PIN, \
	EMC_DICS0_PIN, \
	SSP0_CS2_PIN, \
	SCK0_PIN, \
	SSP0_CS0_PIN, \
	EMC_CKE_PIN, \
	SSP0_CS3_PIN, \
	MISO0_PIN, \
	MOSI0_PIN, \
	EMC_DQM0_PIN, \
	EMC_DQM1_PIN, \
	EMC_DQM2_PIN, \
	EMC_DQM3_PIN, \
	EMC_D0_PIN, \
	EMC_D1_PIN, \
	EMC_D2_PIN, \
	EMC_D3_PIN, \
	EMC_D4_PIN, \
	EMC_D5_PIN, \
	EMC_D6_PIN, \
	EMC_D7_PIN, \
	EMC_D8_PIN, \
	EMC_D9_PIN, \
	EMC_D10_PIN, \
	EMC_D11_PIN, \
	EMC_D12_PIN, \
	EMC_D13_PIN, \
	EMC_D14_PIN, \
	EMC_D15_PIN, \
	EMC_D16_PIN, \
	EMC_D17_PIN, \
	EMC_D18_PIN, \
	EMC_D19_PIN, \
	EMC_D20_PIN, \
	EMC_D21_PIN, \
	EMC_D22_PIN, \
	EMC_D23_PIN, \
	EMC_D24_PIN, \
	EMC_D25_PIN, \
	EMC_D26_PIN, \
	EMC_D27_PIN, \
	EMC_D28_PIN, \
	EMC_D29_PIN, \
	EMC_D30_PIN, \
	EMC_D31_PIN, \
	EMC_A0_PIN, \
	EMC_A1_PIN, \
	EMC_A2_PIN, \
	EMC_A3_PIN, \
	EMC_A4_PIN, \
	EMC_A5_PIN, \
	EMC_A6_PIN, \
	EMC_A7_PIN, \
	EMC_A8_PIN, \
	EMC_A9_PIN, \
	EMC_A10_PIN, \
	EMC_A11_PIN, \
	EMC_A12_PIN, \
	EMC_A13_PIN, \
	EMC_A14_PIN, \
	EMC_A15_PIN, \
	XC_RDWR_PIN, \
	USB1_ID_PIN, \
	SSP1_CS1_PIN, \
	SSP1_CS2_PIN, \
	SCK1_PIN, \
	SSP1_CS0_PIN, \
	MISO1_PIN, \
	MOSI1_PIN, \
	EMC_OE_PIN, \
	EMC_WE_PIN, \
	EMC_BLS0_PIN, \
	EMC_BLS1_PIN, \
	EMC_BLS2_PIN, \
	EMC_BLS3_PIN, \
	EMC_CS0_PIN, \
	EMC_CS1_PIN, \
	T2MAT2_PIN, \
	T2MAT3_PIN, \
	P5_2_GPIO_PIN, \
	P5_3_GPIO_PIN, \
	P5_4_GPIO_PIN, \


/***************************************************************************/
/* io functions */

#define LED1_BIT                BIT(LED1_PIN)
#define LED2_BIT                BIT(LED2_PIN)

#define LED_GP			BIT(LED2_PIN)  /* GENREAL PURPOSE LED */
#define LED_ERR			BIT(LED1_PIN)

/***************************************************************************/
/* io functions */
#define IN_PORT			LPC_GPIO0
#define OUT_PORT		LPC_GPIO1
#define LED_PORT		LPC_GPIO1 /*FIXME*/


#define CREATE_PORT_NAME_PIN(port) port->PIN
#define CREATE_PORT_NAME_CLR(port) port->CLR
#define CREATE_PORT_NAME_SET(port) port->SET

#define GET_IN_PIN(port,in)	((CREATE_PORT_NAME_PIN(port) & in)?1:0)	
#define GET_IN_PORT(port)       (CREATE_PORT_NAME_PIN(port))
#define SET_OUT_PIN(port,out)   (CREATE_PORT_NAME_SET(port)=out)
#define CLR_OUT_PIN(port,out)   (CREATE_PORT_NAME_CLR(port)=out)

/***************************************************************************/
/* watchdog */
//#define WATCHDOG_ENABLED
#define WATCHDOG_TIMEOUT_MS     1000

/***************************************************************************/
/* uLan configuration */

#ifdef UL_LOG_ENABLE
  #undef UL_LOG_ENABLE
#endif

#ifdef ULD_DEFAULT_BUFFER_SIZE
  #undef ULD_DEFAULT_BUFFER_SIZE
  #define ULD_DEFAULT_BUFFER_SIZE 0x2000
#endif

#define UL_DRV_SYSLESS_PORT LPC_UART1_BASE
#define UL_DRV_SYSLESS_BAUD 19200
#define UL_DRV_SYSLESS_IRQ UART1_IRQn
#define UL_DRV_SYSLESS_MY_ADR_DEFAULT 1

#define watchdog_feed lpc_watchdog_feed
#define kvpb_erase lpcisp_kvpb_erase
#define kvpb_copy lpcisp_kvpb_copy
#define kvpb_flush lpcisp_kvpb_flush
#define KVPB_DEFAULT_FLAGS KVPB_DESC_DOUBLE|KVPB_DESC_CHUNKWO

#define ULBOOT_APPSTART_DELAY_MS() \
  (((LPC_SC->RSID ^ 1) & 0x5) || \
   (~LPC_GPIO0->PIN & (1 << (RXD0_PIN & 0x1f)))? \
    5000: 2000)

/***************************************************************************/
/* USB configuration */
#define USB_WITH_UDEV_FNC
#define USB_EP_NUM          32
#define USB_MAX_PACKET0     64
#define USB_MAX_PACKET      8
#define USB_DMA_EP          0x00000000

#define USB_VBUS_PIN_USED   0

/***************************************************************************/
/* I2C1 configuration */
#define I2C_DRV_SYSLESS_IRQ I2C0_IRQn
#define I2C_DRV_SYSLESS_PORT LPC_I2C0_BASE
#define I2C_DRV_SYSLESS_BITRATE 10000
#define I2C_DRV_SYSLESS_SLADR 0

/***************************************************************************/
/* SPI and SPI display configuration */
#define DISPLAY_SPI_NUMBER 1
#define KBD_SPI_NUMBER     1

#endif /* _SYSTEM_DEF_H_ */
