#include "local_config.h"
#include <system_def.h>
#include <cpu_def.h>
#include <lt_timer_types.h>
#include <bspbase.h>
#include <hal_machperiph.h>
#include <hal_gpio.h>
#ifdef MPU_SETUP
  #include <hal_mpu.h>
#endif
#ifdef CONFIG_KEYVAL
  #include <keyvalpb.h>
  #include <lpciap.h>
  #include <lpciap_kvpb.h>
#endif /* CONFIG_KEYVAL */
#ifdef CONFIG_STDIO_COM_PORT
  #include <uart.h>
#endif 
#ifdef CONFIG_OC_UL_DRV_SYSLESS
  #include <ul_lib/ulan.h>
  #include <string.h>
  #include <ul_drv_init.h>
  #include <ul_drv_iac.h>
  #include <ul_lib/ul_drvdef.h>
  extern long int uld_jiffies;
#endif /* CONFIG_OC_UL_DRV_SYSLESS */
#ifdef CONFIG_OC_I2C_DRV_SYSLESS
  #include <i2c_drv.h>
#endif /* CONFIG_OC_I2C_DRV_SYSLESS */


/* timers */
volatile lt_ticks_t sys_timer_ticks;
void (*timer0_isr_appl_call)(void);

#ifdef CONFIG_OC_I2C_DRV_SYSLESS
#define I2C_DRV_NA_MSTIMEOUT    10
i2c_drv_t i2c_drv;
int i2c_drv_na_timer=0;
#endif /* CONFIG_OC_I2C_DRV_SYSLESS */

static const unsigned initial_pin_setup[] = {
  INITIAL_PIN_SETUP_LIST
};

static void sysInit(void) 
{
  int i;

  //lpc_pll_off();
  //lpc_pll_on();
  system_clock_init();

  for (i = 0; i < sizeof(initial_pin_setup) / sizeof(initial_pin_setup[0]); i++) {
    hal_pin_conf(initial_pin_setup[i]);
  }
}

IRQ_HANDLER_FNC(timer0_isr)
{
  unsigned int ir;

  ir=LPC_TIM0->IR;
  if (ir&0x01) {
    do {
      if(timer0_isr_appl_call!=NULL)
        timer0_isr_appl_call();
      LPC_TIM0->MR0 += PCLK / SYS_TIMER_HZ;
      LPC_TIM0->IR=0x01;               // Clear match0 interrupt
     #ifdef CONFIG_OC_UL_DRV_SYSLESS
      uld_jiffies++;
     #endif
     #ifdef CONFIG_OC_I2C_DRV_SYSLESS
      if (i2c_drv.flags&I2C_DRV_MS_INPR) {
        if (i2c_drv.flags&I2C_DRV_NA) {
          i2c_drv_na_timer++;
          if (i2c_drv_na_timer>I2C_DRV_NA_MSTIMEOUT) {
             if (i2c_drv.stroke_fnc)
              i2c_drv.stroke_fnc(&i2c_drv);
            i2c_drv_na_timer=0;
          }
        } else {
          i2c_drv_na_timer=0;
        }
        i2c_drv.flags|=I2C_DRV_NA;
      }
     #endif
      sys_timer_ticks++;
    } while (((int32_t)(LPC_TIM0->MR0-LPC_TIM0->TC))<0);
  }
  return IRQ_HANDLED;
}

void sys_timer_usleep(unsigned long usec)
{
  unsigned long wait_till;

  wait_till = (unsigned long long)usec * PCLK / 1000000;
  wait_till += LPC_TIM0->TC;
  while ((long)(LPC_TIM0->TC - wait_till) < 0);
}

void timerInit(void)
{
  sys_timer_ticks=0;

  request_irq(TIMER0_IRQn, timer0_isr, 0, NULL,NULL);
  enable_irq(TIMER0_IRQn);

  LPC_TIM0->TC=0;
  LPC_TIM0->MCR=0;

  LPC_TIM0->MR0= PCLK / SYS_TIMER_HZ;
  LPC_TIM0->MCR|=1;				  /* TMCR_MR0_I; */

  LPC_TIM0->TCR = 1; 			  /* Run timer 0*/
}

#ifdef MPU_SETUP

const struct {uint32_t addr; uint32_t size; uint32_t attrib; }
  mpu_bsp_regions[] = {
  {0x00000000, 0x00080000, HAL_MPU_ATTR_STD_FLASH}, /* 512 kB FLASH */
  {0x10000000, 0x00010000, HAL_MPU_ATTR_STD_RWX}, /* 64 kB RAM */
  {0x20000000, 0x00008000, HAL_MPU_ATTR_STD_RWX}, /* 32 kB RAM */
 #ifdef SDRAM_BASE
  {SDRAM_BASE, SDRAM_SIZE, HAL_MPU_ATTR_STD_RWX}, /* 32 MB SDRAM */
 #endif /*SDRAM_BASE*/
  {0x20080000, 0x00020000, HAL_MPU_ATTR_STD_IO}, /* 128 kB AHB peripherals */
  {0x40000000, 0x00100000, HAL_MPU_ATTR_STD_IO}, /* 1 MB ABP0 and ABP1 peripherals */
  {0x80000000, 0x10000000, HAL_MPU_ATTR_STD_IO}, /* 256 MB CS0 and CS1 */
  /*{0x1FFE0000, 0x00020000, HAL_MPU_ATTR_STD_FLASH},*/  /* 128kB for IAP ROM */
};

void mpu_bsp_init(void)
{
  int i;
  if (!(MPU->CTRL & MPU_CTRL_ENABLE_Msk))
    hal_mpu_clear();
  for (i = 0; i < sizeof(mpu_bsp_regions)/sizeof(mpu_bsp_regions[0]); i++) {
    hal_mpu_cover_region(mpu_bsp_regions[i].addr, mpu_bsp_regions[i].size, mpu_bsp_regions[i].attrib);
  }
  MPU->CTRL = MPU_CTRL_ENABLE_Msk | MPU_CTRL_HFNMIENA_Msk | MPU_CTRL_PRIVDEFENA_Msk;
}

#endif /* MPU_SETUP */

#ifdef CONFIG_STDIO_COM_PORT

int uartcon_write(int file, const char * ptr, int len)
{
  int cnt;
  unsigned char ch;
  for(cnt=0;cnt<len;cnt++,ptr++){
    ch=*ptr;
    if(ch==0xa)
      uart0Putch(0xd);
    uart0Putch(ch);
  }
  return cnt;
}

void init_system_stub(void) {
  system_stub_ops.write=uartcon_write;
}

#endif /* CONFIG_STDIO_COM_PORT */

#ifdef CONFIG_OC_UL_DRV_SYSLESS

extern unsigned uld_debug_flg; /* Left application  set defaults */

#ifndef CONFIG_KEYVAL
unsigned long lpciap_buff[ISP_RAM2FLASH_BLOCK_SIZE/4];
#endif /* CONFIG_KEYVAL */

#define UL_MTYPE_START32BIT 0x100

static inline int ul_iac_mem_head_rd(uint8_t *buf, int len,
                      uint32_t* pmtype, uint32_t* pstart, uint32_t* plen)
{
  uint32_t val, mtype;
  if (len<6) return -1;
  mtype=*(buf++);       /* memory type */
  mtype+=*(buf++)<<8;
  val=*(buf++);           /* start address */
  val+=*(buf++)<<8;
  if(mtype&UL_MTYPE_START32BIT){
    if (len<8) return -1;
    val+=(uint32_t)*(buf++)<<16;
    val+=(uint32_t)*(buf++)<<24;
  }
  *pstart=val;
  val=*(buf++);           /* length */
  val+=*(buf++)<<8;
  if(mtype&UL_MTYPE_START32BIT){
    if (len==10) {
      val+=(uint32_t)*(buf++)<<16;
      val+=(uint32_t)*(buf++)<<24;
    }
  }
  *plen=val;
  mtype&=~UL_MTYPE_START32BIT;       /* 32-bit start address */
  *pmtype=mtype;
  return 0;
}

int ul_iac_call_rdm(struct ul_drv *udrv,ul_msginfo *msginfo,char *ibuff,ul_iac_data *data)
{
  uint32_t mtype,start,len;

  data->len=0;

  if(ul_iac_mem_head_rd((uint8_t *)ibuff, msginfo->len,&mtype,&start,&len))
    return UL_IAC_RC_PROC;

  if (mtype==0x00) {
    data->len=len;
    data->buff=(char*)start;
    return UL_IAC_RC_FREEMSG;
  }
  return UL_IAC_RC_PROC;
}

int ul_iac_call_erm(struct ul_drv *udrv,ul_msginfo *msginfo,char *ibuff,ul_iac_data *data)
{
  uint32_t mtype,start,len;

  data->len=0;

  if(ul_iac_mem_head_rd((uint8_t *)ibuff, msginfo->len,&mtype,&start,&len))
    return UL_IAC_RC_PROC;

 #ifdef CONFIG_KEYVAL
  if (mtype==0x01) {
    lpcisp_erase((void*)start,len);
    data->len=0;
    return UL_IAC_RC_FREEMSG;
  }
 #endif /* CONFIG_KEYVAL */
  return UL_IAC_RC_PROC;
}

int ul_iac_call_wrm(struct ul_drv *udrv,ul_msginfo *msginfo,char *ibuff,ul_iac_data *data)
{
  uint32_t mtype,start,len;

  data->len=0;

  if(ul_iac_mem_head_rd((uint8_t *)ibuff, msginfo->len,&mtype,&start,&len))
    return UL_IAC_RC_PROC;

  if (mtype==0x00) {
    memcpy((void*)start,data->buff,data->len);
    return UL_IAC_RC_FREEMSG;
  }
 #ifdef CONFIG_KEYVAL
  if (mtype==0x01) {
    lpcisp_write((char*)start, data->buff, ISP_RAM2FLASH_BLOCK_SIZE);
    return UL_IAC_RC_FREEMSG;
  }
 #endif /* CONFIG_KEYVAL */
  return UL_IAC_RC_PROC;
}


int ul_iac_call_deb(struct ul_drv *udrv,ul_msginfo *msginfo,char *ibuff,ul_iac_data *data)
{
  uint32_t debcmd,mtype,start;
  uint8_t *p=(uint8_t*)ibuff;

  if (msginfo->len<1) return UL_IAC_RC_PROC;
  debcmd=*(p++);
  switch (debcmd) {
    case 0x10: /* goto */
      data->len=0;
      if (msginfo->len<5) return UL_IAC_RC_PROC;
      mtype=*(p++);
      mtype+=*(p++)<<8;
      start=*(p++);
      start+=*(p++)<<8;
      if(mtype&UL_MTYPE_START32BIT){
        mtype&=~UL_MTYPE_START32BIT;
        if (msginfo->len<7) return UL_IAC_RC_PROC;
        start+=(uint32_t)*(p++)<<16;
        start+=(uint32_t)*(p++)<<24;
      }
      if (mtype==0x00)
        ((void (*)())start)();
    default:break;
  }
  return UL_IAC_RC_PROC;
}

int ul_iac_call_res(struct ul_drv *udrv,ul_msginfo *msginfo,char *ibuff,ul_iac_data *data)
{
  uint32_t rescmd,pass;
  uint8_t *p=(uint8_t*)ibuff;

  if (msginfo->len<1) return UL_IAC_RC_PROC;
  rescmd=*(p++);
  switch (rescmd) {
    case ULRES_CPU: /* CPU */
      data->len=0;
      if (msginfo->len<3) return UL_IAC_RC_PROC;
      pass=*(p++);
      pass+=*(p++)<<8;
      if (pass==0xaa55) {
//        MEMMAP=MEMMAP_FLASH;
        lpc_watchdog_init(1,10); /* 10ms */
        lpc_watchdog_feed();
        while(1);
      }
    default:break;
  }
  return UL_IAC_RC_PROC;
}

int uLanInit()
{
  struct ul_drv *udrv;

  /* set rs485 mode for UART1 */
 #ifdef TXD1_PIN
  hal_pin_conf(TXD1_PIN);
 #else
  #error TXD1_PIN is not set or valid
 #endif

 #ifdef RXD1_PIN
  hal_pin_conf(RXD1_PIN);
 #else
  #error RXD1_PIN is not set or valid
 #endif

 #ifdef CTS1_PIN
  hal_pin_conf(CTS1_PIN);
 #else
  #error CTS1_PIN is not set or valid
 #endif

 #ifdef DSR1_PIN
  hal_pin_conf(DSR1_PIN);
 #else
  #error DSR1_PIN is not set or valid
 #endif

 #ifdef RTS1_PIN
  hal_pin_conf(RTS1_PIN);
 #else
  #error RTS1_PIN is not set or valid
 #endif

  udrv=ul_drv_new(UL_DRV_SYSLESS_PORT,	    /* port */
             UL_DRV_SYSLESS_IRQ,            /* irq */
	     UL_DRV_SYSLESS_BAUD,	    /* baud */
	     UL_DRV_SYSLESS_MY_ADR_DEFAULT, /* my adr */
	#ifdef CONFIG_OC_UL_DRV_U450_VARPINS
	  #if defined(CONFIG_OC_UL_DRV_U450_VARPINS_DIRNEG) && defined(CONFIG_OC_UL_DRV_U450_VARPINS_MSRSWAP)
	     "16450-dirneg-msrswap",	    /* chip name */
	  #elif defined(CONFIG_OC_UL_DRV_U450_VARPINS_MSRSWAP)
	     "16450-msrswap",		    /* chip name */
	  #elif defined(CONFIG_OC_UL_DRV_U450_VARPINS_DIRNEG)
	     "16450-dirneg",		    /* chip name */
          #else
	     "16450",		    	    /* chip name */
          #endif
	#else /*CONFIG_OC_UL_DRV_U450_VARPINS*/
	     "16450",		    	    /* chip name */
	#endif /*CONFIG_OC_UL_DRV_U450_VARPINS*/
	     0);                            /* baud base - default */

  if (udrv==NULL)
    return -1;

  ul_drv_add_iac(udrv,UL_CMD_RDM,UL_IAC_OP_SND,ul_iac_call_rdm,NULL,0,0,NULL,0);
  ul_drv_add_iac(udrv,UL_CMD_ERM,UL_IAC_OP_CALLBACK,ul_iac_call_erm,NULL,0,UL_IAC_BFL_CB_OFFLT,NULL,0);
  ul_drv_add_iac(udrv,UL_CMD_WRM,UL_IAC_OP_REC,ul_iac_call_wrm,(char*)lpciap_buff,0,UL_IAC_BFL_CB_OFFLT,NULL,0);
  ul_drv_add_iac(udrv,UL_CMD_DEB,UL_IAC_OP_CALLBACK,ul_iac_call_deb,NULL,0,UL_IAC_BFL_CB_OFFLT,NULL,0); 
  ul_drv_add_iac(udrv,UL_CMD_RES,UL_IAC_OP_CALLBACK,ul_iac_call_res,NULL,0,UL_IAC_BFL_CB_OFFLT,NULL,0); 

  return ul_drv_add_dev(udrv);
}
#endif /* CONFIG_OC_UL_DRV_SYSLESS */

#ifdef CONFIG_OC_I2C_DRV_SYSLESS

int
i2cInit(void)
{
 #if I2C_DRV_SYSLESS_PORT == LPC_I2C0_BASE
  LPC_SC->PCONP |= (1 << 7); /*PI2C0*/
  #ifdef SDA0_PIN
  hal_pin_conf(SDA0_PIN);
  #endif
  #ifdef SCL0_PIN
  hal_pin_conf(SCL0_PIN);
  #endif
 #elif I2C_DRV_SYSLESS_PORT == LPC_I2C1_BASE
  LPC_SC->PCONP |= (1 << 19); /*PI2C1*/
  #ifdef SDA1_PIN
  hal_pin_conf(SDA1_PIN);
  #endif
  #ifdef SCL1_PIN
  hal_pin_conf(SCL1_PIN);
  #endif
 #elif I2C_DRV_SYSLESS_PORT == LPC_I2C2_BASE
  LPC_SC->PCONP |= (1 << 26); /*PI2C2*/
  #ifdef SDA1_PIN
  hal_pin_conf(SDA2_PIN);
  #endif
  #ifdef SCL1_PIN
  hal_pin_conf(SCL2_PIN);
  #endif
 #else
  #error unknown I2C_DRV_SYSLESS_PORT address
 #endif

  if (i2c_drv_init(&i2c_drv,
               I2C_DRV_SYSLESS_PORT,
               I2C_DRV_SYSLESS_IRQ,
               I2C_DRV_SYSLESS_BITRATE,
               I2C_DRV_SYSLESS_SLADR)<0) return -1;

  return 1;
}

#endif /*CONFIG_OC_I2C_DRV_SYSLESS*/


void _setup_board()
{

  // initialize the system
  sysInit();

  #ifdef WATCHDOG_ENABLED
    lpc_watchdog_init(1,WATCHDOG_TIMEOUT_MS);
    lpc_watchdog_feed();
  #endif /* WATCHDOG_ENABLED */

  #ifdef CONFIG_STDIO_COM_PORT
    uart0Init( B57600 , UART_8N1, UART_FIFO_8);
    init_system_stub();
  #endif /* CONFIG_STDIO_COM_PORT */

  // initialize the system timer
  timerInit();

  #ifdef SDRAM_SETUP
   if(sdram_init() < 0) {
     do {
      #ifdef LED1_PIN
       hal_gpio_set_value(LED1_PIN, 0);
      #endif
      #ifdef LED2_PIN
       hal_gpio_set_value(LED2_PIN, 1);
      #endif
       sys_timer_usleep(250000);
      #ifdef LED1_PIN
       hal_gpio_set_value(LED1_PIN, 1);
      #endif
      #ifdef LED2_PIN
       hal_gpio_set_value(LED2_PIN, 0);
      #endif
       sys_timer_usleep(750000);
     } while(1);
   }
  #endif /*SDRAM_SETUP*/

  #ifdef MPU_SETUP
   mpu_bsp_init();
  #endif /* MPU_SETUP */

  #ifdef CONFIG_OC_UL_DRV_SYSLESS
//    uld_debug_flg=0x3ff;
    uLanInit();
  #endif /* CONFIG_OC_UL_DRV_SYSLESS */

  #ifdef CONFIG_OC_I2C_DRV_SYSLESS
    i2cInit();
  #endif /* CONFIG_OC_I2C_DRV_SYSLESS */

}
