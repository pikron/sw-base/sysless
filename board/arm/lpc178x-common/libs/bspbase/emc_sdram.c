/*****************************************************************************
 *   emc_sdram.c:  Timer C file for NXP LPC177x_8x Family Microprocessors
 *
 *   Copyright(C) 2009, NXP Semiconductor
 *   All rights reserved.
 *
 *   History
 *   2009.05.26  ver 1.00    Prelimnary version, first Release
 *
 *   2013.01.05  modified for uLAN sys-less by Pavel Pisa
 *
******************************************************************************/
#include <system_def.h>
#include <LPC177x_8x.h>
#include <stdint.h>
#include <bspbase.h>

#ifdef SDRAM_BASE

volatile uint32_t ringosccount[2] = {0,0};

/*****************************************************************************
** Function name:		sdram_test
**
** Descriptions:		sdram test
**
** parameters:			None
**
** Returned value:		1 if test passed, otherwise 0
**
*****************************************************************************/
uint32_t sdram_test( void )
{
  volatile uint32_t *wr_ptr;
  volatile uint16_t *short_wr_ptr;
  uint32_t data;
  uint32_t i, j;

  wr_ptr = (uint32_t *)SDRAM_BASE;
  short_wr_ptr = (uint16_t *)wr_ptr;
  /* Clear content before 16 bit access test */
//  for (i = 0; i < SDRAM_SIZE/4; i++)
//  {
//	*wr_ptr++ = 0;
//  }

  /* 16 bit write */
  for (i = 0; i < SDRAM_SIZE/0x40000; i++)
  {
	for (j = 0; j < 0x100; j++)
	{
	  *short_wr_ptr++ = (i + j);
	  *short_wr_ptr++ = (i + j) + 1;
	}
  }

  /* Verifying */
  wr_ptr = (uint32_t *)SDRAM_BASE;
  for (i = 0; i < SDRAM_SIZE/0x40000; i++)
  {
	for (j = 0; j < 0x100; j++)
	{
	  data = *wr_ptr;
	  if (data != (((((i + j) + 1) & 0xFFFF) << 16) | ((i + j) & 0xFFFF)))
	  {
		return 0x0;
	  }
	  wr_ptr++;
 	}
  }
  return 0x1;
}

/*****************************************************************************
** Function name:		find_cmddly
**
** Descriptions:		find CMDDLY
**
** parameters:			None
**
** Returned value:		1 if test passed, otherwise 0
**
*****************************************************************************/
uint32_t sdram_find_cmddly(void)
{
  uint32_t cmddly, cmddlystart, cmddlyend, dwtemp;
  uint32_t ppass = 0x0, pass = 0x0;

  cmddly = 0x0;
  cmddlystart = cmddlyend = 0xFF;

  while (cmddly < 32)
  {
	dwtemp = LPC_SC->EMCDLYCTL & ~0x1F;
	LPC_SC->EMCDLYCTL = dwtemp | cmddly;

	if (sdram_test() == 0x1)
	{
	  /* Test passed */
	  if (cmddlystart == 0xFF)
	  {
		cmddlystart = cmddly;
	  }
	  ppass = 0x1;
	}
	else
	{
	  /* Test failed */
	  if (ppass == 1)
	  {
		cmddlyend = cmddly;
		pass = 0x1;
		ppass = 0x0;
	  }
	}

	/* Try next value */
	cmddly++;
  }

  /* If the test passed, the we can use the average of the min and max values to get an optimal DQSIN delay */
  if (pass == 0x1)
  {
	cmddly = (cmddlystart + cmddlyend) / 2;
  }
  else if (ppass == 0x1)
  {
	cmddly = (cmddlystart + 0x1F) / 2;
  }
  else
  {
	/* A working value couldn't be found, just pick something safe so the system doesn't become unstable */
	cmddly = 0x10;
  }

  dwtemp = LPC_SC->EMCDLYCTL & ~0x1F;
  LPC_SC->EMCDLYCTL = dwtemp | cmddly;

  return (pass | ppass);
}

/*****************************************************************************
** Function name:		sdram_find_fbclkdly
**
** Descriptions:		find FBCLKDLY
**
** parameters:			None
**
** Returned value:		1 if test passed, otherwise 0
**
*****************************************************************************/
uint32_t sdram_find_fbclkdly(void)
{
  uint32_t fbclkdly, fbclkdlystart, fbclkdlyend, dwtemp;
  uint32_t ppass = 0x0, pass = 0x0;

  fbclkdly = 0x0;
  fbclkdlystart = fbclkdlyend = 0xFF;

  while (fbclkdly < 32)
  {
	dwtemp = LPC_SC->EMCDLYCTL & ~0x1F00;
	LPC_SC->EMCDLYCTL = dwtemp | (fbclkdly << 8);

	if (sdram_test() == 0x1)
	{
	  /* Test passed */
	  if (fbclkdlystart == 0xFF)
	  {
		fbclkdlystart = fbclkdly;
	  }
	  ppass = 0x1;
	}
	else
	{
	  /* Test failed */
	  if (ppass == 1)
	  {
		fbclkdlyend = fbclkdly;
		pass = 0x1;
		ppass = 0x0;
	  }
	}

	/* Try next value */
	fbclkdly++;
  }

  /* If the test passed, the we can use the average of the min and max values to get an optimal DQSIN delay */
  if (pass == 0x1)
  {
	fbclkdly = (fbclkdlystart + fbclkdlyend) / 2;
  }
  else if (ppass == 0x1)
  {
	fbclkdly = (fbclkdlystart + 0x1F) / 2;
  }
  else
  {
	/* A working value couldn't be found, just pick something safe so the system doesn't become unstable */
	fbclkdly = 0x10;
  }

  dwtemp = LPC_SC->EMCDLYCTL & ~0x1F00;
  LPC_SC->EMCDLYCTL = dwtemp | (fbclkdly << 8);

  return (pass | ppass);
}

/*****************************************************************************
** Function name:		sdram_calibration
**
** Descriptions:		Calibration
**
** parameters:			None
**
** Returned value:		current ring osc count
**
*****************************************************************************/
uint32_t sdram_calibration( void )
{
  uint32_t dwtemp, i;
  uint32_t cnt = 0;

  for (i = 0; i < 10; i++)
  {
	dwtemp = LPC_SC->EMCCAL & ~0x4000;
	LPC_SC->EMCCAL = dwtemp | 0x4000;

	dwtemp = LPC_SC->EMCCAL;
	while ((dwtemp & 0x8000) == 0x0000)
	{
	  dwtemp = LPC_SC->EMCCAL;
	}
	cnt += (dwtemp & 0xFF);
  }
  return (cnt / 10);
}

/*****************************************************************************
** Function name:		sdram_adjust_timing
**
** Descriptions:		Adjust timing
**
** parameters:			None
**
** Returned value:		None
**
*****************************************************************************/
void sdram_adjust_timing( void )
{
  uint32_t dwtemp, cmddly, fbclkdly;

  /* Current value */
  ringosccount[1] = sdram_calibration();

  dwtemp = LPC_SC->EMCDLYCTL;
  cmddly = ((dwtemp & 0x1F) * ringosccount[0] / ringosccount[1]) & 0x1F;
  fbclkdly = ((dwtemp & 0x1F00) * ringosccount[0] / ringosccount[1]) & 0x1F00;
  LPC_SC->EMCDLYCTL = (dwtemp & ~0x1F1F) | fbclkdly | cmddly;
}

/*****************************************************************************
** Function name:		sdram_init
**
** Descriptions:		initialize External Memory Controller
**
** parameters:			None
**
** Returned value:		None
**
*****************************************************************************/
int sdram_init( void )
{
  uint32_t i, dwtemp = dwtemp;
  uint16_t wtemp = wtemp;

  if (((uintptr_t)sdram_init >= SDRAM_BASE) &&
      ((uintptr_t)sdram_init < SDRAM_BASE + SDRAM_SIZE)) {
    return 0;
  }

  LPC_SC->PCONP   	|= 0x00000800;
  LPC_SC->EMCDLYCTL   = 0x00001010;
  LPC_EMC->Control 	= 0x00000001;
  LPC_EMC->Config  	= 0x00000000;

#if 0
  EMC_PinConfig(); //Full 32-bit Data bus, 24-bit Address
#endif

  /* IS42S32800D-7TLI 8M X 32, 3V 86TSOP2 */

  //Configure memory layout, but MUST DISABLE BUFFERs during configuration
  LPC_EMC->DynamicConfig0    = 0x00004480; /* 256MB, 8Mx32, 4 banks, row=12, column=9 */

  /*Configure timing for ISSI IS4x32800D SDRAM*/

#if (SDRAM_SPEED==SDRAM_SPEED_48)
//Timing for 48MHz Bus
  LPC_EMC->DynamicRasCas0    = 0x00000201; /* 1 RAS, 2 CAS latency */
  LPC_EMC->DynamicReadConfig = 0x00000001; /* Command delayed strategy, using EMCCLKDELAY */
  LPC_EMC->DynamicRP         = 0x00000000; /* ( n + 1 ) -> 1 clock cycles */
  LPC_EMC->DynamicRAS        = 0x00000002; /* ( n + 1 ) -> 3 clock cycles */
  LPC_EMC->DynamicSREX       = 0x00000003; /* ( n + 1 ) -> 4 clock cycles */
  LPC_EMC->DynamicAPR        = 0x00000001; /* ( n + 1 ) -> 2 clock cycles */
  LPC_EMC->DynamicDAL        = 0x00000002; /* ( n ) -> 2 clock cycles */
  LPC_EMC->DynamicWR         = 0x00000001; /* ( n + 1 ) -> 2 clock cycles */
  LPC_EMC->DynamicRC         = 0x00000003; /* ( n + 1 ) -> 4 clock cycles */
  LPC_EMC->DynamicRFC        = 0x00000003; /* ( n + 1 ) -> 4 clock cycles */
  LPC_EMC->DynamicXSR        = 0x00000003; /* ( n + 1 ) -> 4 clock cycles */
  LPC_EMC->DynamicRRD        = 0x00000000; /* ( n + 1 ) -> 1 clock cycles */
  LPC_EMC->DynamicMRD        = 0x00000000; /* ( n + 1 ) -> 1 clock cycles */
#elif (SDRAM_SPEED==SDRAM_SPEED_50)
//Timing for 50MHz Bus (with 100MHz M3 Core)
  LPC_EMC->DynamicRasCas0    = 0x00000201; /* 1 RAS, 2 CAS latency */
  LPC_EMC->DynamicReadConfig = 0x00000001; /* Command delayed strategy, using EMCCLKDELAY */
  LPC_EMC->DynamicRP         = 0x00000000; /* ( n + 1 ) -> 1 clock cycles */
  LPC_EMC->DynamicRAS        = 0x00000002; /* ( n + 1 ) -> 3 clock cycles */
  LPC_EMC->DynamicSREX       = 0x00000003; /* ( n + 1 ) -> 4 clock cycles */
  LPC_EMC->DynamicAPR        = 0x00000001; /* ( n + 1 ) -> 2 clock cycles */
  LPC_EMC->DynamicDAL        = 0x00000002; /* ( n ) -> 2 clock cycles */
  LPC_EMC->DynamicWR         = 0x00000001; /* ( n + 1 ) -> 2 clock cycles */
  LPC_EMC->DynamicRC         = 0x00000003; /* ( n + 1 ) -> 4 clock cycles */
  LPC_EMC->DynamicRFC        = 0x00000003; /* ( n + 1 ) -> 4 clock cycles */
  LPC_EMC->DynamicXSR        = 0x00000003; /* ( n + 1 ) -> 4 clock cycles */
  LPC_EMC->DynamicRRD        = 0x00000000; /* ( n + 1 ) -> 1 clock cycles */
  LPC_EMC->DynamicMRD        = 0x00000000; /* ( n + 1 ) -> 1 clock cycles */
#elif (SDRAM_SPEED==SDRAM_SPEED_60)
  //Timing for 60 MHz Bus (same as 72MHz)
  LPC_EMC->DynamicRasCas0    = 0x00000202; /* 2 RAS, 2 CAS latency */
  LPC_EMC->DynamicReadConfig = 0x00000001; /* Command delayed strategy, using EMCCLKDELAY */
  LPC_EMC->DynamicRP         = 0x00000001; /* ( n + 1 ) -> 2 clock cycles */
  LPC_EMC->DynamicRAS        = 0x00000003; /* ( n + 1 ) -> 4 clock cycles */
  LPC_EMC->DynamicSREX       = 0x00000005; /* ( n + 1 ) -> 6 clock cycles */
  LPC_EMC->DynamicAPR        = 0x00000002; /* ( n + 1 ) -> 3 clock cycles */
  LPC_EMC->DynamicDAL        = 0x00000003; /* ( n ) -> 3 clock cycles */
  LPC_EMC->DynamicWR         = 0x00000001; /* ( n + 1 ) -> 2 clock cycles */
  LPC_EMC->DynamicRC         = 0x00000004; /* ( n + 1 ) -> 5 clock cycles */
  LPC_EMC->DynamicRFC        = 0x00000004; /* ( n + 1 ) -> 5 clock cycles */
  LPC_EMC->DynamicXSR        = 0x00000005; /* ( n + 1 ) -> 6 clock cycles */
  LPC_EMC->DynamicRRD        = 0x00000001; /* ( n + 1 ) -> 2 clock cycles */
  LPC_EMC->DynamicMRD        = 0x00000001; /* ( n + 1 ) -> 2 clock cycles */
#elif (SDRAM_SPEED==SDRAM_SPEED_72)
  //Timing for 72 MHz Bus
  LPC_EMC->DynamicRasCas0    = 0x00000202; /* 2 RAS, 2 CAS latency */
  LPC_EMC->DynamicReadConfig = 0x00000001; /* Command delayed strategy, using EMCCLKDELAY */
  LPC_EMC->DynamicRP         = 0x00000001; /* ( n + 1 ) -> 2 clock cycles */
  LPC_EMC->DynamicRAS        = 0x00000003; /* ( n + 1 ) -> 4 clock cycles */
  LPC_EMC->DynamicSREX       = 0x00000005; /* ( n + 1 ) -> 6 clock cycles */
  LPC_EMC->DynamicAPR        = 0x00000002; /* ( n + 1 ) -> 3 clock cycles */
  LPC_EMC->DynamicDAL        = 0x00000003; /* ( n ) -> 3 clock cycles */
  LPC_EMC->DynamicWR         = 0x00000001; /* ( n + 1 ) -> 2 clock cycles */
  LPC_EMC->DynamicRC         = 0x00000004; /* ( n + 1 ) -> 5 clock cycles */
  LPC_EMC->DynamicRFC        = 0x00000004; /* ( n + 1 ) -> 5 clock cycles */
  LPC_EMC->DynamicXSR        = 0x00000005; /* ( n + 1 ) -> 6 clock cycles */
  LPC_EMC->DynamicRRD        = 0x00000001; /* ( n + 1 ) -> 2 clock cycles */
  LPC_EMC->DynamicMRD        = 0x00000001; /* ( n + 1 ) -> 2 clock cycles */
#else
	#error UNSUPPORTED SDRAM FREQ
#endif

  sys_timer_usleep(100);
  LPC_EMC->DynamicControl    = 0x00000183; /* Issue NOP command */
  sys_timer_usleep(200);                   /* wait 200ms */
  LPC_EMC->DynamicControl    = 0x00000103; /* Issue PALL command */
  LPC_EMC->DynamicRefresh    = 0x00000002; /* ( n * 16 ) -> 32 clock cycles */
  for(i = 0; i < 0x80; i++);	           /* wait 128 AHB clock cycles */


#if (SDRAM_SPEED==SDRAM_SPEED_48)
  //Timing for 48MHz Bus
  LPC_EMC->DynamicRefresh    = 0x0000002E; /* ( n * 16 ) -> 736 clock cycles -> 15.330uS at 48MHz <= 15.625uS ( 64ms / 4096 row ) */
#elif (SDRAM_SPEED==SDRAM_SPEED_50)
  //Timing for 50MHz Bus
  LPC_EMC->DynamicRefresh    = 0x0000003A; /* ( n * 16 ) -> 768 clock cycles -> 15.360uS at 50MHz <= 15.625uS ( 64ms / 4096 row ) */
#elif (SDRAM_SPEED==SDRAM_SPEED_60)
  //Timing for 60MHz Bus
  LPC_EMC->DynamicRefresh    = 0x0000003A; /* ( n * 16 ) -> 928 clock cycles -> 15.466uS at 60MHz <= 15.625uS ( 64ms / 4096 row ) */
#elif (SDRAM_SPEED==SDRAM_SPEED_72)
  //Timing for 72MHz Bus
  LPC_EMC->DynamicRefresh    = 0x00000046; /* ( n * 16 ) -> 1120 clock cycles -> 15.556uS at 72MHz <= 15.625uS ( 64ms / 4096 row ) */
#else
	#error UNSUPPORTED SDRAM FREQ
#endif

  LPC_EMC->DynamicControl    = 0x00000083; /* Issue MODE command */
  //Timing for 48/60/72MHZ Bus
  dwtemp = *((volatile uint32_t *)(SDRAM_BASE | (0x22<<(2+2+9)))); /* 4 burst, 2 CAS latency */
  LPC_EMC->DynamicControl    = 0x00000000; /* Issue NORMAL command */
//[re]enable buffers
  LPC_EMC->DynamicConfig0    = 0x00084480; /* 256MB, 8Mx32, 4 banks, row=12, column=9 */
  /* Nominal value */
  ringosccount[0] = sdram_calibration();

  if (sdram_find_cmddly() == 0x0)
  {
	return -1;	/* fatal error */
  }

  if (sdram_find_fbclkdly() == 0x0)
  {
	return -2;	/* fatal error */
  }

  sdram_adjust_timing();

  return 0;
}

#endif /*SDRAM_BASE*/

/******************************************************************************
**                            End Of File
******************************************************************************/
