/******************************************************************************
 * based on software from:
 * Copyright 2004, R O SoftWare
 * No guarantees, warrantees, or promises, implied or otherwise.
 * May be used for hobby or commercial purposes provided copyright
 * notice remains intact.
 * 
 * reduced to learn what has to be done to enable and use UART0
 *****************************************************************************/
#ifndef INC_UART_H
#define INC_UART_H

#include <system_def.h>
#include "serial_reg.h"


///////////////////////////////////////////////////////////////////////////////
// Prehistoric solutions slect baudrates according to some magic constants
// Use SI defined Hz/Baud units for selection of baudrate directly for
// uart0Init() and uart1Init()
#define UART_BAUD(baud) (baud)

///////////////////////////////////////////////////////////////////////////////
// Definitions for typical UART 'baud' settings
#define B1200         UART_BAUD(1200)
#define B9600         UART_BAUD(9600)
#define B19200        UART_BAUD(19200)
#define B38400        UART_BAUD(38400)
#define B57600        UART_BAUD(57600)
#define B115200       UART_BAUD(115200)

///////////////////////////////////////////////////////////////////////////////
// Definitions for typical UART 'mode' settings
#define UART_8N1      (uint8_t)(UART_LCR_WLEN8 )
#define UART_7N1      (uint8_t)(UART_LCR_WLEN7 )
#define UART_8N2      (uint8_t)(UART_LCR_WLEN8 | UART_LCR_STOP)
#define UART_7N2      (uint8_t)(UART_LCR_WLEN7 | UART_LCR_STOP)
#define UART_8E1      (uint8_t)(UART_LCR_WLEN8 | UART_LCR_PARITY | UART_LCR_EPAR)
#define UART_7E1      (uint8_t)(UART_LCR_WLEN7 | UART_LCR_PARITY | UART_LCR_EPAR)
#define UART_8E2      (uint8_t)(UART_LCR_WLEN8 | UART_LCR_PARITY | UART_LCR_EPAR | UART_LCR_STOP)
#define UART_7E2      (uint8_t)(UART_LCR_WLEN7 | UART_LCR_PARITY | UART_LCR_EPAR | UART_LCR_STOP)
#define UART_8O1      (uint8_t)(UART_LCR_WLEN8 | UART_LCR_PARITY )
#define UART_7O1      (uint8_t)(UART_LCR_WLEN7 | UART_LCR_PARITY )
#define UART_8O2      (uint8_t)(UART_LCR_WLEN8 | UART_LCR_PARITY | UART_LCR_STOP)
#define UART_7O2      (uint8_t)(UART_LCR_WLEN7 | UART_LCR_PARITY | UART_LCR_STOP)

///////////////////////////////////////////////////////////////////////////////
// Definitions for typical UART 'fmode' settings
#define UART_FIFO_OFF (0x00)
#define UART_FIFO_1   (uint8_t)(UART_FCR_ENABLE_FIFO | UART_FCR_TRIGGER_1)
#define UART_FIFO_4   (uint8_t)(UART_FCR_ENABLE_FIFO | UART_FCR_TRIGGER_4)
#define UART_FIFO_8   (uint8_t)(UART_FCR_ENABLE_FIFO | UART_FCR_TRIGGER_8)
#define UART_FIFO_14  (uint8_t)(UART_FCR_ENABLE_FIFO | UART_FCR_TRIGGER_14)

void uart0Init(uint32_t baud, uint8_t mode, uint8_t fmode);
int uart0Putch(int ch);
uint16_t uart0Space(void);
const char *uart0Puts(const char *string);
int uart0TxEmpty(void);
void uart0TxFlush(void);
int uart0Getch(void);
int uart0GetchW(void);

#endif
