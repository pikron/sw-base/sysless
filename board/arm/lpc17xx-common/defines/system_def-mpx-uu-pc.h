/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  system_def.h - common cover for definition of hardware adresses,
                 registers, timing and other hardware dependant
		 parts of embedded hardware
 
  Copyright (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#ifndef _SYSTEM_DEF_H_
#define _SYSTEM_DEF_H_

#include <stdint.h>
#include <system_stub.h>
#include <LPC17xx.h>
#include <bspbase.h>

#ifndef MACH_LPC17XX
#define MACH_LPC17XX
#endif

#ifndef NULL
#define	NULL	0
#endif

#ifndef VER_CODE
#define VER_CODE(major,minor,patch) (major*0x10000+minor*0x100+patch)
#endif
/* Software version */
#define SW_VER_ID	"MPX-UU-PC"
#define SW_VER_MAJOR	0
#define SW_VER_MINOR	2
#define SW_VER_PATCH	0
#define SW_VER_CODE	VER_CODE(SW_VER_MAJOR,SW_VER_MINOR,SW_VER_PATCH)
/* Hardware version */
#define HW_VER_ID	"MPX-UU-PC"
#define HW_VER_MAJOR	1
#define HW_VER_MINOR	0
#define HW_VER_PATCH	0
#define HW_VER_CODE	VER_CODE(HW_VER_MAJOR,HW_VER_MINOR,HW_VER_PATCH)
/* Version of mechanical  */
#define MECH_VER_ID     "MPX-UU-PC"
#define MECH_VER_MAJOR  0
#define MECH_VER_MINOR  0
#define MECH_VER_PATCH  0
#define MECH_VER_CODE	VER_CODE(MECH_VER_MAJOR,MECH_VER_MINOR,MECH_VER_PATCH)


/*--------------------- Clock Configuration ----------------------------------
//
// <e> Clock Configuration
//   <h> System Controls and Status Register (SCS)
//     <o1.4>    OSCRANGE: Main Oscillator Range Select
//                     <0=>  1 MHz to 20 MHz
//                     <1=> 15 MHz to 24 MHz
//     <e1.5>       OSCEN: Main Oscillator Enable
//     </e>
//   </h>
//
//   <h> Clock Source Select Register (CLKSRCSEL)
//     <o2.0..1>   CLKSRC: PLL Clock Source Selection
//                     <0=> Internal RC oscillator
//                     <1=> Main oscillator
//                     <2=> RTC oscillator
//   </h>
//
//   <e3> PLL0 Configuration (Main PLL)
//     <h> PLL0 Configuration Register (PLL0CFG)
//                     <i> F_cco0 = (2 * M * F_in) / N
//                     <i> F_in must be in the range of 32 kHz to 50 MHz
//                     <i> F_cco0 must be in the range of 275 MHz to 550 MHz
//       <o4.0..14>  MSEL: PLL Multiplier Selection
//                     <6-32768><#-1>
//                     <i> M Value
//       <o4.16..23> NSEL: PLL Divider Selection
//                     <1-256><#-1>
//                     <i> N Value
//     </h>
//   </e>
//
//   <e5> PLL1 Configuration (USB PLL)
//     <h> PLL1 Configuration Register (PLL1CFG)
//                     <i> F_usb = M * F_osc or F_usb = F_cco1 / (2 * P)
//                     <i> F_cco1 = F_osc * M * 2 * P
//                     <i> F_cco1 must be in the range of 156 MHz to 320 MHz
//       <o6.0..4>   MSEL: PLL Multiplier Selection
//                     <1-32><#-1>
//                     <i> M Value (for USB maximum value is 4)
//       <o6.5..6>   PSEL: PLL Divider Selection
//                     <0=> 1
//                     <1=> 2
//                     <2=> 4
//                     <3=> 8
//                     <i> P Value
//     </h>
//   </e>
//
//   <h> CPU Clock Configuration Register (CCLKCFG)
//     <o7.0..7>  CCLKSEL: Divide Value for CPU Clock from PLL0
//                     <2-256:2><#-1>
//   </h>
//
//   <h> USB Clock Configuration Register (USBCLKCFG)
//     <o8.0..3>   USBSEL: Divide Value for USB Clock from PLL1
//                     <0-15>
//                     <i> Divide is USBSEL + 1
//   </h>
//
//   <h> Peripheral Clock Selection Register 0 (PCLKSEL0)
//     <o9.0..1>    PCLK_WDT: Peripheral Clock Selection for WDT
//                     <0=> Pclk = Cclk / 4
//                     <1=> Pclk = Cclk
//                     <2=> Pclk = Cclk / 2
//                     <3=> Pclk = Hclk / 8
//     <o9.2..3>    PCLK_TIMER0: Peripheral Clock Selection for TIMER0
//                     <0=> Pclk = Cclk / 4
//                     <1=> Pclk = Cclk
//                     <2=> Pclk = Cclk / 2
//                     <3=> Pclk = Hclk / 8
//     <o9.4..5>    PCLK_TIMER1: Peripheral Clock Selection for TIMER1
//                     <0=> Pclk = Cclk / 4
//                     <1=> Pclk = Cclk
//                     <2=> Pclk = Cclk / 2
//                     <3=> Pclk = Hclk / 8
//     <o9.6..7>    PCLK_UART0: Peripheral Clock Selection for UART0
//                     <0=> Pclk = Cclk / 4
//                     <1=> Pclk = Cclk
//                     <2=> Pclk = Cclk / 2
//                     <3=> Pclk = Hclk / 8
//     <o9.8..9>    PCLK_UART1: Peripheral Clock Selection for UART1
//                     <0=> Pclk = Cclk / 4
//                     <1=> Pclk = Cclk
//                     <2=> Pclk = Cclk / 2
//                     <3=> Pclk = Hclk / 8
//     <o9.12..13>  PCLK_PWM1: Peripheral Clock Selection for PWM1
//                     <0=> Pclk = Cclk / 4
//                     <1=> Pclk = Cclk
//                     <2=> Pclk = Cclk / 2
//                     <3=> Pclk = Hclk / 8
//     <o9.14..15>  PCLK_I2C0: Peripheral Clock Selection for I2C0
//                     <0=> Pclk = Cclk / 4
//                     <1=> Pclk = Cclk
//                     <2=> Pclk = Cclk / 2
//                     <3=> Pclk = Hclk / 8
//     <o9.16..17>  PCLK_SPI: Peripheral Clock Selection for SPI
//                     <0=> Pclk = Cclk / 4
//                     <1=> Pclk = Cclk
//                     <2=> Pclk = Cclk / 2
//                     <3=> Pclk = Hclk / 8
//     <o9.20..21>  PCLK_SSP1: Peripheral Clock Selection for SSP1
//                     <0=> Pclk = Cclk / 4
//                     <1=> Pclk = Cclk
//                     <2=> Pclk = Cclk / 2
//                     <3=> Pclk = Hclk / 8
//     <o9.22..23>  PCLK_DAC: Peripheral Clock Selection for DAC
//                     <0=> Pclk = Cclk / 4
//                     <1=> Pclk = Cclk
//                     <2=> Pclk = Cclk / 2
//                     <3=> Pclk = Hclk / 8
//     <o9.24..25>  PCLK_ADC: Peripheral Clock Selection for ADC
//                     <0=> Pclk = Cclk / 4
//                     <1=> Pclk = Cclk
//                     <2=> Pclk = Cclk / 2
//                     <3=> Pclk = Hclk / 8
//     <o9.26..27>  PCLK_CAN1: Peripheral Clock Selection for CAN1
//                     <0=> Pclk = Cclk / 4
//                     <1=> Pclk = Cclk
//                     <2=> Pclk = Cclk / 2
//                     <3=> Pclk = Hclk / 6
//     <o9.28..29>  PCLK_CAN2: Peripheral Clock Selection for CAN2
//                     <0=> Pclk = Cclk / 4
//                     <1=> Pclk = Cclk
//                     <2=> Pclk = Cclk / 2
//                     <3=> Pclk = Hclk / 6
//     <o9.30..31>  PCLK_ACF: Peripheral Clock Selection for ACF
//                     <0=> Pclk = Cclk / 4
//                     <1=> Pclk = Cclk
//                     <2=> Pclk = Cclk / 2
//                     <3=> Pclk = Hclk / 6
//   </h>
//
//   <h> Peripheral Clock Selection Register 1 (PCLKSEL1)
//     <o10.0..1>   PCLK_QEI: Peripheral Clock Selection for the Quadrature Encoder Interface
//                     <0=> Pclk = Cclk / 4
//                     <1=> Pclk = Cclk
//                     <2=> Pclk = Cclk / 2
//                     <3=> Pclk = Hclk / 8
//     <o10.2..3>   PCLK_GPIO: Peripheral Clock Selection for GPIOs
//                     <0=> Pclk = Cclk / 4
//                     <1=> Pclk = Cclk
//                     <2=> Pclk = Cclk / 2
//                     <3=> Pclk = Hclk / 8
//     <o10.4..5>   PCLK_PCB: Peripheral Clock Selection for the Pin Connect Block
//                     <0=> Pclk = Cclk / 4
//                     <1=> Pclk = Cclk
//                     <2=> Pclk = Cclk / 2
//                     <3=> Pclk = Hclk / 8
//     <o10.6..7>   PCLK_I2C1: Peripheral Clock Selection for I2C1
//                     <0=> Pclk = Cclk / 4
//                     <1=> Pclk = Cclk
//                     <2=> Pclk = Cclk / 2
//                     <3=> Pclk = Hclk / 8
//     <o10.10..11> PCLK_SSP0: Peripheral Clock Selection for SSP0
//                     <0=> Pclk = Cclk / 4
//                     <1=> Pclk = Cclk
//                     <2=> Pclk = Cclk / 2
//                     <3=> Pclk = Hclk / 8
//     <o10.12..13> PCLK_TIMER2: Peripheral Clock Selection for TIMER2
//                     <0=> Pclk = Cclk / 4
//                     <1=> Pclk = Cclk
//                     <2=> Pclk = Cclk / 2
//                     <3=> Pclk = Hclk / 8
//     <o10.14..15> PCLK_TIMER3: Peripheral Clock Selection for TIMER3
//                     <0=> Pclk = Cclk / 4
//                     <1=> Pclk = Cclk
//                     <2=> Pclk = Cclk / 2
//                     <3=> Pclk = Hclk / 8
//     <o10.16..17> PCLK_UART2: Peripheral Clock Selection for UART2
//                     <0=> Pclk = Cclk / 4
//                     <1=> Pclk = Cclk
//                     <2=> Pclk = Cclk / 2
//                     <3=> Pclk = Hclk / 8
//     <o10.18..19> PCLK_UART3: Peripheral Clock Selection for UART3
//                     <0=> Pclk = Cclk / 4
//                     <1=> Pclk = Cclk
//                     <2=> Pclk = Cclk / 2
//                     <3=> Pclk = Hclk / 8
//     <o10.20..21> PCLK_I2C2: Peripheral Clock Selection for I2C2
//                     <0=> Pclk = Cclk / 4
//                     <1=> Pclk = Cclk
//                     <2=> Pclk = Cclk / 2
//                     <3=> Pclk = Hclk / 8
//     <o10.22..23> PCLK_I2S: Peripheral Clock Selection for I2S
//                     <0=> Pclk = Cclk / 4
//                     <1=> Pclk = Cclk
//                     <2=> Pclk = Cclk / 2
//                     <3=> Pclk = Hclk / 8
//     <o10.26..27> PCLK_RIT: Peripheral Clock Selection for the Repetitive Interrupt Timer
//                     <0=> Pclk = Cclk / 4
//                     <1=> Pclk = Cclk
//                     <2=> Pclk = Cclk / 2
//                     <3=> Pclk = Hclk / 8
//     <o10.28..29> PCLK_SYSCON: Peripheral Clock Selection for the System Control Block
//                     <0=> Pclk = Cclk / 4
//                     <1=> Pclk = Cclk
//                     <2=> Pclk = Cclk / 2
//                     <3=> Pclk = Hclk / 8
//     <o10.30..31> PCLK_MC: Peripheral Clock Selection for the Motor Control PWM
//                     <0=> Pclk = Cclk / 4
//                     <1=> Pclk = Cclk
//                     <2=> Pclk = Cclk / 2
//                     <3=> Pclk = Hclk / 8
//   </h>
//
//   <h> Power Control for Peripherals Register (PCONP)
//     <o11.1>      PCTIM0: Timer/Counter 0 power/clock enable
//     <o11.2>      PCTIM1: Timer/Counter 1 power/clock enable
//     <o11.3>      PCUART0: UART 0 power/clock enable
//     <o11.4>      PCUART1: UART 1 power/clock enable
//     <o11.6>      PCPWM1: PWM 1 power/clock enable
//     <o11.7>      PCI2C0: I2C interface 0 power/clock enable
//     <o11.8>      PCSPI: SPI interface power/clock enable
//     <o11.9>      PCRTC: RTC power/clock enable
//     <o11.10>     PCSSP1: SSP interface 1 power/clock enable
//     <o11.12>     PCAD: A/D converter power/clock enable
//     <o11.13>     PCCAN1: CAN controller 1 power/clock enable
//     <o11.14>     PCCAN2: CAN controller 2 power/clock enable
//     <o11.15>     PCGPIO: GPIOs power/clock enable
//     <o11.16>     PCRIT: Repetitive interrupt timer power/clock enable
//     <o11.17>     PCMC: Motor control PWM power/clock enable
//     <o11.18>     PCQEI: Quadrature encoder interface power/clock enable
//     <o11.19>     PCI2C1: I2C interface 1 power/clock enable
//     <o11.21>     PCSSP0: SSP interface 0 power/clock enable
//     <o11.22>     PCTIM2: Timer 2 power/clock enable
//     <o11.23>     PCTIM3: Timer 3 power/clock enable
//     <o11.24>     PCUART2: UART 2 power/clock enable
//     <o11.25>     PCUART3: UART 3 power/clock enable
//     <o11.26>     PCI2C2: I2C interface 2 power/clock enable
//     <o11.27>     PCI2S: I2S interface power/clock enable
//     <o11.29>     PCGPDMA: GP DMA function power/clock enable
//     <o11.30>     PCENET: Ethernet block power/clock enable
//     <o11.31>     PCUSB: USB interface power/clock enable
//   </h>
// </e>
*/

#define CLOCK_SETUP           1

#define SCS_Val               0x00000020        /* OSCEN */
#define CLKSRCSEL_Val         0x00000001        /* XTAL */

#define PLL0_SETUP            1
#define PLL0CFG_Val           0x0000000B        /* 324403200Hz - must be in the range 275HMz-550MHz */

#define PLL1_SETUP            0
#define PLL1CFG_Val           0x00000023

#define CCLKCFG_Val           0x00000003        /* pplclk/(CCLKCFG_Val+1)=81100800Hz */
#define USBCLKCFG_Val         0x00000005

//#define PCLKSEL0_Val          0x00000000        /* all peripherial sysclk/4 */
//#define PCLKSEL1_Val          0x00000000
//#define PCONP_Val             0x042887DE

#define PCONP_CLK_DIV(x) ((x)==0?4:((x)==1?1:((x)==2?2:8)))

/*--------------------- Flash Accelerator Configuration ----------------------
//
// <e> Flash Accelerator Configuration
//   <o1.0..1>   FETCHCFG: Fetch Configuration
//               <0=> Instruction fetches from flash are not buffered
//               <1=> One buffer is used for all instruction fetch buffering
//               <2=> All buffers may be used for instruction fetch buffering
//               <3=> Reserved (do not use this setting)
//   <o1.2..3>   DATACFG: Data Configuration
//               <0=> Data accesses from flash are not buffered
//               <1=> One buffer is used for all data access buffering
//               <2=> All buffers may be used for data access buffering
//               <3=> Reserved (do not use this setting)
//   <o1.4>      ACCEL: Acceleration Enable
//   <o1.5>      PREFEN: Prefetch Enable
//   <o1.6>      PREFOVR: Prefetch Override
//   <o1.12..15> FLASHTIM: Flash Access Time
//               <0=> 1 CPU clock (for CPU clock up to 20 MHz)
//               <1=> 2 CPU clocks (for CPU clock up to 40 MHz)
//               <2=> 3 CPU clocks (for CPU clock up to 60 MHz)
//               <3=> 4 CPU clocks (for CPU clock up to 80 MHz)
//               <4=> 5 CPU clocks (for CPU clock up to 100 MHz)
//               <5=> 6 CPU clocks (for any CPU clock)
// </e>
*/
#define FLASH_SETUP           1
#define FLASHCFG_Val          0x0000403A

/*----------------------------------------------------------------------------
  Define clocks
 *----------------------------------------------------------------------------*/
#define XTAL        (12000000UL)        /* Oscillator frequency               */
#define OSC_CLK     (      XTAL)        /* Main oscillator frequency          */
#define RTC_CLK     (   32000UL)        /* RTC oscillator frequency           */
#define IRC_OSC     ( 4000000UL)        /* Internal RC oscillator frequency   */




#define SYS_TIMER_HZ 	    1000

#ifndef BIT
#define BIT(n)              (1 << (n))
#endif

// Port Bit Definitions & Macros:    Description - initial conditions
#define P0_0_UNUSED_BIT     BIT(0)      // P0.0 unused - low output
#define P0_1_UNUSED_BIT     BIT(1)      // P0.1 unused - low output
#define TXD0_BIT            BIT(2)      // used by UART0
#define RXD0_BIT            BIT(3)      // used by UART0
#define P0_6_UNUSED_BIT     BIT(6)      // P0.6 unused - low output
#define P0_7_UNUSED_BIT     BIT(7)      // P0.7 unused - low output
#define P0_8_UNUSED_BIT     BIT(8)      // P0.8 unused - low output
#define P0_9_UNUSED_BIT     BIT(9)      // P0.9 unused - low output
#define P0_10_UNUSED_BIT    BIT(10)     // P0.10 unused - low output
#define P0_11_UNUSED_BIT    BIT(11)     // P0.11 unused - low output
#define P0_15_UNUSED_BIT    BIT(15)     // P0.15 unused - low output
#define P0_16_UNUSED_BIT    BIT(16)     // P0.16 unused - low output
#define LED1_BIT            BIT(17)     // P0.17 LED1
#define LED2_BIT            BIT(18)     // P0.18 LED2
#define P0_22_UNUSED_BIT    BIT(22)     // P0.22 unused - low output
#define P0_25_UNUSED_BIT    BIT(25)     // P0.25 unused - low output
#define P0_26_UNUSED_BIT    BIT(26)     // P0.26 unused - low output
#define USBDPLUS_BIT        BIT(29)     // P0.29 USBD+
#define USBDMINUS_BIT       BIT(30)     // P0.30 USBD-


// Port Bit Definitions & Macros:    Description - initial conditions
#define P1_0_UNUSED_BIT     BIT(0)      // P1.0 unused - low output
#define P1_1_UNUSED_BIT     BIT(1)      // P1.1 unused - low output
#define P1_4_UNUSED_BIT     BIT(4)      // P1.4 unused - low output
#define P1_8_UNUSED_BIT     BIT(8)      // P1.8 unused - low output
#define P1_9_UNUSED_BIT     BIT(9)      // P1.9 unused - low output
#define P1_10_UNUSED_BIT    BIT(10)     // P1.10 unused - low output
#define P1_14_UNUSED_BIT    BIT(14)     // P1.14 unused - low output
#define P1_15_UNUSED_BIT    BIT(15)     // P1.15 unused - low output
#define P1_18_UNUSED_BIT    BIT(18)     // P1.18 unused - low output
#define WDT_BIT             BIT(19)     // P1.19 WDT
#define P1_20_UNUSED_BIT    BIT(20)     // P1.20 unused - low output
#define P1_22_UNUSED_BIT    BIT(22)     // P1.22 unused - low output
#define P1_23_UNUSED_BIT    BIT(23)     // P1.23 unused - low output
#define P1_24_UNUSED_BIT    BIT(24)     // P1.24 unused - low output
#define P1_25_UNUSED_BIT    BIT(25)     // P1.25 unused - low output
#define P1_26_UNUSED_BIT    BIT(26)     // P1.26 unused - low output
#define P1_28_UNUSED_BIT    BIT(28)     // P1.28 unused - low output
#define P1_29_UNUSED_BIT    BIT(29)     // P1.29 unused - low output
#define VBUS_BIT            BIT(30)     // P1.30
#define P1_31_UNUSED_BIT    BIT(31)     // P1.31 unused - low output

// Port Bit Definitions & Macros:    Description - initial conditions
#define TXD1_BIT            BIT(0)      // P2.0 TXD
#define RXD1_BIT            BIT(1)      // P2.1 RXD
#define CTS1_BIT	    BIT(2)	// P2.2 CTS connected to RXD1
#define P2_3_UNUSED_BIT     BIT(3)      // P2.3 unused - low output
#define DSR1_BIT	    BIT(4)	// P2.4 DSR connected to TXD1
#define P2_5_UNUSED_BIT     BIT(5)	// P2.5 unused - low output
#define P2_6_UNUSED_BIT	    BIT(6)	// P2.6 unused - low output
#define RTS1_BIT	    BIT(7)	// P2.7 RTS1 used as DIR1
#define P2_8_UNUSED_BIT	    BIT(8)	// P2.8 unused - low output
#define USB_CONNECT_BIT     BIT(9)      // P2.9 USB output for soft connect
#define BOOT_BIT            BIT(10)     // P2.10 Boot input

#define P4_28_UNUSED_BIT    BIT(28)     // P4.28 unused - low output
#define P4_29_UNUSED_BIT    BIT(29)     // P4.29 unused - low output

#define P0IO_INPUT_BITS      (uint32_t) ( \
                                         RXD0_BIT | \
                                         USBDPLUS_BIT | \
                                         USBDMINUS_BIT | \
                                         0 )

#define P1IO_INPUT_BITS      (uint32_t) ( \
                                         VBUS_BIT | \
                                         0 )

#define P2IO_INPUT_BITS      (uint32_t) ( \
                                         RXD1_BIT | \
                                         CTS1_BIT | \
                                         DSR1_BIT | \
                                         BOOT_BIT | \
                                         0 )

#define P3IO_INPUT_BITS      (uint32_t) ( \
                                         0 )

#define P4IO_INPUT_BITS      (uint32_t) ( \
                                         0 )

#define P0IO_ZERO_BITS       (uint32_t) ( \
                                         P0_0_UNUSED_BIT | \
                                         P0_1_UNUSED_BIT | \
                                         P0_6_UNUSED_BIT | \
                                         P0_7_UNUSED_BIT | \
                                         P0_8_UNUSED_BIT | \
                                         P0_9_UNUSED_BIT | \
                                         P0_10_UNUSED_BIT | \
                                         P0_11_UNUSED_BIT | \
                                         P0_15_UNUSED_BIT | \
                                         P0_16_UNUSED_BIT | \
                                         P0_22_UNUSED_BIT | \
                                         P0_25_UNUSED_BIT | \
                                         P0_26_UNUSED_BIT | \
                                         0 )

#define P1IO_ZERO_BITS       (uint32_t) ( \
                                         P1_0_UNUSED_BIT | \
                                         P1_1_UNUSED_BIT | \
                                         P1_4_UNUSED_BIT | \
                                         P1_8_UNUSED_BIT | \
                                         P1_9_UNUSED_BIT | \
                                         P1_10_UNUSED_BIT | \
                                         P1_14_UNUSED_BIT | \
                                         P1_15_UNUSED_BIT | \
                                         P1_18_UNUSED_BIT | \
                                         P1_20_UNUSED_BIT | \
                                         P1_22_UNUSED_BIT | \
                                         P1_23_UNUSED_BIT | \
                                         P1_24_UNUSED_BIT | \
                                         P1_25_UNUSED_BIT | \
                                         P1_26_UNUSED_BIT | \
                                         P1_28_UNUSED_BIT | \
                                         P1_29_UNUSED_BIT | \
                                         P1_31_UNUSED_BIT | \
                                         0 )

#define P2IO_ZERO_BITS       (uint32_t) ( \
                                         P2_3_UNUSED_BIT | \
                                         P2_5_UNUSED_BIT | \
                                         P2_6_UNUSED_BIT | \
                                         P2_8_UNUSED_BIT | \
                                         0 )

#define P3IO_ZERO_BITS       (uint32_t) ( \
                                         0 )

#define P4IO_ZERO_BITS       (uint32_t) ( \
                                         P4_28_UNUSED_BIT | \
                                         P4_29_UNUSED_BIT | \
                                         0 )

#define P0IO_ONE_BITS        (uint32_t) ( \
                                         TXD0_BIT | \
                                         LED1_BIT | \
                                         LED2_BIT | \
                                         0 )

#define P1IO_ONE_BITS        (uint32_t) ( \
                                         WDT_BIT | \
                                         0 )

#define P2IO_ONE_BITS        (uint32_t) ( \
                                         TXD1_BIT | \
                                         RTS1_BIT | \
                                         USB_CONNECT_BIT | \
                                         0 )

#define P3IO_ONE_BITS        (uint32_t) ( \
                                         0 )

#define P4IO_ONE_BITS        (uint32_t) ( \
                                         0 )

#define P0IO_OUTPUT_BITS     (uint32_t) ( \
                                         P0IO_ZERO_BITS | \
                                         P0IO_ONE_BITS )

#define P1IO_OUTPUT_BITS     (uint32_t) ( \
                                         P1IO_ZERO_BITS | \
                                         P1IO_ONE_BITS )

#define P2IO_OUTPUT_BITS     (uint32_t) ( \
                                         P2IO_ZERO_BITS | \
                                         P2IO_ONE_BITS )

#define P3IO_OUTPUT_BITS     (uint32_t) ( \
                                         P3IO_ZERO_BITS | \
                                         P3IO_ONE_BITS )

#define P4IO_OUTPUT_BITS     (uint32_t) ( \
                                         P4IO_ZERO_BITS | \
                                         P4IO_ONE_BITS )



/***************************************************************************/
/* io functions */
#define LED_GP			LED1_BIT  /* GENREAL PURPOSE LED */
#define LED_ERR			LED2_BIT

/***************************************************************************/
/* io functions */
#define LED_PORT		GPIO0->FIO
#define OUT_PORT		GPIO1->FIO

#define CREATE_PORT_NAME_PIN(port) port##PIN
#define CREATE_PORT_NAME_CLR(port) port##CLR
#define CREATE_PORT_NAME_SET(port) port##SET

#define GET_IN_PIN(port,in)	((CREATE_PORT_NAME_PIN(port) & in)?1:0)	
#define GET_IN_PORT(port)       (CREATE_PORT_NAME_PIN(port))
#define SET_OUT_PIN(port,out)   (CREATE_PORT_NAME_SET(port)=out)
#define CLR_OUT_PIN(port,out)   (CREATE_PORT_NAME_CLR(port)=out)

/***************************************************************************/
/* watchdog */
#define WATCHDOG_ENABLED
#define WATCHDOG_TIMEOUT_MS     1000

/***************************************************************************/
/* uLan configuration */

#ifdef UL_LOG_ENABLE
  #undef UL_LOG_ENABLE
#endif

#ifdef ULD_DEFAULT_BUFFER_SIZE
  #undef ULD_DEFAULT_BUFFER_SIZE
  #define ULD_DEFAULT_BUFFER_SIZE 0x2000
#endif

#define UL_DRV_SYSLESS_PORT UART1_BASE
#define UL_DRV_SYSLESS_BAUD 19200
#define UL_DRV_SYSLESS_IRQ UART1_IRQn
#define UL_DRV_SYSLESS_MY_ADR_DEFAULT 1

#define watchdog_feed lpc_watchdog_feed
#define kvpb_erase lpcisp_kvpb_erase
#define kvpb_copy lpcisp_kvpb_copy
#define kvpb_flush lpcisp_kvpb_flush
#define KVPB_DEFAULT_FLAGS KVPB_DESC_DOUBLE|KVPB_DESC_CHUNKWO

/***************************************************************************/
/* USB configuration */
#define USB_WITH_UDEV_FNC
#define USB_EP_NUM          32
#define USB_MAX_PACKET0     64
#define USB_MAX_PACKET      8
#define USB_DMA_EP          0x00000000

#define USB_VBUS_PIN_USED   1


#endif /* _SYSTEM_DEF_H_ */
