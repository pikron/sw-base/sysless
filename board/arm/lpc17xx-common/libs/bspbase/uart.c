/******************************************************************************
 *
 * $RCSfile$
 * $Revision$
 *
 * This module provides interface routines to the LPC ARM UARTs.
 * Copyright 2004, R O SoftWare
 * No guarantees, warrantees, or promises, implied or otherwise.
 * May be used for hobby or commercial purposes provided copyright
 * notice remains intact.
 *
 * reduced to see what has to be done for minimum UART-support by mthomas
 *****************************************************************************/

// #warning "this is a reduced version of the R O Software code"

#include <hal_machperiph.h>
#include "uart.h"
#include "serial_reg.h"

/* on LPC17xx: UART0 TX-Pin=P0.2, RX-Pin=P0.3
   PINSEL0 has to be set to "UART-Function" = Function "01" 
   for Pin 0.2 and 0.3 */

#define PINSEL_BIT_TXD0  (2*2)
#define PINSEL_BIT_RXD0  (3*2)
#define PINSEL_FIRST_ALT_FUNC   1
#define PINSEL_SECOND_ALT_FUNC  2

// Values of Bits 4-7 in PINSEL to activate UART0
#define UART0_PINSEL    ((PINSEL_FIRST_ALT_FUNC<<PINSEL_BIT_TXD0)|(PINSEL_FIRST_ALT_FUNC<<PINSEL_BIT_RXD0))
// Mask of Bits 4-7
#define UART0_PINMASK	((3<<PINSEL_BIT_TXD0)|(3<<PINSEL_BIT_RXD0))

// U0_LCR devisor latch bit 
#define UART0_LCR_DLAB  7

/*    baudrate divisor - use UART_BAUD macro
 *    mode - see typical modes (uart.h)
 *    fmode - see typical fmodes (uart.h)
 *    NOTE: uart0Init(UART_BAUD(9600), UART_8N1, UART_FIFO_8); 
 */
void uart0Init(uint32_t baud, uint8_t mode, uint8_t fmode)
{
  volatile int i;
  uint32_t baud_div;

  // setup Pin Function Select Register (Pin Connect Block) 
  // make sure old values of Bits 0-4 are masked out and
  // set them according to UART0-Pin-Selection
  PINCON->PINSEL0 = (PINCON->PINSEL0 & ~UART0_PINMASK) | UART0_PINSEL; 

  UART0->IER = 0x00;             // disable all interrupts
  //UART0->IIR = 0x00;             // clear interrupt ID register
  //UART0->LSR = 0x00;             // clear line status register

  baud *= 16;
  baud_div = (PCLK + baud / 4) / baud;

  // set the baudrate - DLAB must be set to access DLL/DLM
  UART0->LCR = (1<<UART0_LCR_DLAB); // set divisor latches (DLAB)
  UART0->DLL = (uint8_t)baud_div;         // set for baud low byte
  UART0->DLM = (uint8_t)(baud_div >> 8);  // set for baud high byte

  // set the number of characters and other
  // user specified operating parameters
  // Databits, Parity, Stopbits - Settings in Line Control Register
  UART0->LCR = (mode & ~(1<<UART0_LCR_DLAB)); // clear DLAB "on-the-fly"
  // setup FIFO Control Register (fifo-enabled + xx trig) 
  UART0->FCR = fmode;

  for(i=0;i<65000;i++);
}

int uart0Putch(int ch)
{
  while (!(UART0->LSR & UART_LSR_THRE)) // wait for TX buffer to empty
    continue;                           // also either WDOG() or swap()

  UART0->THR = (uint8_t)ch;  // put char to Transmit Holding Register
  return (uint8_t)ch;      // return char ("stdio-compatible"?)
}

int uart0PutchNW(int ch)
{
  if (!(UART0->LSR & UART_LSR_THRE)) // wait for TX buffer to empty
    return -1;                           // also either WDOG() or swap()

  UART0->THR = (uint8_t)ch;  // put char to Transmit Holding Register
  return (uint8_t)ch;      // return char ("stdio-compatible"?)
}

const char *uart0Puts(const char *string)
{
	char ch;
	
	while ((ch = *string)) {
		if (uart0Putch(ch)<0) break;
		string++;
	}
	
	return string;
}

int uart0TxEmpty(void)
{
  return (UART0->LSR & (UART_LSR_THRE | UART_LSR_TEMT)) == (UART_LSR_THRE | UART_LSR_TEMT);
}

void uart0TxFlush(void)
{
  UART0->FCR |= UART_FCR_CLEAR_XMIT;         // clear the TX fifo
}


/* Returns: character on success, -1 if no character is available */
int uart0Getch(void)
{
  if (UART0->LSR & UART_LSR_DR)              // check if character is available
    return UART0->RBR;                       // return character

  return -1;
}

/* Returns: character on success, waits */
int uart0GetchW(void)
{
	while ( !(UART0->LSR & UART_LSR_DR) ); // wait for character 
	return UART0->RBR;                // return character
}

