/*******************************************************************
  system_def-LTM10.h - common cover for definition of hardware
                       adresses, registers, timing and other
                       hardware dependant parts of embedded hardware
 
  Copyright (C) 2015 by Roman Bartosinski <bartosr@centrum.cz>
 *******************************************************************/

#ifndef _SYSTEM_DEF_H_
#define _SYSTEM_DEF_H_

#include <stdint.h>
#include <system_stub.h>
#include <nRF51822.h>
#include <bspbase.h>

#ifndef MACH_NRF51
#define MACH_NRF51
#endif

#ifndef VER_CODE
#define VER_CODE(major,minor,patch) (major*0x10000+minor*0x100+patch)
#endif
/* Software version */
#define SW_VER_ID	"LTM"
#define SW_VER_MAJOR	1
#define SW_VER_MINOR	0
#define SW_VER_PATCH	0
#define SW_VER_CODE	VER_CODE(SW_VER_MAJOR,SW_VER_MINOR,SW_VER_PATCH)
/* Hardware version */
#define HW_VER_ID	"LTM"
#define HW_VER_MAJOR	1
#define HW_VER_MINOR	0
#define HW_VER_PATCH	0
#define HW_VER_CODE	VER_CODE(HW_VER_MAJOR,HW_VER_MINOR,HW_VER_PATCH)


/* --- Main definitions ----------------------------------------------------- */
#ifndef NULL
  #define	NULL  0
#endif
#ifndef BIT
  #define BIT(n) (1 << (n))
#endif


/*--- Clock Configuration --------------------------------------------------- */
#ifndef CLOCK_SETUP
  #define CLOCK_SETUP           1
#endif

#define HFCLK_XTAL     (16000000UL)        /* HF oscillator frequency 16 or 32 MHz  */
#define LFCLK_XTAL     (32768UL)           /* LF oscillator frequency */

#define LFCLK_SOURCE   CLOCK_LFCLKSRC_SRC_Synth /* LF oscillator source - synthetized from internal HF crystal */

#define HFCLK_RUNONSTART    1
#define LFCLK_RUNONSTART    1  // required for RTC timer which substitutes SysTick


// generated clock from HFCLK
#define HCLK        (16000000UL)
#define PCLK1M      (1000000UL)
#define PCLK16M     (16000000UL)
// generated clock from LFCLK
#define PCLK32K     (32768UL)

// sysTick timer is emulated in RTC timer, which uses low frequency (LFCLK)
#ifndef SYS_TIMER_ENABLE
  #define SYS_TIMER_ENABLE  1
#endif
#ifndef SYS_TIMER_HZ
  #define SYS_TIMER_HZ      100
#endif


/* --- GPIO ----------------------------------------------------------------- */
#ifndef GPIO_SETUP
  #define GPIO_SETUP     1
#endif

/* -------------------------------------------------------------------------- */
/* deska LTM 1.0 */

// for testing purposes :
//  PWM LED outputs
#define LED1_PIN       (23)
#define LED2_PIN       (28)
#define LED3_PIN       (30)
#define LED4_PIN       (7)
#define LED5_PIN       (8)
#define LED6_PIN       (9)
// Button
#define BTN_PIN        (6)
#define PWRON_PIN      (4)
// ADC input
#define ADCIN_PIN      (2)
// UART
#define UART_TXD_PIN   (12)
#define UART_RXD_PIN   (16)
// hard connected RFX RX enable to RF amplifier
#define RFX_RXEN_PIN   (17)
#define RFX_TXEN_PIN   (19) /* workaround for LTM 1.0 */


#define LED1_BIT       BIT(LED1_PIN)
#define LED2_BIT       BIT(LED2_PIN)
#define LED3_BIT       BIT(LED3_PIN)
#define LED4_BIT       BIT(LED4_PIN)
#define LED5_BIT       BIT(LED5_PIN)
#define LED6_BIT       BIT(LED6_PIN)
#define BTN_BIT        BIT(BTN_PIN)
#define PWRON_BIT      BIT(PWRON_PIN)
#define ADCIN_BIT      BIT(ADCIN_PIN)
#define UART_TXD_BIT   BIT(UART_TXD_PIN)
#define UART_RXD_BIT   BIT(UART_RXD_PIN)
#define RFX_RXEN_BIT   BIT(RFX_RXEN_PIN)
#define RFX_TXEN_BIT   BIT(RFX_TXEN_PIN)


// initial settings
#define P0IO_OUTPUT    (LED1_BIT | LED2_BIT | LED3_BIT | LED4_BIT | LED5_BIT | LED6_BIT | PWRON_BIT | UART_TXD_BIT | RFX_RXEN_BIT | RFX_TXEN_BIT)
#define P0IO_ZEROS     (LED1_BIT | LED2_BIT | LED3_BIT | LED4_BIT | LED5_BIT | LED6_BIT | RFX_RXEN_BIT | RFX_TXEN_BIT)
#define P0IO_ONES      (PWRON_BIT)
#define P0IO_ENBINPUTS (BTN_BIT)

/* --- Watchdog ------------------------------------------------------------- */
#ifndef WATCHDOG_ENABLED
  #define WATCHDOG_ENABLED        0
#endif
#ifndef WATCHDOG_TIMEOUT_MS
  #define WATCHDOG_TIMEOUT_MS     1000
#endif


#endif /* _SYSTEM_DEF_H_ */
