/*******************************************************************
  system_def-YJ14004PA.h - common cover for definition of hardware
                           adresses, registers, timing and other
                           hardware dependant parts of embedded
                           hardware
 
  Copyright (C) 2015 by Roman Bartosinski <bartosr@centrum.cz>
 *******************************************************************/

#ifndef _SYSTEM_DEF_H_
#define _SYSTEM_DEF_H_

#include <stdint.h>
#include <system_stub.h>
#include <nRF51822.h>
#include <bspbase.h>

#ifndef MACH_NRF51
#define MACH_NRF51
#endif

#ifndef VER_CODE
#define VER_CODE(major,minor,patch) (major*0x10000+minor*0x100+patch)
#endif
/* Software version */
#define SW_VER_ID	"YJ14004PA"
#define SW_VER_MAJOR	0
#define SW_VER_MINOR	1
#define SW_VER_PATCH	0
#define SW_VER_CODE	VER_CODE(SW_VER_MAJOR,SW_VER_MINOR,SW_VER_PATCH)
/* Hardware version */
#define HW_VER_ID	"YJ14004PA"
#define HW_VER_MAJOR	1
#define HW_VER_MINOR	0
#define HW_VER_PATCH	0
#define HW_VER_CODE	VER_CODE(HW_VER_MAJOR,HW_VER_MINOR,HW_VER_PATCH)

/* --- Main definitions ----------------------------------------------------- */
#ifndef NULL
  #define	NULL  0
#endif
#ifndef BIT
  #define BIT(n) (1 << (n))
#endif


/*--- Clock Configuration --------------------------------------------------- */
#ifndef CLOCK_SETUP
  #define CLOCK_SETUP           1
#endif

#define HFCLK_XTAL     (16000000UL)        /* HF oscillator frequency 16 or 32 MHz  */
#define LFCLK_XTAL     (32768UL)           /* LF oscillator frequency */

#define LFCLK_SOURCE   CLOCK_LFCLKSRC_SRC_Xtal /* LF oscillator source - from external crystal */

#define HFCLK_RUNONSTART    1
#define LFCLK_RUNONSTART    1  // required for RTC timer which substitutes SysTick


// generated clock from HFCLK
#define HCLK        (16000000UL)
#define PCLK1M      (1000000UL)
#define PCLK16M     (16000000UL)
// generated clock from LFCLK
#define PCLK32K     (32768UL)

// sysTick timer is emulated in RTC timer, which uses low frequency (LFCLK)
#ifndef SYS_TIMER_ENABLE
  #define SYS_TIMER_ENABLE  1
#endif
#ifndef SYS_TIMER_HZ
  #define SYS_TIMER_HZ      100
#endif


/* --- GPIO ----------------------------------------------------------------- */
#ifndef GPIO_SETUP
  #define GPIO_SETUP     1
#endif

/* -------------------------------------------------------------------------- */
/* deska YJ14004PA - vyvedene piny jsou jen P0.09, P0.28, P0.29 */
/*                   dalsi nezapojene piny jsou P0.00-08, 10, 12-16, 18-25, 30  */

// for testing purposes :
//  LED is connected to P0.11
//  UART is connected to P0.28 (TXD), P0.29 (RXD)
#define LED1_PIN       (11)
// Button
#define BTN_PIN        (9)
// UART
#define UART_TXD_PIN   (28)
#define UART_RXD_PIN   (29)
// hard connected RFX RX enable to RF amplifier
#define RFX_RXEN_PIN   (17)

#define LED1_BIT       BIT(LED1_PIN)
#define BTN_BIT        BIT(BTN_PIN)
#define UART_TXD_BIT   BIT(UART_TXD_PIN)
#define UART_RXD_BIT   BIT(UART_RXD_PIN)
#define RFX_RXEN_BIT   BIT(RFX_RXEN_PIN)


// initial GPIO settings
#define P0IO_OUTPUT    ( LED1_BIT | RFX_RXEN_BIT)
#define P0IO_ZEROS     ( LED1_BIT | RFX_RXEN_BIT)
#define P0IO_ONES      ( 0 )
#define P0IO_ENBINPUTS (BTN_BIT)

/* --- Watchdog ------------------------------------------------------------- */
#ifndef WATCHDOG_ENABLED
  #define WATCHDOG_ENABLED        0
#endif
#ifndef WATCHDOG_TIMEOUT_MS
  #define WATCHDOG_TIMEOUT_MS     1000
#endif


#endif /* _SYSTEM_DEF_H_ */
