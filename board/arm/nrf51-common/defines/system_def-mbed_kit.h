/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  system_def.h - common cover for definition of hardware adresses,
                 registers, timing and other hardware dependant
		 parts of embedded hardware
 
  Copyright (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#ifndef _SYSTEM_DEF_H_
#define _SYSTEM_DEF_H_

#include <stdint.h>
#include <system_stub.h>
#include <nRF51822.h>
#include <bspbase.h>

#ifndef MACH_NRF51
#define MACH_NRF51
#endif

#ifndef NULL
#define	NULL	0
#endif

#ifndef VER_CODE
#define VER_CODE(major,minor,patch) (major*0x10000+minor*0x100+patch)
#endif
/* Software version */
#define SW_VER_ID	"mbedkit"
#define SW_VER_MAJOR	0
#define SW_VER_MINOR	1
#define SW_VER_PATCH	0
#define SW_VER_CODE	VER_CODE(SW_VER_MAJOR,SW_VER_MINOR,SW_VER_PATCH)
/* Hardware version */
#define HW_VER_ID	"mbedkit"
#define HW_VER_MAJOR	1
#define HW_VER_MINOR	0
#define HW_VER_PATCH	0
#define HW_VER_CODE	VER_CODE(HW_VER_MAJOR,HW_VER_MINOR,HW_VER_PATCH)
/* Version of mechanical  */
#define MECH_VER_ID     "mbedkit"
#define MECH_VER_MAJOR  0
#define MECH_VER_MINOR  0
#define MECH_VER_PATCH  0
#define MECH_VER_CODE	VER_CODE(MECH_VER_MAJOR,MECH_VER_MINOR,MECH_VER_PATCH)


/* --- Main definitions ----------------------------------------------------- */
#ifndef NULL
  #define	NULL  0
#endif
#ifndef BIT
  #define BIT(n) (1 << (n))
#endif


/*--- Clock Configuration --------------------------------------------------- */
#ifndef CLOCK_SETUP
  #define CLOCK_SETUP           1
#endif

#define HFCLK_XTAL     (16000000UL)        /* HF oscillator frequency 16 or 32 MHz  */
#define LFCLK_XTAL     (32768UL)           /* LF oscillator frequency */

#define LFCLK_SOURCE   CLOCK_LFCLKSRC_SRC_Xtal /* LF oscillator source - from external crystal */

#define HFCLK_RUNONSTART    1
#define LFCLK_RUNONSTART    1  // required for RTC timer which substitutes SysTick


// generated clock from HFCLK
#define HCLK        (16000000UL)
#define PCLK1M      (1000000UL)
#define PCLK16M     (16000000UL)
// generated clock from LFCLK
#define PCLK32K     (32768UL)


// sysTick timer is emulated in RTC timer, which uses low frequency (LFCLK)
#ifndef SYS_TIMER_ENABLE
  #define SYS_TIMER_ENABLE  1 
#endif
#ifndef SYS_TIMER_HZ
  #define SYS_TIMER_HZ      100
#endif


/* --- GPIO ----------------------------------------------------------------- */
#ifndef GPIO_SETUP
  #define GPIO_SETUP     1
#endif

// nRF mbed-kit: hard-wired LEDs are connected to P0.18, P0.19; BUTTONs are connected to P0.16, P0.17
#define BTN1_PIN       (16)
#define BTN2_PIN       (17)
#define LED1_PIN       (18)
#define LED2_PIN       (19)

#define BTN1_BIT       BIT(BTN1_PIN)
#define BTN2_BIT       BIT(BTN2_PIN)
#define LED1_BIT       BIT(LED1_PIN)
#define LED2_BIT       BIT(LED2_PIN)

// initial settings
#define P0IO_OUTPUT    ( LED1_BIT | LED2_BIT )
#define P0IO_ZEROS     ( LED1_BIT | LED2_BIT )
#define P0IO_ONES      ( 0 )

/* --- Watchdog ------------------------------------------------------------- */
#ifndef WATCHDOG_ENABLED
  #define WATCHDOG_ENABLED        0
#endif
#ifndef WATCHDOG_TIMEOUT_MS
  #define WATCHDOG_TIMEOUT_MS     1000
#endif

#endif /* _SYSTEM_DEF_H_ */
