/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  system_def.h - common cover for definition of hardware adresses,
                 registers, timing and other hardware dependant
     parts of embedded hardware

  Copyright (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#ifndef _SYSTEM_DEF_H_
#define _SYSTEM_DEF_H_

#include <stdint.h>
#include <system_stub.h>
#include <nRF52.h>
#include <bspbase.h>

#include <board_config.h>

#ifndef MACH_NRF52
#define MACH_NRF52
#endif

#ifndef NULL
#define NULL  0
#endif

#ifndef VER_CODE
#define VER_CODE(major,minor,patch) (major*0x10000+minor*0x100+patch)
#endif
/* Software version */
#define SW_VER_ID "nRF52-PDK"
#define SW_VER_MAJOR  0
#define SW_VER_MINOR  1
#define SW_VER_PATCH  0
#define SW_VER_CODE VER_CODE(SW_VER_MAJOR,SW_VER_MINOR,SW_VER_PATCH)
/* Hardware version */
#define HW_VER_ID "nRF52-PDK"
#define HW_VER_MAJOR  1
#define HW_VER_MINOR  0
#define HW_VER_PATCH  0
#define HW_VER_CODE VER_CODE(HW_VER_MAJOR,HW_VER_MINOR,HW_VER_PATCH)
/* Version of mechanical  */
#define MECH_VER_ID     "nRF52-PDK"
#define MECH_VER_MAJOR  0
#define MECH_VER_MINOR  0
#define MECH_VER_PATCH  0
#define MECH_VER_CODE VER_CODE(MECH_VER_MAJOR,MECH_VER_MINOR,MECH_VER_PATCH)


/* --- Main definitions ----------------------------------------------------- */
#ifndef NULL
  #define NULL  0
#endif

#ifndef BIT
  #define BIT(n) (1 << (n))
#endif

#ifndef PP2PIN
  #define PP2PIN(port,pin)  ((port<<5)|(pin&0x1f))
#endif
#ifndef PIN2PORT
  #define PIN2PORT(pin)     ((pin>>5)&0x1)
#endif
#ifndef PIN2PPIN
  #define PIN2PPIN(pin)     (pin&0x1f)
#endif

/*--- Clock Configuration --------------------------------------------------- */
#ifdef CONFIG_CLOCK_SETUP
  #define CLOCK_SETUP           CONFIG_CLOCK_SETUP
#else
  #define CLOCK_SETUP           1
#endif

#define HFCLK_XTAL     (32000000UL)        /* HF oscillator frequency 32 MHz  */
#define LFCLK_XTAL     (32768UL)           /* LF oscillator frequency */

#define HFCLK_SOURCE   CLOCK_HFCLKSTAT_SRC_Xtal /* HF oscillator source - from external crystal 32MHz */
#define LFCLK_SOURCE   CLOCK_LFCLKSRC_SRC_Xtal  /* LF oscillator source - from external crystal */

#define HFCLK_RUNONSTART    1
//#define LFCLK_RUNONSTART    1  // required for RTC timer which substitutes SysTick
#define LFCLK_RUNONSTART    1


// generated clocks from HFCLK
#define HCLK        (64000000UL)
#define PCLK1M       (1000000UL)
#define PCLK16M     (16000000UL)
#define PCLK32M     (32000000UL)
// generated clock from LFCLK
#define PCLK32K        (32768UL)


//// ARM SysTick timer is used

// sysTick timer is emulated in RTC timer, which uses low frequency (LFCLK)
#ifdef CONFIG_SYS_TIMER_ENABLE
  #define SYS_TIMER_ENABLE        CONFIG_SYS_TIMER_ENABLE
#else
  #define SYS_TIMER_ENABLE        1
#endif /* CONFIG_SYS_TIMER_ENABLE */
#ifdef CONFIG_SYS_TIMER_USE_SYSTICK
  #define SYS_TIMER_USE_SYSTICK   CONFIG_SYS_TIMER_USE_SYSTICK
#else
  #define SYS_TIMER_USE_SYSTICK   0
#endif /* CONFIG_SYS_TIMER_USE_SYSTICK */
#ifdef CONFIG_SYS_TIMER_HZ
  #define SYS_TIMER_HZ            CONFIG_SYS_TIMER_HZ
#else
  #define SYS_TIMER_HZ            100     // SysTimer = 10ms
#endif /* CONFIG_SYS_TIMER_HZ */

/*--- Peripherals Configuration --------------------------------------------- */


/* --- GPIO ----------------------------------------------------------------- */
#ifdef CONFIG_GPIO_SETUP
  #define GPIO_SETUP      CONFIG_GPIO_SETUP
#else
  #define GPIO_SETUP     1
#endif /* CONFIG_GPIO_SETUP */

// nRF52 PDK - PORT0
// LEDs are active low by default.
#define LED0_PIN      13
#define LED1_PIN      14
#define LED2_PIN      15
#define LED3_PIN      16
// Buttons are active low and pins must be configured as inputs with internal pull-up resitors.
#define BTN1_PIN      11
#define BTN2_PIN      12
#define BTN3_PIN      24
#define BTN4_PIN      25
// UART as Virtual COM on interface MCU
#define RX_PIN         8
#define TX_PIN         6
#define CTS_PIN        7
#define RTS_PIN        5
// External memory QSPI (64Mb flash MX25R6435F)
#define QSPI_PORT      0
#define QSPI_CS       17
#define QSPI_SCLK     19
#define QSPI_SIO0     20
#define QSPI_SIO1     21
#define QSPI_SIO2     22
#define QSPI_SIO3     23


#define BTN1_BIT       BIT(BTN1_PIN)
#define BTN2_BIT       BIT(BTN2_PIN)
#define BTN3_BIT       BIT(BTN3_PIN)
#define BTN4_BIT       BIT(BTN4_PIN)
#define LED0_BIT       BIT(LED0_PIN)
#define LED1_BIT       BIT(LED1_PIN)
#define LED2_BIT       BIT(LED2_PIN)
#define LED3_BIT       BIT(LED3_PIN)
#define RX_BIT         BIT(RX_PIN)
#define TX_BIT         BIT(TX_PIN)
#define CTS_BIT        BIT(CTS_PIN)
#define RTS_BIT        BIT(RTS_PIN)

// PORT1 bits
//#define XXX_BIT       BIT(XXX_PIN-32)

// initial settings
#define P0IO_OUTPUT    ( LED0_BIT | LED1_BIT | LED2_BIT | LED3_BIT | TX_BIT )
#define P0IO_ZEROS     ( 0 )
#define P0IO_ONES      ( LED0_BIT | LED1_BIT | LED2_BIT | LED3_BIT | TX_BIT )
#define P0IO_ENBINPUTS ( BTN1_BIT | BTN2_BIT | BTN3_BIT | BTN4_BIT )
#define P0IO_PULLUP    ( BTN1_BIT | BTN2_BIT | BTN3_BIT | BTN4_BIT )
#define P0IO_PULLDOWN  ( 0 )

#define P1IO_OUTPUT    (0)
#define P1IO_ZEROS     (0)
#define P1IO_ONES      (0)
#define P1IO_ENBINPUTS (0)
#define P1IO_PULLUP    (0)
#define P1IO_PULLDOWN  (0)

/* --- Watchdog ------------------------------------------------------------- */
#ifdef CONFIG_WATCHDOG_ENABLED
  #define WATCHDOG_ENABLED        CONFIG_WATCHDOG_ENABLED
#else
  #define WATCHDOG_ENABLED        0       // default: watchdog disabled
#endif /* CONFIG_WATCHDOG_ENABLED */
#ifdef CONFIG_WATCHDOG_TIMEOUT_MS
  #define WATCHDOG_TIMEOUT_MS     CONFIG_WATCHDOG_TIMEOUT_MS
#else
  #define WATCHDOG_TIMEOUT_MS     1000    // default: watchdog time = 1s
#endif /* CONFIG_WATCHDOG_TIMEOUT_MS */

/* --- USB ------------------------------------------------------------------ */
#define USB_WITH_UDEV_FNC
#define USB_USE_EXT_UDEV

#ifdef CONFIG_USB_MAX_PACKET0
  #define USB_MAX_PACKET0   CONFIG_USB_MAX_PACKET0
#else
  #define USB_MAX_PACKET0     64
#endif



#endif /* _SYSTEM_DEF_H_ */
