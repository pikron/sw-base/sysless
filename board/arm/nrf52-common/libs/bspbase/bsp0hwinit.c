#include "local_config.h"
#include <system_def.h>
#include <cpu_def.h>
#include <hal_machperiph.h>

//#ifdef CONFIG_KEYVAL
//  #include <keyvalpb.h>
//  #include <lpciap.h>
//  #include <lpciap_kvpb.h>
//#endif /* CONFIG_KEYVAL */
//#ifdef CONFIG_STDIO_COM_PORT
//  #include <uart.h>
//#endif
//#ifdef CONFIG_OC_UL_DRV_SYSLESS
//  #include <ul_lib/ulan.h>
//  #include <string.h>
//  #include <ul_drv_init.h>
//  #include <ul_drv_iac.h>
//  #include <ul_lib/ul_drvdef.h>
//  extern long int uld_jiffies;
//#endif /* CONFIG_OC_UL_DRV_SYSLESS */
//#ifdef CONFIG_OC_I2C_DRV_SYSLESS
//  #include <i2c_drv.h>
//#endif /* CONFIG_OC_I2C_DRV_SYSLESS */


///* timers */
volatile uint32_t sys_timer_ticks;

//#ifdef CONFIG_OC_I2C_DRV_SYSLESS
//#define I2C_DRV_NA_MSTIMEOUT    10
//i2c_drv_t i2c_drv;
//int i2c_drv_na_timer=0;
//#endif /* CONFIG_OC_I2C_DRV_SYSLESS */

#ifndef P0IO_ENBINPUTS
  #define P0IO_ENBINPUTS  0
#endif
#ifndef P0IO_PULLDOWN
  #define P0IO_PULLDOWN   0
#endif
#ifndef P0IO_PULLUP
  #define P0IO_PULLUP     0
#endif
#ifndef P1IO_ENBINPUTS
  #define P1IO_ENBINPUTS  0
#endif
#ifndef P1IO_PULLDOWN
  #define P1IO_PULLDOWN   0
#endif
#ifndef P1IO_PULLUP
  #define P1IO_PULLUP     0
#endif

static void sysInit(void)
{
  system_clock_init();

  #if (GPIO_SETUP)

    NRF_P0->OUTCLR = P0IO_ZEROS;
    NRF_P0->OUTSET = P0IO_ONES;
    NRF_P0->DIRSET = P0IO_OUTPUT;
    /* nRF51/nRF52 has all inputs disconnected by default - enable selected inputs */
    #if (P0IO_ENBINPUTS)
      uint32_t msk = 0x00000001, *pval = (uint32_t *)&(NRF_P0->PIN_CNF[0]);
      do {
        if (P0IO_ENBINPUTS & msk) {
          uint32_t nv = *pval & ~GPIO_PIN_CNF_INPUT_Msk & ~GPIO_PIN_CNF_DIR_Msk & ~GPIO_PIN_CNF_PULL_Msk;
          if (P0IO_PULLDOWN & msk) {
            nv = nv | (GPIO_PIN_CNF_PULL_Pulldown << GPIO_PIN_CNF_PULL_Pos);
          }
          if (P0IO_PULLUP & msk) {
            nv = nv | (GPIO_PIN_CNF_PULL_Pullup << GPIO_PIN_CNF_PULL_Pos);
          }
          *pval = nv;
        }
        pval++;
        msk <<= 1;
      } while(msk);
    #endif

    NRF_P1->OUTCLR = P1IO_ZEROS;
    NRF_P1->OUTSET = P1IO_ONES;
    NRF_P1->DIRSET = P1IO_OUTPUT;
    /* nRF51/nRF52 has all inputs disconnected by default - enable selected inputs */
    #if (P1IO_ENBINPUTS)
      uint32_t msk = 0x00000001, *pval = (uint32_t *)&(NRF_P1->PIN_CNF[0]);
      do {
        if (P1IO_ENBINPUTS & msk) {
          uint32_t nv = *pval & ~GPIO_PIN_CNF_INPUT_Msk & ~GPIO_PIN_CNF_DIR_Msk & ~GPIO_PIN_CNF_PULL_Msk;
          if (P1IO_PULLDOWN & msk) {
            nv = nv | (GPIO_PIN_CNF_PULL_Pulldown << GPIO_PIN_CNF_PULL_Pos);
          }
          if (P1IO_PULLUP & msk) {
            nv = nv | (GPIO_PIN_CNF_PULL_Pullup << GPIO_PIN_CNF_PULL_Pos);
          }
          *pval = nv;
        }
        pval++;
        msk <<= 1;
      } while(msk);
    #endif

  #endif
}

#if (SYS_TIMER_ENABLE) /* RTC IRQ hadler only if SYS_TIMER is enabled. */
  #if (SYS_TIMER_USE_SYSTICK)
void SysTick_Handler(void)
{
  sys_timer_ticks++;
}
  #else
void RTC0_IRQHandler(void)
{
  if (NRF_RTC0->EVENTS_TICK) { // TICK event
    NRF_RTC0->EVENTS_TICK = 0; /* clear event */

    sys_timer_ticks++;
  }
}
  #endif
#endif

/* use RTC0 timer */
void timerInit(void)
{
  sys_timer_ticks=0;
#if (SYS_TIMER_ENABLE)
  #if (SYS_TIMER_USE_SYSTICK)
    SysTick->LOAD = (HCLK/SYS_TIMER_HZ)-1;
    SysTick->CTRL = SysTick_CTRL_ENABLE_Msk | SysTick_CTRL_TICKINT_Msk | SysTick_CTRL_CLKSOURCE_Msk;
  #else
  NRF_RTC0->PRESCALER = ((((32768<<1)/SYS_TIMER_HZ)+1)>>1)-1; // f[kHz] = 32768/(PRESCALER+1)
  NVIC_EnableIRQ(RTC0_IRQn); // povolit preruseni od RTC0
  NRF_RTC0->INTENSET = RTC_INTENSET_TICK_Msk; // zapnout interrupt z TICK event v RTC0
  NRF_RTC0->TASKS_START = 1; // start timer
  #endif
#endif
}

//#ifdef CONFIG_STDIO_COM_PORT

//int uartcon_write(int file, const char * ptr, int len)
//{
//  int cnt;
//  unsigned char ch;
//  for(cnt=0;cnt<len;cnt++,ptr++){
//    ch=*ptr;
//    if(ch==0xa)
//      uart0Putch(0xd);
//    uart0Putch(ch);
//  }
//  return cnt;
//}

//void init_system_stub(void) {
//  system_stub_ops.write=uartcon_write;
//}

//#endif /* CONFIG_STDIO_COM_PORT */

//#ifdef CONFIG_OC_UL_DRV_SYSLESS

//extern unsigned uld_debug_flg; /* Left application  set defaults */

//#ifndef CONFIG_KEYVAL
//unsigned long lpciap_buff[ISP_RAM2FLASH_BLOCK_SIZE/4];
//#endif /* CONFIG_KEYVAL */

//#define UL_MTYPE_START32BIT 0x100

//static inline int ul_iac_mem_head_rd(uint8_t *buf, int len,
//                      uint32_t* pmtype, uint32_t* pstart, uint32_t* plen)
//{
//  uint32_t val, mtype;
//  if (len<6) return -1;
//  mtype=*(buf++);       /* memory type */
//  mtype+=*(buf++)<<8;
//  val=*(buf++);           /* start address */
//  val+=*(buf++)<<8;
//  if(mtype&UL_MTYPE_START32BIT){
//    if (len<8) return -1;
//    val+=(uint32_t)*(buf++)<<16;
//    val+=(uint32_t)*(buf++)<<24;
//  }
//  *pstart=val;
//  val=*(buf++);           /* length */
//  val+=*(buf++)<<8;
//  if(mtype&UL_MTYPE_START32BIT){
//    if (len==10) {
//      val+=(uint32_t)*(buf++)<<16;
//      val+=(uint32_t)*(buf++)<<24;
//    }
//  }
//  *plen=val;
//  mtype&=~UL_MTYPE_START32BIT;       /* 32-bit start address */
//  *pmtype=mtype;
//  return 0;
//}

//int ul_iac_call_rdm(struct ul_drv *udrv,ul_msginfo *msginfo,char *ibuff,ul_iac_data *data)
//{
//  uint32_t mtype,start,len;

//  data->len=0;

//  if(ul_iac_mem_head_rd((uint8_t *)ibuff, msginfo->len,&mtype,&start,&len))
//    return UL_IAC_RC_PROC;

//  if (mtype==0x00) {
//    data->len=len;
//    data->buff=(char*)start;
//    return UL_IAC_RC_FREEMSG;
//  }
//  return UL_IAC_RC_PROC;
//}

//int ul_iac_call_erm(struct ul_drv *udrv,ul_msginfo *msginfo,char *ibuff,ul_iac_data *data)
//{
//  uint32_t mtype,start,len;

//  data->len=0;

//  if(ul_iac_mem_head_rd((uint8_t *)ibuff, msginfo->len,&mtype,&start,&len))
//    return UL_IAC_RC_PROC;

// #ifdef CONFIG_KEYVAL
//  if (mtype==0x01) {
//    lpcisp_erase((void*)start,len);
//    data->len=0;
//    return UL_IAC_RC_FREEMSG;
//  }
// #endif /* CONFIG_KEYVAL */
//  return UL_IAC_RC_PROC;
//}

//int ul_iac_call_wrm(struct ul_drv *udrv,ul_msginfo *msginfo,char *ibuff,ul_iac_data *data)
//{
//  uint32_t mtype,start,len;

//  data->len=0;

//  if(ul_iac_mem_head_rd((uint8_t *)ibuff, msginfo->len,&mtype,&start,&len))
//    return UL_IAC_RC_PROC;

//  if (mtype==0x00) {
//    memcpy((void*)start,data->buff,data->len);
//    return UL_IAC_RC_FREEMSG;
//  }
// #ifdef CONFIG_KEYVAL
//  if (mtype==0x01) {
//    lpcisp_write((char*)start, data->buff, ISP_RAM2FLASH_BLOCK_SIZE);
//    return UL_IAC_RC_FREEMSG;
//  }
// #endif /* CONFIG_KEYVAL */
//  return UL_IAC_RC_PROC;
//}


//int ul_iac_call_deb(struct ul_drv *udrv,ul_msginfo *msginfo,char *ibuff,ul_iac_data *data)
//{
//  uint32_t debcmd,mtype,start;
//  uint8_t *p=(uint8_t*)ibuff;

//  if (msginfo->len<1) return UL_IAC_RC_PROC;
//  debcmd=*(p++);
//  switch (debcmd) {
//    case 0x10: /* goto */
//      data->len=0;
//      if (msginfo->len<5) return UL_IAC_RC_PROC;
//      mtype=*(p++);
//      mtype+=*(p++)<<8;
//      start=*(p++);
//      start+=*(p++)<<8;
//      if(mtype&UL_MTYPE_START32BIT){
//        mtype&=~UL_MTYPE_START32BIT;
//        if (msginfo->len<7) return UL_IAC_RC_PROC;
//        start+=(uint32_t)*(p++)<<16;
//        start+=(uint32_t)*(p++)<<24;
//      }
//      if (mtype==0x00)
//        ((void (*)())start)();
//    default:break;
//  }
//  return UL_IAC_RC_PROC;
//}

//int ul_iac_call_res(struct ul_drv *udrv,ul_msginfo *msginfo,char *ibuff,ul_iac_data *data)
//{
//  uint32_t rescmd,pass;
//  uint8_t *p=(uint8_t*)ibuff;

//  if (msginfo->len<1) return UL_IAC_RC_PROC;
//  rescmd=*(p++);
//  switch (rescmd) {
//    case ULRES_CPU: /* CPU */
//      data->len=0;
//      if (msginfo->len<3) return UL_IAC_RC_PROC;
//      pass=*(p++);
//      pass+=*(p++)<<8;
//      if (pass==0xaa55) {
////        MEMMAP=MEMMAP_FLASH;
//        lpc_watchdog_init(1,10); /* 10ms */
//        lpc_watchdog_feed();
//        while(1);
//      }
//    default:break;
//  }
//  return UL_IAC_RC_PROC;
//}

//int uLanInit()
//{
//  struct ul_drv *udrv;
//  unsigned int pinsel1_mask = 0;
//  unsigned int pinsel1_set  = 0;
//  unsigned int pinsel4_mask = 0;
//  unsigned int pinsel4_set  = 0;

//  /* set rs485 mode for UART1 */
// #if TXD1_BIT == BIT(15)
//  PINCON->PINSEL0 = (PINCON->PINSEL0 & ~0xC0000000) | 0x40000000; /* rxd on P0.15 */
// #elif TXD1_BIT == BIT(0)
//  pinsel4_mask |= 3 << (0*2);
//  pinsel4_set  |= 2 << (0*2);
// #else
//  #error TXD1_BIT is not set or valid
// #endif

// #if RXD1_BIT == BIT(16)
//  pinsel1_mask |= 3 << ((16-16)*2);
//  pinsel1_set  |= 1 << ((16-16)*2);
// #elif RXD1_BIT == BIT(1)
//  pinsel4_mask |= 3 << (1*2);
//  pinsel4_set  |= 2 << (1*2);
// #else
//  #error RXD1_BIT is not set or valid
// #endif

// #ifdef CTS1_BIT
// #if CTS1_BIT == BIT(17)
//  pinsel1_mask |= 3 << ((17-16)*2);
//  pinsel1_set  |= 1 << ((17-16)*2);
// #elif CTS1_BIT == BIT(2)
//  pinsel4_mask |= 3 << (2*2);
//  pinsel4_set  |= 2 << (2*2);
// #else
//  #error CTS1_BIT is not valid
// #endif
// #endif

// #ifdef DSR1_BIT
// #if DSR1_BIT == BIT(19)
//  pinsel1_mask |= 3 << ((19-16)*2);
//  pinsel1_set  |= 1 << ((19-16)*2);
// #elif DSR1_BIT == BIT(4)
//  pinsel4_mask |= 3 << (4*2);
//  pinsel4_set  |= 2 << (4*2);
// #else
//  #error CTS1_BIT is not valid
// #endif
// #endif

// #if RTS1_BIT == BIT(22)
//  pinsel1_mask |= 3 << ((22-16)*2);
//  pinsel1_set  |= 1 << ((22-16)*2);
// #elif RTS1_BIT == BIT(7)
//  pinsel4_mask |= 3 << (7*2);
//  pinsel4_set  |= 2 << (7*2);
// #else
//  #error RTS1_BIT is not set or valid
// #endif

//  /* port 0 .. 0x00003000; 0x00001000; rts(uDIR) */
//  PINCON->PINSEL1 = (PINCON->PINSEL1 & ~pinsel1_mask) | pinsel1_set;
//  /* port 2 .. 0x00000C0F;  0x0000080A; dsr(rxd), rxd, txd */
//  PINCON->PINSEL4 = (PINCON->PINSEL4 & ~pinsel4_mask) | pinsel4_set;

//  udrv=ul_drv_new(UL_DRV_SYSLESS_PORT,      /* port */
//             UL_DRV_SYSLESS_IRQ,            /* irq */
//       UL_DRV_SYSLESS_BAUD,     /* baud */
//       UL_DRV_SYSLESS_MY_ADR_DEFAULT, /* my adr */
//  #ifdef CONFIG_OC_UL_DRV_U450_VARPINS
//    #if defined(CONFIG_OC_UL_DRV_U450_VARPINS_DIRNEG) && defined(CONFIG_OC_UL_DRV_U450_VARPINS_MSRSWAP)
//       "16450-dirneg-msrswap",      /* chip name */
//    #elif defined(CONFIG_OC_UL_DRV_U450_VARPINS_MSRSWAP)
//       "16450-msrswap",       /* chip name */
//    #elif defined(CONFIG_OC_UL_DRV_U450_VARPINS_DIRNEG)
//       "16450-dirneg",        /* chip name */
//          #else
//       "16450",             /* chip name */
//          #endif
//  #else /*CONFIG_OC_UL_DRV_U450_VARPINS*/
//       "16450",             /* chip name */
//  #endif /*CONFIG_OC_UL_DRV_U450_VARPINS*/
//       0);                            /* baud base - default */

//  if (udrv==NULL)
//    return -1;

//  ul_drv_add_iac(udrv,UL_CMD_RDM,UL_IAC_OP_SND,ul_iac_call_rdm,NULL,0,0,NULL,0);
//  ul_drv_add_iac(udrv,UL_CMD_ERM,UL_IAC_OP_CALLBACK,ul_iac_call_erm,NULL,0,UL_IAC_BFL_CB_OFFLT,NULL,0);
//  ul_drv_add_iac(udrv,UL_CMD_WRM,UL_IAC_OP_REC,ul_iac_call_wrm,(char*)lpciap_buff,0,UL_IAC_BFL_CB_OFFLT,NULL,0);
//  ul_drv_add_iac(udrv,UL_CMD_DEB,UL_IAC_OP_CALLBACK,ul_iac_call_deb,NULL,0,UL_IAC_BFL_CB_OFFLT,NULL,0);
//  ul_drv_add_iac(udrv,UL_CMD_RES,UL_IAC_OP_CALLBACK,ul_iac_call_res,NULL,0,UL_IAC_BFL_CB_OFFLT,NULL,0);

//  return ul_drv_add_dev(udrv);
//}
//#endif /* CONFIG_OC_UL_DRV_SYSLESS */

//#ifdef CONFIG_OC_I2C_DRV_SYSLESS

//int
//i2cInit(void)
//{
// #if I2C_DRV_SYSLESS_PORT == I2C0_BASE
//  SC->PCONP |= (1 << 7); /*PI2C0*/
//  /* SDA0 P0.27, SCL0 P0.28 */
//  PINCON->PINSEL1 = (PINCON->PINSEL0 & ~0x03c00000) | 0x01400000;

// #elif I2C_DRV_SYSLESS_PORT == I2C1_BASE
//  SC->PCONP |= (1 << 19); /*PI2C1*/
//  #if SCL1_BIT == BIT(1)
//  /* SDA1 P0.0, SCL1 P0.1 */
//  PINCON->PINSEL0 |= 0x0000000f;
//  PINCON->PINMODE0 = (PINCON->PINMODE0 & ~0x0000000f) | 0x0000000A;
//  PINCON->PINMODE_OD0 |= 0x00000003;
//  #elif SCL1_BIT == BIT(20)
//  /* SDA1 P0.19, SCL1 P0.20 */
//  PINCON->PINSEL1 |= 0x000003c0;
//  PINCON->PINMODE1 = (PINCON->PINMODE1 & ~0x000003c0) | 0x00000280;
//  PINCON->PINMODE_OD0 |= 0x00180000;
//  #else
//   #error Unknown SCL1_BIT pin position
//  #endif

// #elif I2C_DRV_SYSLESS_PORT == I2C2_BASE
//  SC->PCONP |= (1 << 26); /*PI2C2*/
//  /* SDA2 P0.10, SCL2 P0.11 */
//  PINCON->PINSEL0 = (PINCON->PINSEL0 & ~0x00f00000) | 0x00A00000;
//  PINCON->PINMODE0 = (PINCON->PINMODE0 & ~0x00f00000) | 0x00A00000;
//  PINCON->PINMODE_OD0 |= 0x00000c00;
// #else
//  #error unknown I2C_DRV_SYSLESS_PORT address
// #endif

//  if (i2c_drv_init(&i2c_drv,
//               I2C_DRV_SYSLESS_PORT,
//               I2C_DRV_SYSLESS_IRQ,
//               I2C_DRV_SYSLESS_BITRATE,
//               I2C_DRV_SYSLESS_SLADR)<0) return -1;

//  return 1;
//}

//#endif /*CONFIG_OC_I2C_DRV_SYSLESS*/


void _setup_board()
{
  // initialize the system
  sysInit();

  #if (WATCHDOG_ENABLED)
    watchdog_init(1,WATCHDOG_TIMEOUT_MS);
    watchdog_feed();
  #endif /* WATCHDOG_ENABLED */

//  #ifdef CONFIG_STDIO_COM_PORT
//    uart0Init( B57600 , UART_8N1, UART_FIFO_8);
//    init_system_stub();
//  #endif /* CONFIG_STDIO_COM_PORT */

  // initialize the system timer
  timerInit();

//  #ifdef CONFIG_OC_UL_DRV_SYSLESS
////    uld_debug_flg=0x3ff;
//    uLanInit();
//  #endif /* CONFIG_OC_UL_DRV_SYSLESS */

//  #ifdef CONFIG_OC_I2C_DRV_SYSLESS
//    i2cInit();
//  #endif /* CONFIG_OC_I2C_DRV_SYSLESS */

}
