#ifndef _BSPBASE_H
#define _BSPBASE_H

#include <stdint.h>

extern volatile uint32_t sys_timer_ticks;

#define get_sys_timer_ticks() sys_timer_ticks
int sys_timer_configure(uint32_t sysfreq);

#endif /* _BSPBASE_H */
