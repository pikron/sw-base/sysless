/*
 * system_def.h
 *
 * Derived from the M2M implementation of Open Controller, by Ruud Vlaming
 * and Peter W. Zuidema.
 * Tran Duy Khanh <tran@pbmaster.org>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef _SYSTEM_DEF_H_
#define _SYSTEM_DEF_H_

/**
 * OpenController OC8 (backplane)
 * left to right: PB30 PB28 PB23 PB22 PB29 PB27 PB26 PB21
 */
#define AT91B_LED1          (1<<27)       /**< A2 - Rightmost LED */
#define AT91B_LED2          (1<<28)       /**< B2 */
#define AT91B_LED3          (1<<29)       /**< A1 */
#define AT91B_LED4          (1<<30)       /**< B1 - Leftmost LED */
#define AT91B_LED5          (1<<21)       /**< A4 */
#define AT91B_LED6          (1<<22)       /**< B4 */
#define AT91B_LED7          (1<<26)       /**< A3 */
#define AT91B_LED8          (1<<23)       /**< B3 */
#define AT91B_NB_LED        4             /**< Number of LEDs */
#define AT91B_LED_MASK      (AT91B_LED1|AT91B_LED2|AT91B_LED3|AT91B_LED4)

#define AT91B_POWERLED      (1<<12)       /** < PB12 */

/* All LEDs are on this PIO controller */
#define AT91D_BASE_PIO_LED  (AT91C_BASE_PIOB)

/**
 * ADC definitions
 */
#define M2M_ADC_CH1         AT91C_ADC_CH6 /**< Connector 4 Pin 2 'AD1' */
#define M2M_ADC_CH2         AT91C_ADC_CH7 /**< Connector 4 Pin 3 'AD2' */
#define M2M_ADC_CH3         AT91C_ADC_CH4 /**< Connector 7 Pin 2 'AD3' */
#define M2M_ADC_CH4         AT91C_ADC_CH5 /**< Connector 7 Pin 3 'AD4' */
#define M2M_ADC_CHANNEL_MASK (M2M_ADC_CH1 | M2M_ADC_CH2 | M2M_ADC_CH3 | M2M_ADC_CH4)

/**
 * Serial port definitions
 * The linedriver between the microcontroller and the connector can switch
 * between RS-232 and RS-485 mode. When in differential mode (RS-485) the RTS
 * line is used to enable the TX outputs.
 */

/* Serial port 0 : OC8 connector C5 (ethernet side) */
#define M2M_USART0_BASE     AT91C_BASE_US0  /**< Serial port 0 base register */
#define M2M_USART0_ID       AT91C_ID_US0    /**< Serial port 0 peripheral ID */
#define M2M_USART0_485      AT91C_PIO_PA2   /**< IO line to select mode RS-485 (1) or RS-232 (0) */
#define M2M_USART0_485_PIO  AT91C_BASE_PIOA /**< IO line connected to PIO A or B */
#define M2M_USART0_RXD      AT91C_PA0_RXD0  /**< RXD input pin  : C5.2 and C5.5 (peripheral A) */
#define M2M_USART0_TXD      AT91C_PA1_TXD0  /**< TXD output pin : C5.3 and C5.4 (peripheral A) */
#define M2M_USART0_RTS      AT91C_PA3_RTS0  /**< RTS output pin : C5.7 (peripheral A) */
#define M2M_USART0_CTS      AT91C_PA4_CTS0  /**< CTS input pin  : C5.6 (peripheral A) */

/* Serial port 1 : OC8 connector C2 (next power connector) */
#define M2M_USART1_BASE     AT91C_BASE_US1  /**< Serial port 1 base register */
#define M2M_USART1_ID       AT91C_ID_US1    /**< Serial port 1 peripheral ID */
#define M2M_USART1_485      AT91C_PIO_PB18  /**< IO line to select mode RS-485 (1) or RS-232 (0) */
#define M2M_USART1_485_PIO  AT91C_BASE_PIOB /**< IO line connected to PIO A or B */
#define M2M_USART1_RXD      AT91C_PA5_RXD1  /**< RXD input pin  : C2.2 and C2.5 (peripheral A) */
#define M2M_USART1_TXD      AT91C_PA6_TXD1  /**< TXD output pin : C2.3 and C2.4 (peripheral A) */
#define M2M_USART1_RTS      AT91C_PA8_RTS1  /**< RTS output pin : C2. (peripheral A) */
#define M2M_USART1_CTS      AT91C_PA9_CTS1  /**< CTS input pin  : C2. (peripheral A) */
#define M2M_USART1_DSR      AT91C_PB24_DSR1 /**< DSR input pin  : C2. (peripheral B) */
#define M2M_USART1_DTR      AT91C_PB25_DTR1 /**< DTR output pin : C2. (peripheral B) */

/* Definitions for (old) comm/serial.c */
#define serCOM        AT91C_BASE_US1      /**< Serial port base register */
#define serCOM_ID     AT91C_ID_US1        /**< Serial port peripheral ID */
#define US_RXD_PIN    AT91C_PA5_RXD1      /**< RXD input pin  : C2.2 (peripheral A) */
#define US_TXD_PIN    AT91C_PA6_TXD1      /**< TXD output pin : C2.3 (peripheral A) */

/**
 * SPI definitions
 */

/**
 * Maximal SPI clock speed in Hz.
 *
 * Uses configCPU_CLOCK_HZ to calculate the settings. The resulting clock
 * speed is MCK divided by an integer (1-255).
 */
#define AT91C_SPI_CLK 16000000

/**
 * DAC - Digital to Analog Converter - SPI channel
 */
#define M2M_DAC_SPI   AT91C_BASE_SPI1

/**
 * DAC SPI ChipSelect address
 * Select CS2 in the '4-to-16' mode: 1011b
 */
#define M2M_DAC_CS    0xB

/**
 * DataFlash 4Mbit AT45DB321 / 16Mbit AT45161
 */
#define M2M_DF_SPI   AT91C_BASE_SPI1

/**
 * DataFlash ChipSelect address
 * Select CS1 in the '4-to-16' mode: 1101b
 */
#define M2M_DF_CS    0xD

/**
 * Front IO Expander MCP23S17 for joystick and OLED (OC8 only)
 *
 * Connected via demultiplexer 74AHC138 output Y0.
 * SPI mode 3 (CPOL=1, NCPHA=0)
 */
#define M2M_SPI_FRONT   AT91C_BASE_SPI0

/**
 * Front IO expander SPI ChipSelect address
 *
 * Demux Y0 -> CBA = 000 -> CS0=0, CS1=0, CS3=0 => %0x00 = 0|4
 * 4-to-16 mode (PCSDEC=1) use group 0-3 = CS0
 */
#define M2M_SPI_FRONT_CS    0x0

/**
 * SD-Card
 *
 * Connected via demultiplexer 74AHC138 output Y1.
 * SPI mode 3 (CPOL=1, NCPHA=0)
 */
#define M2M_SPI_SDCARD      AT91C_BASE_SPI0

/**
 * SD-Card SPI ChipSelect address
 *
 * Demux Y1 -> CBA = 001 -> CS0=0, CS1=0, CS3=1 => %0x08 = 8|C
 * 4-to-16 mode (PCSDEC=1) use group 8-11 = CS2
 */
#define M2M_SPI_SDCARD_CS   0x8

/**
 * CARD A and CARD B are connected to this SPI bus.
 *
 * CARD A is located at the ethernet side. CARD B is located at the power side.
 */
#define M2M_SPI_CARDS   AT91C_BASE_SPI0

/**
 * CARD A CS1 ChipSelect address
 *
 * Demux Y5 -> CBA = 101 -> CS0=1, CS1=0, CS3=1 => %1x01 = 9|13
 * 4-to-16 mode (PCSDEC=1) use group 8-11 = CS2
 */
#define M2M_SPI_CARDA_CS1   0x9

/**
 * CARD A CS2 ChipSelect address
 *
 * Demux Y4 -> CBA = 100 -> CS0=1, CS1=0, CS3=0 => %0x01 = 1|5
 * 4-to-16 mode (PCSDEC=1) use group 0-3 = CS0
 */
#define M2M_SPI_CARDA_CS2   0x1

/**
 * CARD B CS1 ChipSelect address
 *
 * Demux Y3 -> CBA = 011 -> CS0=0, CS1=1, CS3=1 => %1x10 = 10|14
 * 4-to-16 mode (PCSDEC=1) use group 8-11 = CS2
 */
#define M2M_SPI_CARDB_CS1   0xA

/**
 * CARD B CS2 ChipSelect address
 *
 * Demux Y2 -> CBA = 010 -> CS0=0, CS1=1, CS3=0 => %0x10 = 2|6
 * 4-to-16 mode (PCSDEC=1) use group 0-3 = CS0
 */
#define M2M_SPI_CARDB_CS2   0x2

/**
 * IO Expander MCP23S17 to read the input on the backplane
 *
 * Connected via demultiplexer 74AHC138.
 * SPI mode 3 (CPOL=1, NCPHA=0)
 * \deprecated Obsolete (only rev 3 backplanes)
 */
#define M2M_IOEXP_SPI   AT91C_BASE_SPI0

/**
 * IO expander SPI ChipSelect address
 * CS3 must be high : 1xxxb -> 4-to-16 mode 1110 = CS0
 * Demux Y5 -> ABC = 101 -> CS0=1 CS1=0 CS3=1 = %1101 = 0xD
 * \deprecated Obsolete (only rev 3 backplanes)
 */
#define M2M_IOEXP_CS    0xD

/**
 * DataFlash definitions
 */

/**
 * Page size in bytes of the applied DataFlash chip.
 */
#define M2M_DF_PAGE_SIZE          528

/**
 * Total number of pages of the applied DataFlash chip.
 *
 * The  4Mbit DataFlash AT45DB041 has 2048 pages of 264 byte.\n
 * The 16Mbit DataFlash AT45DB161 has 4096 pages of 528 byte.\n
 * The 32Mbit DataFlash AT45DB321 has 8192 pages of 528 byte.\n
 * The 64Mbit DataFlash AT45DB642 has 8192 pages of 1056 byte.\n
 */
#define M2M_DF_PAGES             4096

/**
 * The page number where the settings start in the DataFlash.
 */
#define M2M_DF_SETTINGS_FIRSTPAGE   0

/**
 * The number of pages reserved for the settings.
 */
#define M2M_DF_SETTINGS_PAGES       1

/**
 * The page number where the logging starts in the DataFlash
 */
#define M2M_DF_LOG_FIRSTPAGE        1

/**
 * The number of pages reserved for the logging.
 */
#define M2M_DF_LOG_PAGES         3071

/**
 * The page number where the 1st firmware starts in the DataFlash.
 */
#define M2M_DF_FW1_FIRSTPAGE     3072

/**
 * The number of pages reserved for the 1st firmware.
 */
#define M2M_DF_FW1_PAGES          512

/**
 * The page number where the 2nd firmware starts in the DataFlash.
 */
#define M2M_DF_FW2_FIRSTPAGE     3584

/**
 * The number of pages reserved for the 2nd firmware.
 */
#define M2M_DF_FW2_PAGES          512

/**
 * I2C-bus definitions
 */

/**
 * SE95 temperature sensor slave address.
 *
 * Sensor directly connected to the microcontroller TWI-bus.
 */
#define M2M_I2C_SE95          0x1

/**
 * Oscillator definitions
 */

/**
 * Main Oscillator frequency in Hertz - MAINCK.
 *
 * Never used yet, but should be in calculation below.
 */
#define AT91B_MAIN_OSC        20000000

/**
 * configCPU_CLOCK_HZ = 47923200 (See FreeRTOSConfig.h)
 *
 * This macro is called from Cstartup_SAM7.c (ARM mode)
 *
 * TODO calculate it using AT91B_MAIN_OSC.
 *
 * PLLCK = MAINCK / DIV * (MUL + 1) =
 *
 * 14.318 MHz: DIV = 17; MUL = 113 -> PLLCK = 96014823 Hz \n
 * 18.432 MHz: DIV =  5; MUL = 25  -> PLLCK = 95846400 Hz -> CPU CK = 47923200 Hz \n
 * 20.000 MHz: DIV = 20; MUL = 95  -> PLLCK = 96000000 Hz -> CPU CK = 48000000 Hz \n
 */
#define Set_pPMC_PMC_PLLR()                           \
  pPMC->PMC_PLLR = ((AT91C_CKGR_DIV & 20) |           \
                   (AT91C_CKGR_PLLCOUNT & (28<<8)) |  \
                   (AT91C_CKGR_MUL & (95<<16)))

#endif /* _SYSTEM_DEF_H_ */
