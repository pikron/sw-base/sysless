//  ----------------------------------------------------------------------------
//           ATMEL Microcontroller Software Support  -  ROUSSET  -
//  ----------------------------------------------------------------------------
//   The software is delivered "AS IS" without warranty or condition of any
//   kind, either express, implied or statutory. This includes without
//   limitation any warranty or condition with respect to merchantability or
//   fitness for any particular purpose, or against the infringements of
//   intellectual property rights of others.
//  ----------------------------------------------------------------------------
//   File Name           : Cstartup_SAM7.c
//   Object              : Low level initializations written in C for IAR
//   1.0   08/Sep/04 JPP : Creation
//   1.10  10/Sep/04 JPP : Update AT91C_CKGR_PLLCOUNT filed
//   1.11  07/Jan/07 RV  : Cleaned up, modifed for SAM7XC256

/*
 * hwinit.c
 *
 * Derived from the M2M implementation of Open Controller, by Ruud Vlaming
 * and Peter W. Zuidema.
 * Tran Duy Khanh <tran@pbmaster.org>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/* Include the board file description */
#include <AT91SAM7XC256.h>
#include <system_def.h>
#include <lib_AT91SAM7XC256.h>
#include "hwinit.h"

void AT91F_LowLevelInit(void);

/* The following functions must also be compiled in ARM mode */
extern void AT91F_Spurious_Handler(void);
extern void AT91F_Default_IRQ_Handler(void);
extern void AT91F_Default_FIQ_Handler(void);


//*----------------------------------------------------------------------------
//* \fn    AT91F_LowLevelInit
//* \brief This function performs very low level HW initialization.
//*----------------------------------------------------------------------------
void AT91F_LowLevelInit(void)
{
	int            i;
	AT91PS_PMC     pPMC = AT91C_BASE_PMC;

	/* Set Flash Wait state */
	AT91C_BASE_MC->MC_FMR =
			((AT91C_MC_FMCN) & (50 << 16)) | AT91C_MC_FWS_1FWS;

	/* Watchdog Disable */
	AT91C_BASE_WDTC->WDTC_WDMR= AT91C_WDTC_WDDIS;

	/* Set MCK at 47 923 200 Hz
	 * 1. Enabling the Main Oscillator:
	 * SCK = 1/32768 = 30.51 microseconds
	 * Start up time = 8 * 6 / SCK = 56 * 30.51 = 1,46484375 ms */
	pPMC->PMC_MOR = ((AT91C_CKGR_OSCOUNT & (0x06 <<8)) | AT91C_CKGR_MOSCEN);
	/* Wait the startup time */
	while (!(pPMC->PMC_SR & AT91C_PMC_MOSCS))
		;

	/* Set the PLL with board specific values */
	Set_pPMC_PMC_PLLR();

	/* Wait the startup time */
	while (!(pPMC->PMC_SR & AT91C_PMC_LOCK))
		;

	/* 4. Selection of Master Clock and Processor Clock
	 * select the PLL clock divided by 2 */
	pPMC->PMC_MCKR = AT91C_PMC_PRES_CLK_2 ;
	while (!(pPMC->PMC_SR & AT91C_PMC_MCKRDY))
		;
	pPMC->PMC_MCKR |= AT91C_PMC_CSS_PLL_CLK ;
	while (!(pPMC->PMC_SR & AT91C_PMC_MCKRDY))
		;

	/* Enable User Reset and set its minimal assertion to
	 * 960 us = 2^(val+1) / 32768 -> val = 4 */
	AT91C_BASE_RSTC->RSTC_RMR = AT91C_RSTC_URSTEN | (0x4<<8) |
					(unsigned int) (0xA5<<24);

	/* Set up the default interrupt handler vectors */
	AT91C_BASE_AIC->AIC_SVR[0] = (int) AT91F_Default_FIQ_Handler;
	for (i=1;i < 31; i++) {
		AT91C_BASE_AIC->AIC_SVR[i] = (int) AT91F_Default_IRQ_Handler;
	}
	AT91C_BASE_AIC->AIC_SPU = (int) AT91F_Spurious_Handler;
}

/**
 * Configure the microcontroller and the peripherals.
 *
 * Main oscillator setup is performed by the low level init function
 * called from the startup asm file.
 */
static void SetupHardware(void)
{
	/* When using the JTAG debugger the hardware is not always initialised
	 * to the correct default state. This line just ensures that this does
	 * not cause all interrupts to be masked at the start. */
	AT91C_BASE_AIC->AIC_EOICR = 0;

	/* Configure the PIO Lines corresponding to LED1 to LEDx
	 * and the Powerled */
	AT91D_BASE_PIO_LED->PIO_PER = AT91B_LED_MASK | AT91B_POWERLED;
	AT91D_BASE_PIO_LED->PIO_OER = AT91B_LED_MASK | AT91B_POWERLED;

	/* Enable the peripheral clock for both PIO's */
	AT91C_BASE_PMC->PMC_PCER = (1 << AT91C_ID_PIOA) | (1 << AT91C_ID_PIOB);

#ifdef INCLUDE_ETHERNET
	/* Enable the peripheral clock for the EMAC */
	AT91C_BASE_PMC->PMC_PCER = (1 << AT91C_ID_EMAC);
#endif
}

/**
 * Trigger the hardware reset line which is connected to several peripheral
 * chips, including the OLED.
 */
static void HardwareReset(void)
{
	/* Enable User Reset */
	/* [8:11] 0-0xF : t = 2^(val+1)/32768 = 2^(2+1)/32768 = 213 us */
	AT91C_BASE_RSTC->RSTC_RMR = 0xA5000200 | AT91C_RSTC_URSTEN;
	/* External reset = nRST */
	AT91C_BASE_RSTC->RSTC_RCR = 0xA5000000 | AT91C_RSTC_EXTRST;

	/* Wait for hardware reset end. */
	while( !( AT91C_BASE_RSTC->RSTC_RSR & AT91C_RSTC_NRSTL ) ) {
		__asm( "NOP" );
	}
	__asm( "NOP" );
}

void hwinit()
{
	/* Trigger the NRST line to reset the peripherals */
	HardwareReset();
	/* Configure the processor. */
	SetupHardware();
}
