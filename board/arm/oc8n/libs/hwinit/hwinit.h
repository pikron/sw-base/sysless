/*
 * hwinit.h
 *
 * Lowlevel initialization for the Atmel AT91 - OC8-N board.
 *
 * Derived from the M2M implementation of Open Controller, by Ruud Vlaming
 * and Peter W. Zuidema.
 * Tran Duy Khanh <tran@pbmaster.org>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef __HWINIT_H__
#define __HWINIT_H__

void AT91F_LowLevelInit(void);
void hwinit(void);

#endif /* __HWINIT_H__ */
