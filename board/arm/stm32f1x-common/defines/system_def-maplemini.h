
#ifndef _SYSTEM_DEF_H_
#define _SYSTEM_DEF_H_

#include <stdint.h>
#include <system_stub.h>
#include <STM32F103xx.h>
#include <bspbase.h>
#include <hal_gpio.h>

#ifndef MACH_STM32F1XX
#define MACH_STM32F1XX
#endif

#ifndef NULL
  #define NULL  0
#endif

#ifndef BIT
  #define BIT(n)              (1 << (n))
#endif

#define VER_CODE(major,minor,patch) (major*0x10000+minor*0x100+patch)
/* Software version */
#define SW_VER_ID "MAPLEMINI"
#define SW_VER_MAJOR  0
#define SW_VER_MINOR  1
#define SW_VER_PATCH  0
#define SW_VER_CODE VER_CODE(SW_VER_MAJOR,SW_VER_MINOR,SW_VER_PATCH)
/* Hardware version */
#define HW_VER_ID "MAPLEMINI"
#define HW_VER_MAJOR  0
#define HW_VER_MINOR  1
#define HW_VER_PATCH  0
#define HW_VER_CODE VER_CODE(HW_VER_MAJOR,HW_VER_MINOR,HW_VER_PATCH)
/* Version of mechanical  */
#define MECH_VER_ID     "MAPLEMINI"
#define MECH_VER_MAJOR  0
#define MECH_VER_MINOR  1
#define MECH_VER_PATCH  0
#define MECH_VER_CODE VER_CODE(MECH_VER_MAJOR,MECH_VER_MINOR,MECH_VER_PATCH)

/*--------------------- Configuration --------------------------------------- */
#define WATCHDOG_ENABLED      0

#define CLOCK_SETUP           1

#define SCS_Val               0x00000020        /* OSCEN */
#define CLKSRCSEL_Val         0x00000001        /* XTAL */

#define PLL0_SETUP            1
#define PLL0CFG_Val           0x0000000B        /* 324403200Hz - must be in the range 275HMz-550MHz */

#define PLL1_SETUP            0
#define PLL1CFG_Val           0x00000023

#define CCLKCFG_Val           0x00000003        /* pplclk/(CCLKCFG_Val+1)=81100800Hz */
#define USBCLKCFG_Val         0x00000005

//#define PCLKSEL0_Val          0x00000000        /* all peripherial sysclk/4 */
//#define PCLKSEL1_Val          0x00000000
//#define PCONP_Val             0x042887DE

#define PCONP_CLK_DIV(x) ((x)==0?4:((x)==1?1:((x)==2?2:8)))

/* TODO: update or remove part about flash */
/*--------------------- Flash Accelerator Configuration ----------------------
//
// <e> Flash Accelerator Configuration
//   <o1.0..1>   FETCHCFG: Fetch Configuration
//               <0=> Instruction fetches from flash are not buffered
//               <1=> One buffer is used for all instruction fetch buffering
//               <2=> All buffers may be used for instruction fetch buffering
//               <3=> Reserved (do not use this setting)
//   <o1.2..3>   DATACFG: Data Configuration
//               <0=> Data accesses from flash are not buffered
//               <1=> One buffer is used for all data access buffering
//               <2=> All buffers may be used for data access buffering
//               <3=> Reserved (do not use this setting)
//   <o1.4>      ACCEL: Acceleration Enable
//   <o1.5>      PREFEN: Prefetch Enable
//   <o1.6>      PREFOVR: Prefetch Override
//   <o1.12..15> FLASHTIM: Flash Access Time
//               <0=> 1 CPU clock (for CPU clock up to 20 MHz)
//               <1=> 2 CPU clocks (for CPU clock up to 40 MHz)
//               <2=> 3 CPU clocks (for CPU clock up to 60 MHz)
//               <3=> 4 CPU clocks (for CPU clock up to 80 MHz)
//               <4=> 5 CPU clocks (for CPU clock up to 100 MHz)
//               <5=> 6 CPU clocks (for any CPU clock)
// </e>
*/
#define FLASH_SETUP           0
#define FLASHCFG_Val          0x0000403A

/*----------------------------------------------------------------------------
  Define clocks
 *----------------------------------------------------------------------------*/
#define XTAL        (8000000UL)        /* Oscillator frequency               */
#define OSC_CLK     (      XTAL)        /* Main oscillator frequency          */
#define RTC_CLK     (   32768UL)        /* RTC oscillator frequency           */
#define IRC_OSC     ( 4000000UL)        /* Internal RC oscillator frequency   */


#define CCLK                (__CORE_CLK)

#define SystemFrequency     CCLK        /* This constant is necessary for CMSIS drivers. It should be also calculated runtime, but for our case is this solution sufficient. */


#define DELAY_TUNE_FAC    0.40


#define SYS_TIMER_HZ      1000


/*----------------------------------------------------------------------------
  Initialize ports - Maple Mini
 *----------------------------------------------------------------------------*/
//definice
//// GPIO A
//#define ADC_IN0_BIT         BIT(0)      // used by ADC
//#define ADC_IN1_BIT         BIT(1)      // used by ADC
//#define ADC_IN2_BIT         BIT(2)      // used by ADC
//#define ADC_IN3_BIT         BIT(3)      // used by ADC
//#define ADC_IN4_BIT         BIT(4)      // used by ADC
//#define ADC_IN5_BIT         BIT(5)      // used by ADC
//#define ADC_IN6_BIT         BIT(6)      // used by ADC
//#define ADC_IN7_BIT         BIT(7)      // used by ADC

//#define UART1_CK_BIT        BIT(8)      // used by UART1
//#define UART1_TX_BIT        BIT(9)      // used by UART1
//#define UART1_RX_BIT        BIT(10)     // used by UART1
//#define UART1_CTS_BIT       BIT(11)     // used by UART1
//#define UART1_RTS_BIT       BIT(12)     // used by UART1

//#define UART2_CTS_BIT       BIT(0)      // used by UART2
//#define UART2_RTS_BIT       BIT(1)      // used by UART2
//#define UART2_TX_BIT        BIT(2)      // used by UART2
//#define UART2_RX_BIT        BIT(3)      // used by UART2
//#define UART2_CK_BIT        BIT(4)      // used by UART2

//#define SPI1_NSS_BIT        BIT(4)      // used by SPI1
//#define SPI1_SCK_BIT        BIT(5)      // used by SPI1
//#define SPI1_MISO_BIT       BIT(6)      // used by SPI1
//#define SPI1_MOSI_BIT       BIT(7)      // used by SPI1

//// GPIO B
//#define ADC_IN8_BIT         BIT(0)      // used by ADC
//#define ADC_IN9_BIT         BIT(1)      // used by ADC

//#define I2C1_SMBA_BIT       BIT(5)      // used by I2C1
//#define I2C1_SCL_BIT        BIT(6)      // used by I2C1
//#define I2C1_SDA_BIT        BIT(7)      // used by I2C1

//#define I2C2_SCL_BIT        BIT(10)      // used by I2C2
//#define I2C2_SDA_BIT        BIT(11)      // used by I2C2
//#define I2C2_SMBA_BIT       BIT(12)      // used by I2C2

//#define UART3_TX_BIT        BIT(10)      // used by UART3
//#define UART3_RX_BIT        BIT(11)      // used by UART3
//#define UART3_CK_BIT        BIT(12)      // used by UART3
//#define UART3_CTS_BIT       BIT(13)      // used by UART3
//#define UART3_RTS_BIT       BIT(14)      // used by UART3

//#define SPI2_NSS_BIT        BIT(12)      // used by SPI2
//#define SPI2_SCK_BIT        BIT(13)      // used by SPI2
//#define SPI2_MISO_BIT       BIT(14)      // used by SPI2
//#define SPI2_MOSI_BIT       BIT(15)      // used by SPI2

//// GPIO C

// PortB
#define LED_BIT       BIT(1)      // used by on-board LED
#define LED_PIN       PORT_PIN(1, 1, PORT_CONF_GPIO_OUT_HI)

/* corresponding GPIO peripherals also must be enabled */
#define INITIAL_PIN_SETUP_LIST \
    LED_PIN

/***************************************************************************/
// the following peripherals will be enabled at startup
#define ENABLED_PERIPHERALS1  \
  PERIPHERAL1_WWDG | \
  0

#define ENABLED_PERIPHERALS2  \
  PERIPHERAL2_GPIOA | \
  PERIPHERAL2_GPIOB | \
  0


#endif /* _SYSTEM_DEF_H_ */
