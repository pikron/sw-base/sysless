#include "local_config.h"
#include <system_def.h>

#include <cpu_def.h>
#include <lt_timer_types.h>
#include <hal_machperiph.h>
#include <system_stm32f10x.h>
#include "bspbase.h"

#ifdef CONFIG_KEYVAL
  #include <keyvalpb.h>
  #include <lpciap.h>
  #include <lpciap_kvpb.h>
#endif /* CONFIG_KEYVAL */

#ifdef CONFIG_STDIO_COM_PORT
  #include <uart.h>
#endif 


/* timers */
//volatile lt_ticks_t sys_timer_ticks;
//void (*timer0_isr_appl_call)(void);


/* sys init */
static const unsigned initial_pin_setup[] = {
  INITIAL_PIN_SETUP_LIST
};

static void sysInit(void)
{
  int i;

  SystemInit();

  // enable peripherals
#if defined(ENABLED_PERIPHERALS1) || defined(ENABLED_PERIPHERALS2)
  #ifndef ENABLED_PERIPHERALS1
    #define ENABLED_PERIPHERALS1 (0)
  #endif
  #ifndef ENABLED_PERIPHERALS2
    #define ENABLED_PERIPHERALS2 (0)
  #endif
  stm_enable_peripherals(ENABLED_PERIPHERALS1, ENABLED_PERIPHERALS2);
#endif

  // setup GPIOs - only on enabled peripherals
  for (i = 0; i < sizeof(initial_pin_setup) / sizeof(initial_pin_setup[0]); i++) {
    hal_pin_conf(initial_pin_setup[i]);
  }
}


//IRQ_HANDLER_FNC(timer0_isr)
//{
//  unsigned int ir;

//  ir=LPC_TIM0->IR;
//  if (ir&0x01) {
//    do {
//      if(timer0_isr_appl_call!=NULL)
//        timer0_isr_appl_call();
//      LPC_TIM0->MR0 += PCLK / SYS_TIMER_HZ;
//      LPC_TIM0->IR=0x01;               // Clear match0 interrupt
//     #ifdef CONFIG_OC_UL_DRV_SYSLESS
//      uld_jiffies++;
//     #endif
//     #ifdef CONFIG_OC_I2C_DRV_SYSLESS
//      if (i2c_drv.flags&I2C_DRV_MS_INPR) {
//        if (i2c_drv.flags&I2C_DRV_NA) {
//          i2c_drv_na_timer++;
//          if (i2c_drv_na_timer>I2C_DRV_NA_MSTIMEOUT) {
//             if (i2c_drv.stroke_fnc)
//              i2c_drv.stroke_fnc(&i2c_drv);
//            i2c_drv_na_timer=0;
//          }
//        } else {
//          i2c_drv_na_timer=0;
//        }
//        i2c_drv.flags|=I2C_DRV_NA;
//      }
//     #endif
//      sys_timer_ticks++;
//    } while (((int32_t)(LPC_TIM0->MR0-LPC_TIM0->TC))<0);
//  }
//  return IRQ_HANDLED;
//}

//void sys_timer_usleep(unsigned long usec)
//{
//  unsigned long wait_till;

//  wait_till = (unsigned long long)usec * PCLK / 1000000;
//  wait_till += LPC_TIM0->TC;
//  while ((long)(LPC_TIM0->TC - wait_till) < 0);
//}

//void timerInit(void)
//{
//  sys_timer_ticks=0;

//  request_irq(TIMER0_IRQn, timer0_isr, 0, NULL,NULL);
//  enable_irq(TIMER0_IRQn);

//  LPC_TIM0->TC=0;
//  LPC_TIM0->MCR=0;

//  LPC_TIM0->MR0= PCLK / SYS_TIMER_HZ;
//  LPC_TIM0->MCR|=1;				  /* TMCR_MR0_I; */

//  LPC_TIM0->TCR = 1; 			  /* Run timer 0*/
//}


#ifdef CONFIG_STDIO_COM_PORT
int uartcon_write(int file, const char * ptr, int len)
{
  int cnt;
  unsigned char ch;
  for(cnt=0;cnt<len;cnt++,ptr++){
    ch=*ptr;
    if(ch==0xa)
      uart0Putch(0xd);
    uart0Putch(ch);
  }
  return cnt;
}

void init_system_stub(void) {
  system_stub_ops.write=uartcon_write;
}
#endif /* CONFIG_STDIO_COM_PORT */


void _setup_board(void)
{

  // initialize the system
  sysInit();

#if WATCHDOG_ENABLED
  stm_watchdog_init(1,WATCHDOG_TIMEOUT_MS);
  stm_watchdog_feed();
#endif /* WATCHDOG_ENABLED */

#ifdef CONFIG_STDIO_COM_PORT
  uart0Init( B115200 , UART_8N1, UART_FIFO_8);
  init_system_stub();
#endif /* CONFIG_STDIO_COM_PORT */

  // initialize the system timer
//	timerInit();

#ifdef CONFIG_OC_UL_DRV_SYSLESS
//    uld_debug_flg=0x3ff;
  uLanInit();
#endif /* CONFIG_OC_UL_DRV_SYSLESS */

}
