#ifndef _BSPBASE_H
#define _BSPBASE_H

#include <types.h>
#include <lt_timer_types.h>
#include <cpu_def.h>

#ifndef NULL
  #define NULL  0
#endif

extern volatile lt_ticks_t sys_timer_ticks;

#define get_sys_timer_ticks() sys_timer_ticks

//tohle je nutne zavolat na zacatku preruseni, aby mohlo prijit dalsi
//bylo by lepsi dat do nejake knihovny v ARCH, a ne do BSP
#define TIMER0_IRQ_FLG_HANDLING TIM0->IR=0x01
#define TIMER1_IRQ_FLG_HANDLING TIM1->IR=0x01
#define TIMER2_IRQ_FLG_HANDLING TIM2->IR=0x01
#define TIMER3_IRQ_FLG_HANDLING TIM3->IR=0x01

#ifdef APPCFG_WITH_TIMER1

IRQ_HANDLER_FNC(timer1_isr);

void timer1_init(void);

static inline void set_timer1_handler(irq_handler_t *timer1_reload_isr)
{
  disable_irq(TIMER1_IRQn);
  free_irq(TIMER1_IRQn, NULL);
  request_irq(TIMER1_IRQn, timer1_reload_isr, 0, NULL,NULL);
  enable_irq(TIMER1_IRQn);
}

#endif /*APPCFG_WITH_TIMER1*/

void sys_err(int i);


#endif /* _BSPBASE_H */
