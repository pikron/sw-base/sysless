/***********************************************************************/
/*                                                                     */
/*  ROM.ld:  Linker Script File                                        */
/*                                                                     */
/***********************************************************************/

OUTPUT_FORMAT ("elf32-littlearm", "elf32-bigarm", "elf32-littlearm")

PROVIDE( __bbconf_pt_addr = 0 );

MEMORY
{
  /* STM32F103CB : 128kB ROM + 20kB SRAM + 512B SRAM USB/CAN buffers */
  /* no space for bootloader */
  /*--------------------------------------------------- */

  /* Main ROM region - 128k for STM32F103CB - all for APP */
  FLASH (rx) : ORIGIN = 0x08000000, LENGTH = 0x00020000

  /* local static RAM - 20k for STM32F103CB */
  IRAM0 (rwx) : ORIGIN = 0x20000000, LENGTH = 0x00005000

  /* stack location */
  STACK (rw) : ORIGIN = 0x20000000 + 0x00005000 - 32 - 4, LENGTH = 4

  /* AHB SRAM - 512B for STM32F103CB - shared RAM for USB/CAN buffers */
  IRAM1 (rwx) : ORIGIN = 0x40006000, LENGTH = 512
}


ENTRY(g_pfnVectors)

/* PROVIDE (_setup_board = 0); */
PROVIDE (__bbconf_pt_magic = 0xd1ab46d6);

/* SECTION command : Define mapping of input sections */
/* into output sections. */

SECTIONS
{
  /******************************************/
  /* code section */

  /* "normal" code */
  
  .text :
  {
    KEEP(*(.isr_vector .isr_vector.*))
    *(.text.unlikely .text.*_unlikely .text.unlikely.*)
    *(.text.exit .text.exit.*)
    *(.text.startup .text.startup.*)
    *(.text.hot .text.hot.*)
    *(.text .stub .text.* .gnu.linkonce.t.*)
    *(.gnu.warning)
    *(.glue_7t) *(.glue_7) *(.vfp11_veneer) *(.v4_bx)
  } >FLASH

  /* read only data */

  .rodata :
  {
    . = ALIGN(4);
    *(.rodata .rodata*)
    *(.gnu.linkonce.r.*)
  } >FLASH

  .rodata1 : { *(.rodata1) } >FLASH

  /* exceptions */

  .ARM.extab   : {
    *(.ARM.extab* .gnu.linkonce.armextab.*)
  } >FLASH

  .ARM.exidx :
  {
    . = ALIGN(4);
    __exidx_start = .;
    *(.ARM.exidx* .gnu.linkonce.armexidx.*)
    __exidx_end = .;
  } >FLASH

  .eh_frame_hdr : { *(.eh_frame_hdr) } >FLASH
  .eh_frame : { KEEP (*(.eh_frame)) } >FLASH
  .gcc_except_table : { *(.gcc_except_table .gcc_except_table.*) } >FLASH
  .exception_ranges : ONLY_IF_RW { *(.exception_ranges .exception_ranges*) } >FLASH

  /******************************************/
  /* .ctors .dtors are used for c++ constructors/destructors */
  .ctors :
  {
    . = ALIGN(4);
    PROVIDE(__ctors_start = .);
    KEEP(*(SORT(.ctors.*)))
    KEEP(*(.ctors))
    PROVIDE(__ctors_end = .);
  } >FLASH

  .dtors :
  {
    . = ALIGN(4);
    PROVIDE(__dtors_start = .);
    KEEP(*(SORT(.dtors.*)))
    KEEP(*(.dtors))
    PROVIDE(__dtors_end = .);

    . = ALIGN(4);
    /* End Of .text section */
    _etext = .;
                _sifastcode = .;
  } >FLASH

  .app :
  {
    PROVIDE (_mem_app_start = . );
  } > FLASH

  .irqarea (NOLOAD):
  {
    . = ALIGN (512);
    *(.irqarea .irqarea.*)
  } >IRAM0

  /**************************************************/
  /* fastcode - copied at startup & executed in RAM */

  .fastcode :
  {
    . = ALIGN (4);
    _sfastcode = . ;

    *(.fastcode)

    /* add other modules here ... */

    . = ALIGN (4);
    _efastcode = . ;
    _sidata = .;
  } >IRAM0 AT>FLASH

  /******************************************/
  /* This used for USB RAM section */
  .usbram (NOLOAD):
  {
    _usbram = . ;
    *(.usbram)
    . = ALIGN(4);
    _eusbram = . ;
    _usbram_end = . ;
  } > IRAM1

  /* This used for CAN RAM section */
  .canram (NOLOAD):
  {
    _usbram = . ;
    *(.usbram)
    . = ALIGN(4);
    _eusbram = . ;
    _usbram_end = . ;
  } > IRAM1

  /******************************************/
  /* data section */
  .data :
  {
    _sidata = LOADADDR (.data);
    . = ALIGN(4);
    _sdata = .;

    *(vtable vtable.*)
    *(.data .data.*)
    *(.gnu.linkonce.d*)
    *(.data1)

    . = ALIGN(4);
    _edata = . ;
  } >IRAM0 AT>FLASH

  /******************************************/
  /* For no-init variables section */
  .bss (NOLOAD) :
  {
    . = ALIGN(4);
    _sbss = . ;

    *(.dynbss)
    *(.bss .bss.*)
    *(.gnu.linkonce.b*)
    *(COMMON)

    . = ALIGN(4);
    _ebss = . ;

    . = ALIGN(4);
    _end = . ;
    PROVIDE (end = .);
  } >IRAM0

  .stack :
  {
    _stack = .;
  } >STACK


  /******************************************/
  /* Stabs debugging sections.  */
  .stab          0 : { *(.stab) }
  .stabstr       0 : { *(.stabstr) }
  .stab.excl     0 : { *(.stab.excl) }
  .stab.exclstr  0 : { *(.stab.exclstr) }
  .stab.index    0 : { *(.stab.index) }
  .stab.indexstr 0 : { *(.stab.indexstr) }
  /* .comment       0 : { *(.comment) } */
  /* DWARF debug sections.
   Symbols in the DWARF debugging sections are relative to the beginning
   of the section so we begin them at 0.  */
  /* DWARF 1 */
  .debug          0 : { *(.debug) }
  .line           0 : { *(.line) }
  /* GNU DWARF 1 extensions */
  .debug_srcinfo  0 : { *(.debug_srcinfo) }
  .debug_sfnames  0 : { *(.debug_sfnames) }
  /* DWARF 1.1 and DWARF 2 */
  .debug_aranges  0 : { *(.debug_aranges) }
  .debug_pubnames 0 : { *(.debug_pubnames) }
  /* DWARF 2 */
  .debug_info     0 : { *(.debug_info .gnu.linkonce.wi.*) }
  .debug_abbrev   0 : { *(.debug_abbrev) }
  .debug_line     0 : { *(.debug_line .debug_line.* .debug_line_end ) }
  .debug_frame    0 : { *(.debug_frame) }
  .debug_str      0 : { *(.debug_str) }
  .debug_loc      0 : { *(.debug_loc) }
  .debug_macinfo  0 : { *(.debug_macinfo) }
  /* SGI/MIPS DWARF 2 extensions */
  .debug_weaknames 0 : { *(.debug_weaknames) }
  .debug_funcnames 0 : { *(.debug_funcnames) }
  .debug_typenames 0 : { *(.debug_typenames) }
  .debug_varnames  0 : { *(.debug_varnames) }
  /* DWARF 3 */
  .debug_pubtypes 0 : { *(.debug_pubtypes) }
  .debug_ranges   0 : { *(.debug_ranges) }
  /* DWARF Extension.  */
  .debug_macro    0 : { *(.debug_macro) }
}
