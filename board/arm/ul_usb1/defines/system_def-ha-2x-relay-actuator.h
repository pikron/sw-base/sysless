/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  system_def.h - common cover for definition of hardware adresses,
                 registers, timing and other hardware dependant
		 parts of embedded hardware
 
  Copyright (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#ifndef _SYSTEM_DEF_H_
#define _SYSTEM_DEF_H_

#include <stdint.h>
#include <system_stub.h>
#include <LPC210x.h>
#include <bspbase.h>

#ifndef NULL
#define	NULL	0
#endif

#define WITH_SFI_SEL

#ifndef VER_CODE
#define VER_CODE(major,minor,patch) (major*0x10000+minor*0x100+patch)
#endif
/* Software version */
#define SW_VER_ID	"UL_HA_2X_RELAY_ACTUATOR"
#define SW_VER_MAJOR	0
#define SW_VER_MINOR	2
#define SW_VER_PATCH	0
#define SW_VER_CODE	VER_CODE(SW_VER_MAJOR,SW_VER_MINOR,SW_VER_PATCH)
/* Hardware version */
#define HW_VER_ID	"UL_HA_2X_RELAY_ACTUATOR"
#define HW_VER_MAJOR	1
#define HW_VER_MINOR	0
#define HW_VER_PATCH	0
#define HW_VER_CODE	VER_CODE(HW_VER_MAJOR,HW_VER_MINOR,HW_VER_PATCH)
/* Version of mechanical  */
#define MECH_VER_ID     "UL_HA_2X_RELAY_ACTUATOR"
#define MECH_VER_MAJOR  0
#define MECH_VER_MINOR  0
#define MECH_VER_PATCH  0
#define MECH_VER_CODE	VER_CODE(MECH_VER_MAJOR,MECH_VER_MINOR,MECH_VER_PATCH)


// PLL setup values are computed within the LPC include file
// It relies upon the following defines
#define FOSC                (14745600)  // Master Oscillator Freq.
#define PLL_MUL             (4)         // PLL Multiplier
#define CCLK                (FOSC * PLL_MUL) // CPU Clock Freq.

// Pheripheral Bus Speed Divider
#define PBSD                1           // MUST BE 1, 2, or 4
#define PCLK                (CCLK / PBSD) // Pheripheal Bus Clock Freq.

#define SYS_TIMER_HZ 	    1000

#ifndef BIT
#define BIT(n)              (1 << (n))
#endif

// Port Bit Definitions & Macros:    Description - initial conditions
#define RE2_BIT             BIT(0)      // used by UART0
#define RE1_BIT             BIT(1)      // used by UART0
#define LED5_BIT            BIT(2)      
#define LED4_BIT            BIT(3)      
#define LED3_BIT            BIT(4)      
#define LED2_BIT            BIT(5)      
#define LED1_BIT            BIT(6)      
#define P0_07_UNUSED_BIT    BIT(7)      
#define TXD1_BIT            BIT(8)      
#define RXD1_BIT            BIT(9)      // used by UART1
#define RTS1_BIT            BIT(10)     // used by UART1
#define CTS1_BIT            BIT(11)     // used by UART1
#define DSR1_BIT            BIT(12)     // used by UART1
#define P0_13_UNUSED_BIT    BIT(13)     
#define BOOT_BIT            BIT(14)     
#define IN1_BIT             BIT(15)     
#define IN2_BIT             BIT(16)     
#define P0_17_UNUSED_BIT    BIT(17)     
#define P0_18_UNUSED_BIT    BIT(18)     
#define AC_IN_BIT    	    BIT(19)     
#define P0_20_UNUSED_BIT    BIT(20)     
#define P0_21_UNUSED_BIT    BIT(21)     
#define P0_22_UNUSED_BIT    BIT(22)     
#define P0_23_UNUSED_BIT    BIT(23)     
#define P0_24_UNUSED_BIT    BIT(24)     
#define TEMP_BIT            BIT(25)     
#define P0_26_UNUSED_BIT    BIT(26)     
#define P0_27_UNUSED_BIT    BIT(27)     
#define P0_28_UNUSED_BIT    BIT(28)     
#define P0_29_UNUSED_BIT    BIT(29)     
#define P0_30_UNUSED_BIT    BIT(30)     
#define BUTTON_BIT          BIT(31)     


#define P0IO_INPUT_BITS      (uint32_t) ( \
					 BOOT_BIT | \
					 IN1_BIT | \
					 IN2_BIT | \
					 TEMP_BIT | \
					 BUTTON_BIT | \
                                         AC_IN_BIT | \
                                         0 )

#define P0IO_ZERO_BITS       (uint32_t) ( \
                                         RE2_BIT | \
                                         RE1_BIT | \
                                         LED1_BIT | \
                                         LED2_BIT | \
                                         LED3_BIT | \
                                         LED4_BIT | \
                                         LED5_BIT | \
                                         P0_07_UNUSED_BIT | \
                                         P0_13_UNUSED_BIT | \
	                              	 P0_17_UNUSED_BIT | \
  	                                 P0_18_UNUSED_BIT | \
                                         P0_20_UNUSED_BIT | \
                                         P0_21_UNUSED_BIT | \
                                         P0_22_UNUSED_BIT | \
                                         P0_23_UNUSED_BIT | \
                                         P0_24_UNUSED_BIT | \
                                         P0_26_UNUSED_BIT | \
                                         P0_27_UNUSED_BIT | \
                                         P0_28_UNUSED_BIT | \
                                         P0_29_UNUSED_BIT | \
                                         P0_30_UNUSED_BIT | \
                                         0 )



#define P0IO_ONE_BITS        (uint32_t) ( \
				         BOOT_BIT | \
                                         0 )

#define P0IO_OUTPUT_BITS     (uint32_t) ( \
                                         P0IO_ZERO_BITS | \
                                         P0IO_ONE_BITS )


/***************************************************************************/
/* io functions */
#define LED_GP			LED1_BIT  /* GENREAL PURPOSE LED */
#define LED_ERR			LED2_BIT

/***************************************************************************/
/* io functions */
#define IN_PORT			IO0
#define OUT_PORT		IO0
#define LED_PORT		IO0

#define CREATE_PORT_NAME_PIN(port) port##PIN
#define CREATE_PORT_NAME_CLR(port) port##CLR
#define CREATE_PORT_NAME_SET(port) port##SET

#define GET_IN_PIN(port,in)	((CREATE_PORT_NAME_PIN(port) & in)?1:0)	
#define SET_OUT_PIN(port,out)   (CREATE_PORT_NAME_SET(port)=out)
#define CLR_OUT_PIN(port,out)   (CREATE_PORT_NAME_CLR(port)=out)

/***************************************************************************/
/* watchdog */
#define WATCHDOG_ENABLED
#define WATCHDOG_TIMEOUT_MS	1000

/***************************************************************************/
/* uLan configuration */
#ifdef UL_LOG_ENABLE
  #undef UL_LOG_ENABLE
#endif

#ifdef ULD_DEFAULT_BUFFER_SIZE
  #undef ULD_DEFAULT_BUFFER_SIZE
  #define ULD_DEFAULT_BUFFER_SIZE 0x0400
#endif

#define UL_DRV_SYSLESS_PORT 0xE0010000
#define UL_DRV_SYSLESS_BAUD 19200
#define UL_DRV_SYSLESS_IRQ HAL_INTERRUPT_UART1
#define UL_DRV_SYSLESS_MY_ADR_DEFAULT 1


#define watchdog_feed lpc_watchdog_feed
#define kvpb_erase lpcisp_kvpb_erase
#define kvpb_copy lpcisp_kvpb_copy
#define kvpb_flush lpcisp_kvpb_flush
#define KVPB_DEFAULT_FLAGS KVPB_DESC_DOUBLE|KVPB_DESC_CHUNKWO

#define HAL_ARM_LPC2XXX_EXTINT_ERRATA 

#endif /* _SYSTEM_DEF_H_ */
