/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  system_def.h - common cover for definition of hardware adresses,
                 registers, timing and other hardware dependant
		 parts of embedded hardware
 
  Copyright (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#ifndef _SYSTEM_DEF_H_
#define _SYSTEM_DEF_H_

#include <stdint.h>
#include <system_stub.h>
#include <LPC214x.h>
#include <bspbase.h>

#ifndef NULL
#define	NULL	0
#endif

#define WITH_SFI_SEL

#ifndef VER_CODE
#define VER_CODE(major,minor,patch) (major*0x10000+minor*0x100+patch)
#endif
/* Software version */
#define SW_VER_ID	"UL_USB"
#define SW_VER_MAJOR	0
#define SW_VER_MINOR	2
#define SW_VER_PATCH	0
#define SW_VER_CODE	VER_CODE(SW_VER_MAJOR,SW_VER_MINOR,SW_VER_PATCH)
/* Hardware version */
#define HW_VER_ID	"UL_USB"
#define HW_VER_MAJOR	1
#define HW_VER_MINOR	0
#define HW_VER_PATCH	0
#define HW_VER_CODE	VER_CODE(HW_VER_MAJOR,HW_VER_MINOR,HW_VER_PATCH)
/* Version of mechanical  */
#define MECH_VER_ID     "UL_USB"
#define MECH_VER_MAJOR  0
#define MECH_VER_MINOR  0
#define MECH_VER_PATCH  0
#define MECH_VER_CODE	VER_CODE(MECH_VER_MAJOR,MECH_VER_MINOR,MECH_VER_PATCH)


// PLL setup values are computed within the LPC include file
// It relies upon the following defines
#define FOSC                (14745600)  // Master Oscillator Freq.
#define PLL_MUL             (4)         // PLL Multiplier
#define CCLK                (FOSC * PLL_MUL) // CPU Clock Freq.

// Pheripheral Bus Speed Divider
#define PBSD                1           // MUST BE 1, 2, or 4
#define PCLK                (CCLK / PBSD) // Pheripheal Bus Clock Freq.

#define SYS_TIMER_HZ 	    1000

#ifndef BIT
#define BIT(n)              (1 << (n))
#endif

// Port Bit Definitions & Macros:    Description - initial conditions
#define TXD0_BIT            BIT(0)      // used by UART0
#define RXD0_BIT            BIT(1)      // used by UART0
#define P0_02_UNUSED_BIT    BIT(2)      // P0.02 unused - low output
#define P0_03_UNUSED_BIT    BIT(3)      // P0.03 unused - low output
#define P0_04_UNUSED_BIT    BIT(4)      // P0.04 unused - low output
#define P0_05_UNUSED_BIT    BIT(5)      // P0.05 unused - low output
#define IP1_BIT		    BIT(6)      // P0.06 unused - low output
#define IP2_BIT  	    BIT(7)      // P0.07 unused - low output
#define TXD1_BIT            BIT(8)      // used by UART1
#define RXD1_BIT            BIT(9)      // used by UART1
#define RTS1_BIT            BIT(10)     // used by UART1
#define CTS1_BIT            BIT(11)     // used by UART1
#define DSR1_BIT            BIT(12)     // used by UART1
#define P0_13_UNUSED_BIT    BIT(13)     // P0.13 unused - low output
#define BOOT_BIT            BIT(14)     // SWITCH
#define P0_15_UNUSED_BIT    BIT(15)     // P0.15 unused - low output
#define P0_16_UNUSED_BIT    BIT(16)     // P0.16 unused - low output
#define P0_17_UNUSED_BIT    BIT(17)     // P0.17 unused - low output
#define P0_18_UNUSED_BIT    BIT(18)     // P0.18 unused - low output
#define P0_19_UNUSED_BIT    BIT(19)     // P0.19 unused - low output
#define P0_20_UNUSED_BIT    BIT(20)     // P0.20 unused - low output
#define LED1_BIT            BIT(21)     // used by LED
#define LED2_BIT            BIT(22)     // used by LED
#define P0_23_UNUSED_BIT    BIT(23)     // P0.23 unused - low output
#define P0_24_UNUSED_BIT    BIT(24)     // P0.24 unused - low output
#define P0_25_UNUSED_BIT    BIT(25)     // P0.25 unused - low output
#define P0_26_UNUSED_BIT    BIT(26)     // P0.26 unused - low output
#define P0_27_UNUSED_BIT    BIT(27)     // P0.27 unused - low output
#define P0_28_UNUSED_BIT    BIT(28)     // P0.28 unused - low output
#define P0_29_SOUND         BIT(29)     // P0.29 unused - low output
#define P0_30_LED_R         BIT(30)     // P0.30 unused - low output
#define P0_31_LED_G         BIT(31)     // P0.31 unused - low output


#define P1_16_RELE          BIT(16)     // P1.16 unused - low output
#define P1_17_UNUSED_BIT    BIT(17)     // P1.17 unused - low output
#define P1_18_UNUSED_BIT    BIT(18)     // P1.18 unused - low output
#define P1_19_UNUSED_BIT    BIT(19)     // P1.19 unused - low output
#define P1_20_UNUSED_BIT    BIT(20)     // P1.20 unused - low output
#define P1_21_UNUSED_BIT    BIT(21)     // P1.21 unused - low output
#define P1_22_UNUSED_BIT    BIT(22)     // P1.22 unused - low output
#define P1_23_UNUSED_BIT    BIT(23)     // P1.23 unused - low output
#define P1_24_COL369H       BIT(24)     // P1.24 unused - low output
#define P1_25_COL2580       BIT(25)     // P1.25 unused - low output
#define P1_26_COL147S       BIT(26)     // used by JTAG
#define P1_27_ROW123        BIT(27)     // used by JTAG
#define P1_28_ROW456        BIT(28)     // used by JTAG
#define P1_29_ROW789        BIT(29)     // used by JTAG
#define P1_30_ROWS0H        BIT(30)     // used by JTAG
#define P1_31_RING          BIT(31)     // used by JTAG

#define P0IO_INPUT_BITS      (uint32_t) ( \
					 BOOT_BIT | \
					 IP1_BIT | \
					 IP2_BIT | \
                                         0 )

#define P1IO_INPUT_BITS      (uint32_t) ( \
					 P1_27_ROW123 | \
					 P1_28_ROW456 | \
				         P1_29_ROW789 | \
					 P1_30_ROWS0H | \
					 P1_31_RING | \
                                         0 )

#define P0IO_ZERO_BITS       (uint32_t) ( \
                                         P0_02_UNUSED_BIT | \
                                         P0_03_UNUSED_BIT | \
                                         P0_04_UNUSED_BIT | \
                                         P0_05_UNUSED_BIT | \
					 P0_13_UNUSED_BIT | \
					 P0_15_UNUSED_BIT | \
					 P0_16_UNUSED_BIT | \
					 P0_17_UNUSED_BIT | \
					 P0_18_UNUSED_BIT | \
					 P0_19_UNUSED_BIT | \
					 P0_20_UNUSED_BIT | \
                                         P0_23_UNUSED_BIT | \
                                         P0_24_UNUSED_BIT | \
                                         P0_25_UNUSED_BIT | \
                                         P0_26_UNUSED_BIT | \
                                         P0_27_UNUSED_BIT | \
                                         P0_28_UNUSED_BIT | \
				         P0_29_SOUND | \
                                         0 )

#define P1IO_ZERO_BITS       (uint32_t) ( \
					 P1_16_RELE | \
					 P1_17_UNUSED_BIT | \
					 P1_18_UNUSED_BIT | \
					 P1_19_UNUSED_BIT | \
					 P1_20_UNUSED_BIT | \
					 P1_21_UNUSED_BIT | \
					 P1_22_UNUSED_BIT | \
                                         P1_23_UNUSED_BIT | \
                                         0 )


#define P0IO_ONE_BITS        (uint32_t) ( \
                                         LED1_BIT | \
				         BOOT_BIT | \
                                         LED2_BIT | \
					 P0_30_LED_R | \
				         P0_31_LED_G | \
                                         0 )

#define P1IO_ONE_BITS        (uint32_t) ( \
					 P1_24_COL369H | \
					 P1_25_COL2580 | \
					 P1_26_COL147S | \
                                         0 )

#define P0IO_OUTPUT_BITS     (uint32_t) ( \
                                         P0IO_ZERO_BITS | \
                                         P0IO_ONE_BITS )

#define P1IO_OUTPUT_BITS     (uint32_t) ( \
                                         P1IO_ZERO_BITS | \
                                         P1IO_ONE_BITS )


/***************************************************************************/
/* io functions */
#define LED_GP			LED1_BIT  /* GENREAL PURPOSE LED */
#define LED_ERR			LED2_BIT

#define LED_YELLOW		LED1_BIT
#define LED_RED			LED2_BIT
#define LED_KEYPAD_RED		P0_30_LED_R
#define LED_KEYPAD_YELLOW	P0_31_LED_G

/***************************************************************************/
/* io functions */
#define KBD_PORT		IO1
#define IN_PORT			IO0
#define LED_PORT		IO0
#define OUT_PORT		IO1

#define CREATE_PORT_NAME_PIN(port) port##PIN
#define CREATE_PORT_NAME_CLR(port) port##CLR
#define CREATE_PORT_NAME_SET(port) port##SET

#define GET_IN_PIN(port,in)	((CREATE_PORT_NAME_PIN(port) & in)?1:0)	
#define SET_OUT_PIN(port,out)   (CREATE_PORT_NAME_SET(port)=out)
#define CLR_OUT_PIN(port,out)   (CREATE_PORT_NAME_CLR(port)=out)

/***************************************************************************/
/* watchdog */
#define WATCHDOG_ENABLED
#define WATCHDOG_TIMEOUT_MS	1000

/***************************************************************************/
/* uLan configuration */
#ifdef UL_LOG_ENABLE
  #undef UL_LOG_ENABLE
#endif

#ifdef ULD_DEFAULT_BUFFER_SIZE
  #undef ULD_DEFAULT_BUFFER_SIZE
  #define ULD_DEFAULT_BUFFER_SIZE 0x0800
#endif

#define UL_DRV_SYSLESS_PORT 0xE0010000
#define UL_DRV_SYSLESS_BAUD 19200
#define UL_DRV_SYSLESS_IRQ HAL_INTERRUPT_UART1
#define UL_DRV_SYSLESS_MY_ADR_DEFAULT 1

#define watchdog_feed lpc_watchdog_feed
#define kvpb_erase lpcisp_kvpb_erase
#define kvpb_copy lpcisp_kvpb_copy
#define kvpb_flush lpcisp_kvpb_flush
#define KVPB_DEFAULT_FLAGS KVPB_DESC_DOUBLE|KVPB_DESC_CHUNKWO

#define HAL_ARM_LPC2XXX_EXTINT_ERRATA 

/***************************************************************************/
/* kbd */
#define KEY_TIMER sys_timer_ticks
#define KBDDEVICE void

typedef unsigned short kbd_key_t;
typedef unsigned int kbd_keymod_t;
typedef long kbd_interval_t;
typedef unsigned short kbd_scan_code_t;

#define KEY_DEFAULT_TIMES
#define KEY_PUSH_T	70
#define KEY_RELEASE_T	50
#define KEY_REPFIRST_T	60000
#define KEY_REPNEXT_T	300

#define KBD_DR _reg_PTD_DR
#define KBD_SSR IO1PIN
#define KBD_DDIR _reg_PTD_DDIR
#define KBD_PUEN _reg_PTD_PUEN

typedef unsigned long kbdisr_lock_level_t;
#define kbdisr_lock   save_and_cli
#define	kbdisr_unlock restore_flags

#define KBD_SCAN_CNT  3
#define KBD_SCAN_BIT0 24
#define KBD_RET_CNT   5
#define KBD_RET_BIT0  27

#define KBD_SCAN_MASK (((1<<KBD_SCAN_CNT)-1)<<KBD_SCAN_BIT0)
#define KBD_RET_MASK  (((1<<KBD_RET_CNT)-1)<<KBD_RET_BIT0)

#define KBD_USE_IO_SETCLR_OPS

#endif /* _SYSTEM_DEF_H_ */
