/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  system_def.h - common cover for definition of hardware adresses,
                 registers, timing and other hardware dependant
		 parts of embedded hardware
 
  Copyright (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#ifndef _SYSTEM_DEF_H_
#define _SYSTEM_DEF_H_

#include <stdint.h>
#include <system_stub.h>
#include <LPC214x.h>
#include <bspbase.h>

#ifndef NULL
#define	NULL	0
#endif

#define WITH_SFI_SEL

#ifndef VER_CODE
#define VER_CODE(major,minor,patch) (major*0x10000+minor*0x100+patch)
#endif
/* Software version */
#define SW_VER_ID	"UL_USB"
#define SW_VER_MAJOR	0
#define SW_VER_MINOR	2
#define SW_VER_PATCH	0
#define SW_VER_CODE	VER_CODE(SW_VER_MAJOR,SW_VER_MINOR,SW_VER_PATCH)
/* Hardware version */
#define HW_VER_ID	"UL_USB"
#define HW_VER_MAJOR	1
#define HW_VER_MINOR	0
#define HW_VER_PATCH	0
#define HW_VER_CODE	VER_CODE(HW_VER_MAJOR,HW_VER_MINOR,HW_VER_PATCH)
/* Version of mechanical  */
#define MECH_VER_ID     "UL_USB"
#define MECH_VER_MAJOR  0
#define MECH_VER_MINOR  0
#define MECH_VER_PATCH  0
#define MECH_VER_CODE	VER_CODE(MECH_VER_MAJOR,MECH_VER_MINOR,MECH_VER_PATCH)


// PLL setup values are computed within the LPC include file
// It relies upon the following defines
//#define FOSC                (11059200)  // Master Oscillator Freq.
#define FOSC                (12000000)  // Master Oscillator Freq.
#define PLL_MUL             (4)         // PLL Multiplier
#define CCLK                (FOSC * PLL_MUL) // CPU Clock Freq.

// Pheripheral Bus Speed Divider
#define PBSD                1           // MUST BE 1, 2, or 4
#define PCLK                (CCLK / PBSD) // Pheripheal Bus Clock Freq.

#define SYS_TIMER_HZ 	    1000

#ifndef BIT
#define BIT(n)              (1 << (n))
#endif

// Port Bit Definitions & Macros:    Description - initial conditions
#define TXD0_BIT            BIT(0)      // used by UART0
#define RXD0_BIT            BIT(1)      // used by UART0
#define P0_SCL0_PIN         BIT(2)      // I2C 0 SCL
#define P0_SDA0_PIN         BIT(3)      // I2C 0 SDA
#define P0_SCK0_PIN         BIT(4)      // SPI0 clocks
#define P0_MISO0_PIN        BIT(5)      // SPI0 master input
#define P0_MOSI0_PIN        BIT(6)      // SPI0 master output
#define P0_SSEL0_PIN        BIT(7)      // SPI0 external select/ADS1218 DRDY
#define TXD1_BIT            BIT(8)      // used by UART1
#define RXD1_BIT            BIT(9)      // used by UART1
#define RTS1_BIT            BIT(10)     // used by UART1
#define CTS1_BIT            BIT(11)     // used by UART1
#define DSR1_BIT            BIT(12)     // used by UART1
#define LED2_BIT            BIT(13)     // used by LED
#define BOOT_BIT            BIT(14)     // SWITCH
#define LED1_BIT            BIT(15)     // used by LED
#define P0_SWITCH1_PIN      BIT(16)     // pin connected to the switch 1
#define P0_SCK1_PIN         BIT(17)     // SPI1 clocks
#define P0_MISO1_PIN        BIT(18)     // SPI1 master input
#define P0_MOSI1_PIN        BIT(19)     // SPI1 master output
#define P0_SSEL1_PIN        BIT(20)     // SPI1 slave select/25VF016 chipselect
#define P0_21_UNUSED_BIT    BIT(21)     // P0.21 unused - low output
#define P0_22_UNUSED_BIT    BIT(22)     // P0.22 unused - low output
#define P0_SJA1000_ALE_PIN  BIT(23)     // SJA1000 ALE
#define P0_24_UNUSED_BIT    BIT(24)     // P0.24 unused - low output
#define P0_SJA1000_CS_PIN   BIT(25)     // SJA1000 CS
#define P0_26_UNUSED_BIT    BIT(26)     // P0.26 unused - low output
#define P0_27_UNUSED_BIT    BIT(27)     // P0.27 unused - low output
#define P0_SJA1000_RD_PIN   BIT(28)     // SJA1000 RD
#define P0_SJA1000_WR_PIN   BIT(29)     // SJA1000 WR
#define P0_SJA1000_INT_PIN  BIT(30)     // SJA1000 INT
#define P0_USB_CONNECT_PIN  BIT(31)     // USB Connect Control

#define P1_SJA1000_D0_PIN   BIT(16)     // SJA1000 D0
#define P1_SJA1000_D1_PIN   BIT(17)     // SJA1000 D1
#define P1_SJA1000_D2_PIN   BIT(18)     // SJA1000 D2
#define P1_SJA1000_D3_PIN   BIT(19)     // SJA1000 D3
#define P1_SJA1000_D4_PIN   BIT(20)     // SJA1000 D4
#define P1_SJA1000_D5_PIN   BIT(21)     // SJA1000 D5
#define P1_SJA1000_D6_PIN   BIT(22)     // SJA1000 D6
#define P1_SJA1000_D7_PIN   BIT(23)     // SJA1000 D7
#define P1_OUT_PORT_CS_PIN  BIT(24)     // Chip select for 74HC574 chip
#define P1_SJA1000_RST_PIN  BIT(25)     // SJA1000 RST
#define P1_26_UNUSED_BIT    BIT(26)     // used by JTAG
#define P1_27_UNUSED_BIT    BIT(27)     // used by JTAG
#define P1_28_UNUSED_BIT    BIT(28)     // used by JTAG
#define P1_29_UNUSED_BIT    BIT(29)     // used by JTAG
#define P1_30_UNUSED_BIT    BIT(30)     // used by JTAG
#define P1_31_UNUSED_BIT    BIT(31)     // used by JTAG

#define P1_SJA1000_DATA_PINS   (uint32_t) ( \
                                         P1_SJA1000_D0_PIN | \
                                         P1_SJA1000_D1_PIN | \
                                         P1_SJA1000_D2_PIN | \
                                         P1_SJA1000_D3_PIN | \
                                         P1_SJA1000_D4_PIN | \
                                         P1_SJA1000_D5_PIN | \
                                         P1_SJA1000_D6_PIN | \
                                         P1_SJA1000_D7_PIN | \
                                         0 )

#define P0IO_INPUT_BITS      (uint32_t) ( \
                                         P0_SCL0_PIN | \
                                         P0_SDA0_PIN | \
                                         P0_MISO0_PIN | \
                                         P0_SSEL0_PIN | \
                                         P0_MISO1_PIN | \
					 BOOT_BIT | \
                                         P0_SWITCH1_PIN | \
                                         P0_SJA1000_INT_PIN | \
                                         0 )

#define P1IO_INPUT_BITS      (uint32_t) ( \
                                         P1_26_UNUSED_BIT | \
                                         P1_27_UNUSED_BIT | \
                                         P1_28_UNUSED_BIT | \
                                         P1_29_UNUSED_BIT | \
                                         P1_30_UNUSED_BIT | \
                                         P1_31_UNUSED_BIT | \
                                         0 )

#define P0IO_ZERO_BITS       (uint32_t) ( \
                                         P0_21_UNUSED_BIT | \
                                         P0_22_UNUSED_BIT | \
                                         P0_24_UNUSED_BIT | \
                                         P0_26_UNUSED_BIT | \
                                         P0_27_UNUSED_BIT | \
                                         P0_USB_CONNECT_PIN | \
                                         0 )

#define P1IO_ZERO_BITS       (uint32_t) ( \
                                         P1_SJA1000_DATA_PINS | \
                                         P1_SJA1000_RST_PIN | \
                                         0 )


#define P0IO_ONE_BITS        (uint32_t) ( \
                                         P0_SCK0_PIN | \
                                         P0_MOSI0_PIN | \
                                         LED1_BIT | \
				         BOOT_BIT | \
                                         LED2_BIT | \
                                         P0_SCK1_PIN | \
                                         P0_MOSI1_PIN | \
                                         P0_SSEL1_PIN | \
					 P0_SJA1000_ALE_PIN | \
                                         P0_SJA1000_CS_PIN | \
                                         P0_SJA1000_RD_PIN | \
                                         P0_SJA1000_WR_PIN | \
                                         0 )

#define P1IO_ONE_BITS        (uint32_t) ( \
                                         P1_OUT_PORT_CS_PIN | \
                                         0 )

#define P0IO_OUTPUT_BITS     (uint32_t) ( \
                                         P0IO_ZERO_BITS | \
                                         P0IO_ONE_BITS )

#define P1IO_OUTPUT_BITS     (uint32_t) ( \
                                         P1IO_ZERO_BITS | \
                                         P1IO_ONE_BITS )

/***************************************************************************/
/* io functions */
#define LED_GP			LED1_BIT  /* GENREAL PURPOSE LED */
#define LED_ERR			LED2_BIT

#define LED_YELLOW		LED1_BIT
#define LED_RED			LED2_BIT

/***************************************************************************/
/* io functions */
#define IN_PORT			IO0
#define LED_PORT		IO0
#define OUT_PORT		IO1

#define CREATE_PORT_NAME_PIN(port) port##PIN
#define CREATE_PORT_NAME_CLR(port) port##CLR
#define CREATE_PORT_NAME_SET(port) port##SET

#define GET_IN_PIN(port,in)	((CREATE_PORT_NAME_PIN(port) & in)?1:0)	
#define SET_OUT_PIN(port,out)   (CREATE_PORT_NAME_SET(port)=out)
#define CLR_OUT_PIN(port,out)   (CREATE_PORT_NAME_CLR(port)=out)

/***************************************************************************/
/* watchdog */
//#define WATCHDOG_ENABLED
#define WATCHDOG_TIMEOUT_MS	1000

/***************************************************************************/
/* uLan configuration */

#ifdef UL_LOG_ENABLE
  #undef UL_LOG_ENABLE
#endif

#ifdef ULD_DEFAULT_BUFFER_SIZE
  #undef ULD_DEFAULT_BUFFER_SIZE
  #define ULD_DEFAULT_BUFFER_SIZE 0x0800
#endif

#define UL_DRV_SYSLESS_PORT 0xE0010000
#define UL_DRV_SYSLESS_BAUD 19200
#define UL_DRV_SYSLESS_IRQ HAL_INTERRUPT_UART1
#define UL_DRV_SYSLESS_MY_ADR_DEFAULT 1

#define watchdog_feed lpc_watchdog_feed
#define kvpb_erase lpcisp_kvpb_erase
#define kvpb_copy lpcisp_kvpb_copy
#define kvpb_flush lpcisp_kvpb_flush
#define KVPB_DEFAULT_FLAGS KVPB_DESC_DOUBLE|KVPB_DESC_CHUNKWO

/***************************************************************************/
/* USB configuration */
#define USB_WITH_UDEV_FNC
#define USB_EP_NUM          32
#define USB_MAX_PACKET0     64
#define USB_MAX_PACKET      8
#define USB_DMA_EP          0x00000000

/***************************************************************************/
/* i2c0 configuration */
#define I2C_DRV_SYSLESS_IRQ HAL_INTERRUPT_I2C0
#define I2C_DRV_SYSLESS_PORT 0xE001C000
#define I2C_DRV_SYSLESS_BITRATE 100000
#define I2C_DRV_SYSLESS_SLADR 0

/***************************************************************************/
/* Constants for JTAG and supply control port pins: */
#define JTAGIN		(IO0PIN)	// Control ports are on P5.x
#define JTAGSET		(IO0SET)
#define JTAGCLR		(IO0CLR)
#define	JTAGDIR		(IO0DIR)
#define	JTAGOUT_RDBACK	(IO0PIN)	// used to read actual TCLK output value
#undef	JTAGSEL
#define TMS_PIN		(1<<20)	// P0.20 JTAG TMS input pin
#define TDI_PIN		(1<<19)	// P0.19 JTAG TDI input pin  (SIMO1 if SPI mode)
#define TDO_PIN		(1<<18)	// P0.18 JTAG TDO output pin (SOMI1 if SPI mode) 
#define TCK_PIN		(1<<17)	// P0.17 JTAG TCK input pin  (UCLK1 if SPI mode) 
#define TDICTRL2_PIN	0	// Px.x switch TDO to TDI
#define TDICTRL1_PIN	0	// Px.x connects TDI
#define TEST_PIN	0	// Px.x TEST pin (20 & 28-pin devices only)
#define VCCTGT_PIN	0	// Px.x Supply voltage of target board
#define TCLK_PIN	TDI_PIN	// P7.3 TDI (former XOUT) receives TCLK

#define ClrTMS()	do {(JTAGCLR) |= (TMS_PIN);} while(0);
#define SetTMS()	do {(JTAGSET) |= (TMS_PIN);} while(0);
#define ClrTDI()	do {(JTAGCLR) |= (TDI_PIN);} while(0);
#define SetTDI()	do {(JTAGSET) |= (TDI_PIN);} while(0);
#define ClrTCK()	do {(JTAGCLR) |= (TCK_PIN);} while(0);
#define SetTCK()	do {(JTAGSET) |= (TCK_PIN);} while(0);
#define ClrTCLK()	do {(JTAGCLR) |= (TCLK_PIN);} while(0);
#define SetTCLK()	do {(JTAGSET) |= (TCLK_PIN);} while(0);
#define StoreTCLK()	((JTAGOUT_RDBACK & TCLK_PIN) ? 1:0)
#define RestoreTCLK(x)	(x == 0 ? (JTAGCLR |= TCLK_PIN) : (JTAGSET |= TCLK_PIN))
#define ScanTDO()	((JTAGIN & TDO_PIN))	// assumes TDO to be bit0

#define JTAG_IODELAY() __asm__ __volatile__ ("nop\n\tnop\n\tnop\n\tnop\n\tnop\n\tnop\n\tnop\n\tnop\n\tnop\n": : )

/****************************************************************************/
/* Define section for user, related to the controller used (here MSP430F149)*/
/****************************************************************************/

// Constants for Error LED control port:
#define JTAGLEDSET	(IO0SET) // LED ports are P0.x
#define JTAGLEDCLR	(IO0CLR) // LED ports are P0.x
#define	JTAGLEDDIR	(IO0DIR)
#undef	JTAGLEDSEL	/*P1SEL*/
#define JTAGLEDRED	(1<<13)	// P0.13 Red LED (ERROR)
#define JTAGLEDGREEN	(1<<15)	// P0.15 Green LED (OK)

/***************************************************************************/
/* PBMaster configuration */
#define PBM_8250_PORT   0xE0010000
#define PBM_8250_BAUD   19200
#define PBM_8250_IRQ    HAL_INTERRUPT_UART1

#endif /* _SYSTEM_DEF_H_ */
