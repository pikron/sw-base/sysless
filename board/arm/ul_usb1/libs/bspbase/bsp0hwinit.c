#include "local_config.h"
#include <system_def.h>
#ifdef CONFIG_KEYVAL
  #include <keyvalpb.h>
  #include <lpciap.h>
  #include <lpciap_kvpb.h>
#endif /* CONFIG_KEYVAL */
#ifdef CONFIG_STDIO_COM_PORT
  #include <uart.h>
#endif
#ifdef CONFIG_OC_UL_DRV_SYSLESS
  #include <ul_lib/ulan.h>
  #include <string.h>
  #include <ul_drv_init.h>
  #include <ul_drv_iac.h>
  #include <ul_lib/ul_drvdef.h>
  extern long int uld_jiffies;
#endif /* CONFIG_OC_UL_DRV_SYSLESS */
#ifdef CONFIG_OC_I2C_DRV_SYSLESS
  #include <i2c_drv.h>
#endif /* CONFIG_OC_I2C_DRV_SYSLESS */
#ifdef CONFIG_OC_PBM_DRV
  #include <pbmcore.h>
  #include <pbm_8250.h>
  #include <pbm_drv_init.h>
#endif /* CONFIG_OC_PBM_DRV */
#include <hal_machperiph.h>
#include <hal_intr.h>

/* timers */
volatile lt_ticks_t sys_timer_ticks;

#ifdef CONFIG_OC_I2C_DRV_SYSLESS
#define I2C_DRV_NA_MSTIMEOUT    10
i2c_drv_t i2c_drv;
int i2c_drv_na_timer=0;
#endif /* CONFIG_OC_I2C_DRV_SYSLESS */

static void sysInit(void) 
{

  system_clock_init();

  // setup the parallel port pin
  IO0CLR = P0IO_ZERO_BITS;                // clear the ZEROs output
  IO0SET = P0IO_ONE_BITS;                 // set the ONEs output
  IO0DIR = P0IO_OUTPUT_BITS;              // set the output bit direction

 #ifdef P1IO_OUTPUT_BITS
  IO1CLR = P1IO_ZERO_BITS;                // clear the ZEROs output
  IO1SET = P1IO_ONE_BITS;                 // set the ONEs output
  IO1DIR = P1IO_OUTPUT_BITS;              // set the output bit direction
 #endif

  PINSEL1 = (PINSEL1 & 0x3FFFFFFF);

  IO0CLR = LED1_BIT;                      // Indicate functional state on the LED1
}

void timer0_isr(void)
{   
  unsigned int ir;
  ir=T0IR;
  if (ir&TIR_MR0I) {
    do {
      T0MR0+=PCLK/SYS_TIMER_HZ;
      T0IR=TIR_MR0I;               // Clear match0 interrupt
     #ifdef CONFIG_OC_UL_DRV_SYSLESS
      uld_jiffies++;
     #endif
     #ifdef CONFIG_OC_PBM_DRV
      pbm_jiffies++;
     #endif
     #ifdef CONFIG_OC_I2C_DRV_SYSLESS
      if (i2c_drv.flags&I2C_DRV_MS_INPR) {
        if (i2c_drv.flags&I2C_DRV_NA) {
          i2c_drv_na_timer++;
          if (i2c_drv_na_timer>I2C_DRV_NA_MSTIMEOUT) {
             if (i2c_drv.stroke_fnc) 
              i2c_drv.stroke_fnc(&i2c_drv);
            i2c_drv_na_timer=0;
          }
        } else {
          i2c_drv_na_timer=0;
        }
        i2c_drv.flags|=I2C_DRV_NA;
      }
     #endif
      sys_timer_ticks++;
    } while (((int32_t)(T0MR0-T0TC))<0);
  }
}

void timerInit(void)
{
  sys_timer_ticks=0;

  HAL_INTERRUPT_ATTACH(HAL_INTERRUPT_TIMER0,timer0_isr,0);
  HAL_INTERRUPT_UNMASK(HAL_INTERRUPT_TIMER0);
    
  T0TC=0;
  T0MCR=0;

  T0MR0=PCLK/SYS_TIMER_HZ;			      /* ms tics */
  T0MCR|=TMCR_MR0_I;

  T0TCR = TCR_ENABLE; //Run timer 0
}

#ifdef CONFIG_STDIO_COM_PORT

int uartcon_write(int file, const char * ptr, int len)
{
  int cnt;
  unsigned char ch;
  for(cnt=0;cnt<len;cnt++,ptr++){
    ch=*ptr;
    if(ch==0xa)
      uart0Putch(0xd);
    uart0Putch(ch);
  }
  return cnt;
}

void init_system_stub(void) {
  system_stub_ops.write=uartcon_write;
}

#endif /* CONFIG_STDIO_COM_PORT */

#ifdef CONFIG_OC_UL_DRV_SYSLESS

extern unsigned uld_debug_flg; /* Left application  set defaults */

#ifndef CONFIG_KEYVAL
unsigned long lpciap_buff[ISP_RAM2FLASH_BLOCK_SIZE/4];
#endif /* CONFIG_KEYVAL */

#define UL_MTYPE_START32BIT 0x100

static inline int ul_iac_mem_head_rd(uint8_t *buf, int len,
                      uint32_t* pmtype, uint32_t* pstart, uint32_t* plen)
{
  uint32_t val, mtype;
  if (len<6) return -1;
  mtype=*(buf++);       /* memory type */
  mtype+=*(buf++)<<8;
  val=*(buf++);           /* start address */
  val+=*(buf++)<<8;
  if(mtype&UL_MTYPE_START32BIT){
    if (len<8) return -1;
    val+=(uint32_t)*(buf++)<<16;
    val+=(uint32_t)*(buf++)<<24;
  }
  *pstart=val;
  val=*(buf++);           /* length */
  val+=*(buf++)<<8;
  if(mtype&UL_MTYPE_START32BIT){
    if (len==10) {
      val+=(uint32_t)*(buf++)<<16;
      val+=(uint32_t)*(buf++)<<24;
    }
  }
  *plen=val;
  mtype&=~UL_MTYPE_START32BIT;       /* 32-bit start address */
  *pmtype=mtype;
  return 0;
}

int ul_iac_call_rdm(struct ul_drv *udrv,ul_msginfo *msginfo,char *ibuff,ul_iac_data *data)
{
  uint32_t mtype,start,len;

  data->len=0;

  if(ul_iac_mem_head_rd((uint8_t *)ibuff, msginfo->len,&mtype,&start,&len))
    return UL_IAC_RC_PROC;

  if (mtype==0x00) {
    data->len=len;
    data->buff=(char*)start;
    return UL_IAC_RC_FREEMSG;
  }
  return UL_IAC_RC_PROC;
}

int ul_iac_call_erm(struct ul_drv *udrv,ul_msginfo *msginfo,char *ibuff,ul_iac_data *data)
{
  uint32_t mtype,start,len;

  data->len=0;

  if(ul_iac_mem_head_rd((uint8_t *)ibuff, msginfo->len,&mtype,&start,&len))
    return UL_IAC_RC_PROC;

 #ifdef CONFIG_KEYVAL
  if (mtype==0x01) {
    lpcisp_erase((void*)start,len);
    data->len=0;
    return UL_IAC_RC_FREEMSG;
  }
 #endif /* CONFIG_KEYVAL */
  return UL_IAC_RC_PROC;
}

int ul_iac_call_wrm(struct ul_drv *udrv,ul_msginfo *msginfo,char *ibuff,ul_iac_data *data)
{
  uint32_t mtype,start,len;

  data->len=0;

  if(ul_iac_mem_head_rd((uint8_t *)ibuff, msginfo->len,&mtype,&start,&len))
    return UL_IAC_RC_PROC;

  if (mtype==0x00) {
    memcpy((void*)start,data->buff,data->len);
    return UL_IAC_RC_FREEMSG;
  }
 #ifdef CONFIG_KEYVAL
  if (mtype==0x01) {
    lpcisp_write((char*)start, data->buff, ISP_RAM2FLASH_BLOCK_SIZE);
    return UL_IAC_RC_FREEMSG;
  }
 #endif /* CONFIG_KEYVAL */
  return UL_IAC_RC_PROC;
}


int ul_iac_call_deb(struct ul_drv *udrv,ul_msginfo *msginfo,char *ibuff,ul_iac_data *data)
{
  uint32_t debcmd,mtype,start;
  uint8_t *p=(uint8_t*)ibuff;

  if (msginfo->len<1) return UL_IAC_RC_PROC;
  debcmd=*(p++);
  switch (debcmd) {
    case 0x10: /* goto */
      data->len=0;
      if (msginfo->len<5) return UL_IAC_RC_PROC;
      mtype=*(p++);
      mtype+=*(p++)<<8;
      start=*(p++);
      start+=*(p++)<<8;
      if(mtype&UL_MTYPE_START32BIT){
        mtype&=~UL_MTYPE_START32BIT;
        if (msginfo->len<7) return UL_IAC_RC_PROC;
        start+=(uint32_t)*(p++)<<16;
        start+=(uint32_t)*(p++)<<24;
      }
      if (mtype==0x00)
        ((void (*)())start)();
    default:break;
  }
  return UL_IAC_RC_PROC;
}

int ul_iac_call_res(struct ul_drv *udrv,ul_msginfo *msginfo,char *ibuff,ul_iac_data *data)
{
  uint32_t rescmd,pass;
  uint8_t *p=(uint8_t*)ibuff;

  if (msginfo->len<1) return UL_IAC_RC_PROC;
  rescmd=*(p++);
  switch (rescmd) {
    case ULRES_CPU: /* CPU */
      data->len=0;
      if (msginfo->len<3) return UL_IAC_RC_PROC;
      pass=*(p++);
      pass+=*(p++)<<8;
      if (pass==0xaa55) {
        MEMMAP=MEMMAP_FLASH;
        lpc_watchdog_init(1,10); /* 10ms */
        lpc_watchdog_feed();
        while(1);
      }
    default:break;
  }
  return UL_IAC_RC_PROC;
}

int uLanInit()
{
  struct ul_drv *udrv;

  /* set rs485 mode for UART1 */
  PINSEL0 = (PINSEL0 & ~0xFFFF0000) | 0x01550000; /* dsr(txd), cts(rxd), rts(rs485_dir), rxd, txd */

  udrv=ul_drv_new(UL_DRV_SYSLESS_PORT,	    /* port */
             UL_DRV_SYSLESS_IRQ,            /* irq */
	     UL_DRV_SYSLESS_BAUD,	    /* baud */
	     UL_DRV_SYSLESS_MY_ADR_DEFAULT, /* my adr */
	#ifdef CONFIG_OC_UL_DRV_U450_VARPINS
	  #if defined(CONFIG_OC_UL_DRV_U450_VARPINS_DIRNEG) && defined(CONFIG_OC_UL_DRV_U450_VARPINS_MSRSWAP)
	     "16450-dirneg-msrswap",	    /* chip name */
	  #elif defined(CONFIG_OC_UL_DRV_U450_VARPINS_MSRSWAP)
	     "16450-msrswap",		    /* chip name */
	  #elif defined(CONFIG_OC_UL_DRV_U450_VARPINS_DIRNEG)
	     "16450-dirneg",		    /* chip name */
          #else
	     "16450",		    	    /* chip name */
          #endif
	#else /*CONFIG_OC_UL_DRV_U450_VARPINS*/
	     "16450",		    	    /* chip name */
	#endif /*CONFIG_OC_UL_DRV_U450_VARPINS*/
	     0);                            /* baud base - default */

  if (udrv==NULL)
    return -1;

  ul_drv_add_iac(udrv,UL_CMD_RDM,UL_IAC_OP_SND,ul_iac_call_rdm,NULL,0,0,NULL,0);
  ul_drv_add_iac(udrv,UL_CMD_ERM,UL_IAC_OP_CALLBACK,ul_iac_call_erm,NULL,0,UL_IAC_BFL_CB_OFFLT,NULL,0);
  ul_drv_add_iac(udrv,UL_CMD_WRM,UL_IAC_OP_REC,ul_iac_call_wrm,(char*)lpciap_buff,0,UL_IAC_BFL_CB_OFFLT,NULL,0);
  ul_drv_add_iac(udrv,UL_CMD_DEB,UL_IAC_OP_CALLBACK,ul_iac_call_deb,NULL,0,UL_IAC_BFL_CB_OFFLT,NULL,0); 
  ul_drv_add_iac(udrv,UL_CMD_RES,UL_IAC_OP_CALLBACK,ul_iac_call_res,NULL,0,UL_IAC_BFL_CB_OFFLT,NULL,0); 

  return ul_drv_add_dev(udrv);
}
#endif /* CONFIG_OC_UL_DRV_SYSLESS */

#ifdef CONFIG_OC_I2C_DRV_SYSLESS

int
i2cInit(void)
{

  /* set io pins */
 #if (I2C_DRV_SYSLESS_IRQ==9)
  PINSEL0 = (PINSEL0 & ~0x000000F0) | 0x00000050; /* I2C0 - SCL0, SDA0 */
 #elif (I2C_DRV_SYSLESS_IRQ==19)
  PINSEL0 = (PINSEL0 & ~0x30C00000) | 0x30C00000; /* I2C1 - SCL1, SDA1 */
 #else
   #error "wrong I2C pin maping!"
 #endif

  if (i2c_drv_init(&i2c_drv, 
               I2C_DRV_SYSLESS_PORT,
               I2C_DRV_SYSLESS_IRQ,
	       I2C_DRV_SYSLESS_BITRATE,
               I2C_DRV_SYSLESS_SLADR)<0) return -1;

  return 1;
}

#endif /*CONFIG_OC_I2C_DRV_SYSLESS*/

#ifdef CONFIG_OC_PBM_DRV

void pbm_drv_init()
{
	int rv = 0;

	/* set rs485 mode for UART1 */
	/* dsr(txd), cts(rxd), rts(rs485_dir), rxd, txd */
	PINSEL0 = (PINSEL0 & ~0xFFFF0000) | 0x01550000;

	PBMCHIP_INFO(PBMCHIP_DRV_DESCRIPTION ", "
			PBMCHIP_DRV_VERSION ", "
			PBMCHIP_DRV_COPYRIGHT "\n");

	pbm_dev = pbm_alloc_dev(PBM_UART);
	if (!pbm_dev)
		PBM_PRINT("unable to allocate device\n");

	PBM_LOCK_INIT(&pbm_dev->lock);
	PBM_LOCK_INIT(&pbm_dev->irq_lock);

	pbm_dev->node_res = &pbm_8250_res;
	pbm_8250_res.ioport = PBM_8250_PORT;
	pbm_8250_res.irq = PBM_8250_IRQ;
	PBMCHIP_INFO("port=0x%04x irq=%d\n",
			pbm_8250_res.ioport, pbm_8250_res.irq);

	/* disable all interrupts */
	pbm_outb(pbm_dev, UART_IER, 0x00);

	/* get default parameters*/
	pbm_8250_get_params(pbm_dev, 0);
	/* chip operations and registering to pbmcore */
	pbm_dev->chops = chops;
	rv = pbm_register_chip(pbm_dev);
	if (rv)
		PBM_PRINT("unable to register chip\n");

	rv = request_irq(pbm_8250_res.irq, pbm_8250_intr, 0, "pbm_8250", pbm_dev);

	pbm_8250_int_off_all(pbm_dev);
	pbm_8250_int_on_all(pbm_dev);

	/* a hack to generate THRE interrupt on LPC2148 */
	pbm_outb(pbm_dev, UART_TX, 0x00);
}

#endif /* CONFIG_OC_PBM_DRV */

void _setup_board()
{
  // initialize the system
  sysInit();				    

  #ifdef WATCHDOG_ENABLED
    lpc_watchdog_init(1,WATCHDOG_TIMEOUT_MS);
    lpc_watchdog_feed();
  #endif /* WATCHDOG_ENABLED */

  // initialize the system timer
  timerInit();

  #ifdef CONFIG_STDIO_COM_PORT
    uart0Init( B57600 , UART_8N1, UART_FIFO_8); 
    init_system_stub();
  #endif /* CONFIG_STDIO_COM_PORT */

  #ifdef CONFIG_OC_UL_DRV_SYSLESS
//    uld_debug_flg=0x3ff;
    uLanInit();
  #endif /* CONFIG_OC_UL_DRV_SYSLESS */
  
  #ifdef CONFIG_OC_I2C_DRV_SYSLESS
    i2cInit();
  #endif /* CONFIG_OC_I2C_DRV_SYSLESS */

  #ifdef CONFIG_OC_PBM_DRV
    pbm_drv_init();
  #endif /* CONFIG_OC_PBM_DRV */

}
