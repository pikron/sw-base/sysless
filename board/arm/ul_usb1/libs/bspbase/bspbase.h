#ifndef _BSPBASE_H
#define _BSPBASE_H

#include <stdint.h>
#include <lt_timer_types.h>

extern volatile lt_ticks_t sys_timer_ticks;

#define get_sys_timer_ticks() sys_timer_ticks

#endif /* _BSPBASE_H */
