#include <cpu_def.h>
#include "kbd.h"

unsigned char kbd_onerow(unsigned char scan)
{
	kbdisr_lock_level_t level;
	unsigned int scan_mask=KBD_SCAN_MASK;
	unsigned int scan_val;
	unsigned int ret;
	int delay=10;

	kbdisr_lock(level);

       #ifdef KBD_USE_IO_SETCLR_OPS
	scan_val=(scan<<KBD_SCAN_BIT0) & scan_mask;
        SET_OUT_PIN(KBD_PORT,scan_mask);
        CLR_OUT_PIN(KBD_PORT,scan_val);
       #else	
	scan_val=(~scan<<KBD_SCAN_BIT0) & scan_mask;
	KBD_DR|=scan_val;
	KBD_DR&=scan_val|~scan_mask;
       #endif /* KBD_USE_IO_SETCLR_OPS */
	kbdisr_unlock(level);

	while(delay--)
		ret=KBD_SSR;
	ret=KBD_SSR;

	kbdisr_lock(level);
       #ifdef KBD_USE_IO_SETCLR_OPS
        SET_OUT_PIN(KBD_PORT,scan_mask);
       #else	
	KBD_DR |= scan_mask;
       #endif /* KBD_USE_IO_SETCLR_OPS */
	kbdisr_unlock(level);
	
	return (~ret>>KBD_RET_BIT0)&((1<<KBD_RET_CNT)-1);
}

void kbd_setio(void)
{
	kbdisr_lock_level_t level;
	unsigned int scan_mask=KBD_SCAN_MASK;
       #ifndef KBD_USE_IO_SETCLR_OPS
	unsigned int ret_mask=KBD_RET_MASK;
       #endif /* KBD_USE_IO_SETCLR_OPS */

	kbdisr_lock(level);
	
       #ifdef KBD_USE_IO_SETCLR_OPS
        SET_OUT_PIN(KBD_PORT,scan_mask);
       #else	
	KBD_DR|=scan_mask;
	KBD_DDIR|=scan_mask;
	KBD_DDIR&=~ret_mask;
	KBD_PUEN|=ret_mask;
       #endif /* KBD_USE_IO_SETCLR_OPS */

	kbdisr_unlock(level);
}

const scan2key_t kbd_scan2key_hisc_keypad[]={
	[0x00]={0,0},
	[0x01]={'3',0},
	[0x02]={'6',0},
	[0x03]={'9',0},
	[0x04]={'\r',0},
	[0x05]={'A',0},
	[0x06]={'2',0},
	[0x07]={'5',0},
	[0x08]={'8',0},
	[0x09]={'0',0},
	[0x0a]={'B',0},
	[0x0b]={'1',0},
	[0x0c]={'4',0},
	[0x0d]={'7',0},
	[0x0e]={'*',0},
	[0x0f]={'C',0},
};

const scan2mod_t kbd_scan2mod_hisc_keypad[]={
	{0,0,0,0}
};

scan2key_t *kbd_scan2key_tab=(scan2key_t*)kbd_scan2key_hisc_keypad;
scan2mod_t *kbd_scan2mod_tab=(scan2mod_t*)kbd_scan2mod_hisc_keypad;


