#ifndef _KEYVAL_LOC_H
#define _KEYVAL_LOC_H

extern int _keyval_start;
extern int _keyval_page_len;

#define KEYVAL_START ((unsigned int)&_keyval_start)
#define KEYVAL_PAGE_LEN ((unsigned int)&_keyval_page_len)

#endif  /* _KVPB_LOC */
