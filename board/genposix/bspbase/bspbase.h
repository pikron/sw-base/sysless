#ifndef _BSPBASE_H
#define _BSPBASE_H

#include <stdint.h>
#include <lt_timer_types.h>

lt_ticks_t get_sys_timer_ticks(void);

#endif /* _BSPBASE_H */
