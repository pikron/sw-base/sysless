#include <stdint.h>
#include <lt_timer_types.h>
#include <bspbase.h>
#include <sys/time.h>
#include <string.h>

static lt_ticks_t get_sys_timer_ticks_offs;

lt_ticks_t get_sys_timer_ticks(void)
{
  struct timeval tv_actual;
  lt_ticks_t ticks_actual;
  long int sec;
  gettimeofday(&tv_actual,NULL);
  sec=tv_actual.tv_sec;
  ticks_actual=sec*1000+tv_actual.tv_usec/1000+get_sys_timer_ticks_offs;
  return ticks_actual;
}
