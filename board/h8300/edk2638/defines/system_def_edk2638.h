/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  system_def_edk2638.h - definition of hardware adresses and registers
                         for EDK2638 development board
 
  Copyright (C) 2002 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#ifndef _SYSTEM_DEF_HW01_H_
#define _SYSTEM_DEF_HW01_H_

//#define CPU_REF_HZ  11059200l	/* reference clock frequency      */
//#define CPU_SYS_HZ  11059200l	/* default system clock frequency */
//#define CPU_SYS_HZ 24000000l	/* default system clock frequency */

#define CPU_REF_HZ 18423000l 	/* reference clock for EDK2638    */
#define CPU_SYS_HZ 18423000l	/* default system  for EDK2638    */


unsigned long cpu_ref_hz;	/* actual external XTAL reference */
unsigned long cpu_sys_hz;	/* actual system clock frequency  */

volatile unsigned long msec_time;

#define SCI_RS232_CHAN_DEFAULT 1

/* XRAM 0.5 MB (CS0) */
#define XRAM_START    (volatile uint8_t * const)(0x040000)

#define XRAM_SUPPORT_ENABLED

/* IRAM 16 kB of on-chip memory */
/* 0xffb000-0xffcfff .. 8 kB free */
/* 0xffd000-0xffdfff .. 4 kB for Flash emulation */
/* 0xffe000-0xffffc0 .. 4 kB - 64 B free*/
/* 0xffffc0-0xffffff .. 64 B free*/
#define IRAM_START    (volatile uint8_t * const)(0xffb000)
#define IRAM_START1   (volatile uint8_t * const)(0xffe000)
#define FRAM_START    (volatile uint8_t * const)(0xffffc0)

/* SCI0 - RS232 */
/* SCI1 - RS232 / Boot */
/* SCI2 - x */
/* SCI3 - x */
/* SCI4 - x */

/* IRQ0 - x */
/* IRQ1 - x */
/* IRQ6 - x */

/* Some registers are read only on H8S processors */
/* We use shadow registers for some of them */
#define SHADOW_REG_ALT(_reg,_mask,_xor) \
    (*(_reg)=_reg##_shadow=(_reg##_shadow&~(_mask))^(_xor))

#define SHADOW_REG_SET(_reg,_mask) \
    (*(_reg)=_reg##_shadow|=(_mask))

#define SHADOW_REG_CLR(_reg,_mask) \
    (*(_reg)=_reg##_shadow&=~(_mask))

#define SHADOW_REG_RD(_reg) \
    (_reg##_shadow)

#define SHADOW_REG_WR(_reg,_val) \
    (*(_reg)=_reg##_shadow=(_val))

uint8_t DIO_P1DDR_shadow;
uint8_t DIO_P3DDR_shadow;
uint8_t DIO_PFDDR_shadow;
uint8_t DIO_PJDDR_shadow;

#define DEB_LED_INIT() \
         do {\
	*DIO_P1DR &= ~(P1DR_P14DRm | P1DR_P15DRm);\
	    SHADOW_REG_SET(DIO_P1DDR,(P1DDR_P15DDRm|P1DDR_P14DDRm)); /* set P1.5 and P1.4 as output */ \
	} while (0)
	
#define DEB_LED_ON(num) \
  (*DIO_P1DR |= (P1DR_P14DRm << (num)) & (P1DR_P14DRm | P1DR_P15DRm))
#define DEB_LED_OFF(num) \
  (*DIO_P1DR &=~(P1DR_P14DRm << (num)) | ~(P1DR_P14DRm | P1DR_P15DRm))

#endif /* _SYSTEM_DEF_HW01_H_ */
