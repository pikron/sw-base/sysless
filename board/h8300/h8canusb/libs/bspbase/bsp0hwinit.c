/* procesor H8S/2638 ver 1.1  */
#include <stdint.h>
#include <cpu_def.h>
#include <h8s2638h.h>
#include <system_def.h>
#include <string.h>
#include <boot_fn.h>

#ifdef XRAM_SUPPORT_ENABLED
#define FULL_XRAM_ADRBUS
#endif /*XRAM_SUPPORT_ENABLED*/
#define SMALL_ADRBUS 8

static void deb_led_out(char val)
{
  if (val&1)
    DEB_LED_ON(0);
  else
    DEB_LED_OFF(0);
  if (val&2)
    DEB_LED_ON(1);
  else
    DEB_LED_OFF(1);
  if (val&4)
    DEB_LED_ON(2);
  else
    DEB_LED_OFF(2);
  if (val&8)
    DEB_LED_ON(3);
  else
    DEB_LED_OFF(3);
}

/* Provided by linker script */
extern char __boot_fn_load;
extern char __boot_fn_start;
extern char __boot_fn_end;

static void relocate_boot_fn()
{
  size_t reloc_size=&__boot_fn_end-&__boot_fn_start;

  if(&__boot_fn_load != &__boot_fn_start) {
    memcpy(&__boot_fn_start,&__boot_fn_load,reloc_size);
  }
}

void _setup_board()
{
  //int i, j;// POE-100

#if 1 /* registers setup */
  /* Internal RAM enabled, advanced interrupt mode */
  /* *SYS_SYSCR = 1*SYSCR_RAMEm | 1*SYSCR_INTM1m ; */

  /* Remap 4kB of RAM from 0xffd000-0xffdfff to 0x0-0xfff */
  /* *FLM_RAMER= 1*RAMER_RAMSm | 0&RAMER_RAMxm */
  /* Sideefect - sets Flash software protection */

  /* Enables access to flash control registers */
  *IIC_SCRX |= SCRX_FLSHEm;

  /* set shadow registers */
  DIO_P1DDR_shadow=0;
  DIO_P3DDR_shadow=0;
  DIO_PFDDR_shadow=0;
  DIO_PJDDR_shadow=0;

  DEB_LED_INIT();

  relocate_boot_fn();

  /* show something on debug leds */
  deb_led_out(0);
  FlWait(1*100000);

  SHADOW_REG_SET(DIO_P1DDR,0x03); /* A20 and A21 are outputs */

  *DIO_P3DR=0x09;	/* Inactive value of TxD0 and TxD1 has to be log 1 */
  SHADOW_REG_SET(DIO_P3DDR,0x09); /* TxD0 and TxD1 to outputs */

  /* Setup system clock oscilator */
  /* PLL mode x4, */
  /* *SYS_LPWRCR=2&LPWRCR_STCxm; */
  /* PLL mode x2, */
  /* *SYS_LPWRCR=1&LPWRCR_STCxm; */
  {
    const char clkrat2stc[]={0,0/*1*/,1/*2*/,1,2/*4*/,2,2,2,3/*8*/};
    *SYS_LPWRCR=LPWRCR_STCxm&(LPWRCR_STC0m*
                              clkrat2stc[(CPU_SYS_HZ+CPU_REF_HZ/2)/CPU_REF_HZ]);
  }
  deb_led_out(1);
  FlWait(1*100000);

  /* No clock disable, immediate change, busmaster high-speed */
  *SYS_SCKCR=(0*SCKCR_PSTOPm)|(1*SCKCR_STCSm)|(0&SCKCR_SCKxm);
  // POE-100
#if 0
  /* Setup chipselect outputs CS4 CS5 CS6 */
  *DIO_P7DR |=1|2|4;
  SHADOW_REG_SET(DIO_P7DDR,1|2|4);
#else
  // SHADOW_REG_SET(DIO_P7DDR,0); not on 2638
#endif

  /* Setup chipselect outputs CS3 CS2 CS1 CS0 */
  // *DIO_PGDR |=2|4|8|0x10; no on 2638
#if 0
  SHADOW_REG_SET(DIO_PGDDR,2|4|8|0x10);
#else
  // SHADOW_REG_SET(DIO_PGDDR,2|4); no on 2638
#endif

#if 1
  /* setup chipselect 0 - FLASH */
  *BUS_ABWCR&=~ABWCR_ABW0m;	/* 16 bit width */
  *BUS_ASTCR&=~ASTCR_AST0m;	/* 2 states access */
  //*BUS_ASTCR|=ASTCR_AST0m;	/* 3 states access EDK 2638 */
  *BUS_WCRL&=~(WCRL_W01m|WCRL_W00m);/* 0 additional wait states */

  /* setup chipselect 1 - XRAM */
  *BUS_ABWCR&=~ABWCR_ABW1m;	/* 16 bit width */
  *BUS_ASTCR&=~ASTCR_AST1m;	/* 2 states access */
  *BUS_WCRL&=~(WCRL_W11m|WCRL_W10m);/* 0 additional wait states */

  /* setup chipselect 2 - USB */
  *BUS_ABWCR|=ABWCR_ABW2m;	/* 8 bit width */
  *BUS_ASTCR|=ASTCR_AST2m;	/* 3 states access */
  *BUS_WCRL&=~(WCRL_W21m|WCRL_W20m);/* 0 additional wait states */
  *BUS_WCRL|=1*WCRL_W21m;	/* 0/1 additional wait state */

  /* setup chipselect 3 - KBD */
  *BUS_ABWCR|=ABWCR_ABW3m;	/* 8 bit width */
  *BUS_ASTCR|=ASTCR_AST3m;	/* 3 states access */
  *BUS_WCRL|=(WCRL_W31m|WCRL_W30m);/* 0 additional wait states */
#endif

#if 0
  /* setup chipselect 4 - IDE */
  *BUS_ABWCR&=~ABWCR_ABW4m;	/* 16 bit width */
  *BUS_ASTCR|=ASTCR_AST4m;	/* 3 states access */
  *BUS_WCRH&=~(WCRH_W41m|WCRH_W40m);/* 0 additional wait states */

  /* setup chipselect 5 - IDE */
  *BUS_ABWCR&=~ABWCR_ABW5m;	/* 16 bit width */
  *BUS_ASTCR|=ASTCR_AST5m;	/* 3 states access */
  *BUS_WCRH&=~(WCRH_W51m|WCRH_W50m);/* 0 additional wait states */

  /* setup chipselect 6 - KL41 */
  *BUS_ABWCR|=ABWCR_ABW6m;	/* 8 bit width */
  *BUS_ASTCR|=ASTCR_AST6m;	/* 3 states access */
  *BUS_WCRH=WCRH_W61m|WCRH_W60m;	/* 3 additional wait states */
#endif

  deb_led_out(2);
  FlWait(1*100000);

#if 1
  /*  cross cs wait| rd/wr wait    | no burst and DRAM */
  *BUS_BCRH=0*BCRH_ICIS1m | 0*BCRH_ICIS0m;
  /* release      | no DMAC buffer | no external wait */
  *BUS_BCRL=0*BCRL_WDBEm; // 0*BCRL_BRLEm | 0*BCRL_WDBEm | 0*BCRL_WAITEm;  BRLE and WAITE not build in 2638
  *DIO_PCDDR=0xff;		/* A0-A7 are outputs */
#ifndef SMALL_ADRBUS
  *DIO_PBDDR=0xff;		/* A8-A15 are outputs */
#endif /*SMALL_ADRBUS*/
#ifndef FULL_XRAM_ADRBUS
#ifndef SMALL_ADRBUS
  *SYS_PFCR=__val2mfld(PFCR_AExm,16-8);	/* only 16 address lines */
#else /*SMALL_ADRBUS*/
  *SYS_PFCR=__val2mfld(PFCR_AExm,SMALL_ADRBUS-8); /* only SMALL_ADRBUS address lines */
#endif /*SMALL_ADRBUS*/
#endif /* FULL_XRAM_ADRBUS */

#endif /* registers setup */

  FlWait(1*100000);

#ifdef FULL_XRAM_ADRBUS
  /* Setup full 22 address lines */
  *DIO_PADR|=0x0f;
  *DIO_PADDR=0x0f;		/* A16-A19 are outputs */
  /* number of address output signals */
  *SYS_PFCR=__val2mfld(PFCR_AExm,22-8);
#endif /*FULL_XRAM_ADRBUS*/
#endif

}

