
#if defined(__H8300H__)
	.h8300h
#endif
#if defined(__H8300S__)
	.h8300s
#endif

.text

.align	2

.global ___setup_board

___setup_board :
	mov.l	#__iram0_end,sp
	jsr	__setup_board
	jmp	_start

.end
