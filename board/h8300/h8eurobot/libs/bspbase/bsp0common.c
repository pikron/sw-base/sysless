/* procesor H8S/2638 ver 1.1  */
#include <stdint.h>
#include <cpu_def.h>
#include <h8s2638h.h>
#include <system_def.h>

#define SHADOW_SECT __attribute((section (".shadreg")))

uint8_t DIO_P1DDR_shadow SHADOW_SECT;
uint8_t DIO_P3DDR_shadow SHADOW_SECT;
uint8_t DIO_PADDR_shadow SHADOW_SECT;
uint8_t DIO_PFDDR_shadow SHADOW_SECT;
uint8_t DIO_PJDDR_shadow SHADOW_SECT;
