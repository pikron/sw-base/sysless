/* H8HELI  CAN DRIVER  *
 * Ota Herm, 2005      */

#ifndef _mycan_h_
#define _mycan_h_

void INT_CAN0 (void) __attribute__ ((interrupt_handler));
void INT_CAN1 (void) __attribute__ ((interrupt_handler));

void init_CAN_interrupts(void);
void init_CAN0 (void);
void init_CAN1 (void);
void send_temp_message_CAN0(void);
void send_temp_message_CAN1(void);
void debug_out_CAN(void);

extern int CAN0_ready;
extern int CAN1_ready;
extern int CAN0_gsr_wait;
extern int CAN1_gsr_wait;

extern char sent_messages;

extern int *servo_can_output[];
extern int *servo_can_input[];



#endif
