/* H8HELI  CAN DRIVER  *
 * Ota Herm, 2005      */
#include <stdio.h>
#include <stdint.h>
#include <cpu_def.h>
#include <h8s2638h.h>
#include <system_def.h>
#include <string.h>
#include <boot_fn.h>
#include <periph/sci_rs232.h>
#include "mycan.h"
#include "servodrv.h"

const char str_errpass[] = "error passive";

const char str_no_ints[] = "no interrupt yet";
const char str_unknown[] = "(..)unknown interrupt";
const char str_reset[] = "(IRRO)reset interrupt";
const char str_mbempty[] = "(IRR8)mailbox empty";
const char str_unread[] = "(IRR9)unread interrupt";
const char str_recmes[] = "(IRR1)receive message";
const char str_reqmes[] = "(IRR2)remote frame request";
const char str_transovwarn[] = "(IRR3)transmit overload warning";
const char str_recmes_RXPR0[] = "(IRR1-RXPR0)receive message MB0";
const char str_recmes_RXPR1[] = "(IRR1-RXPR1)receive message MB1";
const char str_recmes_RXPR2[] = "(IRR1-RXPR2)receive message MB2";
const char str_recmes_RXPR3[] = "(IRR1-RXPR3)receive message MB3";
const char str_recmes_RXPR4[] = "(IRR1-RXPR4)receive message MB4";
const char str_recmes_RXPR5[] = "(IRR1-RXPR5)receive message MB5";
const char str_recmes_RXPR6[] = "(IRR1-RXPR6)receive message MB6";
const char str_recmes_RXPR7[] = "(IRR1-RXPR7)receive message MB7";
const char str_recmes_RXPR8[] = "(IRR1-RXPR8)receive message MB8";
const char str_recmes_RXPR9[] = "(IRR1-RXPR9)receive message MB9";
const char str_recmes_RXPR10[] = "(IRR1-RXPR10)receive message MB10";
const char str_recmes_RXPR11[] = "(IRR1-RXPR11)receive message MB11";
const char str_recmes_RXPR12[] = "(IRR1-RXPR12)receive message MB12";
const char str_recmes_RXPR13[] = "(IRR1-RXPR13)receive message MB13";
const char str_recmes_RXPR14[] = "(IRR1-RXPR14)receive message MB14";
const char str_recmes_RXPR15[] = "(IRR1-RXPR15)receive message MB15";

char * CAN0_lastint = (char *)str_no_ints;
char * CAN1_lastint = (char *)str_no_ints;

int CAN0_ints = 0;
int CAN1_ints = 0;
int CAN0_irr = 0xFF;
int CAN1_irr = 0xFF;
int CAN0_gsr_wait = 0;
int CAN1_gsr_wait = 0;
int CAN0_ready = 0;
int CAN1_ready = 0;
int CAN1_RXPR = 0x1234;
int CAN0_RXPR = 0x1234;
int CAN0_RFPR = 0x1234;
int CAN0_RXPRL = 0x12;
int CAN0_RXPRH = 0x34;
int remote0 = 0;
int remote = 0;
int data0 = 0;
int data1 = 0;

unsigned char can0_msgdata[8] = {0x55,0x55,0x55,0x55,0x55,0x55,0x55,0x55};
unsigned char can1_msgdata[8] = {0x55,0x55,0x55,0x55,0x55,0x55,0x55,0x55};

char sent_messages = 0;


void init_CAN_interrupts(void) {
	excptvec_set(108,INT_CAN0);
	excptvec_set(109,INT_CAN0);

	excptvec_set(106,INT_CAN1);
	excptvec_set(107,INT_CAN1);
}

void debug_out_CAN(void) {
	unsigned char *point;
	unsigned char n;
	unsigned char m;
	/*CAN DEBUG*/
	printf("CAN0: \n");
	printf("INTS:%5d,IRR:%4X,MCR:%3X,GSR:%3X,BCR:%4X,MBCR:%4X,GSR_WAIT:%5d,RDY:%2d\n",
		CAN0_ints,CAN0_irr,*HCAN0_MCR,*HCAN0_GSR,*HCAN0_BCR,*HCAN0_MBCR,CAN0_gsr_wait,CAN0_ready);
	printf("RXPR:%4X,TXPR:%4X,TXCR:%4X,TXACK:%4X\n",*HCAN0_RXPR,*HCAN0_TXPR,*HCAN0_TXCR,*HCAN0_TXACK);
	printf("IRR:%4X,IRRH:%2X,IRRL:%2X,TEC:%2X\n",*HCAN0_IRR,*HCAN0_IRRH,*HCAN0_IRRL,*HCAN0_TEC);
	printf("Last interrupt: %s\n",CAN0_lastint);
	printf("/RXPR:%4X/RXPRL:%2X/RXPRH:%2X/RFPR:%4X\n",CAN0_RXPR,CAN0_RXPRL,CAN0_RXPRH,CAN0_RFPR);
	printf("Message: %2X,%2X,%2X,%2X,%2X,%2X,%2X,%2X\n",
		can0_msgdata[0],can0_msgdata[1],can0_msgdata[2],can0_msgdata	[3],can0_msgdata[4],can0_msgdata[5],can0_msgdata[6],can0_msgdata[7]);


	printf("Message data:\n");
	point = (char *)HCAN0_MD01;
	for(n=0;n<8;n++) {
		printf("MB%d:",n);
		for(m=0;m<8;m++) 
			printf("%2X ",*point++);
		printf("\n");
	}
	printf("----------------\n");

	printf("Message control:\n");
	point = (char *)HCAN0_MC0;
	for(n=0;n<8;n++) {
		printf("MB%d:",n);
		for(m=0;m<8;m++) 
			printf("%2X ",*point++);
		printf("\n");
	}
	printf("----------------\n");
	printf("Remote: %d, data: %d\n",remote0,data0);

	printf("CAN1: \n");
	printf("INTS:%5d,IRR:%4X,MCR:%3X,GSR:%3X,BCR:%4X,MBCR:%4X,GSR_WAIT:%5d,RDY:%2d\n",
		CAN1_ints,CAN1_irr,*HCAN1_MCR,*HCAN1_GSR,*HCAN1_BCR,*HCAN1_MBCR,CAN1_gsr_wait,CAN1_ready);
	printf("RXPR:%4X,TXPR:%4X,TXCR:%4X,TXACK:%4X\n",*HCAN1_RXPR,*HCAN1_TXPR,*HCAN1_TXCR,*HCAN1_TXACK);
	printf("IRR:%4X,IRRH:%2X,IRRL:%2X,TEC:%2X,REC:%2X\n",
		*HCAN1_IRR,*HCAN1_IRRH,*HCAN1_IRRL,*HCAN1_TEC,*HCAN1_REC);
	printf("Last interrupt: %s\n",CAN1_lastint);
	printf("/RXPR:%4X\n",CAN1_RXPR);
	printf("UMSR:%4X,MBIMR:%4X,IMR:%4X\n",*HCAN1_UMSR,*HCAN1_MBIMR,*HCAN1_IMR);
	printf("Message: %2X,%2X,%2X,%2X,%2X,%2X,%2X,%2X\n",
		can1_msgdata[0],can1_msgdata[1],can1_msgdata[2],can1_msgdata	[3],can1_msgdata[4],can1_msgdata[5],can1_msgdata[6],can1_msgdata[7]);

	printf("Message data 1:\n");
	point = (char *)HCAN1_MD01;
	for(n=0;n<8;n++) {
		printf("MB%d:",n);
		for(m=0;m<8;m++) 
			printf("%2X ",*point++);
		printf("\n");
	}
	printf("----------------\n");

	printf("Message control 1:\n");
	point = (char *)HCAN1_MC0;
	for(n=0;n<8;n++) {
		printf("MB%d:",n);
		for(m=0;m<8;m++) 
			printf("%2X ",*point++);
		printf("\n");
	}
	printf("----------------\n");
	printf("Remote: %d, data: %d\n",remote,data1);
}


/**
*	Inform the Primary Flight Computer about switching to manual mode
**/
void can_send_servo_manual(void) {
	*HCAN0_TXPRL |= TXPRL_TXPR7m;
}

/**
*	Inform the Primary Flight Computer about switching to automatic mode
**/
void can_send_servo_auto(void) {
	*HCAN0_TXPRL |= TXPRL_TXPR6m;
}

void send_temp_message_CAN0(void) {
	if(CAN0_ready == 1) {
		/*CAN0_ready = 2;
		//Try to send a simple message
		*HCAN0_MD11 	= 0x11;
		*(HCAN0_MD11+1)	= 0x22;
		*(HCAN0_MD11+2)	= 0x33;
		*(HCAN0_MD11+3)	= sent_messages;
		sent_messages++;

		//Sending message....by Hardware manual, page 624
		
		//Transmit data setting
		//Arbitration field setting
		*HCAN0_TXPRL |= TXPRL_TXPR1m;




		//Try to send a simple message
		*HCAN0_MD51 	= 0x01;
		*(HCAN0_MD51+1)	= 0x02;
		*(HCAN0_MD51+2)	= 0x03;
		*(HCAN0_MD51+3)	= 0x04;

		//Sending message....by Hardware manual, page 624
		
		//Transmit data setting
		//Arbitration field setting
		*HCAN0_TXPRL |= TXPRL_TXPR5m;*/

	} else {
		if(CAN0_ready == 2) {
			CAN0_ready = 1;
		}
	}
}

void send_temp_message_CAN1(void) {
	if(CAN1_ready == 1) {
		CAN1_ready = 2;
		/*//Try to send a simple message
		*HCAN1_MD11 	= 0x11;
		*(HCAN1_MD11+1)	= 0x22;
		*(HCAN1_MD11+2)	= 0x33;
		*(HCAN1_MD11+3)	= 0x44;

		//Sending message....by Hardware manual, page 624
		
		//Transmit data setting
		//Arbitration field setting
		//
		*HCAN1_TXPRL |= TXPRL_TXPR1m;*/

		*HCAN1_TXPRL |= TXPRL_TXPR7m; //Send SYNC from MB7
	} else {
		if(CAN1_ready == 2) {
			CAN1_ready = 1;
		}
	}
}

void init_CAN0 (void) {
	unsigned char *point;
	unsigned char n;
	unsigned char m;

	/*CAN0 INIT*/
	// By "Hardware reset flowchart", page 615 in the Hardware manual of the H8S2638
	CAN0_ready = 0;

	*SYS_MSTPCRC &= ~MSTPCRC_HCAN0m;	//Switch the HCAN0 module ON
	*HCAN0_IRRL |= IRRL_IRR0m;	//Deactivate reset interrupt
	
	//Speed initialization
	//BCR setting
	//*HCAN0_BCR = 0x0025;
	*HCAN0_BCR = 0x0569;

	//MBCR setting
	*HCAN0_MBCR = 0xFFFF;	//Init all mailboxes for reception
	*HCAN0_MBCR &= ~MBCR_MBCR1m;	//MB 1 for transmission
	*HCAN0_MBCR &= ~MBCR_MBCR3m;	//MB 3 for transmission
	*HCAN0_MBCR &= ~MBCR_MBCR4m;	//MB 4 for transmission
	*HCAN0_MBCR &= ~MBCR_MBCR5m;	//MB 5 for transmission
	*HCAN0_MBCR &= ~MBCR_MBCR6m;	//MB 6 for transmission
	*HCAN0_MBCR &= ~MBCR_MBCR7m;	//MB 7 for transmission
	
	//Mailbox (RAM) initialization
	point = (char *)HCAN0_MD01;
	for(n=0;n<8;n++) {
		for(m=0;m<8;m++) {
			*point = 0x00;
			point++;
		}
	}
	point = (char *)HCAN0_MC0;
	for(n=0;n<=15;n++) {
		for(m=0;m<8;m++)
			*(point+m) = 0x00;
		point += 8;
	}

	//Message transmission method initialization
	*HCAN0_MCR &= ~MCR_MCR2m;	//Transmission order determined by message priority
	*HCAN0_MCR &= ~MCR_MCR0m;	//Go to normal mode from reset mode

	//Wait for GSR3 to go to 0
	while(*HCAN0_GSR & GSR_GSR3m) {
		CAN0_gsr_wait++;
	}

	//IMR setting (interrupt mask)
	*HCAN0_IMR = 0xFFFF; //Everything off
	*HCAN0_IMRL &= ~IMRL_IMR1m;	//Receive message interrupt
	*HCAN0_IMRL &= ~IMRL_IMR2m;	//Remote frame request interrupt
	//*HCAN0_IMRL &= ~IMRL_IMR3m;
	//*HCAN0_IMRL &= ~IMRL_IMR4m;
	*HCAN0_IMRL &= ~IMRL_IMR5m;
	//*HCAN0_IMRL &= ~IMRL_IMR6m;
	//*HCAN0_IMRL &= ~IMRL_IMR7m;
	*HCAN0_IMRH &= ~IMRH_IMR8m;	//Mailbox empty interrupt
	//*HCAN0_IMRH &= ~IMRH_IMR12m;*/
	
	//*HCAN0_IMR = 0x0000; //Experiment>Everything on


	//MBIMR setting (mailbox interrupt mask)
	*HCAN0_MBIMR = 0xFFFF;	//All mailboxes disabled
	//*HCAN0_MBIMRL &= ~MBIMRL_MBIMR0m; //Enable mailbox 0
	*HCAN0_MBIMRL &= ~MBIMRL_MBIMR1m; //Enable mailbox 1
	*HCAN0_MBIMRL &= ~MBIMRL_MBIMR2m; //Enable mailbox 2

	*HCAN0_MBIMR = 0x0000;	//Experiment>All mailboxes enabled

	//MC[x] setting (receive identifier setting)
	*(HCAN0_MC0+8+0) = 0x04;	//Mailbox 1, length = 4 (MC1[1])
	*(HCAN0_MC0+8+6) = 0x3E;	//Mailbox 1, Standard Identifier = 500 (high byte) (MC1[6])
	*(HCAN0_MC0+8+5) = 0x80;	//Mailbox 1, Standard Identifier = 500 (low bits) + RTR + IDE (MC1[5])

	//Prepare mailbox for SYNCHRONIZATION (ID = 20)
	*(HCAN0_MC2+0) = 0x00;	//Mailbox 2, length = 0 (MC2[1])
	*(HCAN0_MC2+6) = 0x02;	//Mailbox 2, Standard Identifier = 20 (high byte) (MC2[6])
	*(HCAN0_MC2+5) = 0x90;	//Mailbox 2, Standard Identifier = 20 (low bits) + RTR + IDE (MC2[5])


	//Prepare mailbox for sending of SERVO VALUES 1 (ID = 35)
	*(HCAN0_MC3+0) = 0x00;	//Mailbox 3, length = 0 (MC3[1])
	*(HCAN0_MC3+6) = 0x04; //Mailbox 3, Standard Identifier = 35 (high byte) (MC3[6])
	*(HCAN0_MC3+5) = 0x80;	//Mailbox 3, Standard Identifier = 35 (low bits) + RTR + IDE (MC3[5])

	//Prepare mailbox for sending of SERVO VALUES 2 (ID = 36)
	*(HCAN0_MC4+0) = 0x00;	//Mailbox 4, length = 0 (MC4[1])
	*(HCAN0_MC4+6) = 0x04;	//Mailbox 4, Standard Identifier = 36 (high byte) (MC4[6])
	*(HCAN0_MC4+5) = 0x80;	//Mailbox 4, Standard Identifier = 36 (low bits) + RTR + IDE (MC4[5])

	*(HCAN0_MC5+0) = 0x04;	//Mailbox 5, length = 4 (MC1[1])
	*(HCAN0_MC5+6) = 0x3E;	//Mailbox 5, Standard Identifier = 501 (high byte) (MC1[6])
	*(HCAN0_MC5+5) = 0xA0;	//Mailbox 5, Standard Identifier = 501 (low bits) + RTR + IDE (MC1[5])

	//Prepare mailboxes for REMOTE FRAME for servo mode information (auto/manual)
	//Auto (ID = 10)
	//MC[x] setting (receive identifier setting)
	*(HCAN0_MC6+0) = 0x00;	//Mailbox 6, length = 0 (MC6[1])
	*(HCAN0_MC6+6) = 0x01;	//Mailbox 6, Standard Identifier = 10 (high byte) (MC6[6])
	*(HCAN0_MC6+5) = 0x50;	//Mailbox 6, Standard Identifier = 10 (low bits) + RTR + IDE (MC6[5])

	//Auto (ID = 11)
	//MC[x] setting (receive identifier setting)
	*(HCAN0_MC7+0) = 0x00;	//Mailbox 7, length = 0 (MC6[1])
	*(HCAN0_MC7+6) = 0x01;	//Mailbox 7, Standard Identifier = 11 (high byte) (MC6[6])
	*(HCAN0_MC7+5) = 0x70;	//Mailbox 7, Standard Identifier = 11 (low bits) + RTR + IDE (MC6[5])


	//LAFM setting (receive identifier mask setting)
/*	*HCAN0_LAFML = 0xFFFF;
	*HCAN0_LAFMH = 0xFFFF;*/
	*HCAN0_LAFML = 0x0000;
	*HCAN0_LAFMH = 0x0000;


	//(after GSR3 goes to 0 -- should be 0 yet...)  and 11 recessive bits are received 



	//NOW THE CAN BUS COMMUNICATION SHOULD BE ENABED
	CAN0_ready = 1;
}

void init_CAN1 (void) {
	unsigned char *point;
	unsigned char n;
	unsigned char m;

	/*CAN1 INIT*/
	// By "Hardware reset flowchart", page 615 in the Hardware manual of the H8S2638
	CAN1_ready = 0;

	*SYS_MSTPCRC &= ~MSTPCRC_HCAN1m;	//Switch the HCAN1 module ON
	*HCAN1_IRRL |= IRRL_IRR0m;	//Deactivate reset interrupt
	
	//BCR setting
	//*HCAN1_BCR = 0x0025;
	*HCAN1_BCR = 0x0569;

	//MBCR setting	
	*HCAN1_MBCR = 0xFFFF;	//Init all mailboxes for reception
//	*HCAN1_MBCR &= ~MBCR_MBCR1m;	//MB 1 for transmission
//	*HCAN1_MBCR &= ~MBCR_MBCR5m;	//MB 5 for transmission
//	*HCAN1_MBCR &= ~MBCR_MBCR6m;	//MB 6 for transmission
	*HCAN1_MBCR &= ~MBCR_MBCR7m;	//MB 7 for transmission

	//Mailbox (RAM) initialization
	point = (char *)HCAN1_MD01;
	for(n=0;n<8;n++) {
		for(m=0;m<8;m++) {
			*point = 0x00;
			point++;
		}
	}
	point = (char *)HCAN1_MC0;
	for(n=0;n<8;n++) {
		for(m=0;m<8;m++) {
			*point = 0x00;
			point++;
		}
	}

	//Message transmission method initialization
	*HCAN1_MCR &= ~MCR_MCR2m; //Transmission order determined by message priority

	//Go to normal mode from reset mode
	*HCAN1_MCR &= ~MCR_MCR0m; 

	//Wait for GSR3 to go to 0
	while(*HCAN1_GSR & GSR_GSR3m) {
		CAN1_gsr_wait++;
	}

	//IMR setting (interrupt mask)
	*HCAN1_IMR = 0xFFFF; //Everything off
	*HCAN1_IMRL &= ~IMRL_IMR1m;	//Receive message interrupt
	*HCAN1_IMRL &= ~IMRL_IMR2m; //Remote frame request interrupt
	*HCAN1_IMRL &= ~IMRL_IMR3m;	//Transmit Overload Warning interrupt
	*HCAN1_IMRL &= ~IMRL_IMR4m;
	*HCAN1_IMRL &= ~IMRL_IMR5m;
	*HCAN1_IMRL &= ~IMRL_IMR6m;
	*HCAN1_IMRL &= ~IMRL_IMR7m;
	*HCAN1_IMRH &= ~IMRH_IMR8m; //Mailbox empty interrupt
	*HCAN1_IMRH &= ~IMRH_IMR9m;	//Unread interrupt
	*HCAN1_IMRH &= ~IMRH_IMR12m; //Bus operation interrupt request (OVR0) to CPU

	//*HCAN1_IMR = 0x0000; //Experiment>Everything on


	//MBIMR setting (mailbox interrupt mask)
	*HCAN1_MBIMR = 0xFFFF;	//All mailboxes disabled
	//*HCAN1_MBIMRL &= ~MBIMRL_MBIMR0m; //Enable mailbox 0
	*HCAN1_MBIMRL &= ~MBIMRL_MBIMR1m; //Enable mailbox 1
	*HCAN1_MBIMRL &= ~MBIMRL_MBIMR2m; //Enable mailbox 2
	*HCAN1_MBIMRL &= ~MBIMRL_MBIMR3m; //Enable mailbox 3
	//*HCAN1_MBIMRL &= ~MBIMRL_MBIMR4m; //Enable mailbox 4
	*HCAN1_MBIMRL &= ~MBIMRL_MBIMR5m; //Enable mailbox 5
	//*HCAN1_MBIMRL &= ~MBIMRL_MBIMR6m; //Enable mailbox 6
	//*HCAN1_MBIMRL &= ~MBIMRL_MBIMR7m; //Enable mailbox 7

	//*HCAN1_MBIMR = 0x0000;	//Experiment>All mailboxes enabled
	//*HCAN1_MBIMRL |= MBIMRL_MBIMR0m; //Disable mailbox 0


	//MC[x] setting (receive identifier setting)
	/**(HCAN1_MC0+0) = 0x04;	//Mailbox 0, length = 4 (MC0[1])
	*(HCAN1_MC0+6) = 0x3E;	//Mailbox 0, Standard Identifier = 500 (high byte) (MC0[6])
	*(HCAN1_MC0+5) = 0x80;	//Mailbox 0, Standard Identifier = 500 (low bits) + RTR + IDE (MC0[5])
*/
	*(HCAN1_MC1+0) = 0x04;	//Mailbox 1, length = 4 (MC1[1])
	*(HCAN1_MC1+6) = 0x3E;	//Mailbox 1, Standard Identifier = 501 (high byte) (MC1[6])
	*(HCAN1_MC1+5) = 0xA0;	//Mailbox 1, Standard Identifier = 501 (low bits) + RTR + IDE (MC1[5])


	*(HCAN1_MC5+0) = 0x04;	//Mailbox 5, length = 4 (MC5[1])
	*(HCAN1_MC5+6) = 0x3E;	//Mailbox 5, Standard Identifier = 500 (high byte) (MC5[6])
	*(HCAN1_MC5+5) = 0x80;	//Mailbox 5, Standard Identifier = 500 (low bits) + RTR + IDE (MC5[5])

	*(HCAN1_MC2+0) = 0x04;	//Mailbox 2, length = 4 (MC1[1])
	*(HCAN1_MC2+6) = 0x01;	//Mailbox 2, Standard Identifier = 10 (high byte) (MC1[6])
	*(HCAN1_MC2+5) = 0x50;	//Mailbox 2, Standard Identifier = 10 (low bits) + RTR + IDE (MC1[5])

	//MC[x] setting (receive identifier setting)
	*(HCAN1_MC3+0) = 0x00;	//Mailbox 3, length = 0 (MC6[1])
	*(HCAN1_MC3+6) = 0x01;	//Mailbox 3, Standard Identifier = 10 (high byte) (MC6[6])
	*(HCAN1_MC3+5) = 0x70;	//Mailbox 3, Standard Identifier = 11 (low bits) + RTR + IDE (MC6[5])

	//Prepare mailbox for SYNCHRONIZATION
	//(ID = 20)
	//MC[x] setting (receive identifier setting)
	*(HCAN1_MC7+0) = 0x00;	//Mailbox 7, length = 0 (MC2[1])
	*(HCAN1_MC7+6) = 0x02;	//Mailbox 7, Standard Identifier = 20 (high byte) (MC2[6])
	*(HCAN1_MC7+5) = 0x90;	//Mailbox 7, Standard Identifier = 20 (low bits) + RTR + IDE (MC2[5])

	
	//LAFM setting (receive identifier mask setting)
	/*
	*HCAN1_LAFML = 0xFFFF;
	*HCAN1_LAFMH = 0xFFFF;
	*/
	*HCAN1_LAFML = 0x0000;
	*HCAN1_LAFMH = 0x0000;


	//(after GSR3 goes to 0 -- should be 0 yet...)  and 11 recessive bits are received 



	//THE CAN BUS COMMUNICATION SHOULD BE ENABED
	CAN1_ready = 1;

}

//CAN0 interrupt
void INT_CAN0 (void) {

	CAN0_irr = *HCAN0_IRR;
	CAN0_ints++;

	CAN0_lastint = (char *)str_unknown;

	led_blink(&timer_led_can0,LED_CAN0,LED_TICK_BLINK_TIME);

	/*rozhodnuti o IRR*/
	if(~(*HCAN0_IRRL & IRRL_IRR0m)) {
		//HCAN0 Reset interrupt
		*HCAN0_IRRL |= IRRL_IRR0m;
		CAN0_lastint = (char *)str_reset;
		//CAN0 initialization
		//MCx and MDx initialization
		//Set the message's Standard identifiers independent here
		
		/*
		*HCAN0_MCR &= ~MCR_MCR0m;

		//Wait for GSR3 = 0;
		while(*HCAN0_GSR & GSR_GSR3m) {
			CAN0_gsr_wait++;
		}
		*/
	}


	if(*HCAN0_IRRL & IRRL_IRR1m) {
		//HCAN0 Receive message interrupt
		//Must clear all the message flags
		CAN0_RXPR = *HCAN0_RXPR;
		CAN0_RXPRL = *HCAN0_RXPRL;
		CAN0_RXPRH = *HCAN0_RXPRH;
		CAN0_lastint = (char *)str_recmes;
		CAN0_RFPR = *HCAN0_RFPR;

		if(*HCAN0_RXPRL & RXPRL_RXPR0m) {
			*HCAN0_RXPRL |= RXPRL_RXPR0m;
			CAN0_lastint = (char *)str_recmes_RXPR0;
			//Exract the received data
			can0_msgdata[0] = *(HCAN0_MD01+0);
			can0_msgdata[1] = *(HCAN0_MD01+1);
			can0_msgdata[2] = *(HCAN0_MD01+2);
			can0_msgdata[3] = *(HCAN0_MD01+3);
			can0_msgdata[4] = *(HCAN0_MD01+4);
			can0_msgdata[5] = *(HCAN0_MD01+5);
			can0_msgdata[6] = *(HCAN0_MD01+6);
			can0_msgdata[7] = *(HCAN0_MD01+7);
		}
		if(*HCAN0_RXPRL & RXPRL_RXPR1m) {
			*HCAN0_RXPRL |= RXPRL_RXPR1m;
			CAN0_lastint = (char *)str_recmes_RXPR1;
			//Exract the received data
			can0_msgdata[0] = *(HCAN0_MD11+0);
			can0_msgdata[1] = *(HCAN0_MD11+1);
			can0_msgdata[2] = *(HCAN0_MD11+2);
			can0_msgdata[3] = *(HCAN0_MD11+3);
			can0_msgdata[4] = *(HCAN0_MD11+4);
			can0_msgdata[5] = *(HCAN0_MD11+5);
			can0_msgdata[6] = *(HCAN0_MD11+6);
			can0_msgdata[7] = *(HCAN0_MD11+7);

		}
		if(*HCAN0_RXPRL & RXPRL_RXPR2m) {
			*HCAN0_RXPRL |= RXPRL_RXPR2m;
			CAN0_lastint = (char *)str_recmes_RXPR2;
			//Exract the received data
			can0_msgdata[0] = *(HCAN0_MD21+0);
			can0_msgdata[1] = *(HCAN0_MD21+1);
			can0_msgdata[2] = *(HCAN0_MD21+2);
			can0_msgdata[3] = *(HCAN0_MD21+3);
			can0_msgdata[4] = *(HCAN0_MD21+4);
			can0_msgdata[5] = *(HCAN0_MD21+5);
			can0_msgdata[6] = *(HCAN0_MD21+6);
			can0_msgdata[7] = *(HCAN0_MD21+7);
			
			//*HCAN0_TXPRL |= TXPRL_TXPR3m;
			//*HCAN0_TXPRL |= TXPRL_TXPR4m;

		}
		if(*HCAN0_RXPRL & RXPRL_RXPR3m) {
			*HCAN0_RXPRL |= RXPRL_RXPR3m;
			CAN0_lastint = (char *)str_recmes_RXPR3;
			//Exract the received data
			can0_msgdata[0] = *(HCAN0_MD31+0);
			can0_msgdata[1] = *(HCAN0_MD31+1);
			can0_msgdata[2] = *(HCAN0_MD31+2);
			can0_msgdata[3] = *(HCAN0_MD31+3);
			can0_msgdata[4] = *(HCAN0_MD31+4);
			can0_msgdata[5] = *(HCAN0_MD31+5);
			can0_msgdata[6] = *(HCAN0_MD31+6);
			can0_msgdata[7] = *(HCAN0_MD31+7);
		}
		if(*HCAN0_RXPRL & RXPRL_RXPR4m) {
			*HCAN0_RXPRL |= RXPRL_RXPR4m;
			CAN0_lastint = (char *)str_recmes_RXPR4;
			//Exract the received data
			can0_msgdata[0] = *(HCAN0_MD41+0);
			can0_msgdata[1] = *(HCAN0_MD41+1);
			can0_msgdata[2] = *(HCAN0_MD41+2);
			can0_msgdata[3] = *(HCAN0_MD41+3);
			can0_msgdata[4] = *(HCAN0_MD41+4);
			can0_msgdata[5] = *(HCAN0_MD41+5);
			can0_msgdata[6] = *(HCAN0_MD41+6);
			can0_msgdata[7] = *(HCAN0_MD41+7);
		}
		if(*HCAN0_RXPRL & RXPRL_RXPR5m) {
			*HCAN0_RXPRL |= RXPRL_RXPR5m;
			CAN0_lastint = (char *)str_recmes_RXPR5;
			//Extract the received data
			can0_msgdata[0] = *(HCAN0_MD51+0);
			can0_msgdata[1] = *(HCAN0_MD51+1);
			can0_msgdata[2] = *(HCAN0_MD51+2);
			can0_msgdata[3] = *(HCAN0_MD51+3);
			can0_msgdata[4] = *(HCAN0_MD51+4);
			can0_msgdata[5] = *(HCAN0_MD51+5);
			can0_msgdata[6] = *(HCAN0_MD51+6);
			can0_msgdata[7] = *(HCAN0_MD51+7);
		}
		if(*HCAN0_RXPRL & RXPRL_RXPR6m) {
			*HCAN0_RXPRL |= RXPRL_RXPR6m;
			CAN0_lastint = (char *)str_recmes_RXPR6;
			//Exract the received data
			can0_msgdata[0] = *(HCAN0_MD61+0);
			can0_msgdata[1] = *(HCAN0_MD61+1);
			can0_msgdata[2] = *(HCAN0_MD61+2);
			can0_msgdata[3] = *(HCAN0_MD61+3);
			can0_msgdata[4] = *(HCAN0_MD61+4);
			can0_msgdata[5] = *(HCAN0_MD61+5);
			can0_msgdata[6] = *(HCAN0_MD61+6);
			can0_msgdata[7] = *(HCAN0_MD61+7);
		}
		if(*HCAN0_RXPRL & RXPRL_RXPR7m) {
			*HCAN0_RXPRL |= RXPRL_RXPR7m;
			CAN0_lastint = (char *)str_recmes_RXPR7;
			//Exract the received data
			can0_msgdata[0] = *(HCAN0_MD71+0);
			can0_msgdata[1] = *(HCAN0_MD71+1);
			can0_msgdata[2] = *(HCAN0_MD71+2);
			can0_msgdata[3] = *(HCAN0_MD71+3);
			can0_msgdata[4] = *(HCAN0_MD71+4);
			can0_msgdata[5] = *(HCAN0_MD71+5);
			can0_msgdata[6] = *(HCAN0_MD71+6);
			can0_msgdata[7] = *(HCAN0_MD71+7);
		}
	 	if(*HCAN0_RXPRH & RXPRH_RXPR8m) {
			*HCAN0_RXPRH |= RXPRH_RXPR8m;
			CAN0_lastint = (char *)str_recmes_RXPR8;
			//Exract the received data
			can0_msgdata[0] = *(HCAN0_MD81+0);
			can0_msgdata[1] = *(HCAN0_MD81+1);
			can0_msgdata[2] = *(HCAN0_MD81+2);
			can0_msgdata[3] = *(HCAN0_MD81+3);
			can0_msgdata[4] = *(HCAN0_MD81+4);
			can0_msgdata[5] = *(HCAN0_MD81+5);
			can0_msgdata[6] = *(HCAN0_MD81+6);
			can0_msgdata[7] = *(HCAN0_MD81+7);
		}
		if(*HCAN0_RXPRH & RXPRH_RXPR9m) {
			*HCAN0_RXPRH |= RXPRH_RXPR9m;
			CAN0_lastint = (char *)str_recmes_RXPR9;
			//Exract the received data
			can0_msgdata[0] = *(HCAN0_MD91+0);
			can0_msgdata[1] = *(HCAN0_MD91+1);
			can0_msgdata[2] = *(HCAN0_MD91+2);
			can0_msgdata[3] = *(HCAN0_MD91+3);
			can0_msgdata[4] = *(HCAN0_MD91+4);
			can0_msgdata[5] = *(HCAN0_MD91+5);
			can0_msgdata[6] = *(HCAN0_MD91+6);
			can0_msgdata[7] = *(HCAN0_MD91+7);
		}
		if(*HCAN0_RXPRH & RXPRH_RXPR10m) {
			*HCAN0_RXPRH |= RXPRH_RXPR10m;
			CAN0_lastint = (char *)str_recmes_RXPR10;
			//Exract the received data
			can0_msgdata[0] = *(HCAN0_MD101+0);
			can0_msgdata[1] = *(HCAN0_MD101+1);
			can0_msgdata[2] = *(HCAN0_MD101+2);
			can0_msgdata[3] = *(HCAN0_MD101+3);
			can0_msgdata[4] = *(HCAN0_MD101+4);
			can0_msgdata[5] = *(HCAN0_MD101+5);
			can0_msgdata[6] = *(HCAN0_MD101+6);
			can0_msgdata[7] = *(HCAN0_MD101+7);
		}
		if(*HCAN0_RXPRH & RXPRH_RXPR11m) {
			*HCAN0_RXPRH |= RXPRH_RXPR11m;
			CAN0_lastint = (char *)str_recmes_RXPR11;
			//Exract the received data
			can0_msgdata[0] = *(HCAN0_MD111+0);
			can0_msgdata[1] = *(HCAN0_MD111+1);
			can0_msgdata[2] = *(HCAN0_MD111+2);
			can0_msgdata[3] = *(HCAN0_MD111+3);
			can0_msgdata[4] = *(HCAN0_MD111+4);
			can0_msgdata[5] = *(HCAN0_MD111+5);
			can0_msgdata[6] = *(HCAN0_MD111+6);
			can0_msgdata[7] = *(HCAN0_MD111+7);
		}
		if(*HCAN0_RXPRH & RXPRH_RXPR12m) {
			*HCAN0_RXPRH |= RXPRH_RXPR12m;
			CAN0_lastint = (char *)str_recmes_RXPR12;
			//Exract the received data
			can0_msgdata[0] = *(HCAN0_MD121+0);
			can0_msgdata[1] = *(HCAN0_MD121+1);
			can0_msgdata[2] = *(HCAN0_MD121+2);
			can0_msgdata[3] = *(HCAN0_MD121+3);
			can0_msgdata[4] = *(HCAN0_MD121+4);
			can0_msgdata[5] = *(HCAN0_MD121+5);
			can0_msgdata[6] = *(HCAN0_MD121+6);
			can0_msgdata[7] = *(HCAN0_MD121+7);
		}
		if(*HCAN0_RXPRH & RXPRH_RXPR13m) {
			*HCAN0_RXPRH |= RXPRH_RXPR13m;
			CAN0_lastint = (char *)str_recmes_RXPR13;
			//Exract the received data
			can0_msgdata[0] = *(HCAN0_MD131+0);
			can0_msgdata[1] = *(HCAN0_MD131+1);
			can0_msgdata[2] = *(HCAN0_MD131+2);
			can0_msgdata[3] = *(HCAN0_MD131+3);
			can0_msgdata[4] = *(HCAN0_MD131+4);
			can0_msgdata[5] = *(HCAN0_MD131+5);
			can0_msgdata[6] = *(HCAN0_MD131+6);
			can0_msgdata[7] = *(HCAN0_MD131+7);
		}
		if(*HCAN0_RXPRH & RXPRH_RXPR14m) {
			*HCAN0_RXPRH |= RXPRH_RXPR14m;
			CAN0_lastint = (char *)str_recmes_RXPR14;
			//Exract the received data
			can0_msgdata[0] = *(HCAN0_MD141+0);
			can0_msgdata[1] = *(HCAN0_MD141+1);
			can0_msgdata[2] = *(HCAN0_MD141+2);
			can0_msgdata[3] = *(HCAN0_MD141+3);
			can0_msgdata[4] = *(HCAN0_MD141+4);
			can0_msgdata[5] = *(HCAN0_MD141+5);
			can0_msgdata[6] = *(HCAN0_MD141+6);
			can0_msgdata[7] = *(HCAN0_MD141+7);
		}
		if(*HCAN0_RXPRH & RXPRH_RXPR15m) {
			*HCAN0_RXPRH |= RXPRH_RXPR15m;
			CAN0_lastint = (char *)str_recmes_RXPR15;
			//Exract the received data
			can0_msgdata[0] = *(HCAN0_MD151+0);
			can0_msgdata[1] = *(HCAN0_MD151+1);
			can0_msgdata[2] = *(HCAN0_MD151+2);
			can0_msgdata[3] = *(HCAN0_MD151+3);
			can0_msgdata[4] = *(HCAN0_MD151+4);
			can0_msgdata[5] = *(HCAN0_MD151+5);
			can0_msgdata[6] = *(HCAN0_MD151+6);
			can0_msgdata[7] = *(HCAN0_MD151+7);
		}


		if(*HCAN0_RXPR!=0) {
			//Temporaly delete unused flags
			*HCAN0_RXPR = 0xFFFF;
			CAN0_lastint = (char *)str_recmes;
		}


		*HCAN0_RXPR = 0xFFFF;
		
		data0++;
	}

	if(*HCAN0_IRRL & IRRL_IRR2m) {
		//HCAN0 Remote frame request interrupt
		//Must clear all the message flags
		*HCAN0_RFPR = 0xFFFF;
		remote0++;
	}

	if(*HCAN0_IRRL & IRRL_IRR3m) {
		//HCAN0 Transmit overload warning interrupt
		*HCAN0_IRRL |= IRRL_IRR3m;
	}
	
	if(*HCAN0_IRRL & IRRL_IRR4m) {
		//HCAN0 Receive overload warning interrupt
		*HCAN0_IRRL |= IRRL_IRR4m;
	}

	if(*HCAN0_IRRL & IRRL_IRR5m) {
		//HCAN0 Error passive interrupt
		*HCAN0_IRRL |= IRRL_IRR5m;
	}

		if(*HCAN0_IRRL & IRRL_IRR6m) {
		//HCAN0 Bus off interrupt
		*HCAN0_IRRL |= IRRL_IRR6m;
	}

	if(*HCAN0_IRRL & IRRL_IRR7m) {
		//HCAN0 Overload frame interrupt
		*HCAN0_IRRL |= IRRL_IRR7m;
	}

	if(*HCAN0_IRRH & IRRH_IRR12m) {
		//HCAN0 Bus operation interrupt
		*HCAN0_IRRH |= IRRH_IRR12m;
	}
	
	if(*HCAN0_IRRH & IRRH_IRR9m) {
		//HCAN0 Unread interrupt
		*HCAN0_IRRH |= IRRH_IRR9m;
	}

	if(*HCAN0_IRRH & IRRH_IRR8m) {
		//HCAN0 Mailbox empty interrupt 
		*HCAN0_IRRH |= IRRH_IRR8m;
		CAN0_lastint = (char *)str_mbempty;
	}
}

//CAN1 interrupt
void INT_CAN1 (void) {

	CAN1_irr = *HCAN1_IRR;
	CAN1_ints++;

	CAN1_lastint = (char *)str_unknown;

	led_blink(&timer_led_can1,LED_CAN1,LED_TICK_BLINK_TIME);

	/*rozhodnuti o IRR*/
	if(~(*HCAN1_IRRL & IRRL_IRR0m)) {
		//HCAN1 Reset interrupt
		*HCAN1_IRRL |= IRRL_IRR0m;
		//CAN1 initialization
		CAN1_lastint = (char *)str_reset;
	}


	if(*HCAN1_IRRL & IRRL_IRR1m) {
		//HCAN1 Receive message interrupt
		data1++;
		//Must clear all the message flags
		CAN1_RXPR = *HCAN1_RXPR;
		if(*HCAN1_RXPRL & RXPRL_RXPR0m) {
			*HCAN1_RXPRL |= RXPRL_RXPR0m;
			CAN1_lastint = (char *)str_recmes_RXPR0;
			//Exract the received data
			can1_msgdata[0] = *(HCAN1_MD01+0);
			can1_msgdata[1] = *(HCAN1_MD01+1);
			can1_msgdata[2] = *(HCAN1_MD01+2);
			can1_msgdata[3] = *(HCAN1_MD01+3);
			can1_msgdata[4] = *(HCAN1_MD01+4);
			can1_msgdata[5] = *(HCAN1_MD01+5);
			can1_msgdata[6] = *(HCAN1_MD01+6);
			can1_msgdata[7] = *(HCAN1_MD01+7);
		}
		if(*HCAN1_RXPRL & RXPRL_RXPR1m) {
			*HCAN1_RXPRL |= RXPRL_RXPR1m;
			CAN1_lastint = (char *)str_recmes_RXPR1;
			//Exract the received data
			can1_msgdata[0] = *(HCAN1_MD11+0);
			can1_msgdata[1] = *(HCAN1_MD11+1);
			can1_msgdata[2] = *(HCAN1_MD11+2);
			can1_msgdata[3] = *(HCAN1_MD11+3);
			can1_msgdata[4] = *(HCAN1_MD11+4);
			can1_msgdata[5] = *(HCAN1_MD11+5);
			can1_msgdata[6] = *(HCAN1_MD11+6);
			can1_msgdata[7] = *(HCAN1_MD11+7);
		}
		if(*HCAN1_RXPRL & RXPRL_RXPR2m) {
			*HCAN1_RXPRL |= RXPRL_RXPR2m;
			CAN1_lastint = (char *)str_recmes_RXPR2;
			//Exract the received data
			can1_msgdata[0] = *(HCAN1_MD21+0);
			can1_msgdata[1] = *(HCAN1_MD21+1);
			can1_msgdata[2] = *(HCAN1_MD21+2);
			can1_msgdata[3] = *(HCAN1_MD21+3);
			can1_msgdata[4] = *(HCAN1_MD21+4);
			can1_msgdata[5] = *(HCAN1_MD21+5);
			can1_msgdata[6] = *(HCAN1_MD21+6);
			can1_msgdata[7] = *(HCAN1_MD21+7);
		}
		if(*HCAN1_RXPRL & RXPRL_RXPR3m) {
			*HCAN1_RXPRL |= RXPRL_RXPR3m;
			CAN1_lastint = (char *)str_recmes_RXPR3;
			//Exract the received data
			can1_msgdata[0] = *(HCAN1_MD31+0);
			can1_msgdata[1] = *(HCAN1_MD31+1);
			can1_msgdata[2] = *(HCAN1_MD31+2);
			can1_msgdata[3] = *(HCAN1_MD31+3);
			can1_msgdata[4] = *(HCAN1_MD31+4);
			can1_msgdata[5] = *(HCAN1_MD31+5);
			can1_msgdata[6] = *(HCAN1_MD31+6);
			can1_msgdata[7] = *(HCAN1_MD31+7);
		}
		if(*HCAN1_RXPRL & RXPRL_RXPR4m) {
			*HCAN1_RXPRL |= RXPRL_RXPR4m;
			CAN1_lastint = (char *)str_recmes_RXPR4;
			//Exract the received data
			can1_msgdata[0] = *(HCAN1_MD41+0);
			can1_msgdata[1] = *(HCAN1_MD41+1);
			can1_msgdata[2] = *(HCAN1_MD41+2);
			can1_msgdata[3] = *(HCAN1_MD41+3);
			can1_msgdata[4] = *(HCAN1_MD41+4);
			can1_msgdata[5] = *(HCAN1_MD41+5);
			can1_msgdata[6] = *(HCAN1_MD41+6);
			can1_msgdata[7] = *(HCAN1_MD41+7);
		}
		if(*HCAN1_RXPRL & RXPRL_RXPR5m) {
			*HCAN1_RXPRL |= RXPRL_RXPR5m;
			CAN1_lastint = (char *)str_recmes_RXPR5;
			//Extract the received data
			can1_msgdata[0] = *(HCAN1_MD51+0);
			can1_msgdata[1] = *(HCAN1_MD51+1);
			can1_msgdata[2] = *(HCAN1_MD51+2);
			can1_msgdata[3] = *(HCAN1_MD51+3);
			can1_msgdata[4] = *(HCAN1_MD51+4);
			can1_msgdata[5] = *(HCAN1_MD51+5);
			can1_msgdata[6] = *(HCAN1_MD51+6);
			can1_msgdata[7] = *(HCAN1_MD51+7);
		}
		if(*HCAN1_RXPRL & RXPRL_RXPR6m) {
			*HCAN1_RXPRL |= RXPRL_RXPR6m;
			CAN1_lastint = (char *)str_recmes_RXPR6;
			//Exract the received data
			can1_msgdata[0] = *(HCAN1_MD61+0);
			can1_msgdata[1] = *(HCAN1_MD61+1);
			can1_msgdata[2] = *(HCAN1_MD61+2);
			can1_msgdata[3] = *(HCAN1_MD61+3);
			can1_msgdata[4] = *(HCAN1_MD61+4);
			can1_msgdata[5] = *(HCAN1_MD61+5);
			can1_msgdata[6] = *(HCAN1_MD61+6);
			can1_msgdata[7] = *(HCAN1_MD61+7);
		}
		if(*HCAN1_RXPRL & RXPRL_RXPR7m) {
			*HCAN1_RXPRL |= RXPRL_RXPR7m;
			CAN1_lastint = (char *)str_recmes_RXPR7;
			//Exract the received data
			can1_msgdata[0] = *(HCAN1_MD71+0);
			can1_msgdata[1] = *(HCAN1_MD71+1);
			can1_msgdata[2] = *(HCAN1_MD71+2);
			can1_msgdata[3] = *(HCAN1_MD71+3);
			can1_msgdata[4] = *(HCAN1_MD71+4);
			can1_msgdata[5] = *(HCAN1_MD71+5);
			can1_msgdata[6] = *(HCAN1_MD71+6);
			can1_msgdata[7] = *(HCAN1_MD71+7);
		}
	 	if(*HCAN1_RXPRH & RXPRH_RXPR8m) {
			*HCAN1_RXPRH |= RXPRH_RXPR8m;
			CAN1_lastint = (char *)str_recmes_RXPR8;
			//Exract the received data
			can1_msgdata[0] = *(HCAN1_MD81+0);
			can1_msgdata[1] = *(HCAN1_MD81+1);
			can1_msgdata[2] = *(HCAN1_MD81+2);
			can1_msgdata[3] = *(HCAN1_MD81+3);
			can1_msgdata[4] = *(HCAN1_MD81+4);
			can1_msgdata[5] = *(HCAN1_MD81+5);
			can1_msgdata[6] = *(HCAN1_MD81+6);
			can1_msgdata[7] = *(HCAN1_MD81+7);
		}
		if(*HCAN1_RXPRH & RXPRH_RXPR9m) {
			*HCAN1_RXPRH |= RXPRH_RXPR9m;
			CAN1_lastint = (char *)str_recmes_RXPR9;
			//Exract the received data
			can1_msgdata[0] = *(HCAN1_MD91+0);
			can1_msgdata[1] = *(HCAN1_MD91+1);
			can1_msgdata[2] = *(HCAN1_MD91+2);
			can1_msgdata[3] = *(HCAN1_MD91+3);
			can1_msgdata[4] = *(HCAN1_MD91+4);
			can1_msgdata[5] = *(HCAN1_MD91+5);
			can1_msgdata[6] = *(HCAN1_MD91+6);
			can1_msgdata[7] = *(HCAN1_MD91+7);
		}
		if(*HCAN1_RXPRH & RXPRH_RXPR10m) {
			*HCAN1_RXPRH |= RXPRH_RXPR10m;
			CAN1_lastint = (char *)str_recmes_RXPR10;
			//Exract the received data
			can1_msgdata[0] = *(HCAN1_MD101+0);
			can1_msgdata[1] = *(HCAN1_MD101+1);
			can1_msgdata[2] = *(HCAN1_MD101+2);
			can1_msgdata[3] = *(HCAN1_MD101+3);
			can1_msgdata[4] = *(HCAN1_MD101+4);
			can1_msgdata[5] = *(HCAN1_MD101+5);
			can1_msgdata[6] = *(HCAN1_MD101+6);
			can1_msgdata[7] = *(HCAN1_MD101+7);
		}
		if(*HCAN1_RXPRH & RXPRH_RXPR11m) {
			*HCAN1_RXPRH |= RXPRH_RXPR11m;
			CAN1_lastint = (char *)str_recmes_RXPR11;
			//Exract the received data
			can1_msgdata[0] = *(HCAN1_MD111+0);
			can1_msgdata[1] = *(HCAN1_MD111+1);
			can1_msgdata[2] = *(HCAN1_MD111+2);
			can1_msgdata[3] = *(HCAN1_MD111+3);
			can1_msgdata[4] = *(HCAN1_MD111+4);
			can1_msgdata[5] = *(HCAN1_MD111+5);
			can1_msgdata[6] = *(HCAN1_MD111+6);
			can1_msgdata[7] = *(HCAN1_MD111+7);
		}
		if(*HCAN1_RXPRH & RXPRH_RXPR12m) {
			*HCAN1_RXPRH |= RXPRH_RXPR12m;
			CAN1_lastint = (char *)str_recmes_RXPR12;
			//Exract the received data
			can1_msgdata[0] = *(HCAN1_MD121+0);
			can1_msgdata[1] = *(HCAN1_MD121+1);
			can1_msgdata[2] = *(HCAN1_MD121+2);
			can1_msgdata[3] = *(HCAN1_MD121+3);
			can1_msgdata[4] = *(HCAN1_MD121+4);
			can1_msgdata[5] = *(HCAN1_MD121+5);
			can1_msgdata[6] = *(HCAN1_MD121+6);
			can1_msgdata[7] = *(HCAN1_MD121+7);
		}
		if(*HCAN1_RXPRH & RXPRH_RXPR13m) {
			*HCAN1_RXPRH |= RXPRH_RXPR13m;
			CAN1_lastint = (char *)str_recmes_RXPR13;
			//Exract the received data
			can1_msgdata[0] = *(HCAN1_MD131+0);
			can1_msgdata[1] = *(HCAN1_MD131+1);
			can1_msgdata[2] = *(HCAN1_MD131+2);
			can1_msgdata[3] = *(HCAN1_MD131+3);
			can1_msgdata[4] = *(HCAN1_MD131+4);
			can1_msgdata[5] = *(HCAN1_MD131+5);
			can1_msgdata[6] = *(HCAN1_MD131+6);
			can1_msgdata[7] = *(HCAN1_MD131+7);
		}
		if(*HCAN1_RXPRH & RXPRH_RXPR14m) {
			*HCAN1_RXPRH |= RXPRH_RXPR14m;
			CAN1_lastint = (char *)str_recmes_RXPR14;
			//Exract the received data
			can1_msgdata[0] = *(HCAN1_MD141+0);
			can1_msgdata[1] = *(HCAN1_MD141+1);
			can1_msgdata[2] = *(HCAN1_MD141+2);
			can1_msgdata[3] = *(HCAN1_MD141+3);
			can1_msgdata[4] = *(HCAN1_MD141+4);
			can1_msgdata[5] = *(HCAN1_MD141+5);
			can1_msgdata[6] = *(HCAN1_MD141+6);
			can1_msgdata[7] = *(HCAN1_MD141+7);
		}
		if(*HCAN1_RXPRH & RXPRH_RXPR15m) {
			*HCAN1_RXPRH |= RXPRH_RXPR15m;
			CAN1_lastint = (char *)str_recmes_RXPR15;
			//Exract the received data
			can1_msgdata[0] = *(HCAN1_MD151+0);
			can1_msgdata[1] = *(HCAN1_MD151+1);
			can1_msgdata[2] = *(HCAN1_MD151+2);
			can1_msgdata[3] = *(HCAN1_MD151+3);
			can1_msgdata[4] = *(HCAN1_MD151+4);
			can1_msgdata[5] = *(HCAN1_MD151+5);
			can1_msgdata[6] = *(HCAN1_MD151+6);
			can1_msgdata[7] = *(HCAN1_MD151+7);
		}


		if(*HCAN1_RXPR!=0) {
			//Temporaly delete unused flags
			*HCAN1_RXPR = 0xFFFF;
			CAN1_lastint = (char *)str_recmes;
		}
	}

	if(*HCAN1_IRRL & IRRL_IRR2m) {
		//HCAN1 Remote frame request interrupt
		//Must clear all the fucked message flags
		*HCAN1_RFPR = 0xFFFF;
		CAN1_lastint = (char *)str_reqmes;
		remote++;
		led_blink(&timer_led_can1,LED_CAN1,LED_SHORT_BLINK_TIME);
	}

	if(*HCAN1_IRRL & IRRL_IRR3m) {
		//HCAN1 Transmit overload warning interrupt
		*HCAN1_IRRL |= IRRL_IRR3m;
		CAN1_lastint = (char *)str_transovwarn;
	}
	
	if(*HCAN1_IRRL & IRRL_IRR4m) {
		//HCAN1 Receive overload warning interrupt
		*HCAN1_IRRL |= IRRL_IRR4m;
	}

	if(*HCAN1_IRRL & IRRL_IRR5m) {
		//HCAN1 Error passive interrupt
		*HCAN1_IRRL |= IRRL_IRR5m;
		CAN0_lastint = (char *)str_errpass;
	}

		if(*HCAN1_IRRL & IRRL_IRR6m) {
		//HCAN1 Bus off interrupt
		*HCAN1_IRRL |= IRRL_IRR6m;
	}

	if(*HCAN1_IRRL & IRRL_IRR7m) {
		//HCAN1 Overload frame interrupt
		*HCAN1_IRRL |= IRRL_IRR7m;
	}

	if(*HCAN1_IRRH & IRRH_IRR12m) {
		//HCAN1 Bus operation interrupt
		*HCAN1_IRRH |= IRRH_IRR12m;
	}
	
	if(*HCAN1_IRRH & IRRH_IRR9m) {
		//HCAN1 Unread interrupt
		//*HCAN1_IRRH |= IRRH_IRR9m;
		*HCAN1_UMSR = 0xFFFF;
		CAN1_lastint = (char *)str_unread;
	}

	if(*HCAN1_IRRH & IRRH_IRR8m) {
		//HCAN1 Mailbox empty interrupt 
		*HCAN1_IRRH |= IRRH_IRR8m;
		CAN1_lastint = (char *)str_mbempty;
	}

}
