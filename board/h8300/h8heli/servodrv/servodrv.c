/* procesor H8S/2638 ver 1.1  */
#include <stdio.h>
#include <stdint.h>
#include <cpu_def.h>
#include <h8s2638h.h>
#include <system_def.h>
#include <string.h>
#include <boot_fn.h>
#include <periph/sci_rs232.h>
#include "mycan.h"
#include "servoout.h"
#include "servodrv.h"


    /* void exit(int status) */
    /* { */
    /*   while(1); */
    /* } */

int cnt = 0;
int ints = 0;
int i = 0;
int timer_1s = 0;
int count_AD = 0;

#define MAXTIME 32767			//Maximum of time_after_start
#define STARTUP_TIME 50*15 		//Time for calibration
int time_after_start = 0;		//Counting time after start, will stop at MAXTIME

int heli_mode = HELI_MODE_INIT;

int timer_led_can0 = LED_SHORT_BLINK_TIME;
int timer_led_can1 = LED_SHORT_BLINK_TIME;

int allow_direct_wd_refresh = 0;	//Hack for WD refresh during printf


#define SER_MODE_IDLE 0
#define SER_MODE_COMMAND 1

int serial_mode = SER_MODE_IDLE;
int serial_command = 0;
int awaited_chars = 0;
int received_chars = 0;

#define REC_LENGTH 16
char receive_array[REC_LENGTH];

//Debuging flags
int debug_voltages = 0;
int debug_CAN = 0;	
int debug_servos = 0;
int debug_main_prog = 0;


int ad0,ad1,ad2,ad3;

int ad0_val = 0;
int ad1_val = 0;
int ad2_val = 0;
int ad3_val = 0;

#define ad0_mul 37
#define ad0_shiftr 9
#define ad1_mul 40
#define ad1_shiftr 8

/* If you want to spare memory and only have support for SCI channels
 * 0 and 1 uncomment the following block. */
#if 1
sci_info_t *sci_rs232_chan_array[] = {
    &sci_rs232_chan0,
    &sci_rs232_chan1
};
#endif

static void deb_led_out(char val)
{
    if (val&1)
        DEB_LED_ON(0);
    else
        DEB_LED_OFF(0);
    if (val&2)
        DEB_LED_ON(1);
    else
        DEB_LED_OFF(1);
    if (val&4)
        DEB_LED_ON(2);
    else
        DEB_LED_OFF(2);
    if (val&8)
        DEB_LED_ON(3);
    else
        DEB_LED_OFF(3);
}

void init()
{/* set shaddow registers */
/*    DIO_P1DDR_shadow=0;
    DIO_P3DDR_shadow=0;  */

/*    *DIO_PCDR=0x00;
    SHADOW_REG_SET(DIO_PJDDR,0xee); / * set PJ.1, PJ.2, PJ.3 LED output * /

    *DIO_P3DR|=0xc5;
    SHADOW_REG_SET(DIO_P3DDR,0x85); */
    
    /* show something on debug leds */
    //deb_led_out(2);  
    //*DIO_PJDR =0x00;
    //FlWait(1*100000);

	//Initialize WD pin as output
    SHADOW_REG_SET(DIO_PCDDR,PCDDR_PC7DDRm);

	DEB_LED_INIT();

}


void led_blink(int* timer, int mask, int time) {
	if(time > *timer) {
		DEB_LED_ON(mask);
		*timer = time;
	}
}

void debug_main(void) {
	printf("STATE: ");
	switch(heli_mode) {
		case HELI_MODE_INIT:	printf("INIT, ");break;
		case HELI_MODE_RUN:		printf("RUN,  ");break;
		default:				printf("????, ");break;	
	}
	printf("Uptime: %i s, ",(time_after_start/50));
	printf("\n");
}

//Interrupt routine
void  LightOn(void) __attribute__ ((interrupt_handler));
void LightOn(void)
{
	*TPU_TSR1 &= ~TSR1_TCFVm ; //reset overflow flag
	*TPU_TCNT1 = 0xFFFF - 29*1000;
	//i = (i + 1) & 7;
	ints++;
	//FlWait(1*200000);
}  

int main() {
	int received;
	int out_num=-1,out_val=-1;


	cli(); //Stop interrupts

	*DIO_PADR|=0x0f;
	*DIO_PADDR=0x0f;		/* A16-A19 are outputs */

  
	excptvec_initfill(LightOn,0);
 
	excptvec_set(42,LightOn);

	init_CAN_interrupts();
	init_servo_outputs();


    //init_hw();
 	init();    

	i = 0;

	deb_led_out(6); 
	FlWait(1*2000);

	sci_rs232_chan_default = 0;
	sci_rs232_setmode(57600, 0, 0, 0);

	init_CAN0();
	init_CAN1();

	init_AD_converter();

	sti();

	print_introduction();
	while (1) {
		received = sci_rs232_recch(0);
		if(received!=-1) {
			switch (serial_mode) {
			case SER_MODE_COMMAND:
				printf("Got char %c in command mode. Total chars: %d, awaiting: %d\n",received,received_chars,awaited_chars);
				receive_array[received_chars++] = (char)received;
				receive_array[received_chars] = 0;
				if(received_chars>=awaited_chars) {
					
					switch(serial_command) {
						case 'O':
							printf("Got sentence: %s\n",receive_array);
							sscanf(receive_array+1,"%4X",&out_val);
							receive_array[1] = 0;
							sscanf(receive_array,"%1X",&out_num);
							printf("Decoded: %d, %X\n",out_num,out_val);
							if((out_num < OUTPUTS_COUNT) && (out_num >= 0)) {
								servo_mirror_input[out_num] = out_val;
							}
							break;
						case 'K':
							printf("Got sentence: %s\n",receive_array);
							sscanf(receive_array,"%2X",&out_val);
							printf("Decoded: %X\n",out_val);
							*rc_maska_can = out_val;
							break;

					}
					serial_mode = SER_MODE_IDLE;received_chars = 0;
				}
				if(received_chars>=REC_LENGTH-1) {
					printf("buffer full\n");
					serial_mode = SER_MODE_IDLE;received_chars = 0;
				}
				break;


			case SER_MODE_IDLE:
				switch (received) {
					case 'H': case 'h': print_help();						break;
					case 'A': case 'a': rc_mode = RC_MODE_AUTO;				break;
					case 'M': case 'm': rc_mode = RC_MODE_MANUAL;			break;
					case 'C': case 'c': debug_CAN 		= ~debug_CAN;		break;
					case 'V': case 'v': debug_voltages	= ~debug_voltages;	break;
					case 'S': case 's': debug_servos	= ~debug_servos;	break;
					case 'P': case 'p': debug_main_prog	= ~debug_main_prog;	break;
					case 'O': case 'o':	awaited_chars = 5; serial_mode = SER_MODE_COMMAND; serial_command = 'O'; break;
					case 'K': case 'k':	awaited_chars = 2; serial_mode = SER_MODE_COMMAND; serial_command = 'K'; break;					
					case '?':			print_short_status();				break;
					default	: printf("For help hit h.\n");					break;
				}
				received_chars = 0;
				break;
			default: serial_mode = SER_MODE_IDLE; break;
		}
	}
		//printf(":%c:\n",received);


	//debug_out_CAN();
	//debug_pwm();
	if(servopulse!=0) {
		servopulse = 0;

		timer_middle_interval();
		watchdog_refresh();
	}
	FlWait(1*20000);

	

  };
 
};

void print_introduction() {
	int temp_wd_rfr = allow_direct_wd_refresh;

	allow_direct_wd_refresh = 1;
	printf("**************************************\n");
	printf("*  H E L I C O P T E R    S E R V O  *\n");
	printf("*         C O N T R O L L E R        *\n");
	printf("*------------------------------------*\n");
	printf("*      Ota Herm, DCE FEE CTU 2006    *\n");
	printf("**************************************\n");
	printf("Compiled: %s, %s\n",__DATE__,__TIME__);
	printf("For help hit h.\n\n");

	allow_direct_wd_refresh = temp_wd_rfr;
}

void print_help() {
	allow_direct_wd_refresh = 1;

	print_introduction();
	printf("KEYS:\n");
	printf("H\tshows this help\n");
	printf("A\tsets to the AUTOMATIC mode\n");
	printf("M\tsets to the MANUAL mode\n");
	printf("V\ttoggles debugging of voltages\n");
	printf("S\ttoggles debugging of servo values\n");
	printf("C\ttoggles debugging of CAN\n");
	printf("P\ttoggles debugging of main program\n");
	printf("OxYYYY\tset value for output number x, value YYYY hexadecimal\n");
	printf("KYY\tset value for mask, value YY hexadecimal\n");
	printf("\n\n");

	allow_direct_wd_refresh = 0;
}

void print_short_status() {
	allow_direct_wd_refresh = 1;
	servo_debug_short();
	allow_direct_wd_refresh = 0;
}

void set_led_living(int state) {
    if (state) {
		DEB_LED_ON(LED_LIVING);
	} else {
		DEB_LED_OFF(LED_LIVING);
	}
}

/**
*	50Hz timer ... to be short as possible
**/
void timer_middle_interval(void) {
	if(heli_mode == HELI_MODE_INIT) {
		//During the startup time blink the LED rapidly
		if((timer_1s % 5) > 2) {
			set_led_living(0);
		} else {
			set_led_living(1);
		}
	} else {
		//After startup time blink the LED slowly
		if (timer_1s>25) {
			set_led_living(0);
		} else {
			set_led_living(1);
		}
	}

	if(timer_led_can0 > 0) {
		DEB_LED_ON(LED_CAN0);
		timer_led_can0--;
		if(timer_led_can0==0) DEB_LED_OFF(LED_CAN0);
	}
	
	if(timer_led_can1 > 0) {
		DEB_LED_ON(LED_CAN1);
		timer_led_can1--;
		if(timer_led_can1==0) DEB_LED_OFF(LED_CAN1);
	}


	if(timer_1s > 50) {
		timer_1s = 0;

		start_AD_conversion();

		allow_direct_wd_refresh = 1;
		if(debug_main_prog)		debug_main();
		//servo_debug_fast();
		if(debug_servos) 	servo_debug_visual();	
		if(debug_CAN)		debug_out_CAN();
		if(debug_voltages)	debug_AD_convertion();

		cnt++;
		send_temp_message_CAN0();
		send_temp_message_CAN1();

		allow_direct_wd_refresh = 0;
	}
	timer_1s++;
	if(time_after_start < MAXTIME) time_after_start++;
	if(time_after_start < STARTUP_TIME) {
		heli_mode = HELI_MODE_INIT;
	} else {
		heli_mode = HELI_MODE_RUN;
	}
	
}

void init_AD_converter(void) {
	*SYS_MSTPCRA &= ~MSTPCRA_MSTPA1m; //Switch the AD module ON

	excptvec_set(28,int_AD_convert);

	*AD_ADCSR |=  ADCSR_SCANm;	//Set to SCAN MODE
	*AD_ADCSR &= ~ADCSR_ADFm;	//Clear INTERRUPT FLAG
	*AD_ADCSR |=  ADCSR_ADIEm;	//Set the INTERRUPT MASK

	//Set the channels AN0 to AN4
	*AD_ADCSR &= ~ADCSR_CH3m;*AD_ADCSR &= ~ADCSR_CH2m;*AD_ADCSR |=  ADCSR_CH1m;	*AD_ADCSR |=  ADCSR_CH0m;

	//Set the LOWEST CONVERSION SPEED
	*AD_ADCR &= ~ADCR_CKS0m;*AD_ADCR &= ~ADCR_CKS1m;
	
	//Trigger the convertion by software	
	*AD_ADCR &= ~ADCR_TRGS0m;*AD_ADCR &= ~ADCR_TRGS1m;

	//Now the AD converter should be prepared
}


void start_AD_conversion(void) {	
	*AD_ADCSR |= ADCSR_ADSTm;
}

void debug_AD_convertion(void) {
	printf("AD:ints:%d,",count_AD);
	printf("VAL:%02X%02X,%02X%02X,%02X%02X,%02X%02X\n", *AD_ADDRAH,*AD_ADDRAL,*AD_ADDRBH,*AD_ADDRBL,*AD_ADDRCH,*AD_ADDRCL,*AD_ADDRDH,*AD_ADDRDL);
	printf("val:%4X,%4X,%4X,%4X\n",ad0,ad1,ad2,ad3);
	printf("val:%4d,%4d,%4d,%4d\n",ad0_val,ad1_val,ad2_val,ad3_val);
}

/**
*	AD convertion finished interrupt
**/
void int_AD_convert(void) {
	unsigned int ad0_temp,ad1_temp;
	

	*AD_ADCSR &= ~ADCSR_ADFm;	//Clear INTERRUPT FLAG
	count_AD++;
		
	ad0 = (((*AD_ADDRAH) << 8) + *AD_ADDRAL);
	ad1 = (((*AD_ADDRBH) << 8) + *AD_ADDRBL);
	ad2 = (((*AD_ADDRCH) << 8) + *AD_ADDRCL);
	ad3 = (((*AD_ADDRDH) << 8) + *AD_ADDRDL);

	ad0_temp = (ad0 >> 6) & 0x03FF;ad1_temp = (ad1 >> 6) & 0x03FF;	ad2_val = (ad2 >> 6) & 0x03FF;	ad3_val = (ad3 >> 6) & 0x03FF;
	ad0_val = (ad0_temp * ad0_mul) >> ad0_shiftr;
	ad1_val = (ad1_temp * ad1_mul) >> ad1_shiftr;
	*AD_ADCSR &= ~ADCSR_ADSTm;

}

void watchdog_refresh(void) {
		//Complement Watchdog
		*DIO_PCDR ^= PCDR_PC7DRm;
}
