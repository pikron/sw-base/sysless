/* H8HELI  MAIN PROGRAM*
 * Ota Herm, 2005      */

#ifndef _servodrv_h_
#define _servodrv_h_



void stub_deb_led_out(char val);


void watchdog_refresh(void);
void timer_middle_interval(void);

#define HELI_MODE_INIT 	0
#define HELI_MODE_RUN	1
extern int heli_mode;

//Led definition
#define LED_LIVING		0	//Green LED near to the watchdog
#define LED_AUTO_MAN	1	//Red LED near the eeprom
#define LED_CAN0		2	//Orange LED near the CAN0 driver
#define LED_CAN1		3	//Orange LED near the CAN1 driver

#define LED_TICK_BLINK_TIME 5
#define LED_SHORT_BLINK_TIME 10
#define LED_LONG_BLINK_TIME 50

extern int timer_led_can0;
extern int timer_led_can1;

void led_blink(int* timer, int mask, int time);

extern int allow_direct_wd_refresh;

void init_AD_converter(void);
void start_AD_conversion(void);
void debug_AD_convertion(void);

void int_AD_convert(void) __attribute__ ((interrupt_handler));
void print_help();
void print_introduction();
void print_short_status();

extern int ad0_val;
extern int ad1_val;
extern int ad2_val;
extern int ad3_val;


#endif
