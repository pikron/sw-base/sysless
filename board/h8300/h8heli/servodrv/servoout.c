/* H8HELI SERVO OUTPUT DRIVER	*
 * Ota Herm, 2005				*/

#include <stdio.h>
#include <stdint.h>
#include <cpu_def.h>
#include <h8s2638h.h>
#include <system_def.h>
#include <string.h>
#include <boot_fn.h>
#include <periph/sci_rs232.h>
#include "mycan.h"
#include "servodrv.h"
#include "servoout.h"

int servo_time_L[INPUTS_COUNT] = {0,0,0,0,0,0,0};	//Falling edge capture time
int servo_time_H[INPUTS_COUNT] = {0,0,0,0,0,0,0};	//Rising edge capture time
int servo_length[INPUTS_COUNT] = {0,0,0,0,0,0,0};	//INPUT length
int sig_init[INPUTS_COUNT] = {0,0,0,0,0,0,0}; 		//Not the first pulse
int count50Hz = 0;						//For checking and maybe timing
int servo_value[OUTPUTS_COUNT] = {0x7E2,0x7E2,0x7E2,0x7E2,0x7E2,0x7E2}; //OUTPUT pulse length
int rc_errors[INPUTS_COUNT] = {0,0,0,0,0,0,0};		//RC bad pulses counter (on INPUT)

int servo_min[INPUTS_COUNT] = {32767,32767,32767,32767,32767,32767,32767};
int servo_max[INPUTS_COUNT] = {0,0,0,0,0,0,0};

int servo_in_val[INPUTS_COUNT] = {-3,-3,-3,-3,-3,-3,-3};

int servopulse = 0;						//Acknowledge of servo function

int rc_mode = RC_MODE_MANUAL;							//Auto/Manual mode

//Pointers to place where to store the output to servos (directly to the TPU units)
int *servo_pwm_value[OUTPUTS_COUNT] = {(int*)TPU_TGR0A,(int*)TPU_TGR0B,(int*)TPU_TGR0C,(int*)TPU_TGR0D,(int*)TPU_TGR1A,(int*)TPU_TGR1B};

//Pointers to place to get the values received over CAN bus
int servo_mirror_input[7] = {0,0,0,0,0,0,0};
int servo_mirror_output[7] = {0,0,0,0,0,0,0};

//Value telling whether to use for servos the values from PFC or RC receirver
unsigned char* rc_maska_can = (unsigned char*)HCAN0_MD11+0;		
const unsigned char maska_fixed_man = 0x00; //Mask for MANUAL control (all channels to RC)
const unsigned char* rc_maska = &maska_fixed_man;

#define servo_period 25000; 			//Basic timer period (dependent on system freq.)


void servo_debug_short() {
	int n;
	printf("M:%02X,",*rc_maska);
	printf("RC:");
	for(n=0;n<INPUTS_COUNT;n++) printf("%04X,",servo_mirror_output[n]);
	printf("O:");
	for(n=0;n<OUTPUTS_COUNT;n++) printf("%04X,",servo_mirror_input[n]);
	printf("\n");
}

void servo_debug_visual(void) {
	int n,m;
	for(n=0;n<7;n++) {
		printf("%1d:",n);
		if(((*rc_maska) >> n) & 0x01) {
			printf("A:");
		} else {
			printf("M:");
		}

		for(m=0;m<=256;m+=16) {
			if(servo_in_val[n] < m) {
				printf("-");
			} else {
				printf("#");
			}
		}
	}
	printf("\n");
	printf("PFC: ");
	for(n=0;n<7;n++) {
		printf("%X, ",servo_mirror_input[n]);
	}
	printf("\n");
}


void servo_debug_fast(void) {
	int n;
	if(rc_mode==RC_MODE_AUTO) {
		printf("AUTO;");
	} else {
		printf("MAN; ");
	}
	printf("\n");
	printf("Len: ");for(n=0;n<7;n++) {printf("%i~%5d ",n,servo_length[n]);};printf("\n");
	printf("Val: ");for(n=0;n<7;n++) {printf("%i~%5d ",n,servo_time2val(n,servo_length[n]));};printf("\n");
	printf("Min: ");for(n=0;n<7;n++) {printf("%i~%5d ",n,servo_min[n]);};printf("\n");
	printf("Max: ");for(n=0;n<7;n++) {printf("%i~%5d ",n,servo_max[n]);};printf("\n");
	printf("Err: ");for(n=0;n<7;n++) {printf("%i~%5d ",n,rc_errors[n]);};printf("\n");
}

void debug_pwm(void) {
	int TCNT0,TCNT1,TCNT2,TCNT3,TCNT4,TCNT5;
	TCNT0 = *TPU_TCNT0; TCNT1 = *TPU_TCNT1; TCNT2 = *TPU_TCNT2; TCNT3 = *TPU_TCNT3;	TCNT4 = *TPU_TCNT4; TCNT5 = *TPU_TCNT5;

	printf("--------TPU--------\n0: TCR0:%2X, TMDR0:%2X, TIOR0H: %2X, TIOR0L: %2X, TIER0: %2X, TSR0: %2X, TCNT0: %4X TGR0A: %4X,TGR0B: %4X,TGR0C: %4X,TGR0D: %4X\n",
		*TPU_TCR0,*TPU_TMDR0,*TPU_TIOR0H,*TPU_TIOR0L,*TPU_TIER0,*TPU_TSR0,TCNT0,
		*TPU_TGR0A,*TPU_TGR0B,*TPU_TGR0C,*TPU_TGR0D);
	printf("1: TCR1:%2X, TMDR1:%2X, TIOR1:  %2X,              TIER1: %2X, TSR1: %2X, TCNT1: %4X TGR1A: %4X,TGR1B: %4X\n",
		*TPU_TCR1,*TPU_TMDR1,*TPU_TIOR1,*TPU_TIER1,*TPU_TSR1,TCNT1,
		*TPU_TGR1A,*TPU_TGR1B);
	printf("2: TCR2:%2X, TMDR2:%2X, TIOR2:  %2X,              TIER2: %2X, TSR2: %2X, TCNT2: %4X TGR2A: %4X,TGR2B: %4X\n",
		*TPU_TCR2,*TPU_TMDR2,*TPU_TIOR2,*TPU_TIER2,*TPU_TSR2,TCNT2,
		*TPU_TGR2A,*TPU_TGR2B);
	printf("3: TCR3:%2X, TMDR3:%2X, TIOR3H: %2X, TIOR3L: %2X, TIER3: %2X, TSR3: %2X, TCNT3: %4X TGR3A: %4X,TGR3B: %4X,TGR3C: %4X,TGR3D: %4X\n",
		*TPU_TCR3,*TPU_TMDR3,*TPU_TIOR3H,*TPU_TIOR3L,*TPU_TIER3,*TPU_TSR3,TCNT3,
		*TPU_TGR3A,*TPU_TGR3B,*TPU_TGR3C,*TPU_TGR3D);
	printf("4: TCR4:%2X, TMDR4:%2X, TIOR4:  %2X,              TIER4: %2X, TSR4: %2X, TCNT4: %4X TGR4A: %4X,TGR4B: %4X\n",
		*TPU_TCR4,*TPU_TMDR4,*TPU_TIOR4,*TPU_TIER4,*TPU_TSR4,TCNT4,
		*TPU_TGR4A,*TPU_TGR4B);
	printf("5: TCR5:%2X, TMDR5:%2X, TIOR5:  %2X,              TIER5: %2X, TSR5: %2X, TCNT5: %4X TGR5A: %4X,TGR5B: %4X\n",
		*TPU_TCR5,*TPU_TMDR5,*TPU_TIOR5,*TPU_TIER5,*TPU_TSR5,TCNT5,
		*TPU_TGR5A,*TPU_TGR5B);

	printf("Servo 1: 1: %4X, 0: %4X, L: %4X\n",servo_time_H[0],servo_time_L[0],servo_length[0]);
	printf("Servo 2: 1: %4X, 0: %4X, L: %4X\n",servo_time_H[1],servo_time_L[1],servo_length[1]);
	printf("Servo 3: 1: %4X, 0: %4X, L: %4X\n",servo_time_H[2],servo_time_L[2],servo_length[2]);
	printf("Servo 4: 1: %4X, 0: %4X, L: %4X\n",servo_time_H[3],servo_time_L[3],servo_length[3]);
	printf("Servo 5: 1: %4X, 0: %4X, L: %4X\n",servo_time_H[4],servo_time_L[4],servo_length[4]);
	printf("Servo 6: 1: %4X, 0: %4X, L: %4X\n",servo_time_H[5],servo_time_L[5],servo_length[5]);
	printf("Servo 7: 1: %4X, 0: %4X, L: %4X\n",servo_time_H[6],servo_time_L[6],servo_length[6]);

	printf("50Hz int check: %5d\n",count50Hz);
}

void init_servo_outputs(void) {
	//Configure the pins for OUTPUTS
	SHADOW_REG_SET(DIO_P1DDR,(DIO_P1DDR_shadow | P1DDR_P10DDRm | P1DDR_P11DDRm| P1DDR_P12DDRm | P1DDR_P13DDRm| P1DDR_P14DDRm | P1DDR_P15DDRm));
	//The inputs are defined default (TODO Define them explicitly)

	//Power TPU unit
	*SYS_MSTPCRA &= ~MSTPCRA_TPUm; 

	//Select counter clock with bits TPSC2 to TPSC0 in TCR,
	//select the input clock edge with CKEG1 and CKEG0 in TCR
	*TPU_TCR0 &= ~TCR0_TPSC0m;	*TPU_TCR0 |=  TCR0_TPSC1m;	*TPU_TCR0 &= ~TCR0_TPSC2m;
	*TPU_TCR0 &= ~TCR0_CKEG0m;	*TPU_TCR0 &= ~TCR0_CKEG1m;

	*TPU_TCR1 &= ~TCR1_TPSC0m;	*TPU_TCR1 |=  TCR1_TPSC1m;	*TPU_TCR1 &= ~TCR1_TPSC2m;
	*TPU_TCR1 &= ~TCR1_CKEG0m;	*TPU_TCR1 &= ~TCR1_CKEG1m;

	*TPU_TCR2 &= ~TCR2_TPSC0m;	*TPU_TCR2 |=  TCR2_TPSC1m;	*TPU_TCR2 &= ~TCR2_TPSC2m;
	*TPU_TCR2 &= ~TCR2_CKEG0m;	*TPU_TCR2 &= ~TCR2_CKEG1m;

	*TPU_TCR3 &= ~TCR3_TPSC0m;	*TPU_TCR3 |=  TCR3_TPSC1m;	*TPU_TCR3 &= ~TCR3_TPSC2m;
	*TPU_TCR3 &= ~TCR3_CKEG0m;	*TPU_TCR3 &= ~TCR3_CKEG1m;

	*TPU_TCR4 &= ~TCR4_TPSC0m;	*TPU_TCR4 |=  TCR4_TPSC1m;	*TPU_TCR4 &= ~TCR4_TPSC2m;
	*TPU_TCR4 &= ~TCR4_CKEG0m;	*TPU_TCR4 &= ~TCR4_CKEG1m;

	
	//Use bits CCLR2 to CCLR0 in TCR to select the TGR
	//to be used as the TCNT clearing source
	*TPU_TCR0 |=  TCR0_CCLR0m;
	*TPU_TCR0 |=  TCR0_CCLR1m;
	*TPU_TCR0 &= ~TCR0_CCLR2m;
	
	*TPU_TCR1 |=  TCR1_CCLR0m;
	*TPU_TCR1 |=  TCR1_CCLR1m;

	*TPU_TCR2 |=  TCR2_CCLR0m;
	*TPU_TCR2 |=  TCR2_CCLR1m;

	*TPU_TCR3 |=  TCR3_CCLR0m;
	*TPU_TCR3 |=  TCR3_CCLR1m;
	*TPU_TCR3 &= ~TCR3_CCLR2m;

	*TPU_TCR4 &= ~TCR4_CCLR0m;
	*TPU_TCR4 |=  TCR4_CCLR1m;

	//Select synchronous operation
	*TPU_TSYR |= TSYR_SYNC0m;
	*TPU_TSYR |= TSYR_SYNC1m;
	*TPU_TSYR |= TSYR_SYNC2m;
	*TPU_TSYR |= TSYR_SYNC3m;
	*TPU_TSYR |= TSYR_SYNC4m;


	//Use TIOR to designate the TGR as an output compare or input capture
	//register, and select the initial value and output value
	//All outputs should be set to Output Compare, initial value 1, after compare set to 0
	/*Output 1*/*TPU_TIOR0H	|=  TIOR0H_IOA0m;	*TPU_TIOR0H &= ~TIOR0H_IOA1m;	*TPU_TIOR0H |=  TIOR0H_IOA2m;	*TPU_TIOR0H &= ~TIOR0H_IOA3m;
	/*Output 2*/*TPU_TIOR0H	|=  TIOR0H_IOB0m;	*TPU_TIOR0H &= ~TIOR0H_IOB1m;	*TPU_TIOR0H	|=  TIOR0H_IOB2m;	*TPU_TIOR0H &= ~TIOR0H_IOB3m;
	/*Output 3*/*TPU_TIOR0L	|=  TIOR0L_IOC0m;	*TPU_TIOR0L &= ~TIOR0L_IOC1m;	*TPU_TIOR0L |=  TIOR0L_IOC2m;	*TPU_TIOR0L &= ~TIOR0L_IOC3m;
	/*Output 4*/*TPU_TIOR0L	|=  TIOR0L_IOD0m;	*TPU_TIOR0L &= ~TIOR0L_IOD1m;	*TPU_TIOR0L |=  TIOR0L_IOD2m;	*TPU_TIOR0L &= ~TIOR0L_IOD3m;
	/*Output 5*/*TPU_TIOR1	|=  TIOR1_IOA0m;	*TPU_TIOR1	&= ~TIOR1_IOA1m;	*TPU_TIOR1 	|=  TIOR1_IOA2m;	*TPU_TIOR1 	&= ~TIOR1_IOA3m;
	/*Output 6*/*TPU_TIOR1	|=  TIOR1_IOB0m;	*TPU_TIOR1	&= ~TIOR1_IOB1m;	*TPU_TIOR1 	|=  TIOR1_IOB2m;	*TPU_TIOR1	&= ~TIOR1_IOB3m;

	/*Input 1*/*TPU_TIOR2	&= ~TIOR2_IOA0m;	*TPU_TIOR2 	|=  TIOR2_IOA1m;	*TPU_TIOR2	&= ~TIOR2_IOA2m;	*TPU_TIOR2	|=  TIOR2_IOA3m;
	/*Input 2*/*TPU_TIOR2	&= ~TIOR2_IOB0m;	*TPU_TIOR2 	|=  TIOR2_IOB1m;	*TPU_TIOR2	&= ~TIOR2_IOB2m;	*TPU_TIOR2	|=  TIOR2_IOB3m;
	/*Input 3*/*TPU_TIOR3H	&= ~TIOR3H_IOA0m;	*TPU_TIOR3H	|=  TIOR3H_IOA1m;	*TPU_TIOR3H	&= ~TIOR3H_IOA2m;	*TPU_TIOR3H	|=  TIOR3H_IOA3m;
	/*Input 4*/*TPU_TIOR3H	&= ~TIOR3H_IOB0m;	*TPU_TIOR3H	|=  TIOR3H_IOB1m;	*TPU_TIOR3H	&= ~TIOR3H_IOB2m;	*TPU_TIOR3H	|=  TIOR3H_IOB3m;
	/*Input 5*/*TPU_TIOR3L	&= ~TIOR3L_IOC0m;	*TPU_TIOR3L	|=  TIOR3L_IOC1m;	*TPU_TIOR3L	&= ~TIOR3L_IOC2m;	*TPU_TIOR3L	|=  TIOR3L_IOC3m;
	/*Input 6*/*TPU_TIOR3L	&= ~TIOR3L_IOD0m;	*TPU_TIOR3L	|=  TIOR3L_IOD1m;	*TPU_TIOR3L	&= ~TIOR3L_IOD2m;	*TPU_TIOR3L	|=  TIOR3L_IOD3m;
	/*Input 7*/*TPU_TIOR4	&= ~TIOR4_IOA0m;	*TPU_TIOR4	|=  TIOR4_IOA1m;	*TPU_TIOR4	&= ~TIOR4_IOA2m;	*TPU_TIOR4	|=  TIOR4_IOA3m;
	/*Period*/ *TPU_TIOR4	&= ~TIOR4_IOB0m;	*TPU_TIOR4	&= ~TIOR4_IOB1m;	*TPU_TIOR4	&= ~TIOR4_IOB2m;	*TPU_TIOR4	&= ~TIOR4_IOB3m;

	//Set the cycle in the TGR selected before, and set 
	//the duty in the other the TGR
	*TPU_TGR0A = 0;	*TPU_TGR0B = 0;	*TPU_TGR0C = 0;	*TPU_TGR0D = 0;	*TPU_TGR1A = 0;	*TPU_TGR1B = 0;
	//Set PWM period (and max. time for capture)
	*TPU_TGR4B = servo_period; //50Hz with 5MHz crystall

	//Select the PWM mode with bits MD3 to MD0 in TMDR
	//Set TPU0,1 used for OUTPUTS as PWM mode 2
	/*Outputs 1,2,3,4*/	*TPU_TMDR0 |=  TMDR0_MD0m;	*TPU_TMDR0 |=  TMDR0_MD1m;	*TPU_TMDR0 &= ~TMDR0_MD2m;	*TPU_TMDR0 &= ~TMDR0_MD3m;
	/*Outputs 5,6*/		*TPU_TMDR1 |=  TMDR1_MD0m;	*TPU_TMDR1 |=  TMDR1_MD1m;	*TPU_TMDR1 &= ~TMDR1_MD2m;	*TPU_TMDR1 &= ~TMDR1_MD3m;
	//Other TPUs are default set to normal mode (TODO Set them explicitely)

	//Set the CST bit in TSTR to 1 to start the count operation
	*TPU_TSTR |= TSTR_CST0m;
	*TPU_TSTR |= TSTR_CST1m;
	*TPU_TSTR |= TSTR_CST2m;
	*TPU_TSTR |= TSTR_CST3m;
	*TPU_TSTR |= TSTR_CST4m;

	//Init interrupts for inputs
	excptvec_set(44,servo_in_capture_2A);	*TPU_TIER2 |= TIER2_TGIEAm; //Pulse input 1
	excptvec_set(45,servo_in_capture_2B);	*TPU_TIER2 |= TIER2_TGIEBm; //Pulse input 2

	excptvec_set(48,servo_in_capture_3A);	*TPU_TIER3 |= TIER3_TGIEAm; //Pulse input 3
	excptvec_set(49,servo_in_capture_3B);	*TPU_TIER3 |= TIER3_TGIEBm; //Pulse input 4
	excptvec_set(50,servo_in_capture_3C);	*TPU_TIER3 |= TIER3_TGIECm; //Pulse input 5
	excptvec_set(51,servo_in_capture_3D);	*TPU_TIER3 |= TIER3_TGIEDm; //Pulse input 6
	excptvec_set(56,servo_in_capture_4A);	*TPU_TIER4 |= TIER4_TGIEAm; //Pulse input 7

	//Init period interrupt (used to synchronized writing to the PWM modules)
	excptvec_set(57,tpu50Hz);				*TPU_TIER4 |= TIER4_TGIEBm; //Enable 50Hz interrupt
	//Go!
}

/**
*	Calculates the position of the servo (0 - 255) from pulse time
*	according to measured minimal and maximal pulse length
*	int num 	... servo number (for max and min)
*	int lenght 	... pulse lenght
*	returns negative number if something went wrong
**/
int servo_time2val(int num,int length) {
	long value;

	if(servo_min[num]>=servo_max[num]) return -1;
	if((length < servo_min[num]) || (length > servo_max[num])) return -2;

	value = ((long)(length-servo_min[num]))*255/((long)(servo_max[num]-servo_min[num])); 
	return  value;
	//return -1;
}

int pulse_length(int A,int B) {
	if(B>A) return B-A-1;
	return servo_period-A+B;
}

/**
* Calculate the length of the pulse, check the ranges and store it 
* returns 0 if the pulse was actually finished and OK
*/
int servo_capture(int num,int input_mask,int timer_value) {
	int templen;
	if(input_mask) {
		servo_time_L[num] = timer_value;
		sig_init[num] = 1;
		return 0;
	}	else {
		servo_time_H[num] = timer_value;
		if(sig_init[num]==1) {
			templen = pulse_length(servo_time_L[num],servo_time_H[num]);
			if((templen > 1000) && (templen < 3000)) {
				//Store the pulse length
				servo_length[num] = templen;					//Pulse length
				servo_in_val[num] = servo_time2val(num,templen);	//0..255 value
				if(heli_mode == HELI_MODE_INIT) {
					if(templen < servo_min[num]) servo_min[num] = templen;
					if(templen > servo_max[num]) servo_max[num] = templen;
				}
				return 1;
			} else {
				rc_errors[num]++;
				return 0;
			}
		}
		return 0;
	}	
}

/**
* servo input interrupt
**/
void servo_in_capture_2A(void) {
	*TPU_TSR2 &= ~TSR2_TGFAm;
	servo_capture(0,*DIO_PORT1 & PORT1_P16m,*TPU_TGR2A);
}

void servo_in_capture_2B(void) {
	*TPU_TSR2 &= ~TSR2_TGFBm;
	servo_capture(1,*DIO_PORT1 & PORT1_P17m,*TPU_TGR2B);
}

void servo_in_capture_3A(void) {
	*TPU_TSR3 &= ~TSR3_TGFAm;
	servo_capture(2,*DIO_PORTB & PORTB_PB0m,*TPU_TGR3A);
}

void servo_in_capture_3B(void) {
	*TPU_TSR3 &= ~TSR2_TGFBm;
	servo_capture(3,*DIO_PORTB & PORTB_PB1m,*TPU_TGR3B);
}

void servo_in_capture_3C(void) {
	*TPU_TSR3 &= ~TSR3_TGFCm;
	servo_capture(4,*DIO_PORTB & PORTB_PB2m,*TPU_TGR3C);
}

void servo_in_capture_3D(void) {
	*TPU_TSR3 &= ~TSR3_TGFDm;
	servo_capture(5,*DIO_PORTB & PORTB_PB3m,*TPU_TGR3D);
}

/**
* Servo used for distingush AUTO/MANUAL
**/
void servo_in_capture_4A(void) {
	*TPU_TSR4 &= ~TSR4_TGFAm;
	if(servo_capture(6,*DIO_PORTB & PORTB_PB4m,*TPU_TGR4A)) {
		if(servo_length[6]<LENGTH_SWITCH_TO_MAN) {
			if(rc_mode == RC_MODE_AUTO) can_send_servo_manual();
			rc_mode = RC_MODE_MANUAL;
			
			DEB_LED_OFF(LED_AUTO_MAN);
		}
		if(servo_length[6]>LENGTH_SWITCH_TO_AUTO) {
			if(rc_mode == RC_MODE_MANUAL) can_send_servo_auto();
			rc_mode = RC_MODE_AUTO;
			DEB_LED_ON(LED_AUTO_MAN);
		}
	}
}


/**
* Cyclic 50Hz timer
* - synchronized setting of PWMs
* - usable for timing
**/
void tpu50Hz(void) {
	int n;

	*TPU_TSR4 &= ~TSR4_TGFBm;
	count50Hz++;
	
	switch(rc_mode) {
		//Get the right values from HCAN mailboxes acording to the mask value
		case RC_MODE_AUTO:				rc_maska = rc_maska_can;break;
		//Manual mode....the data from RC to be feed to output
		case RC_MODE_MANUAL: default:	rc_maska = &maska_fixed_man;break;
	}

	for(n=0;n<6;n++) {
		if(((*rc_maska) >> n) & 0x01) {
			//Mask for the channel is 1 .. use input from PFC
			servo_value[n] = servo_mirror_input[n];
		} else {
			//Mask for the channel is 0 .. use input from RC
			servo_value[n] = servo_length[n]; 
		}

		//Write the values to the hardware
		*servo_pwm_value[n] = servo_value[n];

		//Prepare the servo length for sending over CAN bus
		//servo_mirror_output[n] = servo_value[n];	//send true servo positions
		servo_mirror_output[n] = servo_length[n];	//send RC servo positions
	}
	//Prepare the last input from RC for sending to CAN bus
	servo_mirror_output[6] = servo_length[6];

	servopulse = 1;
	if(allow_direct_wd_refresh) watchdog_refresh();
}
