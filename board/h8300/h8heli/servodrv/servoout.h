/* H8HELI SERVO OUTPUT DRIVER	*
 * Ota Herm, 2005				*/

/* Using TPU channels x .. x 	*
 * in the PWM2 mode for servo	*
 * controlling					*/

#ifndef _servoout_h_
#define _servoout_h_

#define OUTPUTS_COUNT 6 //Number of servo outputs
#define INPUTS_COUNT 7 //Number of servo inputs

#define LENGTH_SWITCH_TO_MAN 	1625 //less than 1.3 ms
#define LENGTH_SWITCH_TO_AUTO 	2125 //more than 1.7 ms

#define RC_MODE_MANUAL			0		//Servos controlled by RC receiver
#define RC_MODE_AUTO			1		//Servos controlled by Primary Flight Computer:-)
extern int rc_mode;						//Auto/Manual mode

/*To be called from main at startup*/ 
void init_servo_outputs(void);

/*Debugging outputs (writing by printf())*/
void debug_pwm(void);
void servo_debug_fast(void);
void servo_debug_visual(void);

/**
*	Calculates the position of the servo (0 - 255) from pulse time
*	according to measured minimal and maximal pulse length
*	int num 	... servo number (for max and min)
*	int lenght 	... pulse lenght
*	returns negative number if something went wrong
**/
int servo_time2val(int num,int length);

/*Interrupts declaration*/
void servo_in_capture_2A(void) __attribute__ ((interrupt_handler));
void servo_in_capture_2B(void) __attribute__ ((interrupt_handler));
void servo_in_capture_3A(void) __attribute__ ((interrupt_handler));
void servo_in_capture_3B(void) __attribute__ ((interrupt_handler));
void servo_in_capture_3C(void) __attribute__ ((interrupt_handler));
void servo_in_capture_3D(void) __attribute__ ((interrupt_handler));
void servo_in_capture_4A(void) __attribute__ ((interrupt_handler));

void tpu50Hz(void) __attribute__ ((interrupt_handler));

extern int servopulse;

extern void can_send_servo_manual(void);
extern void can_send_servo_auto(void);

extern int servo_mirror_input[7];
extern int servo_mirror_output[7];

extern unsigned char* rc_maska_can;

void servo_debug_short();

#endif
