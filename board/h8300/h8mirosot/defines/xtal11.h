#ifndef __H8MIROSOT_XTAL_H
#define __H8MIROSOT_XTAL_H

#define CPU_REF_HZ 11059200l 	/* reference clock for H8CANUSB   */
#define CPU_SYS_HZ 11059200l	/* default system  for H8CANUSB   */

#endif
