/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  system_def_hi_cpu2.h - definition of hardware adresses and registers
 
  Copyright (C) 2002 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#ifndef _SYSTEM_DEF_HW01_H_
#define _SYSTEM_DEF_HW01_H_

//#define CPU_REF_HZ  11059200l	/* reference clock frequency      */
//#define CPU_SYS_HZ  11059200l	/* default system clock frequency */

//#define CPU_REF_HZ 18423000l 	/* reference clock for EDK2638    */
//#define CPU_SYS_HZ 18423000l	/* default system  for EDK2638    */

#define CPU_REF_HZ  5000000l 	/* reference clock for HI_CPU2    */
#define CPU_SYS_HZ 20000000l	/* default system  for HI_CPU2    */


unsigned long cpu_ref_hz;	/* actual external XTAL reference */
unsigned long cpu_sys_hz;	/* actual system clock frequency  */

volatile unsigned long msec_time;

#define SCI_RS232_CHAN_DEFAULT 1

/* Keyboard KL41 (CS3) */
#define KL41_LCD_INST  (volatile uint8_t * const)(0x700000)
#define KL41_LCD_STAT  (volatile uint8_t * const)(0x700001)
#define KL41_LCD_WDATA (volatile uint8_t * const)(0x700002)
#define KL41_LCD_RDATA (volatile uint8_t * const)(0x700003)
#define KL41_LED_WR    (volatile uint8_t * const)(0x700001)
#define KL41_KBD_WR    (volatile uint8_t * const)(0x700003)
#define KL41_KBD_RD    (volatile uint8_t * const)(0x700004)

#define KL41_SUPPORT_ENABLED



/* SGM Small graphics LCD module 240x64 (CS3) */
#define SGM_LCD_DATA   (volatile uint8_t * const)(0x700000)
#define SGM_LCD_CMD    (volatile uint8_t * const)(0x700001)
#define SGM_LCD_STAT   (volatile uint8_t * const)(0x700001)
/* Keyboard on MO_KBD1 */
#define SGM_KBDI       (volatile uint8_t * const)(0x700002)
#define SGM_KBDO       (volatile uint8_t * const)(0x700002)

//#define SGM_SUPPORT_ENABLED

/* XRAM 1 MB (CS2) */
#define XRAM_START    (volatile uint8_t * const)(0x500000)

#define XRAM_SUPPORT_ENABLED

/* SRAM 32 kB (CSx) */
//#define SRAM_START    (volatile uint8_t * const)(0x610000)

/* AUXPORT (CS1) */
#define AUXPORT_START   (volatile uint8_t * const)(0x200000)

#if 0
#define ISR_USB_INTV		EXCPTVEC_IRQ2 	/* pin IRQ2 on PF.0 */
#define PDIUSB_TEST_IRQ()	(!(*DIO_PORTF & 1))
#define PDIUSB_READ_DATA_ADDR   (volatile uint8_t * const)(0x500000)
#define PDIUSB_WRITE_DATA_ADDR  (volatile uint8_t * const)(0x500000)
#define PDIUSB_COMMAND_ADDR     (volatile uint8_t * const)(0x500001)

/* P1.0 .. DACK_N/DMACK0, P7.0 .. DMREQ/DREQ0, P7.2 .. EOT_N/TEND0 */
#undef  PDIUSB_WITH_ADD_IRQ_HANDLER
#define PDIUSB_WITH_EXCPTVECT_SET
#define PDIUSB_SUPPORT_ENABLED
#endif 



/* IDE (CS4) (CS5) powered by PF2 */
#define SIDE_START1   (volatile uint8_t * const)(0x800000)
#define SIDE_START2   (volatile uint8_t * const)(0xA00000)
#define IDE0_DATA     (volatile uint16_t * const)(SIDE_START1+0) /* DATA */
#define IDE0_ERROR     (SIDE_START1+2)	/* Error/Features RO/WO */
#define IDE0_NSECTOR   (SIDE_START1+4)	/* Sector Count R/W */
#define IDE0_SECTOR    (SIDE_START1+6)	/* SN, LBA 0-7 */
#define IDE0_LCYL      (SIDE_START1+8)	/* CL, LBA 8-15 */
#define IDE0_HCYL      (SIDE_START1+10)	/* CH, LBA 16-23 */
#define IDE0_CURRENT   (SIDE_START1+12)	/* 1L1DHHHH , LBA 24-27 */
#define IDE0_STATUS    (SIDE_START1+14)	/* Status */
#define IDE0_SELECT  IDE0_CURRENT
#define IDE0_FEATURE IDE0_ERROR
#define IDE0_COMMAND IDE0_STATUS	/* Command */

#define IDE0_DEVCTRL   (SIDE_START2+12)	/* used for resets */
#define IDE0_ALTSTATUS (SIDE_START2+14)	/* IDE0_STATUS - no clear irq */

#define IDE0_SETPWR(pwr) do{ \
			if(pwr) atomic_clear_mask_b1(4,DIO_PFDR); \
			else atomic_set_mask_b1(4,DIO_PFDR); \
		}while(0)

#define IDE0_PRESENT_M() ((*DIO_PORT9)&0x20)

#if (HW_VER_MAJOR == 0) && (HW_VER_MINOR == 2)
  #define IDE_SWAP_BYTES
#endif

#define IDE0_SUPPORT_ENABLED



/* IRAM 16 kB of on-chip memory */
/* 0xffb000-0xffcfff .. 8 kB free */
/* 0xffd000-0xffdfff .. 4 kB for Flash emulation */
/* 0xffe000-0xffffc0 .. 4 kB - 64 B free*/
/* 0xffffc0-0xffffff .. 64 B free*/
#define IRAM_START    (volatile uint8_t * const)(0xffb000)
#define IRAM_START1   (volatile uint8_t * const)(0xffe000)
#define FRAM_START    (volatile uint8_t * const)(0xffffc0)

/* SCI0 - IrDA */
/* SCI1 - IIC0 (P34, P35) */
/* SCI2 - Boot */
/* SCI3 - SPI */
/* SCI4 - RS232/485 */

/* IRQ0 - RTC */
/* IRQ1 - Index mark */
/* IRQ6 - IDE */

/* Some registers are read only on H8S processors */
/* We use shadow registers for some of them */
#define SHADOW_REG_ALT(_reg,_mask,_xor) \
    (*(_reg)=_reg##_shadow=(_reg##_shadow&~(_mask))^(_xor))

#define SHADOW_REG_SET(_reg,_mask) \
    (*(_reg)=_reg##_shadow|=(_mask))

#define SHADOW_REG_CLR(_reg,_mask) \
    (*(_reg)=_reg##_shadow&=~(_mask))

#define SHADOW_REG_RD(_reg) \
    (_reg##_shadow)

#define SHADOW_REG_WR(_reg,_val) \
    (*(_reg)=_reg##_shadow=(_val))

uint8_t DIO_P1DDR_shadow;
uint8_t DIO_P3DDR_shadow;
uint8_t DIO_PFDDR_shadow;
uint8_t DIO_PJDDR_shadow;

#define DEB_LED_INIT() \
	do {\
	*DIO_PJDR=0x00;\
  	SHADOW_REG_SET(DIO_PJDDR,0xee); /* set PJ.1, PJ.2, PJ.3 LED output */ \
	} while (0)

#define DEB_LED_OFF(num) \
    (*DIO_PJDR |= PJDR_PJ1DRm << (num))
#define DEB_LED_ON(num) \
    (*DIO_PJDR &=~(PJDR_PJ1DRm << (num)))


#endif /* _SYSTEM_DEF_HW01_H_ */
