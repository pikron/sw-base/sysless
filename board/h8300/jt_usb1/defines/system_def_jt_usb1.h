/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  system_def_jt_usb1.h - definition of hardware adresses and registers
 
  Copyright (C) 2002 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#ifndef _SYSTEM_DEF_HW01_H_
#define _SYSTEM_DEF_HW01_H_

//#define CPU_REF_HZ  11059200l	/* reference clock frequency      */
//#define CPU_SYS_HZ  11059200l	/* default system clock frequency */

#define CPU_REF_HZ  4000000l	/* reference clock frequency      */
//#define CPU_SYS_HZ 24000000l	/* default system clock frequency */
#define CPU_SYS_HZ 16000000l	/* default system clock frequency */


unsigned long cpu_ref_hz;	/* actual external XTAL reference */
unsigned long cpu_sys_hz;	/* actual system clock frequency  */

volatile unsigned long msec_time;


/* No external RAM available */
#undef XRAM_START
#undef XRAM_SUPPORT_ENABLED


#if 1
#define ISR_USB_INTV		EXCPTVEC_IRQ6 	/* pin IRQ6 on PG.0 */
#define PDIUSB_TEST_IRQ()	(!(*DIO_PORTG & 1))
#define PDIUSB_READ_DATA_ADDR   (volatile uint8_t * const)(0x600000)
#define PDIUSB_WRITE_DATA_ADDR  (volatile uint8_t * const)(0x600000)
#define PDIUSB_COMMAND_ADDR     (volatile uint8_t * const)(0x600001)

/* P1.0 .. DACK_N/DMACK0, P7.0 .. DMREQ/DREQ0, P7.2 .. EOT_N/TEND0 */
#undef  PDIUSB_WITH_ADD_IRQ_HANDLER
#define PDIUSB_WITH_EXCPTVECT_SET
#define PDIUSB_SUPPORT_ENABLED
#endif 

/* IRAM 16 kB of on-chip memory */
/* 0xffb000-0xffcfff .. 8 kB free */
/* 0xffd000-0xffdfff .. 4 kB for Flash emulation */
/* 0xffe000-0xffffc0 .. 4 kB - 64 B free*/
/* 0xffffc0-0xffffff .. 64 B free*/
#define IRAM_START    (volatile uint8_t * const)(0xffb000)
#define IRAM_START1   (volatile uint8_t * const)(0xffe000)
#define FRAM_START    (volatile uint8_t * const)(0xffffc0)

/* SCI0 - RS232/485 */
/* SCI1 - x */
/* SCI2 - RS232/Boot */
/* SCI3 - x */
/* SCI4 - x */

/* IRQ6 - PDIUSB */

/* Some registers are read only on H8S processors */
/* We use shadow registers for some of them */
#define SHADOW_REG_ALT(_reg,_mask,_xor) \
    (*(_reg)=_reg##_shadow=(_reg##_shadow&~(_mask))^(_xor))

#define SHADOW_REG_SET(_reg,_mask) \
    (*(_reg)=_reg##_shadow|=(_mask))

#define SHADOW_REG_CLR(_reg,_mask) \
    (*(_reg)=_reg##_shadow&=~(_mask))

#define SHADOW_REG_RD(_reg) \
    (_reg##_shadow)

#define SHADOW_REG_WR(_reg,_val) \
    (*(_reg)=_reg##_shadow=(_val))

uint8_t DIO_P1DDR_shadow;
uint8_t DIO_P3DDR_shadow;
uint8_t DIO_P7DDR_shadow;
uint8_t DIO_PFDDR_shadow;
uint8_t DIO_PGDDR_shadow;

#define DEB_LED_INIT() \
       do { /* set P3.2, P3.6, P3.7 LED output */ \
       *DIO_P3DR |= (P3DR_P32DRm | P3DR_P36DRm | P3DR_P37DRm);\
       SHADOW_REG_SET(DIO_P3DDR,(P3DDR_P32DDRm|P3DDR_P36DDRm|P3DDR_P37DDRm));\
       } while (0)

#define DEB_LED_OFF(num) \
    do{switch(num){ case 0: *DIO_P3DR |= P3DR_P32DRm; \
                    case 1: *DIO_P3DR |= P3DR_P36DRm; \
                    case 2: *DIO_P3DR |= P3DR_P37DRm; }}while(0)
#define DEB_LED_ON(num) \
    do{switch(num){ case 0: *DIO_P3DR &= ~P3DR_P32DRm; \
                    case 1: *DIO_P3DR &= ~P3DR_P36DRm; \
                    case 2: *DIO_P3DR &= ~P3DR_P37DRm; }}while(0)

#endif /* _SYSTEM_DEF_HW01_H_ */
