# -*- makefile -*-

APP=boot

ARCH=mcs51
MACH=msc1210
BOARD=hisc

ULAN_ID=$(APP)-$(BOARD)
ULAN_CFG=$(BOARD)-boot

TARGET_ARCH = -m$(ARCH) --model-small

# application
CONFIG_MCS51_ULBOOT=y
# libraries for the application
CONFIG_ULAN=y
CONFIG_MISC_VECT=y
CONFIG_KEYVAL=y
CONFIG_ULAN_DY=y

CONFIG_UL_LIB=n
CONFIG_OC_ULUT=n
CONFIG_ULOI_COM=n

TOHIT=flashmsc
DEV=/dev/ttyUSB0
CPU_SYS_HZ=11059200

LOAD_CMD-boot = \
    $(TOHIT) -d $(DEV) -X $(CPU_SYS_HZ) -E 0x7fff; \
    $(TOHIT) -d $(DEV) -X $(CPU_SYS_HZ) -E 0x807f; \
    $(TOHIT) -d $(DEV) -X $(CPU_SYS_HZ)

RUN_CMD-boot = \
    $(TOHIT) -d $(DEV) -X $(CPU_SYS_HZ) -s $(BOOT_BASE)
