#include <bspbase.h>
#include <local_config.h>
#ifdef CONFIG_MSC_ADC
 #include <msc_adc.h>
#endif
#ifdef CONFIG_MISC_VECT
 #include <vect.h>
#endif
#ifdef CONFIG_ULAN
 #include <ul_lib/ulan.h>
#endif
#ifdef CONFIG_TONEGEN
 #include <tonegen.h>
#endif
/* timers */
lt_ticks_t DATA sys_timer_ticks;

/****************************************************************************/
//definice pruseni (casovac)

void timer(void) 
#ifndef CONFIG_BSPTIMERFNC_WITHOUT_INTERRUPT
interrupt 
#endif
{
  if (AIE & 0x10) {
    uint8_t data msint=MSINT;

    /* timer */
    sys_timer_ticks++;

  #ifdef WATCHDOG_ENABLED
    WATCHDOG_REFRESH();
  #endif
  }
  AI=0; // Clear Aux INT (Bit in EICON)
} 

/****************************************************************************/
void setup_board(void) 
{
  MCON|=1;              //XDATA memory map to 0x8400

  OUTB(OUT_PORT,0xff);  

  sys_timer_ticks=0;

  MSECH=(CPU_SYS_HZ/1000-1) / 0x100;
  MSECL=(CPU_SYS_HZ/1000-1) % 0x100;
  PDCON&=~0x02;  // System Timer ON
  MSINT=9|128;   //10ms
 #ifdef CONFIG_MISC_VECT
  vec_set(timer,0x33);
 #endif
  AIE=0x10;
 #ifndef CONFIG_BSPTIMERFNC_WITHOUT_INTERRUPT
  EAI=1;
 #endif

 #ifdef CONFIG_MSC_ADC
  msc_adc_init();
 #endif

  //********************
  // watchdog
 #ifdef WATCHDOG_ENABLED
  WATCHDOG_ON();
  WATCHDOG_SET_MS(1000); //1s
  WATCHDOG_REFRESH();
 #endif

 #ifdef CONFIG_TONEGEN
  tg_init();
 #endif
}
