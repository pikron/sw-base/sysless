#ifndef _BSPBASE_H
#define _BSPBASE_H

#include <stdint.h>
#include <system_def.h>
#include <cpu_def.h>
#include <lt_timer_types.h>

//timers
extern lt_ticks_t DATA sys_timer_ticks;

#define get_sys_timer_ticks() sys_timer_ticks


void setup_board(void);

#endif /* _BSPBASE_H */
