/*
    TONE GENERATOR
*/

#include <system_def.h>
#include <cpu_def.h>
#include <lt_timer.h>
#include <vect.h>
#include <tonegen.h>

LT_TIMER_DEC(lt_tgmsec)
LT_TIMER_IMP(lt_tgmsec)

bit tg_cnt;

const tg_tone_t tg_do={262,500};
const tg_tone_t tg_re={294,500};
const tg_tone_t tg_mi={330,500};
const tg_tone_t tg_fa={349,500};
const tg_tone_t tg_so={392,500};
const tg_tone_t tg_la={440,500};
const tg_tone_t tg_si={494,500};

/****************************************************************************/
//definice pruseni (casovac2)
void tg_int(void) interrupt 
{
  if (!TF2) return;
  TF2=0;
  tg_cnt=!tg_cnt;
  if (tg_cnt) {
    OUTB(OUT_PORT,0xC0);
  } else {
    OUTB(OUT_PORT,0x00);
  }
} 

/****************************************************************************/
void tg_init(void) 
{
  T2CON = 0x00; 
  vec_set(tg_int,IADDR_TIMER2);
}

/****************************************************************************/
void tg_start(unsigned int tone) 
{
  unsigned long DATA tmp;

  tmp=(CPU_SYS_HZ/12)/tone;
  tmp=65536-tmp;

  RCAP2L=tmp;
  RCAP2H=tmp>>8;  

  TH2 = RCAP2H;
  TL2 = RCAP2L;
  
  lt_tgmsec_init();
  TR2 = 1; 
  ET2=1;                                //povol preruseni casov2
}

/****************************************************************************/
int tg_expired(unsigned int duration) 
{
  return lt_tgmsec_expired(duration);
}

/****************************************************************************/
void tg_stop(void) 
{
  ET2=0;                                //povol preruseni casov2
  TR2 = 0; 
  OUTB(OUT_PORT,0xC0);
}

