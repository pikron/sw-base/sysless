#ifndef _TONEGEN_H
#define _TONEGEN_H
/*
    TONE GENARATOR
*/

typedef struct tg_tone_t {
  unsigned int t;
  unsigned int d;
} tg_tone_t;

extern const tg_tone_t tg_do;
extern const tg_tone_t tg_re;
extern const tg_tone_t tg_mi;
extern const tg_tone_t tg_fa;
extern const tg_tone_t tg_so;
extern const tg_tone_t tg_la;
extern const tg_tone_t tg_si;

void tg_init(void);
void tg_start(unsigned int tone);
int tg_expired(unsigned int duration);
void tg_stop(void);

#endif /* _TONEGEN_H */
