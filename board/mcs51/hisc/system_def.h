/*******************************************************************
  Components for embedded applications builded for
  hisc firmware  
 
  system_def_hisc.h - definition of hardware adresses and registers
 
  Copyright (C) 2004 by Petr Smolik petr.smolik@wo.cz

 *******************************************************************/

#ifndef _SYSTEM_DEF_HISC_H
#define _SYSTEM_DEF_HISC_H

#include <mcu_regs.h>
#include <bspbase.h>

#define CPU_SYS_HZ 11059200l
#define WATCHDOG_ENABLED
#define ES_U ES1
#define SYS_TIMER_HZ    100               //1000Hz pro casovac

#define BAUD2BAUDDIV(baud) \
	((CPU_SYS_HZ+12*8l*baud)/(12*16l*baud))

#define HZ2TMODE1H(x) \
        (0x100-((CPU_SYS_HZ/12/x) / 256))
#define HZ2TMODE1L(x) \
        (0x100-((CPU_SYS_HZ/12/x) % 256))


#if defined(SDCC) || defined(__SDCC)
__sbit __at (0xB2) LED_GP;      // P3.2
__sbit __at (0xB2) LED_ERR;     // P3.2
__sbit __at (0xB6) CS_WR;       // P3.6
__sbit __at (0xA6) CS_O;        // P2.6
__sbit __at (0x87) OUT_CYCLE_PUMP; //P0.7
#else
 sbit	LED_GP  = P3^2;
 sbit	LED_ERR  = P3^2;
 sbit   OUT_CYCLE_PUMP = P0^7;
#endif /*SDCC*/


/***************************************************************************/
/* io functions */
#define IN_PORT			P0
#define LED_PORT		P3
#define OUT_PORT		P0


#define GET_IN_PIN(port,in)	((port & in)?1:0)
#define SET_OUT_PIN(port,out)   out=1;
#define CLR_OUT_PIN(port,out)   out=0;
#define OUTB(port,out)		{CS_O=1;CS_WR=0;port=out;CS_O=0;CS_WR=1;}

/***************************************************************************/
/* watchdog */

#define WATCHDOG_ON() { \
    WDTCON|=0x20;WDTCON&=~0x20; \
    PDCON&=~0x04; \
    WDTCON|=0x80;WDTCON&=~0x80; \
    }

#define WATCHDOG_OFF() { \
    WDTCON|=0x40;WDTCON&=~0x40; \
    PDCON|=0x04; \
    }

#define WATCHDOG_SET_MS(x) { \
    WDTCON=(WDTCON&~0xE0)|(x/100); \
    }

#define WATCHDOG_REFRESH() { \
    WDTCON|=0x20;WDTCON&=~0x20; \
    }

#define watchdog_feed WATCHDOG_REFRESH

#define KVPB_MINIMALIZED
#define KVPB_DEFAULT_FLAGS KVPB_DESC_DOUBLE

#endif /* _SYSTEM_DEF_HISC_H_ */
