#ifndef _ULAN_HIS_H
#define _ULAN_HIS_H

#ifdef __cplusplus
/*extern "C" {*/
#endif

/* basic uLan commands/services */

#define UL_CMD_HIS          0x27    /* Home Information System */

/* UL_CMD_HIS	Home Information System */
#define ULHIS_TIME          0x10    /* Da Mo Ce Ye Hr Mi Se DW */
#define ULHIS_KSWT_SLST     0x20    /* HrSt MiSt HrSo MiSo DW OUT */ 
#define ULHIS_KSWT_GLST     0x21    /* HrSt MiSt HrSo MiSo DW OUT */
#define ULHIS_BLINDER_STOP  0x30    /* Blinder stop */
#define ULHIS_BLINDER_UP    0x31    /* Blinder up , Time*/
#define ULHIS_BLINDER_DOWN  0x32    /* Blinder down, Time*/
        
#ifdef __cplusplus
/*}*/ /* extern "C"*/
#endif

#endif /* _ULAN_HIS_H */

