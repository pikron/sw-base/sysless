#include <bspbase.h>
#include <local_config.h>
#ifdef CONFIG_MISC_VECT
 #include <vect.h>
#endif
#ifdef CONFIG_ULAN
 #include <ul_lib/ulan.h>
#endif

/* timers */
lt_ticks_t DATA sys_timer_ticks;

/****************************************************************************/
//definice pruseni (casovac0)
void timer(void) 
#ifndef CONFIG_BSPTIMERFNC_WITHOUT_INTERRUPT
 interrupt 
 #ifndef CONFIG_MISC_VECT
  1 
 #endif
#endif
{
  TH0=HZ2TMODE1H(SYS_TIMER_HZ);		        //nastav casovac0
  TL0=HZ2TMODE1L(SYS_TIMER_HZ);

  sys_timer_ticks++;
}

/****************************************************************************/
void setup_board(void) 
{
  sys_timer_ticks=0;
  P3=0xff;P1=0xff;
  TMOD=0x21;                                    //citac0-16bitovy,1-8bitovy(reload)
  TH0=HZ2TMODE1H(SYS_TIMER_HZ);		        //nastav casovac0
  TL0=HZ2TMODE1L(SYS_TIMER_HZ);      
  TCON=0x10;                                    //nul. priz. casov0,1;spust casov0
 #ifdef CONFIG_MISC_VECT
  vec_set(timer,IADDR_TIMER0);
 #endif
 #ifndef CONFIG_BSPTIMERFNC_WITHOUT_INTERRUPT
  ET0=1;                                        //povol preruseni casov0  
 #endif
 #ifdef WATCHDOG_ENABLED
  WATCHDOG_ON();
  WATCHDOG_SET_MS(1000); //1s
  WATCHDOG_REFRESH();
 #endif
}
