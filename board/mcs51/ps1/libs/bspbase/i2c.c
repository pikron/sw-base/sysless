#include <stdio.h>
#include <system_def.h>
#include <periph/i2c.h>
#ifdef __KEIL__
#include <intrins.h>
#endif /*__KEIL__*/
 
void i2c_init( void) {
  SDAO = 1; SDAI = 1; SCL = 1;
}
/***************************************************************************/

void i2c_wait(void) {
 #ifdef __KEIL__
     _nop_();
     _nop_();
     _nop_();
     _nop_();
     _nop_();
 #elif SDCC
  _asm
     nop;
     nop;
     nop;
     nop;
     nop;
  _endasm;
 #else
   #error "unsuported compiler!"
 #endif
}
#define i2c_DELAY i2c_wait()

/***************************************************************************/
void i2c_start( void) {
  SDAO = 0;
  i2c_DELAY;
  SCL = 0;
  i2c_DELAY;
}
/***************************************************************************/
char i2c_stop( void) {
  SDAO = 0;
  i2c_DELAY;
  SCL = 1;
  i2c_DELAY;
  SDAO = 1;
  return SDAI;
}
/***************************************************************************/
char i2c_outbyte( unsigned char byte) {
  unsigned char b=8;
  bit ack = 1;
  
  while(b--) {
    SDAO = (byte & 0x80) ? 1 : 0;
    byte<<= 1;
    i2c_DELAY;
    SCL = 1;
    i2c_DELAY;
    SCL = 0;
  }
  i2c_DELAY;
  SDAO = 1;
  i2c_DELAY;
  SCL = 1;
  i2c_DELAY;
  ack = SDAI;
  SCL = 0;
//  SDAO = 0;
  return ack; /* if ack == 0 then OK */
}
/***************************************************************************/
char i2c_inbyte( unsigned char *byte, bit last) {
  unsigned char b = 8;
  unsigned char out = 0;
  bit ack = 1;
  
  while(b--) {
    SDAO = 1;
    i2c_DELAY;
    SCL = 1;
    i2c_DELAY;
    out <<= 1;
    out |= SDAI ? 1 : 0;
    SCL = 0;
  }
  SDAO = last;
  i2c_DELAY;
  SCL = 1;
  i2c_DELAY;
  ack = SDAI;
  SCL = 0;
//  SDAO = 0;
  if ( ack == last) *byte = out;
  return ack;
}
/***************************************************************************/
/***************************************************************************/
/***************************************************************************/
char i2c_write( unsigned char adr, unsigned char *bytes, unsigned char cnt) {
  i2c_start();
  if ( i2c_outbyte( adr)) {
    i2c_stop();
//    printf("iA%02X",adr);
    return 0;
  }
  while(cnt) {
    if ( i2c_outbyte( *bytes++)) {
      i2c_stop();
//      printf("iW%02X(%02X)",adr,*(bytes-1));
      return 0;
    }
    cnt--;
  }
  i2c_stop(); /* return bit */
  return 1;
}
/***************************************************************************/
char i2c_read( unsigned char adr, unsigned char *bytes, unsigned char cnt) {
  i2c_start();
  if ( i2c_outbyte( adr)) {
    i2c_stop();
//    printf("ia%02X",adr);
    return 0;
  }
  while(cnt) {
    i2c_inbyte( bytes++, cnt==1); /* check line */
    cnt--;
  }
  i2c_stop(); /* check stop */
  return 1;
}

