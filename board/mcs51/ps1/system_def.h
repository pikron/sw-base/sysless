/*******************************************************************
  Components for embedded applications builded for
  hisc firmware  
 
  system_def_hisc.h - definition of hardware adresses and registers
 
  Copyright (C) 2004 by Petr Smolik petr.smolik@wo.cz

 *******************************************************************/

#ifndef _SYSTEM_DEF_PS1_H
#define _SYSTEM_DEF_PS1_H

#include <mcu_regs.h>
#include <bspbase.h>

#define CPU_SYS_HZ 11059200l

#define BAUD2BAUDDIV(baud) \
	((CPU_SYS_HZ+12*8l*baud)/(12*16l*baud))

#define HZ2TMODE1H(x) \
        (0x100-((CPU_SYS_HZ/12/x) / 256))
#define HZ2TMODE1L(x) \
        (0x100-((CPU_SYS_HZ/12/x) % 256))

#define SYS_TIMER_HZ    100               //100Hz pro casovac

#define ES_U ES
    
//volatile unsigned long msec_time;

#if 1 /* special for PDIUSBD11 */
#define PDIUSB_READ_DATA_ADDR   (0x35)
#define PDIUSB_WRITE_DATA_ADDR  (0x34)
#define PDIUSB_COMMAND_ADDR     (0x36)

#if defined(SDCC) || defined(__SDCC) // sdcc
 sbit at 0x96 SCL;         // P1.6
 sbit at 0x97 SDAI;        // P1.7
 sbit at 0x97 SDAO;        // P1.7

 sbit at 0xB2 IPDI;        // P3.2
 sbit at 0x94 LEDRX;       // P1.4
 sbit at 0x95 LEDTX;       // P1.5
#else
 sbit	SCL  = P1^6;
 sbit	SDAI = P1^7;
 sbit	SDAO = P1^7;

 sbit	IPDI  = P3^2;
 sbit	LEDRX  = P1^4;
 sbit	LEDTX  = P1^5; 
#endif

/* P1.0 .. DACK_N/DMACK0, P7.0 .. DMREQ/DREQ0, P7.2 .. EOT_N/TEND0 */
//#define ISR_USB_INTV		EXCPTVEC_IRQ6 	/* pin IRQ6 on PG.0 */
#undef  PDIUSB_WITH_ADD_IRQ_HANDLER
#undef PDIUSB_WITH_EXCPTVECT_SET
#define PDIUSB_SUPPORT_ENABLED
#endif 

#endif /* _SYSTEM_DEF_PS1_H_ */
