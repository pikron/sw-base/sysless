#if !defined(__I2C_H)
#define __I2C_H

void i2c_init( void);
char i2c_write( unsigned char adr, unsigned char *bytes, unsigned char cnt);
char i2c_read( unsigned char adr, unsigned char *bytes, unsigned char cnt);

#define I2C_Write i2c_write
#define I2C_Read  i2c_read

#endif

