/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  system_def_jt_usb1.h - definition of hardware adresses and registers
                      of the second prototype version of syringe
                      infussion pump
 
  Copyright (C) 2002 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#ifndef _SYSTEM_DEF_HW01_H_
#define _SYSTEM_DEF_HW01_H_

#include <mcu_regs.h>
#include <bspbase.h>

#define CPU_SYS_HZ 18432100l

#define BAUD2BAUDDIV(baud) \
	((CPU_SYS_HZ+12*8l*baud)/(12*16l*baud))

#define HZ2TMODE1H(x) \
        (0x100-((CPU_SYS_HZ/12/x) / 256))
#define HZ2TMODE1L(x) \
        (0x100-((CPU_SYS_HZ/12/x) % 256))

#define ES_U ES1

#define SYS_TIMER_HZ    100               //1000Hz pro casovac

//volatile unsigned long msec_time;

#if 1 /* special for PDIUSBD11 */
#define PDIUSB_READ_DATA_ADDR   (0x35)
#define PDIUSB_WRITE_DATA_ADDR  (0x34)
#define PDIUSB_COMMAND_ADDR     (0x36)

#if defined(SDCC) || defined(__SDCC)
 __sbit __at 0xB4 SCL;          // P3.4
 __sbit __at 0x90 SDAI;         // P1.0
 __sbit __at 0x91 SDAO;         // P1.1

 __sbit __at 0xB2 IPDI;         // P3.2
 __sbit __at 0xB3 LEDTX;        // P3.3
 __sbit __at 0xB3 LEDRX;        // P3.3
 
 __sbit __at 0xB3 LED_GP;
 __sbit __at 0xB3 LED_ERR;
#else
 __sbit	SCL  = P3^4;
 __sbit	SDAI = P1^0;
 __sbit	SDAO = P1^1;

 __sbit	IPDI  = P3^2;
 __sbit	LEDRX  = P3^3;
 __sbit	LEDTX  = P3^3; 

 __sbit	LED_GP  = P3^3; 
 __sbit	LED_ERR = P3^3; 
#endif /*SDCC*/

/***************************************************************************/
/* io functions */
#define IN_PORT			P0
#define LED_PORT		P3
#define OUT_PORT		P0


#define GET_IN_PIN(port,in)	((port & in)?1:0)
#define SET_OUT_PIN(port,out)   out=1;
#define CLR_OUT_PIN(port,out)   out=0;


/* P1.0 .. DACK_N/DMACK0, P7.0 .. DMREQ/DREQ0, P7.2 .. EOT_N/TEND0 */
//#define ISR_USB_INTV		EXCPTVEC_IRQ6 	/* pin IRQ6 on PG.0 */
#undef  PDIUSB_WITH_ADD_IRQ_HANDLER
#undef PDIUSB_WITH_EXCPTVECT_SET
#define PDIUSB_SUPPORT_ENABLED
#endif 

/* IRAM 16 kB of on-chip memory */
/* 0xffb000-0xffcfff .. 8 kB free */
/* 0xffd000-0xffdfff .. 4 kB for Flash emulation */
/* 0xffe000-0xffffc0 .. 4 kB - 64 B free*/
/* 0xffffc0-0xffffff .. 64 B free*/
/*
#define IRAM_START    (volatile uint8_t * const)(0xffb000)
#define IRAM_START1   (volatile uint8_t * const)(0xffe000)
#define FRAM_START    (volatile uint8_t * const)(0xffffc0)
*/

#define KVPB_MINIMALIZED
#define KVPB_DEFAULT_FLAGS KVPB_DESC_DOUBLE

#endif /* _SYSTEM_DEF_HW01_H_ */
