#ifndef _CMD_INSTANCES_H
#define _CMD_INSTANCES_H

#include <cmd_proc.h>

int cmdproc_instances_insert_cmdio(cmd_io_t *cmd_io);

int cmdproc_instances_remove_cmdio(cmd_io_t *cmd_io);

#endif /*_CMD_INSTANCES_H*/
