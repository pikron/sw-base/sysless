#include <cmd_proc.h>
#include <string.h>
#include <stdint.h>

/** 
 * Blocking call to print a string.
 * 
 * @param cmd_io cmd_io structure.
 * @param str Zero terminated string to print.
 * 
 * @return Upon successful completion, puts() shall return a
 * non-negative number. In case of error, negative number is returned.
 */
int cmd_io_puts(cmd_io_t *cmd_io, const char *str)
{
  int ret;
  unsigned len;
  if (!str) return 0;
  len = strlen(str);
  do {
      ret = cmd_io_write(cmd_io, str, len);
      if (ret > 0) {
          str+=ret;
          len-=ret;
      }
  } while (ret>=0 && len>0);
  return ret;
}

int cmd_io_write_bychar(cmd_io_t *cmd_io,const void *buf,int count)
{
  int cn=0;
  uint8_t* p=(uint8_t*)buf;
  while(count--&&(*cmd_io->putc)(cmd_io,*p++)>=0){
    cn++;
  }
  return cn;
}

int cmd_io_read_bychar(cmd_io_t *cmd_io,void *buf,int count)
{
  int cn=0;
  int ch;
  uint8_t* p=(uint8_t*)buf;
  while(count--&&(ch=(*cmd_io->getc)(cmd_io))>=0){
    *p++=ch;
    cn++;
  }
  return cn;
}

