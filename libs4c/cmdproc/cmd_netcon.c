/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  cmd_netcon.c - text line command processor
               support code for sysless based network IO

  Copyright (C) 2001-2016 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002-2016 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - MPL - Mozilla Public License
   - GPL - GNU Public License
   - other license provided by project originators
 *******************************************************************/

#include <stdio.h>
#include <string.h>
#include <ul_utmalloc.h>
#include "cmd_netcon.h"

#include <ul_log.h>
extern UL_LOG_CUST(ulogd_netcon)

unsigned netcon_con_id_seqcnt;

netcon_con_data_t *netcon_con_init(netcon_con_data_t *ncdata, char malloced_fl,
                                   unsigned buffin_size, void *buffin_mem)
{
  if (ncdata == NULL) {
    ncdata = (typeof(ncdata))malloc(sizeof(*ncdata));
    if (ncdata == NULL)
      return NULL;
    malloced_fl = 1;
  }
  memset(ncdata, 0, sizeof(*ncdata));
  ncdata->malloced_fl = malloced_fl;
  ncdata->id = netcon_con_id_seqcnt;
  netcon_con_id_seqcnt++;
  ncdata->rx_closed_fl = 0;

  netcon_con_init_detached(ncdata);
  netcon_con_proc_queue_init_detached(ncdata);

  ul_dbuff_init(&ncdata->buffin, buffin_size? UL_DBUFF_IS_STATIC: 0);
  if (buffin_size) {
    ncdata->buffin.capacity = buffin_size;
    if (buffin_mem != NULL) {
      ncdata->buffin.data = buffin_mem;
    } else {
      ncdata->buffin.data = malloc(buffin_size);
      if (ncdata->buffin.data == NULL) {
        if (ncdata->malloced_fl)
          free(ncdata);
        return NULL;
      }
      ncdata->malloced_fl |= 2;
    }
  }

  return ncdata;
}

void netcon_con_done(netcon_con_data_t *ncdata)
{
  netcon_server_t *ncserver = ncdata->netcon_server;
  if (ncserver != NULL) {
    netcon_con_delete(ncserver, ncdata);
    netcon_con_proc_queue_del_item(ncdata);
    if (ncserver->con_active)
      ncserver->con_active--;
  }
  ul_dbuff_destroy(&ncdata->buffin);
}

void netcon_con_destroy(netcon_con_data_t *ncdata)
{
  netcon_con_done(ncdata);
  if (ncdata->malloced_fl & 2) {
    if (ncdata->buffin.flags & UL_DBUFF_IS_STATIC)
      free(ncdata->buffin.data);
  }
  if (ncdata->malloced_fl & 1)
    free(ncdata);
}

int netcon_con_established(netcon_con_data_t *ncdata, netcon_server_t *ncserver,
                           struct netcon_con_priv *con_priv)
{
  netcon_con_insert(ncserver, ncdata);
  ncdata->netcon_server = ncserver;
  ncdata->con_priv = con_priv;
  ncserver->con_active++;

  return 0;
}

void netcon_con_aging_update(netcon_con_data_t *ncdata)
{
  netcon_server_t *ncserver = ncdata->netcon_server;

  if (ncserver == NULL)
    return;

  netcon_con_delete(ncserver, ncdata);
  netcon_con_insert(ncserver, ncdata);
}

void netcon_con_queue_proc_rq(netcon_con_data_t *ncdata)
{
  netcon_server_t *ncserver = ncdata->netcon_server;

  ncdata->proc_rq_fl = 1;
  if (ncserver != NULL) {
    netcon_con_proc_queue_del_item(ncdata);
    netcon_con_proc_queue_insert(ncserver, ncdata);
  }
}

int netcon_con_set_rx_close(netcon_con_data_t *ncdata)
{
  ncdata->rx_closed_fl = 1;

  netcon_con_set_proc_rq(ncdata);

  return 0;
}

int netcon_con_queue_received(netcon_con_data_t *ncdata, void *data, size_t datasz)
{
  size_t rem;

  if (!datasz)
    return 0;

  rem = ncdata->buffin.len + datasz;
  rem -= ul_dbuff_cat(&ncdata->buffin, data, datasz);

  netcon_con_aging_update(ncdata);
  netcon_con_set_proc_rq(ncdata);

  return rem;
}

netcon_server_t *netcon_server_init(netcon_server_t *ncserver, int max_connections)
{
  if (ncserver == NULL) {
    ncserver = (typeof(ncserver))malloc(sizeof(*ncserver));
    if (ncserver == NULL)
      return NULL;
  }
  memset(ncserver, 0, sizeof(*ncserver));

  netcon_con_init_head(ncserver);
  netcon_con_proc_queue_init_head(ncserver);
  netcon_con_work_queue_init_head(ncserver);
  ncserver->con_active_max = max_connections;

  return ncserver;
}
