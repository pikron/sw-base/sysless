/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  cmd_netcon.h - text line command processor
               support code for sysless based network IO

  Copyright (C) 2001-2016 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002-2016 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - MPL - Mozilla Public License
   - GPL - GNU Public License
   - other license provided by project originators
 *******************************************************************/

#ifndef _CMD_NETCON_H
#define _CMD_NETCON_H

#include <stdio.h>
#include <string.h>
#include <ul_list.h>
#include <ul_dbuff.h>

struct netcon_con_priv;

typedef struct netcon_server_t netcon_server_t;

typedef int netcon_con_established_callback_t(void *context);

typedef struct netcon_con_data_t {
    ul_list_node_t netcon_peers;
    ul_list_node_t proc_rq;
    netcon_server_t *netcon_server;
    char malloced_fl;
    char proc_rq_fl;
    char rx_closed_fl;
    unsigned id;
    struct netcon_con_priv *con_priv;
    ul_dbuff_t buffin;
    size_t buffin_pos;
    void *user_data;
} netcon_con_data_t;

struct netcon_server_t {
    ul_list_head_t netcon_list;
    ul_list_head_t proc_queue;
    ul_list_head_t work_queue;
    unsigned con_active;
    unsigned con_active_max;
    netcon_con_established_callback_t *con_established_callback;
};


UL_LIST_CUST_DEC(netcon_con, netcon_server_t, netcon_con_data_t,
                 netcon_list, netcon_peers)

UL_LIST_CUST_DEC(netcon_con_proc_queue, netcon_server_t, netcon_con_data_t,
                 proc_queue, proc_rq)

UL_LIST_CUST_DEC(netcon_con_work_queue, netcon_server_t, netcon_con_data_t,
                 work_queue, proc_rq)

netcon_con_data_t *netcon_con_init(netcon_con_data_t *ncdata, char malloced_fl,
                                   unsigned buffin_size, void *buffin_mem);

int netcon_con_established(netcon_con_data_t *ncdata, netcon_server_t *ncserver,
                           struct netcon_con_priv *con_priv);

void netcon_con_aging_update(netcon_con_data_t *ncdata);

void netcon_con_done(netcon_con_data_t *ncdata);

void netcon_con_destroy(netcon_con_data_t *ncdata);

void netcon_con_queue_proc_rq(netcon_con_data_t *ncdata);

static inline
void netcon_con_set_proc_rq(netcon_con_data_t *ncdata)
{
  if (!ncdata->proc_rq_fl)
    netcon_con_queue_proc_rq(ncdata);
}

static inline
void netcon_con_clear_con_priv(netcon_con_data_t *ncdata)
{
  ncdata->con_priv = NULL;
}

int netcon_con_set_rx_close(netcon_con_data_t *ncdata);

int netcon_con_queue_received(netcon_con_data_t *ncdata, void *data, size_t datasz);

netcon_server_t *netcon_server_init(netcon_server_t *ncserver, int max_connections);

#endif /*_CMD_NETCON_H*/
