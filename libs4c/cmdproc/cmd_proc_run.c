/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  cmd_proc.c - text command processor
               enables to define multilevel tables of commands
	       which can be received from more inputs and send reply
	       to respective I/O stream output
 
  Copyright (C) 2001-2016 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002-2016 by PiKRON Ltd. http://www.pikron.com
            (C) 2007 by Michal Sojka <sojkam1@fel.cvut.cz>

  This file can be used and copied according to next
  license alternatives
   - MPL - Mozilla Public License
   - GPL - GNU Public License
   - other license provided by project originators
 *******************************************************************/

#include <cmd_proc.h>
#include "cmd_proc_priv.h"

/**
 * Finish buffered output line after proc_cmd_line call
 */
int cmd_processor_run_finish_line(cmd_io_t *cmd_io, int err_code)
{
  if (cmd_io->priv.ed_line.out->inbuf){
    cmd_io_putc(cmd_io, '\r');
    cmd_io_putc(cmd_io, '\n');
  } else if (err_code < 0) {
    char s[20];
    cmd_io_puts(cmd_io, "ERROR ");
    i2str(s, -err_code, 0, 0);
    cmd_io_puts(cmd_io, s);
    cmd_io_putc(cmd_io, '\r');
    cmd_io_putc(cmd_io, '\n');
  }
  return 0;
}

/**
 * Executes command processor. This function is usually called from
 * application's main loop.
 */
int cmd_processor_run(cmd_io_t *cmd_io, cmd_des_t const **commands)
{
  int val;

  if(cmd_io_line_out(cmd_io))
    return 1; /* Not all the output has been sent. */

  if(cmd_io_line_in(cmd_io)<=0)
    return 0; /* Input line not finished or error. */

  if(commands){
    val = proc_cmd_line(cmd_io, commands, cmd_io->priv.ed_line.in->buf);
  }else{
    val = -CMDERR_BADCMD;
  }

  cmd_processor_run_finish_line(cmd_io, val);

  return 1; /* Command line processed */
}


/* Local Variables: */
/* c-basic-offset: 2 */
/* End */
