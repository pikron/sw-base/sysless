#define _GNU_SOURCE

#include <stdio.h>
#include <string.h>
#include <cmd_proc.h>

ssize_t cmd_io_as_file_read(void *cookie, char *buf, size_t size)
{
  cmd_io_t *cmd_io = (cmd_io_t *)cookie;
  return cmd_io_read(cmd_io, buf, size);
}

ssize_t cmd_io_as_file_write(void *cookie, const char *buf, size_t size)
{
  cmd_io_t *cmd_io = (cmd_io_t *)cookie;
  ssize_t res;
  ssize_t ret = 0;

  while (size) {
    res = cmd_io_write(cmd_io, buf, size);
    if (res < 0) {
      if (ret)
        return ret;
      return -1;
    }
    ret += res;
    buf += res;
    size -= res;
  }
  return ret;
}

/*int cmd_io_as_file_seek(void *cookie, off64_t *offset, int whence)*/

int cmd_io_as_file_close(void *cookie)
{
  return 0;
}

cookie_io_functions_t cmd_io_as_file_io_funcs = {
  .read = cmd_io_as_file_read,
  .write = cmd_io_as_file_write,
  .seek = NULL,
  .close = cmd_io_as_file_close
};

FILE *cmd_io_as_file(cmd_io_t *cmd_io, const char *mode)
{
  if (cmd_io == NULL)
    return NULL;

  return fopencookie(cmd_io, mode, cmd_io_as_file_io_funcs);
}
