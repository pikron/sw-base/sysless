/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  cmd_proc.c - text command processor
               enables to define multilevel tables of commands
               which can be received from more inputs and send reply
               to respective I/O stream output

  Copyright (C) 2001-2015 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002-2015 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - MPL - Mozilla Public License
   - GPL - GNU Public License
   - other license provided by project originators
 *******************************************************************/

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <cmd_proc.h>

#define CMD_IO_ED_LINE_DEFAULT_CHARS 512

typedef struct {
  cmd_io_t cmd_io_data;
  ed_line_buf_t ed_line_in;
  ed_line_buf_t ed_line_out;
  void (*freemem)(void *);
} cmd_io_ed_line_state_t;

cmd_io_t *cmd_io_ed_line_alloc(cmd_io_t *cmd_io_raw, int in_chars, int out_chars,
                               void *(*allocator)(size_t), void (*freemem)(void *),
                               int in_flg, int out_flg)
{
  cmd_io_ed_line_state_t *edlnst;
  cmd_io_t *cmd_io;
  char *edln_buf_in;
  char *edln_buf_out;

  if (allocator == NULL)
    allocator = malloc;
  if (freemem == NULL)
    freemem = free;

  if (!in_chars)
    in_chars = CMD_IO_ED_LINE_DEFAULT_CHARS;
  if (!out_chars)
    out_chars = CMD_IO_ED_LINE_DEFAULT_CHARS;

  edln_buf_in = allocator(in_chars + 1);
  if (edln_buf_in == NULL)
    goto error_edln_buf_in;

  edln_buf_out = allocator(out_chars + 1);
  if (edln_buf_out == NULL)
    goto error_edln_buf_out;

  edlnst = (typeof(edlnst))allocator(sizeof(*edlnst));
  if (edlnst == NULL)
    goto error_edlnst;
  memset(edlnst, 0, sizeof(*edlnst));

  edlnst->ed_line_in.flg = in_flg;
  edlnst->ed_line_in.alloc = in_chars;
  edlnst->ed_line_in.buf = edln_buf_in;

  edlnst->ed_line_out.flg = out_flg;
  edlnst->ed_line_out.alloc = out_chars;
  edlnst->ed_line_out.buf = edln_buf_out;

  edlnst->freemem = freemem;

  cmd_io = &edlnst->cmd_io_data;
  cmd_io->putc = cmd_io_line_putc;
  cmd_io->getc = NULL;
  cmd_io->write = cmd_io_write_bychar;
  cmd_io->read = NULL;
  cmd_io->priv.ed_line.in = &edlnst->ed_line_in;
  cmd_io->priv.ed_line.out = &edlnst->ed_line_out;
  cmd_io->priv.ed_line.io_stack = cmd_io_raw;

  return cmd_io;

 error_edlnst:
  freemem(edln_buf_out);
 error_edln_buf_out:
  freemem(edln_buf_in);
 error_edln_buf_in:
  return NULL;
}

void cmd_io_ed_line_destroy(cmd_io_t *cmd_io, int free_cmd_io_raw_fl)
{
  cmd_io_ed_line_state_t *edlnst = (cmd_io_ed_line_state_t *)cmd_io;

  if (free_cmd_io_raw_fl)
    edlnst->freemem(cmd_io->priv.ed_line.io_stack);

  edlnst->freemem(edlnst->ed_line_out.buf);
  edlnst->freemem(edlnst->ed_line_in.buf);
  edlnst->freemem(edlnst);
}
