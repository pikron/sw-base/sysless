/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  cmd_netcon_lwip.c - text line command processor
               support code for sysless based LwIP network IO

  Copyright (C) 2001-2016 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002-2016 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - MPL - Mozilla Public License
   - GPL - GNU Public License
   - other license provided by project originators
 *******************************************************************/

#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include <stdint.h>
#include <stdlib.h>
#include <lwip/tcp.h>
#include <lwip/init.h>
#if LWIP_VERSION_MAJOR >= 2
#include <lwip/timeouts.h>
#else /*LWIP_VERSION_MAJOR*/
#include <lwip/timers.h>
#endif /*LWIP_VERSION_MAJOR*/
#include <lwip/stats.h>
#include <cmd_proc.h>
#include "cmd_netcon.h"
#include "cmd_instances.h"
#include "cmdio_netcon_lwip.h"

#include <ul_log.h>
extern UL_LOG_CUST(ulogd_netcon)

static inline struct tcp_pcb *
netcon_con2tpcb(netcon_con_data_t *ncdata)
{
  return (struct tcp_pcb *)(ncdata->con_priv);
}

err_t cmd_io_lwip_recv_callback(void *arg, struct tcp_pcb *tpcb,
                               struct pbuf *pbh, err_t err)
{
  netcon_con_data_t *ncdata = (netcon_con_data_t*)arg;
  int rem;
  struct pbuf *p = pbh;
  u32_t togo;
  u32_t l;

  /* do not read the packet if we are not in ESTABLISHED state */
  if (p == NULL) {
    tcp_recv(tpcb, NULL);
    netcon_con_set_rx_close(ncdata);
    ul_logmsg("connection %d remote close rx\n", ncdata->id);
    return ERR_OK;
  }

  togo = p->tot_len;

  do {
    l = p->len;
    if (l > togo)
      l = togo;

    ul_logdeb("Rx tl:%d, l:%d, n:%p\n",
              p->len, p->tot_len, p->next);

    rem = netcon_con_queue_received(ncdata, p->payload, l);
    if (rem) {
      ul_logerr("netcon cannot queue %d received bytes\n", rem);
    }

    togo -= l;
  } while (((p = p->next) != NULL) && togo);

  /* free the received pbuf */
  pbuf_free(pbh);

  return ERR_OK;
}


void cmd_io_lwip_error_callback(void *arg, err_t err)
{
  netcon_con_data_t *ncdata = (netcon_con_data_t*)arg;

  if (ncdata == NULL) {
    ul_logmsg("connection unknown error %d shutdown\n", (int)err);
    return;
  }

  ul_logmsg("connection %d error shutdown %d\n", ncdata->id, (int)err);
  netcon_con_clear_con_priv(ncdata);
  netcon_con_set_proc_rq(ncdata);
}

err_t cmd_io_lwip_accept_callback(void *arg, struct tcp_pcb *newpcb, err_t err)
{
  netcon_con_data_t *ncdata;
  netcon_server_t *ncserver = (netcon_server_t *)arg;
  cmd_io_t *cmd_io;

  if (ncserver->con_active >= ncserver->con_active_max) {
    if (0) {
      ul_logerr("too many active connections\n");
      return ERR_ABRT;
    } else {
      ncdata = netcon_con_first(ncserver);
      if (ncdata == NULL) {
        ul_logerr("connections count too high but netcon_con_first is NULL\n");
        return ERR_ABRT;
      }
      ul_logmsg("connection %d aborted to fulfill accept\n", ncdata->id);
      if (netcon_con2tpcb(ncdata) != NULL) {
        tcp_recv(netcon_con2tpcb(ncdata), NULL);
        tcp_err(netcon_con2tpcb(ncdata), NULL);
        tcp_abort(netcon_con2tpcb(ncdata));
      }
      netcon_con_clear_con_priv(ncdata);
      netcon_con_set_proc_rq(ncdata);
    }
  }

  ncdata = netcon_con_init(NULL, 0, TCP_WND, NULL);
  if (ncdata == NULL) {
    ul_logerr("allocation of netcon_con_data failed\n");
    return ERR_ABRT;
  }

  cmd_io = cmd_io_lwip_alloc(ncdata, NULL, NULL);
  if (cmd_io == NULL) {
    ul_logerr("allocation of cmd_io_lwip failed\n");
    netcon_con_destroy(ncdata);
    return ERR_ABRT;
  }
  ncdata->user_data = cmd_io;
  cmdproc_instances_insert_cmdio(cmd_io);

  ul_logmsg("connection %d accepted\n", ncdata->id);

  /* set argument for recv_callback */
  tcp_arg(newpcb, ncdata);

  /* set the receive callback for this connection */
  tcp_recv(newpcb, cmd_io_lwip_recv_callback);

  tcp_err(newpcb, cmd_io_lwip_error_callback);

  /* enable IPPROTO_TCP/TCP_NODELAY TF_NODELAY */
  /* tcp_nagle_disabled(newpcb); */

  netcon_con_established(ncdata, ncserver, (struct netcon_con_priv *)newpcb);

  if (ncserver->con_established_callback != NULL)
    ncserver->con_established_callback(cmd_io);

  return ERR_OK;
}

int cmd_io_lwip_server_init(netcon_server_t *ncserver, unsigned port, int max_connections,
         cmd_io_lwip_con_established_callback_t *con_established_callback)
{
  struct tcp_pcb *app_pcb;
  err_t err;

  ncserver = netcon_server_init(ncserver, max_connections);

  ncserver->con_established_callback =
    (netcon_con_established_callback_t*)con_established_callback;

  if (ncserver == NULL)
    return -1;

  /* create new TCP PCB structure */
  app_pcb = tcp_new();
  if (!app_pcb) {
    ul_logerr("tcp_new failed (halted)\n");
    return -1;
  }
  /* bind to specified @port */
  err = tcp_bind(app_pcb, IP_ADDR_ANY, port);
  if (err != ERR_OK) {
    ul_logerr("bind to port %d failed : err = %d\n", port, err);
    return -2;
  }

  /* listen for connections */
  app_pcb = tcp_listen(app_pcb);
  if (!app_pcb) {
    ul_logerr("out of memory while tcp_listen\n");
    return -3;
  }

  /* accept callback argument */
  tcp_arg(app_pcb, ncserver);

  /* specify callback to use for incoming connections */
  tcp_accept(app_pcb, cmd_io_lwip_accept_callback);

  return 0;
}

int cmd_io_lwip_poll(netcon_server_t *ncserver)
{
  netcon_con_data_t *ncdata;
  netcon_con_proc_queue_it_t pqit;

 #if 0
  list_splice_init(&ncserver->proc_queue, &ncserver->work_queue);

  ul_list_for_each_cut(netcon_con_work_queue, ncserver, ncdata) {
    struct tcp_pcb  *tpcb = netcon_con2tpcb(ncdata);
    ncdata->proc_rq_fl = 0;

    tcp_recved(tpcb, ncdata->buffin.len);

    /* echo back the payload */
    /* in this case, we assume that the payload is < TCP_SND_BUF */
    if (tcp_sndbuf(tpcb) > ncdata->buffin.len) {
      err_t err;
      err = tcp_write(tpcb, ncdata->buffin.data, ncdata->buffin.len, TCP_WRITE_FLAG_COPY);
    } else {
      ul_logerr("no space in tcp_sndbuf\n");
    }

    ul_dbuff_set_len(&ncdata->buffin, 0);
  }
 #endif

  for(netcon_con_proc_queue_first_it(ncserver, &pqit);
     !netcon_con_proc_queue_is_end_it(&pqit); ) {
    ncdata = netcon_con_proc_queue_it2item(&pqit);
    struct tcp_pcb  *tpcb = netcon_con2tpcb(ncdata);

    if ((ncdata->buffin_pos >= ncdata->buffin.len) || (tpcb == NULL)) {
      ncdata->proc_rq_fl = 0;
      netcon_con_proc_queue_delete_it(&pqit);

      if (tpcb != NULL) {
        tcp_recved(tpcb, ncdata->buffin.len);

        ul_logdeb("A:%lu, Wn:%"PRIu32", W:%d, Wa:%d, We:%"PRIu32"\n",
             ncdata->buffin.len, tpcb->rcv_nxt, tpcb->rcv_wnd,
             tpcb->rcv_ann_wnd, tpcb->rcv_ann_right_edge);

        ncdata->buffin.len = 0;
        ncdata->buffin_pos = 0;

        if (netcon_con2tpcb(ncdata) != NULL)
          tcp_output(tpcb);
      }

      if (ncdata->rx_closed_fl || (netcon_con2tpcb(ncdata) == NULL)) {
        ul_logmsg("connection %d closed\n", ncdata->id);
        if (ncdata->user_data != NULL) {
          cmd_io_t *cmd_io = (cmd_io_t *)ncdata->user_data;
          cmdproc_instances_remove_cmdio(cmd_io);
          cmd_io_lwip_destroy(cmd_io);
        }
        ncdata->user_data = NULL;
        if (netcon_con2tpcb(ncdata) == NULL) {
          netcon_con_destroy(ncdata);
        } else {
          tcp_arg(tpcb, NULL);
          if (tcp_close(tpcb) == ERR_MEM) {
            tcp_arg(tpcb, ncdata);
            netcon_con_set_proc_rq(ncdata);
            break;
          } else {
            netcon_con_destroy(ncdata);
          }
        }
      }
    } else {
      netcon_con_proc_queue_next_it(&pqit);
    }
  }

  return 0;
}

int cmd_io_getc_lwipcon(struct cmd_io *cmd_io)
{
  int ch;

  netcon_con_data_t *ncdata = (netcon_con_data_t *)cmd_io->priv.device.ptr;

  if (ncdata->buffin_pos >= ncdata->buffin.len)
    return -1;

  ch = ncdata->buffin.data[ncdata->buffin_pos++];

  /*printf("ch 0x%02x '%c'\n", ch, ch);*/

  return ch;
}

int cmd_io_putc_lwipcon(struct cmd_io *cmd_io, int ch)
{
  err_t err;
  netcon_con_data_t *ncdata = (netcon_con_data_t *)cmd_io->priv.device.ptr;
  struct tcp_pcb  *tpcb = netcon_con2tpcb(ncdata);
  char ch_buff = ch;

  if (tpcb == NULL)
    return 0;

  if (tcp_sndbuf(tpcb) <= 0) {
    cmd_io_putc_lwip_send_push(cmd_io, ncdata);

    tpcb = netcon_con2tpcb(ncdata);
    if (tpcb == NULL)
      return 0;

    /* Check again if it wasn't emptied */
    if (tcp_sndbuf(tpcb) <= 0)
      return -1;

    tpcb = netcon_con2tpcb(ncdata);
    if (tpcb == NULL)
      return 0;
  }

  err = tcp_write(tpcb, &ch_buff, 1,
                  TCP_WRITE_FLAG_COPY | (ch != '\n'? TCP_WRITE_FLAG_MORE: 0));

  if (err != ERR_OK)
    return -1;

  return ch;
}

cmd_io_t *cmd_io_lwip_alloc(netcon_con_data_t *ncdata,
          void *(*allocator)(size_t), void (*freemem)(void *))
{
  cmd_io_t *cmd_io;
  cmd_io_t *cmd_io_raw;

  if (allocator == NULL)
    allocator = malloc;
  if (freemem == NULL)
    freemem = free;

  cmd_io_raw = (typeof(cmd_io_raw))allocator(sizeof(*cmd_io_raw));
  if (cmd_io_raw == NULL)
     return NULL;
  memset(cmd_io_raw, 0, sizeof(*cmd_io_raw));

  cmd_io_raw->priv.device.ptr = ncdata;
  cmd_io_raw->putc = cmd_io_putc_lwipcon;
  cmd_io_raw->getc = cmd_io_getc_lwipcon;
  cmd_io_raw->write = cmd_io_write_bychar;
  cmd_io_raw->read = cmd_io_read_bychar;

  cmd_io = cmd_io_ed_line_alloc(cmd_io_raw, /*in_chars*/ 0, /*out_chars*/ 0,
                                allocator, freemem, FL_ELB_ECHO * 0, FL_ELB_NOCRLF * 1);
  if (cmd_io == NULL) {
    freemem(cmd_io_raw);
    return NULL;
  }

  return cmd_io;
}

void cmd_io_lwip_destroy(cmd_io_t *cmd_io)
{
  cmd_io_ed_line_destroy(cmd_io, 1);
}
