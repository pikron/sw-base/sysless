/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  cmd_netcon_lwip.h - text line command processor
               support code for sysless based LwIP network IO

  Copyright (C) 2001-2016 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002-2016 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - MPL - Mozilla Public License
   - GPL - GNU Public License
   - other license provided by project originators
 *******************************************************************/

#ifndef _CMD_NETCON_LWIP_H
#define _CMD_NETCON_LWIP_H

#include <lwip/tcp.h>

#include "cmd_netcon.h"

typedef int cmd_io_lwip_con_established_callback_t(struct cmd_io *cmd_io);

cmd_io_t *cmd_io_lwip_alloc(netcon_con_data_t *ncdata,
          void *(*allocator)(size_t), void (*freemem)(void *));

void cmd_io_lwip_destroy(struct cmd_io *cmd_io);

int cmd_io_putc_lwip_send_push(struct cmd_io *cmd_io, netcon_con_data_t *ncdata);

int cmd_io_lwip_server_init(netcon_server_t *ncserver, unsigned port, int max_connections,
         cmd_io_lwip_con_established_callback_t *con_established_callback);

int cmd_io_lwip_poll(netcon_server_t *ncserver);

#endif /*_CMD_NETCON_LWIP_H*/
