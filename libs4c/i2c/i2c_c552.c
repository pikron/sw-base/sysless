/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  i2c_mx1.c - I2C communication automata for M9328 MX1 microcontroller
 
  Copyright holders and project originators
    (C) 2001-2008 by Pavel Pisa pisa@cmp.felk.cvut.cz
    (C) 2002-2008 by PiKRON Ltd. http://www.pikron.com
    (C) 2007-2008 by Petr Smolik

 The COLAMI components can be used and copied under next licenses
   - MPL - Mozilla Public License
   - GPL - GNU Public License
   - LGPL - Lesser GNU Public License
   - and other licenses added by project originators
 Code can be modified and re-distributed under any combination
 of the above listed licenses. If contributor does not agree with
 some of the licenses, he can delete appropriate line.
 Warning, if you delete all lines, you are not allowed to
 distribute code or build project.
 *******************************************************************/


#include <system_def.h>
#include <cpu_def.h>
#include <hal_machperiph.h> /* for PCLK on LPC17xx */
#include "i2c_drv_config.h"
#include "i2c_drv.h"

int c552_poll(i2c_drv_t *drv);
IRQ_HANDLER_FNC(c552_irq_handler);
static int c552_ctrl_fnc(struct i2c_drv *drv, int ctrl, void *p);
int c552_stroke(i2c_drv_t *drv);

// I2C Registers

#if defined(__LPC177x_8x_H__)

#define C552_CONSET(port)  (((LPC_I2C_TypeDef *)(port))->CONSET)  /* Control Set Register */
#define C552_STAT(port)    (((LPC_I2C_TypeDef *)(port))->STAT)    /* Status Register */
#define C552_DAT(port)     (((LPC_I2C_TypeDef *)(port))->DAT)     /* Data Register */
#define C552_ADR(port)     (((LPC_I2C_TypeDef *)(port))->ADR0)    /* Slave Address Register */
#define C552_SCLH(port)    (((LPC_I2C_TypeDef *)(port))->SCLH)    /* SCL Duty Cycle Register (high half word) */
#define C552_SCLL(port)    (((LPC_I2C_TypeDef *)(port))->SCLL)    /* SCL Duty Cycle Register (low half word) */
#define C552_CONCLR(port)  (((LPC_I2C_TypeDef *)(port))->CONCLR)  /* Control Clear Register */
#define C552_MMCTRL(port)  (((LPC_I2C_TypeDef *)(port))->MMCTRL)    /* Monitor Mode Control */

#elif defined(__LPC17xx_H__)

#define C552_CONSET(port)  (((I2C_TypeDef *)(port))->I2CONSET)  /* Control Set Register */
#define C552_STAT(port)    (((I2C_TypeDef *)(port))->I2STAT)    /* Status Register */
#define C552_DAT(port)     (((I2C_TypeDef *)(port))->I2DAT)     /* Data Register */
#define C552_ADR(port)     (((I2C_TypeDef *)(port))->I2ADR0)    /* Slave Address Register */
#define C552_SCLH(port)    (((I2C_TypeDef *)(port))->I2SCLH)    /* SCL Duty Cycle Register (high half word) */
#define C552_SCLL(port)    (((I2C_TypeDef *)(port))->I2SCLL)    /* SCL Duty Cycle Register (low half word) */
#define C552_CONCLR(port)  (((I2C_TypeDef *)(port))->I2CONCLR)  /* Control Clear Register */
#define C552_MMCTRL(port)  (((I2C_TypeDef *)(port))->MMCTRL)    /* Monitor Mode Control */

#else /*__LPC17xx_H__*/

#define C552_CONSET(port)  (((i2cRegs_t *)(port))->conset)     /* Control Set Register */
#define C552_STAT(port)    (((i2cRegs_t *)(port))->stat)       /* Status Register */
#define C552_DAT(port)     (((i2cRegs_t *)(port))->dat)        /* Data Register */
#define C552_ADR(port)     (((i2cRegs_t *)(port))->adr)        /* Slave Address Register */
#define C552_SCLH(port)    (((i2cRegs_t *)(port))->sclh)       /* SCL Duty Cycle Register (high half word) */
#define C552_SCLL(port)    (((i2cRegs_t *)(port))->scll)       /* SCL Duty Cycle Register (low half word) */
#define C552_CONCLR(port)  (((i2cRegs_t *)(port))->conclr)     /* Control Clear Register */

#endif /*__LPC17xx_H__*/

#define C552CON_AA	(1 << 2)
#define C552CON_SI	(1 << 3)
#define C552CON_STO	(1 << 4)
#define C552CON_STA	(1 << 5)
#define C552CON_EN	(1 << 6)

#define C552CON_AAC	(1 << 2)
#define C552CON_SIC	(1 << 3)
#define C552CON_STAC	(1 << 5)
#define C552CON_ENC	(1 << 6)

/***************************************************************************/
int c552_init_start(struct i2c_drv *drv, int port, int irq, int bitrate, int sladr)
{
  unsigned long clock_base;

  clock_base=PCLK/2;

  C552_ADR(port)=sladr;
  C552_SCLH(port)=clock_base/bitrate; //minimal value
  C552_SCLL(port)=clock_base/bitrate;
  drv->irq=irq;
  drv->port=port;
  drv->sfnc_act=NULL;
  drv->ctrl_fnc=c552_ctrl_fnc;
  drv->poll_fnc=c552_poll;
  drv->stroke_fnc=c552_stroke;
  drv->flags=I2C_DRV_ON;    /* todo - use atomic operation */
  C552_CONCLR(port)=0x6C;   /* clearing all flags */
  C552_CONSET(port)=C552CON_EN;
  request_irq(irq, c552_irq_handler, 0, "i2c", drv);
  return 0;
}

static int c552_sfnc_ms_end(struct i2c_drv *drv);
static inline i2c_msg_head_t *c552_sfnc_sl_prep(struct i2c_drv *drv, int cmd, int rxtx);

/***************************************************************************/
static int c552_sfnc_ms_end(struct i2c_drv *drv)
{
  i2c_msg_head_t *msg=drv->msg_act;

  if(msg) {
    if(msg->flags&I2C_MSG_REPEAT){
      drv->master_queue=msg->next;
    }else{
      i2c_drv_queue_msg(msg->flags&I2C_MSG_NOPROC?NULL:&drv->proc_queue,msg);
    }
    msg->flags|=I2C_MSG_FINISHED;
    if((msg->flags&I2C_MSG_CB_END) && (msg->callback))
      msg->callback(drv,I2C_MSG_CB_END,msg);
  }

  if(drv->master_queue) {
    /* there is some more work for master*/
    /* We need to request start of the next transfer somewhere */
    C552_CONSET(drv->port)=C552CON_STA;
  } else {
    drv->flags&=~I2C_DRV_MS_INPR;
  }

  drv->msg_act = NULL;
  return 0;
}

static inline
i2c_msg_head_t *c552_sfnc_sl_prep(struct i2c_drv *drv, int cmd, int rxtx)
{
  i2c_msg_head_t *msg=drv->slave_queue;
  if(!msg) do {
    if((msg->flags&rxtx) && !((cmd^msg->sl_cmd)&msg->sl_msk)){
      drv->slave_queue=msg;
      if((msg->flags&I2C_MSG_CB_START) && (msg->callback))
        msg->callback(drv,I2C_MSG_CB_START|rxtx,msg);
      return msg;
    }
  } while((msg=msg->next)!=drv->slave_queue);
  return NULL;
}

/***************************************************************************/
static int c552_ctrl_fnc(struct i2c_drv *drv, int ctrl, void *p)
{
  unsigned long saveif;
  switch(ctrl){
    case I2C_CTRL_MS_RQ:
      if(!(drv->flags&I2C_DRV_ON))
        return -1;
      if(!drv->master_queue)
        return 0;
      save_and_cli(saveif);
      if(!(drv->flags&I2C_DRV_MS_INPR)) {
        drv->flags|=I2C_DRV_MS_INPR;
	drv->flags&=~I2C_DRV_NA;
        C552_CONSET(drv->port)=C552CON_STA;
      }
      restore_flags(saveif);
      return 0;
    default:
      return -1;
  }
  return 0;
}

/***************************************************************************/
int c552_poll(i2c_drv_t *drv)
{
  i2c_msg_head_t *msg;

  if((msg=drv->proc_queue)!=NULL){
    i2c_drv_queue_msg(NULL,msg);
    if((msg->flags&I2C_MSG_CB_PROC) && (msg->callback))
      msg->callback(drv,I2C_MSG_CB_PROC,msg);
  }  
  return 0;
}

int c552_stroke(i2c_drv_t *drv)
{
  int port;
  volatile int d;
  port=drv->port;

  C552_CONSET(port)=C552CON_STO;
  C552_CONSET(port)=C552CON_STA;
  for(d=0;d<10;d++);
  C552_CONSET(port)=C552CON_STA;
  return 0;
}


int i2c_irq_seq_num=0;

/***************************************************************************/
IRQ_HANDLER_FNC(c552_irq_handler)
{
  i2c_drv_t *drv;
  i2c_msg_head_t *msg;
  int port;
  int stat;

  drv=(i2c_drv_t*)irq_handler_get_context();
  if(drv->magic!=I2C_DRV_MAGIC)
  {
    #ifdef FOR_LINUX_KERNEL
     panic("i2c_irq_handler : BAD drv magic !!!");
    #elif defined(_WIN32)
     I2C_PRINTF("i2c_irq_handler : BAD drv magic !!!\n");
     return FALSE;
    #elif defined(__DJGPP__)||defined(CONFIG_OC_I2C_DRV_SYSLESS)
     I2C_PRINTF("i2c_irq_handler : BAD drv magic !!!\n");
     return;
    #else
     error("i2c_irq_handler : BAD drv magic !!!");
    #endif
  }
  drv->flags&=~I2C_DRV_NA;

  port=drv->port;  
  msg=drv->msg_act;

  stat=C552_STAT(port);

  switch(stat) {
    case 0x00: 
      /* Bus Error has occured */ 
      drv->msg_act=NULL;
      C552_CONSET(port)=C552CON_STO;
      if(drv->master_queue) {
        /* there is some work for master*/
        C552_CONSET(port)=C552CON_STA;
      }
      break;
    case 0x08: /* MS_STA */
      /* the initial start condition has been sent */
      if(!drv->master_queue) {
        C552_CONCLR(port)=C552CON_STAC;
        C552_CONSET(port)=C552CON_STO;
	drv->msg_act=NULL;
	break;
      }
      C552_CONCLR(port)=C552CON_STAC;
      C552_CONSET(port)=C552CON_AA;

      msg=drv->master_queue;
      drv->msg_act=msg;
      msg->tx_len=msg->rx_len=0;

      if((msg->flags&I2C_MSG_CB_START) && (msg->callback))
        msg->callback(drv,I2C_MSG_CB_START,msg);

      if (msg->flags&I2C_MSG_MS_TX) {
        /* proceed Tx request first */
        C552_DAT(port) = msg->addr&~1;
	break;
      }
      /* if there is no request for transmit, continue by Rx immediately */
    case 0x10: /* MS_REPS */
      /* the repeated start has been successfully sent, continue by Rx */
      C552_CONCLR(port)=C552CON_STAC;
      C552_CONSET(port)=C552CON_AA;
      C552_DAT(port) = msg->addr|1;
      if (!msg || !(msg->flags&I2C_MSG_MS_RX)) {
        /* there are no data to be received */
        C552_CONSET(port)=C552CON_STO;
        c552_sfnc_ms_end(drv);
      } else {
        msg->rx_len=0;
      }
      break;
    case 0x18:
      /* sent SLA W received ACK */
    case 0x28:
      /* sent DATA received ACK */
      if (msg->tx_len<msg->tx_rq) {
        C552_DAT(port) = msg->tx_buf[msg->tx_len];
        msg->tx_len++;
        break;
      }
      /* all data has been sent */
      if (!(msg->flags&I2C_MSG_MS_RX)) {
        C552_CONSET(port)=C552CON_STO;
        c552_sfnc_ms_end(drv);
      } else {
        C552_CONSET(port)=C552CON_STA;
      }
      break;
    case 0x30:
      /* sent DATA received NACK */
    case 0x48:
      /* sent SLA R received ACK */
    case 0x20:
      /* vyslano SLA W prijato NACK */
      C552_CONSET(port)=C552CON_STO;
      msg->flags|=I2C_MSG_FAIL;
      c552_sfnc_ms_end(drv);
      break;
    case 0x38:
      /* arbitration lost during Tx */
      C552_CONSET(port)=C552CON_STA;
      break;
    case 0x40:
      /* sent SLA R received ACK */
      if (msg->rx_rq==1)
        C552_CONCLR(port)=C552CON_AAC;
      break;
    case 0x50:
      /* received DATA sent ACK */   
      msg->rx_buf[msg->rx_len]= C552_DAT(port);
      msg->rx_len++;
      if (msg->rx_len+1>=msg->rx_rq)
        C552_CONCLR(port)=C552CON_AAC;
      break;
    case 0x58:
      /* received DATA sent NACK */   
      msg->rx_buf[msg->rx_len]= C552_DAT(port);
      msg->rx_len++;
      C552_CONSET(port)=C552CON_STO;
      c552_sfnc_ms_end(drv);
      break;

    /*** slave mode ***/

    case 0x68:
      /* received own SLA W sent ACK after arbitration lost */

    case 0x78:
      /* received Generall CALL sent ACK after arbitration lost */
      C552_CONSET(port)=C552CON_STA;

    case 0x60:
      /* received own SLA W sent ACK */

    case 0x70:
      /* received Generall CALL sent ACK */
      if(!drv->slave_queue) {
        C552_CONCLR(port)=C552CON_AAC;
	break;
      }
      C552_CONSET(port)=C552CON_AA;
      drv->flags|=I2C_DRV_SL_CEXP|I2C_DRV_SL_INRX;
      break;

    case 0x80:
      /* SLA W : received DATA sent ACK */

    case 0x90:
      /* GCall : received DATA sent ACK */

      if(drv->flags&I2C_DRV_SL_CEXP){
        drv->flags&=~I2C_DRV_SL_CEXP;
	drv->sl_last_cmd=C552_DAT(port);
	msg=c552_sfnc_sl_prep(drv, drv->sl_last_cmd, I2C_MSG_SL_RX);
        drv->msg_act=msg;
      }
      if(!msg || (msg->rx_len>=msg->rx_rq)){
        C552_CONCLR(port)=C552CON_AAC;
	break;
      }
      msg->rx_buf[msg->rx_len]= C552_DAT(port);
      msg->rx_len++;
      break;

    case 0x88:
      /* SLA W : received DATA sent NACK */
      /* may it be, the handling should fall into A0 state */

    case 0x98:
      /* GCall : received DATA sent NACK */
      /* may it be, the handling should fall into A0 state */

      C552_CONSET(port)=C552CON_AA;
      break;

    case 0xA0:
      /* Slave : Repeated START or STOP */
      if(msg && (msg->flags&I2C_MSG_CB_END) && (msg->callback)) {
        int cbcode;
	if(drv->flags&I2C_DRV_SL_INRX)
	   cbcode=I2C_MSG_CB_END|I2C_MSG_SL_RX;
	else
	   cbcode=I2C_MSG_CB_END|I2C_MSG_SL_TX;
        msg->callback(drv,cbcode,msg);
      }
      C552_CONSET(port)=C552CON_AA;
      break;

    case 0xB0:
      /* received own SLA R sent ACK  after arbitration lost */
      C552_CONSET(port)=C552CON_STA;

    case 0xA8:
      /* received own SLA R sent ACK */
      drv->flags&=~I2C_DRV_SL_INRX;
      msg=c552_sfnc_sl_prep(drv, drv->sl_last_cmd, I2C_MSG_SL_RX);
      drv->msg_act=msg;
      if(!msg) {
        C552_CONCLR(port)=C552CON_AAC;
	break;
      }
      C552_CONSET(port)=C552CON_AA;

    case 0xB8:
      /* SLA R : sent DATA received ACK */
      if(!msg || (msg->tx_len>=msg->tx_rq)){
        C552_DAT(port) = 0xff;
	break;
      }
      C552_DAT(port) = msg->tx_buf[msg->tx_len];
      msg->tx_len++;
      break;

    case 0xC0:
      /* SLA R : sent DATA received NACK */
      /* the A0 state is not enerred most probably */

    case 0xC8:
      /* SLA R : last data sent, DATA (AA=0) received ACK */
      /* the A0 state is not enerred most probably */
      C552_CONSET(port)=C552CON_AA;

      if(msg && (msg->flags&I2C_MSG_CB_END) && (msg->callback)) {
        msg->callback(drv,I2C_MSG_CB_END|I2C_MSG_SL_TX,msg);
      }
      break;

    default: break;
  }

  /* vymaz SI bit */
  C552_CONCLR(port)=C552CON_SIC;
}
