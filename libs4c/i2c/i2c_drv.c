/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  i2c_mx1.c - I2C communication automata for M9328 MX1 microcontroller
 
  Copyright holders and project originators
    (C) 2001-2004 by Pavel Pisa pisa@cmp.felk.cvut.cz
    (C) 2002-2004 by PiKRON Ltd. http://www.pikron.com
    (C) 2007-2008 by Petr Smolik

 The COLAMI components can be used and copied under next licenses
   - MPL - Mozilla Public License
   - GPL - GNU Public License
   - LGPL - Lesser GNU Public License
   - and other licenses added by project originators
 Code can be modified and re-distributed under any combination
 of the above listed licenses. If contributor does not agree with
 some of the licenses, he can delete appropriate line.
 Warning, if you delete all lines, you are not allowed to
 distribute code or build project.
 *******************************************************************/


#include <system_def.h>
#include <string.h>
#include "i2c_drv_config.h"
#include "i2c_drv.h"

/***************************************************************************/
int  
i2c_drv_init(i2c_drv_t *drv, int port, int irq, int bitrate,int sladr) 
{
  int r=-1;
  memset(drv,0,sizeof(i2c_drv_t));
  drv->magic=I2C_DRV_MAGIC;
 #ifdef CONFIG_OC_I2C_CHIP_C552
  r=c552_init_start(drv,port,irq,bitrate,sladr);
 #endif /* CONFIG_OC_I2C_CHIP_C552 */
  if (r<0) return r;
  return 0;
}

/********************************************************************/
/* Generic I2C functions */

void i2c_drv_queue_msg(i2c_msg_head_t **queue, i2c_msg_head_t *msg)
{
  I2C_IRQ_LOCK_FINI
  i2c_msg_head_t *prev, *next;
  I2C_IRQ_LOCK;
  if(msg->on_queue){
    if(msg->next==msg){
      if(*msg->on_queue==msg)
        *msg->on_queue=NULL;
    }else{
      msg->next->prev=msg->prev;
      msg->prev->next=msg->next;
      if(*msg->on_queue==msg)
        *msg->on_queue=msg->next;
    }
  }
  if((msg->on_queue=queue)!=NULL){
    if((next=*queue)!=NULL){
      msg->prev=prev=next->prev;
      msg->next=next;
      next->prev=msg;
      prev->next=msg;
    }else{
      *queue=msg->prev=msg->next=msg;
    }
  }
  I2C_IRQ_UNLOCK;
  return;
}

int i2c_drv_master_msg_ins(i2c_drv_t *drv, i2c_msg_head_t *msg)
{
  if(!drv) return -1;
  if(!(drv->flags&I2C_DRV_ON)) return -1;
  if(!msg->tx_buf) msg->flags&=~I2C_MSG_MS_TX;
  if(!msg->rx_buf) msg->flags&=~I2C_MSG_MS_RX;
  i2c_drv_queue_msg(&drv->master_queue,msg);
  drv->ctrl_fnc(drv,I2C_CTRL_MS_RQ,NULL);
  return 0;
}

int i2c_drv_master_msg_rem(i2c_drv_t *drv, i2c_msg_head_t *msg)
{
  int act;
  I2C_IRQ_LOCK_FINI
  do {
    i2c_drv_queue_msg(NULL,msg);
    I2C_IRQ_LOCK;
    act = (msg==drv->msg_act);
    if(act) {
      drv->msg_act=NULL;
    }
    I2C_IRQ_UNLOCK;    
  } while(msg->on_queue || act);
  return 0;
}

int i2c_drv_flush_all(i2c_drv_t *drv)
{
  I2C_IRQ_LOCK_FINI
  i2c_msg_head_t *msg, *next;
  i2c_msg_head_t *queue[3];
  int quenum;

  I2C_IRQ_LOCK;
    queue[0]=drv->master_queue;
    queue[1]=drv->slave_queue;
    queue[2]=drv->proc_queue;
    drv->master_queue=NULL;
    drv->slave_queue=NULL;
    drv->proc_queue=NULL;
    drv->msg_act=NULL;
  I2C_IRQ_UNLOCK;    
  for(quenum=0;quenum<3;quenum++){
    msg=queue[quenum];
    if(!msg) continue;
    msg->prev->next=NULL;
    for(;msg;msg=next){
      next=msg->next;
      msg->flags|=I2C_MSG_FAIL;
      msg->on_queue=NULL;
      if((msg->flags&I2C_MSG_CB_PROC) && (msg->callback))
	msg->callback(drv,I2C_MSG_CB_PROC,msg);
    }
  }
  return 0;
}

int i2c_drv_master_transfer_callback(struct i2c_drv *drv, int code, struct i2c_msg_head *msg)
{
  if(code!=I2C_MSG_CB_PROC) return 0;
  set_bit(0,&(msg->private));
  return 0;
}


int i2c_drv_master_transfer(i2c_drv_t *drv, int addr, int tx_rq, int rx_rq,
                   void *tx_buf, void *rx_buf, int *ptx_len, int *prx_len)
{
  i2c_msg_head_t msg;
  
  msg.flags = I2C_MSG_CB_PROC;
  msg.addr = addr;
  msg.tx_rq = tx_rq;
  msg.rx_rq = rx_rq;
  msg.tx_buf = tx_buf;
  msg.rx_buf = rx_buf;
  msg.on_queue = NULL;
  msg.callback = i2c_drv_master_transfer_callback;
  msg.private = 0;

  if(msg.tx_buf)
    msg.flags |= I2C_MSG_MS_TX;

  if(msg.rx_buf && (msg.rx_rq>=1))
    msg.flags |= I2C_MSG_MS_RX;

  if(!(msg.flags & (I2C_MSG_MS_TX | I2C_MSG_MS_RX)))
    return 0;

  if(i2c_drv_master_msg_ins(drv, &msg)<0) 
    return -1;

  /* wait for message process */
  while(test_bit(0,&(msg.private))==0)
    drv->poll_fnc(drv);

  if(ptx_len) *ptx_len = msg.tx_len;
  if(prx_len) *prx_len = msg.rx_len;
  
  if(msg.flags & I2C_MSG_FAIL)
    return -1;
  
  return msg.tx_len+msg.rx_len;
}

