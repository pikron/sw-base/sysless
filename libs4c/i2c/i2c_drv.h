/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  i2c_drv.h - I2C communication automat interface 
 
  Copyright holders and project originators
    (C) 2001-2008 by Pavel Pisa pisa@cmp.felk.cvut.cz
    (C) 2002-2008 by PiKRON Ltd. http://www.pikron.com
    (C) 2007-2008 by Petr Smolik

 The COLAMI components can be used and copied under next licenses
   - MPL - Mozilla Public License
   - GPL - GNU Public License
   - LGPL - Lesser GNU Public License
   - and other licenses added by project originators
 Code can be modified and re-distributed under any combination
 of the above listed licenses. If contributor does not agree with
 some of the licenses, he can delete appropriate line.
 Warning, if you delete all lines, you are not allowed to
 distribute code or build project.
 *******************************************************************/

#ifndef _I2C_DRV_H_
#define _I2C_DRV_H_

#include <stdint.h>
#include <cpu_def.h>

#if defined(CONFIG_OC_I2C_DRV_SYSLESS)
  #define I2C_IRQ_LOCK_FINI unsigned long i2c_irq_lock_flags=0;
  #define I2C_IRQ_LOCK \
    {save_flags(i2c_irq_lock_flags);cli();}
  #define I2C_IRQ_UNLOCK \
    {restore_flags(i2c_irq_lock_flags);}
  #define I2C_MB()       {asm volatile ("":::"memory");}
#endif

struct i2c_drv;

#define I2C_MSG_TX       0x001
#define I2C_MSG_RX       0x002
#define I2C_MSG_MS_TX    I2C_MSG_TX
#define I2C_MSG_MS_RX    I2C_MSG_RX
#define I2C_MSG_SL_TX    I2C_MSG_TX
#define I2C_MSG_SL_RX    I2C_MSG_RX
#define I2C_MSG_SLAVE    0x004
#define I2C_MSG_FAIL     0x008
#define I2C_MSG_REPEAT   0x010
#define I2C_MSG_NOPROC   0x020
#define I2C_MSG_FINISHED 0x040
#define I2C_MSG_CB_START 0x100
#define I2C_MSG_CB_END   0x200
#define I2C_MSG_CB_PROC  0x400

typedef struct i2c_msg_head {
    unsigned long flags;/* message flags */
    uint8_t  sl_cmd;	/* command for slave queue lookup */
    uint8_t  sl_msk;	/* sl_cmd match mask */
    uint16_t addr;	/* message destination address */
    uint16_t tx_rq;	/* requested TX transfer length */
    uint16_t rx_rq;	/* requested RX transfer length */
    uint16_t tx_len;	/* finished TX transfer length */
    uint16_t rx_len;	/* finished RX transfer length */
    uint8_t *tx_buf;	/* pointer to TX data */
    uint8_t *rx_buf;	/* pointer to RX data */
    struct i2c_msg_head *prev;
    struct i2c_msg_head *next;
    struct i2c_msg_head **on_queue;
    int (*callback)(struct i2c_drv *ifc, int code, struct i2c_msg_head *msg);
    unsigned long private;
  } i2c_msg_head_t;

typedef int (i2c_sfnc_t)(struct i2c_drv *drv, int code);
typedef int (i2c_ctrl_fnc_t)(struct i2c_drv *drv, int ctrl, void *p);
typedef int (i2c_stroke_fnc_t)(struct i2c_drv *drv);

#define I2C_DRV_ON      1 /* flag indicating that driver is ready to operate */
#define I2C_DRV_MS_INPR 2 /* master request in in progress */
#define I2C_DRV_NA      4 /* driver is not active for some period */
#define I2C_DRV_SL_CEXP 8 /* slave expect receive of the first byte */
#define I2C_DRV_SL_INRX 0x10 /* slave in mode */

#define I2C_DRV_MAGIC 0x12345432

typedef struct i2c_drv {
    int	magic;		/* magic number */
    int	irq;		/* irq number */
    long port;		/* base port number */
    uint8_t flags;
    uint16_t self_addr;
    i2c_msg_head_t *master_queue;
    i2c_msg_head_t *slave_queue;
    i2c_msg_head_t *proc_queue;
    i2c_msg_head_t *msg_act;
    i2c_sfnc_t *sfnc_act;
    void *failed;
    i2c_ctrl_fnc_t *ctrl_fnc;
    int (*poll_fnc)(struct i2c_drv *drv);
    i2c_stroke_fnc_t *stroke_fnc;
    uint8_t sl_last_cmd; /* last received slave command */
  } i2c_drv_t;

#define I2C_CTRL_MS_RQ 1

#ifdef CONFIG_OC_I2C_CHIP_C552
int c552_init_start(struct i2c_drv *drv, int port, int irq, int bitrate, int sladr);
#endif /* CONFIG_OC_I2C_CHIP_C552 */

void i2c_drv_queue_msg(i2c_msg_head_t **queue, i2c_msg_head_t *msg);
int  i2c_drv_init(i2c_drv_t *drv, int port, int irq, int bitrate,int sladr);
int  i2c_drv_master_msg_ins(i2c_drv_t *drv, i2c_msg_head_t *msg);
int  i2c_drv_master_msg_rem(i2c_drv_t *drv, i2c_msg_head_t *msg);
int  i2c_drv_flush_all(i2c_drv_t *drv);
int  i2c_drv_master_transfer(i2c_drv_t *drv, int addr, int tx_rq, int rx_rq,
                   void *tx_buf, void *rx_buf, int *ptx_len, int *prx_len);

#ifdef I2C_LOG_ENABLE
  /* todo */
#else
  #define I2C_PRINTF(x,args...) 
#endif

#endif /* _I2C_DRV_H_ */
