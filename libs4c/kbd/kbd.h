#include <system_def.h>

#ifndef KEY_DEFAULT_TIMES
  #define KEY_PUSH_T	20
  #define KEY_RELEASE_T	10
  #define KEY_REPFIRST_T 800
  #define KEY_REPNEXT_T	300
#endif /* KEY_DEFAULT_TIMES */

typedef struct {
	kbd_key_t bc;
	kbd_key_t sc;
} scan2key_t;

typedef struct {
	int scan;
	int flag;
	kbd_keymod_t is_mod;
	kbd_keymod_t set_mod;
	kbd_keymod_t xor_mod;
} scan2mod_t;

#define KBDMOD_SGM_SC		0x8000
#define KBDMOD_SGM_RELEASE	0x0080

extern int key_last_changed;
extern kbd_keymod_t key_mod;
extern unsigned char key_hit;
extern short key_use_timer;
extern unsigned char key_down_arr[KBD_SCAN_CNT];

unsigned char kbd_onerow(unsigned char scan);
void kbd_setio(void);

int kbd_scan();
void kbd_scan2mod(int scan_code);
int kbd_down();
kbd_key_t kbd_scan2key(int scan);

int kbd_Open(KBDDEVICE *pkd);
void kbd_Close(void);
void kbd_GetModifierInfo(kbd_keymod_t *modifiers, kbd_keymod_t *curmodifiers);
int kbd_Read(kbd_key_t *buf, kbd_keymod_t *modifiers, kbd_scan_code_t *scancode);
int kbd_Poll(void);

