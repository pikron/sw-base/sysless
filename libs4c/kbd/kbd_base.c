#include "kbd.h"

extern scan2key_t *kbd_scan2key_tab;
extern scan2mod_t *kbd_scan2mod_tab;

/* State of keyboard matrix and key press reporting */

unsigned char key_down_arr[KBD_SCAN_CNT];
unsigned char key_chng_arr[KBD_SCAN_CNT];
unsigned char key_hit;

kbd_keymod_t key_mod;

int key_last_changed;

/* Internal state for repeat processing */

short key_use_timer;
short key_state;
kbd_interval_t key_time;

#define KEY_STATE_IDLE     0
#define KEY_STATE_PUSH     1
#define KEY_STATE_RELEASE  2
#define KEY_STATE_REPEAT   4
#define KEY_STATE_NOISE    8
#define KEY_STATE_BUSY     (KEY_STATE_PUSH|KEY_STATE_RELEASE)


/**
 * kbd_scan - Scan keyboard matrix and report requests for state change
 *
 * Scans keyboard matrix connected row by row by calling function
 * mx1_kbd_onerow(). Number of scanned output lines is defined
 * by %KBD_SCAN_CNT. Checks read keyboard state against @key_down_arr
 * and updates @key_change_arr array. The @key_down_arr state is 
 * left unchanged. It is changed later by kbd_down() function.
 * Returns 0, if no keyboard activity is found. Returns 1
 * if at least one key is pressed. Returns 2 or 3 in case
 * of detected change.
 */
int 
kbd_scan()
{
	int i, ret=0;
	unsigned char mask, val, chng;
	for(i=0,mask=1;i<KBD_SCAN_CNT;i++,mask<<=1) {
		val=kbd_onerow(mask);
		chng=val^key_down_arr[i];
		key_chng_arr[i]=chng;
		if(val) ret|=1;
		if(chng) ret|=2;
	}
	/* mx1_kbd_onerow(~0); */
	return ret;
}


/**
 * kbd_scan2mod - Propagate keyboard matrix changes between modifiers
 * @scan_code:		Scan code of last detected key change
 *
 * Functions check keyboard matrix state in @key_down_arr.
 * It updates @key_mod according to @key_down_arr and 
 * modifiers transformations table @kbd_scan2mwmod_tab.
 */
void 
kbd_scan2mod(int scan_code)
{
	unsigned char val, chng;
	int s;
	scan2mod_t *mt=kbd_scan2mod_tab;

	for(;(s=mt->scan);mt++) {
		chng=(s==scan_code);
		s--;
		val=key_down_arr[s/KBD_RET_CNT]&(1<<(s%KBD_RET_CNT));
		if(val) {
			key_mod|=mt->set_mod;
			if(chng){
				key_mod^=mt->xor_mod;
			}
		} else {
			key_mod&=~mt->set_mod;
		}
	}
}

/**
 * kbd_down - Detects changed key scancode and applies changes to matrix state
 *
 * Functions check @key_chng_arr and process changes.
 * It updates its internal state @key_state, does
 * noise cancellation and repeat timing, then updates 
 * @key_down_arr, stores detected scancode to @key_last_changed
 * and calls modifiers processing kbd_scan2mod().
 * Return value is zero if no change is detected. 
 * In other case evaluated scancode is returned.
 * Variable @key_hit signals by value 1 pressed key, by value
 * 2 key release.
 */
int 
kbd_down()
{
	int i, j=0;
	unsigned char val;
	
        if(!(key_state&KEY_STATE_BUSY)){
		for(i=0;i<KBD_SCAN_CNT;i++) {
			if(!(val=key_chng_arr[i])) continue;
			for(j=0;!(val&1);j++) val>>=1;
			key_last_changed=i*KBD_RET_CNT+j+1;
			if(key_down_arr[i]&(1<<j)){
				key_time=KEY_TIMER+KEY_PUSH_T;
				key_state=KEY_STATE_RELEASE;
			}else{
				key_time=KEY_TIMER+KEY_RELEASE_T;
				key_state=KEY_STATE_PUSH;
			}
			break;
		}
		if(key_state==KEY_STATE_IDLE)
			return 0;
	} else {
		if(!key_last_changed){
			key_state=KEY_STATE_IDLE;
	  		return 0;
		}
		i=(key_last_changed-1)/KBD_RET_CNT;
		j=(key_last_changed-1)%KBD_RET_CNT;
		if(!(key_chng_arr[i]&(1<<j))){
			/* Noise detected */
			if(!(key_state&KEY_STATE_NOISE)){
			        key_time=KEY_TIMER+KEY_RELEASE_T;
			        key_state|=KEY_STATE_NOISE;
			}
		}
	}

	if(!key_use_timer){
		if(KEY_TIMER) key_use_timer=1;
		if(key_state&KEY_STATE_REPEAT) return 0;
	}else{
		if((long)(KEY_TIMER-key_time)<0) return 0;
	}
	
	if(key_state==KEY_STATE_PUSH) {
		key_down_arr[i]|=1<<j;
		kbd_scan2mod(key_last_changed);
		key_state=KEY_STATE_REPEAT;
		key_time=KEY_TIMER+KEY_REPFIRST_T;
		key_hit=1;
		return key_last_changed;
	} else if(key_state==KEY_STATE_REPEAT) {
		key_time=KEY_TIMER+KEY_REPNEXT_T;
		key_hit=1;
		return key_last_changed;
	} else if(key_state==KEY_STATE_RELEASE) {
		key_down_arr[i]&=~(1<<j);
		kbd_scan2mod(key_last_changed);
	        key_state=KEY_STATE_IDLE;
		key_hit=2;
		return key_last_changed;
	} 
	key_state=KEY_STATE_IDLE;
	return 0;
}

/**
 * kbd_scan2key - Converts scancode to kbd_key_t keyboard values
 * @scan:	Detected scancode
 *
 * Computes kbd_key_t value for detected scancode.
 * Uses @kbd_scan2key_tab transformation table
 * and @key_mod modifiers information.
 */
kbd_key_t kbd_scan2key(int scan)
{
	if((key_mod&KBDMOD_SGM_SC)&&kbd_scan2key_tab[scan].sc)
		return kbd_scan2key_tab[scan].sc;
	return kbd_scan2key_tab[scan].bc;
}

