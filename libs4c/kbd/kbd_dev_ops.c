#include <string.h>
#include "kbd.h"

#ifdef _DEVICE_H
/* create the microwindows keyboard device */

KBDDEVICE kbddev = {
	kbd_Open,
	kbd_Close,
	kbd_GetModifierInfo,
	kbd_Read,
	kbd_Poll
};
#endif /* _DEVICE_H */

/**
 * kbd_Open - Open the keyboard
 * @pkd:	Pointer to keyboard device
 */
int
kbd_Open(KBDDEVICE *pkd)
{
	key_last_changed=0;
	key_mod=0;
	key_hit=0;
	key_use_timer=0;
	memset(key_down_arr,0,sizeof(key_down_arr));
	kbd_setio();
	return 1;
}

/**
 * mx1_kbd_Close - Closes keyboard
 */
void
kbd_Close(void)
{
}

/**
 * mx1_kbd_Poll - Polls for keyboard events
 *
 * Returns non-zero value if change is detected.
 */
int
kbd_Poll(void)
{
	if(key_hit)
		return 1;
	if(kbd_scan())
		kbd_down();
	return key_hit?1:0;
}

/**
 * kbd_GetModifierInfo - Returns the possible modifiers for the keyboard
 * @modifiers:		If non-NULL, ones in defined modifiers bits are returned.
 * @curmodifiers:	If non-NULL, ones in actually active modifiers
 *			bits are returned.
 */
void
kbd_GetModifierInfo(kbd_keymod_t *modifiers, kbd_keymod_t *curmodifiers)
{
	if (modifiers)
		*modifiers = 0;		/* no modifiers available */
	if (curmodifiers)
		*curmodifiers = key_mod&~KBDMOD_SGM_SC;
}

/**
 * mx1_kbd_Read - Reads resolved MWKEY value, modifiers and scancode
 * @buf:		If non-NULL, resolved MWKEY is stored here
 * @modifiers:		If non-NULL, ones in actually active modifiers
 *			bits are returned
 * @scancode:		If non-NULL, scancode of resolved key is stored
 *			here
 *
 * This function reads one keystroke from the keyboard, and the current state
 * of the modifier keys (ALT, SHIFT, etc).  Returns -1 on error, 0 if no data
 * is ready, 1 on a keypress, and 2 on keyrelease.
 * This is a non-blocking call.
 */
int
kbd_Read(kbd_key_t *buf, kbd_keymod_t *modifiers, kbd_scan_code_t *scancode)
{
        int ret;
	if(!key_hit) {
		if(kbd_scan()){
			kbd_down();
		}
	}
	if(modifiers)
		*modifiers = key_mod&~KBDMOD_SGM_SC;
	if(!key_hit)
		return 0;
	if(scancode)
		*scancode = key_last_changed;
	if(buf)
		*buf = kbd_scan2key(key_last_changed);
	ret=key_hit;
	key_hit=0;
	return ret;
}
