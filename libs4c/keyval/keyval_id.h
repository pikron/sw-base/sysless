#ifndef _KEYVAL_ID_H_
#define _KEYVAL_ID_H_

#include "keyvalpb.h"

#define KVPB_KEYID_ULAN_ADDR            0x10
#define KVPB_KEYID_ULAN_SN              0x11

#define KVPB_KEYID_ULAN_PICO		0x15
#define KVPB_KEYID_ULAN_POCO		0x16
#define KVPB_KEYID_ULAN_PIOM		0x17
#define KVPB_KEYID_ULAN_PEV2C		0x18

#define KVPB_KEYID_HIS_ACCESS_CODE	0x20

#endif /* _KEYVAL_ID_H_ */

