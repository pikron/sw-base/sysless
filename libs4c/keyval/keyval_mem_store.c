/******************************************************************************/
/* Key Value block in RAM */

#include <malloc.h>
#include <string.h>
#include <keyvalpb.h>
#include <keyval_mem_store.h>

int kvpb_mem_erase(kvpb_block_t *store, void *base,int size)
{
  memset(base,0xff,size);
  return 0;
}

int kvpb_mem_copy(kvpb_block_t *store, void *dst,const void *src,int len)
{
  memcpy(dst,src,len);
  return 0;
}

#ifndef  KVPB_MINIMALIZED
kvpb_block_t *kvpb_store_create_ram(int size)
{
  kvpb_block_t *store;
  int res;

  if(!size)
    size=0x4000;

  store = malloc(sizeof(*store));
  if(store == NULL)
    return NULL;

  memset(store, 0, sizeof(*store));

  store->base = malloc(size*2);
  if(store->base == NULL)
    goto error_ret;

  store->size = size;
  store->flags = KVPB_DESC_DOUBLE;
 #ifndef  KVPB_MINIMALIZED
  store->erase = kvpb_mem_erase,
  store->copy = kvpb_mem_copy;
 #endif  /* KVPB_MINIMALIZED */

  kvpb_block_erase(store, store->base,store->size);
  if(store->flags & KVPB_DESC_DOUBLE)
    kvpb_block_erase(store, (uint8_t*)store->base+store->size,store->size);

  res=kvpb_check(store,3);

  if(res<0)
    goto error_ret;

  return store;

 error_ret:
  if(store)
    free(store);
  return NULL;
}
#endif  /* KVPB_MINIMALIZED */
