#ifndef _KEYVAL_MEM_STORE_H
#define _KEYVAL_MEM_STORE_H

#include <keyvalpb.h>

int kvpb_mem_erase(kvpb_block_t *store, void *base,int size);
int kvpb_mem_copy(kvpb_block_t *store, void *dst,const void *src,int len);
kvpb_block_t *kvpb_store_create_ram(int size);

#endif /*_KEYVAL_MEM_STORE_H*/
