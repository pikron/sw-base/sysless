/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  cmd_i2str.c - formated text to string conversion
                without need to pull in whole stdio support
 
  Copyright (C) 2001-2010 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002-2010 by PiKRON Ltd. http://www.pikron.com
            (C) 2007 by Michal Sojka <sojkam1@fel.cvut.cz>

  This file can be used and copied according to next
  license alternatives
   - MPL - Mozilla Public License
   - GPL - GNU Public License
   - other license provided by project originators
 *******************************************************************/

#include <i2str.h>

/**
 * Converts integer to string.
 * @param s Buffer to store the result.
 * @param val Value to convert.
 * @param len Minimal width of the converted strign (padded by ' ').
 * @param form Unused.
 * @return 0
 */
int i2str(char *s,long val,int len,int form)
{
  int sig;
  int dig=1;
  int padd=0;
  unsigned base=form&0xff;
  unsigned long u;
  unsigned long mag;
  unsigned long num;
  if(!base) base=10;
  if((sig=(val<0)&&(base==10))) num=-val;
  else num=val;
 
  mag=1;
  u=base*mag;
  while(num>=u){
    dig++;
    mag=u;
    if(mag>(unsigned long)(~(unsigned long)0)/base) break;
    u*=base;
  }

  if(len){
    padd=len-dig;
    if(sig) padd--;
  }
  if(padd<0) padd=0;


  if(form&I2STR_PAD_0) {
    if(sig) *(s++)='-';
    while(padd){
      *(s++)='0';
      padd--;
    }
  }else{
    while(padd){
      *(s++)=' ';
      padd--;
    }
    if(sig) *(s++)='-';
  }

  while(dig--){
    u=num/mag;
    if(u>9) *(s++)='A'-10+u;
    else *(s++)='0'+u;
    num=num%mag;
    mag/=base;
  }
  *s=0;
  return 0;
}

