#ifndef _MTD_SPI_CHIP_H
#define _MTD_SPI_CHIP_H

/*
Serial SPI Flash commands

Based on Microchip/SST SST25VF064C datasheet
64 Mbit SPI Serial Dual I/O Flash
*/

#define MTD_SPI_READ           0x03 /* addr 3, Dum 0, data 1 - inf */
#define MTD_SPI_READ_DUALIO    0xBB /* addr 3, Dum 1, data 1 - inf */
#define MTD_SPI_READ_DUALOUT   0x3B /* addr 3, Dum 1, data 1 - inf */
#define MTD_SPI_READ_HISPEED   0x0B /* addr 3, Dum 1, data 1 - inf */
#define MTD_SPI_ERASE_SECT4K   0x20 /* addr 3, Dum 0, data 0 */
#define MTD_SPI_ERASE_SECT32K  0x52 /* addr 3, Dum 0, data 0 */
#define MTD_SPI_ERASE_SECT64K  0xD8 /* addr 3, Dum 0, data 0 */
#define MTD_SPI_ERASE_FULL     0x60 /* addr 0, Dum 0, data 0 */
#define MTD_SPI_ERASE_FULL2    0xC7 /* addr 0, Dum 0, data 0 */
#define MTD_SPI_PROG_PAGE      0x02 /* addr 3, Dum 0, data 1 - 256 */
#define MTD_SPI_PROG_PAGE_DUAL 0xA2 /* addr 3, Dum 0, data 1 - 128 */
#define MTD_SPI_READ_STATUS    0x05 /* addr 0, Dum 0, data 1 - inf */
#define MTD_SPI_ENABLE_WRITE   0x50 /* addr 0, Dum 0, data 0 */
#define MTD_SPI_WRITE_STATUS   0x01 /* addr 0, Dum 0, data 1 */
#define MTD_SPI_WRITE_ENABLE   0x06 /* addr 0, Dum 0, data 0 */
#define MTD_SPI_WRITE_DISABLE  0x04 /* addr 0, Dum 0, data 0 */
#define MTD_SPI_READ_ID        0x90 /* addr 3, Dum 0, data 1 - inf  */
#define MTD_SPI_READ_ID2       0xAB /* addr 3, Dum 0, data 1 - inf  */
#define MTD_SPI_READ_JEDEC_ID  0x9F /* addr 0, Dum 0, data 3 - inf  */
#define MTD_SPI_ENABLE_HOLD    0xAA /* addr 0, Dum 0, data 0 */
#define MTD_SPI_READ_SID       0x88 /* addr 1, Dum 1, data 1 - 32 */
#define MTD_SPI_PROG_SID9      0xA5 /* addr 1, Dum 0, data 1 - 24 */
#define MTD_SPI_LOCKOUT_SID9   0x85 /* addr 0, Dum 0, data 0 */

#define MTD_SPI_ST_BUSY        0x01
#define MTD_SPI_ST_WEL         0x02
#define MTD_SPI_ST_BP_m        0x3C
#define MTD_SPI_ST_BP0         0x04
#define MTD_SPI_ST_BP1         0x08
#define MTD_SPI_ST_BP2         0x10
#define MTD_SPI_ST_BP3         0x20
#define MTD_SPI_ST_SEC1        0x40
#define MTD_SPI_ST_BPL         0x80

#define MTD_SPI_ERASESECT_MIN  0x1000

#endif /*_MTD_SPI_CHIP_H*/
