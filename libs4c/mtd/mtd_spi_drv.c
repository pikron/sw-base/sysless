/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  mtd_spi_drv.c - SPI connected Flash memories

  Copyright holders and project originators
    (C) 2001-2015 by Pavel Pisa pisa@cmp.felk.cvut.cz
    (C) 2002-2015 by PiKRON Ltd. http://www.pikron.com

 The COLAMI components can be used and copied under next licenses
   - MPL - Mozilla Public License
   - GPL - GNU Public License
   - LGPL - Lesser GNU Public License
   - and other licenses added by project originators
 Code can be modified and re-distributed under any combination
 of the above listed licenses. If contributor does not agree with
 some of the licenses, he can delete appropriate line.
 Warning, if you delete all lines, you are not allowed to
 distribute code or build project.
 *******************************************************************/

#include <stdint.h>
#include <malloc.h>
#include <string.h>
#include <spi_drv.h>
#include <mtd_spi_chip.h>
#include <mtd_spi_drv.h>

#include <cpu_def.h>

static int
mtd_spi_transfer_callback(struct spi_drv *drv, int code, struct spi_msg_head *msg);

static
void mtd_spi_setup_cmd(mtd_spi_state_t *msst, unsigned cmd,
     int len, unsigned rate_boost)
{
  rate_boost += 6;
  if (rate_boost > __mfld2val(SPI_MODE_RATE_m, ~0U))
    rate_boost = __mfld2val(SPI_MODE_RATE_m, ~0U);
  msst->spi_msg.flags = SPI_MSG_MODE_SET;
  msst->spi_msg.addr = msst->spi_addr;
  msst->spi_msg.size_mode = SPI_MODE_0 | SPI_MODE_8BIT |
           __val2mfld(SPI_MODE_RATE_m, rate_boost);
  msst->spi_msg.rq_len = len;
  msst->spi_msg.tx_buf = msst->tx_buff;
  msst->spi_msg.rx_buf = msst->rx_buff;
  msst->spi_msg.ifc = msst->spi_drv;
  msst->spi_msg.callback = mtd_spi_transfer_callback;
  msst->rx_buff[0] = 0;
  msst->rx_buff[1] = 0;

  msst->tx_buff[0] = cmd;
  msst->tx_buff[1] = 0;
}

static
void mtd_spi_fill_cmd_addr(mtd_spi_state_t *msst, unsigned cmd,
                           uint32_t addr, int dummy_bytes)
{
  msst->tx_buff[0] = cmd;
  msst->tx_buff[1] = (unsigned char)(addr >> 16);
  msst->tx_buff[2] = (unsigned char)(addr >> 8);
  msst->tx_buff[3] = (unsigned char)(addr >> 0);
  msst->spi_msg.rq_len = 4 + dummy_bytes;
}

static int
mtd_spi_transfer_callback(struct spi_drv *drv, int code, struct spi_msg_head *msg)
{
  mtd_spi_state_t *msst = UL_CONTAINEROF(msg, mtd_spi_state_t, spi_msg);
  int result = 0;
  size_t chunk_len;
  size_t chunk_limit;
  unsigned erase_cmd;

  if (code != SPI_MSG_FINISHED) {
    msst->msg_inpr = 0;
    msst->result = -2;
    return 1;
  }

  switch (msst->fsm_state) {
    case MTD_SPI_FSM_IDLE:
      break;
    case MTD_SPI_FSM_READ_START:
      msst->fsm_state = MTD_SPI_FSM_READ_WAIT;
      msst->try_cnt = 0;
    case MTD_SPI_FSM_READ_WAIT:
      if (msst->rx_buff[1] & MTD_SPI_ST_BUSY) {
        if (++msst->try_cnt > 100) {
          result = -3;
          break;
        }
      } else {
        msst->fsm_state = MTD_SPI_FSM_READ_HEAD;
        msst->spi_msg.size_mode |= SPI_MODE_KEEPCS;
        mtd_spi_fill_cmd_addr(msst, MTD_SPI_READ, msst->data_addr, 0);
      }
      if (spi_msg_rq_ins(msst->spi_drv, &msst->spi_msg) < 0) {
        result = -4;
        break;
      }
      return 1;
    case MTD_SPI_FSM_READ_HEAD:
      msst->fsm_state = MTD_SPI_FSM_READ_DATA;
      msst->spi_msg.size_mode &= ~SPI_MODE_KEEPCS;
      chunk_len = msst->data_len - msst->data_offs;
      if (chunk_len > 0x1000)
        chunk_len = 0x1000;
      msst->spi_msg.rq_len = chunk_len;
      msst->spi_msg.tx_buf = NULL;
      msst->spi_msg.rx_buf = msst->data_ptr + msst->data_offs;
      if (spi_msg_rq_ins_head(msst->spi_drv, &msst->spi_msg) < 0) {
        result = -5;
        break;
      }
      return 1;
    case MTD_SPI_FSM_READ_DATA:
      msst->data_offs += msst->spi_msg.rq_len;
      if (msst->data_offs < msst->data_len) {
        msst->spi_msg.tx_buf = msst->tx_buff;
        msst->spi_msg.rx_buf = msst->rx_buff;

        msst->spi_msg.size_mode |= SPI_MODE_KEEPCS;
        mtd_spi_fill_cmd_addr(msst, MTD_SPI_READ,
                              msst->data_addr + msst->data_offs, 0);

        msst->fsm_state = MTD_SPI_FSM_READ_HEAD;
        msst->try_cnt = 0;
        if (spi_msg_rq_ins(msst->spi_drv, &msst->spi_msg) < 0) {
          result = -5;
          break;
        }
        return 1;
      }
      result = 1;
      break;
    case MTD_SPI_FSM_WRITE_START:
      msst->fsm_state = MTD_SPI_FSM_WRITE_WAIT;
      msst->try_cnt = 0;
    case MTD_SPI_FSM_WRITE_WAIT:
      if (msst->rx_buff[1] & MTD_SPI_ST_BUSY) {
        if (++msst->try_cnt > 100) {
          result = -3;
          break;
        }
      } else {
        msst->fsm_state = MTD_SPI_FSM_WRITE_WREN;
        msst->tx_buff[0] = MTD_SPI_WRITE_ENABLE;
        msst->spi_msg.rq_len = 1;
      }
      if (spi_msg_rq_ins(msst->spi_drv, &msst->spi_msg) < 0) {
        result = -4;
        break;
      }
      return 1;
    case MTD_SPI_FSM_WRITE_WREN:
      msst->fsm_state = MTD_SPI_FSM_WRITE_HEAD;
      msst->spi_msg.size_mode |= SPI_MODE_KEEPCS;
      mtd_spi_fill_cmd_addr(msst, MTD_SPI_PROG_PAGE,
                            msst->data_addr + msst->data_offs, 0);
      if (spi_msg_rq_ins(msst->spi_drv, &msst->spi_msg) < 0) {
        result = -5;
        break;
      }
      return 1;
    case MTD_SPI_FSM_WRITE_HEAD:
      msst->fsm_state = MTD_SPI_FSM_WRITE_DATA;
      msst->spi_msg.size_mode &= ~SPI_MODE_KEEPCS;
      chunk_limit = msst->page_size;
      if (!msst->data_offs) {
        chunk_limit -= msst->data_addr & (msst->page_size - 1);
      }
      chunk_len = msst->data_len - msst->data_offs;
      if (chunk_len > chunk_limit)
        chunk_len = chunk_limit;

      msst->spi_msg.rq_len = chunk_len;
      msst->spi_msg.tx_buf = msst->data_ptr + msst->data_offs;
      msst->spi_msg.rx_buf = NULL;
      if (spi_msg_rq_ins_head(msst->spi_drv, &msst->spi_msg) < 0) {
        result = -5;
        break;
      }
      return 1;
    case MTD_SPI_FSM_WRITE_DATA:
      msst->data_offs += msst->spi_msg.rq_len;
      if (msst->data_offs < msst->data_len) {
        msst->fsm_state = MTD_SPI_FSM_WRITE_WAIT;

        msst->spi_msg.tx_buf = msst->tx_buff;
        msst->spi_msg.rx_buf = msst->rx_buff;

        msst->spi_msg.rq_len = 2;
        msst->tx_buff[0] = MTD_SPI_READ_STATUS;
        msst->tx_buff[1] = 0;

        msst->try_cnt = 0;
        if (spi_msg_rq_ins(msst->spi_drv, &msst->spi_msg) < 0) {
          result = -5;
          break;
        }
        return 1;
      }
      result = 1;
      break;
    case MTD_SPI_FSM_WRITE_STATUS:
      msst->fsm_state = MTD_SPI_FSM_SEQ_END;
      msst->tx_buff[0] = MTD_SPI_WRITE_STATUS;
      msst->spi_msg.rq_len = 2;
      if (spi_msg_rq_ins(msst->spi_drv, &msst->spi_msg) < 0) {
        result = -5;
        break;
      }
      return 1;
    case MTD_SPI_FSM_SEQ_END:
      result = 1;
      break;
    case MTD_SPI_FSM_ERASE_START:
      msst->fsm_state = MTD_SPI_FSM_ERASE_WAIT;
      msst->try_cnt = 0;
    case MTD_SPI_FSM_ERASE_WAIT:
      if (msst->rx_buff[1] & MTD_SPI_ST_BUSY) {
        if (++msst->try_cnt > 100) {
          result = -3;
          break;
        }
      } else {
        msst->fsm_state = MTD_SPI_FSM_ERASE_WREN;
        msst->tx_buff[0] = MTD_SPI_WRITE_ENABLE;
        msst->spi_msg.rq_len = 1;
      }
      if (spi_msg_rq_ins(msst->spi_drv, &msst->spi_msg) < 0) {
        result = -4;
        break;
      }
      return 1;
    case MTD_SPI_FSM_ERASE_WREN:
      msst->fsm_state = MTD_SPI_FSM_ERASE_PROCEED;
      size_t chunk_len = msst->data_len - msst->data_offs;;
      erase_cmd = MTD_SPI_ERASE_SECT4K;
      if (chunk_len >= 0x10000) {
        if (!((msst->data_addr + msst->data_offs) & 0xffff))
          erase_cmd = MTD_SPI_ERASE_SECT64K;
      } else if (chunk_len >= 0x8000) {
        if (!((msst->data_addr + msst->data_offs) & 0x7fff))
          erase_cmd = MTD_SPI_ERASE_SECT32K;
      }
      mtd_spi_fill_cmd_addr(msst, erase_cmd,
                            msst->data_addr + msst->data_offs, 0);
      if (spi_msg_rq_ins(msst->spi_drv, &msst->spi_msg) < 0) {
        result = -4;
        break;
      }
      return 1;
    case MTD_SPI_FSM_ERASE_PROCEED:
      if (msst->tx_buff[0] == MTD_SPI_ERASE_SECT64K)
        msst->data_offs += 0x10000;
      else if (msst->tx_buff[0] == MTD_SPI_ERASE_SECT32K)
        msst->data_offs += 0x8000;
      else
        msst->data_offs += 0x1000;
      if (msst->data_offs < msst->data_len) {
        msst->fsm_state = MTD_SPI_FSM_ERASE_WAIT;
        msst->spi_msg.rq_len = 2;
        msst->tx_buff[0] = MTD_SPI_READ_STATUS;
        msst->tx_buff[1] = 0;
        msst->try_cnt = 0;
        if (spi_msg_rq_ins(msst->spi_drv, &msst->spi_msg) < 0) {
          result = -5;
          break;
        }
        return 1;
      }
      result = 1;
      break;
    case MTD_SPI_FSM_CHIP_ERASE_START:
      msst->fsm_state = MTD_SPI_FSM_CHIP_ERASE_WAIT;
      msst->try_cnt = 0;
    case MTD_SPI_FSM_CHIP_ERASE_WAIT:
      if (msst->rx_buff[1] & MTD_SPI_ST_BUSY) {
        if (++msst->try_cnt > 100) {
          result = -3;
          break;
        }
      } else {
        msst->fsm_state = MTD_SPI_FSM_CHIP_ERASE_WREN;
        msst->tx_buff[0] = MTD_SPI_WRITE_ENABLE;
        msst->spi_msg.rq_len = 1;
      }
      if (spi_msg_rq_ins(msst->spi_drv, &msst->spi_msg) < 0) {
        result = -4;
        break;
      }
      return 1;
    case MTD_SPI_FSM_CHIP_ERASE_WREN:
      msst->fsm_state = MTD_SPI_FSM_CHIP_ERASE_PROCEED;
      msst->tx_buff[0] = MTD_SPI_ERASE_FULL;
      msst->spi_msg.rq_len = 1;
      if (spi_msg_rq_ins(msst->spi_drv, &msst->spi_msg) < 0) {
        result = -4;
        break;
      }
      return 1;
    case MTD_SPI_FSM_CHIP_ERASE_PROCEED:
      result = 1;
      break;
  }

  msst->fsm_state = MTD_SPI_FSM_IDLE;
  msst->msg_inpr = 0;
  msst->result = result;

  return 1;
}

int mtd_spi_wait_result(mtd_spi_state_t *msst)
{
  while(msst->msg_inpr > 0) {
    __memory_barrier();
  }
  if (msst->msg_inpr < 0)
    return -1;
  return msst->result;
}

int mtd_spi_read_write(mtd_spi_state_t *msst, void *data_ptr,
                 size_t data_len, uint32_t data_addr, int async,
                 unsigned int start_state, unsigned int rate_boost)
{
  int ready_fl;
  unsigned long flags;

  save_and_cli(flags);
  ready_fl = msst->msg_inpr <= 0;
  if (!msst->msg_inpr) {
    msst->msg_inpr = 1;
  }
  restore_flags(flags);
  if (!ready_fl)
    return -1;

  msst->fsm_state = start_state;
  msst->data_ptr = data_ptr;
  msst->data_addr = data_addr;
  msst->data_len = data_len;
  msst->data_offs = 0;

  mtd_spi_setup_cmd(msst, MTD_SPI_READ_STATUS, 2, rate_boost);
  if(spi_msg_rq_ins(msst->spi_drv, &msst->spi_msg) < 0) {
    msst->msg_inpr = -1;
    return -1;
  }

  if (async)
    return 0;
  return mtd_spi_wait_result(msst);
}

int mtd_spi_read(mtd_spi_state_t *msst, void *data_ptr,
                 size_t data_len, uint32_t data_addr, int async)
{
  return mtd_spi_read_write(msst, data_ptr, data_len, data_addr,
                            async, MTD_SPI_FSM_READ_START, 0);
}

int mtd_spi_write(mtd_spi_state_t *msst, void *data_ptr,
                 size_t data_len, uint32_t data_addr, int async)
{
  return mtd_spi_read_write(msst, data_ptr, data_len, data_addr,
                            async, MTD_SPI_FSM_WRITE_START, 0);
}

int mtd_spi_erase(mtd_spi_state_t *msst, uint32_t start_addr,
                  uint32_t len, int async)
{
  uint32_t align_offs = start_addr & (MTD_SPI_ERASESECT_MIN - 1);
  start_addr -= align_offs;
  len += align_offs;

  return mtd_spi_read_write(msst, NULL, len, start_addr,
                            async, MTD_SPI_FSM_ERASE_START, 0);
}

int mtd_spi_chip_erase(mtd_spi_state_t *msst, int mode, int async)
{
  return mtd_spi_read_write(msst, NULL, 0, 0,
                            async, MTD_SPI_FSM_CHIP_ERASE_START, 0);
}

int mtd_spi_set_protect_mode(mtd_spi_state_t *msst, unsigned protect, int async)
{
  int ready_fl;
  unsigned long flags;

  save_and_cli(flags);
  ready_fl = msst->msg_inpr <= 0;
  if (!msst->msg_inpr) {
    msst->msg_inpr = 1;
  }
  restore_flags(flags);
  if (!ready_fl)
    return -1;

  msst->fsm_state = MTD_SPI_FSM_WRITE_STATUS;
  msst->tx_buff[1] = __val2mfld(MTD_SPI_ST_BP_m , protect);

  mtd_spi_setup_cmd(msst, MTD_SPI_WRITE_ENABLE, 1, 0);

  if(spi_msg_rq_ins(msst->spi_drv, &msst->spi_msg) < 0) {
    msst->msg_inpr = -1;
    return -1;
  }

  if (async)
    return 0;
  return mtd_spi_wait_result(msst);
}

int mtd_spi_read_jedec_id(mtd_spi_state_t *msst,
                          unsigned char *id_buff, int buff_size)
{
  unsigned char buff[4];
  unsigned long flags;
  int ready_fl;

  save_and_cli(flags);
  ready_fl = msst->msg_inpr <= 0;
  if (!msst->msg_inpr) {
    msst->msg_inpr = 1;
  }
  restore_flags(flags);

  if (!ready_fl)
    return 0;

  msst->fsm_state = MTD_SPI_FSM_IDLE;
  mtd_spi_setup_cmd(msst, MTD_SPI_READ_JEDEC_ID, 4, 0);
  msst->spi_msg.rx_buf = buff;

  if(spi_msg_rq_ins(msst->spi_drv, &msst->spi_msg) < 0) {
    msst->msg_inpr = -1;
    return -1;
  }

  mtd_spi_wait_result(msst);

  if (buff_size > 3)
    buff_size = 3;

  memcpy(id_buff, buff + 1, buff_size);

  return 1;
}
