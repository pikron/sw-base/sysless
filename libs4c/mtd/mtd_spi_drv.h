#ifndef _MTD_SPI_DRV_H
#define _MTD_SPI_DRV_H

#include <stdint.h>
#include <spi_drv.h>

#define MTD_SPI_CMD_BUFF_SIZE 5

typedef struct mtd_spi_state_t {
  spi_drv_t *spi_drv;
  spi_msg_head_t spi_msg;
  unsigned char tx_buff[MTD_SPI_CMD_BUFF_SIZE];
  unsigned char rx_buff[MTD_SPI_CMD_BUFF_SIZE];
  unsigned char spi_addr;
  int msg_inpr;
  unsigned int fsm_state;
  int result;
  int try_cnt;
  void *data_ptr;
  size_t data_len;
  uint32_t data_addr;
  size_t data_offs;
  size_t page_size;
} mtd_spi_state_t;

typedef enum mtd_spi_fsm_state_t {
  MTD_SPI_FSM_IDLE,
  MTD_SPI_FSM_READ_START,
  MTD_SPI_FSM_READ_WAIT,
  MTD_SPI_FSM_READ_HEAD,
  MTD_SPI_FSM_READ_DATA,
  MTD_SPI_FSM_WRITE_START,
  MTD_SPI_FSM_WRITE_WAIT,
  MTD_SPI_FSM_WRITE_WREN,
  MTD_SPI_FSM_WRITE_HEAD,
  MTD_SPI_FSM_WRITE_DATA,
  MTD_SPI_FSM_WRITE_STATUS,
  MTD_SPI_FSM_SEQ_END,
  MTD_SPI_FSM_ERASE_START,
  MTD_SPI_FSM_ERASE_WAIT,
  MTD_SPI_FSM_ERASE_WREN,
  MTD_SPI_FSM_ERASE_PROCEED,
  MTD_SPI_FSM_CHIP_ERASE_START,
  MTD_SPI_FSM_CHIP_ERASE_WAIT,
  MTD_SPI_FSM_CHIP_ERASE_WREN,
  MTD_SPI_FSM_CHIP_ERASE_PROCEED,
} mtd_spi_fsm_state_t;

int mtd_spi_read(mtd_spi_state_t *msst, void *data_ptr,
                 size_t data_len, uint32_t data_addr, int async);

int mtd_spi_write(mtd_spi_state_t *msst, void *data_ptr,
                 size_t data_len, uint32_t data_addr, int async);

int mtd_spi_wait_result(mtd_spi_state_t *msst);

int mtd_spi_set_protect_mode(mtd_spi_state_t *msst, unsigned protect,
                             int async);

int mtd_spi_erase(mtd_spi_state_t *msst, uint32_t start_addr,
                  uint32_t len, int async);

int mtd_spi_chip_erase(mtd_spi_state_t *msst, int mode, int async);

int mtd_spi_read_jedec_id(mtd_spi_state_t *msst,
                          unsigned char *id_buff, int buff_size);

#endif /*_MTD_SPI_DRV_H*/
