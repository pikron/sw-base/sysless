/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  ser_drv.h - generic serial/UART drivers state

  Copyright holders and project originators
    (C) 2001-2016 by Pavel Pisa pisa@cmp.felk.cvut.cz
    (C) 2002-2016 by PiKRON Ltd. http://www.pikron.com

 The COLAMI components can be used and copied under next licenses
   - MPL - Mozilla Public License
   - GPL - GNU Public License
   - LGPL - Lesser GNU Public License
   - and other licenses added by project originators
 Code can be modified and re-distributed under any combination
 of the above listed licenses. If contributor does not agree with
 some of the licenses, he can delete appropriate line.
 Warning, if you delete all lines, you are not allowed to
 distribute code or build project.
 *******************************************************************/

#ifndef _SER_DRV_H
#define _SER_DRV_H

#include <stdint.h>
#include <ser_que.h>

#define SER_DRV_FLOWC_HW_m  0x1
#define SER_DRV_FLOWC_XON_m 0x2

#define SER_DRV_FL_ROR   0x1	/* Receive overrun error */
#define SER_DRV_FL_RFE   0x2	/* Framing error */
#define SER_DRV_FL_RPE   0x4	/* Parity error */
#define SER_DRV_FL_SXOFF 0x10	/* Request to send XOFF */
#define SER_DRV_FL_SXON  0x20	/* Request to send XON */
#define SER_DRV_FL_TFCDI 0x40	/* Transmission disabled */
#define SER_DRV_FL_RFCDI 0x80	/* Reception disenabled */
#define SER_DRV_FL_TIP   0x100	/* Transmittion at Progress */
#define SER_DRV_FL_TWCTS 0x200	/* Delaying Tx to CTS enabled */

struct ser_drv;
typedef struct ser_drv ser_drv_t;

typedef struct ser_drv_ops {
  int  (*init)(ser_drv_t *drvst);
  int  (*set_mode)(ser_drv_t *drvst, unsigned long baud, int mode, int flowc);
  int  (*send_chr)(ser_drv_t *drvst, int ch);
  int  (*rec_chr)(ser_drv_t *drvst);
  int  (*rec_ready)(ser_drv_t *drvst);
  int  (*send_ready)(ser_drv_t *drvst);
} ser_drv_ops_t;

struct ser_drv {
  const ser_drv_ops_t *drv_ops;
  short drv_flags;
  int   baud;
  int   mode;
  int   flowc;

  /* Queues */
  ser_que_t que_in;
  ser_que_t que_out;
};

static inline
int ser_drv_set_init_state(ser_drv_t *drvst)
{
  return drvst->drv_ops->init(drvst);
}

static inline
int ser_drv_set_mode(ser_drv_t *drvst, unsigned long baud, int mode, int flowc)
{
  return drvst->drv_ops->set_mode(drvst, baud, mode, flowc);
}

static inline
int ser_drv_send_chr(ser_drv_t *drvst, int ch)
{
  return drvst->drv_ops->send_chr(drvst, ch);
}

static inline
int ser_drv_rec_chr(ser_drv_t *drvst)
{
  return drvst->drv_ops->rec_chr(drvst);
}

static inline
int ser_drv_rec_ready(ser_drv_t *drvst)
{
  return drvst->drv_ops->rec_ready(drvst);
}

static inline
int ser_drv_send_ready(ser_drv_t *drvst)
{
  return drvst->drv_ops->send_ready(drvst);
}

static inline
void ser_drv_setup_buffers(ser_drv_t *drvst, void *buf_in, int buf_in_size,
                           void *buf_out, int buf_out_size)
{
  ser_que_setup_buffer(&drvst->que_in, buf_in, buf_in_size);
  ser_que_setup_buffer(&drvst->que_out, buf_out, buf_out_size);
}

#endif /*_SER_DRV_H*/
