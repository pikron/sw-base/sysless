/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  ser_internal.h - serial/UART driver internal defines

  Copyright holders and project originators
    (C) 2001-2016 by Pavel Pisa pisa@cmp.felk.cvut.cz
    (C) 2002-2016 by PiKRON Ltd. http://www.pikron.com

 The COLAMI components can be used and copied under next licenses
   - MPL - Mozilla Public License
   - GPL - GNU Public License
   - LGPL - Lesser GNU Public License
   - and other licenses added by project originators
 Code can be modified and re-distributed under any combination
 of the above listed licenses. If contributor does not agree with
 some of the licenses, he can delete appropriate line.
 Warning, if you delete all lines, you are not allowed to
 distribute code or build project.
 *******************************************************************/

#ifndef _SER_INTERNAL_H
#define _SER_INTERNAL_H

#include <ser_drv.h>

#ifndef WITH_RTEMS

#include <cpu_def.h>

typedef unsigned long ser_isr_lock_level_t;
#define ser_isr_lock   save_and_cli
#define	ser_isr_unlock restore_flags

#else /*WITH_RTEMS*/

#include <rtems.h>
#include <irq.h>

typedef rtems_interrupt_level ser_isr_lock_level_t;
#define ser_isr_lock   rtems_interrupt_disable
#define	ser_isr_unlock rtems_interrupt_enable

#endif /*WITH_RTEMS*/


#endif /*_SER_INTERNAL_H*/