/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  ser_que.h - generic serial/UART queues support

  Copyright holders and project originators
    (C) 2001-2016 by Pavel Pisa pisa@cmp.felk.cvut.cz
    (C) 2002-2016 by PiKRON Ltd. http://www.pikron.com

 The COLAMI components can be used and copied under next licenses
   - MPL - Mozilla Public License
   - GPL - GNU Public License
   - LGPL - Lesser GNU Public License
   - and other licenses added by project originators
 Code can be modified and re-distributed under any combination
 of the above listed licenses. If contributor does not agree with
 some of the licenses, he can delete appropriate line.
 Warning, if you delete all lines, you are not allowed to
 distribute code or build project.
 *******************************************************************/

#ifndef _SER_QUE_H
#define _SER_QUE_H

#include <stdint.h>

#define SER_QUE_INITIALIZER(a_buf_beg, a_buf_size) \
  { \
    .buf_beg = (uint8_t*)(a_buf_beg), \
    .buf_end = (uint8_t*)(a_buf_beg) + (a_buf_size), \
    .ip = (uint8_t*)(a_buf_beg), \
    .op = (uint8_t*)(a_buf_beg), \
  }

typedef struct{
    uint8_t *buf_beg; /* start of adress structur */
    uint8_t *buf_end; /* end of adress structur - beg+sizeof(struct) */
    uint8_t *ip;      /* actual position at queue */
    uint8_t *op;      /* position first unread char of queue */
} ser_que_t;

/* put character c into queue, if full return -1 */
static inline
int ser_que_put(ser_que_t *q, int c)
{
  uint8_t *p;
  p = q->ip;
  *(p++) = c;
  if (p == q->buf_end)
     p = q->buf_beg;
  if (p == q->op)
     return -1;
  q->ip = p;
  return c;
}

/* get character from queue, if empty return -1 */
static inline
int ser_que_get(ser_que_t *q)
{
  uint8_t *p;
  int  c;
  p = q->op;
  if (p == q->ip)
    return -1;
  c = *(p++);
  if (p == q->buf_end)
    p = q->buf_beg;
  q->op = p;
  return c;
}

/*return free space in queue*/
static inline
int ser_que_freecnt(ser_que_t *q)
{
  uint8_t *ip=q->ip, *op=q->op;
  return op - ip - 1 + (op <= ip? q->buf_end - q->buf_beg: 0);
}

static inline
int ser_que_incnt(ser_que_t *q)
{
  uint8_t *ip=q->ip, *op=q->op;
  return ip - op + (ip < op? q->buf_end - q->buf_beg: 0);
}

static inline
void ser_que_setup_buffer(ser_que_t *q, void *buf, int buf_size)
{
  q->buf_beg = (uint8_t *)buf;
  q->buf_end = q->buf_beg + buf_size;
  q->ip = q->op = q->buf_beg;
}

#endif /*_SER_QUE_H*/
