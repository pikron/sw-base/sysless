/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  lpcuart.c - UART driver for LPC17xx/178x and LPC40xx based chips

  Copyright holders and project originators
    (C) 2001-2016 by Pavel Pisa pisa@cmp.felk.cvut.cz
    (C) 2002-2016 by PiKRON Ltd. http://www.pikron.com

 The COLAMI components can be used and copied under next licenses
   - MPL - Mozilla Public License
   - GPL - GNU Public License
   - LGPL - Lesser GNU Public License
   - and other licenses added by project originators
 Code can be modified and re-distributed under any combination
 of the above listed licenses. If contributor does not agree with
 some of the licenses, he can delete appropriate line.
 Warning, if you delete all lines, you are not allowed to
 distribute code or build project.
 *******************************************************************/

#include <stdint.h>
#include <malloc.h>
#include <string.h>
#include <ser_drv.h>
#include <ser_internal.h>
#include <lpcuart.h>
#include <lpcuart_hwdefs.h>
#include <hal_machperiph.h>

IRQ_HANDLER_FNC(lpc_ser_drv_isr)
{
  ser_drv_t *drvst = (ser_drv_t *)irq_handler_get_context();
  lpc_ser_drv_t *lpsdst = ser_drv2lpc(drvst);
  typeof(lpsdst->ser_reg) ser_reg = lpsdst->ser_reg;
  unsigned int iir;
  unsigned int lsr;
  unsigned int msr;
  unsigned int irqloop = 0;
  int ch;
  (void)msr;

  do {
    iir = ser_reg->IIR;
    if (iir & LPC_UART_IIR_NO_INT)
      return irqloop? IRQ_HANDLED: IRQ_NONE;
    irqloop++;
    if (irqloop > 100000) {
      ser_reg->IER = 0x00;
      lpsdst->hwflg |= LPC_SER_HWFLG_IRQ_STUCK_m;
      return IRQ_HANDLED;
    }

    lsr = 0;
    switch (iir & LPC_UART_IIR_ID) {
      case LPC_UART_IIR_THRI:
        lsr = ser_reg->LSR;
        if (lsr & LPC_UART_LSR_THRE) {
          ch = ser_que_get(&drvst->que_out);
          if (ch >= 0) {
            do {
              ser_reg->THR = ch;
              lsr = ser_reg->LSR;
              if (!(lsr & LPC_UART_LSR_THRE))
                break;
              ch = ser_que_get(&drvst->que_out);
            } while(ch >= 0);
            lpsdst->tx_inpr = 1;
          } else {
            lpsdst->tx_inpr = 0;
          }
        }
        break;
      case LPC_UART_IIR_RDI:
        do {
          lsr = ser_reg->LSR;
          if (!(lsr & LPC_UART_LSR_DR))
            break;
          if ((drvst->flowc & SER_DRV_FLOWC_HW_m) &&
              (ser_que_freecnt(&drvst->que_in) <= 0)) {
            ser_reg->IER = ser_reg->IER & ~LPC_UART_IER_RDI;
            lpsdst->rx_full = 1;
            break;
          }
          ch = ser_reg->RBR;
          if (ser_que_put(&drvst->que_in, ch) < 0)
            drvst->drv_flags |= SER_DRV_FL_ROR;
        } while(1);
        break;
      case LPC_UART_IIR_RLSI:
        lsr = ser_reg->LSR;
        break;
      case LPC_UART_IIR_MSI:
        msr = ser_reg->MSR;
        break;
    }
    if (lsr & (LPC_UART_LSR_FE | LPC_UART_LSR_PE | LPC_UART_LSR_OE)) {
      if (lsr & LPC_UART_LSR_FE)
        drvst->drv_flags |= SER_DRV_FL_RFE;
      if (lsr & LPC_UART_LSR_PE)
        drvst->drv_flags |= SER_DRV_FL_RPE;
      if (lsr & LPC_UART_LSR_OE)
        drvst->drv_flags |= SER_DRV_FL_ROR;
    }

  } while(1);
}

int lpc_ser_init_hw(ser_drv_t *drvst)
{
  lpc_ser_drv_t *lpsdst = ser_drv2lpc(drvst);
  typeof(lpsdst->ser_reg) ser_reg = lpsdst->ser_reg;

  ser_reg->IER = 0x00;

  if (request_irq(lpsdst->irqnum, lpc_ser_drv_isr, 0, "lpc_uart", drvst) < 0)
    return -1;

  lpsdst->tx_inpr = 0;
  lpsdst->rx_full = 0;

  lpsdst->hwflg |= LPC_SER_HWFLG_INIT_DONE_m;

  return 0;
}

int lpc_ser_set_mode(ser_drv_t *drvst, unsigned long baud, int mode, int flowc)
{
  lpc_ser_drv_t *lpsdst = ser_drv2lpc(drvst);
  typeof(lpsdst->ser_reg) ser_reg = lpsdst->ser_reg;
  unsigned int lcr_mode = LPC_UART_8N1;
  unsigned int fcr_mode = LPC_UART_FIFO_8;
  unsigned int ier;
  unsigned int mcr;
  uint32_t baud_div;

  if (!(lpsdst->hwflg & LPC_SER_HWFLG_INIT_DONE_m))
   if (lpc_ser_init_hw(drvst) < 0)
     return -1;

  ser_reg->IER = 0x00;             /* disable all interrupts */
  //ser_reg->IIR = 0x00;             /* clear interrupt ID register */
  //ser_reg->LSR = 0x00;             /* clear line status register */

  if (baud == 0) {
    baud = 115200;
  }
  drvst->baud = baud;
  drvst->mode = mode;
  drvst->flowc = flowc;

  baud *= 16;
  baud_div = (PCLK + baud / 4) / baud;

  /* set the baudrate - DLAB must be set to access DLL/DLM */
  ser_reg->LCR = LPC_UART_LCR_DLAB;	/* set divisor latches (DLAB) */
  ser_reg->DLL = (uint8_t)baud_div;	/* set for baud low byte */
  ser_reg->DLM = (uint8_t)(baud_div >> 8); /* set for baud high byte */

  /* Set mode and clear DLAB */
  ser_reg->LCR = (lcr_mode & ~LPC_UART_LCR_DLAB);
  /* setup FIFO Control Register (fifo-enabled + xx trig)  */
  ser_reg->FCR = fcr_mode;

  mcr = LPC_UART_MCR_RTS | LPC_UART_MCR_DTR;
  ier = LPC_UART_IER_THRI | LPC_UART_IER_RDI | LPC_UART_IER_RLSI;

  if (flowc & SER_DRV_FLOWC_HW_m) {
    ier |= LPC_UART_IER_MSI;
    mcr |= LPC_UART_MCR_CTSEN;
    mcr |= LPC_UART_MCR_RTSEN;
  }

  ser_reg->MCR = mcr;
  ser_reg->IER = ier;

  return 0;
}

int lpc_ser_send_chr(ser_drv_t *drvst, int ch)
{
  int ch2tx;
  lpc_ser_drv_t *lpsdst = ser_drv2lpc(drvst);
  typeof(lpsdst->ser_reg) ser_reg = lpsdst->ser_reg;

  if (!(lpsdst->hwflg & LPC_SER_HWFLG_INIT_DONE_m))
    return -1;

  if (ser_que_put(&drvst->que_out, ch) < 0)
    return -1;

  __memory_barrier();

  if (lpsdst->tx_inpr)
    return ch;

  ch2tx = ser_que_get(&drvst->que_out);
  if (ch2tx >= 0) {
    lpsdst->tx_inpr = 1;
    __memory_barrier();
    ser_reg->THR = ch2tx;
  }
  return ch;
}

int lpc_ser_rec_chr(ser_drv_t *drvst)
{
  int chr;
  lpc_ser_drv_t *lpsdst = ser_drv2lpc(drvst);
  typeof(lpsdst->ser_reg) ser_reg = lpsdst->ser_reg;

  if (!(lpsdst->hwflg & LPC_SER_HWFLG_INIT_DONE_m))
    return -1;

  chr = ser_que_get(&drvst->que_in);

  if ((drvst->flowc & SER_DRV_FLOWC_HW_m) && lpsdst->rx_full) {
    ser_isr_lock_level_t saveif;
    ser_isr_lock(saveif);
    if (lpsdst->rx_full) {
      lpsdst->rx_full = 0;
      ser_reg->IER = ser_reg->IER | LPC_UART_IER_RDI;
    }
    ser_isr_unlock(saveif);
  }
  return chr;
}

int lpc_ser_send_ready(ser_drv_t *drvst)
{
  lpc_ser_drv_t *lpsdst = ser_drv2lpc(drvst);
  if (!(lpsdst->hwflg & LPC_SER_HWFLG_INIT_DONE_m))
    return -1;

  return ser_que_freecnt(&drvst->que_out);
}

int lpc_ser_rec_ready(ser_drv_t *drvst)
{
  lpc_ser_drv_t *lpsdst = ser_drv2lpc(drvst);
  if (!(lpsdst->hwflg & LPC_SER_HWFLG_INIT_DONE_m))
    return -1;

  return ser_que_incnt(&drvst->que_in);
}

int lpc_ser_set_init(ser_drv_t *drvst)
{
  lpc_ser_drv_t *lpsdst = ser_drv2lpc(drvst);

  if (!(lpsdst->hwflg & LPC_SER_HWFLG_INIT_DONE_m)) {
    if (lpc_ser_init_hw(drvst) < 0)
      return -1;
  }

  return lpc_ser_set_mode(drvst, drvst->baud, drvst->mode, drvst->flowc);
}

const ser_drv_ops_t lpc_ser_drv_ops = {
  .init = lpc_ser_set_init,
  .set_mode = lpc_ser_set_mode,
  .send_chr = lpc_ser_send_chr,
  .rec_chr = lpc_ser_rec_chr,
  .send_ready = lpc_ser_send_ready,
  .rec_ready = lpc_ser_rec_ready,
};

#if 0
/********************************************************************/
/* LPC UART test functions */

#include <lt_timer.h>
#include <stdio.h>

#define LPC_SER_BUF_SIZE 10

LPC_SER_DRV_STATE_DEFINE(1, 115200, 0, SER_DRV_FLOWC_HW_m, LPC_SER_BUF_SIZE)

int lpc_ser_uart1_init(void)
{
  int ch;
  ser_drv_t *drvst = LPC_SER_DRV_STATE_PTR(1);

  if (ser_drv_set_init_state(drvst) < 0)
    return -1;

  for (ch = '0'; ch <= 'z'; ch++)
    ser_drv_send_chr(drvst, ch);

  return 0;
}

int lpc_ser_uart1_poll(void)
{
  static lt_mstime_t ltime;
  int ch;
  ser_drv_t *drvst = LPC_SER_DRV_STATE_PTR(1);
  lpc_ser_drv_t *lpsdst = ser_drv2lpc(drvst);
  typeof(lpsdst->ser_reg) ser_reg = lpsdst->ser_reg;

  lt_mstime_update();

  if ((actual_msec - ltime) > 200) {
    ltime += 200;
    //ser_drv_send_chr(drvst, 'T');
    printf("U1: 0x%08x %d %d %d\n", lpsdst->hwflg, lpsdst->tx_inpr,
           lpsdst->rx_full, ser_reg->MSR & LPC_UART_MSR_CTS? 1: 0);

    while (ser_drv_send_ready(drvst) >= 2) {
      if ((ch = ser_drv_rec_chr(drvst)) < 0)
        break;
      //ch = ser_reg->MSR & LPC_UART_MSR_CTS? '1': '0';
      ser_drv_send_chr(drvst, ch);
      //ser_drv_send_chr(drvst, '\n');
    }

  }
  return 0;
}

#endif
