/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  lpcuart.h - UART driver for LPC17xx/178x and LPC40xx based chips

  Copyright holders and project originators
    (C) 2001-2016 by Pavel Pisa pisa@cmp.felk.cvut.cz
    (C) 2002-2016 by PiKRON Ltd. http://www.pikron.com

 The COLAMI components can be used and copied under next licenses
   - MPL - Mozilla Public License
   - GPL - GNU Public License
   - LGPL - Lesser GNU Public License
   - and other licenses added by project originators
 Code can be modified and re-distributed under any combination
 of the above listed licenses. If contributor does not agree with
 some of the licenses, he can delete appropriate line.
 Warning, if you delete all lines, you are not allowed to
 distribute code or build project.
 *******************************************************************/

#ifndef _LPCUART_H
#define _LPCUART_H

#include <ser_drv.h>
#include <system_def.h>

#if defined(MACH_LPC178X)
 #include <LPC177x_8x.h>
 #ifndef PCONP
  #define PCONP (LPC_SC->PCONP)
 #endif
#elif defined(MACH_LPC17XX)
 #include <LPC17xx.h>
 #ifndef PCONP
  #define PCONP (SC->PCONP)
 #endif
 #if !defined(LPC_UART0) && defined(UART0)
  #define LPC_UART0 UART0
 #endif
 #if !defined(LPC_UART1) && defined(UART1)
  #define LPC_UART1 UART1
 #endif
#else
 #error LPCUART not supported for this machine (configured?)
#endif

#include <hal_gpio.h>

#define LPC_SER_HWFLG_INIT_DONE_m 1
#define LPC_SER_HWFLG_IRQ_STUCK_m 2

typedef struct lpc_ser_drv {
  ser_drv_t ser_drv;
  typeof(LPC_UART1) ser_reg;
  int irqnum;
  unsigned int hwflg;
  char tx_inpr;
  char rx_full;
} lpc_ser_drv_t;

static inline
lpc_ser_drv_t *ser_drv2lpc(ser_drv_t *drvst)
{
  lpc_ser_drv_t *lpsdst;
 #ifdef UL_CONTAINEROF
  lpsdst = UL_CONTAINEROF(drvst, lpc_ser_drv_t, ser_drv);
 #else /*UL_CONTAINEROF*/
  lpsdst = (lpc_ser_drv_t*)((char*)drvst - __builtin_offsetof(lpc_ser_drv_t, ser_drv));
 #endif /*UL_CONTAINEROF*/
  return lpsdst;
}

extern const ser_drv_ops_t lpc_ser_drv_ops;

#define LPC_SER_DRV_STATE_DEFINE_BUFFERS_PROVIDED(a_channel, a_baud, a_mode, a_flowc, \
                   a_in_buf, a_in_buf_size, a_out_buf, a_out_buf_size) \
  lpc_ser_drv_t lpc_ser_drv_uart##a_channel##_state = { \
    .ser_drv = { \
      .drv_ops = &lpc_ser_drv_ops, \
      .que_in  = SER_QUE_INITIALIZER((a_in_buf), (a_in_buf_size)), \
      .que_out = SER_QUE_INITIALIZER((a_out_buf), (a_out_buf_size)), \
      .baud = (a_baud), \
      .mode = (a_mode), \
      .flowc = (a_flowc), \
    }, \
    .ser_reg = LPC_UART##a_channel, \
    .irqnum = UART##a_channel##_IRQn, \
  };

#define LPC_SER_DRV_STATE_DEFINE(a_channel, a_baud, a_mode, a_flowc, a_buf_size) \
  uint8_t lpc_ser_drv_uart##a_channel##_buf_in[(a_buf_size)]; \
  uint8_t lpc_ser_drv_uart##a_channel##_buf_out[(a_buf_size)]; \
  LPC_SER_DRV_STATE_DEFINE_BUFFERS_PROVIDED(a_channel, a_baud, a_mode, a_flowc, \
          lpc_ser_drv_uart##a_channel##_buf_in, (a_buf_size), \
          lpc_ser_drv_uart##a_channel##_buf_out, (a_buf_size))

#define LPC_SER_DRV_STATE_PTR(a_channel) \
  (&lpc_ser_drv_uart##a_channel##_state.ser_drv)

#endif /*_LPCUART_H*/
