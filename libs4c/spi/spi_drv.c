/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  spi_drv.c - SPI communication common code

  Copyright holders and project originators
    (C) 2001-2010 by Pavel Pisa pisa@cmp.felk.cvut.cz
    (C) 2002-2010 by PiKRON Ltd. http://www.pikron.com

 The COLAMI components can be used and copied under next licenses
   - MPL - Mozilla Public License
   - GPL - GNU Public License
   - LGPL - Lesser GNU Public License
   - and other licenses added by project originators
 Code can be modified and re-distributed under any combination
 of the above listed licenses. If contributor does not agree with
 some of the licenses, he can delete appropriate line.
 Warning, if you delete all lines, you are not allowed to
 distribute code or build project.
 *******************************************************************/

#include <inttypes.h>
#include <malloc.h>
#include <string.h>
#include <spi_drv.h>
#include "spi_internal.h"

/********************************************************************/
/* Generic SPI functions */

int spi_msg_rq_ins(spi_drv_t *ifc, spi_msg_head_t *msg)
{
  spi_isr_lock_level_t saveif;

  if(!ifc) return -1;
  if(!(ifc->flags&SPI_IFC_ON)) return -1;
  spi_isr_lock(saveif);
  spi_rq_queue_insert(ifc,msg);
  spi_isr_unlock(saveif);
  ifc->ctrl_fnc(ifc,SPI_CTRL_WAKE_RQ,NULL);
  return 0;
}

int spi_msg_rq_ins_head(spi_drv_t *ifc, spi_msg_head_t *msg)
{
  spi_isr_lock_level_t saveif;

  if(!ifc) return -1;
  if(!(ifc->flags&SPI_IFC_ON)) return -1;
  spi_isr_lock(saveif);
  spi_rq_queue_ins_head(ifc,msg);
  spi_isr_unlock(saveif);
  ifc->ctrl_fnc(ifc,SPI_CTRL_WAKE_RQ,NULL);
  return 0;
}

int spi_msg_rq_rem(spi_msg_head_t *msg)
{
  int act=0;
  spi_drv_t *ifc;
  spi_isr_lock_level_t saveif;

  spi_isr_lock(saveif);
  spi_rq_queue_del_item(msg);
  if((ifc=msg->ifc))
    act=(msg==ifc->msg_act);
  spi_isr_unlock(saveif);
  return 0;
}

int spi_flush_all(spi_drv_t *ifc)
{
  spi_msg_head_t *msg;
  int act;
  spi_isr_lock_level_t saveif;

  do{
    act=0;
    spi_isr_lock(saveif);
    msg=spi_rq_queue_cut_first(ifc);
    if(msg)
      act=(msg==ifc->msg_act);
    spi_isr_unlock(saveif);
    if(!msg)
      break;
    msg->flags |= SPI_MSG_ABORT;
    if(msg->callback) {
	msg->callback(ifc,SPI_MSG_ABORT,msg);
    }
  }while(1);
  return 0;
}


int spi_transfer_callback(struct spi_drv *ifc, int code, struct spi_msg_head *msg)
{
  if(msg->private){
   #ifdef WITH_RTEMS
    rtems_task_resume(msg->private);
   #endif /*WITH_RTEMS*/
    msg->private=0;
  }
  return 0;
}

int spi_transfer_with_mode(spi_drv_t *ifc, int addr, int rq_len,
                   const void *tx_buf, void *rx_buf, int size_mode)
{

  spi_msg_head_t msg;
 #ifdef WITH_RTEMS
  rtems_id tid;

  rtems_task_ident(RTEMS_SELF, 0, &tid);
 #endif /*WITH_RTEMS*/

  msg.flags = 0;
  msg.ifc = NULL;
  spi_rq_queue_init_detached(&msg);
  msg.addr = addr;
  msg.rq_len = rq_len;
  msg.tx_buf = tx_buf;
  msg.rx_buf = rx_buf;
  msg.callback = spi_transfer_callback;
 #ifdef WITH_RTEMS
  msg.private = tid;
 #else /*WITH_RTEMS*/
  msg.private = 1;
 #endif /*WITH_RTEMS*/
  msg.size_mode = 0;

  if(size_mode != -1) {
    msg.flags |= SPI_MSG_MODE_SET;
    msg.size_mode = size_mode;
  }

  if(spi_msg_rq_ins(ifc, &msg)<0)
    return -1;

  { /* Wait for the request completion */
   #ifdef WITH_RTEMS
    rtems_mode prev_mode_set;

    rtems_task_mode(RTEMS_NO_PREEMPT,RTEMS_PREEMPT_MASK,&prev_mode_set);
    while(msg.private) {
      rtems_task_suspend(RTEMS_SELF);
    }
    rtems_task_mode(prev_mode_set,RTEMS_PREEMPT_MASK,&prev_mode_set);
   #else /*WITH_RTEMS*/
    while(msg.private) {
      __memory_barrier();
    }
   #endif /*WITH_RTEMS*/
  }

  if(msg.flags & (SPI_MSG_FAIL | SPI_MSG_ABORT))
    return -1;

  return msg.rq_len;
}

int spi_transfer(spi_drv_t *ifc, int addr, int rq_len,
                   const void *tx_buf, void *rx_buf)
{
  return spi_transfer_with_mode(ifc, addr, rq_len, tx_buf, rx_buf, -1);
}

/********************************************************************/
/* Test code */

#include <utils.h>
#include <stdio.h>
#include <ctype.h>
#include <cmd_proc.h>

#define TEST_BUF 64

#if 0

cmdproc SPIMST: INIT

cmdproc SPIMST: 0A4 (0a,1a,2a,3a,4a,5a,6a,7a,8a,9a,aa,ba,ca,da,ea,fa)
cmdproc SPISTAT?
cmdproc SPISTAT: TXD=0xB0
cmdproc SPISTAT: INT=0x00
cmdproc SPISTAT: CON=0x00

#endif

#if 0

uint8_t spi_test_buf_tx[TEST_BUF]={0x10,0x11,0x22,0x33};
uint8_t spi_test_buf_rx[TEST_BUF];

int spi_test_callback(struct spi_drv *ifc, int code, struct spi_msg_head *msg);

spi_msg_head_t spi_test_msg={
    tx_buf:spi_test_buf_tx,
    rx_buf:spi_test_buf_rx,

    rq_len:10,
    addr:0x1,
    flags:0,
    callback:spi_test_callback
  };


int spi_test_callback(struct spi_drv *ifc, int code, struct spi_msg_head *msg)
{
  int i;

  printf("SPI! %02X ",msg->addr);
  if(msg->flags&SPI_MSG_FAIL) printf("FAIL ");
  printf("TX(");
  for(i=0;i<msg->rq_len;i++) printf("%s%02X",i?",":"",msg->tx_buf[i]);
  printf(" RX(");
  for(i=0;i<msg->rq_len;i++) printf("%s%02X",i?",":"",msg->rx_buf[i]);
  printf(")");
  printf("\n");
  return 0;
}

static int test_rd_arr(char **ps, uint8_t *buf, int n)
{
  long val;
  int c;
  int i;

  if(si_fndsep(ps,"({")<0) return -CMDERR_BADSEP;
  i=0;
  si_skspace(ps);
  if((**ps!=')') && (**ps!='}')) do{
    if(i>=n) return -CMDERR_BADPAR;
    if(si_long(ps,&val,16)<0) return -CMDERR_BADPAR;
    buf[i]=val;
    i++;
    if((c=si_fndsep(ps,",)}"))<0) return -CMDERR_BADSEP;
  }while(c==',');
  return i;
}

int cmd_do_spimst(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  spi_drv_t *ifc=spi_find_drv(NULL, 0);
  spi_msg_head_t *msg=&spi_test_msg;
  int i;
  char *p;
  long val;

  if(*param[2]!=':') return -CMDERR_OPCHAR;

  if(!(ifc->flags&SPI_IFC_ON)){
    if(spi_mx1_init(ifc)<0)
      return -CMDERR_BADCFG;
  }

  spi_msg_rq_rem(msg);
  msg->flags&=~SPI_MSG_FAIL;
  p=param[3];

  si_skspace(&p);
  if(isdigit(*p)){
    if(si_long(&p,&val,16)<0) return -CMDERR_BADPAR;
    msg->addr=val;
  }
  si_skspace(&p);
  i=test_rd_arr(&p, msg->tx_buf, TEST_BUF);
  if(i<0) return i;
  msg->rq_len=i;

  /*
  spi_test_msg.tx_rq=4;
  spi_test_msg.rx_rq=3;
  spi_test_msg.addr=0xA0;
  spi_test_msg.flags=SPI_MSG_MS_TX;
  */

  spi_msg_rq_ins(ifc,msg);

  /*cmd_io_write(cmd_io,param[0],param[2]-param[0]);
  cmd_io_putc(cmd_io,'=');
  cmd_io_write(cmd_io,param[3],strlen(param[3]));*/
  return 0;
}

cmd_des_t const cmd_des_spimst={0, CDESM_OPCHR,
			"SPIMST","SPI master communication request",
			cmd_do_spimst,{}};

#endif