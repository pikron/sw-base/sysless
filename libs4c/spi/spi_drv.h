/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  spi_drv.h - SPI communication driver interface

  Copyright holders and project originators
    (C) 2001-2004 by Pavel Pisa pisa@cmp.felk.cvut.cz
    (C) 2002-2004 by PiKRON Ltd. http://www.pikron.com

 The COLAMI components can be used and copied under next licenses
   - MPL - Mozilla Public License
   - GPL - GNU Public License
   - LGPL - Lesser GNU Public License
   - and other licenses added by project originators
 Code can be modified and re-distributed under any combination
 of the above listed licenses. If contributor does not agree with
 some of the licenses, he can delete appropriate line.
 Warning, if you delete all lines, you are not allowed to
 distribute code or build project.
 *******************************************************************/

#ifndef _SPI_IFC_H_
#define _SPI_IFC_H_

#include <inttypes.h>
#include <ul_list.h>

struct spi_drv;

#define SPI_MSG_FINISHED 0x040
#define SPI_MSG_ABORT    0x020
#define SPI_MSG_FAIL     0x010
#define SPI_MSG_MODE_SET 0x080

typedef struct spi_msg_head {
    uint16_t flags;	/* message flags */
    uint16_t addr;	/* message destination address */
    uint16_t size_mode;	/* message frame len and mode */
    uint16_t rq_len;	/* requested transfer length */
    const uint8_t *tx_buf;	/* pointer to TX data */
    uint8_t *rx_buf;	/* pointer to RX data */
    ul_list_node_t node;
    struct spi_drv *ifc;
    int (*callback)(struct spi_drv *ifc, int code, struct spi_msg_head *msg);
    long private;
  } spi_msg_head_t;

#define SPI_MODE_RATE_m 0x0700   /* transfer rate selection */

#define SPI_MODE_KEEPCS 0x0800   /* keep CS active after end of transfer */

#define SPI_MODE_CPHA   0x1000   /* 0 .. inactive LO, 1 .. inactive HI */
#define SPI_MODE_CPOL   0x2000   /* 0 .. data before CLK, 1 .. after CLK */
#define SPI_MODE_0      (0|0)
#define SPI_MODE_1      (0|SPI_MODE_CPHA)
#define SPI_MODE_2      (SPI_MODE_CPOL|0)
#define SPI_MODE_3      (SPI_MODE_CPOL|SPI_MODE_CPHA)

#define SPI_MODE_BITS_m 0x003f
#define SPI_MODE_8BIT   0x0008
#define SPI_MODE_16BIT  0x0010
#define SPI_MODE_32BIT  0x0020

typedef int (spi_ctrl_fnc_t)(struct spi_drv *ifc, int ctrl, void *p);

#define SPI_IFC_ON 1

typedef struct spi_drv {
    uint16_t flags;
    uint16_t self_addr;
    ul_list_head_t rq_queue;
    spi_msg_head_t *msg_act;
    spi_ctrl_fnc_t *ctrl_fnc;
    long private;
  } spi_drv_t;

#define SPI_CTRL_WAKE_RQ 1

spi_drv_t *spi_find_drv(char *name, int number);
int spi_msg_rq_ins(spi_drv_t *ifc, spi_msg_head_t *msg);
int spi_msg_rq_ins_head(spi_drv_t *ifc, spi_msg_head_t *msg);
int spi_msg_rq_rem(spi_msg_head_t *msg);
int spi_flush_all(spi_drv_t *ifc);
int spi_transfer(spi_drv_t *ifc, int addr, int rq_len,
                   const void *tx_buf, void *rx_buf);
int spi_transfer_with_mode(spi_drv_t *ifc, int addr, int rq_len,
                   const void *tx_buf, void *rx_buf, int size_mode);

#endif /* _SPI_IFC_H_ */
