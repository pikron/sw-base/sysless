/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  spi_internal.h - SPI communication

  Copyright holders and project originators
    (C) 2001-2004 by Pavel Pisa pisa@cmp.felk.cvut.cz
    (C) 2002-2004 by PiKRON Ltd. http://www.pikron.com

 The COLAMI components can be used and copied under next licenses
   - MPL - Mozilla Public License
   - GPL - GNU Public License
   - LGPL - Lesser GNU Public License
   - and other licenses added by project originators
 Code can be modified and re-distributed under any combination
 of the above listed licenses. If contributor does not agree with
 some of the licenses, he can delete appropriate line.
 Warning, if you delete all lines, you are not allowed to
 distribute code or build project.
 *******************************************************************/

#ifndef _SPI_INTERNAL_H_
#define _SPI_INTERNAL_H_

#include <spi_drv.h>

#undef  WITH_RTEMS
#undef  DEBUG

#ifndef WITH_RTEMS

#include <cpu_def.h>

typedef unsigned long spi_isr_lock_level_t;
#define spi_isr_lock   save_and_cli
#define	spi_isr_unlock restore_flags

#else /*WITH_RTEMS*/

#include <rtems.h>
#include <rtems/io.h>
#include <rtems/libio.h>
#include <rtems/error.h>
#include <irq.h>
#include <stdio.h>
#include <drvsupport.h>

typedef rtems_interrupt_level spi_isr_lock_level_t;
#define spi_isr_lock   rtems_interrupt_disable
#define	spi_isr_unlock rtems_interrupt_enable

#endif /*WITH_RTEMS*/

UL_LIST_CUST_DEC(spi_rq_queue, spi_drv_t, spi_msg_head_t,\
                rq_queue, node)

#endif /* _SPI_INTERNAL_H_ */
