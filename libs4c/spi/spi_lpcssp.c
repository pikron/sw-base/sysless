/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  spi_lpcssp.c - SPI communication driver for LPC17xx SSP interface

  Copyright holders and project originators
    (C) 2001-2010 by Pavel Pisa pisa@cmp.felk.cvut.cz
    (C) 2002-2010 by PiKRON Ltd. http://www.pikron.com

 The COLAMI components can be used and copied under next licenses
   - MPL - Mozilla Public License
   - GPL - GNU Public License
   - LGPL - Lesser GNU Public License
   - and other licenses added by project originators
 Code can be modified and re-distributed under any combination
 of the above listed licenses. If contributor does not agree with
 some of the licenses, he can delete appropriate line.
 Warning, if you delete all lines, you are not allowed to
 distribute code or build project.
 *******************************************************************/

#include <inttypes.h>
#include <malloc.h>
#include <string.h>
#include <spi_drv.h>
#include "spi_internal.h"

#include <LPC17xx.h>
#include <system_def.h>
#include <hal_gpio.h>

#if !defined(SSP0) && defined(LPC_SSP0)
#define SSP0 LPC_SSP0
#endif
#if !defined(SSP1) && defined(LPC_SSP1)
#define SSP1 LPC_SSP1
#endif
#if !defined(SSP2) && defined(LPC_SSP2)
#define SSP2 LPC_SSP2
#endif
#if !defined(SC) && defined(LPC_SC)
#define SC LPC_SC
#endif

#define SSP_CR0_DSS_m  0x000f  /* Data Size Select (num bits - 1) */
#define SSP_CR0_FRF_m  0x0030  /* Frame Format: 0 SPI, 1 TI, 2 Microwire */
#define SSP_CR0_CPOL_m 0x0040  /* SPI Clock Polarity. 0 low between frames, 1 high */
#define SSP_CR0_CPHA_m 0x0080  /* SPI Clock Phase. 0 capture in midle, 1 at end */
#define SSP_CR0_SCR_m  0xff00  /* Serial Clock Rate. PCLK / (CPSDVSR × [SCR+1]) */

#define SSP_CR1_LBM_m  0x0001  /* Loop Back Mode */
#define SSP_CR1_SSE_m  0x0002  /* SSP Enable */
#define SSP_CR1_MS_m   0x0004  /* Master (0)/Slave (1) Mode */
#define SSP_CR1_SOD_m  0x0008  /* Slave Output Disable */

#define SSP_SR_TFE_m   0x0001 /* Tx FIFO Empty */
#define SSP_SR_TNF_m   0x0002 /* Tx FIFO Not Full */
#define SSP_SR_RNE_m   0x0004 /* Rx FIFO Not Empty */
#define SSP_SR_RFF_m   0x0008 /* Rx FIFO Full */
#define SSP_SR_BSY_m   0x0010 /* SSP is busy or the Tx FIFO is not empty */

/* interrupt sources assignment for IMSC, RIS, MIS and ICR registers */
#define SSP_IRQ_ROR_m 1 /* Rx frame owerwritten old data in full Rx FIFO */
#define SSP_IRQ_RT_m  2 /* Rx FIFO is not empty and not read in timeout  */
#define SSP_IRQ_RX_m  4 /* Rx FIFO is at least half full */
#define SSP_IRQ_TX_m  8 /* Tx FIFO is at least half empty */

#define SSP_DMACR_RXDMAE_m 0x1 /* DMA for the Rx FIFO enable */
#define SSP_DMACR_TXDMAE_m 0x2 /* DMA for the Tx FIFO enable */

/********************************************************************/
/* Motorola LPC17xx SSP specific SPI functions */

IRQ_HANDLER_FNC(spi_lpcssp_isr);
static int spi_lpcssp_ctrl_fnc(struct spi_drv *ifc, int ctrl, void *p);

#define SPI_LPCSSP_CS_PIN_COUNT 4

typedef struct spi_lpcssp_drv {
    spi_drv_t spi_drv;
  #ifdef WITH_RTEMS
    rtems_id  task;
    rtems_irq_connect_data *irq_data;
  #endif /*WITH_RTEMS*/
    typeof(SSP0) ssp_regs;
    int irqnum;
    unsigned cs_gpio_pin[SPI_LPCSSP_CS_PIN_COUNT];
    unsigned txcnt;
    unsigned rxcnt;
    char data16_fl;
  } spi_lpcssp_drv_t;

#define SSP_PIN_NA ((unsigned)-1)

static inline void
spi_lpcssp_assert_ss(spi_drv_t *ifc, int addr)
{
  int i;
  spi_lpcssp_drv_t *lpcssp_drv=UL_CONTAINEROF(ifc,spi_lpcssp_drv_t,spi_drv);

  for(i = 0; i < SPI_LPCSSP_CS_PIN_COUNT; i++, addr >>= 1) {
    if(lpcssp_drv->cs_gpio_pin[i]!=SSP_PIN_NA)
      hal_gpio_set_value(lpcssp_drv->cs_gpio_pin[i], !(addr & 1));
  }
}

static inline void
spi_lpcssp_negate_ss(spi_drv_t *ifc)
{
  int i;
  spi_lpcssp_drv_t *lpcssp_drv=UL_CONTAINEROF(ifc,spi_lpcssp_drv_t,spi_drv);

  for(i = 0; i < SPI_LPCSSP_CS_PIN_COUNT; i++) {
    if(lpcssp_drv->cs_gpio_pin[i]!=SSP_PIN_NA)
      hal_gpio_set_value(lpcssp_drv->cs_gpio_pin[i], 1);
  }
}

/* master transaction finished => generate STOP */
static inline int
spi_lpcssp_sfnc_ms_end(struct spi_drv *ifc, int code)
{
  spi_isr_lock_level_t saveif;
  spi_msg_head_t *msg;

  spi_isr_lock(saveif);
  msg=ifc->msg_act;
  if(!msg) {
    spi_isr_unlock(saveif);
  } else {
    spi_rq_queue_del_item(msg);
    msg->flags|=SPI_MSG_FINISHED;
    ifc->msg_act = NULL;
    spi_isr_unlock(saveif);
    if(msg->callback)
      msg->callback(ifc,SPI_MSG_FINISHED,msg);
  }

  return 0;
}


/* master mode error condition */
static inline int
spi_lpcssp_sfnc_ms_err(struct spi_drv *ifc, int code)
{
  spi_isr_lock_level_t saveif;
  spi_msg_head_t *msg;

  spi_isr_lock(saveif);
  msg=ifc->msg_act;
  if(!msg) {
    spi_isr_unlock(saveif);
  } else {
    spi_rq_queue_del_item(msg);
    msg->flags|=SPI_MSG_FAIL;
    ifc->msg_act = NULL;
    spi_isr_unlock(saveif);

    if(msg->callback)
      msg->callback(ifc,SPI_MSG_FAIL,msg);
  }

  return 0;
}

unsigned char spi_lpcssp_rate_table[] = {
  15,
  31,
  15,
  7,
  3,
  2,
  1,
  0
};

IRQ_HANDLER_FNC(spi_lpcssp_isr)
{
  spi_isr_lock_level_t saveif;
  spi_msg_head_t *msg;
  unsigned dr, rxcnt, txcnt, rq_len;
  char data16_fl;
  int stop_fl;
  spi_lpcssp_drv_t *lpcssp_drv = (spi_lpcssp_drv_t*)irq_handler_get_context();

  do {
    msg=lpcssp_drv->spi_drv.msg_act;
    if(!msg) {
      spi_isr_lock(saveif);
      msg=lpcssp_drv->spi_drv.msg_act=spi_rq_queue_first(&lpcssp_drv->spi_drv);
      spi_isr_unlock(saveif);
      if(!msg) {
        lpcssp_drv->ssp_regs->IMSC = 0;
        return IRQ_HANDLED;
      }
      while(lpcssp_drv->ssp_regs->SR & SSP_SR_RNE_m)
        dr=lpcssp_drv->ssp_regs->DR;

      lpcssp_drv->txcnt = 0;
      lpcssp_drv->rxcnt = 0;
      lpcssp_drv->data16_fl = 0;

      if(msg->flags & SPI_MSG_MODE_SET) {
        unsigned int rate;

        rate = __mfld2val(SPI_MODE_RATE_m, msg->size_mode);
        if (rate >= sizeof(spi_lpcssp_rate_table))
          rate = sizeof(spi_lpcssp_rate_table) - 1;

        rate = spi_lpcssp_rate_table[rate];

        if(msg->size_mode & SPI_MODE_16BIT)
          lpcssp_drv->data16_fl = 1;

        lpcssp_drv->ssp_regs->CR0 =
                     __val2mfld(SSP_CR0_DSS_m, lpcssp_drv->data16_fl? 16 - 1 : 8 - 1) |
                     __val2mfld(SSP_CR0_FRF_m, 0) |
                     (msg->size_mode & SPI_MODE_CPOL? SSP_CR0_CPOL_m: 0) |
                     (msg->size_mode & SPI_MODE_CPHA? SSP_CR0_CPHA_m: 0) |
                     __val2mfld(SSP_CR0_SCR_m, rate);

      }

      spi_lpcssp_assert_ss(&lpcssp_drv->spi_drv, msg->addr);
    }

    data16_fl = lpcssp_drv->data16_fl;
    rq_len = msg->rq_len;

    rxcnt = lpcssp_drv->rxcnt;
    txcnt = lpcssp_drv->txcnt;
    do {
      lpcssp_drv->ssp_regs->ICR = SSP_IRQ_RT_m;

      while(lpcssp_drv->ssp_regs->SR & SSP_SR_RNE_m) {
        dr = lpcssp_drv->ssp_regs->DR;
        if(msg->rx_buf && (rxcnt < rq_len)) {
          msg->rx_buf[rxcnt++] = dr;
          if(data16_fl)
            msg->rx_buf[rxcnt++] = dr >> 8;
        }
      }

      while(1) {
        stop_fl = !(lpcssp_drv->ssp_regs->SR & SSP_SR_TNF_m) || (txcnt >= rq_len);
        if(stop_fl)
          break;
        dr = 0;
        if(msg->tx_buf) {
          dr = msg->tx_buf[txcnt++];
          if(data16_fl)
            dr |= msg->tx_buf[txcnt++] << 8;
        } else {
          txcnt += data16_fl? 2: 1;
        }
        lpcssp_drv->ssp_regs->DR = dr;
        if(lpcssp_drv->ssp_regs->SR & SSP_SR_RNE_m)
          break;
      }
    } while(!stop_fl);
    lpcssp_drv->rxcnt = rxcnt;
    lpcssp_drv->txcnt = txcnt;

    if((rxcnt >= rq_len) ||
       (!msg->rx_buf && (txcnt >= rq_len) &&
        !(lpcssp_drv->ssp_regs->SR & SSP_SR_BSY_m))) {
      if(!(msg->flags & SPI_MSG_MODE_SET) ||
         !(msg->size_mode & SPI_MODE_KEEPCS))
        spi_lpcssp_negate_ss(&lpcssp_drv->spi_drv);
      spi_lpcssp_sfnc_ms_end(&lpcssp_drv->spi_drv, 0);
      continue;
    }
    if(txcnt < rq_len) {
      lpcssp_drv->ssp_regs->IMSC = SSP_IRQ_TX_m;
    } else {
      lpcssp_drv->ssp_regs->IMSC = SSP_IRQ_RX_m | SSP_IRQ_RT_m;
    }
    return IRQ_HANDLED;
  } while(1);
}

static int spi_lpcssp_ctrl_fnc(spi_drv_t *ifc, int ctrl, void *p)
{
  spi_lpcssp_drv_t *lpcssp_drv=UL_CONTAINEROF(ifc,spi_lpcssp_drv_t,spi_drv);

  switch(ctrl){
    case SPI_CTRL_WAKE_RQ:
      if(!(ifc->flags&SPI_IFC_ON))
        return -1;
      if(spi_rq_queue_is_empty(ifc))
        return 0;
     #ifdef WITH_RTEMS
      rtems_event_send(lpcssp_drv->task, SPI_EVENT_WAKE_RQ);
     #else /*WITH_RTEMS*/
      lpcssp_drv->ssp_regs->IMSC = SSP_IRQ_TX_m;
     #endif /*WITH_RTEMS*/
      return 0;
    default:
      return -1;
  }
  return 0;
}

int spi_lpcssp_init(spi_drv_t *ifc)
{
  int i;
  spi_lpcssp_drv_t *lpcssp_drv=UL_CONTAINEROF(ifc,spi_lpcssp_drv_t,spi_drv);
  spi_isr_lock_level_t saveif;

  lpcssp_drv->ssp_regs->IMSC = 0;
  lpcssp_drv->ssp_regs->CR1 = SSP_CR1_SSE_m * 0;

  spi_rq_queue_init_head(ifc);
  ifc->msg_act=NULL;
  ifc->ctrl_fnc=spi_lpcssp_ctrl_fnc;

  if(lpcssp_drv->ssp_regs == SSP0) {
    SC->PCONP |= (21 << 10); /*PCSSP0*/
   #ifdef SCK0_PIN
    hal_pin_conf(SCK0_PIN);
   #endif
   #ifdef SSEL0_PIN
    hal_pin_conf(SSEL0_PIN);
   #endif
   #ifdef MISO0_PIN
    hal_pin_conf(MISO0_PIN);
   #endif
   #ifdef MOSI0_PIN
    hal_pin_conf(MOSI0_PIN);
   #endif
  } else if(lpcssp_drv->ssp_regs == SSP1) {
    SC->PCONP |= (1 << 10); /*PCSSP1*/
   #ifdef SCK1_PIN
    hal_pin_conf(SCK1_PIN);
   #endif
   #ifdef SSEL1_PIN
    hal_pin_conf(SSEL1_PIN);
   #endif
   #ifdef MISO1_PIN
    hal_pin_conf(MISO1_PIN);
   #endif
   #ifdef MOSI1_PIN
    hal_pin_conf(MOSI1_PIN);
   #endif
  }

  request_irq(lpcssp_drv->irqnum, spi_lpcssp_isr, 0, "spi", lpcssp_drv);

  lpcssp_drv->ssp_regs->CR0 = __val2mfld(SSP_CR0_DSS_m, 8 - 1) | __val2mfld(SSP_CR0_FRF_m, 0) |
                              SSP_CR0_CPOL_m * 1 | SSP_CR0_CPHA_m * 1 |
                              __val2mfld(SSP_CR0_SCR_m, 15);
  lpcssp_drv->ssp_regs->CR1 = SSP_CR1_LBM_m * 0 | SSP_CR1_SSE_m * 1 | SSP_CR1_MS_m * 0 |
                              SSP_CR1_SOD_m * 0;

  for(i = 0; i < SPI_LPCSSP_CS_PIN_COUNT; i++) {
    if(lpcssp_drv->cs_gpio_pin[i]!=SSP_PIN_NA)
      hal_pin_conf(lpcssp_drv->cs_gpio_pin[i]);
  }

  lpcssp_drv->ssp_regs->CPSR = 2;

  spi_isr_lock(saveif);
  ifc->flags |= SPI_IFC_ON;
  spi_isr_unlock(saveif);

  return 0;
}

spi_lpcssp_drv_t spi0_lpcssp_drv = {
    .spi_drv = {
      .flags=0,
      .self_addr=0,
      .ctrl_fnc=spi_lpcssp_ctrl_fnc,
    },
    .ssp_regs=SSP0,
    .irqnum=SSP0_IRQn,
    .cs_gpio_pin={
      #ifdef SSP0_CS0_PIN
        SSP0_CS0_PIN,
      #else
        SSP_PIN_NA,
      #endif
      #ifdef SSP0_CS1_PIN
        SSP0_CS1_PIN,
      #else
        SSP_PIN_NA,
      #endif
      #ifdef SSP0_CS2_PIN
        SSP0_CS2_PIN,
      #else
        SSP_PIN_NA,
      #endif
      #ifdef SSP0_CS3_PIN
        SSP0_CS3_PIN,
      #else
        SSP_PIN_NA,
      #endif
    }
  };

spi_lpcssp_drv_t spi1_lpcssp_drv = {
    .spi_drv = {
      .flags=0,
      .self_addr=0,
      .ctrl_fnc=spi_lpcssp_ctrl_fnc,
    },
    .ssp_regs=SSP1,
    .irqnum=SSP1_IRQn,
    .cs_gpio_pin={
      #ifdef SSP1_CS0_PIN
        SSP1_CS0_PIN,
      #else
        SSP_PIN_NA,
      #endif
      #ifdef SSP1_CS1_PIN
        SSP1_CS1_PIN,
      #else
        SSP_PIN_NA,
      #endif
      #ifdef SSP1_CS2_PIN
        SSP1_CS2_PIN,
      #else
        SSP_PIN_NA,
      #endif
      #ifdef SSP1_CS3_PIN
        SSP1_CS3_PIN,
      #else
        SSP_PIN_NA,
      #endif
    }
  };

spi_drv_t *spi_find_drv(char *name, int number)
{
  int ret;
  spi_drv_t *ifc=NULL;
  number&=0xff;
  if(number>1) return NULL;
  if(number==0)
    ifc=&spi0_lpcssp_drv.spi_drv;
  else
    ifc=&spi1_lpcssp_drv.spi_drv;
  if(!(ifc->flags&SPI_IFC_ON)){
    ret=spi_lpcssp_init(ifc);
    if(ret<0) return NULL;
  }
  return ifc;
}
