/*************************************************/
/***   Module : USB device descriptor table    ***/
/***   Author : Roman Bartosinski, Pavel Pisa  ***/
/***   Modify : 10.11.2008                     ***/
/*************************************************/

#ifndef _USB_DEVICE_DESCRIPTOR_TABLE
 #define _USB_DEVICE_DESCRIPTOR_TABLE

  #include "usb_spec.h"

  struct _tag_usb_device_configuration_entry {
    const USB_CONFIGURATION_DESCRIPTOR *pConfigDescription;
    int iConfigTotalLength;
  };
 typedef struct _tag_usb_device_configuration_entry 
    USB_DEVICE_CONFIGURATION_ENTRY, *PUSB_DEVICE_CONFIGURATION_ENTRY;

  struct _tag_usb_device_descriptors_table {
    const USB_DEVICE_DESCRIPTOR *pDeviceDescription;
    const USB_DEVICE_CONFIGURATION_ENTRY *pConfigurations;
    const USB_INTERFACE_DESCRIPTOR **pInterfaceDescriptors;
    const PUSB_STRING_DESCRIPTOR *pStrings;
    int      iNumStrings;
    uint8_t  bNumEndpoints;
    uint8_t  bNumConfigurations;
    uint8_t  bNumInterfaces;
  };
 typedef struct _tag_usb_device_descriptors_table
    USB_DEVICE_DESCRIPTORS_TABLE, *PUSB_DEVICE_DESCRIPTOR_TABLE;


#endif /*_USB_DEVICE_DESCRIPTOR_TABLE*/

