/**************************************************************/
/***   Module : USB module - header file                    ***/
/***   Author : Roman Bartosinski (C) 28.04.2002            ***/
/***   Modify : 08.08.2002                                  ***/
/***   Rewrite: 05.10.2009                                  ***/
/**************************************************************/

#include <system_def.h>

#include <usb/usb.h>
#include <usb/usbdebug.h>

#include <usb/usb_srq.h> /* standard control request responses */

/*************************************************************
 *** Common standard control endpoint0 responses
 *************************************************************/
  int usb_standard_control_response(usb_device_t *udev)  REENTRANT_SIGN
  {
    unsigned char req;
    int ret = -1;
    USB_DEVICE_REQUEST *preq = &(udev->request);

    req = preq->bRequest & USB_REQUEST_MASK;
    usb_debug_print( DEBUG_LEVEL_HIGH, ( "StdReq-%d\n", req));
/*
    if ( (udev->stdreq[ req]) != NULL) {
      ret = udev->stdreq[ req]( udev);
    }
    if( ret < 0)
      udev->ack_setup( udev);
*/
    switch( req) {
      case USB_REQUEST_GET_STATUS:        ret=usb_stdreq_get_status( udev); break;
      case USB_REQUEST_CLEAR_FEATURE:     ret=usb_stdreq_clear_feature( udev); break;
      case USB_REQUEST_SET_FEATURE:       ret=usb_stdreq_set_feature( udev); break;
      case USB_REQUEST_SET_ADDRESS:       ret=usb_stdreq_set_address( udev); break;
      case USB_REQUEST_GET_DESCRIPTOR:    ret=usb_stdreq_get_descriptor( udev); break;
//      case USB_REQUEST_SET_DESCRIPTOR:    break;
      case USB_REQUEST_GET_CONFIGURATION: ret=usb_stdreq_get_configuration( udev); break;
      case USB_REQUEST_SET_CONFIGURATION: ret=usb_stdreq_set_configuration( udev); break;
      case USB_REQUEST_GET_INTERFACE:     ret=usb_stdreq_get_interface( udev); break;
      case USB_REQUEST_SET_INTERFACE:     ret=usb_stdreq_set_interface( udev); break;
//      case USB_REQUEST_SYNC_FRAME:        break;
//      default:                            ret=-1; break;
      }
    return ret;
  }
