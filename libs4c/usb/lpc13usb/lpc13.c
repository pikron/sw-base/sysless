/*****************************************************/
/***   Module : USB LPC13                          ***/
/***   Author : Roman Bartosinski (C) 2015/06      ***/
/***   Modify :                                    ***/
/*****************************************************/

#include <system_def.h>
#include <lpcUSB.h>

#include <usb/usb.h>
#include <usb/usbdebug.h>
#include <usb/lpc13usb.h>
#include <usb/lpc13.h>

/* set device address */
int usb_lpc_set_addr(usb_device_t *udev, unsigned char addr) {
  lpc13_usb_set_addr(addr);
  return 0;
}

int usb_lpc_set_configuration(usb_device_t *udev, unsigned char iCfg) {
  lpc13_usb_config_device(iCfg);
  if ( iCfg) {
    int i;
    for(i = 0; i < udev->cntep; i++) {
      if (udev->ep[i].max_packet_size>64) {
        /* ERROR - LPC134x has fixed maximal packet size 64B for int/bulk EP (and 512B for isochronous EP) */
        usb_debug_print(DEBUG_LEVEL_LOW,("LPC13USB: EP #%d require max.packet size %d > 64\n", i, udev->ep[i].max_packet_size));
      }
      lpc13_usb_enableEP(udev->ep[i].epnum);
    }
  }
  return 0;
}

/* connect usb */
int usb_lpc_connect(usb_device_t *udev) {
  lpc13_usb_connect();
  return 0;
}

/* disconnect usb */
int usb_lpc_disconnect(usb_device_t *udev) {
  lpc13_usb_disconnect();
  return 0;
}

/* acknowledge control transfer */
void usb_lpc_ack_setup(usb_device_t *udev) {
  lpc13_usb_write_endpoint(USB_LPC13_LADDR_EP1, NULL, 0);
}

/* stall endpoint X */
void usb_lpc_stall(usb_ep_t *ep) {
  if (ep->epnum)
    lpc13_usb_setstallEP(ep->epnum);
  else
    lpc13_usb_setstallEP(USB_LPC13_LADDR_EP0);
}

/* unstall endpoint X */
void usb_lpc_unstall(usb_ep_t *ep) {
  if (ep->epnum)
    lpc13_usb_clrstallEP(ep->epnum);
  else
    lpc13_usb_clrstallEP(USB_LPC13_LADDR_EP0);
}

/**
 * usb_lpc_check events
 * function reads interrupt register and sets event flags
 * function returns 1 if there is some new event.
 * function returns 0 if there isn't new event but all is OK
 * function returns -1 if there is any error
*/
int usb_lpc_check_events( usb_device_t *udev)
{
  volatile unsigned int disr, val;
  int ret=0, n, m, i;

  disr=USBDevIntSt;
//if (disr&(~0xc01)) usb_debug_print(DEBUG_LEVEL_VERBOSE,("ULChE: 0x%X\n", disr));

  /* Device Status Interrupt (Reset, Connect change, Suspend/Resume) */
  if (disr & USBDevInt_DEV_STAT) {
    USBDevIntClr = USBDevInt_DEV_STAT;
    disr&=~USBDevInt_DEV_STAT;
    val = lpc13_usb_get_devstat();
    usb_debug_print(DEBUG_LEVEL_VERBOSE,("LU:Chck-DevSt 0x%02X\n", val));
    if (val & USBC_DEV_RST) {               /* Reset */
      udev->flags |= USB_FLAG_BUS_RESET;
      ret = 1;
    }
    if (val & USBC_DEV_SUS_CH) {            /* Suspend/Resume */
      if (val & USBC_DEV_SUS) {             /* Suspend */
        udev->flags |= USB_FLAG_SUSPEND;
        ret = 1;
      } else {                              /* Resume */
        /* todo */
      }
    }
  }

  if (disr & USBDevInt_EP_0) { /* prisel paket od hosta */
    disr &= ~USBDevInt_EP_0;
    USBDevIntClr = USBDevInt_EP_0;
    val = lpc13_usb_selectEP_CLRI(USB_LPC13_LADDR_EP0);
    usb_debug_print(DEBUG_LEVEL_VERBOSE, ("Uch:EP0.0 v=0x%02X\n", val));
//    if (val & USBC_EP_SEL_PO) { // packet over-written
//      val = lpc13_usb_selectEP_CLRI(USB_LPC13_LADDR_EP0);
//      usb_debug_print(DEBUG_LEVEL_LOW, (" -! PO v=0x%02X\n", val));
//    }
    /* Setup Packet */
    if (val & USBC_EP_SEL_STP)
      udev->flags |= USB_FLAG_SETUP;
    else
      udev->flags |= USB_FLAG_EVENT_RX0;
    ret = 1;
  }

  if (disr & USBDevInt_EP_1) {
    disr &= ~USBDevInt_EP_1;
    USBDevIntClr = USBDevInt_EP_1;
    val = lpc13_usb_selectEP_CLRI(USB_LPC13_LADDR_EP1);
    usb_debug_print(DEBUG_LEVEL_VERBOSE, ("EP0.1 v=0x%02X (x%lX)\n", val, USBDevIntSt));
    udev->flags |= USB_FLAG_EVENT_TX0;
    ret = 1;
  }

  /* user endpoints - usb_ep_t -> event_mask should be set to USBDevInt_EP_2 - USBDevInt_EP_7 */
  for( i=0; i<udev->cntep; i++) {
    unsigned int msk = (udev->ep+i)->event_mask;
    if (disr & msk) {
      disr &= ~msk;
      USBDevIntClr = msk;
      val = lpc13_usb_selectEP_CLRI((udev->ep+i)->epnum);
      if (val & USBC_EP_SEL_ST) {
        lpc13_usb_clrstallEP((udev->ep+i)->epnum);
        usb_debug_print(DEBUG_LEVEL_VERBOSE, ("Uch:#%d clear stall\n", i));
      }
      usb_debug_print(DEBUG_LEVEL_VERBOSE, ("Uch:#%d (x%X) v=0x%02X (x%X)\n", i, msk, val, disr));
      udev->ep_events |= 1<<i;
      ret = 1;
    }
  }

  if (disr & (USBDevInt_EP_2 | USBDevInt_EP_3 | USBDevInt_EP_4 | USBDevInt_EP_5 | USBDevInt_EP_6 | USBDevInt_EP_7)) {                    /* Endpoint Interrupt Status */
    for (n = 0; n < 6; n++) {    /* Check All Endpoints */
      unsigned int msk = USBDevInt_EP_2 << n;
      if (disr & msk) {
        usb_debug_print(DEBUG_LEVEL_VERBOSE, ("L13oth#%d\n", n));
        m = ((n&1)? 0x80:0x00) | (1+(n>>1));
        disr &= ~msk;
        USBDevIntClr = msk;
        val = lpc13_usb_selectEP_CLRI(m);
        if (!(val & USBC_EP_SEL_ST)) {
          lpc13_usb_setstallEP(m);
        }
        ret = 1;
      }
    }
  }
  USBDevIntClr = disr;
  return ret;
}

int usb_lpc_read_endpoint( usb_ep_t *ep, void *ptr, int size)
{
  int rc = lpc13_usb_read_endpoint(ep->epnum, ptr, size);
  return rc;
}

int usb_lpc_write_endpoint( usb_ep_t *ep, const void *ptr, int size)
{
  int rc = lpc13_usb_write_endpoint(0x80|ep->epnum, ptr, size);
  return rc;
}


int usb_lpc_epnum2evmask(int epnum)
{
  int evmask;
  int evbit;
  /* LPC134x - EP1RX 0x08, EP1TX 0x10, EP2RX 0x20, EP2TX 0x40, ... */

  evbit = (epnum & ~USB_ENDPOINT_DIRECTION_MASK) * 2;
  if (epnum & USB_ENDPOINT_DIRECTION_MASK)
    evbit++;

  evmask = 1 << (evbit+1); // first bit is FrameIdx
  return evmask;
}

/* init usb structures and chip */
int usb_lpc_init( usb_device_t *udev) {

  udev->connect = usb_lpc_connect;
  udev->disconnect = usb_lpc_disconnect;
  udev->set_addr = usb_lpc_set_addr;
  udev->set_configuration = usb_lpc_set_configuration;
  udev->ack_setup = usb_lpc_ack_setup;
  udev->ack_control_setup = NULL;
  udev->stall = usb_lpc_stall;
  udev->unstall = usb_lpc_unstall;
  udev->check_events = usb_lpc_check_events;
  udev->read_endpoint = usb_lpc_read_endpoint;
  udev->write_endpoint = usb_lpc_write_endpoint;

  udev->ep0.max_packet_size = USB_MAX_PACKET0;

  lpc13_usb_hw_init();
  return 0;
}
