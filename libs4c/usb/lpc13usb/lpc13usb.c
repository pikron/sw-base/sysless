/*****************************************************/
/***   Module : LPC134x USB                        ***/
/***   Author : Roman Bartosinski (C) 01.05.2015   ***/
/*****************************************************/

#include <system_def.h>
#include <usb/usb.h>
#include <usb/lpc13usb.h>

#include <LPC13xx.h>
#include <lpcUSB.h>

#include <usb/usbdebug.h>

// gpio pins are configured in bsp0hwinit (system_def.h)

inline unsigned int lpc_ep2addr(unsigned int ep_num)
{
  return (((ep_num & 0x0F) << 1) | ((ep_num & 0x80) ? 1 : 0));
}

void lpc13_write_cmd(unsigned int cmd)
{
  USBDevIntClr = USBDevInt_CCEMPTY;
  USBCmdCode = cmd & 0x00ffff00;
  while (!(USBDevIntSt & USBDevInt_CCEMPTY)) ;
  USBDevIntClr = USBDevInt_CCEMPTY;
}

void lpc13_write_cmd_data(unsigned int cmd, uint8_t val)
{
  USBDevIntClr = USBDevInt_CCEMPTY;
  USBCmdCode = cmd & 0x00ffff00;
  while (!(USBDevIntSt & USBDevInt_CCEMPTY)) ;
  USBDevIntClr = USBDevInt_CCEMPTY;
  USBCmdCode = USB_DAT_WR_BYTE(val);
  while (!(USBDevIntSt & USBDevInt_CCEMPTY)) ;
  USBDevIntClr = USBDevInt_CCEMPTY;
}

uint16_t lpc13_read_cmd_data(unsigned int cmd)
{
  uint16_t ret;
  int n = cmd>>24;
  USBDevIntClr = USBDevInt_CCEMPTY | USBDevInt_CDFULL;
  USBCmdCode = cmd & 0x00ffff00;
  while (!(USBDevIntSt & USBDevInt_CCEMPTY)) ;
  USBDevIntClr = USBDevInt_CCEMPTY;
  cmd = (cmd & 0x00ff0000) | 0x0200; /* read phase */
  USBCmdCode = cmd;
  while (!(USBDevIntSt & USBDevInt_CDFULL)) ;
  ret = USBCmdData & 0xff;
  USBDevIntClr = USBDevInt_CDFULL;
  if (n>1) {
    USBCmdCode = cmd;
    while (!(USBDevIntSt & USBDevInt_CDFULL)) ;
    ret |= (USBCmdData & 0xff)<<8;
    USBDevIntClr = USBDevInt_CDFULL;
  }
  return ret;
}

/* -------------------------------------------------------------------------- */
inline void lpc13_usb_connect(void)
{
  lpc13_write_cmd_data(USB_CMD_SET_DEV_STAT, USBC_DEV_CON);
}

inline void lpc13_usb_disconnect(void)
{
  lpc13_write_cmd_data(USB_CMD_SET_DEV_STAT, 0);
}

inline void lpc13_usb_setstallEP(unsigned int ep_num)
{
  lpc13_write_cmd_data(USB_CMD_SET_EP_STAT(lpc_ep2addr(ep_num)), USBC_EP_STAT_ST);
}

inline void lpc13_usb_clrstallEP (unsigned int ep_num)
{
  lpc13_write_cmd_data(USB_CMD_SET_EP_STAT(lpc_ep2addr(ep_num)), 0);
}

inline void lpc13_usb_enableEP(unsigned int ep_num)
{
  lpc13_write_cmd_data(USB_CMD_SET_EP_STAT(lpc_ep2addr(ep_num)), 0);
//  usb_debug_print(DEBUG_LEVEL_LOW,("LU: enable EP x%X\n", ep_num));
}

inline void lpc13_usb_disableEP(unsigned int ep_num)
{
  lpc13_write_cmd_data(USB_CMD_SET_EP_STAT(lpc_ep2addr(ep_num)),USBC_EP_STAT_DA);
}

inline void lpc13_usb_set_addr(unsigned int adr)
{
  lpc13_write_cmd_data(USB_CMD_SET_ADDR, USBC_DEV_EN | adr); /*  Setup Status Phase */
//  usb_debug_print(DEBUG_LEVEL_VERBOSE,("l13u:set address 0x%02X\n", adr));
}

inline void lpc13_usb_config_device(unsigned int fConfigured)
{
  lpc13_write_cmd_data(USB_CMD_CFG_DEV, fConfigured ? USBC_CONF_DEVICE : 0); /*  Setup Status Phase */
//  usb_debug_print(DEBUG_LEVEL_VERBOSE,("l13u: device is %sconfigured\n", (fConfigured) ? "" : "UN"));
}

inline unsigned int lpc13_usb_get_devstat(void)
{
  return lpc13_read_cmd_data(USB_CMD_GET_DEV_STAT);
}


inline void lpc13_usb_reset(void)
{
  USBDevIntClr = 0xFFFFFFFF;
  USBDevIntEn  = 0; // we don't require generation of interrupts, flags in USBDevIntSt are normally set
  lpc13_write_cmd_data(USB_CMD_SET_DEV_STAT, USBC_DEV_RST);
//  usb_debug_print(DEBUG_LEVEL_VERBOSE,("l13u:reset device\n"));
}

inline unsigned int lpc13_usb_selectEP(unsigned int ep_num)
{
  uint16_t ret = lpc13_read_cmd_data(USB_CMD_SEL_EP(lpc_ep2addr(ep_num)));
  usb_debug_print(DEBUG_LEVEL_VERBOSE,("l13u:selEP 0x%02X 0x%04X\n", ep_num, ret));
  return ret;
}

inline unsigned int lpc13_usb_selectEP_CLRI(unsigned int ep_num)
{
  uint16_t ret = lpc13_read_cmd_data(USB_CMD_SEL_EP_CLRI(lpc_ep2addr(ep_num)));
//  usb_debug_print(DEBUG_LEVEL_VERBOSE,("l13u:selEPclri 0x%02X 0x%04X\n", ep_num, ret));
  return ret;
}

void lpc13_usb_hw_init(void)
{
  /* Partial Manual Reset since Automatic Bus Reset is not working */
  lpc13_write_cmd_data(USB_CMD_SET_MODE, 0);
  lpc13_usb_reset();
//  lpc13_usb_set_addr(0);
}

/*
 * lpc_usb_read_endpoint: Read USB Endpoint Data
 * @EPNum: logical endpoint address - EPNum.0..4: Address, EPNum.7: Dir
 * @ptr: Pointer to Data Buffer
 * @size:
 * Return Value: Number of bytes read
 */
int lpc13_usb_read_endpoint(unsigned int ep_num, void *ptr, int size)
{
  volatile unsigned int cnt,i,dwData;
  unsigned char *p=ptr;

  USBCtrl = ((ep_num & 0x0F) << 2) | USBCtrl_RD_EN;
  do {
    __asm("NOP"); // remark in section 10.10.3.5.1 UM10375
    __asm("NOP"); // remark in section 10.10.3.5.1 UM10375
    __asm("NOP"); // remark in section 10.10.3.5.1 UM10375
    cnt = USBRxPLen;
//usb_debug_print(DEBUG_LEVEL_VERBOSE,("l13u:rdEP 0x%02X sz=%d - l=0x%X\n", ep_num, size, cnt));
  } while (!(cnt & USBRxPLen_DV));
  cnt &= USBRxPLen_PKT_LNGTH;

  // get data
  while (USBCtrl & USBCtrl_RD_EN) {
    dwData = USBRxData;
    if (p != NULL) {
      for (i = 0; (i < 4) && size; i++) {
        size--;
        *p = dwData & 0xFF;
        p++;
        dwData >>= 8;
      }
    }
  }

  i = lpc13_read_cmd_data(USB_CMD_SEL_EP(lpc_ep2addr(ep_num)));
  i = lpc13_read_cmd_data(USB_CMD_CLR_BUF);

  if (i & 0x01) { // Packet-overwritten
    usb_debug_print(DEBUG_LEVEL_VERBOSE,("l13u:rd PO\n"));
  }

  USBDevIntClr = USBDevInt_RxENDPKT;

  return cnt;
}

/*
 * lpc_usb_write_endpoint: Write USB Endpoint Data
 * @ep_num: logical endpoint address - ep_num.0..4: Address, ep_num.7: Dir
 * @ptr: Pointer to Data Buffer
 * @size:   Number of bytes to write
 * Return Value:    Number of bytes written
 */
int lpc13_usb_write_endpoint(unsigned int ep_num, const void *ptr, int size)
{
  unsigned int n;
  const unsigned char *p=ptr;

//  n = lpc13_read_cmd_data(USB_CMD_SEL_EP(lpc_ep2addr(ep_num)));
//  if (n & USBC_EP_SEL_F) return 0; // jeste nebylo odeslano vse do USB

//usb_debug_print(DEBUG_LEVEL_VERBOSE,("l13u:wrEP 0x%02X sz=%d\n", ep_num, size));

  USBCtrl = ((ep_num & 0x0F) << 2) | USBCtrl_WR_EN;
  USBTxPLen = size;

  if (size) {
    n = size/4;
    while(n--) {
      USBTxData = (p[3] << 24) | (p[2] << 16) | (p[1] << 8) | p[0];
      p += 4;
    }
    n = size & 0x03;
    if (n) {
      uint32_t d = p[0];
      if (n>1) d |= p[1] << 8;
      if (n>2) d |= p[2] << 16;
      USBTxData = d;
    }
  } else {
    USBTxData = 0; // in empty packet, a single write is required
  }

  while (!(USBDevIntSt & USBDevInt_TxENDPKT)) ;

  lpc13_usb_selectEP(ep_num);
  lpc13_write_cmd(USB_CMD_VALID_BUF);

  USBDevIntClr = USBDevInt_TxENDPKT;
  return size;
}
