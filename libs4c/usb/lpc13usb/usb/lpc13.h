#ifndef _USB_LPC13_SUBMODULE_HEADER_FILE_
#define _USB_LPC13_SUBMODULE_HEADER_FILE_

#include <usb/usb.h>

/* control endpoints max.64B */
#define USB_LPC13_LADDR_EP0     (0x00)
#define USB_LPC13_LADDR_EP1     (0x80)
/* bulk/interrupt endpoints max.64B */
#define USB_LPC13_LADDR_EP2     (0x01)
#define USB_LPC13_LADDR_EP3     (0x81)
#define USB_LPC13_LADDR_EP4     (0x02)
#define USB_LPC13_LADDR_EP5     (0x82)
#define USB_LPC13_LADDR_EP6     (0x03)
#define USB_LPC13_LADDR_EP7     (0x83)
/* isochronous enpoints max.512B */
#define USB_LPC13_LADDR_EP8     (0x04)
#define USB_LPC13_LADDR_EP9     (0x84)


int usb_lpc_init(usb_device_t *udev);
int usb_lpc_epnum2evmask(int epnum);

#endif /* _USB_LPC13_SUBMODULE_HEADER_FILE_ */

