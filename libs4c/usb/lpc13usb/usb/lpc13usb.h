#ifndef _LPC13USB_MODULE
#define _LPC13USB_MODULE

/*********************************************************/
// Function prototypes
//
// LPC13USB common commands

inline void lpc13_usb_set_addr(unsigned int adr);
inline void lpc13_usb_config_device(unsigned int fConfigured);
inline void lpc13_usb_connect(void);
inline void lpc13_usb_disconnect(void);
inline void lpc13_usb_setstallEP(unsigned int ep_num);
inline void lpc13_usb_clrstallEP (unsigned int ep_num);
inline void lpc13_usb_enableEP(unsigned int ep_num);
inline void lpc13_usb_disableEP(unsigned int ep_num);
inline unsigned int lpc13_usb_get_devstat(void);
inline void lpc13_usb_reset(void);
inline unsigned int lpc13_usb_selectEP(unsigned int ep_num);
inline unsigned int lpc13_usb_selectEP_CLRI(unsigned int ep_num);

void lpc13_usb_hw_init(void) ;
int lpc13_usb_read_endpoint(unsigned int ep_num, void *ptr, int size);
int lpc13_usb_write_endpoint(unsigned int ep_num, const void *ptr, int size);

#endif /* from _LPC13USB_MODULE */
