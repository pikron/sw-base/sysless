/*****************************************************/
/***   Module : USB PDI                            ***/
/***   Author : Roman Bartosinski (C) 28.04.2002   ***/
/***   Modify : 08.08.2002, 16.04.2003             ***/
/*****************************************************/

#include <system_def.h>
#include <usb/usb.h>
#include <usb/lpcusb.h>

#ifdef MACH_LPC17XX
#include <lpcUSB.h>
#endif

/* set device address */
int usb_lpc_set_addr( usb_device_t *udev, unsigned char addr) {
  lpc_usb_set_addr(addr);
  return 0;
}

int usb_lpc_set_configuration( usb_device_t *udev, unsigned char iCfg) {
  lpc_usb_config_device(iCfg);
  if ( iCfg) {
    int i;
    for(i = 0; i < udev->cntep; i++) {
      lpc_usb_configEP(udev->ep[i].epnum, udev->ep[i].max_packet_size);
    }
  }
  return 0;
}

/* connect usb */
int usb_lpc_connect( usb_device_t *udev) {
  lpc_write_cmd_data(USB_CMD_SET_DEV_STAT,USB_DAT_WR_BYTE(USBC_DEV_CON));
  return 0;
}

/* disconnect usb */
int usb_lpc_disconnect( usb_device_t *udev) {
  lpc_write_cmd_data(USB_CMD_SET_DEV_STAT,USB_DAT_WR_BYTE(0));
  return 0;
}

/* acknowledge control transfer */
void usb_lpc_ack_setup( usb_device_t *udev) {
  lpc_usb_write_endpoint(0x80|0x00, NULL, 0);
}

/* stall endpoint X */
void usb_lpc_stall( usb_ep_t *ep) {
  if (!ep->epnum)
    lpc_usb_setstallEP(0x80|ep->epnum);
  lpc_usb_setstallEP(ep->epnum);
}

/* unstall endpoint X */
void usb_lpc_unstall( usb_ep_t *ep) {
  if (!ep->epnum)
    lpc_usb_clrstallEP(0x80|ep->epnum);
  lpc_usb_clrstallEP(ep->epnum);
}

/**
 * usb_lpc_check events
 * function reads interrupt register and sets event flags
 * function returns 1 if there is some new event.
 * function returns 0 if there isn't new event but all is OK
 * function returns -1 if there is any error
*/
int usb_lpc_check_events( usb_device_t *udev) 
{
  unsigned int disr,val,last_int;
  int ret=0,n,m,i;

  disr=USBDevIntSt;

  /* Device Status Interrupt (Reset, Connect change, Suspend/Resume) */
  if (disr & USBDevInt_DEV_STAT) {
    USBDevIntClr = USBDevInt_DEV_STAT;
    disr&=~USBDevInt_DEV_STAT;
    lpc_write_cmd(USB_CMD_GET_DEV_STAT);
    val=lpc_read_cmd_data(USB_DAT_GET_DEV_STAT);
    if (val & USBC_DEV_RST) {               /* Reset */
      lpc_usb_reset();
      udev->flags |= USB_FLAG_BUS_RESET;
      ret = 1;
    }
    if (val & USBC_DEV_SUS_CH) {            /* Suspend/Resume */
      if (val & USBC_DEV_SUS) {             /* Suspend */
        udev->flags |= USB_FLAG_SUSPEND;
        ret = 1;
      } else {                              /* Resume */
        /* todo */
      }
    }
  }

  /* Endpoint's Slow Interrupt */
  if (disr & USBDevInt_EP_SLOW) {
    USBDevIntClr = USBDevInt_EP_SLOW;
    disr&=~USBDevInt_EP_SLOW;

    last_int = USBEpIntSt;

    /* EP0_OUT */
    if (last_int & (1 << 0)) {
      last_int &= ~(1 << 0);
      USBEpIntClr = 1 << 0;
      lpc_wait4devint(USBDevInt_CDFULL);
      val = USBCmdData;
      /* Setup Packet */
      if (val & USBC_EP_SEL_STP)   
        udev->flags |= USB_FLAG_SETUP;
      else
        udev->flags |= USB_FLAG_EVENT_RX0;
      ret = 1;
    }

    /* EP0_IN */
    if (last_int & (1 << 1)) {
      last_int &= ~(1 << 1);
      USBEpIntClr = 1 << 1;
      lpc_wait4devint(USBDevInt_CDFULL);
      val = USBCmdData;
      udev->flags |= USB_FLAG_EVENT_TX0;
      ret = 1;
    }    

    /* user endpoints */
    for( i=0; i<udev->cntep; i++) {
      if ( last_int & (udev->ep+i)->event_mask) {
        last_int &= ~((udev->ep+i)->event_mask);
        USBEpIntClr = (udev->ep+i)->event_mask;
        lpc_wait4devint(USBDevInt_CDFULL);
        val = USBCmdData;
        udev->ep_events |= 1<<i;
        ret = 1;
      }
    }

    while (last_int) {                    /* Endpoint Interrupt Status */
      for (n = 0; n < USB_EP_NUM; n++) {    /* Check All Endpoints */
        if (last_int & (1 << n)) {
          last_int &= ~(1 << n);
          USBEpIntClr = 1 << n;
          m = n >> 1;
          if (n&1) m|=0x80;
          lpc_wait4devint(USBDevInt_CDFULL);
          val = USBCmdData;
          lpc_usb_setstallEP(m);
        }
      }
    }
  }
  if (!disr)
    USBDevIntClr = disr;
  return ret;
}

int usb_lpc_read_endpoint( usb_ep_t *ep, void *ptr, int size)
{
  return lpc_usb_read_endpoint(ep->epnum, ptr, size);
}

int usb_lpc_write_endpoint( usb_ep_t *ep, const void *ptr, int size)
{
  return lpc_usb_write_endpoint(0x80|ep->epnum, ptr, size);
}

int usb_lpc_epnum2evmask(int epnum)
{
  int evmask;
  int evbit;
  /* LPC17xx EP1RX 0x04, EP1TX 0x08, EP2RX 0x10, EP2TX 0x20 */

  evbit = (epnum & ~USB_ENDPOINT_DIRECTION_MASK) * 2;
  if (epnum & USB_ENDPOINT_DIRECTION_MASK)
    evbit++;

  evmask = 1 << evbit;
  return evmask;
}

/* init usb structures and chip */
int usb_lpc_init( usb_device_t *udev) {

  udev->connect = usb_lpc_connect;
  udev->set_addr = usb_lpc_set_addr;
  udev->set_configuration = usb_lpc_set_configuration;
  udev->disconnect = usb_lpc_disconnect;
  udev->ack_setup = usb_lpc_ack_setup; 
  udev->ack_control_setup = NULL;
  udev->stall = usb_lpc_stall;
  udev->unstall = usb_lpc_unstall;
  udev->check_events = usb_lpc_check_events;
  udev->read_endpoint = usb_lpc_read_endpoint;
  udev->write_endpoint = usb_lpc_write_endpoint;

  udev->ep0.max_packet_size = USB_MAX_PACKET0;

  lpc_usb_hw_init();  
  return 0;
}
