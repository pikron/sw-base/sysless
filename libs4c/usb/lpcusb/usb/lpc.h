#ifndef _USB_LPC_SUBMODULE_HEADER_FILE_
#define _USB_LPC_SUBMODULE_HEADER_FILE_

#include <usb/usb.h>
 
int usb_lpc_init( usb_device_t *udev);
int usb_lpc_epnum2evmask(int epnum);

#endif /* _USB_LPC_SUBMODULE_HEADER_FILE_ */

