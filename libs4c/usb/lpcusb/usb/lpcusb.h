#ifndef _LPCUSB_BASE_MODULE
#define _LPCUSB_BASE_MODULE

/*********************************************************/
// Function prototypes
//
// LPCUSB common commands
 
unsigned int lpc_ep2addr(unsigned int ep_num);
void lpc_wait4devint(unsigned int intrs);
void lpc_write_cmd(unsigned int cmd);
void lpc_write_cmd_data (unsigned int cmd, unsigned int val);
unsigned int lpc_read_cmd_data (unsigned int cmd);
void lpc_usb_realizeEP(unsigned int idx,unsigned int wmaxpsize);
void lpc_usb_configEP(unsigned int ep_num,unsigned int wmaxpsize);
void lpc_usb_setstallEP (unsigned int ep_num);
void lpc_usb_clrstallEP (unsigned int ep_num);
void lpc_usb_enableEP(unsigned int ep_num); 
void lpc_usb_disableEP(unsigned int ep_num);
void lpc_usb_config_device(int fConfigured);
void lpc_usb_reset(void);
void lpc_usb_set_addr(unsigned int adr);
void lpc_usb_hw_init (void);
int lpc_usb_read_endpoint( unsigned int ep_num, void *ptr, int size);
int lpc_usb_write_endpoint( unsigned int ep_num, const void *ptr, int size);

#endif // from _LPCUSB_BASE_MODULE
