#ifndef _MSC_LOADER_H
#define _MSC_LOADER_H

#include <usb/usb_loader.h>

int usb_msc1210_loader(usb_device_t *udev);

#endif /*_MSC_LOADER_H*/
