
#ifndef _USB_COM_H
  #define _USB_COM_H
  
/* Queued USB Module */
  extern int usb_tm_snd;
  extern int usb_tm_snded;
  extern int usb_tm_rcv;
  
  int usb_com_init( void);
  void usb_com_start_send( void);
  
  int usb_com_sendch(int c);
  int usb_com_recch();
  int usb_com_sendstr(const char *s);

#endif /* _USB_COM_H */
