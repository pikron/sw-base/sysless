#include <usb/usb.h>

#ifndef _USB_LOADER_H
#define _USB_LOADER_H

#define USB_VENDOR_GET_CAPABILITIES  0x00 // get capabilities
#define USB_VENDOR_RESET_DEVICE      0x08
// #define USB_VENDOR_SET_BYTE          0x10
// #define USB_VENDOR_SET_WORD          0x20
#define USB_VENDOR_GET_SET_MEMORY    0x30
#define USB_VENDOR_ERASE_MEMORY      0x40 // erase memory for 1 Byte
#define USB_VENDOR_ERASE_1KB_MEMORY  0x48 // erase memory for 1 KB
#define USB_VENDOR_MASS_ERASE        0x50 // erase all device memory
#define USB_VENDOR_GOTO              0x60
#define USB_VENDOR_CALL              0x70
#define USB_VENDOR_GET_STATUS        0xF0
#define USB_VENDOR_MASK              0xF8 // mask for vendor commands

#define USB_VENDOR_MEMORY_BY_BULK    0x80

/* COMMON */
#define USB_VENDOR_TARGET_RAM        0x01
#define USB_VENDOR_TARGET_FLASH      0x02
/* MSP430 */
#define USB_VENDOR_TARGET_ADAPTER    0x01
#define USB_VENDOR_TARGET_MSP430     0x02
/* MSC1210 */
#define USB_VENDOR_TARGET_DATA       0x01
#define USB_VENDOR_TARGET_XDATA      0x02

#define USB_VENDOR_TARGET_MASK       0x07

/* bRequest - type of request */
/* wValue - lower address word */
/* wIndex - higher address word */
/* wLength - data or length of data */

#endif /*_USB_LOADER_H*/
