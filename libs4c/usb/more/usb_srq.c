/*****************************************************/
/***   Module : USB module                         ***/
/***   Author : Roman Bartosinski (C) 28.04.2002   ***/
/***   Modify : 08.08.2002, 16.04.2003             ***/
/*****************************************************/

#include <string.h>
#include <cpu_def.h>
#include <system_def.h>
#include <usb/usb.h>
#include <usb/usb_spec.h>
#include <usb/usbdebug.h>
#include <usb/usb_srq.h>

  // ****************************
  int usb_stdreq_get_status( usb_device_t *udev)
  {
    unsigned char c, buf[2] = { 0, 0};
    unsigned char epid = (unsigned char) udev->request.wIndex;
    usb_ep_t *ep0 = &(udev->ep0);

    usb_debug_print( DEBUG_LEVEL_HIGH, ("GetStatus\n"));
    switch( udev->request.bmRequestType & USB_RECIPIENT) {
      case USB_RECIPIENT_DEVICE:
        if ( udev->flags & USB_FLAG_REMOTE_WAKE) //.remote_wake_up == 1)
          buf[0] = USB_GETSTATUS_REMOTE_WAKEUP_ENABLED | USB_GETSTATUS_SELF_POWERED;
        else
          buf[0] = USB_GETSTATUS_SELF_POWERED;
        break;
      case USB_RECIPIENT_INTERFACE:
        break;
      case USB_RECIPIENT_ENDPOINT:
       #if 0
        if ( epid & USB_ENDPOINT_DIRECTION_MASK)
          c = pdiSelectEp(pdiEp2Idx(epid)); // endpoint in
        else
          c = pdiSelectEp(pdiEp2Idx(epid));     // endpoint Out
          #ifdef PDIUSBD12
          buf[0] = (( c & PDI_SELEP_STALL) == PDI_SELEP_STALL);
          #else
          buf[0] = 0;
          #endif
       #warning usb_stdreq_get_status is DUMMY
       #else
        (void) c;
        (void) epid;
        #pragma message "usb_stdreq_get_status is DUMMY"
       #endif
        break;
      default:
        return USB_COMPLETE_FAIL;
    }
    usb_udev_write_endpoint( ep0, buf, 2);
    return USB_COMPLETE_OK;
  }

  int usb_stdreq_clear_feature( usb_device_t *udev)
  {
    usb_ep_t *ep = NULL;
    USB_DEVICE_REQUEST *dreq = &(udev->request);
    unsigned char epid = (unsigned char) dreq->wIndex;

    usb_debug_print( DEBUG_LEVEL_HIGH, ("ClearFeature\n"));
    switch( dreq->bmRequestType & USB_RECIPIENT) {
      case USB_RECIPIENT_DEVICE:
        if ( dreq->wValue == USB_FEATURE_REMOTE_WAKEUP) {
          udev->flags &= ~USB_FLAG_REMOTE_WAKE; //.remote_wake_up = 0;
          usb_udev_ack_setup( udev);
          return USB_COMPLETE_OK;
        }
        break;
      case USB_RECIPIENT_ENDPOINT:
        if(!epid) {
          ep = &udev->ep0;
        } else {
          unsigned char i;
          for(i = 0; i < udev->cntep; i++) {
            if(udev->ep[i].epnum == epid) {
              ep = &udev->ep[i];
            }
          }
          if(!ep)
            break;
        }
        if ( dreq->wValue == USB_FEATURE_ENDPOINT_STALL) {
          usb_udev_unstall(ep);
          usb_udev_ack_setup( udev);
          return USB_COMPLETE_OK;
        }
        break;
    }
    return USB_COMPLETE_FAIL;
  }

  int usb_stdreq_set_feature( usb_device_t *udev)
  {
    usb_ep_t *ep = NULL;
    USB_DEVICE_REQUEST *dreq = &(udev->request);
    unsigned char epid = (unsigned char) dreq->wIndex;

    usb_debug_print( DEBUG_LEVEL_HIGH, ("SetFeature\n"));
    switch( dreq->bmRequestType & USB_RECIPIENT) {
      case USB_RECIPIENT_DEVICE:
        if ( dreq->wValue == USB_FEATURE_REMOTE_WAKEUP) {
          udev->flags |= USB_FLAG_REMOTE_WAKE; //.remote_wake_up = 1;
          usb_udev_ack_setup( udev);
          return USB_COMPLETE_OK;
        }
        break;
      case USB_RECIPIENT_ENDPOINT:
        if(!epid) {
          ep = &udev->ep0;
        } else {
          unsigned char i;
          for(i = 0; i < udev->cntep; i++) {
            if(udev->ep[i].epnum == epid) {
              ep = &udev->ep[i];
            }
          }
          if(!ep)
            break;
        }
        if ( dreq->wValue == USB_FEATURE_ENDPOINT_STALL) {
          usb_udev_stall(ep);
          usb_udev_ack_setup( udev);
          return USB_COMPLETE_OK;
        }
        break;
    }
    return USB_COMPLETE_FAIL;
  }

  int usb_stdreq_set_address( usb_device_t *udev)
  {
    int adr;
    USB_DEVICE_REQUEST *dreq = &(udev->request);
    adr=dreq->wValue & DEVICE_ADDRESS_MASK;
    usb_debug_print( DEBUG_LEVEL_HIGH, ("SetAddr-%d\n",adr));
    usb_udev_set_addr( udev, adr);
    usb_udev_ack_setup( udev);
    return USB_COMPLETE_OK;
  }

  int usb_stdreq_get_configuration( usb_device_t *udev)
  {
    unsigned char buf = udev->configuration; //usb_flags.configured;
    usb_ep_t *ep0 = &(udev->ep0);
    usb_debug_print( DEBUG_LEVEL_HIGH, ("GetConfig\n"));
    usb_udev_write_endpoint( ep0, &buf, 1);
    return USB_COMPLETE_OK;
  }

  int usb_stdreq_set_configuration( usb_device_t *udev)
  {
    USB_DEVICE_REQUEST *dreq = &(udev->request);
    unsigned char iCfg = dreq->wValue & 0xff;
    usb_debug_print( DEBUG_LEVEL_HIGH, ("SetConfig\n"));
    while (1) {       // put device in unconfigured state or set configuration 1 ( no else)
      const USB_DEVICE_CONFIGURATION_ENTRY *pcd = udev->devdes_table->pConfigurations;
      if(iCfg) {
        unsigned char i = udev->devdes_table->bNumConfigurations;
        for(; i && (pcd->pConfigDescription->bConfigurationValue != iCfg); i--, pcd++);
        if(!i)
          break;
      }

      udev->flags &= ~USB_FLAG_CONFIGURED;

      usb_udev_ack_setup( udev);

      usb_udev_set_configuration( udev, iCfg);

      udev->configuration = iCfg;  //usb_flags.configured = iCfg;

      if(iCfg)
        udev->flags |= USB_FLAG_CONFIGURED;

      return USB_COMPLETE_OK;
    }
    return USB_COMPLETE_FAIL;
  }

  int usb_stdreq_get_interface( usb_device_t *udev)
  {
    unsigned char buf = 0; /// udev->interface
    usb_ep_t *ep0 = &(udev->ep0);
    usb_debug_print( DEBUG_LEVEL_HIGH, ("GetIface\n"));
    usb_udev_write_endpoint( ep0, &buf, 1);
    return USB_COMPLETE_OK;
  }

  int usb_stdreq_set_interface( usb_device_t *udev)
  {
    USB_DEVICE_REQUEST *dreq = &(udev->request);
    
    usb_debug_print( DEBUG_LEVEL_HIGH, ("SetIface\n"));
    if (( dreq->wValue == 0) && ( dreq->wIndex == 0)) {
      usb_udev_ack_setup( udev);
      return USB_COMPLETE_OK;
    } else {
      return USB_COMPLETE_FAIL;
    }
  }

  int usb_stdreq_get_descriptor( usb_device_t *udev)
  {
    unsigned char confidx;
    unsigned char *pDesc;
    unsigned short Len = 0;
    USB_DEVICE_REQUEST *dreq = &(udev->request);
    const USB_DEVICE_CONFIGURATION_ENTRY *pconfent;
    int i;

    i = (dreq->wValue >> 8) & 0xff; /* MSB part of wValue */
    usb_debug_print( DEBUG_LEVEL_HIGH, ("GetDesc\n"));
    usb_debug_print( DEBUG_LEVEL_VERBOSE, ( " - %s desc.\n", /*(unsigned int)*/ usb_debug_get_std_descriptor(i)));

    switch (i) {
      case USB_DESCRIPTOR_TYPE_DEVICE:
        pDesc = (unsigned char *)udev->devdes_table->pDeviceDescription;
        Len = sizeof( USB_DEVICE_DESCRIPTOR);
        break;
      case USB_DESCRIPTOR_TYPE_DEVICE_QUALIFIER: // not supported
        //usb_udev_write_endpoint(&udev->ep0, NULL, 0); /* NAK */
        return USB_COMPLETE_FAIL;
      case USB_DESCRIPTOR_TYPE_CONFIGURATION:
        /* FIXME confidx = udev->configuration; */
        confidx = 0;
        pconfent = &udev->devdes_table->pConfigurations[confidx];
        pDesc = (unsigned char *)pconfent->pConfigDescription;
        Len = pconfent->iConfigTotalLength;
        break;
      case USB_DESCRIPTOR_TYPE_INTERFACE:
        /* FIXME multiple interfaces */
        pDesc = (unsigned char *)udev->devdes_table->pInterfaceDescriptors[0];
        Len = sizeof( USB_INTERFACE_DESCRIPTOR);
        break;
      case USB_DESCRIPTOR_TYPE_STRING:
        i = dreq->wValue & 0xff; /* LSB part of wValue */
        /*printf("Get descriptor indx=0x%02x\n", i);*/
        if ( i < udev->devdes_table->iNumStrings) {
          pDesc = (unsigned char *) udev->devdes_table->pStrings[i];
          Len = *pDesc;
          /*usb_debug_print(0,("indx=0x%02x ptr=%p len=%d : '%c'\n", i, pDesc, Len, pDesc[2]));*/
        } else {
          return USB_COMPLETE_FAIL;
        }
        break;
      default:
        return USB_COMPLETE_FAIL;
    }
    if ( dreq->wLength < Len) Len = dreq->wLength;
    usb_send_control_data( udev, pDesc, Len);
    return USB_COMPLETE_OK;
  }


/*  
  void usb_init_stdreq_fnc( usb_device_t *udev)
  {
    // memset( udev->stdreq, 0, sizeof(udev->stdreq));
    
    udev->stdreq[USB_REQUEST_GET_STATUS] = usb_stdreq_get_status;
    udev->stdreq[USB_REQUEST_CLEAR_FEATURE] = usb_stdreq_clear_feature;
    udev->stdreq[USB_REQUEST_SET_FEATURE] = usb_stdreq_set_feature;
    udev->stdreq[USB_REQUEST_SET_ADDRESS] = usb_stdreq_set_address;
    udev->stdreq[USB_REQUEST_GET_DESCRIPTOR] = usb_stdreq_get_descriptor;
    udev->stdreq[USB_REQUEST_GET_CONFIGURATION] = usb_stdreq_get_configuration;
    udev->stdreq[USB_REQUEST_SET_CONFIGURATION] = usb_stdreq_set_configuration;
    udev->stdreq[USB_REQUEST_GET_INTERFACE] = usb_stdreq_get_interface;
    udev->stdreq[USB_REQUEST_SET_INTERFACE] = usb_stdreq_set_interface;
  }
*/
