/*****************************************************/
/***   Module : USB nRF52                          ***/
/***   Author : Roman Bartosinski (C) 10/2018      ***/
/***   Modify :                                    ***/
/*****************************************************/

#include <system_def.h>

#include <usb/usb.h>
#include <usb/usbdebug.h>
#include <usb/nrf52usb.h>

#define USB_EXTFLAG_SETADDR         0x1000
#define USB_EXTFLAG_READ_SETUP      0x2000
#define USB_EXTFLAG_GOTO_EP0STATUS  0x4000
#define USB_EXTFLAG_GOTO_EP0RECEIVE 0x8000

/* ========================================================================== */
/* nRF52 USB - hardware specific functions */
void usb_nrf52_hw_init(void)
{
  NRF_USBD->EVENTCAUSE =  USBD_EVENTCAUSE_READY_Msk | USBD_EVENTCAUSE_USBWUALLOWED_Msk |
                          USBD_EVENTCAUSE_RESUME_Msk | USBD_EVENTCAUSE_SUSPEND_Msk |
                          USBD_EVENTCAUSE_ISOOUTCRC_Msk;

//  NRF_USBD->INTENSET =  USBD_INTENSET_EP0SETUP_Msk | USBD_INTENSET_EP0DATADONE_Msk
//                        USBD_INTENSET_ENDEPIN0_Msk | USBD_INTENSET_ENDEPOUT0_Msk |
//                        USBD_INTENSET_STARTED_Msk | USBD_INTENSET_USBRESET_Msk |
//                        USBD_INTENSET_USBEVENT_Msk;
  // events from specific EP will be enabled/disabled with enabling/disabled the EP
  NRF_USBD->INTENCLR = 0xffffffff; /* disable all interrupts */
}


/* ========================================================================== */
/* usb stack functions */
/* connect usb - before using, function usb_check_power must be run */
int usb_nrf52_connect(usb_device_t *udev) {
  /* if USB is not powered */
  if (!(udev->flags & USB_FLAG_POWERED)) return -1;

  // enable USBD if not already enabled - errata
  if (!(NRF_USBD->ENABLE & USBD_ENABLE_ENABLE_Msk)) {
    // clear READY event flag
    NRF_USBD->EVENTCAUSE |= USBD_EVENTCAUSE_READY_Msk;
    // errata 187
    *(volatile uint32_t *)0x4006EC00 = 0x00009375;
    *(volatile uint32_t *)0x4006ED14 = 0x00000003;
    *(volatile uint32_t *)0x4006EC00 = 0x00009375;
    /* Enable the peripheral */
    NRF_USBD->ENABLE = USBD_ENABLE_ENABLE_Msk; // or NRF_USBD->LOWPOWER = 0x00000000;
    // cekani na READY (TODO: presunout do preruseni)
    while (1) {
      uint32_t ec = NRF_USBD->EVENTCAUSE;
      if (ec & USBD_EVENTCAUSE_READY_Msk) break;
    }
    // errata 187
    NRF_USBD->EVENTCAUSE &= ~USBD_EVENTCAUSE_READY_Msk;
    *(volatile uint32_t *)0x4006EC00 = 0x00009375;
    *(volatile uint32_t *)0x4006ED14 = 0x00000000;
    *(volatile uint32_t *)0x4006EC00 = 0x00009375;
  }

  // connect PULL-UP
  NRF_USBD->USBPULLUP = USBD_USBPULLUP_CONNECT_Msk;
  udev->flags |= USB_FLAG_CONNECTED;
  return 0;
}
/* disconnect usb */
int usb_nrf52_disconnect(usb_device_t *udev) {
  NRF_USBD->USBPULLUP = 0;
  NRF_USBD->ENABLE = 0;
  udev->flags &= ~USB_FLAG_CONNECTED;
  return 0;
}


/* acknowledge control transfer */
void usb_nrf52_ack_setup(usb_device_t *udev) {
  if (udev->flags & USB_EXTFLAG_SETADDR) {
    udev->flags &= ~USB_EXTFLAG_SETADDR;
  } else {
    NRF_USBD->TASKS_EP0STATUS = 1;
    usb_debug_print(DEBUG_LEVEL_VERBOSE,(" -> un52: EP0STATUS\n"));
  }
}
/* acknowledge setup part of control transfer */
void usb_nrf52_ack_control_setup(usb_device_t *udev) {
  if ((( udev->request.bmRequestType & USB_DATA_DIR_MASK) == USB_DATA_DIR_FROM_HOST) && udev->request.wLength) {
    udev->flags |= USB_EXTFLAG_GOTO_EP0RECEIVE | USB_FLAG_EVENT_RX0;;
    usb_debug_print(DEBUG_LEVEL_VERBOSE,(" ... ack_ctrl_setup - enb EXTFL_EP0RECEIVE\n"));
  }
}

/* set device address */
int usb_nrf52_set_addr(usb_device_t *udev, unsigned char addr) {
  udev->flags |= USB_EXTFLAG_SETADDR; /* do not fire EP0STATUS task */
  return 0;
}

int usb_nrf52_set_configuration(usb_device_t *udev, unsigned char iCfg) {
//  lpc13_usb_config_device(iCfg);
  if ( iCfg) {
    int i;
    for(i = 0; i < udev->cntep; i++) {
      if (udev->ep[i].max_packet_size>64) {
        /* ERROR - LPC134x has fixed maximal packet size 64B for int/bulk EP (and 512B for isochronous EP) */
        usb_debug_print(DEBUG_LEVEL_LOW,("NRF52USB: EP #%d require max.packet size %d > 64\n", i, udev->ep[i].max_packet_size));
      }

      uint32_t msk = 1<<(udev->ep[i].epnum & 0x7f);
      if (udev->ep[i].epnum & 0x80)  // in
        NRF_USBD->EPINEN |= msk;
      else { // out
        NRF_USBD->EPOUTEN |= msk;
        NRF_USBD->SIZE.EPOUT[udev->ep[i].epnum] = 0; /* enable receiving data */
      }
    }
  }
  return 0;
}

/* stall endpoint X */
void usb_nrf52_stall(usb_ep_t *ep) {
  uint8_t epnum = ep->epnum;
  NRF_USBD->EPSTALL = (uint32_t)(epnum) | USBD_EPSTALL_STALL_Msk;
  if (epnum==0) {
    NRF_USBD->TASKS_EP0STALL;
    usb_debug_print(DEBUG_LEVEL_VERBOSE,(" -> un52: EP0STALL\n"));
  }
}

/* unstall endpoint X */
void usb_nrf52_unstall(usb_ep_t *ep) {
  uint8_t epnum = ep->epnum;
  NRF_USBD->EPSTALL = (uint32_t)(epnum);
}

/**
 * usb_nrf52_check events
 * function reads interrupt register and sets event flags
 * function returns 1 if there is some new event.
 * function returns 0 if there isn't new event but all is OK
 * function returns -1 if there is any error
*/
int usb_nrf52_check_events(usb_device_t *udev)
{
  int ret=0;

  /* USB reset event */
  if (NRF_USBD->EVENTS_USBRESET) {
    udev->flags |= USB_FLAG_BUS_RESET;
    ret = 1;
    NRF_USBD->EVENTS_USBRESET = 0;
  }
  if (NRF_USBD->EVENTS_USBEVENT) {
    uint32_t evs = NRF_USBD->EVENTCAUSE;
    NRF_USBD->EVENTCAUSE = ~evs;
    NRF_USBD->EVENTS_USBEVENT = 0;
    if (evs & USBD_EVENTCAUSE_SUSPEND_Msk) {
        udev->flags |= USB_FLAG_SUSPEND;
        ret = 1;
    }
    if (evs & USBD_EVENTCAUSE_RESUME_Msk) {
      /* TODO */
    }
  }
  /* SETUP packet received */
  if (NRF_USBD->EVENTS_EP0SETUP) {
    udev->flags |= USB_FLAG_SETUP | USB_EXTFLAG_READ_SETUP;
    udev->request.bmRequestType = NRF_USBD->BMREQUESTTYPE;
    udev->request.bRequest = NRF_USBD->BREQUEST;
    udev->request.wValue  = (NRF_USBD->WVALUEH<<8) | NRF_USBD->WVALUEL;
    udev->request.wIndex  = (NRF_USBD->WINDEXH<<8) | NRF_USBD->WINDEXL;
    udev->request.wLength = (NRF_USBD->WLENGTHH<<8) | NRF_USBD->WLENGTHL;
    ret = 1;
    NRF_USBD->EVENTS_EP0SETUP = 0;
//    usb_debug_print( DEBUG_LEVEL_HIGH,("USB:SETUP rtp=x%02X req=x%02X val=x%04X idx=x%04X len=x%04X\n",
//                      udev->request.bmRequestType, udev->request.bRequest, udev->request.wValue,
//                      udev->request.wIndex, udev->request.wLength));
  }

  /* EP0.In ,EP0.Out data stage */
  if (NRF_USBD->EVENTS_EP0DATADONE) {
    usb_debug_print( DEBUG_LEVEL_HIGH,("usb_nrf52: EP0DATADONE (%d)\n", udev->ep0.flags));
    switch (udev->ep0.flags & USB_STATE_MASK) {
      case USB_STATE_TRANSMIT:
        udev->flags |= USB_FLAG_EVENT_TX0;
        break;
      case USB_STATE_RECEIVE:
        NRF_USBD->TASKS_STARTEPOUT[0] = 1;
        usb_debug_print(DEBUG_LEVEL_VERBOSE,(" -> un52: STARTEPOUT[0] (%lu/%lu @ x%08lX)\n", NRF_USBD->EPOUT[0].AMOUNT, NRF_USBD->EPOUT[0].MAXCNT, NRF_USBD->EPOUT[0].PTR));
        break;
      case USB_STATE_IDLE:
        if (udev->flags & USB_EXTFLAG_GOTO_EP0STATUS) {
          udev->flags &= ~USB_EXTFLAG_GOTO_EP0STATUS;
          usb_nrf52_ack_setup(udev);
        }
        break;
    }
    NRF_USBD->EVENTS_EP0DATADONE = 0;
  }
  if (NRF_USBD->EVENTS_ENDEPOUT[0]) {
    NRF_USBD->EVENTS_ENDEPOUT[0] = 0;
    usb_debug_print(DEBUG_LEVEL_VERBOSE,("usb_nrf52: (fl=%d) ENDEPOUT[0] (%lu/%lu @ x%08lX)\n", udev->ep0.flags, NRF_USBD->EPOUT[0].AMOUNT, NRF_USBD->EPOUT[0].MAXCNT, NRF_USBD->EPOUT[0].PTR));
    udev->flags |= USB_FLAG_EVENT_RX0;
  }

  /* EP1-EP7 data packets */
  if (NRF_USBD->EVENTS_EPDATA) {
    uint32_t eps = NRF_USBD->EPDATASTATUS;
    NRF_USBD->EVENTS_EPDATA = 0;
    for (int i=0;i<udev->cntep;i++) {
      uint8_t epa = udev->ep[i].epnum;
      uint32_t msk = usb_nrf52_epaddr2mask(epa);
      if (eps & msk) {
        NRF_USBD->EPDATASTATUS = msk;
        if (epa & USB_ENDPOINT_DIRECTION_MASK) {
          if (NRF_USBD->HALTED.EPIN[epa & USB_ENDPOINT_NUMBER_MASK] & USBD_HALTED_EPIN_GETSTATUS_Halted)
            usb_nrf52_unstall(&udev->ep[i]);
        } else {
          if (NRF_USBD->HALTED.EPOUT[epa & USB_ENDPOINT_NUMBER_MASK] & USBD_HALTED_EPIN_GETSTATUS_Halted)
            usb_nrf52_unstall(&udev->ep[i]);
        }
        udev->ep_events |= (1<<i);
        ret = 1;
      }
    }
  }

  return ret;
}

int usb_nrf52_read_endpoint(usb_ep_t *ep, void *ptr, int size)
{
  int rc = 0;
  usb_debug_print(DEBUG_LEVEL_MEDIUM,("usb_nrf52: read sz=%d, ep=%d (udev fl=x%04X) -> ptr=%p\n", size, ep->epnum, ep->udev->flags, ptr));
  if (!ep->epnum) { /* data from host on control EP0 (epnum by mel by USB_NRF52_LADDR_EP0OUT) */
    if (ep->udev->flags & USB_EXTFLAG_READ_SETUP) {
      /* SETUP PACKET is already placed in udev->request */
      rc = sizeof(USB_DEVICE_REQUEST);
      ep->udev->flags &= ~USB_EXTFLAG_READ_SETUP;
    } else {
      if (ep->udev->flags & USB_EXTFLAG_GOTO_EP0RECEIVE) {
        ep->udev->flags &= ~USB_EXTFLAG_GOTO_EP0RECEIVE;
        NRF_USBD->EPOUT[0].PTR = (uint32_t)ptr;
        NRF_USBD->EPOUT[0].MAXCNT = size;
        NRF_USBD->TASKS_EP0RCVOUT = 1;
        usb_debug_print(DEBUG_LEVEL_VERBOSE,(" -> un52: EP0RCVOUT (%lu/%lu @ x%08lX)\n", NRF_USBD->EPOUT[0].AMOUNT, NRF_USBD->EPOUT[0].MAXCNT, NRF_USBD->EPOUT[0].PTR));
      } else {
        /* TODO: read data from host in data stage */
        rc = NRF_USBD->EPOUT[0].AMOUNT;
        usb_debug_print(DEBUG_LEVEL_VERBOSE,(" ... read %d @ x%08lX [x%08lX]\n", rc, NRF_USBD->EPOUT[0].PTR, *(uint32_t *)ptr));
        if (ep->actual+rc < ep->size) {
          NRF_USBD->TASKS_EP0RCVOUT = 1;
          usb_debug_print(DEBUG_LEVEL_VERBOSE,(" -> un52: EP0RCVOUT\n"));
        }
      }
    }

  } else { /* read from other endpoints */
    int ei = ep->epnum & USB_ENDPOINT_NUMBER_MASK;
    rc = NRF_USBD->SIZE.EPOUT[ei];
    if (size<rc) rc = size;
    if (rc>ep->max_packet_size) rc = ep->max_packet_size;
    NRF_USBD->EPOUT[ei].PTR = (uint32_t)ptr;
    NRF_USBD->EPOUT[ei].MAXCNT = rc;
    NRF_USBD->EVENTS_ENDEPOUT[ei] = 0;
    NRF_USBD->TASKS_STARTEPOUT[ei] = 1;
    usb_debug_print(DEBUG_LEVEL_VERBOSE,(" -> un52: STARTEPOUT %d\n", ei));
    /* TODO: remove waiting for finishing downloading -> async. access */
    while (NRF_USBD->EVENTS_ENDEPOUT[ei]==0) {
    }
    NRF_USBD->EVENTS_ENDEPOUT[ei] = 0;
  }
  return rc;
}

int usb_nrf52_write_endpoint(usb_ep_t *ep, const void *ptr, int size)
{
  int rc = 0;
  usb_debug_print(DEBUG_LEVEL_MEDIUM,("usb_nrf52: write sz=%d, ptr=%p -> ep=%d (udev fl=x%04X)\n", size, ptr, ep->epnum, ep->udev->flags));
  if (!ep->epnum) { /* data to host from control EP0 (epnum by mel byt USB_NRF52_LADDR_EP0IN) */
    if (size<ep->max_packet_size)
      ep->udev->flags |= USB_EXTFLAG_GOTO_EP0STATUS;
    NRF_USBD->EPIN[0].PTR = (uint32_t)ptr;
    NRF_USBD->EPIN[0].MAXCNT = size;
    NRF_USBD->TASKS_STARTEPIN[0] = 1;
    usb_debug_print(DEBUG_LEVEL_VERBOSE,(" -> un52: STARTEPIN[0]\n"));
    rc = size;

  } else { /* write data for other endpoints */
    int ei = ep->epnum & USB_ENDPOINT_NUMBER_MASK;
    if (size>ep->max_packet_size) size = ep->max_packet_size;
    NRF_USBD->EPIN[ei].PTR = (uint32_t)ptr;
    NRF_USBD->EPIN[ei].MAXCNT = size;
    NRF_USBD->EVENTS_ENDEPIN[ei] = 0;
    NRF_USBD->TASKS_STARTEPIN[ei] = 1;
    usb_debug_print(DEBUG_LEVEL_VERBOSE,(" -> un52: STARTEPIN %d\n", ei));
    /* TODO: remove waiting for finishing downloading -> async. access */
    while (NRF_USBD->EVENTS_ENDEPIN[ei]==0) {
    }
    NRF_USBD->EVENTS_ENDEPIN[ei] = 0;
    rc = size;
  }
  return rc;
}

int usb_nrf52_check_power(usb_device_t *udev)
{
  return (NRF_POWER->USBREGSTATUS & POWER_USBREGSTATUS_VBUSDETECT_Msk);
}

/* ========================================================================== */
/* init usb structures and chip */
int usb_nrf52_init( usb_device_t *udev)
{
  udev->connect = usb_nrf52_connect;
  udev->disconnect = usb_nrf52_disconnect;
  udev->ack_setup = usb_nrf52_ack_setup;
  udev->ack_control_setup = usb_nrf52_ack_control_setup;
  udev->set_addr = usb_nrf52_set_addr;
  udev->set_configuration = usb_nrf52_set_configuration;
  udev->stall = usb_nrf52_stall;
  udev->unstall = usb_nrf52_unstall;
  udev->check_events = usb_nrf52_check_events;
  udev->check_power = usb_nrf52_check_power;

  udev->read_endpoint = usb_nrf52_read_endpoint;
  udev->write_endpoint = usb_nrf52_write_endpoint;

  udev->ep0.max_packet_size = USB_MAX_PACKET0;

  usb_nrf52_hw_init();
  return 0;
}
