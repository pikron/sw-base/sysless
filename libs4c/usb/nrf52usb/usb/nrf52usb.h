#ifndef _NRF52USB_MODULE
#define _NRF52USB_MODULE

#include <usb/usb.h>

/* control endpoints can be of any size */
#define USB_NRF52_LADDR_EP0IN       (0x80)
#define USB_NRF52_LADDR_EP0OUT      (0x00)
/* bulk/interrupt endpoints max.64B */
#define USB_NRF52_LADDR_EP1IN       (0x81)
#define USB_NRF52_LADDR_EP1OUT      (0x01)
#define USB_NRF52_LADDR_EP2IN       (0x82)
#define USB_NRF52_LADDR_EP2OUT      (0x02)
#define USB_NRF52_LADDR_EP3IN       (0x83)
#define USB_NRF52_LADDR_EP3OUT      (0x03)
#define USB_NRF52_LADDR_EP4IN       (0x84)
#define USB_NRF52_LADDR_EP4OUT      (0x04)
#define USB_NRF52_LADDR_EP5IN       (0x85)
#define USB_NRF52_LADDR_EP5OUT      (0x05)
#define USB_NRF52_LADDR_EP6IN       (0x86)
#define USB_NRF52_LADDR_EP6OUT      (0x06)
#define USB_NRF52_LADDR_EP7IN       (0x87)
#define USB_NRF52_LADDR_EP7OUT      (0x07)
/* isochronous enpoints max.1023B */
#define USB_NRF52_LADDR_EP8IN       (0x88)
#define USB_NRF52_LADDR_EP8OUT      (0x08)


#define usb_nrf52_epaddr2mask(epa) ( (uint32_t)1 << (((uint32_t)epa & 0x0F) + ((epa&0x80)?0:16) ))

int usb_nrf52_init(usb_device_t *udev);

#endif /* from _NRF52USB_MODULE */
