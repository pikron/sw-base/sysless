/**
 * USB CDC class specification
 * RB 2015
 * based on USB Class Definitions for Communications Devices rev. 1.2 December 6, 2012
 */

#ifndef _USB_CDC_CLASS_SPECIFICATIONS_
 #define _USB_CDC_CLASS_SPECIFICATIONS_

#include <stdint.h>
#include <usb/usb_spec.h>
#include <usb/usb.h>

#ifndef PACKED
  #ifdef  __GNUC__
    #define PACKED __attribute__((packed))
  #else /*__GNUC__*/
    #define PACKED /*nothing*/
  #endif /*__GNUC__*/
#endif

/* USB CDC Interface Class */
#define USBCDC_IFACE_CLASS_COM            USB_DEVICE_CLASS_COMMUNICATIONS
#define USBCDC_IFACE_CLASS_DATA           USB_DEVICE_CLASS_CDC_DATA

/* USB CDC Interface Communication Subclass */
#define USBCDC_COM_IFACE_SUBCLS_DLCM      0x01   /* Direct Line Control Model */
#define USBCDC_COM_IFACE_SUBCLS_ACM       0x02   /* Abstract Control Model */
#define USBCDC_COM_IFACE_SUBCLS_TCM       0x03   /* Telephone Control Model */
#define USBCDC_COM_IFACE_SUBCLS_MCCM      0x04   /* Multi-Channel Control Model */
#define USBCDC_COM_IFACE_SUBCLS_CAPI      0x05   /* CAPI Control Model */
#define USBCDC_COM_IFACE_SUBCLS_ENCM      0x06   /* Ethernet Networking Control Model */
#define USBCDC_COM_IFACE_SUBCLS_ATM       0x07   /* ATM Networking Control Model */
#define USBCDC_COM_IFACE_SUBCLS_WHCM      0x08   /* Wireless Handset Control Model */
#define USBCDC_COM_IFACE_SUBCLS_DEVMGMT   0x09   /* Device Management */
#define USBCDC_COM_IFACE_SUBCLS_MDLM      0x0A   /* Mobile Direct Line Model */
#define USBCDC_COM_IFACE_SUBCLS_OBEX      0x0B   /* OBEX */
#define USBCDC_COM_IFACE_SUBCLS_EEM       0x0C   /* Ethernet Emulation Model */
#define USBCDC_COM_IFACE_SUBCLS_NCM       0x0D   /* Network Control Model */
#define USBCDC_COM_IFACE_SUBCLS_MBIM      0x0E   /* Mobile Broadband Interface Model */

/* USB CDC Interface Communication Protocol */
#define USBCDC_COM_IFACE_PROTO_NONE       0x00   /* No class specific protocol required */
#define USBCDC_COM_IFACE_PROTO_V250       0x01   /* AT Commands: V.250 etc */
#define USBCDC_COM_IFACE_PROTO_PCCA101    0x02   /* AT Commands defined by PCCA-101 */
#define USBCDC_COM_IFACE_PROTO_PCCA101O   0x03   /* AT Commands defined by PCCA-101 & Annex O */
#define USBCDC_COM_IFACE_PROTO_GSM07      0x04   /* AT Commands defined by GSM 07.07 */
#define USBCDC_COM_IFACE_PROTO_3GPP27     0x05   /* AT Commands defined by 3GPP 27.007 */
#define USBCDC_COM_IFACE_PROTO_TIA_CDMA   0x06   /* AT Commands defined by TIA for CDMA */
#define USBCDC_COM_IFACE_PROTO_EEM        0x07   /* Ethernet Emulation Model */
#define USBCDC_COM_IFACE_PROTO_EXTERNAL   0xFE   /* External Protocol: Commands defined by Command Set Functional Descriptor */
#define USBCDC_COM_IFACE_PROTO_VENDOR     0xFF   /* Vendor-specific */

/* USB CDC Interface Data Subclass */
#define USBCDC_DATA_IFACE_SUBCLS          0x00

/* USB CDC Interface Data Protocol */
#define USBCDC_DATA_IFACE_PROTO_NONE      0x00
#define USBCDC_DATA_IFACE_PROTO_NTB       0x01  /* Network Transfer Block */
#define USBCDC_DATA_IFACE_PROTO_NTBDSS    0x02  /* Network Transfer Block (IP+DSS) */
#define USBCDC_DATA_IFACE_PROTO_I430      0x30  /* Physical interface protocol for ISDN BRI */
#define USBCDC_DATA_IFACE_PROTO_HDLC      0x31  /* HDLC (ISO/IEC 3309-1993) */
#define USBCDC_DATA_IFACE_PROTO_TRANSP    0x32  /* Transparent */
#define USBCDC_DATA_IFACE_PROTO_Q921MGMT  0x50  /* Management protocol for Q.921 data link protocol */
#define USBCDC_DATA_IFACE_PROTO_Q921      0x51  /* Data link protocol for Q.931 */
#define USBCDC_DATA_IFACE_PROTO_Q921TM    0x52  /* TEI-multiplexor for Q.921 data link protocol */
#define USBCDC_DATA_IFACE_PROTO_V42BIS    0x90  /* Data compression procedures */
#define USBCDC_DATA_IFACE_PROTO_EISDN     0x91  /* Euro-ISDN protocol control */
#define USBCDC_DATA_IFACE_PROTO_V120      0x92  /* V.24 rate adaptation to ISDN */
#define USBCDC_DATA_IFACE_PROTO_CAPI      0x93  /* CAPI Commands */
#define USBCDC_DATA_IFACE_PROTO_FCNDESC   0xFE  /* The protocol(s) are described using a Protocol Unit Functional Descriptors on Communications Class Interface */
#define USBCDC_DATA_IFACE_PROTO_VENDOR    0xFF  /* Vendor-specific */

/* -------------------------------------------------------------------------- */
/* Functional Descriptors */
  /* Functional Descriptor Type */
#define USBCDC_COM_FCN_TYPE_CS_INTERFACE     0x24
#define USBCDC_COM_FCN_TYPE_CS_ENDPOINT      0x25

  /* Functional Descriptor Subtype in Communication Class */
#define USBCDC_COM_FCN_SUBTYPE_HEADER      0x00  /* Header Functional Descriptor, which marks the beginning of the concatenated set of functional descriptors for the interface. */
#define USBCDC_COM_FCN_SUBTYPE_CALLMGMT    0x01  /* Call Management Functional Descriptor. */
#define USBCDC_COM_FCN_SUBTYPE_ACMGMT      0x02  /* Abstract Control Management Functional Descriptor. */
#define USBCDC_COM_FCN_SUBTYPE_DLMGMT      0x03  /* Direct Line Management Functional Descriptor. */
#define USBCDC_COM_FCN_SUBTYPE_TELRING     0x04  /* Telephone Ringer Functional Descriptor. */
#define USBCDC_COM_FCN_SUBTYPE_TELCALL     0x05  /* Telephone Call and Line State Reporting Capabilities Functional Descriptor. */
#define USBCDC_COM_FCN_SUBTYPE_UNION       0x06  /* Union Functional Descriptor */
#define USBCDC_COM_FCN_SUBTYPE_COUNTRYSEL  0x07  /* Country Selection Functional Descriptor */
#define USBCDC_COM_FCN_SUBTYPE_TELOP       0x08  /* Telephone Operational Modes Functional Descriptor */
#define USBCDC_COM_FCN_SUBTYPE_USBTERM     0x09  /* USB Terminal Functional Descriptor */
#define USBCDC_COM_FCN_SUBTYPE_NCTERM      0x0A  /* Network Channel Terminal Descriptor */
#define USBCDC_COM_FCN_SUBTYPE_PROTOUNIT   0x0B  /* Protocol Unit Functional Descriptor */
#define USBCDC_COM_FCN_SUBTYPE_EXTUNIT     0x0C  /* Extension Unit Functional Descriptor */
#define USBCDC_COM_FCN_SUBTYPE_MCMGMT      0x0D  /* Multi-Channel Management Functional Descriptor */
#define USBCDC_COM_FCN_SUBTYPE_CAPI        0x0E  /* CAPI Control Management Functional Descriptor */
#define USBCDC_COM_FCN_SUBTYPE_ENCM        0x0F  /* Ethernet Networking Functional Descriptor */
#define USBCDC_COM_FCN_SUBTYPE_ATMNET      0x10  /* ATM Networking Functional Descriptor */
#define USBCDC_COM_FCN_SUBTYPE_WHCM        0x11  /* Wireless Handset Control Model Functional Descriptor */
#define USBCDC_COM_FCN_SUBTYPE_MDLM        0x12  /* Mobile Direct Line Model Functional Descriptor */
#define USBCDC_COM_FCN_SUBTYPE_MDLM_DET    0x13  /* MDLM Detail Functional Descriptor */
#define USBCDC_COM_FCN_SUBTYPE_DEVMGMT     0x14  /* Device Management Model Functional Descriptor */
#define USBCDC_COM_FCN_SUBTYPE_OBEX        0x15  /* OBEX Functional Descriptor */
#define USBCDC_COM_FCN_SUBTYPE_CMDSET      0x16  /* Command Set Functional Descriptor */
#define USBCDC_COM_FCN_SUBTYPE_CMDSET_DET  0x17  /* Command Set Detail Functional Descriptor */
#define USBCDC_COM_FCN_SUBTYPE_TCM         0x18  /* Telephone Control Model Functional Descriptor */
#define USBCDC_COM_FCN_SUBTYPE_OBEX_SID    0x19  /* OBEX Service Identifier Functional Descriptor */
#define USBCDC_COM_FCN_SUBTYPE_NCM         0x1A  /* NCM Functional Descriptor */
#define USBCDC_COM_FCN_SUBTYPE_MBIM        0x1B  /* MBIM Functional Descriptor */
#define USBCDC_COM_FCN_SUBTYPE_MBIMEXT     0x1C  /* MBIM Extended Functional Descriptor */

  /* Functional Descriptor Subtype in Data Class */
#define USBCDC_DATA_FCN_SUBTYPE_HEADER     0x00  /* Header */

/* Header Functional Descriptor */
  struct _tag_usbcdc_header_functional_descriptor {
    uint8_t  bFunctionLength;
    uint8_t  bDescriptorType;
    uint8_t  bDescriptorSubtype;
    uint16_t bcdCDC;
  } PACKED;
  typedef struct _tag_usbcdc_header_functional_descriptor
    USBCDC_HEADER_FCN_DESCRIPTOR, *PUSBCDC_HEADER_FCN_DESCRIPTOR;

/* Union Functional Descriptor */
  struct _tag_usbcdc_union_functional_descriptor {
    uint8_t  bFunctionLength;
    uint8_t  bDescriptorType;
    uint8_t  bDescriptorSubtype;
    uint8_t  bControlInterface;
    //uint8_t  bSubordinateInterface0[];
    uint8_t  bSubordinateInterface0;
  } PACKED;
  typedef struct _tag_usbcdc_union_functional_descriptor
    USBCDC_UNION_FCN_DESCRIPTOR, *PUSBCDC_UNION_FCN_DESCRIPTOR;

/* Country Selection Functional Descriptor */
  struct _tag_usbcdc_countrysel_functional_descriptor {
    uint8_t  bFunctionLength;
    uint8_t  bDescriptorType;
    uint8_t  bDescriptorSubtype;
    uint8_t  iCountryCodeRelDate;
    uint16_t wCountryCode[];
  } PACKED;
  typedef struct _tag_usbcdc_countrysel_functional_descriptor
    USBCDC_COUNTRYSEL_FCN_DESCRIPTOR, *PUSBCDC_COUNTRYSEL_FCN_DESCRIPTOR;

/* Class-Specific Request Codes */
#define USBCDC_REQUEST_SEND_ENCAPS_CMD            0x00
#define USBCDC_REQUEST_GET_ENCAPS_RESP            0x01
#define USBCDC_REQUEST_SET_COMM_FEATURE           0x02
#define USBCDC_REQUEST_GET_COMM_FEATURE           0x03
#define USBCDC_REQUEST_CLEAR_COMM_FEATURE         0x04
#define USBCDC_REQUEST_RESET_FUNCTION             0x05
#define USBCDC_REQUEST_SET_AUX_LINE_STATE         0x10
#define USBCDC_REQUEST_SET_HOOK_STATE             0x11
#define USBCDC_REQUEST_PULSE_SETUP                0x12
#define USBCDC_REQUEST_SEND_PULSE                 0x13
#define USBCDC_REQUEST_SET_PULSE_TIME             0x14
#define USBCDC_REQUEST_RING_AUX_JACK              0x15
#define USBCDC_REQUEST_SET_LINE_CODING            0x20
#define USBCDC_REQUEST_GET_LINE_CODING            0x21
#define USBCDC_REQUEST_SET_CONTROL_LINE_STATE     0x22
#define USBCDC_REQUEST_SEND_BREAK                 0x23
#define USBCDC_REQUEST_SET_RINGER_PARMS           0x30
#define USBCDC_REQUEST_GET_RINGER_PARMS           0x31
#define USBCDC_REQUEST_SET_OPERATION_PARMS        0x32
#define USBCDC_REQUEST_GET_OPERATION_PARMS        0x33
#define USBCDC_REQUEST_SET_LINE_PARMS             0x34
#define USBCDC_REQUEST_GET_LINE_PARMS             0x35
#define USBCDC_REQUEST_DIAL_DIGITS                0x36
#define USBCDC_REQUEST_SET_UNIT_PARAMETER         0x37
#define USBCDC_REQUEST_GET_UNIT_PARAMETER         0x38
#define USBCDC_REQUEST_CLEAR_UNIT_PARAMETER       0x39
#define USBCDC_REQUEST_GET_PROFILE                0x3A
#define USBCDC_REQUEST_SET_ETH_MULTICAST_FILTERS          0x40
#define USBCDC_REQUEST_SET_ETH_PWR_MGMT_PATTERN_FILTER    0x41
#define USBCDC_REQUEST_GET_ETH_PWR_MGMT_PATTERN_FILTER    0x42
#define USBCDC_REQUEST_SET_ETH_PACKET_FILTER      0x43
#define USBCDC_REQUEST_GET_ETH_STATISTICS         0x44
#define USBCDC_REQUEST_SET_ATM_DATA_FORMAT        0x50
#define USBCDC_REQUEST_SET_ATM_DEVICE_STATISTICS  0x51
#define USBCDC_REQUEST_SET_ATM_DEFAULT_VC         0x52
#define USBCDC_REQUEST_GET_ATM_VC_STATISTICS      0x53
// #define USBCDC_REQUEST_MDLM_SPECIFIC 0x60-0x7F
#define USBCDC_REQUEST_GET_NTB_PARAMETERS         0x80
#define USBCDC_REQUEST_GET_NET_ADDRESS            0x81
#define USBCDC_REQUEST_SET_NET_ADDRESS            0x82
#define USBCDC_REQUEST_GET_NTB_FORMAT             0x83
#define USBCDC_REQUEST_SET_NTB_FORMAT             0x84
#define USBCDC_REQUEST_GET_NTB_INPUT_SIZE         0x85
#define USBCDC_REQUEST_SET_NTB_INPUT_SIZE         0x86
#define USBCDC_REQUEST_GET_MAX_DATAGRAM_SIZE      0x87
#define USBCDC_REQUEST_SET_MAX_DATAGRAM_SIZE      0x88
#define USBCDC_REQUEST_GET_CRC_MODE               0x89
#define USBCDC_REQUEST_SET_CRC_MODE               0x8A

/* Notification Code */
#define USBCDC_NOTIFY_NETWORK_CONNECTION          0x00
#define USBCDC_NOTIFY_RESPONSE_AVAILABLE          0x01
#define USBCDC_NOTIFY_AUX_JACK_HOOK_STATE         0x08
#define USBCDC_NOTIFY_RING_DETECT                 0x09
#define USBCDC_NOTIFY_SERIAL_STATE                0x20
#define USBCDC_NOTIFY_CALL_STATE_CHANGE           0x28
#define USBCDC_NOTIFY_LINE_STATE_CHANGE           0x29
#define USBCDC_NOTIFY_CONNECTION_SPEED_CHANGE     0x2A
// #define USBCDC_NOTIFY_MDML_SPECIFIC   0x40-0x5F


/* ========================================================================== */
/* USB Communication Class, Subclass Specification for PSTN Devices */

/* Call Management Functional Descriptor */
  struct _tag_usbcdc_callmgmt_functional_descriptor {
    uint8_t  bFunctionLength;
    uint8_t  bDescriptorType;
    uint8_t  bDescriptorSubtype;
    uint8_t  bmCapabilities;
    uint8_t  bDataInterface;
  } PACKED;
  typedef struct _tag_usbcdc_callmgmt_functional_descriptor
    USBCDC_CALLMGMT_FCN_DESCRIPTOR, *PUSBCDC_CALLMGMT_FCN_DESCRIPTOR;

/* Abstract Control Management Functional Descriptor */
  struct _tag_usbcdc_acm_functional_descriptor {
    uint8_t  bFunctionLength;
    uint8_t  bDescriptorType;
    uint8_t  bDescriptorSubtype;
    uint8_t  bmCapabilities;
  } PACKED;
  typedef struct _tag_usbcdc_acm_functional_descriptor
    USBCDC_ACM_FCN_DESCRIPTOR, *PUSBCDC_ACM_FCN_DESCRIPTOR;

#define USBCDC_ACM_FCN_CAP_SUPPORT_NETCONN      0x08    /* Device supports the notification Network_Connection. */
#define USBCDC_ACM_FCN_CAP_SUPPORT_SENDBREAK    0x04    /* Device supports the request Send_Break */
#define USBCDC_ACM_FCN_CAP_SUPPORT_LINECTRL     0x02    /* Device supports the request combination of Set_Line_Coding, Set_Control_Line_State, Get_Line_Coding, and the notification Serial_State. */
#define USBCDC_ACM_FCN_CAP_SUPPORT_COMMFEATURE  0x01    /* Device supports the request combination of Set_Comm_Feature, Clear_Comm_Feature, and Get_Comm_Feature. */


/* Direct Line Management Functional Descriptor */
  struct _tag_usbcdc_dlmgmt_functional_descriptor {
    uint8_t  bFunctionLength;
    uint8_t  bDescriptorType;
    uint8_t  bDescriptorSubtype;
    uint8_t  bmCapabilities;
  } PACKED;
  typedef struct _tag_usbcdc_dlmgmt_functional_descriptor
    USBCDC_DLMGMT_FCN_DESCRIPTOR, *PUSBCDC_DLMGMT_FCN_DESCRIPTOR;

/* Telephone Rigner Functional Descriptor */
  struct _tag_usbcdc_telring_functional_descriptor {
    uint8_t  bFunctionLength;
    uint8_t  bDescriptorType;
    uint8_t  bDescriptorSubtype;
    uint8_t  bRingerVolSteps;
    uint8_t  bNumRingerPatterns;
  } PACKED;
  typedef struct _tag_usbcdc_telring_functional_descriptor
    USBCDC_TELRING_FCN_DESCRIPTOR, *PUSBCDC_TELRING_FCN_DESCRIPTOR;

/* Telephone Operational Modes Functional Descriptor */
  struct _tag_usbcdc_telopmodes_functional_descriptor {
    uint8_t  bFunctionLength;
    uint8_t  bDescriptorType;
    uint8_t  bDescriptorSubtype;
    uint8_t  bmCapabilities;
  } PACKED;
  typedef struct _tag_usbcdc_telopmodes_functional_descriptor
    USBCDC_TELOPMODES_FCN_DESCRIPTOR, *PUSBCDC_TELOPMODES_FCN_DESCRIPTOR;

/* Telephone Call & Line State Reporting Capabilities Descriptor */
  struct _tag_usbcdc_telcall_functional_descriptor {
    uint8_t  bFunctionLength;
    uint8_t  bDescriptorType;
    uint8_t  bDescriptorSubtype;
    uint8_t  bmCapabilities;
  } PACKED;
  typedef struct _tag_usbcdc_telcall_functional_descriptor
    USBCDC_TELCALL_FCN_DESCRIPTOR, *PUSBCDC_TELCALL_FCN_DESCRIPTOR;

/* ---------------------------------- */
/* DLCM Requests */
// SetAuxLineState - Optional
// SetHookState - Required
// PulseSetup - Optional
// SendPulse - Optional
// SetPulseTime - Optional
// RingAuxJack - Optional


/* ACM Requests */
// SendEncapsulatedCommand - Required
// GetEncapsulatedResponse - Required
// SetCommFeature - Optional
// GetCommFeature - Optional
// ClearCommFeature - Optional
// SetLineCoding - Optional (Recommended)
// GetLineCoding - Optional (Recommended)
// SetControlLineState - Optional
// SendBreak - Optional

/* TCM Requests */
// SetCommFeature - Optional
// GetCommFeature - Optional
// ClearCommFeature - Optional
// SetRingerParms - Optional
// GetRingerParms - Required
// SetOperationParms - Optional
// GetOperationParms - Optional
// SetLineParms - Required
// GetLineParms - Required
// DialDigits - Required

/* SetLineCoding, GetLineCoding */
  struct _tag_usbcdc_line_coding_structure {
    uint32_t dwDTERate;   // data terminal rate, in bits per second
    uint8_t  bCharFormat; // stop bits
    uint8_t  bParityType; // parity
    uint8_t  bDataBits;   // Data bits
  } PACKED;
  typedef struct _tag_usbcdc_line_coding_structure
    USBCDC_LINE_CODING_STRUCTURE, *PUSBCDC_LINE_CODING_STRUCTURE;

#define USBCDC_LINE_CODING_STOP_1         0
#define USBCDC_LINE_CODING_STOP_15        1
#define USBCDC_LINE_CODING_STOP_2         2

#define USBCDC_LINE_CODING_PARITY_NONE    0
#define USBCDC_LINE_CODING_PARITY_ODD     1
#define USBCDC_LINE_CODING_PARITY_EVEN    2
#define USBCDC_LINE_CODING_PARITY_MARK    3
#define USBCDC_LINE_CODING_PARITY_SPACE   4

#define USBCDC_LINE_CODING_DATA_5         5
#define USBCDC_LINE_CODING_DATA_6         6
#define USBCDC_LINE_CODING_DATA_7         7
#define USBCDC_LINE_CODING_DATA_8         8
#define USBCDC_LINE_CODING_DATA_16        16


/* ---------------------------------- */
/* DLCM Notifications */
// AuxJackHookState - Optional
// RingDetect - Required

/* ACM Notifications */
// NetworkConnection - Optional (Recommended)
// ResponseAvailable - Required
// SerialState - Optional (Recommended)

/* TCM Notifications */
// CallStateChange - Required
// Reports - Optional

#define USBCDC_SERIAL_STATE_RX_CARRIER    0x0001
#define USBCDC_SERIAL_STATE_TX_CARRIER    0x0002
#define USBCDC_SERIAL_STATE_BREAK         0x0004
#define USBCDC_SERIAL_STATE_RING_SIGNAL   0x0008
#define USBCDC_SERIAL_STATE_FRAMING       0x0010
#define USBCDC_SERIAL_STATE_PARITY        0x0020
#define USBCDC_SERIAL_STATE_OVERRUN       0x0040


/* usbcdc_acm.c */
// extern acm_desc_t usbcdc_acm_description - init in program
typedef struct acm_desc {
  USBCDC_LINE_CODING_STRUCTURE line_coding;
  unsigned int line_state;
  unsigned int flags;
} acm_desc_t;


#define USB_ACM_LINE_STATE_DTR_PRESENT 0x01
#define USB_ACM_LINE_STATE_RTS_ACTIVE  0x02

#define USB_ACM_FLAG_SET_LINE_CODING  0x01


int usbcdc_acm_class_response(struct usb_device_t *udev);


#endif /* _USB_CDC_CLASS_SPECIFICATIONS_ */
