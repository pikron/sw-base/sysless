/**
 * USB ACM module
 * Roman Bartosinski, 2015
 */

#include <string.h>

#include <system_def.h>
#include <usb/usb.h>
#include <usb/usb_cdc.h>

#include <usb/usbdebug.h>

extern acm_desc_t usbcdc_acm_description;

int usbcdc_acm_send_enc_cmd(struct usb_device_t *udev)
{
  usb_debug_print(DEBUG_LEVEL_LOW,("CDC-SEND_CMD\n"));
  return USB_COMPLETE_FAIL;
}

int usbcdc_acm_get_enc_resp(struct usb_device_t *udev)
{
  usb_debug_print(DEBUG_LEVEL_LOW,("CDC-GET_RESP\n"));
  return USB_COMPLETE_FAIL;
}


int usbcdc_acm_set_line_coding_complete(struct usb_ep_t *ep, int status)
{
  usb_debug_print(DEBUG_LEVEL_LOW,("CDC-SLC st=%d, ep-sz=%d\n", status, ep->size));
  usb_debug_print(DEBUG_LEVEL_HIGH,(" ... brate=%lu, stop=%u, par=%u, dbit=%u\n",
                  usbcdc_acm_description.line_coding.dwDTERate,
                  usbcdc_acm_description.line_coding.bCharFormat,
                  usbcdc_acm_description.line_coding.bParityType,
                  usbcdc_acm_description.line_coding.bDataBits));
  usb_ack_setup(ep);
  return USB_COMPLETE_OK;
}

int usbcdc_acm_set_line_coding(struct usb_device_t *udev)
{
  usb_debug_print( DEBUG_LEVEL_HIGH, ("CDC-SetLineCoding\n"));
  udev->ep0.ptr = (unsigned char *)&usbcdc_acm_description.line_coding;
  udev->ep0.size = sizeof(usbcdc_acm_description.line_coding);
  udev->ep0.complete_fnc = usbcdc_acm_set_line_coding_complete;
  return USB_COMPLETE_OK;
}

int usbcdc_acm_get_line_coding(struct usb_device_t *udev)
{
  unsigned int len = sizeof(usbcdc_acm_description.line_coding);
  USB_DEVICE_REQUEST *preq = &(udev->request);
  usb_debug_print(DEBUG_LEVEL_HIGH, ("CDC-GetLineCoding\n"));
  if (preq->wLength < len) len = preq->wLength;
  usb_send_control_data(udev, (unsigned char *)&usbcdc_acm_description.line_coding, len);
  return USB_COMPLETE_OK;
}

int usbcdc_acm_set_control_line_state(struct usb_device_t *udev)
{
  usb_debug_print(DEBUG_LEVEL_HIGH, ("CDC-SetContrlLineState\n"));
  usbcdc_acm_description.line_state = udev->request.wValue & (USB_ACM_LINE_STATE_DTR_PRESENT | USB_ACM_LINE_STATE_RTS_ACTIVE);
  usb_udev_ack_setup(udev);
  return USB_COMPLETE_OK;
}

int usbcdc_acm_class_response(struct usb_device_t *udev)
{
  unsigned char req;
  int ret = USB_COMPLETE_FAIL;
  USB_DEVICE_REQUEST *preq = &(udev->request);

  req = preq->bRequest;
  usb_debug_print(DEBUG_LEVEL_HIGH, ("CDC-ACMReq-%d\n", req));

  if (((preq->bmRequestType & USB_REQUEST_TYPE_MASK) == USB_CLASS_REQUEST) &&
      ((preq->bmRequestType & USB_RECIPIENT) == USB_RECIPIENT_INTERFACE)) {
    switch(req) {
      case USBCDC_REQUEST_SEND_ENCAPS_CMD:
        ret = usbcdc_acm_send_enc_cmd(udev);
        break;
      case USBCDC_REQUEST_GET_ENCAPS_RESP:
        ret = usbcdc_acm_get_enc_resp(udev);
        break;
//    case USBCDC_REQUEST_SET_COMM_FEATURE:
//    case USBCDC_REQUEST_GET_COMM_FEATURE:
//    case USBCDC_REQUEST_CLEAR_COMM_FEATURE:
      case USBCDC_REQUEST_SET_LINE_CODING:
        ret = usbcdc_acm_set_line_coding(udev);
        break;
      case USBCDC_REQUEST_GET_LINE_CODING:
        ret = usbcdc_acm_get_line_coding(udev);
        break;
      case USBCDC_REQUEST_SET_CONTROL_LINE_STATE:
        ret = usbcdc_acm_set_control_line_state(udev);
        break;
//    case USBCDC_REQUEST_SEND_BREAK:
      default:
        usb_udev_stall((&(udev->ep0)));
        break;
    }
  }
  return ret;
}
