/**
 * USB HID class specification
 * RB 2015
 * based on USB Device Class Definition for Human Interface Devices (HID) FW spec. 6/27/01 ver.1.11
 */

#ifndef _USB_HID_CLASS_SPECIFICATIONS_
 #define _USB_HID_CLASS_SPECIFICATIONS_

#include <stdint.h>

#ifndef PACKED
  #ifdef  __GNUC__
    #define PACKED __attribute__((packed))
  #else /*__GNUC__*/
    #define PACKED /*nothing*/
  #endif /*__GNUC__*/
#endif

/* USB HID Interface Subclass */
#define USBHID_IFACE_SUBCLASS_NONE       0x00
#define USBHID_IFACE_SUBCLASS_BOOT       0x01

/* USB HID Interface Protocol */
#define USBHID_IFACE_PROTOCOL_NONE       0x00
#define USBHID_IFACE_PROTOCOL_KEYBOARD   0x01
#define USBHID_IFACE_PROTOCOL_MOUSE      0x02

/* USB HID Descriptor Type */
#define USBHID_DESCRIPTOR_TYPE_HID       0x21
#define USBHID_DESCRIPTOR_TYPE_REPORT    0x22
#define USBHID_DESCRIPTOR_TYPE_PHYSICAL  0x23

/* USB HID class requests */
#define USBHID_REQUEST_GET_REPORT        0x01
#define USBHID_REQUEST_GET_IDLE          0x02
#define USBHID_REQUEST_GET_PROTOCOL      0x03
#define USBHID_REQUEST_SET_REPORT        0x09
#define USBHID_REQUEST_SET_IDLE          0x0A
#define USBHID_REQUEST_SET_PROTOCOL      0x0B

/* Report Type in Hi(wValue) for GET_REPORT/SET_REPORT */
#define USBHID_REQUEST_REPORT_TYPE_INPUT    0x01
#define USBHID_REQUEST_REPORT_TYPE_OUTPUT   0x02
#define USBHID_REQUEST_REPORT_TYPE_FEATURE  0x03


struct _tag_usb_hid_descriptor {
  uint8_t  bLength;
  uint8_t  bDescriptorType;
  uint16_t bcdHID;
  uint8_t  bCountryCode;
  uint8_t  bNumDescriptors;
  /** FIXME! { array_begin **/
  uint8_t  bDescriptorType_;
  uint16_t wDescriptorLength;
  /** } array end **/
} PACKED;
typedef struct _tag_usb_hid_descriptor 
USB_HID_DESCRIPTOR, *PUSB_HID_DESCRIPTOR;

/* USB HID Country Code */
#define USBHID_COUNTRY_NOTSUP    00
#define USBHID_COUNTRY_AR        01   /* Arabic */
#define USBHID_COUNTRY_BE        02   /* Belgian */
#define USBHID_COUNTRY_CA_BI     03   /* Canadian-Bilingual */
#define USBHID_COUNTRY_CA_FR     04   /* Canadian-French */
#define USBHID_COUNTRY_CS        05   /* Czech Republic */
#define USBHID_COUNTRY_DA        06   /* Danish */
#define USBHID_COUNTRY_FI        07   /* Finnish */
#define USBHID_COUNTRY_FR        08   /* French */
#define USBHID_COUNTRY_DE        09   /* German */
#define USBHID_COUNTRY_EL        10   /* Greek */
#define USBHID_COUNTRY_HE        11   /* Hebrew */
#define USBHID_COUNTRY_HU        12   /* Hungary */
#define USBHID_COUNTRY_ISO       13   /* International (ISO) */
#define USBHID_COUNTRY_IT        14   /* Italian */
#define USBHID_COUNTRY_JA        15   /* Japan (Katakana) */
#define USBHID_COUNTRY_KO        16   /* Korean */
 #define USBHID_COUNTRY_LAT       17   /* Latin American */
#define USBHID_COUNTRY_NL        18   /* Netherlands/Dutch */
#define USBHID_COUNTRY_NO        19   /* Norwegian */
 #define USBHID_COUNTRY_PER       20   /* Persian (Farsi) */
#define USBHID_COUNTRY_PL        21   /* Poland */
#define USBHID_COUNTRY_PT        22   /* Portuguese */
#define USBHID_COUNTRY_RU        23   /* Russia */
#define USBHID_COUNTRY_SK        24   /* Slovakia */
#define USBHID_COUNTRY_ES        25   /* Spanish */
#define USBHID_COUNTRY_SV        26   /* Swedish */
#define USBHID_COUNTRY_CH_FR     27   /* Swiss/French */
#define USBHID_COUNTRY_CH_DE     28   /* Swiss/German */
#define USBHID_COUNTRY_CH        29   /* Switzerland */
#define USBHID_COUNTRY_TW        30   /* Taiwan */
#define USBHID_COUNTRY_TR_Q      31   /* Turkish-Q */
#define USBHID_COUNTRY_EN_GB     32   /* UK */
#define USBHID_COUNTRY_EN_US     33   /* US */
#define USBHID_COUNTRY_YUG       34   /* Yugoslavia */
#define USBHID_COUNTRY_TR_F      35   /* Turkish-F */

/* -------------------------------------------------------------------------- */
/* REPORT Descriptor - items */
/* - Items have variable length (in the following macros, l=8bit -> no suffix, l=16bit -> suffix '16' and l=32bit -> suffix '32' */
/* Item Types: Main, Global, Local */

/* --- Report item macros --- */
/* Main items */
/* Input, Output, Feature, Collection, EndCollection - !!! we assume bit8=0 (BitField, no BufferedBytes) is used in all items (USB HID Standard 1.11 pg.28) */
#define USBHID_RIM_Input(x)        0x81,x
#define USBHID_RIM_Input16(x)      0x82,(x&0xff),((x>>8)&0xff)
#define USBHID_RIM_Output(x)       0x91,x
#define USBHID_RIM_Output16(x)     0x92,(x&0xff),((x>>8)&0xff)
#define USBHID_RIM_Feature(x)      0xB1,x
#define USBHID_RIM_Feature16(x)    0xB2,(x&0xff),((x>>8)&0xff)
#define USBHID_RIM_Collection(x)   0xA1,x
#define USBHID_RIM_EndCollection   0xC0

/* Data (Input/Output/Feature main items) */
#define USBHID_RIM_IOF_DATA            (0<<0)
#define USBHID_RIM_IOF_CONSTANT        (1<<0)
#define USBHID_RIM_IOF_ARRAY           (0<<1)
#define USBHID_RIM_IOF_VARIABLE        (1<<1)
#define USBHID_RIM_IOF_ABSOLUTE        (0<<2)
#define USBHID_RIM_IOF_RELATIVE        (1<<2)
#define USBHID_RIM_IOF_NOWRAP          (0<<3)
#define USBHID_RIM_IOF_WRAP            (1<<3)
#define USBHID_RIM_IOF_LINEAR          (0<<4)
#define USBHID_RIM_IOF_NONLINEAR       (1<<4)
#define USBHID_RIM_IOF_PREFFEREDSTATE  (0<<5)
#define USBHID_RIM_IOF_NOPREFFERED     (1<<5)
#define USBHID_RIM_IOF_NONULLPOSITION  (0<<6)
#define USBHID_RIM_IOF_NULLSTATE       (1<<6)
#define USBHID_RIM_IOF_NONVOLATILE     (0<<7)
#define USBHID_RIM_IOF_VOLATILE        (1<<7)
#define USBHID_RIM_IOF_BITFIELD        (0<<8)
#define USBHID_RIM_IOF_BUFFEREDBYTES   (1<<8)

/* Data (Collection main items) */
#define USBHID_RIM_COL_PHYSICAL        0x00
#define USBHID_RIM_COL_APPLICATION     0x01
#define USBHID_RIM_COL_LOGICAL         0x02
#define USBHID_RIM_COL_REPORT          0x03
#define USBHID_RIM_COL_NAMEDARRAY      0x04
#define USBHID_RIM_COL_USAGESWITCH     0x05
#define USBHID_RIM_COL_USAGEMODIFIER   0x06

/* Global items */
#define USBHID_RIG_UsagePage(x)       0x05,x
#define USBHID_RIG_UsagePage16(x)     0x06,(x&0xff),((x>>8)&0xff)
#define USBHID_RIG_UsagePage32(x)     0x07,(x&0xff),((x>>8)&0xff),((x>>16)&0xff),((x>>24)&0xff)
#define USBHID_RIG_UsageVendorPage(x) 0x06,x,0xff   /* vendor pages = 0xff00-0xffff */
#define USBHID_RIG_LogicalMin(x)      0x15,x
#define USBHID_RIG_LogicalMin16(x)    0x16,(x&0xff),((x>>8)&0xff)
#define USBHID_RIG_LogicalMin32(x)    0x17,(x&0xff),((x>>8)&0xff),((x>>16)&0xff),((x>>24)&0xff)
#define USBHID_RIG_LogicalMax(x)      0x25,x
#define USBHID_RIG_LogicalMax16(x)    0x26,(x&0xff),((x>>8)&0xff)
#define USBHID_RIG_LogicalMax32(x)    0x27,(x&0xff),((x>>8)&0xff),((x>>16)&0xff),((x>>24)&0xff)
#define USBHID_RIG_PhysicalMin(x)     0x35,x
#define USBHID_RIG_PhysicalMin16(x)   0x36,(x&0xff),((x>>8)&0xff)
#define USBHID_RIG_PhysicalMin32(x)   0x37,(x&0xff),((x>>8)&0xff),((x>>16)&0xff),((x>>24)&0xff)
#define USBHID_RIG_PhysicalMax(x)     0x45,x
#define USBHID_RIG_PhysicalMax16(x)   0x46,(x&0xff),((x>>8)&0xff)
#define USBHID_RIG_PhysicalMax32(x)   0x47,(x&0xff),((x>>8)&0xff),((x>>16)&0xff),((x>>24)&0xff)
#define USBHID_RIG_UnitExponent(x)    0x55,x
#define USBHID_RIG_Unit(x)            0x65,x
#define USBHID_RIG_Unit16(x)          0x66,(x&0xff),((x>>8)&0xff)
#define USBHID_RIG_Unit32(x)          0x67,(x&0xff),((x>>8)&0xff),((x>>16)&0xff),((x>>24)&0xff)
#define USBHID_RIG_ReportSize(x)      0x75,x
#define USBHID_RIG_ReportSize16(x)    0x76,(x&0xff),((x>>8)&0xff)
#define USBHID_RIG_ReportSize32(x)    0x77,(x&0xff),((x>>8)&0xff),((x>>16)&0xff),((x>>24)&0xff)
#define USBHID_RIG_ReportID(x)        0x85,x
#define USBHID_RIG_ReportCount(x)     0x95,x
#define USBHID_RIG_ReportCount16(x)   0x96,(x&0xff),((x>>8)&0xff)
#define USBHID_RIG_ReportCount32(x)   0x97,(x&0xff),((x>>8)&0xff),((x>>16)&0xff),((x>>24)&0xff)
#define USBHID_RIG_Push               0xA4
#define USBHID_RIG_Pop                0xB4
/* Unit codes */
// TODO - see USB HID specification 1.11 pg 37-39
//#define USBHID_RIG_UNIT_....

/* Local Items */
#define USBHID_RIL_Usage(x)           0x09,x
#define USBHID_RIL_Usage16(x)         0x0A,(x&0xff),((x>>8)&0xff)
#define USBHID_RIL_Usage32(x)         0x0B,(x&0xff),((x>>8)&0xff),((x>>16)&0xff),((x>>24)&0xff)
#define USBHID_RIL_UsageMin(x)        0x19,x
#define USBHID_RIL_UsageMax(x)        0x29,x
#define USBHID_RIL_DesignatorIdx(x)   0x39,x
#define USBHID_RIL_DesignatorMin(x)   0x49,x
#define USBHID_RIL_DesignatorMax(x)   0x59,x
#define USBHID_RIL_StringIdx(x)       0x79,x
#define USBHID_RIL_StringMin(x)       0x89,x
#define USBHID_RIL_StringMax(x)       0x99,x
#define USBHID_RIL_DelimiterOpen      0xA9,0x01
#define USBHID_RIL_DelimiterClose     0xA9,0x00


/* --- Usage pages --- from Universal Serial Bus HID Usage Tables 1.12 */
#define USHHID_UP_UNDEFINED           0x00
#define USBHID_UP_GENERICDESKTOP      0x01
#define USBHID_UP_SIMULATION          0x02
#define USBHID_UP_VR                  0x03
#define USBHID_UP_SPORT               0x04
#define USBHID_UP_GAME                0x05
#define USBHID_UP_GENERICDEVICE       0x06
#define USBHID_UP_KEYBOARD            0x07
#define USBHID_UP_LED                 0x08
#define USBHID_UP_BUTTON              0x09
#define USBHID_UP_ORDINAL             0x0A
#define USBHID_UP_TELEPHONY           0x0B
#define USBHID_UP_CONSUMER            0x0C
#define USBHID_UP_DIGITIZER           0x0D
#define USBHID_UP_PIDPAGE             0x0F
#define USBHID_UP_UNICODE             0x10
#define USBHID_UP_ALPHANUMERIC        0x14
#define USBHID_UP_MEDICAL             0x40
#define USBHID_UP_MONITOR             0x80
#define USBHID_UP_MONITOR_ENUMS       0x81
#define USBHID_UP_MONITOR_VESAVIRT    0x82
#define USBHID_UP_MONITOR_VESACMD     0x83
#define USBHID_UP_POWERDEVICE         0x84
#define USBHID_UP_BATTERYSYSTEM       0x85
#define USBHID_UP_BARCODESCANNER      0x8C
#define USBHID_UP_SCALE               0x8D
#define USBHID_UP_MAGNETICSTRIPE      0x8E
#define USBHID_UP_CAMERACONTROL       0x90
#define USBHID_UP_ARCADE              0x91
/* Vendor-defined 0xff00-0xffff */

/* Usage types */
// Linear Control (LC)
// ON/OFF Control (OOC)
// Momentary Control (MC)
// One Shot Control (OSC)
// Re-trigger Control (RTC)

/* Generic Deskop Page (Usage Page = 0x01) */
#define USBHID_UPGENERIC_POINTER                0x01
#define USBHID_UPGENERIC_MOUSE                  0x02
#define USBHID_UPGENERIC_JOYSTICK               0x04
#define USBHID_UPGENERIC_GAMEPAD                0x05
#define USBHID_UPGENERIC_KEYBOARD               0x06
#define USBHID_UPGENERIC_KEYPAD                 0x07
#define USBHID_UPGENERIC_MULTIAXIS              0x08
#define USBHID_UPGENERIC_TABLETPCSYSTEM         0x09
#define USBHID_UPGENERIC_X                      0x30
#define USBHID_UPGENERIC_Y                      0x31
#define USBHID_UPGENERIC_Z                      0x32
#define USBHID_UPGENERIC_RX                     0x33
#define USBHID_UPGENERIC_RY                     0x34
#define USBHID_UPGENERIC_RZ                     0x35
#define USBHID_UPGENERIC_SLIDER                 0x36
#define USBHID_UPGENERIC_DIAL                   0x37
#define USBHID_UPGENERIC_WHEEL                  0x38
#define USBHID_UPGENERIC_HATSWITCH              0x39
#define USBHID_UPGENERIC_COUNTED_BUFFER         0x3A
#define USBHID_UPGENERIC_BYTE_COUNT             0x3B
#define USBHID_UPGENERIC_MOTION_WAKEUP          0x3C
#define USBHID_UPGENERIC_START                  0x3D
#define USBHID_UPGENERIC_SELECT                 0x3E
#define USBHID_UPGENERIC_VX                     0x40
#define USBHID_UPGENERIC_VY                     0x41
#define USBHID_UPGENERIC_VZ                     0x42
#define USBHID_UPGENERIC_VBRX                   0x43
#define USBHID_UPGENERIC_VBRY                   0x44
#define USBHID_UPGENERIC_VBRZ                   0x45
#define USBHID_UPGENERIC_VNO                    0x46
#define USBHID_UPGENERIC_FEATURE_NOTIFICATION   0x47
#define USBHID_UPGENERIC_RESOLUTION_MULTIPLIER  0x48
#define USBHID_UPGENERIC_SYSTEM_CONTROL         0x80
#define USBHID_UPGENERIC_SYSTEM_POWER           0x81
#define USBHID_UPGENERIC_SYSTEM_SLEEP           0x82
#define USBHID_UPGENERIC_SYSTEM_WAKEUP          0x83
#define USBHID_UPGENERIC_SYSTEM_CONTEXT_MENU    0x84
#define USBHID_UPGENERIC_SYSTEM_MAIN_MENU       0x85
#define USBHID_UPGENERIC_SYSTEM_APP_MENU        0x86
#define USBHID_UPGENERIC_SYSTEM_HELP_MENU       0x87
#define USBHID_UPGENERIC_SYSTEM_MENU_EXIT       0x88
#define USBHID_UPGENERIC_SYSTEM_MENU_SELECT     0x89
#define USBHID_UPGENERIC_SYSTEM_MENU_RIGHT      0x8A
#define USBHID_UPGENERIC_SYSTEM_MENU_LEFT       0x8B
#define USBHID_UPGENERIC_SYSTEM_MENU_UP         0x8C
#define USBHID_UPGENERIC_SYSTEM_MENU_DOWN       0x8D
#define USBHID_UPGENERIC_SYSTEM_COLD_RESTART    0x8E
#define USBHID_UPGENERIC_SYSTEM_WARM_RESTART    0x8F
#define USBHID_UPGENERIC_DPAD_UP                0x90
#define USBHID_UPGENERIC_DPAD_DOWN              0x91
#define USBHID_UPGENERIC_DPAD_RIGHT             0x92
#define USBHID_UPGENERIC_DPAD_LEFT              0x93
#define USBHID_UPGENERIC_SYSTEM_DOCK            0xA0
#define USBHID_UPGENERIC_SYSTEM_UNDOCK          0xA1
#define USBHID_UPGENERIC_SYSTEM_SETUP           0xA2
#define USBHID_UPGENERIC_SYSTEM_BREAK           0xA3
#define USBHID_UPGENERIC_SYSTEM_DBG_BREAK       0xA4
#define USBHID_UPGENERIC_APP_BREAK              0xA5
#define USBHID_UPGENERIC_APP_DBG_BREAK          0xA6
#define USBHID_UPGENERIC_SYSTEM_SPEAKER_MUTE    0xA7
#define USBHID_UPGENERIC_SYSTEM_HIBERNATE       0xA8
#define USBHID_UPGENERIC_SYSDISP_INVERT         0xB0
#define USBHID_UPGENERIC_SYSDISP_INTERNAL       0xB1
#define USBHID_UPGENERIC_SYSDISP_EXTERNAL       0xB2
#define USBHID_UPGENERIC_SYSDISP_BOTH           0xB3
#define USBHID_UPGENERIC_SYSDISP_DUAL           0xB4
#define USBHID_UPGENERIC_SYSDISP_TOGGLE_INTEXT  0xB5
#define USBHID_UPGENERIC_SYSDISP_SWAP_PRISEC    0xB6
#define USBHID_UPGENERIC_SYSDISP_LCDAUTOSCALE   0xB7

/* Simulation Control Page (Usage Page = 0x02) */

/* VR Controls Page (Usage Page = 0x03) */

/* Sport Control Page (Usage Page = 0x04) */

/* Game Controls Page (Usage Page = 0x05) */

/* Generic Device Controls Page (Usage Page = 0x06) */

/* Keyboard/Keypad Page (Usage Page = 0x07) */

/* LED Page (Usage Page = 0x08) */

/* Button Page (Usage Page = 0x09) */

/* Ordinal Page (Usage Page = 0x0A) */

/* Telephony Device Page (Usage Page = 0x0B) */

/* Consumer Page (Usage Page = 0x0C) */
#define USBHID_UPCONSUMER_CONSUMER_CONTROL                          0x0001
#define USBHID_UPCONSUMER_NUMERIC_KEY_PAD                           0x0002
#define USBHID_UPCONSUMER_PROGRAMMABLE_BUTTONS                      0x0003
#define USBHID_UPCONSUMER_MICROPHONE                                0x0004
#define USBHID_UPCONSUMER_HEADPHONE                                 0x0005
#define USBHID_UPCONSUMER_GRAPHIC_EQUALIZER                         0x0006
//RESERVED 0x0007-0x001F
#define USBHID_UPCONSUMER_PLUS_10                                   0x0020
#define USBHID_UPCONSUMER_PLUS_100                                  0x0021
#define USBHID_UPCONSUMER_AM_PM                                     0x0022
//RESERVED 0x0023-0x003F
#define USBHID_UPCONSUMER_POWER                                     0x0030
#define USBHID_UPCONSUMER_RESET                                     0x0031
#define USBHID_UPCONSUMER_SLEEP                                     0x0032
#define USBHID_UPCONSUMER_SLEEP_AFTER                               0x0033
#define USBHID_UPCONSUMER_SLEEP_MODE                                0x0034
#define USBHID_UPCONSUMER_ILLUMINATION                              0x0035
#define USBHID_UPCONSUMER_FUNCTION_BUTTONS                          0x0036
//RESERVED 0x0037-0x003F
#define USBHID_UPCONSUMER_MENU                                      0x0040
#define USBHID_UPCONSUMER_MENU_PICK                                 0x0041
#define USBHID_UPCONSUMER_MENU_UP                                   0x0042
#define USBHID_UPCONSUMER_MENU_DOWN                                 0x0043
#define USBHID_UPCONSUMER_MENU_LEFT                                 0x0044
#define USBHID_UPCONSUMER_MENU_RIGHT                                0x0045
#define USBHID_UPCONSUMER_MENU_ESCAPE                               0x0046
#define USBHID_UPCONSUMER_MENU_VALUE_INCREASE                       0x0047
#define USBHID_UPCONSUMER_MENU_VALUE_DECREASE                       0x0048
//RESERVED 0x0049-0x005F
#define USBHID_UPCONSUMER_DATA_ON_SCREEN                            0x0060
#define USBHID_UPCONSUMER_CLOSED_CAPTION                            0x0061
#define USBHID_UPCONSUMER_CLOSED_CAPTION_SELECT                     0x0062
#define USBHID_UPCONSUMER_VCR_TV                                    0x0063
#define USBHID_UPCONSUMER_BROADCAST_MODE                            0x0064
#define USBHID_UPCONSUMER_SNAPSHOT                                  0x0065
#define USBHID_UPCONSUMER_STILL                                     0x0066
//RESERVED 0x0067-0x007F
#define USBHID_UPCONSUMER_SELECTION                                 0x0080
#define USBHID_UPCONSUMER_ASSIGN_SELECTION                          0x0081
#define USBHID_UPCONSUMER_MODE_STEP                                 0x0082
#define USBHID_UPCONSUMER_RECALL_LAST                               0x0083
#define USBHID_UPCONSUMER_ENTER_CHANNEL                             0x0084
#define USBHID_UPCONSUMER_ORDER_MOVIE                               0x0085
#define USBHID_UPCONSUMER_CHANNEL                                   0x0086
#define USBHID_UPCONSUMER_MEDIA_SELECTION                           0x0087
#define USBHID_UPCONSUMER_MEDIA_SELECT_COMPUTER                     0x0088
#define USBHID_UPCONSUMER_MEDIA_SELECT_TV                           0x0089
#define USBHID_UPCONSUMER_MEDIA_SELECT_WWW                          0x008A
#define USBHID_UPCONSUMER_MEDIA_SELECT_DVD                          0x008B
#define USBHID_UPCONSUMER_MEDIA_SELECT_TELEPHONE                    0x008C
#define USBHID_UPCONSUMER_MEDIA_SELECT_PROGRAM_GUIDE                0x008D
#define USBHID_UPCONSUMER_MEDIA_SELECT_VIDEO_PHONE                  0x008E
#define USBHID_UPCONSUMER_MEDIA_SELECT_GAMES                        0x008F
#define USBHID_UPCONSUMER_MEDIA_SELECT_MESSAGES                     0x0090
#define USBHID_UPCONSUMER_MEDIA_SELECT_CD                           0x0091
#define USBHID_UPCONSUMER_MEDIA_SELECT_VCR                          0x0092
#define USBHID_UPCONSUMER_MEDIA_SELECT_TUNER                        0x0093
#define USBHID_UPCONSUMER_QUIT                                      0x0094
#define USBHID_UPCONSUMER_HELP                                      0x0095
#define USBHID_UPCONSUMER_MEDIA_SELECT_TAPE                         0x0096
#define USBHID_UPCONSUMER_MEDIA_SELECT_CABLE                        0x0097
#define USBHID_UPCONSUMER_MEDIA_SELECT_SATELLITE                    0x0098
#define USBHID_UPCONSUMER_MEDIA_SELECT_SECURITY                     0x0099
#define USBHID_UPCONSUMER_MEDIA_SELECT_HOME                         0x009A
#define USBHID_UPCONSUMER_MEDIA_SELECT_CALL                         0x009B
#define USBHID_UPCONSUMER_CHANNEL_INCREMENT                         0x009C
#define USBHID_UPCONSUMER_CHANNEL_DECREMENT                         0x009D
#define USBHID_UPCONSUMER_MEDIA_SELECT_SAP                          0x009E
#define USBHID_UPCONSUMER_RESERVED_A0_VCR_PLUS                      0x009F
#define USBHID_UPCONSUMER_ONCE                                      0x00A1
#define USBHID_UPCONSUMER_DAILY                                     0x00A2
#define USBHID_UPCONSUMER_WEEKLY                                    0x00A3
#define USBHID_UPCONSUMER_MONTHLY                                   0x00A4
//RESERVED 0x00A5-0x00AF
#define USBHID_UPCONSUMER_PLAY                                      0x00B0
#define USBHID_UPCONSUMER_PAUSE                                     0x00B1
#define USBHID_UPCONSUMER_RECORD                                    0x00B2
#define USBHID_UPCONSUMER_FAST_FORWARD                              0x00B3
#define USBHID_UPCONSUMER_REWIND                                    0x00B4
#define USBHID_UPCONSUMER_SCAN_NEXT_TRACK                           0x00B5
#define USBHID_UPCONSUMER_SCAN_PREVIOUS_TRACK                       0x00B6
#define USBHID_UPCONSUMER_STOP                                      0x00B7
#define USBHID_UPCONSUMER_EJECT                                     0x00B8
#define USBHID_UPCONSUMER_RANDOM_PLAY                               0x00B9
#define USBHID_UPCONSUMER_SELECT_DISC                               0x00BA
#define USBHID_UPCONSUMER_ENTER_DISC                                0x00BB
#define USBHID_UPCONSUMER_REPEAT                                    0x00BC
#define USBHID_UPCONSUMER_TRACKING                                  0x00BD
#define USBHID_UPCONSUMER_TRACK_NORMAL                              0x00BE
#define USBHID_UPCONSUMER_SLOW_TRACKING                             0x00BF
#define USBHID_UPCONSUMER_FRAME_FORWARD                             0x00C0
#define USBHID_UPCONSUMER_FRAME_BACK                                0x00C1
#define USBHID_UPCONSUMER_MARK                                      0x00C2
#define USBHID_UPCONSUMER_CLEAR_MARK                                0x00C3
#define USBHID_UPCONSUMER_REPEAT_FROM_MARK                          0x00C4
#define USBHID_UPCONSUMER_RETURN_TO_MARK                            0x00C5
#define USBHID_UPCONSUMER_SEARCH_MARK_FORWARD                       0x00C6
#define USBHID_UPCONSUMER_SEARCH_MARK_BACKWARDS                     0x00C7
#define USBHID_UPCONSUMER_COUNTER_RESET                             0x00C8
#define USBHID_UPCONSUMER_SHOW_COUNTER                              0x00C9
#define USBHID_UPCONSUMER_TRACKING_INCREMENT                        0x00CA
#define USBHID_UPCONSUMER_TRACKING_DECREMENT                        0x00CB
#define USBHID_UPCONSUMER_STOP_EJECT                                0x00CC
#define USBHID_UPCONSUMER_PLAY_PAUSE                                0x00CD
#define USBHID_UPCONSUMER_PLAY_SKIP                                 0x00CE
//RESERVED 0x00CF-0x00DF
#define USBHID_UPCONSUMER_VOLUME                                    0x00E0
#define USBHID_UPCONSUMER_BALANCE                                   0x00E1
#define USBHID_UPCONSUMER_MUTE                                      0x00E2
#define USBHID_UPCONSUMER_BASS                                      0x00E3
#define USBHID_UPCONSUMER_TREBLE                                    0x00E4
#define USBHID_UPCONSUMER_BASS_BOOST                                0x00E5
#define USBHID_UPCONSUMER_SURROUND_MODE                             0x00E6
#define USBHID_UPCONSUMER_LOUDNESS                                  0x00E7
#define USBHID_UPCONSUMER_MPX                                       0x00E8
#define USBHID_UPCONSUMER_VOLUME_INCREMENT                          0x00E9
#define USBHID_UPCONSUMER_VOLUME_DECREMENT                          0x00EA
//RESERVED 0x00EB-0x00EF
#define USBHID_UPCONSUMER_SPEED_SELECT                              0x00F0
#define USBHID_UPCONSUMER_PLAYBACK_SPEED                            0x00F1
#define USBHID_UPCONSUMER_STANDARD_PLAY                             0x00F2
#define USBHID_UPCONSUMER_LONG_PLAY                                 0x00F3
#define USBHID_UPCONSUMER_EXTENDED_PLAY                             0x00F4
#define USBHID_UPCONSUMER_SLOW                                      0x00F5
//RESERVED 0x00F6-0x00FF
#define USBHID_UPCONSUMER_FAN_ENABLE                                0x0100
#define USBHID_UPCONSUMER_FAN_SPEED                                 0x0101
#define USBHID_UPCONSUMER_LIGHT_ENABLE                              0x0102
#define USBHID_UPCONSUMER_LIGHT_ILLUMINATION_LEVEL                  0x0103
#define USBHID_UPCONSUMER_CLIMATE_CONTROL_ENABLE                    0x0104
#define USBHID_UPCONSUMER_ROOM_TEMPERATURE                          0x0105
#define USBHID_UPCONSUMER_SECURITY_ENABLE                           0x0106
#define USBHID_UPCONSUMER_FIRE_ALARM                                0x0107
#define USBHID_UPCONSUMER_POLICE_ALARM                              0x0108
#define USBHID_UPCONSUMER_PROXIMITY                                 0x0109
#define USBHID_UPCONSUMER_MOTION                                    0x010A
#define USBHID_UPCONSUMER_DURESS_ALARM                              0x010B
#define USBHID_UPCONSUMER_HOLDUP_ALARM                              0x010C
#define USBHID_UPCONSUMER_MEDICAL_ALARM                             0x010D
//RESERVED 0x010E-0x014F
#define USBHID_UPCONSUMER_BALANCE_RIGHT                             0x0150
#define USBHID_UPCONSUMER_BALANCE_LEFT                              0x0151
#define USBHID_UPCONSUMER_BASS_INCREMENT                            0x0152
#define USBHID_UPCONSUMER_BASS_DECREMENT                            0x0153
#define USBHID_UPCONSUMER_TREBLE_INCREMENT                          0x0154
#define USBHID_UPCONSUMER_TREBLE_DECREMENT                          0x0155
//RESERVED 0x0156-0x015F
#define USBHID_UPCONSUMER_SPEAKER_SYSTEM                            0x0160
#define USBHID_UPCONSUMER_CHANNEL_LEFT                              0x0161
#define USBHID_UPCONSUMER_CHANNEL_RIGHT                             0x0162
#define USBHID_UPCONSUMER_CHANNEL_CENTER                            0x0163
#define USBHID_UPCONSUMER_CHANNEL_FRONT                             0x0164
#define USBHID_UPCONSUMER_CHANNEL_CENTER_FRONT                      0x0165
#define USBHID_UPCONSUMER_CHANNEL_SIDE                              0x0166
#define USBHID_UPCONSUMER_CHANNEL_SURROUND                          0x0167
#define USBHID_UPCONSUMER_CHANNEL_LOW_FREQUENCY_ENHANCEMENT         0x0168 
#define USBHID_UPCONSUMER_CHANNEL_TOP                               0x0169
#define USBHID_UPCONSUMER_CHANNEL_UNKNOWN                           0x016A
//RESERVED 0x016B-0x016F
#define USBHID_UPCONSUMER_SUBCHANNEL                                0x0170
#define USBHID_UPCONSUMER_SUBCHANNEL_INCREMENT                      0x0171
#define USBHID_UPCONSUMER_SUBCHANNEL_DECREMENT                      0x0172
#define USBHID_UPCONSUMER_ALTERNATE_AUDIO_INCREMENT                 0x0173
#define USBHID_UPCONSUMER_ALTERNATE_AUDIO_DECREMENT                 0x0174
//RESERVED 0x0175-0x017F
#define USBHID_UPCONSUMER_APPLICATION_LAUNCH_BUTTONS                0x0180
#define USBHID_UPCONSUMER_AL_LAUNCH_BUTTON_CONFIGURATION_TOOL       0x0181 
#define USBHID_UPCONSUMER_AL_PROGRAMMABLE_BUTTON_CONFIGURATION      0x0182 
#define USBHID_UPCONSUMER_AL_CONSUMER_CONTROL_CONFIGURATION         0x0183
#define USBHID_UPCONSUMER_AL_WORD_PROCESSOR                         0x0184
#define USBHID_UPCONSUMER_AL_TEXT_EDITOR                            0x0185
#define USBHID_UPCONSUMER_AL_SPREADSHEET                            0x0186
#define USBHID_UPCONSUMER_AL_GRAPHICS_EDITOR                        0x0187
#define USBHID_UPCONSUMER_AL_PRESENTATION_APP                       0x0188
#define USBHID_UPCONSUMER_AL_DATABASE_APP                           0x0189
#define USBHID_UPCONSUMER_AL_EMAIL_READER                           0x018A
#define USBHID_UPCONSUMER_AL_NEWSREADER                             0x018B
#define USBHID_UPCONSUMER_AL_VOICEMAIL                              0x018C
#define USBHID_UPCONSUMER_AL_CONTACTS_ADDRESS_BOOK                  0x018D
#define USBHID_UPCONSUMER_AL_CALENDAR_SCHEDULE                      0x018E
#define USBHID_UPCONSUMER_AL_TASK_PROJECT_MANAGER                   0x018F
#define USBHID_UPCONSUMER_AL_LOG_JOURNAL_TIMECARD                   0x0190
#define USBHID_UPCONSUMER_AL_CHECKBOOK_FINANCE                      0x0191
#define USBHID_UPCONSUMER_AL_CALCULATOR                             0x0192
#define USBHID_UPCONSUMER_AL_A_V_CAPTURE_PLAYBACK                   0x0193
#define USBHID_UPCONSUMER_AL_LOCAL_MACHINE_BROWSER                  0x0194
#define USBHID_UPCONSUMER_AL_LAN_WAN_BROWSER                        0x0195
#define USBHID_UPCONSUMER_AL_INTERNET_BROWSER                       0x0196
#define USBHID_UPCONSUMER_AL_REMOTE_NETWORKING_ISP_CONNECT          0x0197 
#define USBHID_UPCONSUMER_AL_NETWORK_CONFERENCE                     0x0198
#define USBHID_UPCONSUMER_AL_NETWORK_CHAT                           0x0199
#define USBHID_UPCONSUMER_AL_TELEPHONY_DIALER                       0x019A
#define USBHID_UPCONSUMER_AL_LOGON                                  0x019B
#define USBHID_UPCONSUMER_AL_LOGOFF                                 0x019C
#define USBHID_UPCONSUMER_AL_LOGON_LOGOFF                           0x019D
#define USBHID_UPCONSUMER_AL_TERMINAL_LOCK_SCREENSAVER              0x019E
#define USBHID_UPCONSUMER_AL_CONTROL_PANEL                          0x019F
#define USBHID_UPCONSUMER_AL_COMMAND_LINE_PROCESSOR_RUN             0x01A0
#define USBHID_UPCONSUMER_AL_PROCESS_TASK_MANAGER                   0x01A1
#define USBHID_UPCONSUMER_AL_SELECT_TASK_APPLICATION                0x01A2
#define USBHID_UPCONSUMER_AL_NEXT_TASK_APPLICATION                  0x01A3
#define USBHID_UPCONSUMER_AL_PREVIOUS_TASK_APPLICATION              0x01A4
#define USBHID_UPCONSUMER_AL_PREEMPTIVE_HALT_TASK_APPLICATION       0x01A5 
#define USBHID_UPCONSUMER_AL_INTEGRATED_HELP_CENTER                 0x01A6
#define USBHID_UPCONSUMER_AL_DOCUMENTS                              0x01A7
#define USBHID_UPCONSUMER_AL_THESAURUS                              0x01A8
#define USBHID_UPCONSUMER_AL_DICTIONARY                             0x01A9
#define USBHID_UPCONSUMER_AL_DESKTOP                                0x01AA
#define USBHID_UPCONSUMER_AL_SPELL_CHECK                            0x01AB
#define USBHID_UPCONSUMER_AL_GRAMMAR_CHECK                          0x01AC
#define USBHID_UPCONSUMER_AL_WIRELESS_STATUS                        0x01AD
#define USBHID_UPCONSUMER_AL_KEYBOARD_LAYOUT                        0x01AE
#define USBHID_UPCONSUMER_AL_VIRUS_PROTECTION                       0x01AF
#define USBHID_UPCONSUMER_AL_ENCRYPTION                             0x01B0
#define USBHID_UPCONSUMER_AL_SCREEN_SAVER                           0x01B1
#define USBHID_UPCONSUMER_AL_ALARMS                                 0x01B2
#define USBHID_UPCONSUMER_AL_CLOCK                                  0x01B3
#define USBHID_UPCONSUMER_AL_FILE_BROWSER                           0x01B4
#define USBHID_UPCONSUMER_AL_POWER_STATUS                           0x01B5
#define USBHID_UPCONSUMER_AL_IMAGE_BROWSER                          0x01B6
#define USBHID_UPCONSUMER_AL_AUDIO_BROWSER                          0x01B7
#define USBHID_UPCONSUMER_AL_MOVIE_BROWSER                          0x01B8
#define USBHID_UPCONSUMER_AL_DIGITAL_RIGHTS_MANAGER                 0x01B9
#define USBHID_UPCONSUMER_AL_DIGITAL_WALLET                         0x01BA
//RESERVED 0x01BB
#define USBHID_UPCONSUMER_AL_INSTANT_MESSAGING                      0x01BC
#define USBHID_UPCONSUMER_AL_OEM_FEATURES_TIPS_TUTORIAL_BROWSER     0x01BD 
#define USBHID_UPCONSUMER_AL_OEM_HELP                               0x01BE
#define USBHID_UPCONSUMER_AL_ONLINE_COMMUNITY                       0x01BF
#define USBHID_UPCONSUMER_AL_ENTERTAINMENT_CONTENT_BROWSER          0x01C0 
#define USBHID_UPCONSUMER_AL_ONLINE_SHOPPING_BROWSER                0x01C1
#define USBHID_UPCONSUMER_AL_SMARTCARD_INFORMATION_HELP             0x01C2
#define USBHID_UPCONSUMER_AL_MARKET_MONITOR_FINANCE_BROWSER         0x01C3 
#define USBHID_UPCONSUMER_AL_CUSTOMIZED_CORPORATE_NEWS_BROWSER      0x01C4 
#define USBHID_UPCONSUMER_AL_ONLINE_ACTIVITY_BROWSER                0x01C5
#define USBHID_UPCONSUMER_AL_RESEARCH_SEARCH_BROWSER                0x01C6
#define USBHID_UPCONSUMER_AL_AUDIO_PLAYER                           0x01C7
//RESERVED 0x01C8-0x01FF
#define USBHID_UPCONSUMER_GENERIC_GUI_APPLICATION_CONTROLS          0x0200 
#define USBHID_UPCONSUMER_AC_NEW                                    0x0201
#define USBHID_UPCONSUMER_AC_OPEN                                   0x0202
#define USBHID_UPCONSUMER_AC_CLOSE                                  0x0203
#define USBHID_UPCONSUMER_AC_EXIT                                   0x0204
#define USBHID_UPCONSUMER_AC_MAXIMIZE                               0x0205
#define USBHID_UPCONSUMER_AC_MINIMIZE                               0x0206
#define USBHID_UPCONSUMER_AC_SAVE                                   0x0207
#define USBHID_UPCONSUMER_AC_PRINT                                  0x0208
#define USBHID_UPCONSUMER_AC_PROPERTIES                             0x0209
#define USBHID_UPCONSUMER_AC_UNDO                                   0x021A
#define USBHID_UPCONSUMER_AC_COPY                                   0x021B
#define USBHID_UPCONSUMER_AC_CUT                                    0x021C
#define USBHID_UPCONSUMER_AC_PASTE                                  0x021D
#define USBHID_UPCONSUMER_AC_SELECT_ALL                             0x021E
#define USBHID_UPCONSUMER_AC_FIND                                   0x021F
#define USBHID_UPCONSUMER_AC_FIND_AND_REPLACE                       0x0220
#define USBHID_UPCONSUMER_AC_SEARCH                                 0x0221
#define USBHID_UPCONSUMER_AC_GO_TO                                  0x0222
#define USBHID_UPCONSUMER_AC_HOME                                   0x0223
#define USBHID_UPCONSUMER_AC_BACK                                   0x0224
#define USBHID_UPCONSUMER_AC_FORWARD                                0x0225
#define USBHID_UPCONSUMER_AC_STOP                                   0x0226
#define USBHID_UPCONSUMER_AC_REFRESH                                0x0227
#define USBHID_UPCONSUMER_AC_PREVIOUS_LINK                          0x0228
#define USBHID_UPCONSUMER_AC_NEXT_LINK                              0x0229
#define USBHID_UPCONSUMER_AC_BOOKMARKS                              0x022A
#define USBHID_UPCONSUMER_AC_HISTORY                                0x022B
#define USBHID_UPCONSUMER_AC_SUBSCRIPTIONS                          0x022C
#define USBHID_UPCONSUMER_AC_ZOOM_IN                                0x022D
#define USBHID_UPCONSUMER_AC_ZOOM_OUT                               0x022E
#define USBHID_UPCONSUMER_AC_ZOOM                                   0x022F
#define USBHID_UPCONSUMER_AC_FULL_SCREEN_VIEW                       0x0230
#define USBHID_UPCONSUMER_AC_NORMAL_VIEW                            0x0231
#define USBHID_UPCONSUMER_AC_VIEW_TOGGLE                            0x0232
#define USBHID_UPCONSUMER_AC_SCROLL_UP                              0x0233
#define USBHID_UPCONSUMER_AC_SCROLL_DOWN                            0x0234
#define USBHID_UPCONSUMER_AC_SCROLL                                 0x0235
#define USBHID_UPCONSUMER_AC_PAN_LEFT                               0x0236
#define USBHID_UPCONSUMER_AC_PAN_RIGHT                              0x0237
#define USBHID_UPCONSUMER_AC_PAN                                    0x0238
#define USBHID_UPCONSUMER_AC_NEW_WINDOW                             0x0239
#define USBHID_UPCONSUMER_AC_TILE_HORIZONTALLY                      0x023A
#define USBHID_UPCONSUMER_AC_TILE_VERTICALLY                        0x023B
#define USBHID_UPCONSUMER_AC_FORMAT                                 0x023C
#define USBHID_UPCONSUMER_AC_EDIT                                   0x023D
#define USBHID_UPCONSUMER_AC_BOLD                                   0x023E
#define USBHID_UPCONSUMER_AC_ITALICS                                0x023F
#define USBHID_UPCONSUMER_AC_UNDERLINE                              0x0240
#define USBHID_UPCONSUMER_AC_STRIKETHROUGH                          0x0241
#define USBHID_UPCONSUMER_AC_SUBSCRIPT                              0x0242
#define USBHID_UPCONSUMER_AC_SUPERSCRIPT                            0x0243
#define USBHID_UPCONSUMER_AC_ALL_CAPS                               0x0244
#define USBHID_UPCONSUMER_AC_ROTATE                                 0x0245
#define USBHID_UPCONSUMER_AC_RESIZE                                 0x0246
#define USBHID_UPCONSUMER_AC_FLIP_HORIZONTAL                        0x0247
#define USBHID_UPCONSUMER_AC_FLIP_VERTICAL                          0x0248
#define USBHID_UPCONSUMER_AC_MIRROR_HORIZONTAL                      0x0249
#define USBHID_UPCONSUMER_AC_MIRROR_VERTICAL                        0x024A
#define USBHID_UPCONSUMER_AC_FONT_SELECT                            0x024B
#define USBHID_UPCONSUMER_AC_FONT_COLOR                             0x024C
#define USBHID_UPCONSUMER_AC_FONT_SIZE                              0x024D
#define USBHID_UPCONSUMER_AC_JUSTIFY_LEFT                           0x024E
#define USBHID_UPCONSUMER_AC_JUSTIFY_CENTER_H                       0x024F
#define USBHID_UPCONSUMER_AC_JUSTIFY_RIGHT                          0x0250
#define USBHID_UPCONSUMER_AC_JUSTIFY_BLOCK_H                        0x0251
#define USBHID_UPCONSUMER_AC_JUSTIFY_TOP                            0x0252
#define USBHID_UPCONSUMER_AC_JUSTIFY_CENTER_V                       0x0253
#define USBHID_UPCONSUMER_AC_JUSTIFY_BOTTOM                         0x0254
#define USBHID_UPCONSUMER_AC_JUSTIFY_BLOCK_V                        0x0255
#define USBHID_UPCONSUMER_AC_INDENT_DECREASE                        0x0256
#define USBHID_UPCONSUMER_AC_INDENT_INCREASE                        0x0257
#define USBHID_UPCONSUMER_AC_NUMBERED_LIST                          0x0258
#define USBHID_UPCONSUMER_AC_RESTART_NUMBERING                      0x0259
#define USBHID_UPCONSUMER_AC_BULLETED_LIST                          0x025A
#define USBHID_UPCONSUMER_AC_PROMOTE                                0x025B
#define USBHID_UPCONSUMER_AC_DEMOTE                                 0x025C
#define USBHID_UPCONSUMER_AC_YES                                    0x025D
#define USBHID_UPCONSUMER_AC_NO                                     0x025E
#define USBHID_UPCONSUMER_AC_CANCEL                                 0x025F
#define USBHID_UPCONSUMER_AC_CATALOG                                0x0260
#define USBHID_UPCONSUMER_AC_BUY_CHECKOUT                           0x0261
#define USBHID_UPCONSUMER_AC_ADD_TO_CART                            0x0262
#define USBHID_UPCONSUMER_AC_EXPAND                                 0x0263
#define USBHID_UPCONSUMER_AC_EXPAND_ALL                             0x0264
#define USBHID_UPCONSUMER_AC_COLLAPSE                               0x0265
#define USBHID_UPCONSUMER_AC_COLLAPSE_ALL                           0x0266
#define USBHID_UPCONSUMER_AC_PRINT_PREVIEW                          0x0267
#define USBHID_UPCONSUMER_AC_PASTE_SPECIAL                          0x0268
#define USBHID_UPCONSUMER_AC_INSERT_MODE                            0x0269
#define USBHID_UPCONSUMER_AC_DELETE                                 0x026A
#define USBHID_UPCONSUMER_AC_LOCK                                   0x026B
#define USBHID_UPCONSUMER_AC_UNLOCK                                 0x026C
#define USBHID_UPCONSUMER_AC_PROTECT                                0x026D
#define USBHID_UPCONSUMER_AC_UNPROTECT                              0x026E
#define USBHID_UPCONSUMER_AC_ATTACH_COMMENT                         0x026F
#define USBHID_UPCONSUMER_AC_DELETE_COMMENT                         0x0270
#define USBHID_UPCONSUMER_AC_VIEW_COMMENT                           0x0271
#define USBHID_UPCONSUMER_AC_SELECT_WORD                            0x0272
#define USBHID_UPCONSUMER_AC_SELECT_SENTENCE                        0x0273
#define USBHID_UPCONSUMER_AC_SELECT_PARAGRAPH                       0x0274
#define USBHID_UPCONSUMER_AC_SELECT_COLUMN                          0x0275
#define USBHID_UPCONSUMER_AC_SELECT_ROW                             0x0276
#define USBHID_UPCONSUMER_AC_SELECT_TABLE                           0x0277
#define USBHID_UPCONSUMER_AC_SELECT_OBJECT                          0x0278
#define USBHID_UPCONSUMER_AC_REDO_REPEAT                            0x0279
#define USBHID_UPCONSUMER_AC_SORT                                   0x027A
#define USBHID_UPCONSUMER_AC_SORT_ASCENDING                         0x027B
#define USBHID_UPCONSUMER_AC_SORT_DESCENDING                        0x027C
#define USBHID_UPCONSUMER_AC_FILTER                                 0x027D
#define USBHID_UPCONSUMER_AC_SET_CLOCK                              0x027E
#define USBHID_UPCONSUMER_AC_VIEW_CLOCK                             0x027F
#define USBHID_UPCONSUMER_AC_SELECT_TIME_ZONE                       0x0280
#define USBHID_UPCONSUMER_AC_EDIT_TIME_ZONES                        0x0281
#define USBHID_UPCONSUMER_AC_SET_ALARM                              0x0282
#define USBHID_UPCONSUMER_AC_CLEAR_ALARM                            0x0283
#define USBHID_UPCONSUMER_AC_SNOOZE_ALARM                           0x0284
#define USBHID_UPCONSUMER_AC_RESET_ALARM                            0x0285
#define USBHID_UPCONSUMER_AC_SYNCHRONIZE                            0x0286
#define USBHID_UPCONSUMER_AC_SEND_RECEIVE                           0x0287
#define USBHID_UPCONSUMER_AC_SEND_TO                                0x0288
#define USBHID_UPCONSUMER_AC_REPLY                                  0x0289
#define USBHID_UPCONSUMER_AC_REPLY_ALL                              0x028A
#define USBHID_UPCONSUMER_AC_FORWARD_MSG                            0x028B
#define USBHID_UPCONSUMER_AC_SEND                                   0x028C
#define USBHID_UPCONSUMER_AC_ATTACH_FILE                            0x028D
#define USBHID_UPCONSUMER_AC_UPLOAD                                 0x028E
#define USBHID_UPCONSUMER_AC_DOWNLOAD                               0x028F //(SAVE TARGET AS)
#define USBHID_UPCONSUMER_AC_SET_BORDERS                            0x0290
#define USBHID_UPCONSUMER_AC_INSERT_ROW                             0x0291
#define USBHID_UPCONSUMER_AC_INSERT_COLUMN                          0x0292
#define USBHID_UPCONSUMER_AC_INSERT_FILE                            0x0293
#define USBHID_UPCONSUMER_AC_INSERT_PICTURE                         0x0294
#define USBHID_UPCONSUMER_AC_INSERT_OBJECT                          0x0295
#define USBHID_UPCONSUMER_AC_INSERT_SYMBOL                          0x0296
#define USBHID_UPCONSUMER_AC_SAVE_AND_CLOSE                         0x0297
#define USBHID_UPCONSUMER_AC_RENAME                                 0x0298
#define USBHID_UPCONSUMER_AC_MERGE                                  0x0299
#define USBHID_UPCONSUMER_AC_SPLIT                                  0x029A
#define USBHID_UPCONSUMER_AC_DISRIBUTE_HORIZONTALLY                 0x029B
#define USBHID_UPCONSUMER_AC_DISTRIBUTE_VERTICALLY                  0x029C
//RESERVED 0x029D-0xFFFF



/* Digitizer Page (Usage Page = 0x0D) */

/* Unicode Page (Usage Page = 0x10) */

/* Alphanumeric Display Page (Usage Page = 0x14) */

/* Medical Instrument Page (Usage Page = 0x40) */

/* ... */


#endif /* _USB_HID_CLASS_SPECIFICATIONS_ */
