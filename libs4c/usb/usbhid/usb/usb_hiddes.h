/**
 * usb_hiddes.h - USB HID description
 */

#ifndef _USB_HID_DESCRIPTION_HEADER_FILE_
  #define _USB_HID_DESCRIPTION_HEADER_FILE_

#include <usb/usb.h>
#include <usb/usb_hid.h>

/* -------------------------------------------------------------------------- */
/* flags in  struct _tag_usb_hid_descriptors_table.flags */
#define USB_HIDDES_FL_GET_REPIN          0x0001   /* IN REPORT can be read with GET_REPORT request */
#define USB_HIDDES_FL_GET_REPOUT         0x0002   /* OUT REPORT can be read with GET_REPORT request */
#define USB_HIDDES_FL_GET_REPFEAT        0x0004   /* FEATURE REPORT can be read with GET_REPORT request */
#define USB_HIDDES_FL_GET_IDLE           0x0008   /* IDLE can be read with GET_IDLE request */
#define USB_HIDDES_FL_SET_REPIN          0x0010   /* IN REPORT can be write with SET_REPORT request */
#define USB_HIDDES_FL_SET_REPOUT         0x0020   /* OUT REPORT can be write with SET_REPORT request */
#define USB_HIDDES_FL_SET_REPFEAT        0x0040   /* FEATURE REPORT can be write with SET_REPORT request */
#define USB_HIDDES_FL_SET_IDLE           0x0080   /* IDLE can be write with SET_IDLE request */

#define USB_HIDDES_FL_UPDATED_REPIN      0x0100   /* IN REPORT was updated with SET_REPORT request */
#define USB_HIDDES_FL_UPDATED_REPOUT     0x0200   /* OUT REPORT was updated with SET_REPORT request */
#define USB_HIDDES_FL_UPDATED_REPFEAT    0x0400   /* FEATURE REPORT was updated with SET_REPORT request */

typedef struct usb_hid_usage {
    const USB_HID_DESCRIPTOR *pHIDDescriptor;
    int iHIDDescriptorSize;
    const unsigned char *pHIDReportDescriptor;
    int iHIDReportDescriptorSize;

    int InterfaceIdx; /* for testing Interface index */
    int nReportIDs; /* number of reports, if =0 ReportID is not used in Get/Set_Report */

//    int nreports; /* number of reports = size of all arrays (pInReport, pOutReport, pFeatureReport, pIdle) */
//    unsigned char **pInReport;  // array of IN reports (in requests LO(wValue) is index to the array)
//    int iInReportSize;
//    unsigned char **pOutReport; // array of OUT reports
//    int iOutReportSize;
//    unsigned char **pFeatureReport; // array of FEATURE reports
//    int iFeatureReportSize;
//    unsigned char *pIdle; // array of IDLE values

//    unsigned long flags;

    // callback functions
    int (*init)(usb_device_t *udev, struct usb_hid_usage *uhid) REENTRANT_SIGN; /* user callback function to initiate usb hid structures */
    int (*get_report)(usb_device_t *udev, struct usb_hid_usage *uhid) REENTRANT_SIGN; /* user callback function to process GET_REPORT class request */
    int (*set_report)(usb_device_t *udev, struct usb_hid_usage *uhid) REENTRANT_SIGN; /* user callback function to process SET_REPORT class request */
    int (*get_idle)(usb_device_t *udev, struct usb_hid_usage *uhid) REENTRANT_SIGN; /* user callback function to process GET_IDLE class request */
    int (*set_idle)(usb_device_t *udev, struct usb_hid_usage *uhid) REENTRANT_SIGN; /* user callback function to process SET_IDLE class request */
    int (*get_protocol)(usb_device_t *udev, struct usb_hid_usage *uhid) REENTRANT_SIGN; /* user callback function to process GET_PROTOCOL class request */
    int (*set_protocol)(usb_device_t *udev, struct usb_hid_usage *uhid) REENTRANT_SIGN; /* user callback function to process SET_PROTOCOL class request */

} usb_hid_usage_t;


//extern USB_HID_DESCRIPTORS_TABLE usb_hid_descriptors;

int usbhid_get_descriptor(usb_device_t *udev);
int usbhid_class_req(usb_device_t *udev);
int usbhid_init(usb_device_t *udev, usb_hid_usage_t *uhid);

#endif /* _USB_HID_DESCRIPTION_HEADER_FILE_ */
