/*
 * usbhid.c - core functions
 */

#include <stdint.h>
#include <system_def.h>
#include <usb/usb.h>
#include <usb/usb_hid.h>
#include <usb/usb_hiddes.h>

int usbhid_get_descriptor(usb_device_t *udev)
{
  uint8_t *pDesc;
  uint16_t len = 0;
  USB_DEVICE_REQUEST *dreq = &(udev->request);
  uint8_t dtype;
  usb_hid_usage_t *uhid;

  if (!udev->usb_class_struct) return USB_COMPLETE_NOTPROCESSED;
  uhid = udev->usb_class_struct;

  if ((dreq->bmRequestType & USB_RECIPIENT) == USB_RECIPIENT_INTERFACE) {
    dtype = (dreq->wValue >> 8) & 0xff; /* MSB part of wValue */
    switch (dtype) {
    case USBHID_DESCRIPTOR_TYPE_HID:
      if ((dreq->wIndex & 0xff) != uhid->InterfaceIdx)
        return USB_COMPLETE_FAIL;
      pDesc = (uint8_t *)uhid->pHIDDescriptor;
      len = uhid->iHIDDescriptorSize;
      break;
    case USBHID_DESCRIPTOR_TYPE_REPORT:
      if ((dreq->wIndex & 0xff) != uhid->InterfaceIdx)
        return USB_COMPLETE_FAIL;
      pDesc = (uint8_t *)uhid->pHIDReportDescriptor;
      len = uhid->iHIDReportDescriptorSize;
      break;
    case USBHID_DESCRIPTOR_TYPE_PHYSICAL:
    default:
      return USB_COMPLETE_FAIL;
    }
    if (dreq->wLength < len)
      len = dreq->wLength;
    usb_send_control_data(udev, pDesc, len);
    return USB_COMPLETE_OK;
  }
  return USB_COMPLETE_NOTPROCESSED;
}

int usbhid_class_req(usb_device_t *udev)
{
  usb_hid_usage_t *uhid;
  int urc;

  if (!udev->usb_class_struct) return USB_COMPLETE_NOTPROCESSED;
  uhid = udev->usb_class_struct;

  /* TODO: test if interface index is correct */

  switch (udev->request.bRequest) {
    case USBHID_REQUEST_GET_REPORT:
      if (uhid->get_report) {
        return uhid->get_report(udev, uhid);
      }
      break;

    case USBHID_REQUEST_SET_REPORT:
      if (uhid->set_report) {
        urc = uhid->set_report(udev, uhid);
        if (urc==USB_COMPLETE_OK)
          usb_udev_ack_setup(udev);
        return urc;
      }
      break;

    case USBHID_REQUEST_GET_IDLE:
      if (uhid->get_idle) {
        return uhid->get_idle(udev, uhid);
      }
      break;

    case USBHID_REQUEST_SET_IDLE:
      if (uhid->set_idle) {
        urc = uhid->set_idle(udev, uhid);
        if (urc==USB_COMPLETE_OK)
          usb_udev_ack_setup(udev);
        return urc;
      }
      break;

    case USBHID_REQUEST_GET_PROTOCOL:
      if (uhid->get_protocol) {
        return uhid->get_protocol(udev, uhid);
      }
      break;

    case USBHID_REQUEST_SET_PROTOCOL:
      if (uhid->set_protocol) {
        urc = uhid->set_protocol(udev, uhid);
        if (urc==USB_COMPLETE_OK)
          usb_udev_ack_setup(udev);
        return urc;
      }
      break;
  }
  return USB_COMPLETE_NOTPROCESSED;
}

int usbhid_init(usb_device_t *udev, usb_hid_usage_t *uhid)
{
  int rc;
  if (udev==NULL) return -1;
  udev->class_fnc = usbhid_class_req;
  udev->usb_class_struct = uhid;
  rc = usb_init(udev);
  if (rc) return rc;
  if (uhid->init) rc = uhid->init(udev, uhid);
  return rc;
}
